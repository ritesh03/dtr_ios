//
//  GroupEditViewModel.swift
//  dtr-ios-test
//
//  Created by apple on 27/07/21.
//

import Foundation
import Combine

class GroupEditViewModel: ObservableObject {
    
    @Published  var userGroup = [DtrGroup]()
    
    init(group:DtrGroup) {
        self.grouEdit = group
    }
    
    @Published var grouEdit : DtrGroup
}
