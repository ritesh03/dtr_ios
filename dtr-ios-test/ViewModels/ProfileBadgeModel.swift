//
//  ProfileBadgeModel.swift
//  dtr-ios-test
//
//  Created by apple on 27/08/21.
//

import Foundation

@dynamicMemberLookup
struct RideBadgeData {
    var forID = [String:DtrBadge]()
    
    subscript(dynamicMember id: String) -> DtrBadge {
        return forID[id, default: DtrBadge(id: id)]
    }
}

struct DtrBadge : Hashable, Codable {
    var name = ""
    var description = ""
    var id: String = ""
    var iconInput = ""
    var status = false

    init(id: String = "SETUP") {
        self.id = id
    }
    
    static var `default`: DtrBadge {
        var sampleProfile = DtrBadge(id: "Default Badge")
        sampleProfile.name = "Default Name"
        return sampleProfile
    }
    
    var data:DataDict {
        let result: DataDict = [
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "iconInput": self.iconInput,
            "status": self.status,
        ]
        return result
    }
    
    init(data: DataDict) {
        self.id = asString(data["id"], defaultValue: "unknown")
        self.name = asString(data["name"], defaultValue: "New Rider")
        if self.name == "" {
            self.name = ""
        }
        self.description = asString(data["description"], defaultValue: "")
        self.iconInput = asString(data["iconInput"], defaultValue: "")
        self.status = asBool(data["status"])
    }
}
