//
//  RideTagsModel.swift
//  dtr-ios-test
//
//  Created by apple on 01/09/21.
//

import Foundation

@dynamicMemberLookup
struct RideTagsData {
    var forID = [String:RideTagsModel]()
    
    subscript(dynamicMember id: String) -> RideTagsModel {
        return forID[id, default: RideTagsModel(id: id)]
    }
}

struct RideTagsModel : Hashable, Codable {
    var description = ""
    var iconFile = ""
    var id: String = ""
    var name = ""
    var sortOrder = 0
    var status = false

    init(id: String = "SETUP") {
        self.id = id
    }
    
    static var `default`: DtrBadge {
        var sampleProfile = DtrBadge(id: "Default Badge")
        sampleProfile.name = "Default Name"
        return sampleProfile
    }
    
    var data:DataDict {
        let result: DataDict = [
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "iconFile": self.iconFile,
            "status": self.status,
            "sortOrder": self.sortOrder,
        ]
        return result
    }
    
    init(data: DataDict) {
        self.id = asString(data["id"], defaultValue: "unknown")
        self.name = asString(data["name"], defaultValue: "New Rider")
        if self.name == "" {
            self.name = ""
        }
        self.description = asString(data["description"], defaultValue: "")
        self.iconFile = asString(data["iconFile"], defaultValue: "")
        self.status = asBool(data["status"])
        self.sortOrder = asInt(data["sortOrder"], defaultValue: 0)
    }
}
