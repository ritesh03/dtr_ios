//
//  UserSearchViewModel.swift
//  dtr-ios-test
//
//  Created by Mobile on 03/03/21.
//

import UIKit
import Combine
import Firebase

class UserSearchViewModel : ObservableObject {
    
    @Published var resetValueArray: Bool = false
    
    func searchUsers1(searchKeyboard: String,data: DataDict,completion: @escaping ([DtrProfile],Int,[String]) -> Void) {
        var userProfiles = [DtrProfile]()
        var userProfilesId = [String]()
        let callData: DataDict = ["name": "search.lookup","keyword":searchKeyboard,"args": data]
        DtrCommand.sharedInstance.dtrCommandWrapper(callData: callData) { result in
            let dictUsr = asDataDict(result.output)
            if let userProfileArry = dictUsr["hits"] as? NSArray {
                for i in 0..<userProfileArry.count {
                    let userDict = asDataDict(userProfileArry[i])
                    if let objId =  userDict["objectID"] as? String {
                        var profile = DtrData.sharedInstance.profileDirectory[dynamicMember: objId]
                        profile.isFollower = profile.followed.contains(DtrData.sharedInstance.profileID)
                        if !userProfiles.contains(profile) && DtrData.sharedInstance.profileID != profile.id{
                            userProfiles.append(profile)
                            userProfilesId.append(profile.id)
                        }
                    }
                }
                completion(userProfiles, dictUsr["nbHits"] as! Int,userProfilesId)
            }
        }
    }
}

class GroupSearchViewModel : ObservableObject {
    
    @Published var resetValueArray: Bool = false
    
    func groupSearch(searchKeyboard: String,data: DataDict,completion: @escaping ([DtrGroup],Int) -> Void) {
        var userGroups = [DtrGroup]()
        let callData: DataDict = ["name": "search.groupSearch","keyword":searchKeyboard,"args": data]
        DtrCommand.sharedInstance.dtrCommandWrapper(callData: callData) { result in
            let dictUsr = asDataDict(result.output)
            if let userProfileArry = dictUsr["hits"] as? NSArray {
                for i in 0..<userProfileArry.count {
                    let userDict = asDataDict(userProfileArry[i])
                    if let objId =  userDict["objectID"] as? String {
                        let groupDetails = DtrData.sharedInstance.groupsDirectory[dynamicMember: objId]
                        if !userGroups.contains(groupDetails){
                            userGroups.append(groupDetails)
                        }
                    }
                }
                completion(userGroups, dictUsr["nbHits"] as! Int)
            }
        }
    }
}
