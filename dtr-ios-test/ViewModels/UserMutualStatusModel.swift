//
//  UserMutualStatusModel.swift
//  dtr-ios-test
//
//  Created by apple on 24/08/21.
//

import Foundation


struct UserMutualStatusModel {
    let userId: String
    let staus: String
    init(userId: String, staus: String) {
        self.userId = userId
        self.staus = staus
    }
}

struct RideIdCheckModel {
    let rideId: String
    let timeIntrval: Date
    init(rideId: String, timeIntrval: Date) {
        self.rideId = rideId
        self.timeIntrval = timeIntrval
    }
}
