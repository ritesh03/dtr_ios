//
//  UserListViewModel.swift
//  dtr-ios-test
//
//  Created by Mobile on 27/05/21.
//

import Foundation
import Combine

class UserListViewModel : ObservableObject {
    
    var didChange = PassthroughSubject<UserListViewModel, Never>()
    
    @Published  var userProfiles = [DtrProfile]()
    @Published  var userFollowers = [DtrProfile]()
    @Published  var userFollowings = [DtrProfile]()
    @Published  var userGroups = [DtrGroup]()
    @Published  var showActivityIndicator : Bool = false { didSet { self.didChange.send(self) }}
    
    var dtrData = DtrData.sharedInstance
    
    func getFollowers(profile:DtrProfile) {
        var profilesDict = [String:DtrProfile]()
        showActivityIndicator = true
        dtrData.resolveFollowedProfiles(profile: profile) { resolvedProfile in
            profilesDict = resolvedProfile.followdProfiles
            self.userProfiles =  Array(profilesDict.values).sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
            self.showActivityIndicator = false
        }
    }
    
    func getFollowings(profile:DtrProfile) {
        var profilesDict = [String:DtrProfile]()
        showActivityIndicator = true
        dtrData.resolveFollowingProfiles(profile: profile) { resolvedProfile in
            profilesDict = resolvedProfile.followingProfiles
            self.userProfiles =  Array(profilesDict.values).sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
            self.showActivityIndicator = false
        }
    }
    
    func getFriends(profile:DtrProfile) {
        var profilesDict = [String:DtrProfile]()
        showActivityIndicator = true
        dtrData.resolveFriendProfiles(profile: profile) { resolvedProfile in
            profilesDict = resolvedProfile.friendProfiles
            self.userProfiles =  Array(profilesDict.values).sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
            self.showActivityIndicator = false
        }
    }
    
    
    func getFriendsForInvite(profile:DtrProfile) {
        var profilesForFriends = [String:DtrProfile]()
        showActivityIndicator = true
        dtrData.resolveFriendProfiles(profile: profile) { resolvedProfile in
            profilesForFriends = resolvedProfile.friendProfiles
            self.userProfiles =  Array(profilesForFriends.values).sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
            self.showActivityIndicator = false
        }
    }
    
    func getFollowersForInvite(profile:DtrProfile) {
        var profilesDict = [String:DtrProfile]()
        showActivityIndicator = true
        dtrData.resolveFollowedProfiles(profile: profile) { resolvedProfile in
            profilesDict = resolvedProfile.followdProfiles
            self.userFollowers =  Array(profilesDict.values).sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
            self.showActivityIndicator = false
        }
    }
    
    func getFollowingsForInvite(profile:DtrProfile) {
        var profilesDict = [String:DtrProfile]()
        showActivityIndicator = true
        dtrData.resolveFollowingProfiles(profile: profile) { resolvedProfile in
            profilesDict = resolvedProfile.followingProfiles
            self.userFollowings =  Array(profilesDict.values).sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
            self.showActivityIndicator = false
        }
    }

    func SearchInviteFriends(isForType: UserType, searchKeyboard: String,data: DataDict,completion: @escaping ([DtrProfile],Int,[String]) -> Void) {
        var callData: DataDict = ["": ""]
        if isForType == .friend {
            callData  = ["name": "search.friends","keyword":searchKeyboard,"data": data]
        } else if isForType == .follower {
            callData = ["name": "search.followers","keyword":searchKeyboard,"data": data]
        } else {
            callData = ["name": "search.groupProfiles","keyword":searchKeyboard,"data": data]
        }
        var userProfiles = [DtrProfile]()
        var userProfilesId = [String]()
        print(callData)
        DtrCommand.sharedInstance.dtrCommandWrapper(callData: callData) { result in
            print(result)
            let dictUsr = asDataDict(result.output)
            if let userProfileArry = dictUsr["hits"] as? NSArray {
                for i in 0..<userProfileArry.count {
                    let userDict = asDataDict(userProfileArry[i])
                    if let objId =  userDict["objectID"] as? String {
                        var profile = DtrData.sharedInstance.profileDirectory[dynamicMember: objId]
                        profile.isFollower = profile.followed.contains(DtrData.sharedInstance.profileID)
                        if !userProfiles.contains(profile) && DtrData.sharedInstance.profileID != profile.id{
                            userProfiles.append(profile)
                            userProfilesId.append(profile.id)
                        }
                    }
                }
                completion(userProfiles, dictUsr["nbHits"] as! Int,userProfilesId)
            }
        }
        
    }

    
    func getGroupMembersForInvite(profile:DtrProfile, listOfSelectedMembers: [String]) {
        let listSetOfSelectedMembers = Set(UserStore.shared.isLocallSelectGroupMembers + UserStore.shared.isLocallSelectFriends + UserStore.shared.isLocallSelectFollowers + listOfSelectedMembers)
        showActivityIndicator = true
        self.userGroups.removeAll()
        let groupList = profile.joinedgroups + profile.createdGroups
        for groupId in groupList {
            dtrData.resolveGroupProfiles(profile: profile, groupID: groupId) { resolvedProfile in
                if resolvedProfile.members.count != 0 {
                    self.userGroups.append(resolvedProfile)
                    self.userGroups = self.userGroups.sorted(by: {(first: DtrGroup, second: DtrGroup) -> Bool in return first.name.lowercased() < second.name.lowercased() })
                    for i in 0...(self.userGroups.count - 1) {
                        let findListsetOfSelecectedGoup = Set(self.userGroups[i].members)
                        let containAllElementsOfCurrentGroup = findListsetOfSelecectedGoup.isSubset(of: listSetOfSelectedMembers)

                        if self.userGroups[i].leader == profile.id && findListsetOfSelecectedGoup.count != 1 {
                            if containAllElementsOfCurrentGroup {
                                self.userGroups[i].selectDeselectTitle = "Deselect all"
                            }else{
                                self.userGroups[i].selectDeselectTitle = "Select all"
                            }
                            
                        } else {
                            self.userGroups[i].selectDeselectTitle = ""
                        }
                    }
                }
                self.showActivityIndicator = false
            }
        }
    }
}


