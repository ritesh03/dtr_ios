//
//  RideSkillIndicators.swift
//  dtr-ios-test
//
//  Created by apple on 14/07/21.
//

import SwiftUI
import Firebase

struct RideSkillIndicators: View {
    
    @Binding var ride: DtrRide
    @State var comeFromHomeScreen = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    
    var body: some View {
        if (ride.details.skillA || ride.details.skillB || ride.details.skillC || ride.details.skillD || ride.details.skillS) {
            HStack(alignment: .center) {
                if ride.details.skillA {
                    showSkilsImage(imgName: RideSkillsIndicatorsString.SkillAStr)
                }
                if ride.details.skillB {
                    showSkilsImage(imgName: RideSkillsIndicatorsString.SkillBStr)
                }
                if ride.details.skillC {
                    showSkilsImage(imgName: RideSkillsIndicatorsString.SkillCStr)
                }
                if ride.details.skillD {
                    showSkilsImage(imgName: RideSkillsIndicatorsString.SkillDStr)
                }
                if ride.details.skillS {
                    showSkilsImage(imgName: RideSkillsIndicatorsString.SkillSStr)
                }
                
                if comeFromHomeScreen {
                    Group {
                        Text("skill levels").font(.system(size: 14)).foregroundColor(.gray)
                        Image(ImageConstantsName.UnVectorImg)
                            .resizable()
                            .frame(width: 15,height: 15)
                    }.opacity(dtrData.profile.id == ride.leader ? 1 : 0)
                }
                Spacer()
            }
            .onAppear{
                Analytics.logEvent(AnalyticsScreen.aboutSkills.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
            }
        }else{
            EmptyView()
        }
    }
}

func showSkilsImage(imgName:String) -> AnyView {
    return AnyView(
        Image(imgName)
            .resizable()
            .frame(width: 25, height: 25, alignment: .center)
            .cornerRadius(10)
    )
}

struct RideSkillIndicators_Previews: PreviewProvider {
    static var previews: some View {
        RideSkillIndicators(ride: .constant(DtrRide.default))
    }
}
