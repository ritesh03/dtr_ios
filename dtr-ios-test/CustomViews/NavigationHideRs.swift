//
//  NavigationHideRs.swift
//  dtr-ios-test
//
//  Created by Mobile on 25/01/21.
//

import Foundation
import SwiftUI


struct HiddenNavigationBar: ViewModifier {
    func body(content: Content) -> some View {
        content
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("")
    }
}

extension View {
    func hiddenNavigationBarStyle() -> some View {
        modifier( HiddenNavigationBar() )
    }
}
