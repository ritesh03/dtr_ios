//
//  CircleImage.swift
//  dtr-ios-test
//
//  Created by Mobile on 21/01/21.
//

import SwiftUI

struct CustomCircleImageWithCustomProperty: View {
    var imgName = ImageConstantsNameForChatScreen.DownArrowWithCircleImg
    var imgWidth:CGFloat = 20
    var imgHeight:CGFloat = 20
    
    var body: some View {
        Image(imgName)
            .resizable()
            .frame(width: imgWidth, height: imgHeight)
            .clipShape(Circle())
    }
}

struct CustomCircleImageWithCustomProperty_Previews: PreviewProvider {
    static var previews: some View {
        CustomCircleImageWithCustomProperty()
    }
}
