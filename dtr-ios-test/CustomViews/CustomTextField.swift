//
//  CustomTextField.swift
//  dtr-ios-test
//
//  Created by Mobile on 16/02/21.
//

import SwiftUI

struct CustomTextField: View {
    @Binding var userInput : String
    var placholerText = CommonAllString.BlankStr
    var isBackgroundColor = false
    var BackgroundColor = CommonAllString.BlankStr
    var isforegroundColor = false
    var foregroundColor = CommonAllString.BlankStr
    var custmHeight = 40
    var custmAlignment : Alignment = .center
    var cornorRadius = 0
     
    var body: some View {
        TextField(placholerText, text: $userInput)
            .frame(height: CGFloat(custmHeight), alignment: custmAlignment)
            .foregroundColor(.white)
            .padding(.horizontal)
            .background(isBackgroundColor ==  false ? Color.clear:Color.init(BackgroundColor))
            .cornerRadius(CGFloat(cornorRadius))
    }
}

struct CustomTextField_Previews: PreviewProvider {
    static var previews: some View {
        CustomTextField(userInput: .constant(""))
    }
}
