//
//  NavigationHideRs.swift
//  dtr-ios-test
//
//  Created by Mobile on 25/01/21.
//

import Foundation
import SwiftUI

//struct HiddenNavigationBar: ViewModifier {
//    func body(content: Content) -> some View {
//        content
//            .navigationBarHidden(true)
//            .navigationBarBackButtonHidden(true)
//            //.navigationBarTitle(CommonAllString.BlankStr, displayMode: .inline)
//            .navigationTitle(CommonAllString.BlankStr)
//           // .navigationViewStyle(.stack)//ritesh
//            //.navigationBarTitleDisplayMode(.never)
//    }
//}

extension View {
    func hiddenNavigationBarStyle() -> some View {
        self
       // modifier( HiddenNavigationBar() )
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
}
