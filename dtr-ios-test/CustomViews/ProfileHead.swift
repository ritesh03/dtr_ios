//
//  SwiftUIView.swift
//  dtr-ios-test
//
//  Created by Mobile on 27/01/21.
//

import SwiftUI
import Combine
import FirebaseStorage
import SDWebImage
import SDWebImageSwiftUI

extension PlatformImage {
    static var wifiExclamationmark: PlatformImage {
        return PlatformImage(systemName: ImageConstantsName.WifiMarkImg)!.withTintColor(.label, renderingMode: .alwaysOriginal)
    }
}

enum ProfileSize {
    case small
    case medium
    case large
    case extralarge
}

struct ProfileHead: View {
    var profile: DtrProfile
    var size: ProfileSize = .small
    var comeFromAnoucmentScreen = false
    var comeFromChatScreen = false
    var comeFromNotificationScreen = false
    @State var doneApiOnce = false
    @State var badgeDetails = DtrBadge.default
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    private var frameSize: CGFloat {
        switch size {
        case .small:
            return 32
        case .medium:
            return 48
        case .large:
            return 60
        case .extralarge:
            return 128
        }
    }
    
    @State private var anoucmentImage = Image(uiImage: UIImage(named: ImageConstantsName.AccetAppIconImg)!)
    @State private var image = Image(ImageConstantsName.PersonPlaceholderUserImg)
    
    
    var body: some View {
        Group {
            if profile.id == "DTR" {
                if comeFromAnoucmentScreen {
                    self.anoucmentImage
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: self.frameSize, height: self.frameSize)
                } else {
                    self.image
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: self.frameSize, height: self.frameSize)
                }
            } else {
                if profile.profileImage != "" {
                    ZStack(alignment: .bottomTrailing) {
                        WebImage(url:URL(string: profile.profileImage), options: [.progressiveLoad], isAnimating: .constant(true))
                            .purgeable(true)
                            .placeholder(image)
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: self.frameSize, height: self.frameSize)
                            .clipShape(Circle())
                            .accessibility(label: Text("user profile picture"))
                            .padding(1)//.overlay(,alignment: .bottomTrailing)
                        showBadegImage(rideId: profile.id)
                            .frame(width: 20, height:20)
                            .offset(x: 2, y: -2)
                    }
                   
                } else {
                    if comeFromChatScreen {
                        self.anoucmentImage
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: self.frameSize, height: self.frameSize)
                    }else{
                        self.image
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: self.frameSize, height: self.frameSize)
                            .clipShape(Circle())
                            .accessibility(label: Text("user profile picture"))
                            .padding(1).overlay(showBadegImage(rideId: profile.id),alignment: .bottomTrailing)
                    }
                }
            }
        }
        
        .onAppear {
            if self.profile.id != "DTR" {
                if profile.profileImage == "" {
                    if comeFromAnoucmentScreen {
                        self.image = anoucmentImage
                    }else{
                        self.image = Image(uiImage: self.dtrData.profilePictureWithName(profileID: self.profile.id, inLarge: false))
                    }
                }
            }
        }
    }
    
    func showBadegImage(rideId: String) -> AnyView {
        if !comeFromNotificationScreen {
            if dtrData.profileDirectory[dynamicMember: rideId].badge != "" {
                if dtrData.badgesDirectory[dynamicMember: dtrData.profileDirectory[dynamicMember: rideId].badge].iconInput != "" {
                    return AnyView(
                        WebImage(url:URL(string: dtrData.badgesDirectory[dynamicMember: dtrData.profileDirectory[dynamicMember: rideId].badge].iconInput), options: [.progressiveLoad], isAnimating: .constant(true))
                            .purgeable(true)
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 20, height: 20)
                            .clipShape(Circle())
                    )
                }
            }
        }
        return AnyView(EmptyView())
    }
}

struct ProfileHead_Previews: PreviewProvider {
    static var previews: some View {
        ProfileHead(profile: DtrProfile(id: "PREVIEW"))
    }
}
