//
//  ButtonCustom.swift
//  dtr-ios-test
//
//  Created by Mobile on 21/01/21.
//

import SwiftUI

struct CustomButtonCircleWithCustomProperty: View {
    var btnTitle = CommonAllString.BlankStr
    var btnWidth: CGFloat = 20.0
    var btnHeight: CGFloat = 20.0
    var backgroundColord = false
    var txtColorAccent  = false
    var strokeColor  = false
    var textBold  = false
    var lineWidth  = true
    var disabled = false
    var action: () -> Void
    
 
    var body: some View {
        Button(action: self.action) {
            Text(btnTitle)
                .font(.system(size: 17, weight: textBold ==  true ? .bold : .regular, design: .default))
                .frame(width: btnWidth, height: btnHeight)
                .padding()
                .foregroundColor(setColor())
                .background(backgroundColord == false ? Color.clear : Color.init(ColorConstantsName.AccentColour))
                .cornerRadius(btnWidth/2)
                .overlay(
                    RoundedRectangle(cornerRadius: btnWidth/2)
                        .stroke(strokeColor == false ? Color.gray: Color.init(ColorConstantsName.AccentColour), lineWidth: lineWidth == false ? 0: 1)
                )
        }
    }
    
    func setColor() -> Color {
        if txtColorAccent {
            return Color.init(ColorConstantsName.AccentColour)
        } else {
            if disabled {
                return Color.gray
            } else {
               return Color.white
            }
        }
    }
}

struct ButtonCustom_Previews: PreviewProvider {
    static var previews: some View {
        CustomButtonCircleWithCustomProperty(action:{
        })
    }
}
