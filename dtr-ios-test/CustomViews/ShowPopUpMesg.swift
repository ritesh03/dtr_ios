//
//  ShowPopUpMesg.swift
//  dtr-ios-test
//
//  Created by Mobile on 23/02/21.
//

import SwiftUI

struct ShowPopUpMesg: View {
    var titleMesg:String
    var customMesg:String
    @Binding var show: Bool
    @Binding var opticity: Bool
    var body: some View {
        if show {
            ZStack {
                VStack(alignment:.leading,spacing:20) {
                    HStack(alignment: .top){
                        if titleMesg != "" {
                            Text(titleMesg)
                                .font(.system(size: 16, weight: .bold, design: .default))
                            
                            Spacer()
                        }else{
                            Text(customMesg)
                                .font(.system(size: 14))
                                .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                        }
                        Spacer()
                        Image(systemName:ImageConstantsName.XmarkImg)
                            .resizable()
                            .frame(width: 10, height: 10)
                            .foregroundColor(.black)
                            .onTapGesture {
                                show.toggle()
                            }
                    }
                    if titleMesg != CommonAllString.BlankStr {
                        Text(customMesg)
                            .font(.system(size: 14))
                            .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    }
                }
            }.padding()
            .background(Color.white)
            .opacity(opticity == false ? 0.95:1.0)
            .foregroundColor(.black)
            .cornerRadius(10)
        }
    }
}

struct ShowPopUpMesg_Previews: PreviewProvider {
    static var previews: some View {
        ShowPopUpMesg(titleMesg: "", customMesg: "222", show: .constant(true), opticity: .constant(false))
    }
}
