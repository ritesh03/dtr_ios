//
//  ReturnSingleText.swift
//  dtr-ios-test
//
//  Created by apple on 25/08/21.
//

import SwiftUI

struct ReturnSingleText: View {
    var textName : String
    var isaccentColor = false
    var body: some View {
        Text(textName).font(.system(size: 14, design: .default))
            .foregroundColor(isaccentColor == false ? Color.gray:Color.init(ColorConstantsName.AccentColour))
    }
}
