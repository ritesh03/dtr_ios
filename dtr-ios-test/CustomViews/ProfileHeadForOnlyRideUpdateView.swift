//
//  lblView.swift
//  dtr-ios-test
//
//  Created by apple on 23/07/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct ProfileHeadForOnlyRideUpdateView: View {
    var profile: DtrProfile
    var size: ProfileSize = .small
    var comeFromRideUpdatNotifcaiton =  false
    @State var showRideDetailScreen = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    private var frameSize: CGFloat {
        switch size {
        case .small:
            return 32
        case .medium:
            return 48
        case .large:
            return 64
        case .extralarge:
            return 128
        }
    }
    
    @State private var image = Image(ImageConstantsName.PersonPlaceholderUserImg)
    
    
    var body: some View {
        Group {
            if profile.id == "DTR" {
                self.image
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: self.frameSize, height: self.frameSize)
            } else {
                if profile.profileImage != "" {
                    WebImage(url:URL(string: profile.profileImage), options: [.progressiveLoad], isAnimating: .constant(true))
                        .purgeable(true)
                        .placeholder(image)
                        .renderingMode(.original)
                        .resizable()
                        .frame(width: self.frameSize, height: self.frameSize)
                        .clipShape(Circle())
                        .accessibility(label: Text("user profile picture"))
                        .padding(1)
                } else {
                    self.image
                        .renderingMode(.original)
                        .resizable()
                        .frame(width: self.frameSize, height: self.frameSize)
                        .clipShape(Circle())
                        .accessibility(label: Text("user profile picture"))
                        .padding(1)
                }
            }
        }
        
        .onTapGesture {
            if comeFromRideUpdatNotifcaiton {
                showRideDetailScreen = true
            }
        }
        
        .onAppear {
            if self.profile.id != "DTR" {
                if profile.profileImage == "" {
                    self.image = Image(uiImage: self.dtrData.profilePictureWithName(profileID: self.profile.id, inLarge: false))
                }
            }
        }
        
        if comeFromRideUpdatNotifcaiton {
            NavigationLink(destination: ProfileScreen(updatedDismissView:.constant(false),day: .constant(0),userProfile: .constant(dtrData.profileInfo(profileID: profile.id)),showBackButtonOrNot:true), isActive: $showRideDetailScreen){
                EmptyView()
            }.buttonStyle(PlainButtonStyle()).frame(width: 0, height: 0).hidden().background(Color.red)
        }
    }
}
