//
//  ProfilePicture.swift
//  dtr-ios-test
//
//  Created by Mobile on 20/05/21.
//

import SwiftUI

struct ProfilePicture: View {
    var picture: UIImage
    var size: ProfileSize = .small
    
    private var frameSize: CGFloat {
        switch size {
        case .small:
            return 32
        case .medium:
            return 48
        case .large:
            return 64
        case .extralarge:
            return 128
        }
    }
    
    var body: some View {
        Image(uiImage: picture)
            .renderingMode(.original)
            .resizable()
            .frame(width: self.frameSize + 2, height: self.frameSize + 2)
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.gray, lineWidth: 1))
            .accessibility(label: Text(CommonAllString.TextUserProfilePicStr))
    }
}
