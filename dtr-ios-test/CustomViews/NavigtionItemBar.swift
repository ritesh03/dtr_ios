//
//  NavigtionItemBar.swift
//  dtr-ios-test
//
//  Created by Mobile on 16/02/21.
//

import SwiftUI

struct NavigtionItemBar: View {
    var isComeFromPrivacyTermsScreen = false
    var isImage = false
    var isSystemImage  = false
    var isText = false
    var textTitle = CommonAllString.BlankStr
    var systemImageName = CommonAllString.BlankStr
    var imageName = CommonAllString.BlankStr
    var textColorDiffernt  = false
    var textColorCode  = CommonAllString.BlankStr
    var action: () -> Void
    
    var body: some View {
        Button(action: self.action) {
            HStack {
                if isImage {
                    if isSystemImage {
                        if isComeFromPrivacyTermsScreen {
                            Image(systemName: systemImageName)
                                .foregroundColor(textColorDiffernt == false ? Color.white:Color.init(textColorCode))
                        }else{
                            Image(systemName: systemImageName).foregroundColor(.white)
                        }
                    }else{
                        Image(imageName)
                    }
                }
                if isText {
                    Text(textTitle)
                        .foregroundColor(textColorDiffernt == false ? Color.white:Color.init(textColorCode))
                }
            }}
    }
}
