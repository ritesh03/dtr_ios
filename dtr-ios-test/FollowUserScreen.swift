//
//  FollowUserScreen.swift
//  dtr-ios-test
//
//  Created by apple on 02/07/21.
//

import SwiftUI
import Firebase
 
struct FollowUserScreen: View {
    
    @Binding var currentDay : Int
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    @State private var titleDtr: String = ""
    @State private var showSearchTxtFld: Bool = false
    @State private var clearAllText: Bool = false
    @State private var showActivity = false
    @State private var totalCountArray = 0
    @State var comesFromChatScreen = false
    @State var updateKeyboard = true
    
    @State private var page: Int = 0
    @State private var totalUserCount: Int = 0
    private let pageSize: Int = 20
    
    @State var userProfiles = [DtrProfile]()
    
    var body: some View {
        ZStack {
            Color.init("DTR-MainThemeBackground")
                .ignoresSafeArea()
            VStack(spacing: 15) {
                VStack(spacing:0) {
                    List {
                        if updateKeyboard {
                            if userProfiles.count > 0 {
                                ForEach(userProfiles.indices, id: \.self) { indexs in
                                    if userProfiles.count > indexs {
                                    if  userProfiles.indices.contains(indexs) {
                                        Text("\(indexs)")
                                        if userProfiles[indexs].id != "" {
                                            UserSearchView(day:$currentDay,userProfiles:$userProfiles[indexs],showProfileScreen:.constant(false)).listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0)).onAppear {
                                                print(currentDay)
                                            }
                                        }
                                            
    //                                            if ((userProfiles.endIndex - 1) == indexs) {
    //                                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
    //                                                    if userProfiles.count != totalUserCount && userProfiles.count < totalUserCount{
    //                                                        if titleDtr != "" {
    //                                                            page += 1
    //                                                            searchUsers1(searchKeyboard: titleDtr, data: ["page": "\(page)","hitsPerPage":20], completion: { response in
    //                                                            })
    //                                                        }
    //                                                    }
    //                                                }
    //                                            }
    //                                        }.listRowBackground(Color.clear)
    //                                        if ((userProfiles.endIndex - 1) == indexs) {
    //                                            if userProfiles.count != totalUserCount && userProfiles.count < totalUserCount{
    //                                                ShowLoadingText().listRowBackground(Color.clear).frame(alignment: .center)
    //                                            }
    //                                        }
                                            
                                        }
                                    }
                                }
                            }
                        }
                        
                    }.listStyle(PlainListStyle()).listRowBackground(Color.clear)
                }
            }.onAppear{
                searchUsers1(searchKeyboard: "Ranj", data: ["page": "0","hitsPerPage":20], completion: { response in
                    totalUserCount = response - 1
                    updateKeyboard
                })
                
                DispatchQueue.main.asyncAfter(deadline: .now()+3.0) {
                    userProfiles.removeAll()
                    userProfiles.append(DtrProfile.default)
                }
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarItems(leading:
                                Button(action: {
                                    UIApplication.shared.endEditing()
                                    
                                    if comesFromChatScreen {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                }) {
                                    HStack {
                                        if comesFromChatScreen {
                                            Image(systemName: "arrow.left")
                                        }
                                        Text("Find and Invite")
                                    }})
        
    }
    
    func searchUsers1(searchKeyboard: String,data: DataDict,completion: @escaping (Int) -> Void) {
        let callData: DataDict = ["name": "search.lookup","keyword":searchKeyboard,"args": data]
        DtrCommand.sharedInstance.dtrCommandWrapper(callData: callData) { result in
            let dictUsr = asDataDict(result.output)
                        
            print("Current page = ",dictUsr["page"] as Any,"Total Pages = ",dictUsr["nbPages"] as Any,"Total User = ",dictUsr["nbHits"] as Any)
            
            if let userProfileArry = dictUsr["hits"] as? NSArray {
                for i in 0..<userProfileArry.count {
                    let userDict = asDataDict(userProfileArry[i])
                    
                    if let objId =  userDict["objectID"] as? String {
                        var profile = DtrData.sharedInstance.directory[dynamicMember: objId]
                        
                        profile.isFollower = profile.followed.contains(DtrData.sharedInstance.profileID)
                        if !self.userProfiles.contains(profile) && DtrData.sharedInstance.profileID != profile.id{
                            self.userProfiles.append(profile)
                        }
                    }
                    
                }
                print("Search Result = ",userProfileArry.count,"Search Total Count = ",self.userProfiles.count)
                completion(dictUsr["nbHits"] as! Int)
            }
        }
    }
}
