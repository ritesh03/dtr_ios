//
//  FontClass.swift
//  dtr-ios-test
//
//  Created by Mobile on 16/04/21.
//

import UIKit
import SwiftUI

struct AppFontName {
    static let thin = "Raleway-Thin"
    static let regular = "Raleway-Regular"
    static let medium = "Raleway-Medium"
    static let semibold = "Raleway-SemiBold"
    static let bold = "Raleway-Bold"
    static let italic = "Raleway-Italic"
    static let lightItalic = "Raleway-LightItalic"
    static let boldItalic = "Raleway-BoldItalic"
    static let black = "Raleway-Black"
    static let blackItalic = "Raleway-BlackItalic"
    static let light = "Raleway-Light"
}

extension Font {

    static func thinFontSize(size: CGFloat) -> Font {
        return Font.custom(AppFontName.thin, size: size)
    }
    static func regularFontSize(size: CGFloat) -> Font {
        return Font.custom(AppFontName.regular, size: size)
    }
    static func mediumFontSize(size: CGFloat) -> Font {
        return Font.custom(AppFontName.medium, size: size)
    }
    static func semiboldFontSize(size: CGFloat) -> Font {
        return Font.custom(AppFontName.semibold, size: size)
    }
    
    static func boldFontSize(size: CGFloat) -> Font {
        return Font.custom(AppFontName.bold, size: size)
    }
    
    static func italicFontSize(size: CGFloat) -> Font {
        return Font.custom(AppFontName.italic, size: size)
    }

    static func LightItalicFontSize(size: CGFloat) -> Font {
        return Font.custom(AppFontName.lightItalic, size: size)
    }
    
    static func boldItalicFontSize(size: CGFloat) -> Font {
        return Font.custom(AppFontName.boldItalic, size: size)
    }
    
    static func blackFontSize(size: CGFloat) -> Font {
        return Font.custom(AppFontName.black, size: size)
    }
    
    static func blackItalicFontSize(size: CGFloat) -> Font {
        return Font.custom(AppFontName.blackItalic, size: size)
    }
    
    static func lightFontSize(size: CGFloat) -> Font {
        return Font.custom(AppFontName.light, size: size)
    }
}
