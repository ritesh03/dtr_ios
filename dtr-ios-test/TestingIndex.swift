////
////  TestingIndex.swift
////  dtr-ios-test
////
////  Created by apple on 12/07/21.
////
//
//import SwiftUI
//import Firebase
//
//struct HomeView1: View {
//    
//    @State private var day = 0
//    @State private var tabSelection = 0
//    @State private var resetNavigationID = UUID()
//    @State private var resetNavigationID1 = UUID()
//    @State private var resetNavigationID2 = UUID()
//    @State private var selection = 0
//    
//    var body: some View {
//        let selectable = Binding(
//            get: {UserStore.shared.saveTabBarUniqueIdForSecond},
//            set: {
//                if $0 == 2 {
//                    // For Profile Screen
//                    if UserStore.shared.isForSecondTabId == 2 {
//                        UserStore.shared.saveTabBarUniqueIdForSecond = $0
//                        self.resetNavigationID1 = UUID()
//                        UserStore.shared.isForSecondTabId = 3
//                    }else{
//                        UserStore.shared.isForSecondTabId = 2
//                    }
//                    UserStore.shared.isForFirstTabId = 0
//                    UserStore.shared.isForThirdTabId = 0
//                } else if $0 == 1 {
//                    if UserStore.shared.isForFirstTabId == 1 {
//                        UserStore.shared.saveTabBarUniqueIdForSecond = $0
//                        self.resetNavigationID = UUID()
//                        UserStore.shared.isForFirstTabId = 5
//                    } else {
//                        UserStore.shared.isForFirstTabId = 1
//                    }
//                    UserStore.shared.isForSecondTabId = 0
//                    UserStore.shared.isForThirdTabId = 0
//                } else if $0 == 3 {
//                    if UserStore.shared.isForThirdTabId == 3 {
//                        if UserStore.shared.isRootClassSearch {
//                            UserStore.shared.saveTabBarUniqueIdForSecond = $0
//                            self.resetNavigationID2 = UUID()
//                            UserStore.shared.isForThirdTabId = 6
//                        }
//                    } else {
//                        UserStore.shared.isForThirdTabId = 3
//                    }
//                    UserStore.shared.isForFirstTabId = 0
//                    UserStore.shared.isForSecondTabId = 0
//                }
//                UserStore.shared.saveTabBarUniqueIdForSecond = $0
//            })
//        TabView(selection: selectable) {
//            First_Ride_tab1().id(self.resetNavigationID)
//                .tabItem {
//                    Image(systemName: "bicycle")
//                    Text("My Home")
//                }.tag(1)
//            NavigationView {
//                FollowerScreen(currentDay: $day).id(self.resetNavigationID2)
//            }.hiddenNavigationBarStyle()
//            .tabItem {
//                Image(systemName: "person.2")
//                Text("User")
//            }.tag(3)
//            NavigationView {
//                ProfileScreen1().id(self.resetNavigationID1)
//            }.hiddenNavigationBarStyle()
//            .tabItem {
//                Image(systemName: "person")
//                Text("Section")
//            }.tag(2)
//        }.accentColor(.white)
//        .onAppear(perform: {
//            UIScrollView.appearance().bounces = true
//            self.resetNavigationID = UUID()
//            UserStore.shared.saveTabBarUniqueIdForSecond = 0
//        })
//    }
//}
//
//struct First_Ride_tab1: View {
//    var body: some View {
//        NavigationView {
//            ZStack {
//                Color.init("DTR-MainThemeBackground")
//                    .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
//            }
//        }
//    }
//}
//
//struct ProfileScreen1: View {
//    
//    var body: some View {
//        ZStack {
//            Color.init("DTR-MainThemeBackground")
//                .ignoresSafeArea()
//        }
//    }
//}
//
//import SwiftUI
//import Firebase
//
//struct FollowerScreen1: View {
//    
//    @Binding var currentDay : Int
//    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
//    @StateObject var userSearchModel = UserSearchViewModel()
//    @State private var titleDtr: String = ""
//    @State private var showSearchTxtFld: Bool = false
//    @State private var clearAllText: Bool = false
//    var callApiIfRequired: Bool = true
//    @State private var showActivity = false
//    @State private var totalCountArray = 0
//    @State var comesFromChatScreen = false
//    @State var updateKeyboard = true
//    
//    @State private var page: Int = 0
//    @State private var totalUserCount: Int = 0
//    @State private var showListOrNot = false
//    private let pageSize: Int = 20
//    
//    var body: some View {
//        ZStack {
//            VStack(spacing: 15) {
//                HStack {
//                    CustomTxtfldForFollowerScrn(isDeleteAcntScreen: .constant(false), text:  $titleDtr, clearAllText: $clearAllText, isFirstResponder: $updateKeyboard, completion: { (reponse) in
//                        updateKeyboard = true
//                        if titleDtr.count >= 3 {
//                            page = 0
//                            showActivity = true
//                            totalCountArray = 0
//                            showListOrNot = true
//                            userSearchModel.userProfiles.removeAll()
//                            userSearchModel.searchUsers1(searchKeyboard: titleDtr, data: ["page": "0","hitsPerPage":20], completion: { response in
//                                totalUserCount = response - 1
//                            })
//                            
//                        }else if titleDtr.count <= 3 {
//                            DispatchQueue.main.async {
//                                page = 0
//                                showActivity = false
//                                totalCountArray = 0
//                                showListOrNot = false
//                            }
//                        }
//                    })
//                    
//                    if self.showActivity {
//                        ProgressView()
//                            .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
//                            .frame(width: 30, height: 30)
//                    }
//                    
//                    Button(action: {
//                        withAnimation {
//                            page = 0
//                            totalCountArray = 0
//                            clearAllText = true
//                            updateKeyboard = false
//                            showListOrNot = false
//                        }
//                    }) {
//                        Image(systemName: "xmark")
//                            .aspectRatio(contentMode: .fit)
//                    }
//                }
//                .padding()
//                .cornerRadius(10)
//                .foregroundColor(.white)
//                .padding(.all)
//                .frame(height: 50)
//                VStack(spacing:0) {
//                    List {
//                        if showListOrNot {
//                                ForEach(userSearchModel.userProfiles.indices, id: \.self) { indexs in
//                                        if userSearchModel.userProfiles.count > indexs {
//                                            UserSearchView(day:$currentDay,userProfiles:$userSearchModel.userProfiles[indexs],showProfileScreen:.constant(false)).listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
//                                                .onAppear {
//                                                if ((userSearchModel.userProfiles.endIndex - 1) == indexs) {
//                                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
//                                                        if userSearchModel.userProfiles.count != totalUserCount && userSearchModel.userProfiles.count < totalUserCount{
//                                                            if titleDtr != "" {
//                                                                page += 1
//                                                                userSearchModel.searchUsers1(searchKeyboard: titleDtr, data: ["page": "\(page)","hitsPerPage":20], completion: { response in
//                                                                })
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                            }.listRowBackground(Color.clear)
//                                            if ((userSearchModel.userProfiles.endIndex - 1) == indexs) {
//                                                if userSearchModel.userProfiles.count != totalUserCount && userSearchModel.userProfiles.count < totalUserCount{
//                                                    ShowLoadingText().listRowBackground(Color.clear).frame(alignment: .center)
//                                                }
//                                            }
//                                        }
//                                    
//                                }
//                            
//                        }
//                    }.listStyle(PlainListStyle()).listRowBackground(Color.clear)
//                }
//            }
//        }
//        .onDisappear{
//            updateKeyboard = false
//        }
//        .onReceive(userSearchModel.$userProfiles, perform: { (retnrUsrProfile) in
//            if retnrUsrProfile.count > 0 {
//                if retnrUsrProfile.count == totalCountArray {
//                    totalCountArray = 0
//                    showActivity = false
//                }else{
//                    totalCountArray += 1
//                    if retnrUsrProfile.count == totalCountArray {
//                        totalCountArray = 0
//                        showActivity = false
//                    }
//                }
//            }
//        })
//        
//        .navigationBarBackButtonHidden(true)
//        .navigationBarTitle("", displayMode: .inline)
//        .navigationBarItems(leading:
//                                Button(action: {
//                                    UIApplication.shared.endEditing()
//                                    
//                                    if comesFromChatScreen {
//                                        self.presentationMode.wrappedValue.dismiss()
//                                    }
//                                }) {
//                                    HStack {
//                                        if comesFromChatScreen {
//                                            Image(systemName: "arrow.left")
//                                        }
//                                        Text("Find and Invite")
//                                    }})
//        
//    }
//}
