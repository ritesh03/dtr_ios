//
//  FlexibleView.swift
//  dtr-ios-test
//
//  Created by Mobile on 11/02/21.
//

import Foundation
import SwiftUI

class ContentViewModel: ObservableObject {
    
    @Published var originalItems = [DtrRideTags]()
    @Published var spacing: CGFloat = 8
    @Published var padding: CGFloat = 8
    @Published var wordCount: Int = 75
    @Published var alignmentIndex = 0
    @Published var showActivityIndicator = false
    
    init() {
        getTags()
    }
    
    let alignments: [HorizontalAlignment] = [.leading, .center, .trailing]
    
    var alignment: HorizontalAlignment {
        alignments[alignmentIndex]
    }
    
    func getTags() {
        showActivityIndicator = true
        var tagItems = [DtrRideTags]()
        let callData: DataDict = ["name": "tag.read","objectID":DtrData.sharedInstance.profileID]
        DtrCommand.sharedInstance.dtrCommandWrapper(callData: callData) { result in
            let dictUsr = asDataDict(result.output)
            if let defaultTags = dictUsr["defaultTags"] as? NSArray {
                for i in 0..<defaultTags.count {
                    tagItems.append(DtrRideTags(data: defaultTags[i] as! DataDict))
                }
            }
            if let customTags = dictUsr["customTags"] as? NSArray {
                for i in 0..<customTags.count {
                    tagItems.append(DtrRideTags(data: customTags[i] as! DataDict))
                }
            }
            self.originalItems = tagItems.sorted(by: { $0.sortOrder < $1.sortOrder})
            self.showActivityIndicator = false
        }
    }
}

struct FlexibleView<Data: Collection, Content: View>: View where Data.Element: Hashable {
    let data: Data
    let spacing: CGFloat
    let alignment: HorizontalAlignment
    let content: (Data.Element) -> Content
    @State private var availableWidth: CGFloat = 0
    
    var body: some View {
        ZStack(alignment: Alignment(horizontal: alignment, vertical: .center)) {
            Color.clear
                .frame(height: 1)
                .readSize { size in
                    availableWidth = size.width
                }
            
            _FlexibleView(
                availableWidth: availableWidth,
                data: data,
                spacing: spacing,
                alignment: alignment,
                content: content
            )
        }
    }
}

struct _FlexibleView<Data: Collection, Content: View>: View where Data.Element: Hashable {
    let availableWidth: CGFloat
    let data: Data
    let spacing: CGFloat
    let alignment: HorizontalAlignment
    let content: (Data.Element) -> Content
    @State var elementsSize: [Data.Element: CGSize] = [:]
    
    var body : some View {
        VStack(alignment: alignment, spacing: spacing) {
            ForEach(computeRows(), id: \.self) { rowElements in
                HStack(spacing: spacing) {
                    ForEach(rowElements, id: \.self) { element in
                        content(element)
                            .fixedSize()
                            .readSize { size in
                                elementsSize[element] = size
                            }
                    }
                }
            }
        }
    }
    
    func computeRows() -> [[Data.Element]] {
        var rows: [[Data.Element]] = [[]]
        var currentRow = 0
        var remainingWidth = availableWidth
        
        for element in data {
            let elementSize = elementsSize[element, default: CGSize(width: availableWidth, height: 1)]
            if remainingWidth - (elementSize.width + spacing) >= 0 {
                rows[currentRow].append(element)
            } else {
                currentRow = currentRow + 1
                rows.append([element])
                remainingWidth = availableWidth
            }
            remainingWidth = remainingWidth - (elementSize.width + spacing)
        }
        return rows
    }
}


extension View {
    func readSize(onChange: @escaping (CGSize) -> Void) -> some View {
        background(
            GeometryReader { geometryProxy in
                Color.clear
                    .preference(key: SizePreferenceKey.self, value: geometryProxy.size)
            }
        )
        .onPreferenceChange(SizePreferenceKey.self, perform: onChange)
    }
}

private struct SizePreferenceKey: PreferenceKey {
    static var defaultValue: CGSize = .zero
    static func reduce(value: inout CGSize, nextValue: () -> CGSize) {}
}
