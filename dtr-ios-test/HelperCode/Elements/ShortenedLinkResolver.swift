//
//  ShortenedLinkResolver.swift
//  dtr-ios-test
//
//  Created by Mobile on 26/05/21.
//

import Foundation

class ShortenedLinkResolver : NSObject, URLSessionDelegate, URLSessionDataDelegate {
    var url: URL
    var resolved: (String, Error?) -> Void = { resolved, error in }
    
    var session : URLSession!
    var tasks : [URLSessionDataTask : String] = [URLSessionDataTask : String]()
    
    init(_ url: URL, resolved: @escaping (String, Error?) -> Void = { resolved, error in }) {
        self.url = url
        self.resolved = resolved
    }
    
    func start() {
        self.session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)
        let request : URLRequest = URLRequest(url: self.url)
        let dataTask : URLSessionDataTask = session.dataTask(with: request)
        self.tasks[dataTask] = self.url.absoluteString
        dataTask.resume()
    }

    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        // if we get here, then the URL was not shortened...return the original URL (as a string) as the resolved
        resolved(self.url.absoluteString, nil)
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let error = error {
            print(error.localizedDescription)
        }
        
        if let urlString = self.tasks[task as! URLSessionDataTask] {
            print(urlString)
            resolved(urlString, error)
            return
        }

        resolved(self.url.absoluteString, error)
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        let headers = response.allHeaderFields
        let location = asString(headers[CommonAllString.LocationCapsStr], defaultValue: self.url.absoluteString)
        resolved(location, nil)
    }

    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(URLSession.AuthChallengeDisposition.performDefaultHandling, nil)
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(URLSession.AuthChallengeDisposition.performDefaultHandling, nil)
    }

    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        if let error = error {
            print(error.localizedDescription)
        }
        
        resolved(self.url.absoluteString, error)
    }
}

