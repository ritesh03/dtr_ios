//
//  CircleButtonStyle.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/26/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI

struct CircleButtonStyle: ButtonStyle {
    var active: Bool
    
    func makeBody(configuration: Self.Configuration) -> some View {
            configuration.label
                .frame(width: 45, height: 45, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .background(Circle().fill(active ? Color.accentColor : Color.clear))
                .foregroundColor(active ? Color.white : Color.accentColor)
        }
}

//Add by RS
struct CircleButtonStyleModifiy: ButtonStyle {
    
    let yellowColur = Color(UIColor(displayP3Red: 198/255, green: 251/255, blue:1/255, alpha: 1.0))
    let grayColour = Color(UIColor(displayP3Red: 43/255, green: 46/255, blue: 46/255, alpha: 1.0))
    let textColur = Color(UIColor(displayP3Red: 168/255, green: 171/255, blue: 172/255, alpha: 1.0))
    
    var active: Bool
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(width: 45, height: 45, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .background(Circle().fill(active ? yellowColur : grayColour))
            .foregroundColor(active ? Color.black : textColur)
    }
}
