//
//  ActionButtonStyle.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/26/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI

struct ActionButtonStyle: ButtonStyle {
    var bgColor: Color = Color.accentColor
    
    func makeBody(configuration: Self.Configuration) -> some View {
            configuration.label
                .padding()
                .background(RoundedRectangle(cornerRadius: 150, style: .continuous).fill(bgColor))
                .foregroundColor(Color.white)
                .animation(.spring())
        }
}


