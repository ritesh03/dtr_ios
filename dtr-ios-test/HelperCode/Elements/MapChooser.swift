//
//  File.swift
//  dtr-ios-test
//
//  Created by Mobile on 04/02/21.
//

import Foundation
import SwiftUI
import MapKit

typealias MapChooserCompletion = ((Bool, String, CLLocationCoordinate2D,String) -> Void)

struct MapChooser: UIViewRepresentable {
    @Binding var selectionCity: String
    @Binding var selectionCountry: String
    @Binding var selectionCoordinate: CLLocationCoordinate2D
    
    @Binding var mapType: MKMapType
    @Binding var mapReset: Bool
    var canSetLocation: Bool
    var action: MapChooserCompletion
    
    @State private var hasSelection: Bool
    @State private var city: String
    @State private var Country: String
    @State private var address: String
    @State private var coordinate: CLLocationCoordinate2D
    
    @State private var currentLocationCity = ""
    @State private var currentLocationCountry = ""
    @State private var currentLocationCoordinate = CLLocationCoordinate2D()
    
    
    
    init(selectionCity: Binding<String>,selectionCountry: Binding<String>, selectionCoordinate: Binding<CLLocationCoordinate2D>, mapType: Binding<MKMapType>, mapReset: Binding<Bool>, ride: DtrRide, canSetLocation: Bool = false, completion: @escaping MapChooserCompletion) {
        _selectionCity = selectionCity
        _selectionCountry = selectionCountry
        _selectionCoordinate = selectionCoordinate
        _mapType = mapType
        _mapReset = mapReset
        self.canSetLocation = canSetLocation
        self.action = completion
    
        let initialCoordinate = CLLocationCoordinate2D(latitude: ride.details.startPlaceLat, longitude: ride.details.startPlaceLon)
        
        _hasSelection = State(initialValue: ride.details.hasStartPlace)
        _city = State(initialValue: ride.details.startPlace)
        _Country = State(initialValue: ride.details.startPlace)
        _address = State(initialValue: "")
        _coordinate = State(initialValue: initialCoordinate)
    }
    
    func isChange(city: String, coordinate: CLLocationCoordinate2D) -> Bool {
        if city != self.city {
            return true
        }
        if coordinate.latitude != self.coordinate.latitude {
            return true
        }
        if coordinate.longitude != self.coordinate.longitude {
            return true
        }
        return false
    }
    
    func makeUIView(context: Context) -> MKMapView {
        let map = MKMapView(frame: .zero)
        map.mapType = self.mapType
        map.showsScale = true
        map.showsCompass = true
        map.userTrackingMode = .none
        map.showsUserLocation = true
        map.userLocation.title = ""
        
        if self.canSetLocation {
            let gesture = UILongPressGestureRecognizer(target: context.coordinator, action: #selector(Coordinator.gesture))
            gesture.minimumPressDuration = 0.5
            map.addGestureRecognizer(gesture)
        }
        
        if self.coordinate.latitude != 0 && self.coordinate.longitude != 0 {
            let annotation = MKPointAnnotation()
                annotation.coordinate = self.coordinate
                annotation.title = self.city
                
                let location = CLLocation(latitude: self.coordinate.latitude, longitude: self.coordinate.longitude)

                CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
                    guard error == nil else { return }
                    
                    let city = resolvedCityState(from: placemarks?.first)
                    annotation.title = city
                    
                    if let theAddress = resolvedAddress(from: placemarks?.first) {
                        annotation.subtitle = theAddress
                    }
                }
                
                map.addAnnotation(annotation)
                map.showAnnotations(map.annotations, animated: false)
                
                DispatchQueue.main.asyncAfter(wallDeadline: .now() + .seconds(1)) {
                    map.selectAnnotation(annotation, animated: true)
                }
        }
        return map
    }
    
    func updateUIView(_ view: MKMapView, context: Context) {
        let locationManager = CLLocationManager()
        view.mapType = self.mapType
        
        if self.mapReset {
            let coordinate = view.userLocation.coordinate
            let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: locationManager.location?.coordinate.latitude ?? 0.0, longitudinalMeters: locationManager.location?.coordinate.longitude ?? 0.0)
            view.setRegion(region, animated: false)
            view.removeAnnotations(view.annotations)
        }
        view.delegate = context.coordinator
        locationManager.delegate = context.coordinator
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    func makeCoordinator() -> MapChooser.Coordinator {
        return Coordinator(self)
    }
    
    class Coordinator: NSObject, MKMapViewDelegate, CLLocationManagerDelegate {
        var parent: MapChooser
        var annotation: MKPointAnnotation?
        var updated = false
        
        init(_ control: MapChooser) {
            self.parent = control
        }
        
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            // get current city here
            if let location = locations.first {
                CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
                    guard error == nil else { return }
                    self.parent.currentLocationCoordinate = location.coordinate
                    let city = resolvedCityState(from: placemarks?.first)
                    self.parent.currentLocationCity = city
                    
                    let country = resolvedCountry(from: placemarks?.first)
                    self.parent.currentLocationCountry = country

                    if self.parent.selectionCity == "" {
                        self.parent.selectionCity = city
                        self.parent.selectionCoordinate = location.coordinate
                    }
                }
            }
        }
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("Location Error: \(error.localizedDescription)")
        }
        
        func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
            if !updated {
                mapView.showAnnotations(mapView.annotations, animated: true)
                updated = true
            }
        }
        
        func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
            if !animated {
                DispatchQueue.main.async {
                    self.parent.mapReset = false
                }
            }
        }
        
        private func resolveValue(_ value: String??) -> String {
            if let hasValue = value {
                if let value = hasValue {
                    return value
                }
            }
            
            return ""
        }
        
        func mapView (_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            // use the default annotation for the user's current location
            if annotation is MKUserLocation { return nil }
            
            if let reusedView = mapView.dequeueReusableAnnotationView(withIdentifier: "startlocation") {
                return reusedView
            }
            
            // first time we are creating the annotationView (will be reused in future)
            let annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "startlocation")
            annotationView.animatesWhenAdded = true
            annotationView.canShowCallout = true
            
//            let callout = UIHostingController(rootView: MapAnnotationView(canSet: parent.canSetLocation, city: parent.$city, address: parent.$address, coordinate: parent.$coordinate, completion: parent.completion))
//
//            annotationView.detailCalloutAccessoryView = callout.view
            
            return annotationView
        }
        
        func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
            if view.annotation is MKUserLocation { return }
            guard let annotation = view.annotation else { return }
            
            parent.hasSelection = true
            
            let city = resolveValue(annotation.title)
            
            if parent.isChange(city: city, coordinate: annotation.coordinate) {
                parent.coordinate = annotation.coordinate
                parent.city = resolveValue(annotation.title)
                parent.address = resolveValue(annotation.subtitle)
                parent.selectionCity = parent.city
                parent.selectionCoordinate = parent.coordinate
                parent.selectionCountry = parent.Country
                parent.action(true, parent.city, parent.coordinate, parent.Country)
            }
        }
        
        func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
            if view.annotation is MKUserLocation { return }
            if parent.isChange(city: "", coordinate: CLLocationCoordinate2D(latitude: 0, longitude: 0)) {
                DispatchQueue.main.async {
                    self.parent.hasSelection = false
                    self.parent.coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
                    self.parent.city = "TBD"
                    self.parent.address = ""
                    self.parent.selectionCity = self.parent.currentLocationCity
                    self.parent.selectionCoordinate = self.parent.currentLocationCoordinate
                    self.parent.selectionCountry =  self.parent.currentLocationCountry
                    self.parent.action(false, self.parent.city, self.parent.coordinate,self.parent.Country)
                }
            }
        }
        
        @objc func gesture(recognizer: UIGestureRecognizer) {
            if let map = recognizer.view as? MKMapView {
                let point = recognizer.location(in: map)
                let coordinate = map.convert(point, toCoordinateFrom: map)
                
                switch recognizer.state {
                case UIGestureRecognizer.State.began:
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = coordinate
                    
                    map.addAnnotation(annotation)
                    self.annotation = annotation
                    self.updated = false
                    
                case .changed:
                    if let annotation = self.annotation {
                        annotation.coordinate = coordinate
                    }
                    
                case .ended:
                    if let annotation = self.annotation {
                        // resolve the location
                        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                        
                        DispatchQueue.main.asyncAfter(wallDeadline: .now() + .seconds(1)) {
                            map.selectAnnotation(annotation, animated: true)
                        }
                        
                        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
                            guard error == nil else { return }
                            
                            let city = resolvedCityState(from: placemarks?.first)
                            annotation.title = city
                            
                            let country = resolvedCountry(from: placemarks?.first)
                           
                                                        
                            if let theAddress = resolvedAddress(from: placemarks?.first) {
                                annotation.subtitle = theAddress
                            }
                            
                            self.parent.city = city
                            self.parent.Country = country
                            self.parent.coordinate = location.coordinate
                            self.parent.selectionCity = city
                            self.parent.selectionCountry  = country
                            self.parent.selectionCoordinate = location.coordinate
                            self.parent.action(true, self.parent.city, self.parent.coordinate, self.parent.Country)
                        }
                    }
                    
                default:
                    _ = "nothing"
                }
            }
        }
    }
}

struct MapChooser_Previews: PreviewProvider {
    static var previews: some View {
        MapChooser(selectionCity: .constant("here"), selectionCountry: .constant("here"),  selectionCoordinate: .constant(CLLocationCoordinate2D()),mapType: .constant(.standard), mapReset: .constant(false), ride: DtrRide.default) { _,_,_,_  in }
    }
}

//extension MKMapView {
//
//    /// When we call this function, we have already added the annotations to the map, and just want all of them to be displayed.
//    func fitAll() {
//        var zoomRect            = MKMapRect.null;
//        for annotation in annotations {
//            let annotationPoint = MKMapPoint(annotation.coordinate)
//            let pointRect       = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0.01, height: 0.01);
//            zoomRect            = zoomRect.union(pointRect);
//        }
//        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
//    }
//
//    /// We call this function and give it the annotations we want added to the map. we display the annotations if necessary
//    func fitAll(in annotations: [MKAnnotation], andShow show: Bool) {
//        var zoomRect:MKMapRect  = MKMapRect.null
//
//        for annotation in annotations {
//            let aPoint          = MKMapPoint(annotation.coordinate)
//            let rect            = MKMapRect(x: aPoint.x, y: aPoint.y, width: 0.1, height: 0.1)
//
//            if zoomRect.isNull {
//                zoomRect = rect
//            } else {
//                zoomRect = zoomRect.union(rect)
//            }
//        }
//        if(show) {
//            addAnnotations(annotations)
//        }
//        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
//    }
//
//}


func resolvedAddress(from placemark: CLPlacemark?) -> String? {
    if let placemark = placemark {
        if let street = placemark.thoroughfare {
            if let number = placemark.subThoroughfare {
                return "\(number) \(street)"
            }
            return "\(street)"
        }
    }
    return ""
}

func resolvedCityState(from placemark: CLPlacemark?) -> String {
    if let placemark = placemark {
        if let state = placemark.administrativeArea {
            if let city = placemark.locality {
                return "\(city), \(state)"
            }
            return "\(state)"
        }
    }
    return ""
}

func resolvedCountry(from placemark: CLPlacemark?) -> String {
    if let placemark = placemark {
        if let country = placemark.country {
            return "\(country)"
        }
    }
    return ""
}
