//
//  CustomTextFieldView.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/02/21.
//

import Foundation
import SwiftUI

struct MultilineTextField: View {

    private var placeholder: String
    private var onCommit: (() -> Void)?
    @State private var viewHeight: CGFloat = 30
    @State  var shouldShowPlaceholder = false
    @Binding private var text: String
    
    private var internalText: Binding<String> {
        Binding<String>(get: { self.text } ) {
            self.text = $0
            self.shouldShowPlaceholder = $0.isEmpty
        }
    }

    var body: some View {
        UITextViewWrapper(text: self.internalText, calculatedHeight: $viewHeight, onDone: onCommit)
            .frame(minHeight: viewHeight, maxHeight: viewHeight)
            .background(placeholderView, alignment: .topLeading)
    }

    var placeholderView: some View {
        Group {
            if shouldShowPlaceholder {
                Text(placeholder).foregroundColor(.gray)
                    .padding(.leading, 4)
                    .padding(.top, 8)
            }
        }
    }
    
    init (_ placeholder: String = "", text: Binding<String>, onCommit: (() -> Void)? = nil) {
        self.placeholder = placeholder
        self.onCommit = onCommit
        self._text = text
        self._shouldShowPlaceholder = State<Bool>(initialValue: self.text.isEmpty)
    }
}


private struct UITextViewWrapper: UIViewRepresentable {
    typealias UIViewType = UITextView

    @Binding var text: String
    @Binding var calculatedHeight: CGFloat
    var onDone: (() -> Void)?

    func makeUIView(context: UIViewRepresentableContext<UITextViewWrapper>) -> UITextView {
        let textField = UITextView()
        textField.delegate = context.coordinator
        textField.isEditable = true
        textField.font = UIFont.preferredFont(forTextStyle: .body)
        textField.isSelectable = true
        textField.isUserInteractionEnabled = true
        textField.isScrollEnabled = false
        textField.textColor = .white
        textField.backgroundColor = UIColor.clear
        if nil != onDone {
            textField.returnKeyType = .done
        }
        textField.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return textField
    }

    func updateUIView(_ uiView: UITextView, context: UIViewRepresentableContext<UITextViewWrapper>) {
        if uiView.text != self.text {
            uiView.text = self.text
        }
        if uiView.window != nil, !uiView.isFirstResponder {
            //uiView.becomeFirstResponder()
        }
        UITextViewWrapper.recalculateHeight(view: uiView, result: $calculatedHeight)
    }

    private static func recalculateHeight(view: UIView, result: Binding<CGFloat>) {
        let newSize = view.sizeThatFits(CGSize(width: view.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        if result.wrappedValue != newSize.height {
            DispatchQueue.main.async {
                result.wrappedValue = newSize.height // call in next render cycle.
            }
        }
    }

    func makeCoordinator() -> Coordinator {
        return Coordinator(text: $text, height: $calculatedHeight, onDone: onDone)
    }

    final class Coordinator: NSObject, UITextViewDelegate {
        var text: Binding<String>
        var calculatedHeight: Binding<CGFloat>
        var onDone: (() -> Void)?

        init(text: Binding<String>, height: Binding<CGFloat>, onDone: (() -> Void)? = nil) {
            self.text = text
            self.calculatedHeight = height
            self.onDone = onDone
        }

        func textViewDidChange(_ uiView: UITextView) {
            text.wrappedValue = uiView.text
            UITextViewWrapper.recalculateHeight(view: uiView, result: calculatedHeight)
        }

        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if let onDone = self.onDone, text == "\n" {
                textView.resignFirstResponder()
                onDone()
                return false
            }
            return true
        }
    }
}

struct CustomTxtfldForFollowerScrn: UIViewRepresentable {
    
    @Binding var isDeleteAcntScreen: Bool
    @Binding var isGroupListScreen: Bool
    @Binding var text: String
    @Binding var clearAllText: Bool
    @Binding var isFirstResponder: Bool
    var completion: (String) -> Void
    
    func makeUIView(context: UIViewRepresentableContext<CustomTxtfldForFollowerScrn>) -> UITextField {
        let textField = UITextField(frame: .zero)
        textField.text = text
        textField.delegate = context.coordinator
        textField.backgroundColor = .clear
        var placeholderText = ""
        if isDeleteAcntScreen {
            placeholderText = "DELETE"
        }else if isGroupListScreen {
            placeholderText = "Group Name or Group Code"
        } else {
            placeholderText = "Username"
        }
        textField.attributedPlaceholder = NSAttributedString(
            string: placeholderText,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray]
        )

        textField.returnKeyType = .default
        textField.textColor = .white
        return textField
    }
    
    func makeCoordinator() -> CustomTxtfldForFollowerScrn.Coordinator {
        return Coordinator(self)
    }
    
    func updateUIView(_ uiView: UITextField, context: UIViewRepresentableContext<CustomTxtfldForFollowerScrn>) {
        uiView.text = text
        if isFirstResponder && !context.coordinator.didBecomeFirstResponder  {
            uiView.becomeFirstResponder()
            context.coordinator.didBecomeFirstResponder = true
        }else if clearAllText {
            DispatchQueue.main.async {
                uiView.text! = ""
                text = ""
                clearAllText = false
            }
        }else if !isFirstResponder {
            DispatchQueue.main.async {
                UIApplication.shared.endEditing()
                context.coordinator.didBecomeFirstResponder = false
            }
        }
    }
    
    class Coordinator: NSObject, UITextFieldDelegate {
        var didBecomeFirstResponder = false
        var parent: CustomTxtfldForFollowerScrn
        
        init(_ view: CustomTxtfldForFollowerScrn) {
            self.parent = view
        }
        
        func textFieldDidChangeSelection(_ textField: UITextField) {
            DispatchQueue.main.async {
                self.parent.text = textField.text ?? ""
                self.parent.completion(textField.text!)
            }
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
    }
}

extension Collection where Indices.Iterator.Element == Index {
    subscript (exist index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

struct TextWithAttributedString: UIViewRepresentable {
    
    @Binding var heightText :CGFloat
    var attributedString: NSAttributedString
    var fixedWidth: CGFloat
    
    let textView = UITextView(frame: .zero)
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(self)
    }
    
    
    class Coordinator: NSObject, UITextViewDelegate {
        var parent: TextWithAttributedString
        init(_ parent: TextWithAttributedString) {
            self.parent = parent
        }
    }
    
    func makeUIView(context: Context) -> UITextView {
        textView.textColor = .white
        textView.isEditable = false
        textView.dataDetectorTypes = .all
        textView.autocapitalizationType = .sentences
        textView.isSelectable = true
        textView.isUserInteractionEnabled = true
        textView.dataDetectorTypes = .all
        textView.isScrollEnabled = false
        textView.backgroundColor = .clear
        textView.delegate = context.coordinator
        textView.showsHorizontalScrollIndicator = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.widthAnchor.constraint(equalToConstant: fixedWidth).isActive = true
        return textView
    }
    
    func updateUIView(_ textView: UITextView, context: Context) {
        DispatchQueue.main.async {
            textView.attributedText = self.attributedString
            textView.attributedText = NSMutableAttributedString(string: attributedString.string, attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 17.0)])
            UITextView.appearance().linkTextAttributes = [ .foregroundColor: UIColor(displayP3Red: 25/255, green: 187/255, blue: 217/255, alpha: 1.0) ]
            textView.textColor = .white
            heightText = textView.frame.size.height
        }
    }
}


struct CustomTxtfldForVerificationCode: UIViewRepresentable {
    
    class Coordinator: NSObject, UITextFieldDelegate {
        @Binding var text: String
        var didBecomeFirstResponder = false
        
        init(text: Binding<String>) {
            _text = text
        }
        
        func textFieldDidChangeSelection(_ textField: UITextField) {
            text = textField.text ?? ""
            
        }
    }
    
    
    @Binding var text: String
    var isFirstResponder: Bool = false
    
    func makeUIView(context: UIViewRepresentableContext<CustomTxtfldForVerificationCode>) -> UITextField {
        let textField = UITextField(frame: .zero)
        textField.delegate = context.coordinator
        textField.keyboardType = .numberPad
        textField.textColor = .clear
        textField.textContentType = .oneTimeCode
        textField.tintColor = .clear
        return textField
    }
    
    func makeCoordinator() -> CustomTxtfldForVerificationCode.Coordinator {
        return Coordinator(text: $text)
    }
    
    func updateUIView(_ uiView: UITextField, context: UIViewRepresentableContext<CustomTxtfldForVerificationCode>) {
        uiView.text = text
        if isFirstResponder && !context.coordinator.didBecomeFirstResponder  {
            DispatchQueue.main.async {
                uiView.becomeFirstResponder()
                context.coordinator.didBecomeFirstResponder = true
            }
        }
    }
}


struct CustomTextFieldForSignup: UIViewRepresentable {
    @Binding var becomeFirstResponder: Bool
    @Binding var phoneNumber: String
    @Binding var clearAllText: Bool
    @Binding var showOnlyNumberPad: Bool
    var tag: Int = 0
    var isShownbackgroundColor: Bool = false
    var completion: (String) -> Void
    
    func makeUIView(context: Context) -> UITextField {
        var placeholderText: String = ""
        let textField = UITextField()
        textField.tag = self.tag
        textField.text = phoneNumber
        textField.delegate = context.coordinator
        if isShownbackgroundColor {
            textField.backgroundColor = UIColor(Color.init(ColorConstantsName.HeaderBgColour))
            placeholderText = "Phone Number"
            textField.returnKeyType = .go
            textField.borderStyle = .roundedRect
        }else{
            textField.backgroundColor = .clear
            placeholderText = "Enter Username"
            textField.returnKeyType = .default
        }
        textField.textColor = .white
        textField.attributedPlaceholder = NSAttributedString(
            string: placeholderText,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray]
        )

        if showOnlyNumberPad {
            textField.keyboardType = .numberPad
        }
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: textField.frame.size.width, height: 44))
        let flexButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(textField.doneButtonClicked(button:)))
        toolBar.items = [flexButton,doneButton]
        toolBar.setItems([flexButton,doneButton], animated: true)
        textField.inputAccessoryView = toolBar
        return textField
    }
    
//    func updateUIView(_ uiView: UITextField, context: Context) {
//            uiView.text = text
//
//        }
    
    func updateUIView(_ uiView: UITextField, context: Context) {
        if self.becomeFirstResponder {
            DispatchQueue.main.async {
                uiView.becomeFirstResponder()
                self.phoneNumber = uiView.text!
                self.becomeFirstResponder = false
            }
        }else{
            if clearAllText {
                uiView.text! = ""
                DispatchQueue.main.async {
                    clearAllText = false
                }
            }
        }
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(self)
    }
    
    class Coordinator: NSObject, UITextFieldDelegate {
        var parent: CustomTextFieldForSignup
        
        init(_ view: CustomTextFieldForSignup) {
            self.parent = view
        }
        
        func textFieldDidChangeSelection(_ textField: UITextField) {
            DispatchQueue.main.async {
                self.parent.phoneNumber = textField.text ?? ""
                if !self.parent.isShownbackgroundColor {
                    self.parent.completion(textField.text!)
                }
            }
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if string == numberFiltered {
                let maxLength = 10
                let currentString: NSString = (textField.text ?? "") as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
            return false
        }
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            textField.resignFirstResponder()
            parent.becomeFirstResponder = false
            if parent.isShownbackgroundColor {
                parent.completion(textField.text!)
            }
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
    }
}

struct WrappedTextFieldForLocation: UIViewRepresentable {
    @Binding var text: String
    @Binding var placeholderText: String

    func makeUIView(context: Context) -> UITextField {
        let textField = UITextField()
        textField.delegate = context.coordinator
        textField.placeholder = placeholderText
        textField.textColor = .white
        return textField
    }

    func updateUIView(_ uiView: UITextField, context: Context) {
        uiView.text = text
    }

    func makeCoordinator() -> Coordinator {
        return Coordinator(text: $text)
    }

    class Coordinator: NSObject, UITextFieldDelegate {
        @Binding var text: String

        init(text: Binding<String>) {
            self._text = text
        }

        func textFieldDidChangeSelection(_ textField: UITextField) {
            DispatchQueue.main.async {
                self.text = textField.text ?? ""
            }
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
        }
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
                textField.resignFirstResponder()
            }
            return true
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let maxLength = 0
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
    }
}
extension  UITextField{
    @objc func doneButtonClicked(button:UIBarButtonItem) -> Void {
       self.resignFirstResponder()
    }

}
