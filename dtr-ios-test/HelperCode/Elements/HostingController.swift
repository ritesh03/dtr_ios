//
//  HostingController.swift
//  dtr-ios
//
//  Created by Dan Kinney on 3/4/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI

final class HostingController<T: View>: UIHostingController<T> {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
}
