//
//  MailView.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/03/21.
//

import UIKit
import SwiftUI
import MessageUI
import Firebase

struct MailView: UIViewControllerRepresentable {
    @Environment(\.presentationMode) var presentation
    @Binding var result: Result<MFMailComposeResult, Error>?
    var recipients: [String]
    var subject: String
    var body: String
    var isHTML: Bool
    var isRideFeedback: Bool
    var rideFeedbackData: DtrRide
    var comesFromProfileScreen = false
    
    @EnvironmentObject var session: DtrData

    class Coordinator: NSObject, MFMailComposeViewControllerDelegate {
        @Binding var presentation: PresentationMode
        @Binding var result: Result<MFMailComposeResult, Error>?

        init(presentation: Binding<PresentationMode>,
             result: Binding<Result<MFMailComposeResult, Error>?>) {
            _presentation = presentation
            _result = result
        }

        func mailComposeController(_ controller: MFMailComposeViewController,
                                   didFinishWith result: MFMailComposeResult,
                                   error: Error?) {
            defer {
                $presentation.wrappedValue.dismiss()
            }
            
            var resultString = ""
            
            switch result {
                case .cancelled : resultString = "cancelled"
                case .saved: resultString = "saved"
                case .sent: resultString = "sent"
                case .failed: resultString = "failed"
                default: resultString = "unknown"
            }
            Analytics.logEvent(AnalyticsEvent.mailSent.rawValue, parameters: ["result": resultString])
            guard error == nil else {
                self.result = .failure(error!)
                return
            }
            self.result = .success(result)
        }
    }

    func makeCoordinator() -> Coordinator {
        return Coordinator(presentation: presentation, result: $result)
    }

    func makeUIViewController(context: UIViewControllerRepresentableContext<MailView>) -> MFMailComposeViewController {
        let vc = MFMailComposeViewController()
        vc.mailComposeDelegate = context.coordinator
        
        vc.setToRecipients(self.recipients)
        vc.setSubject(self.subject)
        
        let info = appInfo().simpleFormatted()
        //let rideFeedback = rideFeedbackData.data.simpleFormatted()
    
        if self.isHTML {
            if isRideFeedback {
                //vc.setMessageBody("\(self.body)<p>&nbsp</p><p>Ride Details:<p>&nbsp</p><p></p><p>\(rideFeedback)</p>profile: \(session.profile.id)</p><p>\(info)</p>", isHTML: self.isHTML)
                vc.setMessageBody("\(self.body)", isHTML: self.isHTML)
            }else{
                vc.setMessageBody("\(self.body)<p>&nbsp</p><p>&nbsp</p><p>profile: \(session.profile.id)</p><p>\(info)</p>", isHTML: self.isHTML)
            }
        } else {
            if !comesFromProfileScreen {
                vc.setMessageBody("\(self.body)\n\n\n\nprofile: \(session.profile.id)\n\n\(info)", isHTML: self.isHTML)
            }
        }
        return vc
    }

    func updateUIViewController(_ uiViewController: MFMailComposeViewController,
                                context: UIViewControllerRepresentableContext<MailView>){
        // NOTE: Handled in the screen that display this
        // Analytics.setScreenName(AnalyticsScreen.composeEmail.rawValue, screenClass: AnalyticsScreen.composeEmail.rawValue)
    }
}
