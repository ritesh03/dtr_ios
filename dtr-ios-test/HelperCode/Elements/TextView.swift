//
//  TextView.swift
//  dtr-ios-test
//
//  Created by Mobile on 12/02/21.
//

import SwiftUI

struct TextView: UIViewRepresentable {
    @Binding var comesFromDetailAndEditScreen:Bool
    @Binding var isEditable:Bool
    @Binding var text: String
    @Binding var textStyle: UIFont.TextStyle
    @Binding var comesFromEditScreen: Bool
    @Binding var didStartEditing: Bool
    var placeHolderText: String = ""
    
    
    func makeUIView(context: Context) -> UITextView {
        let textView = UITextView()
        textView.delegate = context.coordinator
        textView.font = UIFont.preferredFont(forTextStyle: textStyle)
        textView.autocapitalizationType = .sentences
        textView.isSelectable = true
        textView.isUserInteractionEnabled = true
        textView.isScrollEnabled = true
        textView.dataDetectorTypes = .all
        if isEditable {
            textView.isEditable = true
            textView.backgroundColor = UIColor(Color.init(ColorConstantsName.HeaderBgColour))
        }else{
            textView.layer.cornerRadius = 15.0
            textView.isEditable = false
            textView.backgroundColor = UIColor(Color.init(ColorConstantsName.HeaderBgColour)).withAlphaComponent(0.7)
            if comesFromDetailAndEditScreen {
                textView.layer.borderColor = UIColor.white.cgColor
                textView.layer.borderWidth = 0.5
            }
        }
        textView.textColor = .white
        return textView
    }
    
    func updateUIView(_ uiView: UITextView, context: Context) {
        let selectedRange = uiView.selectedRange
        if comesFromEditScreen {
            if didStartEditing {
                uiView.text = text
                uiView.font = .preferredFont(forTextStyle: textStyle)
                uiView.selectedRange = selectedRange
                uiView.textColor = UIColor.white
            } else {
                if text != "" {
                    uiView.text = text
                    uiView.textColor = UIColor.white
                    uiView.font = .preferredFont(forTextStyle: textStyle)
                    uiView.selectedRange = selectedRange
                }else{
                    uiView.text = placeHolderText
                    uiView.textColor = UIColor.lightGray
                }
            }
        }else{
            uiView.text = text
            uiView.font = .preferredFont(forTextStyle: textStyle)
            uiView.selectedRange = selectedRange
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator($text)
    }
    
    class Coordinator: NSObject, UITextViewDelegate {
        var text: Binding<String>
        
        init(_ text: Binding<String>) {
            self.text = text
        }
        
        func textViewDidChange(_ textView: UITextView) {
            DispatchQueue.main.async {
                let selectedRange = textView.selectedRange
                textView.selectedRange = selectedRange
                self.text.wrappedValue = textView.text
            }
        }
    }
}
