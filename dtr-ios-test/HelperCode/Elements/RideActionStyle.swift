//
//  RideActionStyle.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 1/2/21.
//

import SwiftUI

struct RideActionStyle: ButtonStyle {
    var rideAction: DtrRideFromActionType
    
    var fgColor: Color {
        switch self.rideAction {
            case .none: return Color.green
            case .cancel, .leave: return Color.red
        }
    }
    
    func makeBody(configuration: Self.Configuration) -> some View {
            configuration.label
                .padding(4)
                .foregroundColor(self.fgColor)
        }
}
