//
//  SkillToggleModifier.swift
//  dtr-ios
//
//  Created by Dan Kinney on 10/28/19.
//  Copyright © 2019 Social Active. All rights reserved.
//

import SwiftUI

struct SkillStateModifier: ViewModifier {
    var on: Bool
    var onColor: Color = Color.accentColor
    
    func body(content: Content) -> some View {
        return content.foregroundColor(self.on ? onColor : Color.primary)
    }
}

struct SkillToggleModifier: ViewModifier {
    @Binding var on: Bool
    var onColor: Color = Color.accentColor
    
    func body(content: Content) -> some View {
        return content.foregroundColor(self.on ? onColor : Color.primary)
    }
}
