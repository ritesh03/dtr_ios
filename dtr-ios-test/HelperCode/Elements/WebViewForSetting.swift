//
//  WebViewForSetting.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/03/21.
//

import SwiftUI
import WebKit
import Combine
import Foundation

struct WebViewForSetting : UIViewRepresentable {
    var filename: String
    
    func makeUIView(context: Context) -> WKWebView  {
        return WKWebView()
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        if let path = Bundle.main.path(forResource: self.filename, ofType: "html") {
            let url = URL(fileURLWithPath: path)
            uiView.loadFileURL(url, allowingReadAccessTo: url)
        }
    }
}

struct ActivityIndicator: UIViewRepresentable {
    
    @Binding var isAnimating: Bool
    let style: UIActivityIndicatorView.Style
    
    func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
    }
}

struct ActivityIndicatorView<Content>: View where Content: View {
    @Binding var isShowing: Bool
    var content: () -> Content
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                self.content()
                    .disabled(self.isShowing)
                    .blur(radius: self.isShowing ? 3 : 0)
                VStack {
                    ActivityIndicator(isAnimating: .constant(true), style: .medium)
                }
                .frame(width: 60, height: 50)
                .cornerRadius(25)
                .opacity(self.isShowing ? 1 : 0)
            }
        }
    }
}
