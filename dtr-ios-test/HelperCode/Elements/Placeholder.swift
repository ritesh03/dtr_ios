//
//  Placeholder.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/24/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI

struct Placeholder: View {
    var body: some View {
        Text("Placeholder")
        .navigationBarTitle("Placeholder")
    }
}

struct Placeholder_Previews: PreviewProvider {
    static var previews: some View {
        Placeholder()
    }
}
