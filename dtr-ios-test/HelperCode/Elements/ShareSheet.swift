//
//  ShareSheet.swift
//  dtr-ios-test
//
//  Created by Mobile on 15/03/21.
//


import SwiftUI
import LinkPresentation

struct ShareSheet: UIViewControllerRepresentable {
    typealias Callback = (_ activityType: UIActivity.ActivityType?, _ completed: Bool, _ returnedItems: [Any]?, _ error: Error?) -> Void
      
    let activityItems: [Any]
    var callback: Callback
    
    let applicationActivities: [UIActivity]? = nil
    let excludedActivityTypes: [UIActivity.ActivityType]? = [.addToReadingList]
    
//    func makeUIViewController(context: Context) -> UIActivityViewController {
//        let controller = UIActivityViewController(
//            activityItems: activityItems,
//            applicationActivities: applicationActivities)
//        controller.excludedActivityTypes = excludedActivityTypes
//        controller.completionWithItemsHandler = callback
//        return controller
//    }
        
    func makeUIViewController(context: Context) -> UIActivityViewController {
            let controller = UIActivityViewController(
                activityItems: activityItems,
                applicationActivities: applicationActivities)
            controller.excludedActivityTypes = excludedActivityTypes
            controller.completionWithItemsHandler = callback
            controller.navigationController?.navigationBar.tintColor = .lightGray
            controller.view.tintColor = .lightGray
            return controller
        }
      
    func updateUIViewController(_ uiViewController: UIActivityViewController, context: Context) {
        // nothing to do here
    }
    
    init(activityItems: [Any], callback: @escaping Callback = { activityType, completed, returnedItems, error in }) {
        self.activityItems = activityItems
        self.callback = callback
        UITableView.appearance().backgroundColor = UIColor.systemGroupedBackground
        UITableViewCell.appearance().backgroundColor = UIColor.systemGroupedBackground
    }
}

enum LinkType: String {
    case unknown, ride, friend, code, group
    
    var label: String {
        switch self {
        case .ride: return "Ride Invitation"
        case .friend: return "Friend Invitation"
        case .code: return "Promo Code"
        case .group: return "Group Invitation"
        default: return "Link"
        }
    }
}

class ShareActivityItemSource: NSObject, UIActivityItemSource {
    var linkType = LinkType.unknown
    var linkMetaData = LPLinkMetadata()
    var imageURl = ""
    
    init(type: LinkType, message: String, url: URL,imageURl:String) {
        linkType = type
        linkMetaData.title = message
        linkMetaData.url = url
        linkMetaData.originalURL = url
        linkMetaData.iconProvider =  NSItemProvider(object: UIImage(named: "DTR-ShareIcon")!)
//        linkMetaData.imageProvider = NSItemProvider.init(contentsOf: URL(string: imageURl))
        super.init()
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return UIImage(named: "DTR-ShareIcon") as Any
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return linkType.label
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        if let type = activityType {
            switch type {
                case .message:
                    return linkMetaData.url
                case .mail:
                    return "<html><body><p>\(linkMetaData.title!)</p></p><p><a href=\"\(linkMetaData.url!)\"</a>\(linkMetaData.url!)</p>"
                default: break                    
            }
        }
        return nil
    }
    
    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        return linkMetaData
    }
}


