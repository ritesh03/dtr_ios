//
//  Handle.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 12/31/20.
//

import SwiftUI

struct Handle: View {
    private let handleThickness = CGFloat(5.0)
    
    var body: some View {
        RoundedRectangle(cornerRadius: handleThickness / 2.0)
            .frame(width: 40, height: handleThickness)
            .padding(5)
    }
}

struct Handle_Previews: PreviewProvider {
    static var previews: some View {
        Handle()
    }
}
