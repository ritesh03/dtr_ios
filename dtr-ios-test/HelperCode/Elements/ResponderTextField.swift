//
//  ResponderTextField.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/26/20.
//

import SwiftUI
import Combine

struct ResponderTextField: View {
    var placeholder: String
    @Binding var text: String
    @Binding var isFirstResponder: Bool
    var keyboardType: UIKeyboardType
    var showDone: Bool
    
    private var textFieldDelegate: TextFieldDelegate

    init(_ placeholder: String, text: Binding<String>, isFirstResponder: Binding<Bool>, keyboardType: UIKeyboardType = .default, showDone: Bool = false) {
        self.placeholder = placeholder
        self._text = text
        self._isFirstResponder = isFirstResponder
        self.keyboardType = keyboardType
        self.showDone = showDone
        self.textFieldDelegate = .init(text: text)
    }

    var body: some View {
        ResponderView<UITextField>(isFirstResponder: $isFirstResponder) {
            $0.text = self.text
            $0.placeholder = self.placeholder
            $0.delegate = self.textFieldDelegate
            $0.keyboardType = keyboardType
            
            if showDone {
                let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: $0.frame.size.width, height: 44))
                let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
                let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector($0.doneButtonTapped(button:)))
                toolBar.items = [flexibleSpace, doneButton]
                $0.inputAccessoryView = toolBar
            }
        }
    }
}

extension  UITextField{
    @objc func doneButtonTapped(button:UIBarButtonItem) -> Void {
       self.resignFirstResponder()
    }
}

// MARK: - TextFieldDelegate
private extension ResponderTextField {
    final class TextFieldDelegate: NSObject, UITextFieldDelegate {
        @Binding private(set) var text: String

        init(text: Binding<String>) {
            _text = text
        }

        func textFieldDidChangeSelection(_ textField: UITextField) {
            text = textField.text ?? ""
        }
    }
}

struct ResponderTextField_Previews: PreviewProvider {
    static var previews: some View {
        ResponderTextField("Placeholder",
                           text: .constant(""),
                           isFirstResponder: .constant(false))
            .previewLayout(.fixed(width: 300, height: 40))
    }
}
