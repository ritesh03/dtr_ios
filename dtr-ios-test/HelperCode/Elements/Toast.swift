//
//  Toast.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/16/19.
//  Copyright © 2019 Social Active. All rights reserved.
//

import SwiftUI

struct Toast<Presenting>: View where Presenting: View {
    // The binding that decides the appropriate drawing in the body.
    @Binding var isShowing: Bool
    
    // The view that will be "presenting" this toast
    let presenting: () -> Presenting
    
    // The text to show
    let text: Text

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .bottom) {
                self.presenting()
                     // .blur(radius: self.isShowing ? 1 : 0)

                self.text
                .font(.caption)
                .padding()
                .background(Color.secondary.colorInvert())
                .foregroundColor(Color.primary)
                .cornerRadius(8)
                .transition(.slide)
                .opacity(self.isShowing ? 1 : 0)
                .position(CGPoint(x: geometry.size.width / 2, y: geometry.size.height / 2))
            }
        }
    }
}

extension View {
    func toast(isShowing: Binding<Bool>, text: Text) -> some View {
        Toast(isShowing: isShowing, presenting: { self }, text: text)
    }
}
