//
//  CustomTabViewHighlight.swift
//  dtr-ios-test
//
//  Created by Mobile on 24/05/21.
//

import SwiftUI

struct Tab {
    var title: String
}

struct CustomTabViewHighlight: View {
    var fixed = true
    var tabs: [Tab] = [.init(title: "Following"),.init(title: "Followers"),.init(title: "Friends")]
    var geoWidth: CGFloat
    @Binding var selectedTab: Int

    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            ScrollViewReader { proxy in
                VStack(spacing: 0) {
                    HStack(spacing: 0) {
                        ForEach(0 ..< tabs.count, id: \.self) { row in
                            Button(action: {
                                withAnimation {
                                    selectedTab = row
                                }
                            }, label: {
                                VStack(spacing: 0) {
                                    HStack {
                                        Text(tabs[row].title)
                                            .font(Font.system(size: 18, weight: .semibold))
                                            .foregroundColor(selectedTab == row ? Color.init(ColorConstantsName.LimeColour) : Color.init(UIColor.init(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.32)))
                                            .padding(EdgeInsets(top: 10, leading: 3, bottom: 10, trailing: 15))
                                    }
                                    .frame(width: fixed ? (geoWidth / CGFloat(tabs.count)) : .none, height: 52)
                                    Rectangle().fill(selectedTab == row ? Color.init(ColorConstantsName.LimeColour) : Color.clear)
                                        .frame(height: 3)
                                }.fixedSize()
                            })
                                .accentColor(Color.white)
                                .buttonStyle(PlainButtonStyle())
                        }
                    }
                    .onChange(of: selectedTab) { target in
                        withAnimation {
                            proxy.scrollTo(target)
                        }
                    }
                }
            }
        }
        .frame(height: 55)
        
        .onAppear(perform: {
            UIScrollView.appearance().backgroundColor = UIColor(Color.init(ColorConstantsName.MainThemeBgColour))
            UIScrollView.appearance().bounces = fixed ? false : true
        })
        
        .onDisappear(perform: {
            UIScrollView.appearance().bounces = true
            UIScrollView.appearance().backgroundColor = .clear
        })
    }
}
