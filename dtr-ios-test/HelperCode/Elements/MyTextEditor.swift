//
//  MyTextEditor.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/26/19.
//  Copyright © 2019 Social Active. All rights reserved.
//

import SwiftUI
import Combine

struct MyTextEditor: UIViewRepresentable {
    @Binding var text: String
    var isEditable: Bool = true
    @Binding var wasChanged: Bool
    var completion: (String) -> Void = { value in }
    var showDone: Bool = true
    var gainFocus: Bool = false
    
    private var originalValue: String = ""
    
    init(text: Binding<String>, isEditable: Bool, gainFocus: Bool, wasChanged: Binding<Bool>, completion: @escaping (String)->Void = { value in } ) {
        _text = text
        _wasChanged = wasChanged
        self.isEditable = isEditable
        self.gainFocus = gainFocus
        self.completion = completion
        
        self.originalValue = text.wrappedValue
    }
    
    func makeUIView(context: Context) -> UITextView {
        let view = UITextView()
        view.isScrollEnabled = true
        view.isEditable = self.isEditable
        view.isUserInteractionEnabled = true
        view.font = UIFont.systemFont(ofSize: 18)
        view.dataDetectorTypes = .all
        view.text = self.text
        view.delegate = context.coordinator
        
        if isEditable && showDone {
            let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 44))
            let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
            let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(view.doneButtonTapped(button:)))
            toolBar.items = [flexibleSpace, doneButton]
            view.inputAccessoryView = toolBar
        }
        
        if isEditable && gainFocus {
            view.becomeFirstResponder()
        }
        
        return view
    }
    
    func updateUIView(_ view: UITextView, context: UIViewRepresentableContext<MyTextEditor>) {
        view.text = self.text
    }
    
    func makeCoordinator() -> MyTextEditor.Coordinator {
        Coordinator(self, text: $text, wasChanged: $wasChanged)
    }
    
    class Coordinator: NSObject, UITextViewDelegate {
        var parent: MyTextEditor
        @Binding var text: String
        @Binding var wasChanged: Bool
        
        init(_ control: MyTextEditor, text: Binding<String>, wasChanged: Binding<Bool>) {
            self.parent = control
            _text = text
            _wasChanged = wasChanged
        }
        
        func textViewDidChange(_ textView: UITextView) {
            text = textView.text
            wasChanged = (text != self.parent.originalValue)
        }
        
        func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
            self.parent.completion(textView.text)
            return true
        }
    }
}

class ObservableText: ObservableObject {
    @Published var value: String
    
    init(initialValue: String) {
        self.value = initialValue
    }
}

extension  UITextView{
    @objc func doneButtonTapped(button:UIBarButtonItem) -> Void {
       self.resignFirstResponder()
    }
}
