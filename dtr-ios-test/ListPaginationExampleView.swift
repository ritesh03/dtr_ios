//
//  ListPaginationExampleView.swift
//  dtr-ios-test
//
//  Created by apple on 01/07/21.
//

import SwiftUI

struct ListPaginationExampleView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ListPaginationExampleView_Previews: PreviewProvider {
    static var previews: some View {
        ListPaginationExampleView()
    }
}
