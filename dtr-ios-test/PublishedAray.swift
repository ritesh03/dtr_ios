//
//  PublishedAray.swift
//  dtr-ios-test
//
//  Created by apple on 01/07/21.
//

import SwiftUI

struct PublishedAray: View {
    
    @State private var isLoading: Bool = false
    @State private var page: Int = 0
    private let pageSize: Int = 25
    @StateObject var searchViewModel = SearchViewModel()
    
    var body: some View {
        List {
            if searchViewModel.items.count > 0 {
                ForEach(searchViewModel.items.indices, id: \.self) { indexs in
                    if  searchViewModel.items.indices.contains(indexs) {
                        VStack(alignment: .leading) {
                            Text(searchViewModel.items[indexs])
                            if isLoading && searchViewModel.items.isLastItem(searchViewModel.items[indexs]){
                                Divider()
                                ProgressView()
                            }
                        }
                    }
                }
            }
        }
    }
}

struct PublishedAray_Previews: PreviewProvider {
    static var previews: some View {
        PublishedAray()
    }
}





import UIKit
import Combine
import Firebase

class SearchViewModel : ObservableObject {
    
    @Published var items: [String] = Array(0...24).map { "Item \($0)" }
    
    func getMoreItems(forPage page: Int,pageSize: Int) -> [String] {
        let maximum = ((page * pageSize) + pageSize) - 1
        let moreItems: [String] = Array(items.count...maximum).map { "Item \($0)" }
        return moreItems
    }
}
