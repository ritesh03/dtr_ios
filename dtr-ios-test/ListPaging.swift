//
//  ListPaging.swift
//  dtr-ios-test
//
//  Created by apple on 01/07/21.
//

import SwiftUI

struct ContentView: View {
    @State var userProfiles : [DtrProfile] = [DtrProfile.default]

    var body: some View {
        VStack{
        List(userProfiles.indices, id: \.self, rowContent: row(for:)).background(Color.red)
        }.background(Color.red)
    }

    // helper function to have possibility to generate & inject proxy binding
    private func row(for idx: Int) -> some View {
        let isOn = Binding(
            get: {
                // safe getter with bounds validation
                idx < self.userProfiles.count ? self.userProfiles[idx] : DtrProfile.default
            },
            set: { self.userProfiles[idx] = $0 }
        )
        return Toggle(isOn: isOn.isFollower, label: { Text("\(idx)").background(Color.red) } )
    }
}

struct ListPaging: View {
    @State private var items: [String] = Array(0...24).map { "Item \($0)" }
    @State private var isLoading: Bool = false
    @State private var page: Int = 0
    private let pageSize: Int = 25
    
    var body: some View {
        List(items) { item in
            VStack(alignment: .leading) {
                Text(item)
                if isLoading && items.isLastItem(item){
                    Divider()
                    ProgressView()
                }
            }.onAppear{
                listItemAppears(item)
            }
        }
    }
    
    func listItemAppears<Item: Identifiable>(_ item: Item) {
        if items.isLastItem(item) {
            isLoading = true
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
                page += 1
                let moreItems = getMoreItems(forPage: page, pageSize: pageSize)
                items.append(contentsOf: moreItems)
                isLoading = false
            }
        }
    }
    
    func getMoreItems(forPage page: Int,pageSize: Int) -> [String] {
        let maximum = ((page * pageSize) + pageSize) - 1
        let moreItems: [String] = Array(items.count...maximum).map { "Item \($0)" }
        return moreItems
    }
    
}

struct ListPaging_Previews: PreviewProvider {
    static var previews: some View {
        ListPaging()
    }
}



extension String: Identifiable {
    public var id: String {
        return self
    }
}

extension RandomAccessCollection where Self.Element: Identifiable {
    func isLastItem<Item: Identifiable>(_ item: Item) -> Bool {
        guard !isEmpty else {
            return false
        }
        
        guard let itemIndex = firstIndex(where: { $0.id.hashValue == item.id.hashValue }) else {
            return false
        }
        
        let distance = self.distance(from: itemIndex, to: endIndex)
        return distance == 1
    }
}


struct Item : Identifiable {
    var id = UUID()
    var flagged = false
    var title : String
}

class StateManager : ObservableObject {
    @Published var items = [Item(title: "Item #1"),Item(title: "Item #2"),Item(title: "Item #3"),Item(title: "Item #4"),Item(title: "Item #5")]
    
    func singularBinding(forIndex index: Int) -> Binding<Bool> {
        Binding<Bool> { () -> Bool in
            self.items[index].flagged
        } set: { (newValue) in
            self.items = self.items.enumerated().map { itemIndex, item in
                var itemCopy = item
                if index == itemIndex {
                    itemCopy.flagged = newValue
                } else {
                    //not the same index
                    if newValue {
                        itemCopy.flagged = false
                    }
                }
                return itemCopy
            }
        }
    }
    
    func reset() {
        items = items.map { item in
            var itemCopy = item
            itemCopy.flagged = false
            return itemCopy
        }
    }
}

struct MainView: View {
    @ObservedObject var stateManager = StateManager()
    
    var body: some View {
        VStack(spacing: 50) {
            VStack {
                ForEach(Array(stateManager.items.enumerated()), id:\.1.id) { (index,item) in
                    OutterView(text: item.title, flag: stateManager.singularBinding(forIndex: index))
                }
            }
            
            Text("Flagged: \(stateManager.items.filter({ $0.flagged }).map({$0.title}).description)")
            
            Button(action: {
                stateManager.reset()
            }, label: {
                Text("Reset flagged")
            })
        }
    }
}

struct OutterView: View {
    var text: String
    @Binding  var flag: Bool
    private var color: Color { flag ? Color.green : Color.gray }
    
    var body: some View {
        InnerView(color: color, text: text)
            .onTapGesture {
                flag.toggle()
            }
    }
}

struct InnerView: View {
    let color: Color
    let text: String
    
    var body: some View {
        Text(text)
            .padding()
            .background(
                Capsule()
                    .fill(color))
    }
}
