//
//  DocumentChangeType+name.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/27/20.
//

import Foundation
import Firebase

// for debugging help
extension DocumentChangeType {
    var name: String {
        switch self {
            case .added: return "Added"
            case .removed: return "Removed"
            case .modified: return "Modified"
        }
    }
}
