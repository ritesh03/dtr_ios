//
//  MinimumBuild.swift
//  dtr-ios-test
//
//  Created by Mobile on 07/05/21.
//

import UIKit

import Foundation

struct MinimumBuild {
    var ios: Int = 0
    var android: Int = 0
    var titleStrings = DataDict()
    var messageStrings = DataDict()
    
    var data: DataDict {
        get {
            return [
                "ios": self.ios,
                "android": self.android,
                "title": self.titleStrings,
                "message": self.messageStrings
            ]
        }
    }
    
    init(_ data: DataDict = [:]) {
        self.ios = asInt(data["ios"], defaultValue: 0)
        self.android = asInt(data["android"], defaultValue: 0)
        
        if let dict = data["title"] as? DataDict {
            self.titleStrings = dict
        }
        
        if let dict = data["message"] as? DataDict {
            self.messageStrings = dict
        }
    }
    
    func title(_ lang: String) -> String {
        if let value = asStringOptional(self.titleStrings[lang]) {
            return value
        }
        if let value_en = asStringOptional(self.titleStrings["en"]) {
            return value_en
        }
        return "Title"
    }
    
    func message(_ lang: String) -> String {
        if let value = asStringOptional(self.messageStrings[lang]) {
            return value
        }
        
        if let value_en = asStringOptional(self.messageStrings["en"]) {
            return value_en
        }
        return "Message"
    }
}

struct StaffRole {
    var profileID: String = ""
    var roles: [String] = []
    
    var data: DataDict {
        get {
            return [
                "profileID": self.profileID,
                "roles": self.roles
            ]
        }
    }
    
    init(_ data: DataDict = [:]) {
        self.profileID = asString(data["profileID"], defaultValue: "")
        if let roles = data["roles"] as? [String] {
            self.roles = roles
        } else {
            self.roles = [String]()
        }
    }
}
