//
//  AnalyticsType.swift
//  dtr-ios
//
//  Created by Dan Kinney on 4/9/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import Foundation

enum CampaignResult: String {
    case tagged, accepted, expired, limited, updated
}

enum AnalyticsEvent: String {
    // ride events from main view - params ["dateCode": dateCodeValue]
    case activate, cancel, summary, join, leave
    
//    case intro_completed, start_phone_verification, down_to_ride_click , down_to_ride_update
    
    // ride details - params ["rideID": rideID]  NOTE: rideID == leader+dateCode
    case rideReset, rideWrite, rideCancel, rideDrop, rideUpdateSummary, rideUpdateTime, rideUpdateLocation, rideUpdateSkills, rideUpdateNotes, rideAbandonNotes, rideUpdateMessage, rideAbandonTime, rideAbandonLocation, rideUpdate
    
    // links - params ["source": profileID]
    case friendLinkCreate, friendLinkDestroy, friendLinkReceive, friendRelationshipCreate, friendRelationshipDestroy, rideLinkRecieve, codeLinkRecieve
    
    // campaign - params ["promoCode": promoCodeValue, "result": CampaignResult]
    case campaign
    
    // notification events - params ["notificationID" : notificationID, "type": AppNotification.AppNotificationType.data]
    case notificationSent, notificationRead, notificationUnread, notificationCleared, notificationCancelled, notificationAccepted, notificationRejected, notificationExpired
    
    // messaging - params ["result": resultString]
    case mailSent, messageSent
    
    // responses - params ["type": typeString] (demographics, clubs)
    case response
    
    // NOTE: Standard Firebase Analytics types are used for
    // login - params ["method": methodString] (anoynmous, phone, email)
    // signUp - params ["method": methodString] (emailPassword, name)
    // share - params [content_type: activityTypeString, // (activityType.rawValue)
    //                 item_type: typeOfItem]  // (rideLink, friendLink, terms, privacy)
    //                 item_id: idOfItem]  // (ride.id, profile.id, termsURL, privacyURL)
    // present_offer - params [item_id, item_name, item_category]
    // purchase - params [tbd]
    
    case logout
}
enum AnalyticsObjectEvent: String {
    case first_launch
    case intro_completed
    case start_phone_verification
    case resend_verification_code
    case submit_verification_code
    case verification_failed
    case verification_completed
    case down_to_ride_click
    case down_to_ride_update
    case invite_friend_click
    case phone_number
    case share_ride
    case share_friend_link
    case share_group
    
    case group_join
    
    case open_link_friend
    case open_link_ride
    case open_link_code
    case open_link_group
    
    case receive_notification
    case click_notification
}


enum AnalyticsObjectProperties:String {
    case phone_number
    case name
    case joined_group
    case email
    case friends
    case followed
    case following
    case number_members
}
enum AnalyticsScreen: String {
    // containers
    case modal, modalNav, navigationLink
    case profile, user, friends, directory, multipleFriendsRequest, friendLink, profilePicturePicker,setting,deleteAccount
    case rideDetails, rideTime, rideLocation, rideNotes, rideAddRider, rideRiders, rideLink, rideInvites ,rideVisiblity
    case notifications, notification
    case composeMessage, composeEmail
    case onBoardingScreen, verification, verificationPhone, verificationEmail
    case aboutSkills
    case connectivity, terms, termsShare, privacy, privacyShare, premiumAd, startupTroubleshooting
    case adminTools, staffTools, profileTags, serviceStatus, blackList, blackListUserSearch, profileDisconnect, profileRides
    case campaigns, campaign, campaignCreate, campaignEdit, campaignAction
    case announcements, announcement, announcementCreate, announcementEdit, announcementAction
    case poke_join_weekend_launch, poke_join_weekend_ride, poke_go_dtr_weekend_launch, poke_go_dtr_weekend_ride, poke_come_back_launch, poke_come_back_ride, poke_friend_launch, poke_friend_ride,poke_test_total_launch, poke_test_total_ride,poke_control_total_launch,poke_control_total_ride
    case main // dateCode
}
