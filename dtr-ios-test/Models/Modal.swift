//
//  SwiftUIView.swift
//  dtr-ios-test
//
//  Created by Mobile on 04/02/21.
//

import SwiftUI


import SwiftUI

enum ModalType {
    case plain, cancel, save, share
}

struct Modal<Content>: View where Content: View {
    @Binding var isPresented: Bool
    var title: String
    var isShareRideScreen = false
    var type: ModalType = .plain
    var action: () -> Void = {}
    
    var content: () -> Content
    
    var body: some View {
        VStack(alignment: .center, spacing: 0) {
            ZStack(alignment: .top) {
                Rectangle()
                .frame(height: 72)
                .foregroundColor(Color.init(ColorConstantsName.MainThemeBgColour))
                VStack {
                    Handle()
                    HStack {
                        Button(action: {
                            self.isPresented = false
                        }) {
                            Image(uiImage: UIImage(named: ImageConstantsName.AccetAppIconImg)!)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 32.0, height: 32.0)
                        }
                        
                        Text(title)
                        .fontWeight(.bold)
                        .foregroundColor(Color.init(ColorConstantsName.HeaderForGrandColour))
                        
                        Spacer()
                        
                        if self.type == .cancel {
                            Button(action: {
                                self.isPresented = false
                            }) {
                                Image(systemName: "x.circle.fill")
                                .foregroundColor(Color.init(ColorConstantsName.HeaderForGrandColour))
                            }

                        } else if self.type == .save {
                            Button(action: {
                                self.action()

                            }) {
                                Text("SAVE")
                                .foregroundColor(Color.init(ColorConstantsName.HeaderForGrandColour))
                            }
                            
                        } else if self.type == .share {
                            Button(action: {
                                self.action()

                            }) {
                                Image(systemName: "square.and.arrow.up")
                                .foregroundColor(Color.init(ColorConstantsName.HeaderForGrandColour))
                            }
                        } else {
                            Button(action: {
                                if isShareRideScreen {
                                    UITableView.appearance().backgroundColor =  UIColor.init(Color.init(ColorConstantsName.MainThemeBgColour))
                                    UITableViewCell.appearance().backgroundColor =  UIColor.init(Color.init(ColorConstantsName.MainThemeBgColour))
                                }
                                self.action()
                                self.isPresented = false
                            }) {
                                Text("Done")
                            }
                        }
                    }
                    .padding(.horizontal)
                }
            }
            self.content()
            Spacer()
        }
        .accentColor(Color.init(ColorConstantsName.AccentColour))
    }
}

struct Modal_Previews: PreviewProvider {
    static var previews: some View {
        Modal(isPresented: .constant(true), title: "Title") {
            Text("Hello World")
        }
    }
}
