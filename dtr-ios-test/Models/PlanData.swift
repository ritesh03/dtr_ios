//
//  PlanData.swift
//  dtr-ios
//
//  Created by Dan Kinney on 2/21/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import Foundation
import Firebase

typealias PlanCompletion = (PlanData, AppError?) -> Void

struct PlanData {
    var dateCode: String
    var status: RiderStatus = .notRiding
    var ride: RideData?
    
    // data is not persisted, but it handy for debug prints
    var data: DataDict {
        [
            "dateCode": self.dateCode,
            "status": self.status.data
        ]
    }
    
    init(dateCode: String)
    {
        self.dateCode = dateCode
    }
    
    init(data: DataDict)
    {
        self.dateCode = asString(data["dateCode"], defaultValue: "19700101")
        
        let statusRaw = asInt(data["status"], defaultValue: RiderStatus.notRiding.data)
        
        if let status = RiderStatus(rawValue: statusRaw) {
            self.status = status
        }
    }
    
    func update(completion: @escaping PlanCompletion) {
        guard let profileID = Auth.auth().currentUser?.uid else {
            completion(self, AppError(.invalidProfileID))
            return
        }
        
        var plan = self
        
        if let ride = self.ride {
            plan.status = .downToRide   // at least
            
            if ride.leader == profileID {
                if ride.riders.count > 1 {
                    plan.status = .leading
                }
            } else {
                plan.status = .riding
            }
            
        } else {
            plan.status = .notRiding
        }
        
        // update the friendsRides here, based on this new status
        
        completion(plan, nil)
    }
    
    func activate(completion: @escaping PlanCompletion) {
        guard let profileID = Auth.auth().currentUser?.uid else {
            completion(self, AppError(.invalidProfileID))
            return
        }
        
//        guard self.status == .notRiding else {
//            completion(self, AppError(.stateTransition))
//            return
//        }
        
        var plan = self
        plan.ride = RideData(profileID: profileID, dateCode: self.dateCode)
        
        plan.update() { plan, error in
            print("plan for \(self.dateCode) updated")
            completion(plan, nil)
        }
    }
    
    func cancel(completion: @escaping PlanCompletion) {
        guard self.status == .downToRide || self.status == .leading else {
            completion(self, AppError(.stateTransition))
            return
        }
        
        var plan = self
        plan.ride = nil
        
        plan.update { plan, error in
            completion(plan, nil)
        }
    }
}

@dynamicMemberLookup
struct PlanDataCollection {
    var forDate = [String:PlanData]()
    
    subscript(dynamicMember dateCode: String) -> PlanData {
        return forDate[dateCode, default: PlanData(dateCode: dateCode)]
    }
}
