//
//  ReachabilityStore.swift
//  dtr-ios-test
//
//  Created by Mobile on 02/04/21.
//

import UIKit

import Combine
import os

class ReachabilityStore: ObservableObject {
    private var reachability: Reachability

    @Published var reachable: Bool = false
    @Published var reachableViaWifi: Bool = false
    @Published var reachableViaCellular: Bool = false

    init() {
        reachability = try! Reachability()

        reachability.whenReachable = { [weak self] reachability in
            guard let self = self else { return }

            self.reachable = true
            self.reachableViaWifi = reachability.connection == .wifi
            self.reachableViaCellular = !self.reachableViaWifi

            os_log(
                "Reachable via %{public}s",
                self.reachableViaWifi ? "WiFi" : "Cellular"
            )
        }

        reachability.whenUnreachable = { [weak self] _ in
            guard let self = self else { return }

            os_log("Unreachable")

            self.reachable = false
            self.reachableViaWifi = false
            self.reachableViaCellular = false
        }

        do {
            try reachability.startNotifier()
        } catch {
            os_log("Unable to start reachability notifier.")
        }
    }
}
