//
//  DtrError.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 12/31/20.
//

import Foundation

import Foundation

enum DtrErrorType: Int {
    case none, unknown, authorization, permission, ride,stateTransition
    
    var title: String {
        switch self {
        case .none: return "none"
        case .unknown: return "Unknown"
        case .authorization: return "Authorization"
        case .permission: return "Permission"
        case .ride: return "Ride"
        case .stateTransition: return "Transition Error"
        }
    }
    
    var errorDescription: String {
        switch self {
        case .none: return "No error"
        case .unknown: return "An unknown error has occurred"
        case .authorization: return "You are not authorized"
        case .permission: return "You are not authorized to complete the command"
        case .ride: return "An error occurred updating the ride"
        case .stateTransition: return "The intended state cannot be updated from the current state"
        }
    }
}

struct DtrError: DtrErrorProtocol {
    var type: DtrErrorType = .none
    var failureReason: String?
    
    var code: Int { type.rawValue }
    var title: String? { type.title }
    var errorDescription: String? { type.errorDescription }
    
    private var _description: String = ""
    
    init(_ type: DtrErrorType, message: String? = nil) {
        self.type = type
        self.failureReason = message
        self._description = type.errorDescription
        
        if let description = self.errorDescription {
            self._description = description
        }
    }
    
    init(error: Error?) {
        if let error = error {
            self.type = .unknown
            self.failureReason = error.localizedDescription
            self._description = type.errorDescription
            self._description = error.localizedDescription
            return
        }
    }
}

protocol DtrErrorProtocol: LocalizedError {
    var code: Int { get }
    var title: String? { get }
    var errorDescription: String? { get }
    var failureReason: String? { get }
}
