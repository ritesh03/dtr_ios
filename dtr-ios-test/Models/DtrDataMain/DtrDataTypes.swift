//
//  DtrDataTypes.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/27/20.
//

import Foundation

typealias DataDict = Dictionary<String,Any>

enum DtrDataStatus: Int {
    case unknown
    case signedIn
    case ready
    case signedOut
    
    var label: String {
        switch self {
        case .unknown: return "unknown"
        case .signedIn: return "signed in"
        case .ready: return "ready"
        case .signedOut: return "signed out"
        }
    }
}

enum DtrNotificationType: Int {
    case message
    case friendRequest
    case rideInvite
    case rideCancelled
    case rideUpdate
    
    var label: String {
        switch self {
        case .message: return "Message"
        case .friendRequest: return "Friend Request"
        case .rideInvite: return "Ride Invitation"
        case .rideCancelled: return "Ride Cancelled"
        case .rideUpdate: return "Ride Update"
        }
    }
}

enum DtrRideFromActionType: Int, Codable {
    case none
    case cancel
    case leave
    
    var label: String {
        switch self {
        case .none: return "Join Ride"
        case .cancel: return "Cancel Ride"
        case .leave: return "Leave Ride"
        }
    }
}


// Helper functions

func asStringOptional(_ value: Any?) -> String?
{
    if value == nil {
        return nil
    }
    
    return asString(value)
}

func asString(_ value: Any?, defaultValue: String = "") -> String
{
    if value == nil {
        return ""
    }
    
    if value is String {
        if let stringValue = value as? String {
            return stringValue
        }
    }
    
    // see if it is a non-string that can be represented as a string (like a number)
    let stringValue = "\(value ?? defaultValue)"
    
    return stringValue
}

func asIntOptional(_ value: Any?) -> Int?
{
    if value == nil {
        return nil
    }
    
    return asInt(value, defaultValue: 0)
}

func asInt(_ value: Any?, defaultValue: Int) -> Int
{
    var ret = defaultValue
    
    if let actualValue = value {
        let strValue: String  = "\(actualValue)"
        
        if !(strValue == "") && !(strValue == "null") {
            ret = Int((strValue as NSString).intValue)
        }
    }
    
    return ret
}

func asDoubleOptional(_ value: Any?) -> Double?
{
    if value == nil {
        return nil
    }
    
    return asDouble(value, defaultValue: 0)
}

func asDouble(_ value: Any?, defaultValue: Double) -> Double {
    var ret = defaultValue
    
    if let actualValue = value {
        let strValue: String  = "\(actualValue)"
        
        if !(strValue == "") && !(strValue == "null") {
            ret = Double((strValue as NSString).doubleValue)
        }
    }
    
    return ret
}

func asBoolOptional(_ value: Any?) -> Bool?
{
    if value == nil {
        return nil
    }
    
    return asBool(value)
}

func asBool(_ value: Any?) -> Bool
{
    if value == nil {
        return false
    }
    
    var ret = false
    
    switch value {
    case is Bool:
        return value as! Bool
        
    default:
        let strValue: String  = "\(value!)"
        
        if !(strValue == "") && !(strValue == "null") {
            let intValue = Int((strValue as NSString).intValue)
            ret = (intValue == 1)
        }
    }
    
    return ret
}

func asDate(_ value: Any?, defaultValue: Date) -> Date{
    var ret = Date()
    if let actualValue = value {
        let strValue: String  = "\(actualValue)"
        
        if !(strValue == "") && !(strValue == "null") {
            let doubleValue = Double((strValue as NSString).doubleValue)
            ret = Date(timeIntervalSince1970: doubleValue)
        }
    }
    return ret
}

func asDateOptional(_ value: Any?) -> Date? {
    if let actualValue = value {
        let strValue: String  = "\(actualValue)"
        
        if !(strValue == "") && !(strValue == "null") {
            let doubleValue = Double((strValue as NSString).doubleValue)
            return Date(timeIntervalSince1970: doubleValue)
        }
    }
    return nil
}

func asDataDictOptional(_ value: Any?) -> DataDict? {
    if value == nil {
        return nil
    }
    
    if let dataString = asStringOptional(value) {
        if let dict = try? dataString.asJSONToDictionary() {
            return dict
        }
    }
    
    if let object = value as? [String:Any] {
        if let data = try? JSONSerialization.data(withJSONObject: object, options: []) {
            if let result = try? data.toDictionary() {
                return result
            }
        }
    }
    
    return nil
}

func asDataDict(_ value: Any?) -> DataDict {
    if let dict = asDataDictOptional(value) {
        return dict
    }
    return [:]
}


func asArray(_ maybeRides: [DtrRide]?) -> [DtrRide] {
    if let rides = maybeRides {
        return rides
    }
    
    return []
}
