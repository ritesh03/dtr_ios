//
//  DtrData.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/27/20.
//

import Foundation
import Combine
import Firebase

typealias DtrRideByDate = [String:DtrRide]
typealias DtrRidesByDate = [String:Set<DtrRide>]

class DtrData : NSObject, ObservableObject, UNUserNotificationCenterDelegate {
    @Published var statusMessage: String = "Starting"
    @Published var statusLogin: DtrDataStatus = .unknown
    @Published var profileID: String = ""
    @Published var profileImageUploading: Bool = false
    @Published var profile = DtrProfile() { didSet { self.didChange.send(self) }}
    @Published var profileDirectory = Profiles() { didSet  { self.didChange.send(self)  } }
    @Published var groupsDirectory = Groups() { didSet  { self.didChange.send(self)  } }
    @Published var tagsDirectory = RideTagsData() { didSet  { self.didChange.send(self)  } }
    @Published var badgesDirectory = RideBadgeData() { didSet  { self.didChange.send(self)  } }
    @Published var plansMessage = Plans() { didSet  { self.didChange.send(self)  } }
    
    @Published var friendsProfiles = [DtrProfile]() { didSet { self.didChange.send(self) } }
    
    @Published var followingProfiles = [DtrProfile]() { didSet { self.didChange.send(self) } }
    @Published var follwedProfiles = [DtrProfile]() { didSet { self.didChange.send(self) } }
    
    @Published var rideList = RideList() { didSet { self.didChange.send(self) } }
    @Published var announcements = [AnnouncementsModel]() { didSet  { self.didChange.send(self) } }
    @Published var staffRole = StaffRole() { didSet { self.didChange.send(self) } }
    
    @Published var myRidesByDate = DtrRideByDate(){ didSet {
        if self.statusLogin == .signedIn {
            self.statusLogin = .ready
        }
        self.didChange.send(self)
    }}
    
    @Published var suggestedRidesByDate = DtrRidesByDate() { didSet { self.didChange.send(self) } }
    @Published var allRidesForWeek = DtrRidesByDate() { didSet { self.didChange.send(self) } }
    @Published var allUserRidesForWeek = DtrRidesByDate() { didSet { self.didChange.send(self) } }
    @Published var deletedRides = [""] { didSet { self.didChange.send(self) } }
    
    @Published var network = ReachabilityStore()
    @Published var dyanamicLinkRide = DtrRide(profileID: "RIDER_ID", dateCode: "1912542", summary: "Test") { didSet { self.didChange.send(self) } }
    @Published var dyanamicLinkGroup = DtrGroup(id: "Setup Group") { didSet { self.didChange.send(self) } }
    
    var db: Firestore
    private var authStateListener: AuthStateDidChangeListenerHandle?
    private var profileListener: ListenerRegistration?
    private var myRidesListener: ListenerRegistration?
    private var suggestedRidesListener: ListenerRegistration?
    private var weeklyRidesListener: ListenerRegistration?
    private var getAllProfile: ListenerRegistration?
    private var getAllGroups: ListenerRegistration?
    private var getAllTags: ListenerRegistration?
    private var getAllbadges: ListenerRegistration?
    
    @Published var receivedNotification: AppNotification? { didSet { self.didChange.send(self) } }
    
    let dateCodeFormatter = DateFormatter()
    
    //Rs
    private var sendNotification: ListenerRegistration?
    private var incomincomingNotifiction: ListenerRegistration?
    private var announcementsNotifiction: ListenerRegistration?
    
    private var minimumBuildListener: ListenerRegistration?
    private var staffRoleListener:ListenerRegistration?
    
    @Published var sentNotifications = [AppNotification]()
    @Published var sentRideInviteNotifications = [AppNotification]()
    
    @Published var notifications = [AppNotification]() {
        didSet {
            UIApplication.shared.applicationIconBadgeNumber = notifications.count(where: {$0.status == .unread})
            self.didChange.send(self)
        }
    }
    
    var unreadNotifications: Int {
        return notifications.count(where: {$0.status == .unread})
    }
    
    @Published var minimumBuild = MinimumBuild() { didSet { self.didChange.send(self) } }
    
    override init() {
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        self.db = Firestore.firestore()
        db.settings = settings
        
        dateCodeFormatter.dateFormat = "yyyyMMdd"
        
        super.init()
        UNUserNotificationCenter.current().delegate = self
        
        self.reconnect()
        
        NotificationCenter.default.addObserver(self, selector:#selector(calendarDayDidChange), name:.NSCalendarDayChanged, object:nil)
    }
    
    @objc func calendarDayDidChange() {
        print("Date Change")
        self.reconnect()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping(UNNotificationPresentationOptions) -> Swift.Void) {
        // this executes when the notification is about to present itself on screen
        print("*** willPresent:\n\(notification.request.content.userInfo.prettyFormatted()) Identifier: \(notification.request.identifier)")
        completionHandler([.banner, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping() -> Swift.Void) {
        // this executes when the user has touched on a notification
        print("••• didReceive (background):\n\(response.notification.request.content.userInfo.prettyFormatted())\n")
        if let data = asDataDictOptional(response.notification.request.content.userInfo["notification"])  {
            let notification = AppNotification(data: data)
            self.receivedNotification = notification
        } else if let dataDict = asDataDictOptional(response.notification.request.content.userInfo)  {
            var notification = AppNotification.default
            
            if let paylodID = dataDict["payloadID"]  {
                notification.payloadID = paylodID as! String
            }
            if let sourceID = dataDict["source"]  {
                notification.source = sourceID as! String
            }
            
            if let tagID = dataDict["tag"] {
                let varTagid = tagID as! String
                if varTagid  == "follow" {
                    notification.type = .follow
                }
                if varTagid  == "friendInvite" {
                    notification.type = .friendInvite
                }
                if varTagid  == "rideInvite" {
                    notification.type = .rideInvite
                }
                if varTagid  == "rideLocation" {
                    notification.type = .rideLocation
                }
                if varTagid  == "rideTime" {
                    notification.type = .rideTime
                }
                if varTagid  == "rideCancelled" {
                    notification.type = .rideCancelled
                }
                if varTagid  == "rideUpdate" {
                    notification.type = .rideUpdate
                }
                
                if varTagid  == "poke" {
                    notification.type = .poke
                }
                
                if varTagid  == "groupJoin" {
                    notification.type = .groupJoin
                }
                
                if varTagid  == "groupLeadernotify" {
                    notification.type = .groupLeadernotify
                }
                
                if varTagid  == "message" {
                    notification.type = .message
                }
            }
            self.receivedNotification = notification
        }
        completionHandler()
    }
    
    
    func reconnect() {
        if let handle = self.authStateListener {
            Auth.auth().removeStateDidChangeListener(handle)
        }
        self.authStateListener = self.listenAuthState()
    }
    
    func listenAuthState() -> AuthStateDidChangeListenerHandle? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        dateFormatter.timeZone = NSTimeZone.local
        
        let listener = Auth.auth()
            .addStateDidChangeListener { (auth, user) in
                if let user = user {
                    print("auth changed to user: \(user.uid)")
                    self.statusMessage = "Authorized"
                    
                    self.profileID = user.uid
                    UserStore.shared.profileIdForUnitTest = user.uid
                    
                    if UserStore.shared.isVerificationComplelteOrNot {
                        self.statusLogin = .signedIn
                    }
                    
                    // check to see if profile exists or not
                    self.db.collection("profiles").document(self.profileID).getDocument(completion: { documentSnapshot, error in
                        if let error = error {
                            print("profile error: \(error.localizedDescription)")
                        }
                        
                        if let snapshot = documentSnapshot {
                            if let data = snapshot.data() {
                                // NOTE: don't set the profile here...let it get set in the listener
                                // check to see if the token needs to be updated, however
                                let friends = data["friends"] as? [String] ?? []
                                let followed = data["followed"] as? [String] ?? []
                                let phonenumber = Auth.auth().currentUser?.phoneNumber
                                let following = data["following"] as? [String] ?? []
                                let joinGroups = data["joinedgroups"] as? [String] ?? []
                                
                                Analytics.setUserProperty(data["name"] as? String ?? "", forName: AnalyticsObjectProperties.name.rawValue)
                                Analytics.setUserProperty(phonenumber, forName: AnalyticsObjectProperties.phone_number.rawValue)
                                Analytics.setUserProperty(data["email"] as? String ?? "", forName: AnalyticsObjectProperties.email.rawValue)
                                Analytics.setUserProperty("\(friends.count)", forName: AnalyticsObjectProperties.friends.rawValue)
                                Analytics.setUserProperty("\(followed.count)", forName: AnalyticsObjectProperties.followed.rawValue)
                                Analytics.setUserProperty("\(following.count)", forName: AnalyticsObjectProperties.following.rawValue)
                                Analytics.setUserProperty("\(joinGroups.count)", forName: AnalyticsObjectProperties.joined_group.rawValue)
                                
                                let theProfile = DtrProfile(data: data)
                                if let token = Messaging.messaging().fcmToken {
                                    if let currentToken = theProfile.pushToken {
                                        if currentToken != token {
                                            self.updatePushToken(token: token)
                                        }else{
                                            self.updatePushToken(token: token)
                                        }
                                    } else {
                                        self.updatePushToken(token: token)
                                    }
                                }
                            }
                        }
                    })
                    
                    self.getAllProfile = self.listenDirectory()
                    self.getAllGroups = self.listenGroupDirectory()
                    self.getAllbadges = self.listenBadgeDirectory()
                    self.getAllTags = self.listenTagsDirectory()
                    self.profileListener = self.listenProfile()
                    self.staffRoleListener = self.listenStaffRole()
                    self.sendNotification = self.listenSentNotifications()
                    self.incomincomingNotifiction = self.listenIncomingNotifications()
                    self.announcementsNotifiction = self.listenAnnoucements()
                    self.minimumBuildListener = self.listenMinimumBuild()
                } else {
                    // there is no current user
                    self.getAllProfile?.remove()
                    self.getAllGroups?.remove()
                    self.getAllTags?.remove()
                    self.profileListener?.remove()
                    self.myRidesListener?.remove()
                    self.suggestedRidesListener?.remove()
                    self.weeklyRidesListener?.remove()
                    self.getAllbadges = self.listenBadgeDirectory()
                    self.announcementsNotifiction?.remove()
                    self.sendNotification?.remove()
                    self.staffRoleListener?.remove()
                    self.incomincomingNotifiction?.remove()
                    self.minimumBuildListener?.remove()
                    // reset all user-specific data
                    self.profileID = ""
                    self.statusLogin = .signedOut
                    self.profile = DtrProfile()
                    self.myRidesByDate = DtrRideByDate()
                    self.suggestedRidesByDate = DtrRidesByDate()
                    self.allRidesForWeek = DtrRidesByDate()
                    self.allUserRidesForWeek = DtrRidesByDate()
                    self.staffRole = StaffRole()
                }
            }
        return listener
    }
    
    func listenStaffRole() -> ListenerRegistration {
        let listener = db.collection("staff")
            .whereField("profileID", isEqualTo: self.profileID)
            .addSnapshotListener { (querySnapshot, err) in
                guard err == nil else {
                    print("staffRole listener error: \(err?.localizedDescription ?? "UNKNOWN")")
                    return
                }
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    if let document = documents.first {
                        let role = StaffRole(document.data())
                        self.staffRole = role
                        print("••• staffRole: \(role.data.prettyFormatted())")
                    } else {
                        self.staffRole = StaffRole()
                        print("••• staffRole: undefined")
                    }
                }
            }
        return listener
    }
    
    func listenMinimumBuild() -> ListenerRegistration {
        let listener = db.collection("admin").document("minimumBuild")
            .addSnapshotListener { (documentSnapshot, err) in
                guard err == nil else {
                    print("minimumBuildListener error: \(err?.localizedDescription ?? "UNKNOWN")")
                    return
                }
                
                if let snapshot = documentSnapshot {
                    if let data = snapshot.data() {
                        let minimumBuild = MinimumBuild(data)
                        self.minimumBuild = minimumBuild
                    }
                }
            }
        return listener
    }
    
    func listenAnnoucements() -> ListenerRegistration {
        let listener = db.collection("announcements")
            .addSnapshotListener { (querySnapshot, err) in
                guard err == nil else {
                    print("listener error: \(err?.localizedDescription ?? "UNKNOWN")")
                    return
                }
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    var all = [AnnouncementsModel]()
                    for document in documents {
                        let annoucement = AnnouncementsModel(data: document.data())
                        if annoucement.id != "build90" {
                            all.append(annoucement)
                        }
                        //  if annoucement.expires > Date() {
                        //if !annoucement.responded(profileID: self.profileID ) {
                        // let notification = AppNotification(source: CommonAllString.DtrStr, target: self.profileID , type: .announcement, payloadID: annoucement.id, message: annoucement.title, expires: annoucement.expires)
                        //self.sendNotification(notification: notification)
                        //annoucement.respond(profileID: self.profileID , state: .activated)
                        //self.saveAnnouncement(annoucement)
                        //}
                        //}
                    }
                    self.announcements = all
                }
            }
        return listener
    }
    
    
    func respondAnnouncement(announcementID: String, state: AnnouncmentAction.State) {
        guard let user = Auth.auth().currentUser else {
            print("invalid user!")
            return
        }
        if let index = self.announcements.firstIndex(where: { $0.id == announcementID }) {
            var announcement = announcements[index]
            announcement.respond(profileID: user.uid, state: state)
            saveAnnouncement(announcement)
        }
    }
    
    func saveAnnouncement(_ announcement: AnnouncementsModel) {
        db.collection("announcements").document(announcement.id).setData(announcement.data, merge: true)
    }
    
    
    func listenProfile() -> ListenerRegistration {
        let listener = db.collection("profiles")
            .whereField("id", isEqualTo: self.profileID)
            .addSnapshotListener { (querySnapshot, err) in
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    if let document = documents.first {
                        self.statusMessage = "Loading Profile"
                        let profile = DtrProfile(data: document.data())
                        self.resolveFriendProfiles(profile: profile) { resolvedProfile in
                            self.profile = resolvedProfile
                            // startup ride listeners now that we have our profile fully resolved
                            self.myRidesListener = self.listenMyRides()
                            self.suggestedRidesListener = self.listenSuggestedRides()
                            self.weeklyRidesListener = self.listenWeekRides()
                            self.rebuildFriendsList()
                        }
                        self.resolveFollowedProfiles(profile: profile) { resolvedProfile in
                            self.profile = resolvedProfile
                            self.rebuildFollowedList()
                        }
                        self.resolveFollowingProfiles(profile: profile) { resolvedProfile in
                            self.profile = resolvedProfile
                            self.rebuildFollowingList()
                        }
                    } else {
                        print("No profile document exists for \(self.profileID)")
                        self.statusLogin = .signedOut
                    }
                }
            }
        return listener
    }
    
    func listenDirectory() -> ListenerRegistration {
        var currentUser = ""
        if let user = Auth.auth().currentUser {
            currentUser = user.uid
        }
        let listener = db.collection("profiles")
            .addSnapshotListener { querySnapshot, error in
                if let e = error {
                    print("error fetching profile documents from listener: \(e.localizedDescription)")
                    return
                }
                var updatedDirectory = Profiles()
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    for document in documents {
                        if let profileID = asStringOptional(document["id"]) {
                            // skip incomplete profiles
                            if profileID == "SETUP" {
                                continue
                            }
                            
                            if profileID == currentUser {
                                //continue
                            }
                            let directoryProfile = DtrProfile(data: document.data())
                            updatedDirectory.forID[profileID] = directoryProfile
                        }
                    }
                }
                self.profileDirectory = updatedDirectory
            }
        return listener
    }
    
    func listenGroupDirectory() -> ListenerRegistration {
        let listener = db.collection("groups")
            .addSnapshotListener { querySnapshot, error in
                if let e = error {
                    print("error fetching profile documents from listener: \(e.localizedDescription)")
                    return
                }
                var updatedDirectory = Groups()
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    for document in documents {
                        if let groupID = asStringOptional(document["id"]) {
                            let directoryGroup = DtrGroup(data: document.data())
                            updatedDirectory.forID[groupID] = directoryGroup
                        }
                    }
                }
                self.groupsDirectory = updatedDirectory
            }
        return listener
    }
    
    func listenBadgeDirectory() -> ListenerRegistration {
        let listener = db.collection("badges")
            .addSnapshotListener { querySnapshot, error in
                if let e = error {
                    print("error fetching profile documents from listener: \(e.localizedDescription)")
                    return
                }
                var updatedDirectory = RideBadgeData()
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    for document in documents {
                        if let groupID = asStringOptional(document["id"]) {
                            let directoryGroup = DtrBadge(data: document.data())
                            updatedDirectory.forID[groupID] = directoryGroup
                        }
                    }
                }
                self.badgesDirectory = updatedDirectory
            }
        return listener
    }
    
    
    func listenTagsDirectory() -> ListenerRegistration {
        let listener = db.collection("tags")
            .addSnapshotListener { querySnapshot, error in
                if let e = error {
                    print("error fetching profile documents from listener: \(e.localizedDescription)")
                    return
                }
                var updatedDirectory = RideTagsData()
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    for document in documents {
                        if let groupID = asStringOptional(document["id"]) {
                            let directoryGroup = RideTagsModel(data: document.data())
                            updatedDirectory.forID[groupID] = directoryGroup
                        }
                    }
                }
                self.tagsDirectory = updatedDirectory
            }
        return listener
    }
    
    
    func listenMyRides() -> ListenerRegistration {
        let startDateCode = dateCode(offset: 0)
        let endDateCode = dateCode(offset: 6)
        self.statusMessage = "Checking rider profile"
        //self.myRidesByDate.removeAll()
        let listener = db.collection("rides")
            .whereField("dateCode", isGreaterThanOrEqualTo: startDateCode)
            .whereField("dateCode", isLessThanOrEqualTo: endDateCode)
            .whereField("riders", arrayContains: self.profileID)
            .addSnapshotListener { querySnapshot, error in
                if let error = error {
                    print("my rides listener error: \(error.localizedDescription)")
                    return
                }
                
                if let snapshot = querySnapshot {
                    if UserStore.shared.isVerificationComplelteOrNot {
                        if self.statusLogin != .ready && snapshot.documents.count == 0 {
                            self.statusLogin = .ready
                        }
                    }
                    snapshot.documentChanges.forEach { diff in
                        let ride = DtrRide(data: diff.document.data())
                        let dateCode = ride.dateCode
                        
                        if diff.type == .removed  {
                            self.myRidesByDate.removeValue(forKey: dateCode)
                        }
                        
                        if  diff.type == .modified {
                            self.myRidesByDate.removeValue(forKey: dateCode)
                            self.myRidesByDate[dateCode] = ride
                        }
                        
                        if diff.type == .added {
                            self.myRidesByDate[dateCode] = ride
                        }
                        if ride.riders.contains(self.profileID) {
                            self.updatePlan(for: self.profileID, on: dateCode)
                        } else {
                            self.rebuildRideList(dateCode: dateCode)
                        }
                    }
                }
            }
        return listener
    }
    
    func listenSuggestedRides() -> ListenerRegistration {
        let startDateCode = dateCode(offset: 0)
        let endDateCode = dateCode(offset: 6)
        let listener = db.collection("rides")
            .whereField("dateCode", isGreaterThanOrEqualTo: startDateCode)
            .whereField("dateCode", isLessThanOrEqualTo: endDateCode)
            .whereField("visibleTo", arrayContains: self.profileID)
            .addSnapshotListener { querySnapshot, error in
                if let error = error {
                    print("suggested rides listener error: \(error.localizedDescription)")
                    return
                }
                if let snapshot = querySnapshot {
                    snapshot.documentChanges.forEach { diff in
                        let ride = DtrRide(data: diff.document.data())
                        let dateCode = ride.dateCode
                        var ridesForDate = Set<DtrRide>()
                        if let existingRidesForDate = self.suggestedRidesByDate[dateCode] {
                            ridesForDate = existingRidesForDate
                        }
                        switch diff.type {
                        case .removed:
                            ridesForDate.remove(ride)
                        case .modified:
                            if let index = ridesForDate.firstIndex(where: { $0.id == ride.id }) {
                                ridesForDate.remove(at: index)
                                ridesForDate.insert(ride)
                            }
                        case .added:
                            ridesForDate.insert(ride)
                        }
                        self.suggestedRidesByDate.updateValue(ridesForDate, forKey: dateCode)
                    }
                }
            }
        return listener
    }
    
    func listenWeekRides() -> ListenerRegistration {
        let startDateCode = dateCode(offset: 0)
        let endDateCode = dateCode(offset: 6)
        let listener = db.collection("rides")
            .whereField("dateCode", isGreaterThanOrEqualTo: startDateCode)
            .whereField("dateCode", isLessThanOrEqualTo: endDateCode)
            .addSnapshotListener { querySnapshot, error in
                if let error = error {
                    print("suggested rides listener error: \(error.localizedDescription)")
                    return
                }
                if let snapshot = querySnapshot {
                    snapshot.documentChanges.forEach { diff in
                        let ride = DtrRide(data: diff.document.data())
                        let dateCode = ride.dateCode
                        var ridesSuggestedForDate = Set<DtrRide>()
                        
                        if ride.details.visibility == .public {
                            if ride.details.hasStartTime && ride.details.hasStartPlace {
                                if ride.details.skillA || ride.details.skillB || ride.details.skillC || ride.details.skillD || ride.details.skillS {
                                    if ride.summary.lowercased() != "Down to Ride".lowercased() {
                                        if let existingRidesForDate = self.allRidesForWeek[dateCode] {
                                            ridesSuggestedForDate = existingRidesForDate
                                        }
                                        switch diff.type {
                                        case .removed:
                                            ridesSuggestedForDate.remove(ride)
                                            self.deletedRides.append(ride.id)
                                        case .modified:
                                            if let index = ridesSuggestedForDate.firstIndex(where: { $0.id == ride.id }) {
                                                ridesSuggestedForDate.remove(at: index)
                                                ridesSuggestedForDate.insert(ride)
                                            }
                                        case .added:
                                            ridesSuggestedForDate.insert(ride)
                                        }
                                        self.allRidesForWeek.updateValue(ridesSuggestedForDate, forKey: dateCode)
                                    }
                                }
                            }
                        }
                        
                        
                        var ridesUserSuggestedForDate = Set<DtrRide>()
                        
                        if let existingRidesForDate = self.allUserRidesForWeek[dateCode] {
                            ridesUserSuggestedForDate = existingRidesForDate
                        }
                        
                        switch diff.type {
                        case .removed:
                            ridesUserSuggestedForDate.remove(ride)
                            self.deletedRides.append(ride.id)
                        case .modified:
                            if let index = ridesUserSuggestedForDate.firstIndex(where: { $0.id == ride.id }) {
                                ridesUserSuggestedForDate.remove(at: index)
                                ridesUserSuggestedForDate.insert(ride)
                            }
                        case .added:
                            ridesUserSuggestedForDate.insert(ride)
                        }
                        self.allUserRidesForWeek.updateValue(ridesUserSuggestedForDate, forKey: dateCode)
                    }
                }
            }
        return listener
    }
    
    
    func getSuggestedRidesCount(dateCode:String, completion: @escaping ([DtrRide]?) -> Void) {
        var arraySuggestedRides = [DtrRide]()
        db.collection("rides")
            .whereField("dateCode", isEqualTo:dateCode)
            .whereField("visibleTo", arrayContains: self.profileID)
            .getDocuments() { querySnapshot, error in
                if let snapshot = querySnapshot {
                    snapshot.documentChanges.forEach { diff in
                        let ride = DtrRide(data: diff.document.data())
                        arraySuggestedRides.append(ride)
                    }
                }
                completion(arraySuggestedRides)
            }
    }
    
    func getRidesForInviteScrn(userID:String,dateCode:String, completion: @escaping ([DtrRide]?) -> Void) {
        var arraySuggestedRides = [DtrRide]()
        db.collection("rides")
            .whereField("dateCode", isEqualTo:dateCode)
            .whereField("leader", isEqualTo:userID)
            .getDocuments() { querySnapshot, error in
                if let snapshot = querySnapshot {
                    snapshot.documentChanges.forEach { diff in
                        let ride = DtrRide(data: diff.document.data())
                        arraySuggestedRides.append(ride)
                    }
                }
                completion(arraySuggestedRides)
            }
    }
    
    
    private func friendIsRiding(riders: [String]) -> Bool {
        let intersection = [String](Set<String>(self.profile.friends).intersection(riders))
        let result = intersection.count > 0
        return result
    }
    
    private func onMyRide(ride: DtrRide) -> Bool {
        if let myRide = self.myRidesByDate[ride.dateCode] {
            let intersection = [String](Set<String>(myRide.riders).intersection(ride.riders))
            return intersection.count > 0
        }
        return false
    }
    
    private func shouldAddRide(rides: [DtrRide], ride: DtrRide) -> Bool {
        guard let user = Auth.auth().currentUser else {
            print("invalid user!")
            return false
        }
        
        // this is called on each ride that matches the suggestedRidesListener
        
        // RULES:
        // Exclude any rides that I am already participating
        // Exclude any duplicate rides
        
        if friendIsRiding(riders: ride.riders) {
            if !onMyRide(ride: ride) {
                if !ride.riders.contains(user.uid) {
                    //Removed  line by rs becuase when current ride delete and again DTR the ride now showing again in the suggested list
                    //if !rides.contains(where: { $0.id == ride.id }) {
                    return true
                    //}
                }
            }
        }
        return false
    }
    
    func updatePushToken(token: String) {
        //        self.profile.pushToken = token
        //        self.profile.write(self.db, propagate: false) { profile, error in
        //            if let error = error {
        //                print("error updating token: \(error.localizedDescription)")
        //                return
        //            }
        //            self.profile = profile
        //        }
        db.collection("profiles").document(profile.id).updateData(["pushToken":token]) { (error) in
        }
    }
    
    func loadProfile(profileID: String, completion: @escaping (DtrProfile?) -> Void) {
        guard Auth.auth().currentUser != nil else {
            completion(nil)
            return
        }
        if profileID != CommonAllString.BlankStr {
            self.db.collection("profiles").document(profileID).getDocument { documentSnapshot, error in
                if let snapshot = documentSnapshot {
                    if let data = snapshot.data() {
                        let profile = DtrProfile(data: data)
                        //let localHash = self.profilePictureHash(profileID: profileID)
                        //self.saveProfilePictureIfNeeded(profileID: profileID, localHash: localHash)
                        completion(profile)
                        return
                    }
                }
                completion(nil)
            }
        }else{
            completion(nil)
        }
    }
    
    func loadBadgeDetail(badgeID: String, completion: @escaping (DtrBadge?) -> Void) {
        guard Auth.auth().currentUser != nil else {
            completion(nil)
            return
        }
        self.db.collection("badges").document(badgeID).getDocument { documentSnapshot, error in
            if let snapshot = documentSnapshot {
                if let data = snapshot.data() {
                    let profile = DtrBadge(data: data)
                    completion(profile)
                    return
                }
            }
            completion(nil)
        }
    }
    
    
    func upcomingRides(for profile: DtrProfile, completion: @escaping ([DtrRide]) -> Void) {
        guard Auth.auth().currentUser != nil else {
            completion([])
            return
        }
        
        let startDateCode = dateCode(offset: 0)
        let endDateCode = dateCode(offset: 6)
        
        self.db.collection("rides")
            .whereField("dateCode", isGreaterThanOrEqualTo: startDateCode)
            .whereField("dateCode", isLessThanOrEqualTo: endDateCode)
            .whereField("riders", arrayContains: profile.id)
            .getDocuments { (querySnapshot, err) in
                guard err == nil else {
                    print("loadUpcomingRides query error: \(err?.localizedDescription ?? "UNKNOWN")")
                    completion([])
                    return
                }
                
                if let snapshot = querySnapshot {
                    var upcomingRides = [DtrRide]()
                    
                    for document in snapshot.documents {
                        let ride = DtrRide(data: document.data())
                        upcomingRides.append(ride)
                    }
                    self.resolveRiderProfiles(rides: upcomingRides) { resolvedRides in
                        completion(resolvedRides)
                    }
                }
            }
    }
    
    
    func getRideDetailForSpecificRide(rideID: String, completion: @escaping (DtrRide?) -> Void) {
        db.collection("rides")
            .whereField("id", isEqualTo: rideID)
            .getDocuments() { querySnapshot, error in
                if let snapshot = querySnapshot {
                    if let rideDocument = snapshot.documents.first {
                        completion(DtrRide(data: rideDocument.data()))
                    }
                }
                completion(nil)
            }
    }
    
    
    
    func upcomingRideWithSourceCode(profileID: String, dateCode: String, completion: @escaping (DtrRide?) -> Void) {
        db.collection("rides")
            .whereField("riders", arrayContains: profileID)
            .whereField("id", isEqualTo: dateCode)
            .getDocuments() { querySnapshot, error in
                if let snapshot = querySnapshot {
                    if let rideDocument = snapshot.documents.first {
                        completion(DtrRide(data: rideDocument.data()))
                    }
                }
                completion(nil)
            }
    }
    
    func upcomingRideWithId(profileID: String, dateCode: String, completion: @escaping (DtrRide?) -> Void) {
        db.collection("rides")
            .whereField("riders", arrayContains: profileID)
            .whereField("dateCode", isEqualTo: dateCode)
            .getDocuments() { querySnapshot, error in
                if let snapshot = querySnapshot {
                    if let rideDocument = snapshot.documents.first {
                        completion(DtrRide(data: rideDocument.data()))
                    }
                }
                completion(nil)
            }
    }
    
    func upcomingRideWithPayloadId(profileID: String, dateCode: String, completion: @escaping (DtrRide?) -> Void) {
        db.collection("rides")
            .whereField("riders", arrayContains: profileID)
            .whereField("dateCode", isEqualTo: dateCode)
            .getDocuments() { querySnapshot, error in
                if let snapshot = querySnapshot {
                    if let rideDocument = snapshot.documents.first {
                        completion(DtrRide(data: rideDocument.data()))
                    }
                }
                completion(nil)
            }
    }
    
    
    func resolveRiderProfiles(rides: [DtrRide], completion: @escaping ([DtrRide]) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var updatedRides = [DtrRide]()
            
            let rideGroup = DispatchGroup()
            
            for ride in rides {
                rideGroup.enter()
                
                var updatedRide = ride
                var profiles = Dictionary<String,DtrProfile>()
                let resolveGroup = DispatchGroup()
                
                for rider in ride.riders {
                    resolveGroup.enter()
                    
                    self.loadProfile(profileID: rider) { resolvedProfile in
                        if let profile = resolvedProfile {
                            profiles[profile.id] = profile
                            //if rider == ride.leader {
                            //updatedRide.leaderName = profile.name
                            //}
                        }
                        resolveGroup.leave()
                    }
                }
                
                resolveGroup.wait()
                
                DispatchQueue.main.async {
                    updatedRide.riderProfiles = profiles
                    updatedRides.append(updatedRide)
                    rideGroup.leave()
                }
            }
            
            rideGroup.wait()
            
            DispatchQueue.main.async {
                completion(updatedRides)
            }
        }
    }
    
    func resolveRiderProfiles(ride: DtrRide, completion: @escaping (DtrRide) -> Void) {
        let rides: [DtrRide] = [ride]
        resolveRiderProfiles(rides: rides) { resolvedRides in
            guard let resolvedRide = resolvedRides.first else {
                // something went wrong, just return the unresolved ride
                completion(ride)
                return
            }
            completion(resolvedRide)
        }
    }
    
    func resolveFriendProfiles(profile: DtrProfile, completion: @escaping (DtrProfile) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var updatedProfile = profile
            var profiles = Dictionary<String,DtrProfile>()
            let resolveGroup = DispatchGroup()
            for friend in profile.friends {
                resolveGroup.enter()
                if !profiles.contains(where: { $0.value.id != friend }) {
                    self.loadProfile(profileID: friend) { profile in
                        if let profile = profile {
                            profiles[profile.id] = profile
                        }
                        resolveGroup.leave()
                    }
                }
            }
            resolveGroup.wait()
            DispatchQueue.main.async {
                updatedProfile.friendProfiles = profiles
                completion(updatedProfile)
            }
        }
    }
    
    func resolveFollowedProfiles(profile: DtrProfile, completion: @escaping (DtrProfile) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var updatedProfile = profile
            var profiles = Dictionary<String,DtrProfile>()
            let resolveGroup = DispatchGroup()
            for friend in profile.followed {
                resolveGroup.enter()
                if !profiles.contains(where: { $0.value.id != friend }) {
                    self.loadProfile(profileID: friend) { profile in
                        if let profile = profile {
                            profiles[profile.id] = profile
                        }
                        resolveGroup.leave()
                    }
                }
            }
            
            resolveGroup.wait()
            DispatchQueue.main.async {
                updatedProfile.followdProfiles = profiles
                completion(updatedProfile)
            }
        }
    }
    
    func resolveGroupProfiles(profile: DtrProfile,groupID: String, completion: @escaping (DtrGroup) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var groupDetailEdit : GroupEditViewModel = GroupEditViewModel.init(group: DtrGroup.default)
            let resolveGroup = DispatchGroup()
            
            resolveGroup.enter()
            print("group-->",groupID)
            DtrCommand.sharedInstance.groupProfileList(groupID: groupID) { result in
                print(result.output)
                if let groupData = result.output["group"] as? DataDict {
                    groupDetailEdit.grouEdit = DtrGroup.init(data: groupData)
                }
                resolveGroup.leave()
            }
            resolveGroup.wait()
            DispatchQueue.main.async {
                completion(groupDetailEdit.grouEdit)
            }
        }
    }
    
    func resolveFollowingProfiles(profile: DtrProfile, completion: @escaping (DtrProfile) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var updatedProfile = profile
            var profiles = Dictionary<String,DtrProfile>()
            let resolveGroup = DispatchGroup()
            for friend in profile.following {
                resolveGroup.enter()
                if !profiles.contains(where: { $0.value.id != friend }) {
                    self.loadProfile(profileID: friend) { profile in
                        if let profile = profile {
                            profiles[profile.id] = profile
                        }
                        resolveGroup.leave()
                    }
                }
            }
            
            resolveGroup.wait()
            DispatchQueue.main.async {
                updatedProfile.followingProfiles = profiles
                completion(updatedProfile)
            }
        }
    }
    
    func profilePictureHash(profileID: String) -> String {
        let manager = FileManager.default
        let url = manager.urls(for: .cachesDirectory, in: .userDomainMask).first
        let directory1 = url!.appendingPathComponent("profilePictures")
        let hashPath = directory1.appendingPathComponent("\(profileID).hash").path
        var hash: String = ""
        if manager.fileExists(atPath: hashPath) {
            do {
                try hash = String(contentsOfFile: hashPath, encoding: .utf8)
            } catch {
                print("profilePictureHash error: \(error.localizedDescription)")
            }
        }
        return hash
    }
    
    func profilePicture(profileID: String) -> UIImage {
        var profileIdVar = profileID
        if profileIdVar == CommonAllString.DtrStr {
            return UIImage(named: "DTR-AccentIAppIcon")!
        }
        if let dotRange = profileIdVar.range(of: "+") {
            profileIdVar.removeSubrange(dotRange.lowerBound..<profileIdVar.endIndex)
        }
        let manager = FileManager.default
        let url = manager.urls(for: .cachesDirectory, in: .userDomainMask).first
        let directory1 = url!.appendingPathComponent("profilePictures")
        let imagePath = directory1.appendingPathComponent("\(profileIdVar).png").path
        if manager.fileExists(atPath: imagePath) {
            if let imageFromPath = UIImage(contentsOfFile: imagePath) {
                return imageFromPath
            }
        }
        let name = profileInfo(profileID: profileIdVar).name
        let letters = name.lowercased().filter {("a"..."z").contains($0)}
        var symbolname = "person.circle"
        if let first = letters.first {
            symbolname = String(first)
        }
        if let image = UIImage(systemName: "\(symbolname).circle.fill") {
            return image
        }
        return UIImage(systemName: "person.crop.circle")!
    }
    
    func profilePictureWithName(profileID: String ,inLarge:Bool) -> UIImage {
        let name = profileInfo(profileID: profileID).name
        let letters = name.lowercased().filter {("a"..."z").contains($0)}
        var symbolname = "person.circle"
        // DTR-FriendOfFriend
        if let first = letters.first {
            symbolname = String(first)
        }
        
        if let image = UIImage(systemName: "\(symbolname).circle.fill") {
            // let image = // your image
            let scaledImageSize = CGSize(width: 350, height: (350 * (99.5/100)))
            
            let renderer = UIGraphicsImageRenderer(size: scaledImageSize)
            let scaledImage = renderer.image { _ in
                image.draw(in: CGRect(origin: .zero, size: scaledImageSize))
            }
            if inLarge {
                return scaledImage
            }else{
                return image
            }
        }
        return UIImage(systemName: "person.crop.circle")!
    }
    
    
    func profileInfo(profileID: String) -> DtrProfile {
        guard let user = Auth.auth().currentUser else {
            print("ERROR1: unable to update profile information for user")
            return DtrProfile(id: "SETUP")
        }
        if profileID == user.uid {
            return self.profile
        }
        if profileID == CommonAllString.DtrStr {
            var system = DtrProfile(id: CommonAllString.DtrStr)
            system.cityState = "Metro Detroit"
            system.name = CommonAllString.DtrStr
            system.description = "Are you down to ride?"
            return system
        }
        return self.profileDirectory[dynamicMember: profileID]
    }
    
    func saveProfilePictureIfNeeded(profileID: String, localHash: String) {
        let manager = FileManager.default
        let url = manager.urls(for: .cachesDirectory, in: .userDomainMask).first
        let directory1 = url!.appendingPathComponent("profilePictures")
        let directoryPath = url!.appendingPathComponent("profilePictures").path
        let imageFile = directory1.appendingPathComponent("\(profileID).png")
        let hashFile = directory1.appendingPathComponent("\(profileID).hash")
        
        var subfolderExists = false
        var isDirectory: ObjCBool = false
        
        let profileRef = Storage.storage().reference().child("dtr/profiles/\(profileID).png")
        
        profileRef.getMetadata() { metadataData, metadataError in
            if metadataError != nil {
                // print("profilePicture metadata error: \(error.localizedDescription)")
                return
            }
            
            if let metadata = metadataData {
                let serverHash = asString(metadata.md5Hash, defaultValue: "not")
                
                if serverHash == localHash {
                    return
                }
                
                // print("••••• downloading profile picture for \(profileID)")
                
                profileRef.getData(maxSize: metadata.size) { imageData, imageError in
                    if let error = imageError {
                        print("profilePicture imageData error: \(error.localizedDescription)")
                        return
                    }
                    
                    do {
                        if manager.fileExists(atPath: directoryPath, isDirectory: &isDirectory) {
                            if isDirectory.boolValue {
                                subfolderExists = true
                            }
                        }
                        if !subfolderExists {
                            try manager.createDirectory(at: directory1, withIntermediateDirectories: true, attributes: nil)
                        }
                        try imageData?.write(to: imageFile, options: .atomic)
                        try serverHash.write(to: hashFile, atomically: true, encoding: .utf8)
                        // touch the profile to cause subscribers to read again (hack)
                        var updatedProfile = self.profile
                        updatedProfile.pictureHash = serverHash
                        self.profile = updatedProfile
                        self.profileImageUploading = false
                    } catch {
                        print("profilePicture exception: \(error.localizedDescription)")
                    }
                }
            }
        }
    }
    
    
    static let sharedInstance: DtrData = {
        let instance = DtrData()
        // in case we need to setup up
        return instance
    }()
    
    var didChange = PassthroughSubject<DtrData, Never>()
    
    func oldHistoryRides(completion: @escaping ([DtrRide]) -> Void) {
        var tempData = [DtrRide]()
        let startDateCode = dateCode(offset:6)
        db.collection("rides").order(by: "dateCode", descending: true).limit(to: 200)
            .whereField("dateCode", isLessThanOrEqualTo: startDateCode)
            .whereField("leader", isEqualTo: self.profileID)
            .getDocuments { (querySnapshot, error) in
                if let error = error {
                    print("suggested rides listener error: \(error.localizedDescription)")
                    return
                }
                if let snapshot = querySnapshot {
                    for document in snapshot.documents {
                        let ride = DtrRide(data: document.data())
                        if ride.summary.lowercased() != "Down to Ride".lowercased() {
                            if tempData.count <= 24 {
                                tempData.append(ride)
                            }
                        }
                    }
                    completion(tempData)
                }
            }
    }
    
    func relationship(profileID: String) -> Relationship {
        var relationship: Relationship = .none
        
        let invitations = sentNotifications.filter{$0.target == profileID && $0.type == .friendInvite}.map{$0.target}
        let requests = notifications.filter{$0.source == profileID && $0.type == .friendInvite}.map{$0.source}
        
        guard let currentUser = Auth.auth().currentUser else {
            // print("ERROR: unable to determine my id")
            return relationship
        }
        
        if profileID == currentUser.uid {
            relationship = .me
        }
        
        if profile.followed.contains(profileID) {
            relationship = .followed
        }
        
        if profile.following.contains(profileID) {
            relationship = .following
        }
        
        if profile.friends.contains(profileID) {
            relationship = .acceptedFriend
        }
        
        if invitations.contains(profileID) {
            relationship = .invitedFriend
            if profile.following.contains(profileID) {
                relationship = .follwoingWithInvite
            }
        }
        
        if requests.contains(profileID) {
            relationship = .requestedFriend
        }
        return relationship
    }
    
    func listenIncomingNotifications() -> ListenerRegistration {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        
        let listener = db.collection("notifications")
            .whereField("target", isEqualTo: self.profileID)
            .addSnapshotListener { (querySnapshot, err) in
                guard err == nil else {
                    print("messages listener error: \(err?.localizedDescription ?? "UNKNOWN")")
                    return
                }
                var notifications = [AppNotification]()
                //var expiredNotifications = [String]()
                //let noExpiration = Date(timeIntervalSince1970: 0)
                let currentTimeStamp = Date().timeIntervalSince1970
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    
                    for document in documents {
                        let notification = AppNotification(data: document.data())
                        if let expiration =  notification.expires {
                            //print("Notification Timestamp = ",expiration,"currentTimeStamp Timestamp = ",currentTimeStamp,"isExpired = ",expiration > currentTimeStamp)
                            if notification.type == .rideUpdate || notification.type == .rideInvite || notification.type == .rideTime || notification.type == .rideLocation || notification.type == .rideCancelled {
                                if expiration > currentTimeStamp {
                                    notifications.append(notification)
                                }else if expiration == 0 {
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "yyyyMMdd"
                                    let ntfifCatDate = dateFormatter.date(from: notification.payloadID)
                                    if ntfifCatDate != nil {
                                        let days = ntfifCatDate!.days(from: Date())
                                        if days >= 0 {
                                            notifications.append(notification)
                                        }
                                    }
                                }
                            } else {
                                notifications.append(notification)
                            }
                        }
                        //if let expiration = notification.expires {
                        //print("Notification: \(document.documentID), Expires: \(formatter.string(from: expiration))")
                        //if expiration == noExpiration || expiration > Date() {
                        //notifications.append(notification)
                        //} else {
                        //expiredNotifications.append(document.documentID)
                        //}
                        //} else {
                        //notifications.append(notification)
                        //}
                    }
                }
                self.notifications = notifications
                //self.cleanupNotifications(ids: expiredNotifications)
            }
        return listener
    }
    
    func listenSentNotifications() -> ListenerRegistration {
        let listener = db.collection("notifications")
            .whereField("source", isEqualTo: self.profileID)
            .addSnapshotListener { (querySnapshot, err) in
                guard err == nil else {
                    print("messages listener error: \(err?.localizedDescription ?? "UNKNOWN")")
                    return
                }
                
                var notifications = [AppNotification]()
                var rideInviteNotifications = [AppNotification]()
                let currentTimeStamp = Date().timeIntervalSince1970
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    
                    for document in documents {
                        var notification = AppNotification(data: document.data())
                        // Append only Current and next 6 day ride invite notification.
                        if  notification.type == .rideInvite {
                            if let expiration =  notification.expires {
                                if expiration > currentTimeStamp {
                                    notification.id = document.documentID
                                    rideInviteNotifications.append(notification)
                                }else if expiration == 0 {
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "yyyyMMdd"
                                    let ntfifCatDate = dateFormatter.date(from: notification.payloadID)
                                    if ntfifCatDate != nil {
                                        let days = ntfifCatDate!.days(from: Date())
                                        if days >= 0 {
                                            notification.id = document.documentID
                                            rideInviteNotifications.append(notification)
                                        }
                                    }
                                }
                            }
                        }
                        switch notification.type {
                        case .rideUpdate, .rideCancelled, .friendMessage:
                            _ = "these should be ommitted from the sentNotifications"
                        default:
                            notification.id = document.documentID
                            notifications.append(notification)
                        }
                    }
                }
                self.sentNotifications = notifications
                self.sentRideInviteNotifications = rideInviteNotifications
            }
        return listener
    }
    
    private func cleanupNotifications(ids: [String]) {
        let batch = db.batch()
        for id in ids {
            print("cleaning up \(id)")
            batch.deleteDocument(self.db.collection("notifications").document(id))
        }
        batch.commit()
    }
    
    func updateNotificationStatus(notification: AppNotification,isTypeIndex:Int){
        if isTypeIndex == 0 {
            if notification.id != "" {
                db.collection("notifications").document(notification.id).updateData(["status": "read"]) { (error) in
                }
            }
            
        } else {
            if profile.id != "" {
                db.collection("profiles").document(profile.id).updateData(["lastSignIn":Date().timeIntervalSince1970]) { (error) in
                    UserStore.shared.lastLoginUpdatedTimeStamp = Date().timeIntervalSince1970
                    print("User Stor Time Stamp =",UserStore.shared.lastLoginUpdatedTimeStamp,"Current Time Stamp = ",Date().timeIntervalSince1970)
                }
            }
        }
    }
    
    func createFriendRelationship(source: String, target: String) {
        guard let currentUser = Auth.auth().currentUser else {
            print("ERROR: unable to create friend relationship")
            return
        }
        
        print("\(currentUser.uid) is creating a friend relationship between \(source) and \(target)")
        let batch = db.batch()
        let profiles = db.collection("profiles")
        let sourceDocument = profiles.document(source)
        let targetDocument = profiles.document(target)
        batch.updateData(["friends": FieldValue.arrayUnion([target])], forDocument: sourceDocument)
        batch.updateData(["friends": FieldValue.arrayUnion([source])], forDocument: targetDocument)
        batch.commit { error in
            if let error = error {
                print("createFriendRelationship error: \(error.localizedDescription)")
            }
            self.rebuildFriendsList()
        }
    }
    
    func rebuildFriendsList(completion: @escaping () -> Void = {}) {
        // print("rebuilding friends list")
        guard let user = Auth.auth().currentUser else {
            print("RebuildFriendsList ERROR2: unable to update profile information for user")
            return
        }
        self.statusMessage = "Building friend list"
        var friendsList = [DtrProfile]()
        let myProfile = self.profileInfo(profileID: user.uid)
        // update my friends list
        for friend in myProfile.friends {
            let friendProfile = self.profileInfo(profileID: friend)
            friendsList.append(friendProfile)
        }
        self.friendsProfiles = friendsList.sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
        let dateCodeFormatter = DateFormatter()
        dateCodeFormatter.dateFormat = "yyyyMMdd"
        for offset in 0...6 {
            let dateCode = dateCodeFormatter.string(from: dateFrom(offset: offset))
            self.rideList.forDate[dateCode] = [DtrRide]()
            rebuildRideList(dateCode: dateCode)
        }
        completion()
    }
    
    func rebuildFollowingList(completion: @escaping () -> Void = {}) {
        // print("rebuilding friends list")
        guard let user = Auth.auth().currentUser else {
            print("ERROR3: unable to update profile information for user")
            return
        }
        self.statusMessage = "Building friend list"
        var follwingList = [DtrProfile]()
        let myProfile = self.profileInfo(profileID: user.uid)
        // update my friends list
        for following in myProfile.following {
            let followingProfile = self.profileInfo(profileID: following)
            follwingList.append(followingProfile)
        }
        self.followingProfiles = follwingList.sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
        let dateCodeFormatter = DateFormatter()
        dateCodeFormatter.dateFormat = "yyyyMMdd"
        for offset in 0...6 {
            let dateCode = dateCodeFormatter.string(from: dateFrom(offset: offset))
            self.rideList.forDate[dateCode] = [DtrRide]()
            rebuildRideList(dateCode: dateCode)
        }
        completion()
    }
    
    func rebuildFollowedList(completion: @escaping () -> Void = {}) {
        // print("rebuilding friends list")
        guard let user = Auth.auth().currentUser else {
            print("ERROR4: unable to update profile information for user")
            return
        }
        self.statusMessage = "Building friend list"
        var follwedList = [DtrProfile]()
        let myProfile = self.profileInfo(profileID: user.uid)
        // update my friends list
        for followed in myProfile.followed {
            let followingProfile = self.profileInfo(profileID: followed)
            follwedList.append(followingProfile)
        }
        self.follwedProfiles = follwedList.sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
        let dateCodeFormatter = DateFormatter()
        dateCodeFormatter.dateFormat = "yyyyMMdd"
        for offset in 0...6 {
            let dateCode = dateCodeFormatter.string(from: dateFrom(offset: offset))
            self.rideList.forDate[dateCode] = [DtrRide]()
            rebuildRideList(dateCode: dateCode)
        }
        completion()
    }
    
    
    
    private func rebuildRideList(dateCode: String) {
        db.collection("rides")
            .whereField("dateCode", isEqualTo: dateCode)
            .getDocuments { (querySnapshot, err) in
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    for document in documents {
                        var rides = self.rideList[dynamicMember: dateCode]
                        let ride = DtrRide(data: document.data())
                        if self.shouldAddRide(rides: rides, ride: ride) {
                            rides.append(ride)
                        }
                        //print("rides on \(dateCode): \(rides.count)")
                        self.rideList.forDate[dateCode] = rides
                    }
                }
            }
    }
    
    func followerRelationship(profileID: String) -> Relationship {
        var relationship: Relationship = .none
        
        guard let currentUser = Auth.auth().currentUser else {
            //print("ERROR: unable to determine my id")
            return relationship
        }
        
        if profileID == currentUser.uid {
            relationship = .me
        }
        
        if profile.followed.contains(profileID) {
            relationship = .followed
        }
        
        if profile.following.contains(profileID) {
            relationship = .following
        }
        return relationship
    }
    
    func verifyPhone(verificationPhone: String, completion: @escaping (String?, Error?) -> Void = { verificationID, error in }) {
        PhoneAuthProvider.provider().verifyPhoneNumber(verificationPhone, uiDelegate: nil) { verificationID, error in
            if let error = error {
                print("PhoneAuthProvider error: \(error.localizedDescription)")
            }
            completion(verificationID, error)
        }
    }
    
    func signIn(id: String, code: String, completion: @escaping (String?, Error?,Bool) -> Void) {
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: id, verificationCode: code)
        Auth.auth().signIn(with: credential) { authResult, error in
            if let error = error {
                completion("", error,false)
            }
            if let result = authResult {
                print("Check User id Valid or not = ",result.user.uid)
                self.checkProfileNeedToSetupOrNot(userID:  result.user.uid) { (response) in
                    if response == false {
                        completion(result.user.uid, error,false)
                    }else{
                        Analytics.logEvent("verification_completed", parameters: nil)
                        completion(result.user.uid, error,true)
                    }
                }
            }
        }
    }
    
    func checkProfileNeedToSetupOrNot(userID:String,completion: @escaping (Bool) -> Void)  {
        db.collection("profiles")
            .whereField("id", isEqualTo: userID)
            .getDocuments { [self] (querySnapshot, error) in
                if let error = error {
                    print("profile listener error: \(error.localizedDescription)")
                    return
                }
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    if let document = documents.first {
                        //print("profile data: \(document.data().prettyFormatted())")
                        completion(false)
                    } else {
                        //print("No profile document exists for \(self.profileID)") // Rs
                        //self.statusLogin = .signedIn
                        completion(true)
                    }
                }
            }
    }
    
    
    func update(profileImage: UIImage,completion: @escaping (String)-> Void) {
        guard let currentUser = Auth.auth().currentUser else {
            print("ERROR: unable to update profile image for user")
            return
        }
        profileImageUploading = true
        if let data = profileImage.jpegData(compressionQuality: 0.5)! as? Data {
            let imageRef = Storage.storage().reference().child("dtr/profiles/\(currentUser.uid).png")
            
            let md = StorageMetadata()
            md.contentType = "image/png"
            
            _ = imageRef.putData(data, metadata: md, completion: { metadata, error in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                //if let metadata = metadata {
                //print("Image Uploaded = ",metadata)
                //}
                imageRef.downloadURL() { url, error in
                    if url?.absoluteString != "" && url?.absoluteString != nil {
                        completion(url!.absoluteString)
                        self.db.collection("profiles").document(currentUser.uid).updateData(["profileImage": url!.absoluteString]) { (error) in
                            self.profileImageUploading = false
                        }
                    }
                }
                
                //let localHash = self.profilePictureHash(profileID: currentUser.uid)
                //self.saveProfilePictureIfNeeded(profileID: currentUser.uid, localHash: localHash)
                
                // set a URL to the photo into the firebase auth record
                // so that it will show up on the web
                //self.updatePhotoURL(profileID: currentUser.uid)
            })
        }
    }
    
    func updateRideImageOnServer(RideImage: UIImage,profileId:String,rideId:String,completion: @escaping (String)-> Void) {
        guard let _ = Auth.auth().currentUser else {
            print("ERROR: unable to update profile image for user")
            completion("")
            return
        }
        profileImageUploading = true
        if let data = RideImage.jpegData(compressionQuality: 0.5)! as? Data {
            let md = StorageMetadata()
            md.contentType = "image/png"
            let timestamp: Double = Date().timeIntervalSince1970.rounded()
            print("Curret = ",timestamp)
            let imageRef = Storage.storage().reference().child("dtr/ridesImages/\(timestamp)+\(profileId).png")
            print("imageRef = ",imageRef)
            _ = imageRef.putData(data, metadata: md, completion: { metadata, error in
                if let error = error {
                    print(error.localizedDescription)
                    completion("")
                    self.profileImageUploading = false
                    return
                }
                imageRef.downloadURL() { url, error in
                    if url?.absoluteString != "" && url?.absoluteString != nil {
                        print("download URL: \(url!.absoluteString)")
                        let addNewImageToImageArray = self.db.collection("profiles").document(profileId)
                        addNewImageToImageArray.updateData(["ridesImages": FieldValue.arrayUnion([url!.absoluteString])]) { error in
                            if error == nil {
                                //                                self.updateTheRideImageUrl(rideImageUrl: url!.absoluteString, rideId: rideId) { responeUpload in
                                //                                    if error == nil {
                                self.profileImageUploading = false
                                completion(url!.absoluteString)
                                
                                //                                    }
                                //                                }
                            } else {
                                self.profileImageUploading = false
                            }
                        }
                    } else {
                        self.profileImageUploading = false
                    }
                }
            })
        }
    }
    
    
    func updateGroupImageOnServer(GroupImage: UIImage,profileId:String,completion: @escaping (String)-> Void) {
        guard let _ = Auth.auth().currentUser else {
            print("ERROR: unable to update profile image for user")
            completion("")
            return
        }
        profileImageUploading = true
        if let data = GroupImage.jpegData(compressionQuality: 0.5)! as? Data {
            let md = StorageMetadata()
            md.contentType = "image/png"
            let timestamp: Double = Date().timeIntervalSince1970.rounded()
            let imageRef = Storage.storage().reference().child("dtr/groupImages/\(timestamp)+\(profileId).png")
            _ = imageRef.putData(data, metadata: md, completion: { metadata, error in
                if let error = error {
                    print(error.localizedDescription)
                    completion("")
                    return
                }
                imageRef.downloadURL() { url, error in
                    if url?.absoluteString != "" && url?.absoluteString != nil {
                        print("download URL: \(url!.absoluteString)")
                        self.profileImageUploading = false
                        completion(url!.absoluteString)
                    }
                }
            })
        }
    }
    
    
    
    func updateTheRideImageUrl(rideImageUrl:String,rideId:String,completion: @escaping (Bool)-> Void){
        profileImageUploading = true
        self.db.collection("rides").document(rideId).updateData(["rideCoverImage": rideImageUrl]) { (error) in
            if error == nil {
                self.profileImageUploading = false
                completion(true)
            }else{
                self.profileImageUploading = false
                completion(false)
            }
        }
    }
    
    func removeFriendItem(tbleName:String,arrayUpdate:String,userId:String,completion: @escaping (Error?)-> Void) {
        self.db.collection(tbleName).document(profile.id).updateData([arrayUpdate: FieldValue.arrayRemove([userId])]) { response in
            print("Response Delete User id = ",response as Any)
            completion(response)
        }
    }
    
    func clearNotification(_ notification: AppNotification) {
        Analytics.logEvent(AnalyticsEvent.notificationCleared.rawValue, parameters: ["notificationID": notification.id, "type": notification.type.data])
        db.collection("notifications").document(notification.id).delete() { error in
            if let error = error {
                print("error clearing notification document at \(notification.id): \(error.localizedDescription)")
            }
        }
    }
    
    //Download Image and save to gallery
    private func updatePhotoURL(profileID: String) {
        let imageRef = Storage.storage().reference().child("dtr/profiles/\(profileID).png")
        imageRef.downloadURL() { url, error in
            if let error = error {
                print("url error: \(error.localizedDescription)")
                return
            }
            if let download = url {
                print("download URL: \(download.absoluteString)")
                if let user = Auth.auth().currentUser {
                    let changeRequest = user.createProfileChangeRequest()
                    changeRequest.photoURL = download
                    changeRequest.commitChanges { error in
                        guard error == nil else {
                            print("firebase profile update error: \(error?.localizedDescription ?? "UNKNOWN")")
                            return
                        }
                        //print("firebase profile picture URL updated")
                    }
                }
            }
        }
    }
    
    func update(profile: DtrProfile,isLoginProcess:Bool) -> Bool{
        //print("writing to the profile: \(profile.data.prettyFormatted())")
        guard let currentUser = Auth.auth().currentUser else {
            print("ERROR5: unable to update profile information for user")
            return false
        }
        profile.write(self.db, propagate: false) { profile, error in
            if let error = error {
                print("error updating profile: \(error.localizedDescription)")
                return
            }
            if currentUser.displayName != profile.name {
                let changeRequest = currentUser.createProfileChangeRequest()
                changeRequest.displayName = profile.name
                //TODO: create a link to the profile picture
                changeRequest.commitChanges { error in
                    guard error == nil else {
                        print("firebase profile update error: \(error?.localizedDescription ?? "UNKNOWN")")
                        return
                    }
                }
            }
        }
        return true
    }
    
    func signOut(completion: @escaping (Bool)-> Void) {
        do {
            updatePushToken(token: "")
            try Auth.auth().signOut()
            self.getAllProfile = self.listenDirectory()
            self.getAllGroups = self.listenGroupDirectory()
            self.getAllbadges = self.listenBadgeDirectory()
            self.getAllTags = self.listenTagsDirectory()
            self.profileListener?.remove()
            self.myRidesListener?.remove()
            self.suggestedRidesListener?.remove()
            self.weeklyRidesListener?.remove()
            UserStore.shared.removeAllkeys()
            self.sendNotification?.remove()
            self.announcementsNotifiction?.remove()
            UserStore.shared.UpdateAndRestLocalStoreWhenStartingApp() //change
            self.incomincomingNotifiction?.remove()
            self.minimumBuildListener?.remove()
            self.staffRoleListener?.remove()
            // reset all user-specific data
            self.profileID = ""
            self.statusLogin = .signedOut
            self.profile = DtrProfile()
            self.myRidesByDate = DtrRideByDate()
            self.suggestedRidesByDate = DtrRidesByDate()
            self.allRidesForWeek = DtrRidesByDate()
            self.allUserRidesForWeek = DtrRidesByDate()
            self.staffRole = StaffRole()
            UIApplication.shared.applicationIconBadgeNumber = 0
            completion(true)
            return
        } catch {
            completion(false)
        }
    }
    
    // MARK: Notifications
    func sendNotification(notification: AppNotification) {
        guard let user = Auth.auth().currentUser else {
            print("invalid user!")
            return
        }
        db.collection("notifications")
            .whereField("source", isEqualTo: user.uid)
            .whereField("target", isEqualTo: notification.target)
            .whereField("type", isEqualTo: notification.type.data)
            .whereField("payloadID", isEqualTo: notification.payloadID)
            .getDocuments { (querySnapshot, err) in
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    //print("removing \(documents.count) notifications...")
                    for document in documents {
                        self.db.collection("notifications").document(document.documentID).delete()
                    }
                    // print("actually sending the notification...")
                    self.addNotification(notification: notification)
                }
            }
    }
    
    private func addNotification(notification: AppNotification) {
        let documentReference: DocumentReference? = db.collection("notifications").document()
        var notificationToSend = notification
        if let ref = documentReference {
            notificationToSend.id = ref.documentID
            ref.setData(notificationToSend.data) { error in
                if let error = error {
                    print("ERROR adding notification: \(error.localizedDescription)")
                    return
                }
                let profile = self.profileDirectory[dynamicMember: notificationToSend.target]
                if let token = profile.pushToken {
                    notificationToSend.push(token: token)
                }
            }
        } else {
            print("sendNotification error - failed to create document!")
        }
    }
    
    
    func sendPushNotificationOnly(notification: AppNotification){
        let notificationToSend = notification
        let profile = self.profileDirectory[dynamicMember: notificationToSend.target]
        if let token = profile.pushToken {
            notificationToSend.push(token: token)
        }
    }
    
    func updatePlan(for profileID: String, on dateCode: String) {
        var plan = self.plansMessage[dynamicMember: dateCode]
        db.collection("rides")
            .whereField("dateCode", isEqualTo: dateCode)
            .whereField("riders", arrayContains: profileID)
            .getDocuments { querySnapshot, error in
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    var ride: DtrRide? = nil
                    if let document = documents.last {
                        ride = DtrRide(data: document.data())
                    }
                    plan.ride = ride
                    plan.update { plan, error in
                        if let error = error {
                            print("plan update error: \(error.localizedDescription)")
                            return
                        }
                        self.plansMessage.forDate[dateCode] = plan
                        self.rebuildRideList(dateCode: dateCode)
                    }
                }
            }
    }
    
    func createFriendLink(completion: @escaping (String) -> Void = { linkCode in } ) {
        guard let user = Auth.auth().currentUser else {
            print("invalid user!")
            return
        }
        let documentReference: DocumentReference? = self.db.collection("links").document()
        if let ref = documentReference {
            ref.setData(["id": user.uid,"name": self.profile.name,"description": self.profile.description,"cityState": self.profile.cityState]) { error in
                if let error = error {
                    print("error creating friend link: \(error.localizedDescription)")
                    return
                }
                self.db.collection("profiles").document(user.uid).updateData(["linkCode": ref.documentID]) { error in
                    if let error = error {
                        print("ERROR creating linkCode for \(user.uid): \(error.localizedDescription)")
                        return
                    }
                    Analytics.logEvent(AnalyticsEvent.friendLinkCreate.rawValue, parameters: ["source": ref.documentID])
                    completion(ref.documentID)
                }
            }
        }
    }
    
    func processRideLink(type:String,source: String, rideID: String) {
        guard let user = Auth.auth().currentUser else {
            print("invalid user Ride!")
            if type != "" && source != "" && rideID != "" {
                let result = UserStore.shared.setDyanamicLinkLocal(type: type, source: source, data: rideID, isSetValue: true)
                print(result.0)
                print(result.1)
                print(result.2)
            }
            return
        }
        
        db.collection("rides").document(rideID).getDocument { documentSnapshot, error in
            if let snapshot = documentSnapshot {
                if let data = snapshot.data() {
                    self.dyanamicLinkRide = DtrRide.default
                    let ride = DtrRide(data: data)
                    self.dyanamicLinkRide = DtrRide(data: data)
                    Analytics.logEvent(AnalyticsEvent.rideLinkRecieve.rawValue, parameters: ["source": source])
                    let expires = toDate(timeString: "23:59", on: ride.dateCode)
                    let notification = AppNotification(source: source, target: user.uid, type: .rideInvite, payloadID: ride.dateCode, message: ride.summary, expires: expires)
                    self.receivedNotification = notification
                }
            }
        }
    }
    
    func processFriendLink(type:String,source: String) {
        guard let user = Auth.auth().currentUser else {
            print("invalid user!")
            if type != "" && source != "" {
                let _ = UserStore.shared.setDyanamicLinkLocal(type: type, source: source, data: "", isSetValue: true)
            }
            return
        }
        Analytics.logEvent(AnalyticsEvent.friendLinkReceive.rawValue, parameters: ["source": source])
        db.collection("links").document(source).getDocument { documentSnapshot, error in
            if let snapshot = documentSnapshot {
                if let data = snapshot.data() {
                    if let profileID = asStringOptional(data["id"]) {
                        // if !self.profile.friends.contains(profileID) {
                        let message = AppNotification(source: profileID, target: user.uid, type: .friendInvite, payloadID: "\(profileID)-\(user.uid)", message: "Friend Profile Link", expires: Date().thirtyDaysFromNow)
                        self.receivedNotification = message
                        // }
                    }else {
                        print("No Data3")
                    }
                }else{
                    print("No Data1")
                }
            }
        }
    }
    
    func processCodeLink(source: String) {
    }
    
    func processGroupLink(type:String,source: String, groupID: String) {
        guard let user = Auth.auth().currentUser else {
            print("invalid user Ride!")
            if type != "" && source != "" && groupID != "" {
                let result = UserStore.shared.setDyanamicLinkLocal(type: type, source: source, data: groupID, isSetValue: true)
                print(result.0)
                print(result.1)
                print(result.2)
            }
            return
        }
        
        db.collection("groups").document(groupID).getDocument { documentSnapshot, error in
            if let snapshot = documentSnapshot {
                if let data = snapshot.data() {
                    self.dyanamicLinkGroup = DtrGroup.default
                    let group = DtrGroup(data: data)
                    self.dyanamicLinkGroup = DtrGroup(data: data)
                    //let expires = toDate(timeString: "23:59", on: ride.dateCode)
                    let notification = AppNotification(source: source, target: user.uid, type: .message, payloadID: group.id, message:"Group_Detail", expires: Date())
                    self.receivedNotification = notification
                }
            }
        }
    }
}

