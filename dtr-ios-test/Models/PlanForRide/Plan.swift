//
//  File.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/02/21.
//

 import Foundation
import Firebase

typealias PlanCompletion = (Plan, DtrError?) -> Void

struct Plan {
    var dateCode: String
    var status: RiderStatus = .notRiding
    var ride: DtrRide?
    
    // data is not persisted, but it handy for debug prints
    var data: DataDict {
        [
            "dateCode": self.dateCode,
            "status": self.status.data
        ]
    }
    
    init(dateCode: String)    {
        self.dateCode = dateCode
    }
    
    init(data: DataDict){
        self.dateCode = asString(data["dateCode"], defaultValue: "19700101")
        
        let statusRaw = asInt(data["status"], defaultValue: RiderStatus.notRiding.data)
        
        if let status = RiderStatus(rawValue: statusRaw) {
            self.status = status
        }
    }
    
    func update(completion: @escaping PlanCompletion) {
        guard let profileID = Auth.auth().currentUser?.uid else {
            completion(self, DtrError(.authorization))
            return
        }
        
        var plan = self
        
        if let ride = self.ride {
            plan.status = .downToRide   // at least
            
            if ride.leader == profileID {
                if ride.riders.count > 1 {
                    plan.status = .leading
                }
            } else {
                plan.status = .riding
            }
        } else {
            plan.status = .notRiding
        }
        // update the friendsRides here, based on this new status
        completion(plan, nil)
    }
    
    func activate(completion: @escaping PlanCompletion) {
        guard let profileID = Auth.auth().currentUser?.uid else {
            completion(self, DtrError(.authorization))
            return
        }
        
        guard self.status == .notRiding else {
            completion(self, DtrError(.stateTransition))
            return
        }
        
        var plan = self
        //plan.ride = DtrRide(profileID: profileID, dateCode: self.dateCode)
        plan.ride = DtrRide(profileID: profileID, dateCode: self.dateCode, summary: "Down to ride")
        plan.update() { plan, error in
            completion(plan, nil)
        }
    }
    
    func cancel(completion: @escaping PlanCompletion) {
        guard self.status == .downToRide || self.status == .leading else {
            completion(self, DtrError(.stateTransition))
            return
        }
        
        var plan = self
        plan.ride = nil
        
        plan.update { plan, error in
            completion(plan, nil)
        }
    }
}

@dynamicMemberLookup
struct Plans {
    var forDate = [String:Plan]()
    subscript(dynamicMember dateCode: String) -> Plan {
        return forDate[dateCode, default: Plan(dateCode: dateCode)]
    }
}
