//
//  DtrGroup.swift
//  dtr-ios-test
//
//  Created by apple on 26/07/21.
//

import Foundation

@dynamicMemberLookup
struct Groups {
    var forID = [String:DtrGroup]()
    
    subscript(dynamicMember id: String) -> DtrGroup {
        return forID[id, default: DtrGroup(id: id)]
    }
}

struct DtrGroup : Hashable, Codable {
    var id: String = ""
    var name = ""
    var cityState = ""
    var country = ""
    var leader = ""
    var image: String = ""
    var members = [String]()
    var groupcode = ""
    var groupLink = ""
    var details : DtrGroupDetails
    var selectDeselectTitle = ""
    var isExpanded = false
    
    init(id: String = "SETUP") {
        self.id = id
        self.details = DtrGroupDetails()
    }
    
    static func == (lhs: DtrGroup, rhs: DtrGroup) -> Bool {
        lhs.id == rhs.id
    }
    
    static var `default`: DtrGroup {
        var sampleProfile = DtrGroup(id: "Default Profile")
        sampleProfile.name = "Profile Name"
        return sampleProfile
    }
    
    var data:DataDict {
        let result: DataDict = [
            "id": self.id,
            "name": self.name,
            "groupcode": self.groupcode,
            "groupLink": self.groupLink,
            "cityState": self.cityState,
            "country": self.country,
            "members": self.members,
            "leader": self.leader,
            "image":self.image,
            "details": self.details.data,
        ]
        return result
    }
    
    init(data: DataDict) {
        self.id = asString(data["id"], defaultValue: "unknown")
        self.name = asString(data["name"], defaultValue: "New Rider")
        if self.name == "" {
            self.name = "New Rider"
        }
        self.groupcode = asString(data["groupcode"], defaultValue: "")
        self.groupLink = asString(data["groupLink"], defaultValue: "")
        self.cityState = asString(data["cityState"], defaultValue: "")
        self.country = asString(data["country"], defaultValue: "")
        if let details = data["details"] as? DataDict {
            self.details = DtrGroupDetails(data: details)
        } else {
            self.details = DtrGroupDetails()
        }
        if let members = data["members"] as? [String] {
            var tempMembers = [""]
            tempMembers.removeAll()
            for i in 0..<members.count {
                if DtrData.sharedInstance.profileDirectory[dynamicMember:members[i]].name != "" &&  DtrData.sharedInstance.profileDirectory[dynamicMember:members[i]].cityState != "" {
                    tempMembers.append(members[i])
                }
            }
            self.members = tempMembers
        } else {
            self.members = [String]()
        }
        self.leader = asString(data["leader"], defaultValue: "")
        self.image = asString(data["image"], defaultValue: "")
    }
    
    var inviteMessage: String {
        let message = "Join \(name) group on Down To Ride"
        return message
    }
}

struct DtrGroupDetails: Hashable,Codable {
    var LocationLat: Double = 0.0
    var LocationLon: Double = 0.0
    
    var data: DataDict {
        return [
            "LocationLat": self.LocationLat,
            "LocationLon": self.LocationLon,
        ]
    }
    
    init() {}
    
    init(data: DataDict){
        self.LocationLat = asDouble(data["LocationLat"], defaultValue: 0.0)
        self.LocationLon = asDouble(data["LocationLon"], defaultValue: 0.0)
    }
}
