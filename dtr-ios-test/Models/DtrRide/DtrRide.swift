//
//  DtrRide.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 1/10/21.
//

import SwiftUI
import Combine
import Firebase

enum RiderStatus: Int {
    case notRiding, downToRide, riding, leading
    
    var data: Int {
        return self.rawValue
    }
    
    var name: String {
        switch self {
            case .notRiding: return "Not Riding"
            case .downToRide: return "Down to Ride"
            case .riding: return "Riding"
            case .leading: return "Leading"
        }
    }
}

typealias RideCompletion = (DtrRide, DtrError?) -> Void

struct DtrRide: Hashable, Codable {
    var id: String
    var dateCode: String
    var leader: String
    var summary: String
    var details: DtrRideDetails
    var _geoloc: DtrRideLatLongDetails
    var riders: [String] = [String]()
    var visibleTo: [String] = [String]()
    var chat = DtrRideMessages()
    var rideCoverImage: String = ""
    
    // this is not persisted, but it is used to resolve all of the
    // profile information for those participating in the ride
    var riderProfiles: [String:DtrProfile] = [String:DtrProfile]()
    var fromAction: DtrRideFromActionType = .none
    var hasAddCustomPublicTagText: Bool = false
    
    var data:[String:Any] {
        return [
            "id": self.id,
            "dateCode": self.dateCode,
            "leader": self.leader,
            "summary": self.summary,
            "details": self.details.data,
            "_geoloc":self._geoloc.data,
            "riders": self.riders,
            "visibleTo": self.visibleTo,
            "chat": self.chat.data,
            "rideCoverImage":self.rideCoverImage,
        ]
    }
    
    init(profileID: String, dateCode: String, summary: String) {
        self.leader = profileID
        self.dateCode = dateCode
        self.summary = summary
        self.details = DtrRideDetails()
        self._geoloc = DtrRideLatLongDetails()
        self.id = "\(leader)+\(dateCode)"
        
        if let currentUser = Auth.auth().currentUser {
            if self.leader == currentUser.uid {
                self.fromAction = .cancel
                
            } else if self.riders.contains(currentUser.uid) {
                self.fromAction = .leave
                
            } else {
                self.fromAction = .none
            }
        }
        
        self.rideCoverImage = asString(data["rideCoverImage"], defaultValue: "")
    }
    
    init(data: DataDict){
        self.dateCode = asString(data["dateCode"], defaultValue: "19700101")
        self.leader = asString(data["leader"], defaultValue: "LEADER_ID")
        self.summary = asString(data["summary"], defaultValue: "Sample Summary")
        self.rideCoverImage = asString(data["rideCoverImage"], defaultValue: "")
        
        
        if let details = data["details"] as? DataDict {
            self.details = DtrRideDetails(data: details)
        } else {
            self.details = DtrRideDetails()
        }
        
        if let _geoloc = data["_geoloc"] as? DataDict {
            self._geoloc = DtrRideLatLongDetails(data: _geoloc)
        } else {
            self._geoloc = DtrRideLatLongDetails()
        }
        
        if let riders = data["riders"] as? [String] {
            self.riders = riders
        }
        
        if let visibleTo = data["visibleTo"] as? [String] {
            self.visibleTo = visibleTo
        }
        
        if !riders.contains(self.leader) {
            self.riders.append(self.leader)
        }
        
        if let chatData = data["chat"] as? [DataDict] {
            self.chat = DtrRideMessages(data: chatData)
        }
        
        self.id = "\(leader)+\(dateCode)"
        
        if let currentUser = Auth.auth().currentUser {
            if self.leader == currentUser.uid {
                self.fromAction = .cancel
                
            } else if self.riders.contains(currentUser.uid) {
                self.fromAction = .leave
                
            } else {
                self.fromAction = .none
            }
        }
    }

    static func == (lhs: DtrRide, rhs: DtrRide) -> Bool {
        lhs.id == rhs.id && lhs.summary == rhs.summary && lhs.leader == rhs.leader && lhs.details == rhs.details && lhs._geoloc == rhs._geoloc && lhs.riders == rhs.riders && lhs.visibleTo == rhs.visibleTo && lhs.rideCoverImage == rhs.rideCoverImage
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.leader)
        hasher.combine(self.dateCode)
    }
    
    // sample instances which can be helpful for previews
    
    static var `default`: DtrRide {
        return DtrRide(profileID: "RIDER_ID", dateCode: "19700101", summary: "Sample Ride")
    }
    
    static let `sampleRides` = [ DtrRide.default ]
    
    func write(database: Firestore, with: DtrRide? = nil, and: DtrRide? = nil, completion: @escaping RideCompletion) {
        let batch = database.batch()
        let rideDoc = database.collection("rides").document(self.id)
        batch.setData(self.data, forDocument: rideDoc, merge: true)
        
        if let withRide = with {
            let withDoc = database.collection("rides").document(withRide.id)
            batch.setData(withRide.data, forDocument: withDoc, merge: true)
        }
        
        if let andRide = and {
            let andDoc = database.collection("rides").document(andRide.id)
            batch.setData(andRide.data, forDocument: andDoc, merge: true)
        }
        
        batch.commit() { error in
            if let error = error {
                print("ride commit error: \(error.localizedDescription)")
                completion(self, DtrError(.unknown,message: error.localizedDescription))
            }
            print("ride committed")
            completion(self, nil)
        }
    }
    
    func update(message: String, completion: @escaping RideCompletion) {
        guard let profileID = Auth.auth().currentUser?.uid else {
            completion(self, DtrError(.authorization))
            return
        }
        let chatMessage = DtrRideMessage(sender: profileID, message: message)
        var ride = self
        if let last = ride.chat.messages.last {
            if last.message == message && last.sender == profileID {
                ride.chat.messages.remove(at: ride.chat.messages.count - 1)
            }
        }
        ride.chat.add(message: chatMessage)
        completion(ride, nil)
    }
    
    var inviteMessage: String {
        let dayOfWeek = dayName(dateCode: self.dateCode)
        let time = self.details.hasStartTime ? " at \(self.details.startTime)" : ""
        let place = self.details.hasStartPlace ? " in \(self.details.startPlace)" : ""
        let message = "I am \"\(self.summary)\" on \(dayOfWeek)\(time)\(place) - You down to ride?"
        return message
    }
}

struct DtrRideDetails: Hashable,Codable {
    var hasStartTime: Bool = false
    var startTime: String = "Time TBD"
    var hasStartPlace: Bool = false
    var startPlace: String = "Location TBD"
    var startPlaceLat: Double = 0.0
    var startPlaceLon: Double = 0.0
    var skillA: Bool = false
    var skillB: Bool = false
    var skillC: Bool = false
    var skillD: Bool = false
    var skillS: Bool = false
    var notes: String = ""
    var visibility: DtrRideVisibilityState = .friends
    var attributes = [DtrRideAttributeType]()
    var tags: [String] = [String]()
    
    var data: DataDict {
        return [
            "hasStartTime": self.hasStartTime,
            "startTime": self.startTime,
            "startPlace": self.startPlace,
            "skillA": self.skillA,
            "skillB": self.skillB,
            "skillC": self.skillC,
            "skillD": self.skillD,
            "skillS": self.skillS,
            "hasStartPlace": self.hasStartPlace,
            "startPlaceLat": self.startPlaceLat,
            "startPlaceLon": self.startPlaceLon,
            "notes": self.notes,
            "tags":self.tags
        ]
    }
    
    init() {
        // everything as default values
    }
    
    init(data: DataDict) {
        self.hasStartTime = asBool(data["hasStartTime"])
        self.startTime = asString(data["startTime"], defaultValue: "Time TBD")
        self.startTime = startTime.replacingOccurrences(of: ".", with: "")
        self.startTime = startTime.replacingOccurrences(of: "pm", with: "PM")
        self.startTime = startTime.replacingOccurrences(of: "am", with: "AM")
        self.startPlace = asString(data["startPlace"], defaultValue: "Location TBD")
        self.skillA = asBool(data["skillA"])
        self.skillB = asBool(data["skillB"])
        self.skillC = asBool(data["skillC"])
        self.skillD = asBool(data["skillD"])
        self.skillS = asBool(data["skillS"])
        self.hasStartPlace = asBool(data["hasStartPlace"])
        self.startPlaceLat = asDouble(data["startPlaceLat"], defaultValue: 0.0)
        self.startPlaceLon = asDouble(data["startPlaceLon"], defaultValue: 0.0)
        self.notes = asString(data["notes"], defaultValue: "")
        
        if let visibilityValue = asStringOptional(data["visibility"]) {
            if let visibility = DtrRideVisibilityState(rawValue: visibilityValue) {
                self.visibility = visibility
            }
        }
        
        if let attributeValues = data["attributes"] as? [Int] {
            var attributes = [DtrRideAttributeType]()
            for value in attributeValues {
                if let attribute = DtrRideAttributeType(rawValue: value) {
                    attributes.append(attribute)
                }
            }
            self.attributes = attributes
        }
        
        if let tags = data["tags"] as? [String] {
            // Some time return duplicate tags id from backend, so we need to get
            let unique = Array(Set(tags))
            self.tags = unique
        }
    }
    
    func isEmpty() -> Bool {
        !self.hasStartTime
            && !self.hasStartPlace
            && !self.skillA
            && !self.skillB
            && !self.skillC
            && !self.skillD
            && !self.skillS
            && self.notes == ""
    }
    
    static func == (lhs: DtrRideDetails, rhs: DtrRideDetails) -> Bool {
        lhs.hasStartTime == rhs.hasStartTime
            && lhs.startTime == rhs.startTime
            && lhs.hasStartPlace == rhs.hasStartPlace
            && lhs.startPlace == rhs.startPlace
            && lhs.startPlaceLat == rhs.startPlaceLat
            && lhs.startPlaceLon == rhs.startPlaceLon
            && lhs.skillA == rhs.skillA
            && lhs.skillB == rhs.skillB
            && lhs.skillC == rhs.skillC
            && lhs.skillD == rhs.skillD
            && lhs.skillS == rhs.skillS
            && lhs.notes == rhs.notes && lhs.visibility == rhs.visibility && lhs.tags == rhs.tags
    }
}

struct DtrRideMessages: Codable {
    var messages: [DtrRideMessage]
    
    var data:[DataDict] {
        var result = [DataDict]()
        
        for item in messages {
            result.append(item.data)
        }
        
        return result
    }
    
    init() {
        self.messages = [DtrRideMessage]()
    }
    
    init(data: [DataDict]) {
        self.messages = [DtrRideMessage]()
        
        for item in data {
            self.messages.append(DtrRideMessage(data: item))
        }
    }
    
    mutating func add(message: DtrRideMessage) {
        self.messages.append(message)
    }
}

struct DtrRideMessage: Codable {
    var sender: String
    var message: String
    var timestamp: Double = Date().timeIntervalSince1970
    
    var data:DataDict {
        return [
            "message": message,
            "sender": sender,
            "timestamp": timestamp
        ]
    }
    
    var time: String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.timeZone = NSTimeZone.local
        
        let date = Date(timeIntervalSince1970: self.timestamp)
        
        if Calendar.current.isDateInToday(date) {
            dateFormatter.dateStyle = .none
            return dateFormatter.string(from: date)
        }
        
        dateFormatter.dateStyle = .short
        return dateFormatter.string(from: date)
    }
    
    init(sender: String, message: String) {
        self.message = message
        self.sender = sender
    }
    
    init(data: DataDict) {
        self.message = asString(data["message"])
        self.sender = asString(data["sender"])
        self.timestamp = asDouble(data["timestamp"], defaultValue: Date().timeIntervalSince1970)
    }
}

enum DtrRideVisibilityState: String, Codable {
    case friends
    case followers
    case `public`
    
    var data: String {
        return self.rawValue
    }
    
    var name: String {
        switch self {
            case .friends: return "Friends of friends"
            case .followers: return "Friends of friends & followers"
            case .public: return "Public Ride"
        }
    }
    
    var description: String {
        switch self {
            case .friends: return "Anyone that is friends with someone on your ride can see and join this ride."
            case .followers: return "Your ride is available to all of your friends and followers to see and join."
            case .public: return "Your ride is avialble to all DTR users to see and join."
        }
    }
}

enum DtrRideAttributeType: Int, CaseIterable, Identifiable, Codable {
    case womenOnly
    case ebikeFriendly
    case firstTimeGroupRiderFriendly
    case drop
    case noDrop
    case slowRoll
    case weeklyRide
    case lightsRequired
    
    var data: Int { return self.rawValue }
    var id: Int { data }
    
    var name: String {
        switch self {
            case .womenOnly: return "Women Only"
            case .ebikeFriendly: return "eBike Friendly"
            case .firstTimeGroupRiderFriendly: return "First Time Group Rider Friendly"
            case .drop: return "Drop"
            case .noDrop: return "No-Drop"
            case .slowRoll: return "Slow Roll"
            case .weeklyRide: return "Weekly Ride"
            case .lightsRequired: return "Lights Required"
        }
    }
}

struct DtrRideLatLongDetails: Hashable,Codable {
    var lat: Double = 0.0
    var lng: Double = 0.0
    
    var data: DataDict {
        return [
            "lat": self.lat,
            "lng": self.lng,
        ]
    }
    
    init() {}
    
    init(data: DataDict){
        self.lat = asDouble(data["lat"], defaultValue: 0.0)
        self.lng = asDouble(data["lng"], defaultValue: 0.0)
    }
}


@dynamicMemberLookup
struct RideList {
    var forDate = [String:[DtrRide]]()
    subscript(dynamicMember dateCode: String) -> [DtrRide] {
        return forDate[dateCode, default: [DtrRide]()]
    }
}

struct DtrRideTags: Codable, Hashable {
    
    let description : String
    let iconFile : String
    let id : String
    let name : String
    let sortOrder : Int
    let status : Int
    let visibleTo : [String]
    
    init(data: DataDict){
        self.description = asString(data["description"],defaultValue: "")
        self.iconFile = asString(data["iconFile"],defaultValue: "")
        self.id = asString(data["id"],defaultValue: "")
        self.name = asString(data["name"],defaultValue: "")
        self.sortOrder = asInt(data["sortOrder"], defaultValue: 0)
        self.status = asInt(data["status"], defaultValue: 0)
        self.visibleTo = data["visibleTo"] as? [String] ?? []
    }
}
