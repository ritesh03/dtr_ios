//
//  DtrProfile.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 1/10/21.
//

import SwiftUI
import Combine
import Firebase


@dynamicMemberLookup
struct Profiles {
    var forID = [String:DtrProfile]()
    
    subscript(dynamicMember id: String) -> DtrProfile {
        return forID[id, default: DtrProfile(id: id)]
    }
}

enum Relationship: String {
    case none, me, acceptedFriend, invitedFriend, requestedFriend, blockedFriend,followed,following,follwoingWithInvite
}

typealias ProfileCompletion = (DtrProfile, Error?) -> Void

struct DtrProfile : Hashable, Codable {
    var id: String = "SETUP"
    var name = ""
    var description = ""
    var cityState = ""
    var country = ""
    var activated = Date()
    var friends = [String]()
    var ridesImages = [String]()
    var createdGroups = [String]()
    var joinedgroups = [String]()
    var userLon: Double = 0.0
    var userLat: Double = 0.0
    var lastSignIn: Double = 0.0
    
    var phone: String?
    var email: String?
    var pushToken: String?
    var verificationDeclined = false
    var defaultRideVisibility = ""
    
    var defaultStatus = "Down to Ride"
    var linkCode = ""
    var tags = [String]()
    var platform = "iOS"
    var pictureHash: String = ""
    var profileImage: String = ""
    var badge: String = ""
    
    
    
    // not persisted, but available in memory
    var friendProfiles = Dictionary<String,DtrProfile>()
    var upcomingRides = Dictionary<String,DtrRide>()
    
    
    // Rs
    var following = [String]()
    var followed = [String]()
    
    var followdProfiles = Dictionary<String,DtrProfile>()
    var followingProfiles = Dictionary<String,DtrProfile>()
    var groupMembersProfiles = Dictionary<String,DtrProfile>()
    
    var isFollower = false
    
    init(id: String = "SETUP") {
        self.id = id
    }
    
    static func == (lhs: DtrProfile, rhs: DtrProfile) -> Bool {
        lhs.id == rhs.id
    }
    
    static var `default`: DtrProfile {
        var sampleProfile = DtrProfile(id: "Default Profile")
        sampleProfile.name = "Profile Name"
        sampleProfile.description = "Testing"
        return sampleProfile
    }
    
    var data:DataDict {
        var result: DataDict = [
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "defaultRideVisibility": self.defaultRideVisibility,
            "cityState": self.cityState,
            "country": self.country,
            "activated": self.activated.timeIntervalSince1970,
            "friends": self.friends,
            "ridesImages":self.ridesImages,
            "createdGroups":self.createdGroups,
            "joinedgroups":self.joinedgroups,
            "following":self.following,
            "followed":self.followed,
            "verificationDeclined": self.verificationDeclined,
            "defaultStatus": self.defaultStatus,
            "profileImage":self.profileImage,
            "badge":self.badge,
            "linkCode": self.linkCode,
            "tags": self.tags,
            "isFollower":self.isFollower,
            "userLat": self.userLat,
            "lastSignIn": self.lastSignIn,
            "userLon": self.userLon,
            "platform": "iOS"   // the last one always wins here
        ]
        
        if let phone = self.phone {
            result["phone"] = phone
        }
        
        if let email = self.email {
            result["email"] = email
        }
        
        if let pushToken = self.pushToken {
            result["pushToken"] = pushToken
        }
        
        return result
    }
    
    init(data: DataDict) {
        self.id = asString(data["id"], defaultValue: "unknown")
        
        self.name = asString(data["name"], defaultValue: "New Rider")
        
        if self.name == "" {
            self.name = "New Rider"
        }
        
        self.defaultRideVisibility = asString(data["defaultRideVisibility"], defaultValue: "")
        self.description = asString(data["description"], defaultValue: "description")
        self.cityState = asString(data["cityState"], defaultValue: "")
        self.country = asString(data["country"], defaultValue: "")
        
        self.platform = asString(data["platform"], defaultValue: "iOS")
        
        // deprecated
        // self.active = asBool(data["active"])
        
        if let activated = asDateOptional(data["activated"]) {
            self.activated = activated
        } else {
            self.activated = Date()
        }
        
        if let friends = data["friends"] as? [String] {
            self.friends = friends
        } else {
            self.friends = [String]()
        }
        
        
        if let ridesImages = data["ridesImages"] as? [String] {
            self.ridesImages = ridesImages
        }else{
            self.ridesImages = [String]()
        }
        
        if let createdGroups = data["createdGroups"] as? [String] {
            self.createdGroups = createdGroups
        }else{
            self.createdGroups = [String]()
        }
        
        if let joinedgroups = data["joinedgroups"] as? [String] {
            self.joinedgroups = joinedgroups
        }else{
            self.joinedgroups = [String]()
        }
                
        if let following = data["following"] as? [String] {
            self.following = following
        } else {
            self.following = [String]()
        }
        
        if let followed = data["followed"] as? [String] {
            self.followed = followed
        } else {
            self.followed = [String]()
        }
        
        self.userLat = asDouble(data["userLat"], defaultValue: 0.0)
        self.userLon = asDouble(data["userLon"], defaultValue: 0.0)
        self.lastSignIn = asDouble(data["lastSignIn"], defaultValue: 0.0)
        
        self.phone = asStringOptional(data["phone"])
        self.email = asStringOptional(data["email"])
        self.pushToken = asStringOptional(data["pushToken"])
        self.verificationDeclined = asBool(data["verificationDeclined"])
        
        self.defaultStatus = asString(data["defaultStatus"], defaultValue: "Down to Ride")
        self.linkCode = asString(data["linkCode"], defaultValue: "")
        self.profileImage = asString(data["profileImage"], defaultValue: "")
        self.badge = asString(data["badge"], defaultValue: "")
        
        if let tags = data["tags"] as? [String] {
            self.tags = tags
        } else {
            self.tags = [String]()
        }
    }
    
    func write(_ database: Firestore, propagate: Bool = false, completion: @escaping ProfileCompletion = { profile, error in }) {
        database.collection("profiles").document(self.id).setData(self.data, merge: true)
        
        // Update firebase auth
        if let user = Auth.auth().currentUser {
            let changeRequest = user.createProfileChangeRequest()
            changeRequest.displayName = self.name
            
            changeRequest.commitChanges { error in
                if let error = error {
                    completion(self, error)
                    return
                }
                
                if propagate {
                    self.updateRidesAsRider(database: database) { profile, error in
                        if let error = error {
                            completion(self, error)
                            return
                        }
                        completion(self, error)
                        self.updateRidesAsLeader(database: database) { profile, error in
                            if let error = error {
                                completion(self, error)
                                return
                            }
                            completion(self, error)
                        }
                    }
                }
            }
        }
    }
    
    
    private func updateRidesAsRider(database: Firestore, completion: @escaping ProfileCompletion = { profile, error in }) {
        // update rides where I am a rider
        
        let todayDateCode = dateCode(offset: 0)
        
        database.collection("rides")
            .whereField("rider", isEqualTo: self.id)
            .whereField("dateCode", isGreaterThanOrEqualTo: todayDateCode)
            .getDocuments { (querySnapshot, err) in
                guard err == nil else {
                    print("profile update error: \(err?.localizedDescription ?? "UNKNOWN")")
                    completion(self, err)
                    return
                }
                
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    
                    let batch = database.batch()
                    
                    for document in documents {
                        let ref = database.collection("rides").document(document.documentID)
                        batch.updateData(["riderName": self.name], forDocument: ref)
                    }
                    
                    batch.commit() { error in
                        completion(self, error)
                    }
                }
            }
    }
    
    private func updateRidesAsLeader(database: Firestore, completion: @escaping ProfileCompletion = { profile, error in }) {
        // update rides where I am a leader
        
        let todayDateCode = dateCode(offset: 0)
        
        database.collection("rides")
            .whereField("leader", isEqualTo: self.id)
            .whereField("dateCode", isGreaterThanOrEqualTo: todayDateCode)
            .getDocuments { (querySnapshot, err) in
                guard err == nil else {
                    print("profile update error: \(err?.localizedDescription ?? "UNKNOWN")")
                    return
                }
                
                if let snapshot = querySnapshot {
                    let documents = snapshot.documents
                    
                    let batch = database.batch()
                    
                    for document in documents {
                        let ref = database.collection("rides").document(document.documentID)
                        batch.updateData(["leaderName": self.name], forDocument: ref)
                    }
                    
                    batch.commit() { error in
                        if let error = error {
                            print("Transaction failed: \(error.localizedDescription)")
                        } else {
                            // print("profile leader update successfully committed!")
                        }
                    }
                }
            }
    }
}
