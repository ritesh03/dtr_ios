//
//  ExtensionC.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/01/21.
//

import Foundation
import SwiftUI

extension UIScreen {
    static let screenWidth = UIScreen.main.bounds.size.width
    static let screenHeight = UIScreen.main.bounds.size.height
    static let screenSize = UIScreen.main.bounds.size
}


extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}


struct RoundedCorner: Shape {
    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

struct PageControl: UIViewRepresentable {
    var numberOfPages: Int
    @Binding var currentPage: Int
    
    func makeUIView(context: Context) -> UIPageControl {
        let control = UIPageControl()
        control.numberOfPages = numberOfPages
        control.currentPageIndicatorTintColor = UIColor(displayP3Red: 190/255, green: 251/255, blue: 0/255, alpha: 1.0)
        control.pageIndicatorTintColor = UIColor(displayP3Red: 142/255, green: 144/255, blue: 144/255, alpha: 1.0)
        return control
    }
    
    func updateUIView(_ uiView: UIPageControl, context: Context) {
        uiView.currentPage = currentPage
    }
}

@available(iOS 13.0, *)
public struct TrackableScrollView<Content>: View where Content: View {
    let axes: Axis.Set
    let showIndicators: Bool
    @Binding var contentOffset: CGFloat
    let content: Content
    var completion : ((CGFloat)->Void)?
    
    public init(_ axes: Axis.Set = .vertical, showIndicators: Bool = true, contentOffset: Binding<CGFloat>, @ViewBuilder content: () -> Content,completion: ((CGFloat)->Void)? = nil) {
        self.axes = axes
        self.showIndicators = showIndicators
        self._contentOffset = contentOffset
        self.content = content()
        self.completion = completion
    }
    
    public var body: some View {
        GeometryReader { outsideProxy in
            ScrollView{
                LazyVStack {
                    ZStack(alignment: self.axes == .vertical ? .top : .leading) {
                        GeometryReader { insideProxy in
                            Color.clear
                                .preference(key: ScrollOffsetPreferenceKey.self, value: [self.calculateContentOffset(fromOutsideProxy: outsideProxy, insideProxy: insideProxy)])
                        }
                        VStack {
                            self.content
                        }
                    }
                }
            }
            .listStyle(GroupedListStyle())
            .onPreferenceChange(ScrollOffsetPreferenceKey.self) { value in
                self.contentOffset = value[0]
                self.completion?(self.contentOffset)
            }
        }
    }
    
    private func calculateContentOffset(fromOutsideProxy outsideProxy: GeometryProxy, insideProxy: GeometryProxy) -> CGFloat {
        if axes == .vertical {
            return outsideProxy.frame(in: .global).minY - insideProxy.frame(in: .global).minY
        } else {
            return outsideProxy.frame(in: .global).minX - insideProxy.frame(in: .global).minX
        }
    }
}


@available(iOS 13.0, *)
struct ScrollOffsetPreferenceKey: PreferenceKey {
    typealias Value = [CGFloat]
    
    static var defaultValue: [CGFloat] = [0]
    
    static func reduce(value: inout [CGFloat], nextValue: () -> [CGFloat]) {
        value.append(contentsOf: nextValue())
    }
}

extension View {
    func expandHorizontally() -> some View {
        frame(maxWidth: .infinity)
    }
}

extension CGFloat {
    func bounce(inRange range: ClosedRange<CGFloat>) -> CGFloat {
        let increasing = Int(self / (range.upperBound - range.lowerBound)) % 2 == 0
        let newWidth = abs(self).truncatingRemainder(dividingBy: range.upperBound - range.lowerBound)
        return increasing ? range.lowerBound + newWidth : range.upperBound - newWidth
    }
}

