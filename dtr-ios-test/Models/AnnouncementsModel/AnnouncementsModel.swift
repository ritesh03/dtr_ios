//
//  AnnouncementsModel.swift
//  dtr-ios-test
//
//  Created by Mobile on 10/03/21.
//

import UIKit

import Foundation

struct AnnouncmentAction {
    enum State: String {
        case none, activated, viewed
    }
    
    var profileID: String
    var date: Date
    var state: State
    
    var data: DataDict {
        [
            "profileID": self.profileID,
            "date": self.date.timeIntervalSince1970,
            "state": self.state.rawValue
        ]
    }
    
    init(profileID: String, state: State) {
        self.profileID = profileID
        self.state = state
        self.date = Date()
    }
    
    init(data: DataDict) {
        self.profileID = asString(data["profileID"])
        self.date = asDate(data["date"], defaultValue: Date())
        
        if let state = AnnouncmentAction.State(rawValue: asString(data["state"])) {
            self.state = state
        } else {
            self.state = .none
        }
    }
}

struct AnnouncementsModel: Identifiable {
    var id: String
    var title: String
    var message: String
    var expires: Date
    var actions = [AnnouncmentAction]()
    
    var data: DataDict {
        var actionsData = [DataDict]()
        
        for action in actions {
            actionsData.append(action.data)
        }
        
        return [
            "id": self.id,
            "title": self.title,
            "message": self.message,
            "expires": self.expires.timeIntervalSince1970,
            "actions": actionsData
        ]
    }
    
    var expired: Bool {
        self.expires <= Date()
    }
    
    init(id: String, title: String, message: String, expires: Date) {
        self.id = id
        self.title = title
        self.message = message
        self.expires = expires
    }
    
    init(data: DataDict) {
        self.id = asString(data["id"])
        self.title = asString(data["title"])
        self.message = asString(data["message"])
        self.expires = asDate(data["expires"], defaultValue: Date())
        
        self.actions = [AnnouncmentAction]()
        
        if let actionsData = data["actions"] as? [DataDict] {
            for data in actionsData {
                self.actions.append(AnnouncmentAction(data: data))
            }
        }
    }
    
    func responded(profileID: String) -> Bool {
        self.actions.contains(where: { $0.profileID == profileID })
    }
    
    mutating func respond(profileID: String, state: AnnouncmentAction.State) {
        if let index = self.actions.firstIndex(where: { $0.profileID == profileID }) {
            var action = self.actions[index]
            action.state = state
            action.date = Date()
            
            self.actions[index] = action
            return
        }
        
        self.actions.append(AnnouncmentAction(profileID: profileID, state: state))
    }
}
