//
//  Errors.swift
//  dtr-ios
//
//  Created by Dan Kinney on 2/23/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import Foundation

enum AppErrorType: Int {
    case unknown, stateTransition, invalidProfileID, planLoad, rideJoin
    
    var title: String {
        switch self {
            case .unknown: return "Unknown"
            case .stateTransition: return "Transition Error"
            case .invalidProfileID: return "Invalid User"
            case .planLoad: return "Invalid User"
            case .rideJoin: return "Ride Join"
        }
    }
    
    var errorDescription: String {
        switch self {
            case .unknown: return "An unknown error has occurred"
            case .stateTransition: return "The intended state cannot be updated from the current state"
            case .invalidProfileID: return "The profile is not valid"
            case .planLoad: return "Could not load plans for user"
            case .rideJoin: return "Could not join the ride"
        }
    }
}

struct AppError: AppErrorProtocol {
    var type: AppErrorType
    var failureReason: String?
    
    var code: Int { type.rawValue }
    var title: String? { type.title }
    var errorDescription: String? { type.errorDescription }
    
    private var _description: String
    
    init(_ type: AppErrorType, message: String? = nil) {
        self.type = type
        self.failureReason = message
        self._description = type.errorDescription
        
        if let description = self.errorDescription {
            self._description = description
        }
    }
    
    init(_ type: AppErrorType, error: Error?) {
        self.type = type
        self.failureReason = error.debugDescription
        self._description = type.errorDescription
        
        if let error = error {
            self._description = error.localizedDescription
        }
    }
}

protocol AppErrorProtocol: LocalizedError {
    var code: Int { get }
    var title: String? { get }
    var errorDescription: String? { get }
    var failureReason: String? { get }
}
