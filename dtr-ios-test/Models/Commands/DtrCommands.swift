//
//  DtrCommands.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 12/31/20.
//

import Foundation
import Combine
import Firebase

typealias FunctionCompletion = (ResultDtrCommand) -> Void

class DtrCommand : NSObject, ObservableObject {
    private var db: Firestore
    var functions = Functions.functions()
    
    static let sharedInstance: DtrCommand = {
        let instance = DtrCommand()
        return instance
    }()
    
    var didChange = PassthroughSubject<DtrCommand, Never>()
    
    override init() {
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        self.db = Firestore.firestore()
        db.settings = settings
        super.init()
    }
    
    func dtrCommandWrapper(callData: DataDict, initiatedBy: String = "",completion: @escaping FunctionCompletion = { result in } ) {
        guard let currentUser = Auth.auth().currentUser else {
            var result = ResultDtrCommand()
            result.result = .error
            result.feedback = "User is not authorized"
            return
        }
        var data = callData
        if initiatedBy.isEmpty {
            data["initiatedBy"] = currentUser.uid
        } else {
            data["initiatedBy"] = initiatedBy
        }
//        print("data ------>",  data)
        functions.httpsCallable("dtrCommand").call(data) { functionResult, error in
            if let error = error {
                completion(ResultDtrCommand(error: error))
                return
            }
            if let resultData = functionResult?.data as? [String:Any] {
//                print("resultData ------>", resultData)
                completion(ResultDtrCommand(data: resultData))
                return
            }
            completion(ResultDtrCommand())
        }
    }
}
