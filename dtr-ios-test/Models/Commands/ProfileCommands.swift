//
//  ProfileCommands.swift
//  dtr-ios-test
//
//  Created by apple on 23/07/21.
//

import Foundation
import Firebase

extension DtrCommand {
    
    func getMutualConnectionParticularUser(profileID:String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.mutuals","objectID": profileID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func getMutualConnectionListOfUsers(userProfilesIds:[String], completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.relations","data":["profiles":userProfilesIds]]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func profileRead(profileID: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.read","objectID": profileID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func profileUpdate(profileID: String, data: DataDict, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.update","objectID": profileID,"data": data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func profileFollow(profileID: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.follow","objectID": profileID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func profileUnfollow(profileID: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.unfollow","objectID": profileID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func userUnfriend(profileID: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.unfriend","objectID": profileID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func userDecline(profileID: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.revoke","objectID": profileID,"arg" : ""]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func userUnfriendFromRelationShip(profileID: String, completion: @escaping FunctionCompletion = { result in } ) {
        Analytics.logEvent(AnalyticsEvent.friendRelationshipDestroy.rawValue, parameters: ["source": DtrData.sharedInstance.profileID])
        let callData: DataDict = ["name": "profile.unfriend","objectID": profileID,"arg" : ""]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    //Working Commands For Profile
    func sendFriendRequest(profileID: String, completion: @escaping FunctionCompletion = { result in } ) {
        Analytics.logEvent(AnalyticsEvent.friendRelationshipCreate.rawValue, parameters: ["source": DtrData.sharedInstance.profileID])
        let callData: DataDict = ["name": "profile.invite","objectID": profileID,"arg" : ""]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func createFriendShipRelation(profileID: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.friend","objectID": profileID,"arg" : ""]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func deleteUserPermemtly(profileID: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.deleteUser","objectID": profileID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
}
