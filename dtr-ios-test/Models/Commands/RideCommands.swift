//
//  RideCommands.swift
//  dtr-ios-test
//
//  Created by apple on 23/07/21.
//

import Foundation
import Firebase


extension DtrCommand {
    
    func getPublicRides(dateCode: String,currentlat:CGFloat,currentlon:CGFloat, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "ride.publicRides","dateCode":dateCode, "lat": currentlat, "lon": currentlon,"arg":""]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func getRideInvite(dateCode: String,userID:String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.rideOn","objectID": userID,"arg":dateCode]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func rideRead(rideID: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "ride.read","objectID": rideID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func rideUpdate(rideID: String, data: DataDict, completion: @escaping FunctionCompletion = { result in } ) {
        Analytics.logEvent(AnalyticsEvent.rideUpdate.rawValue, parameters: ["rideID": rideID])
        let callData: DataDict = ["name": "ride.update","objectID": rideID,"data": data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func rideUpdateTime(rideID: String, data: DataDict, completion: @escaping FunctionCompletion = { result in } ) {
        Analytics.logEvent(AnalyticsEvent.rideUpdateTime.rawValue, parameters: ["rideID": rideID])
        let callData: DataDict = ["name": "ride.updateTime","objectID": rideID,"data": data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func rideUpdateLocation(rideID: String, data: DataDict, completion: @escaping FunctionCompletion = { result in } ) {
        Analytics.logEvent(AnalyticsEvent.rideUpdateLocation.rawValue, parameters: ["rideID": rideID])
        let callData: DataDict = ["name": "ride.updateLocation","objectID": rideID,"data": data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func rideCreateWithDefaultValue(defaultRideInfo:DtrRide,dateCode:String, completion: @escaping FunctionCompletion = { result in } ) {
        var dictCreate = [String:Any]()
        dictCreate = ["summary": "Down to Ride","details":["hasStartPlace":defaultRideInfo.details.hasStartPlace,"hasStartTime":defaultRideInfo.details.hasStartTime,"notes":defaultRideInfo.details.notes,"skillA":defaultRideInfo.details.skillA,"skillB":defaultRideInfo.details.skillB,"skillC":defaultRideInfo.details.skillC,"skillD":defaultRideInfo.details.skillD,"skillS":defaultRideInfo.details.skillS,"startPlace":defaultRideInfo.details.startPlace,"startPlaceLat":defaultRideInfo.details.startPlaceLat,"startPlaceLon":defaultRideInfo.details.startPlaceLon,"startTime":defaultRideInfo.details.startTime,"visibility":DtrData.sharedInstance.profile.defaultRideVisibility == "" ? "followers" : DtrData.sharedInstance.profile.defaultRideVisibility]] as [String : Any]
        let callData: DataDict = ["name": "ride.create","arg": dateCode,"data": dictCreate]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func rideCreate(dateCode: String, data: DataDict, completion: @escaping FunctionCompletion = { result in } ) {
        Analytics.logEvent(AnalyticsEvent.activate.rawValue, parameters: ["dateCode": dateCode])
        let callData: DataDict = ["name": "ride.create","arg": dateCode,"data": data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func rideJoin(rideID: String,dateCode: String, completion: @escaping FunctionCompletion = { result in } ) {
        Analytics.logEvent(AnalyticsEvent.join.rawValue, parameters: ["dateCode": dateCode])
        let callData: DataDict = ["name": "ride.join","objectID": rideID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func rideLeave(rideID: String,dateCode: String, completion: @escaping FunctionCompletion = { result in } ) {
       Analytics.logEvent(AnalyticsEvent.leave.rawValue, parameters: ["dateCode": dateCode])
        let callData: DataDict = ["name": "ride.leave","objectID": rideID]
        dtrCommandWrapper(callData: callData) { result in
            result.debug(methodName: "rideLeave")
            completion(result)
        }
    }
    
    func rideCancel(rideID: String,dateCode: String, message: String, completion: @escaping FunctionCompletion = { result in } ) {
        Analytics.logEvent(AnalyticsEvent.cancel.rawValue, parameters: ["dateCode": dateCode])
        let callData: DataDict = ["name": "ride.cancel","objectID": rideID,"arg": message]
        dtrCommandWrapper(callData: callData) { result in
            UserDefaults.standard.removeObject(forKey: UserStore.shared.MyRideDetailLocally)
            completion(result)
        }
    }
    
    func rideVisibility(rideID: String, data: DataDict, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "ride.visibility","objectID": rideID,"data": data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func upcomingRides(profileID: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "upcoming.rides","objectID": profileID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func sendRideInvitation(profileID: String, data: DataDict, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "ride.invite","objectID": profileID,"data" : data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }

    func createNewGroupLeader(groupId: String, currentLeader: String, newLeader: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.newGroupLeader","objectID": groupId,"currentLeaderProfileID" : currentLeader, "newLeaderProfileID":newLeader]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func sendGroupInvitation(profileID: String, groupId: String, data: DataDict, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.invite", "initiatedBy": profileID, "objectID": groupId,"data" : data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func sendRideMessage(rideID: String, message: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "ride.message","objectID": rideID,"arg" : message]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func sendSimpleNotification(profileID: String, data: DataDict, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "notification.send","objectID": profileID,"data" : data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func sendRideMessageWithPushNotification(rideID: String, message: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "ride.messageNotify","objectID": rideID,"arg" : message]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func clearNotifcationFromServer(data: DataDict, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "notification.clear","data":data,"arg" : ""]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func sendMesgTopUpdateRideInfo(rideID: String, message: String, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "ride.message","objectID": rideID,"arg" :message]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
}
