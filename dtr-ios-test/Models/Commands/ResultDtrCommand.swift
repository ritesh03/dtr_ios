//
//  ResultDtrCommand.swift
//  dtr-ios-test
//
//  Created by apple on 23/07/21.
//

import Foundation
struct ResultDtrCommand {
    enum ResultType: String {
        case ok, error, exception, unknown
    }
    
    var result: ResultType = .unknown
    var output: DataDict = [:]
    var feedback: String = ""
    var input: DataDict?
    
    init() {
        self.result = .unknown
        self.output = [:]
        self.feedback = ""
    }
    
    init(result: ResultType, feedback: String) {
        self.result = result
        self.feedback = feedback
    }
    
    init(error: Error) {
        self.result = .error
        self.feedback = error.localizedDescription
    }
    
    init(data: [String:Any]) {
        self.result = ResultDtrCommand.ResultType(rawValue: asString(data["result"])) ?? .unknown
        self.feedback = asString(data["feedback"])
        self.output = asDataDict(data["output"])
        self.input = asDataDict(data["input"])
        if feedback == "profile.mutuals" {
            let stringSatus = asString(data["output"])
            self.output = [feedback:stringSatus]
        }
        if feedback == "group.groupsProfileList" {
            if let outputs = data["output"] as? [AnyObject] {
                for outputFirst in outputs {
                    if let dataOutput = outputFirst as? [String:AnyObject], let profile = dataOutput["profile"] as? [String:AnyObject] {
                        self.output = asDataDict(profile)
                    }
                }
            }
        }
    }
    
    func debug(methodName:String) {
//        print("DTR Command Result: \(methodName)")
//        print("result: \(self.result.rawValue)")
//        print("feedback: \(self.feedback)")
        //print("output: \(self.output.prettyFormatted())")
//        if let input = self.input {
//            print("input: \(input.prettyFormatted())")
//        }
    }
}
