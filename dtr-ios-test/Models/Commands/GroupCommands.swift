//
//  GroupCommands.swift
//  dtr-ios-test
//
//  Created by apple on 23/07/21.
//

import Foundation

//dtrCommand({name:'group.getAllGroupList'})
//dtrCommand({name:'group.create',arg: 'profileID', data: {summary: 'testGroup'}})
//dtrCommand({name:'group.read', objectID: 'groupID'})
//dtrCommand({name:'group.update', arg: 'ProfileID', objectID: 'GroupID', data: {summary: 'FUNNY Ride',name:'honey'}})
//dtrCommand({name: 'group.join', initiatedBy: 'ProfileID', objectID: 'GroupID'})
//dtrCommand({name: 'group.left', initiatedBy: 'ProfileID', objectID: 'GroupID'})
//dtrCommand({name:'group.groupsProfileList', initiatedBy: 'groupID',data:{pageSize:6, pageNumber:1}})
//dtrCommand({name:'group.getAllGroupList', lastDoc:'o4Mvthffy4dWnkKxb24Vxn8UnpT2+wope', pageSize:3})
//dtrCommand({name: 'group.decline', initiatedBy: 'requestedprofileID', objectID: 'groupID',requestedProfileID:'groupLeader profileID' })
//dtrCommand({name: 'group.invite', initiatedBy: 'profileID', objectID: 'groupID', data:{recipients:['profileID','profileID']} })



extension DtrCommand {
    
    
    func groupDeclineComand(groupID:String,groupLeaderID:String,completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.decline","objectID":groupID,"requestedProfileID":groupLeaderID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    
    func groupInviteComand(groupID:String,data: DataDict,completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.invite","objectID":groupID,"data": data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    
    func getAllgroupListForProfile(completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "profile.read"]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func getAllgroupList(lastDocId:String,completion: @escaping FunctionCompletion = { result in } ) {
        var pageSize = 3
        if lastDocId ==  "" {
            pageSize = 10
        }
        let callData: DataDict = ["name": "group.getAllGroupList","lastDoc":lastDocId,"pageSize":pageSize]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func getAllgroupListNew(lastDocId:String,completion: @escaping FunctionCompletion = { result in } ) {
        var pageSize = 3
        if lastDocId ==  "" {
            pageSize = 10
        }
        let callData: DataDict = ["name": "group.groupsList","lastDoc":lastDocId,"pageSize":pageSize]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func groupCreate(data: DataDict, completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.create","arg": DtrData.sharedInstance.profileID,"data": data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func getParticularGroupDetail(groupID:String,completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.read","objectID":groupID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func getUpdateGroupDetails(groupID:String,data: DataDict,completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.update","arg": DtrData.sharedInstance.profileID,"objectID":groupID,"data": data]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func groupJoin(groupID:String,completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.join","objectID":groupID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func groupJoinDecline(groupID:String, requestedProfileID: String,completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.decline","objectID":groupID, "requestedProfileID":requestedProfileID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    
    func groupLeft(groupID:String,completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.leave","objectID":groupID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func groupDelete(groupID:String,completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.groupDelete","objectID":groupID]
        dtrCommandWrapper(callData: callData) { result in
            completion(result)
        }
    }
    
    func groupProfileList(groupID:String,completion: @escaping FunctionCompletion = { result in } ) {
        let callData: DataDict = ["name": "group.read", "objectID": groupID]
        dtrCommandWrapper(callData: callData, initiatedBy: groupID) { result in
            completion(result)
        }
    }
}
