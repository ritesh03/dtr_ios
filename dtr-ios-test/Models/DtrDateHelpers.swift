//
//  DtrDateHelpers.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/27/20.
//


import Foundation

func nextMidnight() -> Date {
    let startOfDay = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
    return Calendar.current.date(byAdding: .day, value: 1, to: startOfDay)!
}

func oneDay() -> TimeInterval {
    let minute: TimeInterval = 60.0
    let hour: TimeInterval = 60.0 * minute
    let day: TimeInterval = 24 * hour
    
    return day
}


func dateFormatGetDat(offset: Int) ->String{
    let dateCodeFormatter = DateFormatter()
    dateCodeFormatter.dateFormat = "yyyyMMdd"
    let now = Date()
    let date =  dateCodeFormatter.string(from: Calendar.current.date(byAdding: .day, value: offset, to: now)!)
    return date
}


func dateFrom(offset: Int) -> Date {
    let now = Date()
    if let date = Calendar.current.date(byAdding: .day, value: offset, to: now) {
        return date
    }
    return now
}

func dateCode(offset: Int) -> String {
    let dateCodeFormatter = DateFormatter()
    dateCodeFormatter.dateFormat = "yyyyMMdd"
    return dateCodeFormatter.string(from: dateFrom(offset: offset))
}

func getIndexDatCode(i:Int)-> String {
    let dayTemp = dateCode(offset: i)
    if let dateCode =  dayShortName(dateCode:dayTemp) as? String {
        return dateCode
    }
    return dayName(dateCode:dateCode(offset: 0))
}


func getSatDatCode()-> String {
    for i in 0..<6 {
        let dayTemp = dateCode(offset: i)
        if dayShortName(dateCode:dayTemp) == "Sat" {
            return dayTemp
        }
    }
    return dayName(dateCode:dateCode(offset: 0))
}

func getSunDatCode()-> String {
    for i in 0..<6 {
        let dayTemp = dateCode(offset: i)
        if dayShortName(dateCode:dayTemp) == "Sun" {
            return dayTemp
        }
    }
    return dayName(dateCode:dateCode(offset: 0))
}

func getIndexSunDatCode()-> Int {
    for i in 0..<6 {
        let dayTemp = dateCode(offset: i)
        if dayShortName(dateCode:dayTemp) == "Sun" {
            return i
        }
    }
    return 0
}

func getIndexSatDatCode()-> Int {
    for i in 0..<6 {
        let dayTemp = dateCode(offset: i)
        if dayShortName(dateCode:dayTemp) == "Sat" {
            return i
        }
    }
    return 0
}

func toDate(timeString: String, on dateCode: String? = nil) -> Date {
    var date = Date()
    let formatter = DateFormatter()
    formatter.timeZone = TimeZone.current
    formatter.dateFormat = "yyyyMMdd"
    if let dateCode = dateCode {
        if let parsedDate = formatter.date(from: dateCode) {
            date = parsedDate
        }
    }
    formatter.dateFormat = "h:mm a"
    if let time = formatter.date(from: timeString) {
        let hour = Calendar.current.component(.hour, from: time)
        let minute = Calendar.current.component(.minute, from: time)
        
        if let adjustedDate = Calendar.current.date(bySettingHour: hour, minute: minute, second: 0, of: date) {
            return adjustedDate
        }
    }
    
    formatter.dateFormat = "HH:mm"
    
    if let time = formatter.date(from: timeString) {
        let hour = Calendar.current.component(.hour, from: time)
        let minute = Calendar.current.component(.minute, from: time)
        
        if let adjustedDate = Calendar.current.date(bySettingHour: hour, minute: minute, second: 0, of: date) {
            return adjustedDate
        }
    }
    
    // default is today at noon
    if let adjustedDate = Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: date) {
        return adjustedDate
    }
    print("Datetsh = ",date,timeString)
    return date
}

func dayName(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "EEE"
    return formatter.string(from: date)
}

func dayName(dateCode: String) -> String {
    let input = DateFormatter()
    input.dateFormat = "yyyyMMdd"
    if let date = input.date(from: dateCode) {
        let output = DateFormatter()
        output.dateFormat = "EEEE"
        return output.string(from: date)
    }
    return "Unknown"
}

func dayShortName(dateCode: String) -> String {
    let input = DateFormatter()
    input.dateFormat = "yyyyMMdd"
    if let date = input.date(from: dateCode) {
        let output = DateFormatter()
        output.dateFormat = "EEE"
        return output.string(from: date)
    }
    return "Unknown"
}

func dayIndex(dateCode: String) -> String {
    let input = DateFormatter()
    input.dateFormat = "yyyyMMdd"
    if let date = input.date(from: dateCode) {
        let output = DateFormatter()
        output.dateFormat = "EEEE"
        return output.string(from: date)
    }
    return "Unknown"
}




func dateMonthYearRetrun(dateCode: String) -> String {
    let input = DateFormatter()
    input.dateFormat = "yyyyMMdd"
    if let date = input.date(from: dateCode) {
        let output = DateFormatter()
        output.dateFormat = "dd/MM/yy"
        return output.string(from: date)
    }
    return "Unknown"
}



func monthAndDateName(dateCode: String) -> String {
    let input = DateFormatter()
    input.dateFormat = "yyyyMMdd"
    if let date = input.date(from: dateCode) {
        let output = DateFormatter()
        output.dateFormat = "MMM d, EEE"
        return output.string(from: date)
    }
    return "Unknown"
}

func memberSince(_ date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "MMM yyyy"
    
    return formatter.string(from: date)
}

func toOffset(dateCode: String) -> Int {
    let targetDate = toDate(timeString: "00:00", on: dateCode)
    let components = Calendar.current.dateComponents([Calendar.Component.day], from: Date().startOfDay, to: targetDate)
    if let daysFromNow = components.day {
        if daysFromNow > 0 {
            return daysFromNow
        }
    }
    return 0
}

extension Collection {
    func count(where test: (Element) throws -> Bool) rethrows -> Int {
        return try self.filter(test).count
    }
}

extension Date {
    var startOfDay: Date {
        return Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: self)!
    }
    var startOfDayPrevious: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self.startOfDay)!
    }
    var startOfDayNext: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self.startOfDay)!
    }
    var startOfWeek: Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
    }
    var startOfWeekPrevious: Date {
        return Calendar.current.date(byAdding: .weekOfYear, value: -1, to: self.startOfWeek)!
    }
    var startOfWeekNext: Date {
        return Calendar.current.date(byAdding: .weekOfYear, value: 1, to: self.startOfWeek)!
    }
    var startOfMonth: Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    var daysInMonth: Int {
        let range = Calendar.current.range(of: .day, in: .month, for: self)!
        return range.count
    }
    var startOfMonthPrevious: Date {
        return Calendar.current.date(byAdding: .month, value: -1, to: self.startOfMonth)!
    }
    var startOfMonthNext: Date {
        return Calendar.current.date(byAdding: .month, value: 1, to: self.startOfMonth)!
    }
    var startOfYear: Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year], from: Calendar.current.startOfDay(for: self)))!
    }
    var daysInYear: Int {
        let range = Calendar.current.range(of: .day, in: .year, for: self)!
        return range.count
    }
    var startOfYearPrevious: Date {
        return Calendar.current.date(byAdding: .year, value: -1, to: self.startOfYear)!
    }
    var startOfYearNext: Date {
        return Calendar.current.date(byAdding: .year, value: 1, to: self.startOfYear)!
    }
    var sevenDaysFromNow: Date {
        return Calendar.current.date(byAdding: .day, value: 7, to: self)!
    }
    var thirtyDaysFromNow: Date {
        return Calendar.current.date(byAdding: .day, value: 30, to: self)!
    }
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}
