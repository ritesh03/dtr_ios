//
//  AppNotification.swift
//  dtr-ios
//
//  Created by Dan Kinney on 3/7/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI
import Firebase

struct AppNotification: Identifiable {
    let firebaseServerKey = "AAAAGM3_ePc:APA91bE7tQ-YVZ_8etVvjbEpoZXeG38zz586cLbb4_Iehcg91y_A9IefBHTC1_hjiYFtwjVU7TeAgGLNOZbSLTWPFnXZYRBciNuaRlLISoa-2zxyTH4mkdWLC1KMuhVmzFfcOmUj2bhn"
    
    enum AppNotificationType: String {
        case unknown
        case campaign, announcement
        case friendInvite, friendMessage, message,follow, groupUpdate
        case rideInvite, rideUpdate, rideLocation, rideTime, rideCancelled
        case poke
        case groupLeadernotify,groupJoin,groupLeaderMessage,groupInvite
        
        
        var data: String {
            return self.rawValue
        }
        
        var label: String {
            switch self {
            case .unknown: return "Message"
            case .campaign: return "Promotion"
            case .announcement: return "Announcement"
            case .friendInvite: return "Friend Request"
            case .friendMessage: return "Friend Message"
            case .follow: return "Friend Following"
            case .groupUpdate: return "Group Update"
            case .message: return "Friend Message"
            case .rideInvite: return "Ride Invitation"
            case .rideUpdate: return "Ride Update"
            case .rideLocation: return "Ride Location Change"
            case .rideTime: return "Ride Time Change"
            case .rideCancelled: return "Ride Cancelled"
            case .poke: return "Poke"
            case .groupLeadernotify: return "Group Message"
            case .groupJoin: return "Group Message"
            case .groupLeaderMessage: return "Group Leader Message"
            case .groupInvite: return "Group Invite"
                
            }
        }
        
        var action: AppNotificationAction {
            switch self {
            case .unknown, .announcement: return .none
            case .campaign: return .offer
            case .friendInvite, .friendMessage,.message,.follow,.groupUpdate: return .user
            case .rideInvite, .rideUpdate, .rideLocation, .rideTime, .rideCancelled: return .ride
            case .poke:return .ride
            case .groupInvite,.groupLeadernotify,.groupJoin,.groupLeaderMessage:return .group
            }
        }
    }
    
    enum AppNotificationStatus: String, CaseIterable {
        case unread, read, accepted, rejected, cancelled
        
        var data: String {
            return self.rawValue
        }
    }
    
    enum AppNotificationAction: String, CaseIterable {
        case none, update, offer, survey, user, ride, group, groupLeaderMessage ,groupLeft
        
        var data: String {
            return self.rawValue
        }
    }
    
    var id: String = ""
    var source: String
    var target: String
    var type: AppNotificationType = .unknown
    var payloadID: String
    var status: AppNotificationStatus = .unread
    var action: AppNotificationAction = .none
    var message: String
    var timestamp: Double
    
    var expires: Double?
    
    var data:DataDict {
        get {
            let value: DataDict = [
                "id": id,
                "source": source,
                "target": target,
                "type": type.data,
                "payloadID": payloadID,
                "status": status.data,
                "action": action.data,
                "message": message,
                "timestamp": timestamp,
                "expires": expires ?? 0.0
            ]
            return value
        }
    }
    
    init(source: String, target: String, type: AppNotificationType, payloadID: String, message: String, expires: Date?) {
        self.id = "SETUP"
        self.source = source
        self.target = target
        self.type = type
        self.payloadID = payloadID
        self.action = type.action
        self.message = message
        self.status = .unread
        self.timestamp = Date().timeIntervalSince1970
        self.expires = Date().timeIntervalSince1970
    }
    
    init(data: DataDict) {
        self.id = asString(data["id"])
        self.source = asString(data["source"])
        self.target = asString(data["target"])
        self.payloadID = asString(data["payloadID"])
        
        
        if let type = AppNotificationType(rawValue: asString(data["type"])) {
            self.type = type
        } else {
            self.type = AppNotificationType.unknown
        }
        
        if let action = AppNotificationAction(rawValue: asString(data["action"])) {
            self.action = action
        } else {
            self.action = AppNotificationAction.none
        }
        
        if let status = AppNotificationStatus(rawValue: asString(data["status"])) {
            self.status = status
        } else {
            self.status = AppNotificationStatus.unread
        }
        self.message = asString(data["message"])
        self.timestamp = asDouble(data["timestamp"], defaultValue: Date().timeIntervalSince1970)
        self.expires = asDouble(data["expires"], defaultValue: Date().timeIntervalSince1970)
    }
    
    static var `default`: AppNotification {
        return AppNotification(source: "SETUP", target: "SETUP", type: .unknown, payloadID: "", message: "", expires: nil)
    }
    
    var getNotifictionDay: String {
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: date)
    }
    
    var getNotifictionTime: String {
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: date)
    }
    
    var notify: Bool {
        switch self.type {
        case .unknown, .rideUpdate, .groupUpdate, .rideLocation, .rideTime, .rideCancelled: return false
        case .campaign, .announcement, .friendInvite, .friendMessage,.message,.follow,.rideInvite,.poke,.groupLeadernotify,.groupJoin,.groupLeaderMessage, .groupInvite: return true
        }
    }
    
    func push(token: String) {
        var title = self.type.label
        
        if self.type == .rideUpdate {
            title += " on \(dayName(dateCode: self.payloadID))"
        }
        
        let payload: DataDict = [
            "to": token,
            "mutable_content": true,
            "notification": [
                "title": title,
                "body": message,
                "sound": "bikebell.mp3",
                "click_action": "openNotification"
            ],
            "data": [
                "title": self.type.label,
                "body": message,
                "notification": self.data
            ]
        ]
        
        let url = URL(string: "https://fcm.googleapis.com/fcm/send")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // get your **server key** from your Firebase project console under **Cloud Messaging** tab
        request.setValue("key=\(firebaseServerKey)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: payload, options: [.prettyPrinted])
            
        } catch {
            print("unexpected error creating payload: \(error.localizedDescription)")
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error ?? "")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print(response ?? "")
            }
            
            // print("Notification sent successfully.")
            let responseString = String(data: data, encoding: .utf8)
            print(responseString ?? "")
        }
        task.resume()
    }
}
