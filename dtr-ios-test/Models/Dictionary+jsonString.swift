//
//  Dictionary+jsonString.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 1/8/21.
//

import Foundation

extension Dictionary {
    func jsonString() -> NSString? {
        let jsonData = try? JSONSerialization.data(withJSONObject: self, options: [])
        guard jsonData != nil else {return nil}
        let jsonString = String(data: jsonData!, encoding: .utf8)
        guard jsonString != nil else {return nil}
        return jsonString! as NSString
    }
}

func parseLinkPath(_ urlString: String) -> (type: String, source: String, data: String) {
    var type: String = "unknown"
    var source: String = "unknown"
    var data: String = "empty"
    
    if let url = URL(string: urlString) {
        if let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true),
            let path = components.path{
            let elements = path.split(separator: "/")
            if elements.count > 0 {
                type = String(elements[0])
            }
            if elements.count > 1 {
                source = String(elements[1])
            }
            if elements.count > 2 {
                data = String(elements[2])
            }
        }
    }
    return (type, source, data)
}
