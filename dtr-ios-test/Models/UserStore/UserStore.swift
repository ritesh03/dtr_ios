//
//  UserStore.swift
//  dtr-ios-test
//
//  Created by Mobile on 22/02/21.
//

import UIKit

class UserStore: NSObject {
    
    static let shared = UserStore()
    private override init(){
        super.init()
    }
    
    let UserDeafult = UserDefaults.standard
    let SaveTimeStampKey = "Save_Time_Stampe"
    let ForFirstTabId = "First_Tab_Id"
    let ForThirdTabId = "Third_Tab_Id"
    let ForSecondTabId = "Second_Tab_Id"
    let ForFourthTabId = "Fourth_Tab_Id"
    let RegistrationPrgrss1Key = "Registration_In_Prgrss1"
    let AllSaveTimeStampRideKey = "Save_All_timstampe"
    let SaveTabBarUniqueIdForSecond = "SaveTab_Unique_Second"
    let SaveProdOrLocalValue = "Save_Prod_Or_Local_Value"
    let SaveProdOrLocalByToggleValue = "Save_Prod_Or_Local_By_Toggle_Value"
    let SaveLocallySelectFollowersForInvite = "Save_Locally_Select_Followers_For_Invite"
    let SaveLocallySelectFriendsForInvite = "Save_Locally_Select_Friends_For_Invite"
    let SaveLocallySelectGroupForInvite = "Save_Locally_Select_Group_For_Invite"
    let SaveDyanamicLinkLocal = "Save_Dyanamic_Link_Local"
    let SaveFirstTimeUserRegsiter = "Save_First_Time_User_Regsiter"
    let SaveOnlyOneTimeFCMToken = "Save_Only_One_Time_FCM_Token"
    let UpdateForInviteAndShare = "Save_Update_For_Invite_And_Share"
    let MyRideDetailLocally = "Save_Ride_Detail_Locally"
    let SaveRootClassSearch = "Save_Root_Class_Search"
    
    let SaveShouldRecordComeBackRide = "should_Record_Come_Back_Ride"
    let SaveShouldRecordJoinWeekendRide = "should_Record_Join_Weekend_Ride"
    let SaveShouldRecordGoDTRWeekendRide = "should_Record_Go_DTR_Weekend_Ride"
    let notificationMayBeDisabled = "notificationMayBeDisabled"
    let comeFromDyanamicLink = "come_From_Dyanamic_Link"
    
    var rideWithTimeStmp = [String : Double]()
    
    func removeAllkeys() {
        UserDeafult.removeObject(forKey: ForFirstTabId)
        UserDeafult.removeObject(forKey: ForSecondTabId)
        UserDeafult.removeObject(forKey: SaveTimeStampKey)
        UserDeafult.removeObject(forKey: AllSaveTimeStampRideKey)
        UserDeafult.removeObject(forKey: RegistrationPrgrss1Key)
        UserDeafult.removeObject(forKey: SaveLocallySelectFollowersForInvite)
        UserDeafult.removeObject(forKey: SaveLocallySelectFriendsForInvite)
        UserDeafult.removeObject(forKey: SaveLocallySelectGroupForInvite)
        UserDeafult.removeObject(forKey: SaveFirstTimeUserRegsiter)
        UserDeafult.removeObject(forKey: UpdateForInviteAndShare)
        UserDeafult.removeObject(forKey: SaveShouldRecordComeBackRide)
        UserDeafult.removeObject(forKey: SaveShouldRecordJoinWeekendRide)
        UserDeafult.removeObject(forKey: SaveShouldRecordGoDTRWeekendRide)
        UserDeafult.removeObject(forKey: "isVerificationComplelteOrNot")
        UserDeafult.removeObject(forKey: "publicViewAlreadyShowOrNot")
        UserDeafult.removeObject(forKey: "profileIdForUnitTest")
        rideWithTimeStmp.removeAll()
    }
    
    func UpdateAndRestLocalStoreWhenStartingApp() {
        UserStore.shared.dyanamicLinkNotWorking = CommonAllString.BlankStr
        UserStore.shared.isComeFromDyanamic =  false
        UserStore.shared.isForFirstTabId = 0
        UserStore.shared.isForSecondTabId = 0
        UserStore.shared.isForThirdTabId = 0
        UserStore.shared.isForFourthTabId = 0
        UserStore.shared.saveTabBarUniqueIdForSecond = 0
        UserStore.shared.isFirstTimeUserRegsiter =  false
        UserStore.shared.updateOnlyOneTimeFCMToken = true
        UserStore.shared.updateOnlyOneTimeForINviteAndShare = false
        UserStore.shared.isRootClassSearch = false
        UserDeafult.removeObject(forKey: MyRideDetailLocally)
    }
    
    
    var lastLoginUpdatedTimeStamp: TimeInterval {
        get {
            return UserDefaults.standard.value(forKey: "lastLoginUpdatedTimeStamp") as? TimeInterval ?? 0.0
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "lastLoginUpdatedTimeStamp")
        }
    }
    
    
    
    var profileIdForUnitTest: String {
        get {
            return UserDefaults.standard.value(forKey: "profileIdForUnitTest") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "profileIdForUnitTest")
        }
    }

    var publicViewAlreadyShowOrNot: Bool {
        get {
            return UserDefaults.standard.value(forKey: "publicViewAlreadyShowOrNot") as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "publicViewAlreadyShowOrNot")
        }
    }
    
    var dyanamicLinkNotWorking: String {
        get {
            return UserDefaults.standard.value(forKey: "dyanamicLinkNotWorking") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "dyanamicLinkNotWorking")
        }
    }
    
    var isVerificationComplelteOrNot: Bool {
        get {
            return UserDefaults.standard.value(forKey: "isVerificationComplelteOrNot") as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "isVerificationComplelteOrNot")
        }
    }
    
    func saveDateCodeWithUserLocation(city:String,country:String,lat:String,long:String,dateCode:String,isSetData:Bool) -> (String,String,String,String,String) {
        var city = city
        var country = country
        var lat = lat
        var long = long
        var dateCode = dateCode
        if isSetData {
            let  dict = ["city":city,"country":country,"lat":lat,"long":long,"dateCode":dateCode]
            if UserDeafult.object(forKey: dateCode) != nil{
                UserDeafult.removeObject(forKey: dateCode)
            }
            UserDeafult.set(dict, forKey: dateCode)
        }else{
            if let stringDict = UserDefaults.standard.object(forKey: dateCode) as?  [String:String] {
                city = stringDict["city"]!
                country = stringDict["country"]!
                lat = stringDict["lat"]!
                long = stringDict["long"]!
                dateCode = stringDict["dateCode"]!
                return (city,country,lat,long,dateCode)
            }
        }
        return ("","","","","")
    }
    
    
    //MARK:- Is For First Tab Id
    var isLocallSelectFollowers: [String] {
        get {
            return UserDefaults.standard.value(forKey: SaveLocallySelectFollowersForInvite) as? [String] ?? []
        }
        set {
            UserDefaults.standard.set(newValue, forKey: SaveLocallySelectFollowersForInvite)
        }
    }
    
    //MARK:- Is For First Tab Id
    var isLocallSelectFriends: [String] {
        get {
            return UserDefaults.standard.value(forKey: SaveLocallySelectFriendsForInvite) as? [String] ?? []
        }
        set {
            UserDefaults.standard.set(newValue, forKey: SaveLocallySelectFriendsForInvite)
        }
    }
    
    var isLocallSelectGroupMembers: [String] {
        get {
            return UserDefaults.standard.value(forKey: SaveLocallySelectGroupForInvite) as? [String] ?? []
        }
        set {
            UserDefaults.standard.set(newValue, forKey: SaveLocallySelectGroupForInvite)
        }
    }
    
    //MARK:- Is For First Tab Id
    var isLocalBuild: Bool {
        get {
            return UserDefaults.standard.value(forKey: SaveProdOrLocalValue) as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: SaveProdOrLocalValue)
        }
    }
    
    //MARK:- Is For First Tab Id
    var isForFirstTabId: Int {
        get {
            return UserDefaults.standard.value(forKey: ForFirstTabId) as? Int ?? 0
        }
        set {
            UserDefaults.standard.set(newValue, forKey: ForFirstTabId)
        }
    }
    
    //MARK:- Is For First Tab Id
    var isComeFromDyanamic: Bool {
        get {
            return UserDefaults.standard.value(forKey: comeFromDyanamicLink) as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: comeFromDyanamicLink)
        }
    }
    
    
    
    //MARK:- Selected Tab Id
    var isForSecondTabId: Int {
        get {
            return UserDefaults.standard.value(forKey: ForSecondTabId) as? Int ?? 0
        }
        set {
            UserDefaults.standard.set(newValue, forKey: ForSecondTabId)
        }
    }
    
    //MARK:- Is For First Tab Id
    var isForThirdTabId: Int {
        get {
            return UserDefaults.standard.value(forKey: ForThirdTabId) as? Int ?? 0
        }
        set {
            UserDefaults.standard.set(newValue, forKey: ForThirdTabId)
        }
    }
    
    //MARK:- Is For Fourth Tab Id
    var isForFourthTabId: Int {
        get {
            return UserDefaults.standard.value(forKey: ForFourthTabId) as? Int ?? 0
        }
        set {
            UserDefaults.standard.set(newValue, forKey: ForFourthTabId)
        }
    }
    
    //MARK:- secuirytPartolModelWithOutInternt
    var SaveTimeStamp:Double {
        get {
            return UserDeafult.value(forKey: SaveTimeStampKey) as? Double ?? 0.0
        } set {
            UserDeafult.set(newValue, forKey: SaveTimeStampKey)
        }
    }
    
    //MARK:- Registration In Progress
    var registrationInProgress1: Int {
        get {
            return UserDefaults.standard.value(forKey: RegistrationPrgrss1Key) as? Int ?? 0
        }
        set {
            UserDefaults.standard.set(newValue, forKey: RegistrationPrgrss1Key)
        }
    }
    
    var saveTabBarUniqueIdForSecond: Int {
        get {
            return UserDefaults.standard.value(forKey: SaveTabBarUniqueIdForSecond) as? Int ?? 0
        }
        set {
            UserDefaults.standard.set(newValue, forKey: SaveTabBarUniqueIdForSecond)
        }
    }
    
    func setTimeStmpWithRideid(rideiD:String,timeStmp:Double){
        if UserDeafult.object(forKey: "\(rideiD)") != nil{
            UserDeafult.removeObject(forKey: rideiD)
        }
        rideWithTimeStmp["\(rideiD)"] = timeStmp
        UserDeafult.set(rideWithTimeStmp, forKey: AllSaveTimeStampRideKey)
    }
    
    //MARK:- Is For First Tab Id
    var isFirstTimeUserRegsiter: Bool {
        get {
            return UserDefaults.standard.value(forKey: SaveFirstTimeUserRegsiter) as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: SaveFirstTimeUserRegsiter)
        }
    }
    
    //MARK:- Is For First Tab Id
    var updateOnlyOneTimeFCMToken: Bool {
        get {
            return UserDefaults.standard.value(forKey: SaveOnlyOneTimeFCMToken) as? Bool ?? true
        }
        set {
            UserDefaults.standard.set(newValue, forKey: SaveOnlyOneTimeFCMToken)
        }
    }
    
    //MARK:- Is For First Tab Id
    var updateOnlyOneTimeForINviteAndShare: Bool {
        get {
            return UserDefaults.standard.value(forKey: UpdateForInviteAndShare) as? Bool ?? true
        }
        set {
            UserDefaults.standard.set(newValue, forKey: UpdateForInviteAndShare)
        }
    }
    
    
    func setDyanamicLinkLocal(type:String,source:String,data:String,isSetValue:Bool) -> (String,String,String) {
        var type1 = type
        var source1 = source
        var data1 = data
        if isSetValue {
            let  dict = ["type": type,"source":source,"data":data]
            UserDeafult.set(dict, forKey: SaveDyanamicLinkLocal)
            return ("","","")
        } else {
            if let strings = UserDefaults.standard.object(forKey: SaveDyanamicLinkLocal) as?  [String:String] {
                type1 = strings["type"]!
                source1 = strings["source"]!
                data1 = strings["data"]!
            }
            return (type1,source1,data1)
        }
    }
    
    func saveLocallyRideDetail(myRideForDay:DtrRide,isSetData:Bool,_ completion: @escaping (DtrRide) -> Void) {
        if isSetData {
            UserDeafult.removeObject(forKey: MyRideDetailLocally)
            do {
                try UserDeafult.setObject(myRideForDay, forKey: MyRideDetailLocally)
            } catch {
                print("Error While Save Locally Data = ",error.localizedDescription)
            }
        }else{
            do {
                let playingItMyWay = try UserDefaults.standard.getObject(forKey: MyRideDetailLocally, castTo: DtrRide.self)
                let ride = RideEditViewModel.init(ride: playingItMyWay)
                completion(ride.rideEdit)
            } catch {
                print("Error While Get Locally Data = ",error.localizedDescription)
            }
        }
    }
    
    func getRideDetailDetails()-> (String,String,String,String) {
        do {
            let playingItMyWay = try UserDefaults.standard.getObject(forKey: MyRideDetailLocally, castTo: DtrRide.self)
            let ride = RideEditViewModel.init(ride: playingItMyWay)
            return (ride.rideEdit.id,ride.rideEdit.leader,ride.rideEdit.summary,ride.rideEdit.dateCode)
        } catch {
            print("Error While Get Locally Data = ",error.localizedDescription)
        }
        return ("","","","")
    }
    
    //MARK:- Is For First Tab Id
    var isRootClassSearch: Bool {
        get {
            return UserDefaults.standard.value(forKey: SaveRootClassSearch) as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: SaveRootClassSearch)
        }
    }
    
    
    var shouldRecordGoDTRWeekendRide: Bool {
        get {
            return UserDefaults.standard.value(forKey: SaveShouldRecordGoDTRWeekendRide) as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: SaveShouldRecordGoDTRWeekendRide)
        }
    }
    
    var shouldRecordJoinWeekendRide: Bool {
        get {
            return UserDefaults.standard.value(forKey: SaveShouldRecordJoinWeekendRide) as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: SaveShouldRecordJoinWeekendRide)
        }
    }
    
    var shouldRecordComeBackRide: Bool {
        get {
            return UserDefaults.standard.value(forKey: SaveShouldRecordComeBackRide) as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: SaveShouldRecordComeBackRide)
        }
    }
    
    //MARK:- s
    var notificationMayBeDisabledData: [String] {
        get {
            return UserDefaults.standard.value(forKey: notificationMayBeDisabled) as? [String] ?? []
        }
        set {
            UserDefaults.standard.set(newValue, forKey: notificationMayBeDisabled)
        }
    }
}
