//
//  Dictionary+format.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/27/20.
//

import Foundation

extension Dictionary {
    func format(options: JSONSerialization.WritingOptions) -> Any?    {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: options)
            return try JSONSerialization.jsonObject(with: jsonData, options: [.allowFragments])
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func prettyFormatted() -> String     {
        if let formatted = format(options: [.prettyPrinted, .sortedKeys]) {
            return "\(formatted)"
        }
        
        return "\(self)"
    }
    
    func simpleFormatted() -> String    {
        if let formatted = format(options: [.withoutEscapingSlashes, .sortedKeys]) {
            return "\(formatted)"
        }
        
        return "\(self)"
    }
}
