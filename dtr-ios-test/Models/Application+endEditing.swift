//
//  Application+endEditing.swift
//  dtr-ios
//
//  Created by Dan Kinney on 4/28/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import UIKit

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
