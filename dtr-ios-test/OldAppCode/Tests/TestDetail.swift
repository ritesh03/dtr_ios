//
//  TestDetail.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 1/8/21.
//

import SwiftUI

struct TestDetail: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct TestDetail_Previews: PreviewProvider {
    static var previews: some View {
        TestDetail()
    }
}
