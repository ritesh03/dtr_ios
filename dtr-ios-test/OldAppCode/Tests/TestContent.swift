//
//  TestContent.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 1/8/21.
//

import SwiftUI
import Firebase

struct TestContent: View {
    @EnvironmentObject var dtrData: DtrData
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    
    @Binding var showActivity: Bool
    
    @State private var day: Int = 0;
    @State private var myProfileID: String = ""
    @State private var todaysRide: DtrRide = DtrRide.default
    @State private var todaysRideStatus = "Unknown Status"
    
    @State private var profileReadStatus = "ready"
    @State private var profileUpdateStatus = "ready"
    @State private var upcomingRidesStatus = "ready"
    @State private var rideReadStatus = "ready"
    @State private var rideUpdateStatus = "ready"
    @State private var rideCreateStatus = "ready"
    @State private var rideJoinStatus = "ready"
    @State private var rideLeaveStatus = "ready"
    @State private var rideCancelStatus = "ready"
    
    var testDateCode: String {
        return dateCode(offset: self.day)
    }
    
    var dayStatusMessage: String {
        if let ride = self.dtrData.myRidesByDate[self.testDateCode] {
            return ride.summary
        }
        
        return "Not riding"
    }
    
    func updateRide() {
        if let ride = self.dtrData.myRidesByDate[self.testDateCode] {
            self.todaysRide = ride
            self.todaysRideStatus = ride.summary
            return
        }
        self.todaysRide = DtrRide.default
        self.todaysRideStatus = "Not Riding"
    }
    
    var body: some View {
        List {
            Section(header: Text("Status: \(self.todaysRide.summary)")) {
                DayChooser(currentDay: self.$day)
            }
            
            Section(header: Text("Profile Tests")) {
                TestCard(testName: "Profile Read", testStatus: self.$profileReadStatus) {
                    self.profileReadStatus = "starting"
                    self.showActivity = true
                    
                    dtrCommand.profileRead(profileID: self.myProfileID) { result in
                        self.showActivity = false
                        self.profileReadStatus = result.feedback
                    }
                }
                
                TestCard(testName: "Upcoming Rides", testStatus: self.$upcomingRidesStatus) {
                    self.upcomingRidesStatus = "starting"
                    self.showActivity = true
                    
                    dtrCommand.upcomingRides(profileID: self.myProfileID) { result in
                        self.showActivity = false
                        self.upcomingRidesStatus = result.feedback
                    }
                }
                
                TestCard(testName: "Profile Update", testStatus: self.$profileUpdateStatus) {
                    self.profileUpdateStatus = "starting"
                    self.showActivity = true
                    
                    dtrCommand.profileUpdate(profileID: myProfileID, data: ["defaultStatus": "Profile Updated"]) { result in
                        self.showActivity = false
                        self.profileUpdateStatus = result.feedback
                    }
                }
            }
            
            Section(header: Text("Ride Tests")) {
                TestCard(testName: "Ride Read", testStatus: self.$rideReadStatus) {
                    self.rideReadStatus = "starting"
                    let rideID = "\(self.myProfileID)+\(testDateCode)"
                    self.showActivity = true
                    
                    dtrCommand.rideRead(rideID: rideID) { result in
                        self.showActivity = false
                        self.rideReadStatus = result.feedback
                    }
                }
                
                TestCard(testName: "Ride Update", testStatus: self.$rideUpdateStatus) {
                    self.rideUpdateStatus = "starting"
                    let rideID = "\(self.myProfileID)+\(testDateCode)"
                    self.showActivity = true
                    
                    dtrCommand.rideUpdate(rideID: rideID, data: ["summary": "summary updated"]) { result in
                        self.showActivity = false
                        self.rideUpdateStatus = result.feedback
                    }
                }
                
                TestCard(testName: "Ride Create", testStatus: self.$rideCreateStatus) {
                    self.rideCreateStatus = "starting"
                    self.showActivity = true
                    dtrCommand.rideCreate(dateCode: testDateCode, data: ["summary": "test summary"]) { result in
                        self.showActivity = false
                        self.rideCreateStatus = (result.result == .error) ? result.feedback : "done"
                    }
                }
                
                TestCard(testName: "Ride Join", testStatus: self.$rideJoinStatus) {
                    let rideID = "\(self.myProfileID)+\(testDateCode)"
                    self.rideJoinStatus = "starting"
                    self.showActivity = true
                    
                    dtrCommand.rideLeave(rideID: rideID, dateCode: testDateCode) { result in
                        self.showActivity = false
                        self.rideJoinStatus = result.feedback
                    }
                }
                
                TestCard(testName: "Ride Leave", testStatus: self.$rideLeaveStatus) {
                    self.rideLeaveStatus = "starting"
                    let rideID = "\(self.myProfileID)+\(testDateCode)"
                    self.showActivity = true
                    
                    dtrCommand.rideLeave(rideID: rideID, dateCode: testDateCode) { result in
                        self.showActivity = false
                        self.rideLeaveStatus = (result.result == .error) ? result.feedback : "done"
                    }
                }
                
                TestCard(testName: "Ride Cancel", testStatus: self.$rideCancelStatus) {
                    self.rideCancelStatus = "starting"
                    let rideID = "\(self.myProfileID)+\(testDateCode)"
                    self.showActivity = true
                    
                    dtrCommand.rideCancel(rideID: rideID,dateCode:testDateCode, message: "ride was cancelled") { result in
                        self.showActivity = false
                        self.rideCancelStatus = result.feedback
                    }
                }
            }
        }
        .listStyle(GroupedListStyle())
        .onAppear {
            if let currentUser = Auth.auth().currentUser {
                self.myProfileID = currentUser.uid
            }
        }
        .onChange(of: self.day) { newDayValue in
            self.updateRide()
        }
        .onReceive(self.dtrData.$myRidesByDate) { rideDict in
            self.updateRide()
        }
        .buttonStyle(ActionButtonStyle())
    }
}

struct TestContent_Previews: PreviewProvider {
    static var previews: some View {
        TestContent(showActivity: .constant(false))
    }
}
