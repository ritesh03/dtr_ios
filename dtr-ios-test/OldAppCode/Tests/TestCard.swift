//
//  TestCard.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 1/8/21.
//

import SwiftUI

struct TestCard: View {
    var testName: String
    @Binding var testStatus: String
    var testAction: () -> Void = {}
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(testName).fontWeight(.semibold)
                Text(testStatus).font(.caption)
            }
            
            Spacer()
            Button(action: {
                self.testAction()
                
            }) {
                Image(systemName: "play")
            }
        }
    }
}

struct TestCard_Previews: PreviewProvider {
    static var previews: some View {
        TestCard(testName: "Test Name", testStatus: .constant("")) {
            print("test activated")
        }
    }
}
