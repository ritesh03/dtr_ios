//
//  TestsTab.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 1/8/21.
//

import SwiftUI

struct TestsTab: View {
    @Binding var showActivity: Bool
    @Binding var showNotifications: Bool
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        NavigationView {
            VStack {
                TestContent(showActivity: self.$showActivity)
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    if self.showActivity {
                        ProgressView()
                            .font(.title)
                            .padding()
                    }
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {
                        self.showNotifications = true
                    }) {
                        Image(systemName: "bell").font(.title)
                    }
                }
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("API Tests", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }) {
                                        HStack {
                                            Image(systemName: "arrow.left")
                                        }})
        }
    }
}

struct TestsTab_Previews: PreviewProvider {
    static var previews: some View {
        TestsTab(showActivity: .constant(false), showNotifications: .constant(false))
    }
}
