//
//  CommunityCard.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/27/20.
//

import SwiftUI

struct CommunityCard: View {
    var body: some View {
        HStack {
            RoundedRectangle(cornerRadius: 10, style: .continuous)
            .fill(Color.gray)
            .frame(width: 40, height: 40)
            .padding(.trailing, 8)
            
            VStack(alignment: .leading) {
                Text("Author Name")
                Text("Summary")
                    .fontWeight(.semibold)
                    .lineLimit(2)
            }.font(.caption)
            
            Spacer()
        }
    }
}

struct CommunityCard_Previews: PreviewProvider {
    static var previews: some View {
        CommunityCard()
    }
}
