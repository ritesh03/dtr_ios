//
//  CommunityTab.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/24/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI

struct CommunityTab: View {
    @Binding var showActivity: Bool
    @Binding var showNotifications: Bool
    
    var body: some View {
        NavigationView {
            VStack {
                CommunityContent()
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    if self.showActivity {
                        ProgressView()
                            .font(.title)
                            .padding()
                    }
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {
                        self.showNotifications = true
                    }) {
                        Image(systemName: "bell").font(.title)
                    }
                }
            }
            .navigationTitle("Community")
        }
    }
}

struct CommunityTab_Previews: PreviewProvider {
    static var previews: some View {
        CommunityTab(showActivity: .constant(false), showNotifications: .constant(false))
    }
}
