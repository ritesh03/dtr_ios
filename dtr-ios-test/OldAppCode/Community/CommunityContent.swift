//
//  CommunityContent.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/26/20.
//

import SwiftUI

struct CommunityContent: View {
    var body: some View {
        List {
            CommunityHeader()
            
            Section(header: HStack {
                Text("Latest Activity")
                Spacer()

            }) {
                ForEach(0..<10) { index in
                    NavigationLink(destination: Placeholder()) {
                        CommunityCard()
                    }
                }
            }
        }
        .listStyle(GroupedListStyle())
        .buttonStyle(ActionButtonStyle())
    }
}

struct CommunityContent_Previews: PreviewProvider {
    static var previews: some View {
        CommunityContent()
    }
}
