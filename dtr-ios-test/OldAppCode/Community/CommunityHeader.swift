//
//  CommunityHeader.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/26/20.
//

import SwiftUI

struct CommunityHeader: View {
    var body: some View {
        VStack(alignment: .center) {
            HStack {
                Text("DTR is all about the cycling community")
                .font(.caption)
                
                Spacer()
            }
            
            RoundedRectangle(cornerRadius: 50, style: .continuous)
            .fill(Color.gray)
            .frame(width: 200, height: 200)
            .padding()
            
//            Button(action: {
//                
//            }) {
//                Text("Visit Facebook Page")
//            }
//            .padding()
        }
        .buttonStyle(ActionButtonStyle())
    }
}

struct CommunityHeader_Previews: PreviewProvider {
    static var previews: some View {
        CommunityHeader()
    }
}
