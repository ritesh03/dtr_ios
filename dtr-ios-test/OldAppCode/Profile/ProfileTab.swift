//
//  ProfileTab.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/24/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI
import Firebase

struct ProfileTab: View {
    @EnvironmentObject var dtrData: DtrData
    
    @Binding var signedIn: Bool
    @Binding var showActivity: Bool
    @Binding var showNotifications: Bool
    @State private var showSettings: Bool = false
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        NavigationView {
            ProfileContent(profile: self.dtrData.profile, showActivity: self.$showActivity)
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    if self.showActivity {
                        ProgressView()
                            .font(.title)
                            .padding()
                    }
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {
                        self.showSettings = true
                    }) {
                        Image(systemName: "gearshape").font(.title)
                    }
                }
            }
            .navigationTitle("My Profile")
        }
    
        .background(EmptyView().sheet(isPresented: self.$showSettings, content: {
            VStack {
                Button(action: {
                    print("Signing out...")
                    dtrData.signOut(completion: { success in })
                    self.signedIn = false
                    self.showSettings = false
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Text("Sign Out")
                }
                .buttonStyle(ActionButtonStyle())
                .padding()
                
                Spacer()
            }
            .padding()
        }))
    }
}

struct ProfileTab_Previews: PreviewProvider {
    static var previews: some View {
        ProfileTab(signedIn: .constant(true), showActivity: .constant(false), showNotifications: .constant(false))
    }
}
