//
//  ProfileHeader.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/26/20.
//

import SwiftUI

struct ProfileHeader: View {
    @EnvironmentObject var dtrData: DtrData
    
    var profile: DtrProfile
    
    var body: some View {
        VStack(alignment: .center) {
            HStack {
                Spacer()
                
                Circle()
                .fill(Color.gray)
                .frame(width: 150, height: 150)
                .padding()
                
                Spacer()
            }
            
            Text(self.profile.description).font(.caption)
                
            HStack {
                Image(systemName: "rosette")
                Text("12,987")
                    .fontWeight(.bold)
            }
            .padding()
            
            VStack {
                Text(self.profile.cityState)
                Text("DTR since \(memberSince(self.profile.activated))")
            }
            .font(.caption)
            .padding()
            
//            HStack {
//                Text("Send friend request to see friends-only rides and information")
//                    .font(.caption)
//                Spacer()
//            }
//            .padding()
        }
    }
}

struct ProfileHeader_Previews: PreviewProvider {
    static var previews: some View {
        ProfileHeader(profile: DtrProfile.default)
    }
}
