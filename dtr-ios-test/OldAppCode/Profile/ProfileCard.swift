//
//  ProfileCard.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/27/20.
//

import SwiftUI

struct ProfileCard: View {
    @State var profile: DtrProfile
    
    var body: some View {
        HStack {
            Circle()
            .fill(Color.gray)
            .frame(width: 40, height: 40)
            .padding(.trailing, 8)
            
            VStack(alignment: .leading) {
                Text(profile.name)
                    .fontWeight(.semibold)
                
                // Text("Last Status Message")
                
                // ProfileSocialCompact()
                
            }.font(.caption)
            
            Spacer()
        }
    }
}

struct ProfileCard_Previews: PreviewProvider {
    static var previews: some View {
        ProfileCard(profile: DtrProfile.default)
    }
}
