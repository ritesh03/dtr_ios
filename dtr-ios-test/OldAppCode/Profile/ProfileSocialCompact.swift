//
//  ProfileSocialCompact.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/27/20.
//

import SwiftUI

struct ProfileSocialCompact: View {
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Image(systemName: "square.and.arrow.down")
                Text("258").fontWeight(.semibold)
            }
            
            Spacer()
            
            VStack(alignment: .leading) {
                Image(systemName: "square.and.arrow.up")
                Text("1,363").fontWeight(.semibold)
            }
            
            Spacer()
            
            VStack(alignment: .leading) {
                Image(systemName: "person.2")
                Text("32").fontWeight(.semibold)
            }
            
        }.font(.caption)
    }
}

struct ProfileSocialCompact_Previews: PreviewProvider {
    static var previews: some View {
        ProfileSocialCompact()
    }
}
