//
//  Onboarding.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/26/20.
//

import SwiftUI
import Firebase

struct Onboarding: View {
    
    @Binding var signedIn: Bool
    @State var phoneNumber: String = ""
    @State var firstResponder = true
    @State var feedback = ""
    @State var showCodeEntry = false
    @State var verificationID = ""
    @State var verificationCode: String = ""
    
    func verifyPhone(verificationPhone: String, completion: @escaping (String?, Error?) -> Void = { verificationID, error in }) {
        PhoneAuthProvider.provider().verifyPhoneNumber(verificationPhone, uiDelegate: nil) { verificationID, error in
            if let error = error {
                print("PhoneAuthProvider error: \(error.localizedDescription)")
            }
            completion(verificationID, error)
        }
    }
    
    func signIn(id: String, code: String, completion: @escaping(String) -> Void) {
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: id, verificationCode: code)
        
        Auth.auth().signIn(with: credential) { authResult, error in
            if let error = error {
                print("sign in error: \(error.localizedDescription)")
                self.feedback = error.localizedDescription
            }
            
            if let result = authResult {
                completion(result.user.uid)
            }
            
            self.showCodeEntry = false
        }
    }
    
    func linkPhone(id: String, code: String, completion: @escaping (Error?) -> Void = { error in } ) {
        guard let prevUser = Auth.auth().currentUser else {
            print("you must first already be logged in!")
            return
        }
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: id, verificationCode: code)
        
        prevUser.link(with: credential) { authResult, error in
            if let result = authResult {
                print("auth was successful for \(result.user.uid)")
            }
            
            completion(error)
        }
    }
    
    var body: some View {
        VStack {
            Text("Sign In").font(.title).fontWeight(.bold)
                .padding()
            
            ResponderTextField("+12485551212", text: $phoneNumber, isFirstResponder: $firstResponder, keyboardType: .phonePad)
                .padding(8)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(showCodeEntry ? Color.gray : Color.blue, lineWidth: 1))
                .frame(height: 40)
                .padding(.horizontal)
                .disabled(showCodeEntry)
            
            Button(action: {
                print("verifying \(phoneNumber)")
                self.feedback = "Verifying phone number \(phoneNumber)"
                
                verifyPhone(verificationPhone: phoneNumber) { verificationID, error in
                    if let error = error {
                        self.feedback = error.localizedDescription
                        print("error verifying phone: \(error.localizedDescription)")
                        return
                    }
                    
                    if let id = verificationID {
                        self.verificationID = id
                        self.feedback = "Enter the code just sent to this number:"
                        showCodeEntry = true
                    }
                }
                
            }) {
                Text("Verify")
            }
            .buttonStyle(ActionButtonStyle())
            .padding()
            .disabled(showCodeEntry)
            
            Text(feedback)
                .font(.caption)
                .padding()
            
            if showCodeEntry {
                ResponderTextField("Verification Code", text: $verificationCode, isFirstResponder: $firstResponder, keyboardType: .phonePad)
                    .padding(8)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.blue, lineWidth: 1))
                    .frame(height: 40)
                    .padding(.horizontal)
                
                Button(action: {
                    signIn(id: verificationID, code: verificationCode) { newUserID in
                        print("signed in as \(newUserID)")
                        self.signedIn = true
                    }
                    
//                    linkPhone(id: verificationID, code: verificationCode) { error in
//                        if let error = error {
//                            self.feedback = error.localizedDescription
//                            print("linkPhone error: \(error.localizedDescription)")
//                            return
//                        }
//
//                        self.feedback = "Phone is Linked"
//                    }
                    
                }) {
                    Text("Let's Do This!")
                }
                .buttonStyle(ActionButtonStyle())
                .padding()
            }
            
            Spacer()
        }
    }
}

struct Onboarding_Previews: PreviewProvider {
    static var previews: some View {
        Onboarding(signedIn: .constant(false))
    }
}
