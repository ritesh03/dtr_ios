//
//  ProfileContent.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/28/20.
//

import SwiftUI

struct ProfileContent: View {
    @EnvironmentObject var dtrData: DtrData
    
    @State var profile: DtrProfile
    @Binding var showActivity: Bool
    
    @State private var friends = [String:DtrProfile]()
    @State private var upcomingRides = [DtrRide]()
    
    var sortedFriends: [DtrProfile] {
        return Array(self.friends.values).sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
    }
    
    var body: some View {
        VStack {
            List {
                ProfileHeader(profile: profile)
                
                if profile.id != dtrData.profile.id {
                    Section(header: HStack {
                        Text("Friends")
                        Spacer()
                        Text("\(self.sortedFriends.count)")

                    }) {
                        ForEach(self.sortedFriends, id:\.self) { profile in
                            NavigationLink(destination: ProfileContent(profile: profile, showActivity: self.$showActivity)) {
                                ProfileCard(profile: profile)
                            }
                        }
                    }
                } else {
                    Section(header: HStack {
                        Text("Upcoming Rides")
                        Spacer()
                        Text("\(self.upcomingRides.count)")

                    }) {
                        ForEach(self.upcomingRides, id:\.self) { ride in
                            NavigationLink(destination: RideContent(ride: ride, showActivity: self.$showActivity)) {
                                RideCard(ride: ride)
                            }
                        }
                    }
                }
            }
            .listStyle(GroupedListStyle())
        }
        .onAppear {
            dtrData.resolveFriendProfiles(profile: self.profile) { resolvedProfile in
                self.friends = resolvedProfile.friendProfiles
            }
        }
        .onReceive(self.dtrData.$myRidesByDate) { _ in
            // rebuild upcomingRides in case they may have changed
            //print("[myRides] rebuilding upcoming rides for \(self.profile.name)")

            self.dtrData.upcomingRides(for: self.profile) { profileRides in
                // note: these rides are already resolved
                self.upcomingRides = profileRides
            }
        }
        
        .onReceive(self.dtrData.$suggestedRidesByDate) { _ in
            // rebuild upcomingRides in case they may have changed
            //print("[suggestedRides] rebuilding upcoming rides for \(self.profile.name)")

            self.dtrData.upcomingRides(for: self.profile) { profileRides in
                // note: these rides are already resolved
                self.upcomingRides = profileRides
            }
        }
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                if self.showActivity {
                    ProgressView()
                        .font(.title)
                        .padding()
                }
            }
            ToolbarItem(placement: .navigationBarTrailing) {
                Button(action: {
                }) {
                    Image(systemName: "person.badge.plus").font(.title)
                }
            }
        }
        .navigationTitle(profile.name)
    }
}

struct ProfileContent_Previews: PreviewProvider {
    static var previews: some View {
        ProfileContent(profile: DtrProfile.default, showActivity: .constant(false))
    }
}
