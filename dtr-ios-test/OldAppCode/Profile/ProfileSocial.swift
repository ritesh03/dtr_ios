//
//  ProfileSocial.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/27/20.
//

import SwiftUI

struct ProfileSocial: View {
    var body: some View {
        VStack {
            HStack {
                VStack(alignment: .leading) {
                    Text("258").fontWeight(.bold)
                    Text("Following").font(.caption)
                }
                
                Spacer()
                
                VStack(alignment: .leading) {
                    Text("1,363").fontWeight(.bold)
                    Text("Followers").font(.caption)
                }
                
                Spacer()
                
                VStack(alignment: .leading) {
                    Text("32").fontWeight(.bold)
                    Text("Friends").font(.caption)
                }
            }
            .padding()
            
            Button(action: {
                
            }) {
                Text("Follow")
                    .font(.caption)
            }
            .buttonStyle(ActionButtonStyle())
            .padding()
        }
    }
}

struct ProfileSocial_Previews: PreviewProvider {
    static var previews: some View {
        ProfileSocial()
    }
}
