//
//  CreateRide.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 12/31/20.
//

import SwiftUI

struct CreateRide: View {
    @Binding var presenting: Bool
    var completion: (String) -> Void = { updatedMessage in }
    
    @State private var statusMessage: String = ""
    
    var body: some View {
        VStack {
            Handle()
            
            HStack {
                VStack(alignment: .leading) {
                    Text("Going DTR...").fontWeight(.semibold)
                    Text("Add a custom status so your friends know what you're thinking about").font(.caption)
                }
                
                Spacer()
            }
            
            HStack {
                ResponderTextField("Status", text: self.$statusMessage, isFirstResponder: .constant(true), keyboardType: .default, showDone: true)
                    .padding(8)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.blue, lineWidth: 1))
                    .frame(height: 40)
//                    .padding(.horizontal)
            }
            .padding(.vertical)
            
            HStack {
                Button(action: {
                    // kind of a hack
                    self.presenting = false
                    completion(statusMessage)
                    
                }) {
                    Text("Create Ride")
                }
                .buttonStyle(ActionButtonStyle())
            }
            
            Spacer()
        }
        .padding()
    }
}

struct CreateRide_Previews: PreviewProvider {
    static var previews: some View {
        CreateRide(presenting: .constant(true))
    }
}
