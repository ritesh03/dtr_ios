//
//  RidesTab.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 12/31/20.
//

import SwiftUI

struct RidesTab: View {
    @ObservedObject var dtrData = DtrData.sharedInstance
    @Binding var day: Int
    @Binding var showActivity: Bool
    @Binding var showNotifications: Bool
    
    @State private var myRideForDay: DtrRide?
    
    var body: some View {
        NavigationView {
            RidesForDay(ridesForDay: RidesForDayFactory(ride: self.$myRideForDay, day: self.$day, showActivity: self.$showActivity))
                .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    if self.showActivity {
                        ProgressView()
                            .font(.title)
                            .padding()
                    }
                }
                
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {
                        self.showNotifications = true
                    }) {
                        Image(systemName: "bell").font(.title)
                    }
                }
            }
            .navigationTitle("My Rides")
//            .onChange(of: self.day) { (updateDay) in
//                self.myRideForDay = self.dtrData.myRidesByDate[dateCode(offset: updateDay)]
//            }
//            .onReceive(self.dtrData.$myRidesByDate) { rideDict in
//                self.myRideForDay = rideDict[dateCode(offset: day)]
//            }
            .onChange(of: self.day) { (updateDay) in
                self.myRideForDay = self.dtrData.myRidesByDate[dateCode(offset: updateDay)]
            }
            .onReceive(self.dtrData.$myRidesByDate) { rideDict in
                self.myRideForDay = rideDict[dateCode(offset: day)]
            }
        }
    }
}

struct RidesTab_Previews: PreviewProvider {
    static var previews: some View {
        RidesTab(day: .constant(0), showActivity: .constant(false), showNotifications: .constant(false))
    }
}
