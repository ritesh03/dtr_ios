//
//  DayButton.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/24/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI

struct DayButton: View {
    var offset = 0
    var active = false
    var action: (Int) -> Void = { offset in }
    
    var label: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "E"
        
        let date = dateFrom(offset: offset)
        
        return String(formatter.string(from: date).prefix(3).uppercased())
    }
    
    var body: some View {
        Button (action: {
            action(offset)
        }) {
            Text(label).font(.caption)
        }.buttonStyle(CircleButtonStyleModifiy(active: active))
    }
}

struct DayButton_Previews: PreviewProvider {
    static var previews: some View {
        DayButton()
    }
}
