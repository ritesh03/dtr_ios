//
//  RideVisibility.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/29/20.
//

import SwiftUI

struct RideVisibilityDan: View {
    @Binding var ride: DtrRide
    @Binding var wasChanged: Bool
    var editable: Bool = false
    
    @State var visibilityState: DtrRideVisibilityState = .friends
    
    func rideIsDifferent() -> Bool {
        self.ride.details.visibility != visibilityState
    }
    
    func saveChanges() {
        // todo
    }
    
    var body: some View {
        ScrollView {
            VStack {
                HStack {
                    Text("Who can see and join this ride?").font(.caption)
                    
                    Spacer()
                }
                .onAppear {
                    self.visibilityState = self.ride.details.visibility
                }
                .padding()
                
                HStack {
                    Image(systemName: "circle.fill").font(.largeTitle)
                    
                    VStack(alignment: .leading) {
                        Text(DtrRideVisibilityState.friends.name).font(.caption).fontWeight(.bold)
                        Text(DtrRideVisibilityState.friends.description).font(.caption)
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        self.visibilityState = .friends
                        self.wasChanged = rideIsDifferent()
                    }) {
                        Image(systemName: (self.visibilityState == .friends) ? "checkmark.circle.fill": "circle.fill").font(.title)
                    }
                    .disabled(!editable)
                    
                }
                .padding()
                
                HStack {
                    Image(systemName: "circle.fill").font(.largeTitle)
                    
                    VStack(alignment: .leading) {
                        Text(DtrRideVisibilityState.followers.name).font(.caption).fontWeight(.bold)
                        Text(DtrRideVisibilityState.followers.description).font(.caption)
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        self.visibilityState = .followers
                        self.wasChanged = rideIsDifferent()
                    }) {
                        Image(systemName: (self.visibilityState == .followers) ? "checkmark.circle.fill": "circle.fill").font(.title)
                    }
                    .disabled(!editable)
                }
                .disabled(!editable)
                .padding()
                
                HStack {
                    Image(systemName: "circle.fill").font(.largeTitle)
                    
                    VStack(alignment: .leading) {
                        Text(DtrRideVisibilityState.public.name).font(.caption).fontWeight(.bold)
                        Text(DtrRideVisibilityState.public.description).font(.caption)
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        self.visibilityState = .public
                        self.wasChanged = rideIsDifferent()
                    }) {
                        Image(systemName: (self.visibilityState == .public) ? "checkmark.circle.fill": "circle.fill").font(.title)
                    }
                    .disabled(!editable)
                }
                .disabled(!editable)
                .padding()
            }
        }
        .navigationBarTitle("Ride Visibility")
        .navigationBarItems(trailing:
            Button(action: {
                self.saveChanges()

            }) {
                Text("Update")
            }
            .disabled(!editable)
        )
    }
}

struct RideVisibilityDan_Previews: PreviewProvider {
    static var previews: some View {
        RideVisibilityDan(ride: .constant(DtrRide.default), wasChanged: .constant(false))
    }
}
