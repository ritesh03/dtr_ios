//
//  RideCard.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/27/20.
//

import SwiftUI

struct RideCard: View {
    @EnvironmentObject var dtrData: DtrData
    
    @State var ride: DtrRide
    
    @State private var leaderName = " "
    
    var timeAndLocationString: String {
        if ride.details.hasStartTime && ride.details.hasStartPlace {
            return "\(ride.details.startTime) • \(ride.details.startPlace)"
        }
        
        if ride.details.hasStartTime {
            return ride.details.startTime
        }
        
        if ride.details.hasStartPlace {
            return ride.details.startPlace
        }
        
        return ""
    }
    
    var body: some View {
        VStack {
            HStack {
                VStack(alignment: .leading) {
                    Text(ride.summary).fontWeight(.semibold)
                    Text(ride.riderProfiles[ride.leader]?.name ?? "Leader")
                }.font(.caption)
                
                Spacer()
                
//                RoundedRectangle(cornerRadius: 10, style: .continuous)
//                .fill(Color.gray)
//                .frame(width: 50, height: 50)
            }
            
            RideRiders(ride: self.ride)
            
            HStack {
                Text(self.timeAndLocationString)
                Spacer()
            }
            .font(.caption)
            RideSkillIndicators(ride: $ride)
            // Text(ride.details.attributes.map{$0.name}.joined(separator: " • ")).font(.caption)
        }
        .padding(.top)
    }
}

struct RideCard_Previews: PreviewProvider {
    static var previews: some View {
        RideCard(ride: DtrRide.default)
    }
}
