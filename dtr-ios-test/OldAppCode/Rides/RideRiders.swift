//
//  RideRiders.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/26/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI

struct RideRiders: View {
    var ride: DtrRide
    
    @State private var leaderProfile = DtrProfile()
    @State private var friendProfiles = Set<DtrProfile>()
    @State private var nonFriendProfiles = Set<DtrProfile>()
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                Circle()
                .fill(Color.gray)
                .frame(width: 32, height: 32)
                .padding(.leading, 2)
                .padding(.trailing, 8)
            
                ForEach(Array(self.friendProfiles), id: \.self) { profile in
                    Circle()
                    .fill(Color.gray)
                    .frame(width: 24, height: 24)
                }
                
                ForEach(Array(self.nonFriendProfiles), id: \.self) { profile in
                    Circle()
                    .fill(Color.gray)
                    .frame(width: 24, height: 24)
                }
                
                Spacer()
            }
            .onAppear {
                if let profile = ride.riderProfiles[ride.leader] {
                    self.leaderProfile = profile
                }
                
                for rider in ride.riders {
                    if let profile = ride.riderProfiles[rider] {
                        if rider == ride.leader {
                            continue
                        }
                        
                        if self.leaderProfile.friends.contains(rider) {
                            self.friendProfiles.insert(profile)
                        } else {
                            self.nonFriendProfiles.insert(profile)
                        }
                    }
                }
            }
        }
        .disabled(true)
    }
}

struct RideRiders_Previews: PreviewProvider {
    static var previews: some View {
        RideRiders(ride: DtrRide.default)
    }
}
