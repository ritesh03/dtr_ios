//
//  RideStartPlace.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/29/20.
//

import SwiftUI

struct RideStartPlace: View {
    var ride: DtrRide
    var editable: Bool = false
    
    var body: some View {
        VStack {
            Text("Ride Start Place")
        }
        .navigationBarTitle("Start Place")
        .navigationBarItems(trailing:
            Button(action: {
                
            }) {
                Text("Update")
            }
            .disabled(!editable)
        )
    }
}

struct RideStartPlace_Previews: PreviewProvider {
    static var previews: some View {
        RideStartPlace(ride: DtrRide.default)
    }
}
