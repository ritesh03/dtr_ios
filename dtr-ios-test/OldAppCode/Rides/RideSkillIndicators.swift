//
//  RideSkillIndicators.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/29/20.
//

import SwiftUI
import Firebase

struct RideSkillIndicators: View {
    
    @Binding var ride: DtrRide
    var showNone = false
    
    var noneDefined: Bool {
        !ride.details.skillA
            && !ride.details.skillB
            && !ride.details.skillC
            && !ride.details.skillD
            && !ride.details.skillS
    }
    
    var body: some View {
        if (ride.details.skillA || ride.details.skillB || ride.details.skillC || ride.details.skillD || ride.details.skillS || ride.details.notes != "") {
            HStack(alignment: .center) {
                if ride.details.skillA {
                    showSkilsImage(imgName: "DTR-SkiilA")
                }
                
                if ride.details.skillB {
                    showSkilsImage(imgName: "DTR-SkiilB")
                }
                
                if ride.details.skillC {
                    showSkilsImage(imgName: "DTR-SkiilC")
                }
                
                if ride.details.skillD {
                    showSkilsImage(imgName: "DTR-SkiilD")
                }
                
                if ride.details.skillS {
                    showSkilsImage(imgName: "DTR-SkiilS")
                }
                Spacer()
            }
            .onAppear{
                Analytics.logEvent(AnalyticsScreen.aboutSkills.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
            }
        }else{
            EmptyView()
        }
    }
}

func showSkilsImage(imgName:String) -> AnyView {
    return AnyView(
        Image(imgName)
            .resizable()
            .frame(width: 25, height: 25, alignment: .center)
            .cornerRadius(10)
    )
}

struct RideSkillIndicators_Previews: PreviewProvider {
    static var previews: some View {
        RideSkillIndicators(ride: .constant(DtrRide.default))
    }
}
