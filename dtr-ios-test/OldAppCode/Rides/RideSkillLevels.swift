//
//  RideSkillLevelsDan.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/29/20.
//

import SwiftUI

struct RideSkillLevelsDan: View {
    @Binding var ride: DtrRide
    @Binding var wasChanged: Bool
    var editable: Bool = false
    
    @State var skillA: Bool = false
    @State var skillB: Bool = false
    @State var skillC: Bool = false
    @State var skillD: Bool = false
    @State var skillS: Bool = false
    
    @State var confirm = false
    
    func rideIsDifferent() -> Bool {
        return ( self.skillA != self.ride.details.skillA
            || self.skillB != self.ride.details.skillB
            || self.skillC != self.ride.details.skillC
            || self.skillD != self.ride.details.skillD
            || self.skillS != self.ride.details.skillS )
    }
    
    func saveChanges() {
        
    }
    

    
    var body: some View {
        ScrollView {
            VStack {
                HStack {
                    if editable {
                        Text("Select the skill levels that you're expecting for the ride. You can include multiple levels.").font(.caption)
                    } else {
                        Text("Joining a ride the appropriate skill levels will ensure a good experience.").font(.caption)
                    }
                    
                    Spacer()
                }
                .padding()
                
                HStack {
                    Image(systemName: "a.circle.fill").font(.largeTitle).foregroundColor(Color.init("DTR-SkiilA"))
                    
                    VStack(alignment: .leading) {
                        Text("Expert / Elite").font(.caption).fontWeight(.bold)
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.").font(.caption)
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        self.skillA.toggle()
                        self.wasChanged = rideIsDifferent()
                    }) {
                        Image(systemName: (self.skillA) ? "checkmark.circle.fill": "circle.fill").font(.title)
                    }
                    .onAppear {
                        self.skillA = ride.details.skillA
                    }
                    .disabled(!editable)
                    
                }
                .padding()
                
                HStack {
                    Image(systemName: "b.circle.fill").font(.largeTitle).foregroundColor(Color.init("DTR-SkiilB"))
                    
                    VStack(alignment: .leading) {
                        Text("Expert and Fast Sport").font(.caption).fontWeight(.bold)
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.").font(.caption)
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        self.skillB.toggle()
                        self.wasChanged = rideIsDifferent()
                    }) {
                        Image(systemName: (self.skillB) ? "checkmark.circle.fill": "circle.fill").font(.title)
                    }
                    .onAppear {
                        self.skillB = ride.details.skillB
                    }
                    .disabled(!editable)
                }
                .disabled(!editable)
                .padding()
                
                HStack {
                    Image(systemName: "c.circle.fill").font(.largeTitle).foregroundColor(Color.init("DTR-SkiilC"))
                    
                    VStack(alignment: .leading) {
                        Text("Sport and Fast Beginner").font(.caption).fontWeight(.bold)
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.").font(.caption)
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        self.skillC.toggle()
                        self.wasChanged = rideIsDifferent()
                    }) {
                        Image(systemName: (self.skillC) ? "checkmark.circle.fill": "circle.fill").font(.title)
                    }
                    .onAppear {
                        self.skillC = ride.details.skillC
                    }
                    .disabled(!editable)
                }
                .disabled(!editable)
                .padding()
                
                HStack {
                    Image(systemName: "d.circle.fill").font(.largeTitle).foregroundColor(Color.init("DTR-SkiilD"))
                    
                    VStack(alignment: .leading) {
                        Text("Beginner - No Drop").font(.caption).fontWeight(.bold)
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.").font(.caption)
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        self.skillD.toggle()
                        self.wasChanged = rideIsDifferent()
                    }) {
                        Image(systemName: (self.skillD) ? "checkmark.circle.fill": "circle.fill").font(.title)
                    }
                    .onAppear {
                        self.skillD = ride.details.skillD
                    }
                    .disabled(!editable)
                }
                .disabled(!editable)
                .padding()
                
                HStack {
                    Image(systemName: "s.circle.fill").font(.largeTitle).foregroundColor(Color.init("DTR-SkiilS"))
                    
                    VStack(alignment: .leading) {
                        Text("Social ride or other non-ride event").font(.caption).fontWeight(.bold)
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.").font(.caption)
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        self.skillS.toggle()
                        self.wasChanged = rideIsDifferent()
                    }) {
                        Image(systemName: (self.skillS) ? "checkmark.circle.fill": "circle.fill").font(.title)
                    }
                    .onAppear {
                        self.skillS = ride.details.skillS
                    }
                    .disabled(!editable)
                }
                .disabled(!editable)
                .padding()
                
            }
        }
        .navigationBarTitle("Skill Levels")
        .navigationBarItems(trailing:
            Button(action: {
                self.confirm = true
            }) {
                Text("Update")
            }
            .disabled(!editable)
        )
    }
}

struct RideSkillLevels_Previews: PreviewProvider {
    static var previews: some View {
        RideSkillLevelsDan(ride: .constant(DtrRide.default), wasChanged: .constant(false))
    }
}
