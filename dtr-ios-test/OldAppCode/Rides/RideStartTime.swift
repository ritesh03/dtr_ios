//
//  RideStartTime.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/29/20.
//

import SwiftUI

struct RideStartTime: View {
    var ride: DtrRide
    var editable: Bool = false
    
    var body: some View {
        VStack {
            Text("Ride Start Time")
        }
        .navigationBarTitle("Start Time")
        .navigationBarItems(trailing:
            Button(action: {
                
            }) {
                Text("Update")
            }
            .disabled(!editable)
        )
    }
}

struct RideStartTime_Previews: PreviewProvider {
    static var previews: some View {
        RideStartTime(ride: DtrRide.default)
    }
}
