//
//  RideContent.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/28/20.
//

import SwiftUI

struct RideContent: View {
    @EnvironmentObject var dtrData: DtrData
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @Environment(\.presentationMode) var presentationMode   // to dismiss()
    
    @State var ride: DtrRide
    @Binding var showActivity: Bool
    
    @State var editable: Bool = false
    @State var rideChanged = false
    @State var initialMyRides = true
    @State var initialSuggestededRides = true
    
    @State private var confirmAction = false
    @State private var confirmCancel = false
    
    func saveChanges() {
    }
    
    var body: some View {
        ScrollView {
            Rectangle()
                .foregroundColor(Color.yellow)
                .frame(height: 200)
            
            if rideChanged {
                Text("The ride has changed!!")
            }
            
            VStack {
                HStack {
                    Text(ride.summary).fontWeight(.bold)
                    Spacer()
                }
                .padding()
                
                HStack {
                    Text("Riding with")
                    Text(ride.riderProfiles[ride.leader]?.name ?? "Someone").fontWeight(.semibold)
                    Spacer()
                }
                .font(.caption)
                .padding(.horizontal)
            }
            
            Group {
                RideRiders(ride: self.ride)
                    .padding()
                
                HStack {
                    Image(systemName: "clock")
                    Text(ride.details.startTime)
                    
                    if editable {
                        NavigationLink(destination: RideStartTime(ride: self.ride, editable: self.editable)) {
                            Text("Edit").font(.caption)
                        }
                    }
                    
                    Text("  ")  // hack
                    
                    Image(systemName: "mappin.and.ellipse")
                    Text(ride.details.startPlace)
                    
                    if editable {
                        NavigationLink(destination: RideStartPlace(ride: self.ride, editable: self.editable)) {
                            Text("Edit").font(.caption)
                        }
                    }
                    
                    Spacer()
                }
                .font(.caption)
                .padding()
                
                RideContentBlock(label: "Skill Levels", ride: $ride, rideChanged: $rideChanged, editable: false, destinationView: RideSkillLevelsDan(ride: self.$ride, wasChanged: self.$rideChanged, editable: self.editable), displayView: AnyView(
                                    RideSkillIndicators(ride: $ride, showNone: true)
                ))
                
                RideContentBlock(label: "Ride Attributes", ride: $ride, rideChanged: $rideChanged, editable: false, destinationView: RideAttributes(ride: self.$ride, wasChanged: self.$rideChanged, editable: self.editable), displayView: AnyView(
                                    HStack {
                                        if ride.details.attributes.isEmpty {
                                            Text("None provided").italic().disabled(true).font(.caption)
                                        } else {
                                            Text(ride.details.attributes.map{$0.name}.joined(separator: " • ")).font(.caption)
                                        }

                                        Spacer()
                                    }
                ))
                
                RideContentBlock(label: "Ride Notes", ride: $ride, rideChanged: $rideChanged, editable: false, destinationView: RideNotes(ride: self.$ride, wasChanged: self.$rideChanged, editable: self.editable), displayView: AnyView(
                                    HStack {
                                        if ride.details.notes.isEmpty {
                                            Text("None provided").italic().disabled(true).font(.caption)
                                        } else {
                                            Text(ride.details.notes)
                                        }

                                        Spacer()
                                    }
                ))
            }
            .background(EmptyView().actionSheet(isPresented: self.$confirmAction) {
                ActionSheet(
                    title: Text("Are you sure?"),
                    buttons: [
                        .destructive(Text(self.ride.fromAction.label), action: {
                            if self.ride.fromAction == .none {
                                self.showActivity = true
                                self.dtrCommands.profileFollow(profileID: "aVEsaw8MtAMelQ9auRJquVXd3Sh1"){_ in 
                                }
                            } else if self.ride.fromAction == .leave {
                                self.showActivity = true
                                self.dtrCommands.rideLeave(rideID: self.ride.id, dateCode: self.ride.dateCode) { result in
                                    self.showActivity = false
                                    if result.result == .ok {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                }
                                
                            } else if self.ride.fromAction == .cancel {
                                self.showActivity = true
                                self.dtrCommands.rideCancel(rideID: self.ride.id,dateCode:self.ride.dateCode, message: "ride cancelled") { result in
                                    self.showActivity = false
                                    if result.result == .ok {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                }
                            } else {
                                print("WARNING! - unknown action")
                            }
                        }),
                        .cancel {
                        }
                    ])
            })
            .background(EmptyView().actionSheet(isPresented: self.$confirmCancel) {
                ActionSheet(
                    title: Text("Are you sure? This will affect \(self.ride.riders.count - 1) other riders."),
                    buttons: [
                        .destructive(Text(self.ride.fromAction.label), action: {
                            print("TODO: compose a cancellation message here")
                            
                            self.showActivity = true
                            self.dtrCommands.rideCancel(rideID: self.ride.id,dateCode:self.ride.dateCode, message: "cancellation messge") { result in
                                self.showActivity = false
                                if result.result == .ok {
                                    self.presentationMode.wrappedValue.dismiss()
                                }
                            }
                        }),
                        .cancel {
                        }
                    ])
            })
            
            Divider()
            .padding()
            
            HStack {
                Text("Riders").font(.caption).fontWeight(.bold)
                Spacer()
            }
            .padding(.horizontal)
            
            ForEach(ride.riders, id:\.self) { rider in
                ProfileCard(profile: ride.riderProfiles[rider] ?? DtrProfile.default)
                    .padding()
            }
        }
        .onAppear() {
            if self.ride.leader == self.dtrData.profileID {
                self.editable = true
            }
        }
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                if self.showActivity {
                    ProgressView()
                        .font(.title)
                        .padding()
                }
                
            }
            ToolbarItem(placement: .navigationBarTrailing) {
                Button(action: {
                    if self.ride.fromAction == .cancel && self.ride.riders.count > 1 {
                        self.confirmCancel = true
                        return
                    }
                    
                    self.confirmAction = true
                    
                }) {
                    Text(self.ride.fromAction.label)
                }
                .buttonStyle(RideActionStyle(rideAction: self.ride.fromAction))
                
            }
        }
        .navigationTitle("Ride Details")
    }
}

struct RideContentBlock<DestinationView: View>: View {
    var label: String
    @Binding var ride: DtrRide
    @Binding var rideChanged: Bool
    var editable: Bool = false
    
    var destinationView: DestinationView
    var displayView: AnyView
    
    init(label: String, ride: Binding<DtrRide>, rideChanged: Binding<Bool>, editable: Bool, destinationView: DestinationView, displayView: AnyView) {
        _ride = ride
        _rideChanged = rideChanged
        self.label = label
        self.editable = editable
        self.destinationView = destinationView
        self.displayView = displayView
    }
    
    var body: some View {
        VStack {
            HStack {
                Text(self.label).font(.caption).fontWeight(.semibold)
                Spacer()
                
                NavigationLink(destination: self.destinationView) {
                    if editable {
                        Text("Edit").font(.caption)
                    } else {
                        Text("About").font(.caption)
                    }
                }
            }
            
            NavigationLink(destination: self.destinationView) {
                self.displayView
            }
        }
        .padding()
    }
}

struct RideContent_Previews: PreviewProvider {
    @EnvironmentObject var dtrData: DtrData
    
    static var previews: some View {
        RideContent(ride: DtrRide.default, showActivity: .constant(false))
    }
}
