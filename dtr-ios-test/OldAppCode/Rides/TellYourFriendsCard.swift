//
//  TellYourFriendsCard.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/26/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI

struct TellYourFriendsCard: View {
    @Binding var day: Int
    var completion: (String) -> Void = { statusMessage in }
    
    @State private var statusMessage = ""
    @State private var showClearButton = true
    
    var dateCodeForDay: String {
        return dateCode(offset: self.day)
    }
    
    var body: some View {
            
            VStack {
                HStack {
                    Text("Tell your friends you're down to ride.").font(.caption).fontWeight(.semibold).fixedSize(horizontal: false, vertical: true)
                    Spacer()
                }
                .padding(.top)
                
                HStack {
                    Text("You'll be able to see who else is down to ride.").font(.caption).fixedSize(horizontal: false, vertical: true)
                    Spacer()
                }
                
                TextField("Let's get a ride in after work.", text: $statusMessage, onEditingChanged: { editing in
                    // self.showClearButton = editing
                    
                }, onCommit: {
                    // self.showClearButton = false
                    
                    if (self.statusMessage != "") {
                        self.completion(self.statusMessage.trimmingCharacters(in: .whitespacesAndNewlines))
                    }
                    
                })
                .modifier(ClearButton(text: $statusMessage, visible: $showClearButton))
                .padding(8)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.blue, lineWidth: 1))
                .frame(height: 40)
                
                Button(action: {
                    self.completion(self.statusMessage.trimmingCharacters(in: .whitespacesAndNewlines))
                    
                }) {
                    Text("I'm Down to Ride")
                }
                .buttonStyle(ActionButtonStyle())
                .disabled(self.statusMessage == "")
                .padding()
                
                Spacer()
            }.modifier(AdaptsToSoftwareKeyboard())
        
        
    }
}

struct TellYourFriendsCard_Previews: PreviewProvider {
    static var previews: some View {
        TellYourFriendsCard(day: .constant(0))
    }
}
