//
//  RidesForDay.swift
//  dtr-ios-test
//
//  Created by Dan Kinney on 1/10/21.
//

import SwiftUI

protocol RidesForDayProtocol {
    var view: AnyView { get }
}

struct NotRidingRidesForDay: View, RidesForDayProtocol {
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    
    @Binding var day: Int
    @Binding var showActivity: Bool
    
    var body: some View {
        List {
            App_Header_C(currentDay: self.$day)
            TellYourFriendsCard(day: self.$day) { statusMessage in
                self.showActivity = true
                
                self.dtrCommand.rideCreate(dateCode: dateCode(offset: day), data: ["summary": statusMessage]) { result in
                    self.showActivity = false
                }
            }
        }
    }
    
    var view: AnyView {
        AnyView(self)
    }
}

struct RidingRidesForDay: View, RidesForDayProtocol {
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    
    var ride: DtrRide
    @Binding var day: Int
    @Binding var showActivity: Bool
    
    @State private var ridesToDisplay = [DtrRide]()
    
    var body: some View {
        List {
             //DayChooser(currentDay: self.$day) Change by Rs
            App_Header_C(currentDay: self.$day)
            
            MyRideCard(ride: self.ride, showActivity: self.$showActivity)
            
            Section(header: HStack {
                Text("Rides")
                Spacer()
                Text("\(ridesToDisplay.count)")

            }) {
                ForEach(self.ridesToDisplay, id: \.self) { ride in
                    NavigationLink(destination: RideContent(ride: ride, showActivity: self.$showActivity)) {
                        RideCard(ride: ride).id(UUID())     // the .id will force the children update when array updates
                    }
                }
            }
            .listStyle(GroupedListStyle())
        }
        
        .onChange(of: self.day) { updatedDay in
            if let rides = self.dtrData.suggestedRidesByDate[dateCode(offset: updatedDay)] {
                self.ridesToDisplay = rides.map{$0}
                //print("Rides For Day onChange = ",self.ridesToDisplay.count)
            } else {
                self.ridesToDisplay = []
            }
        }
//        .onReceive(self.dtrData.$myRidesByDate) { rideDict in
//            if let ride = rideDict[dateCode(offset: self.day)] {
//                self.ride = ride
//            }
//        }
        .onReceive(self.dtrData.$suggestedRidesByDate) { ridesForDate in
            if let rides = ridesForDate[dateCode(offset: self.day)] {
                self.ridesToDisplay = rides.map{$0}
                //print("Rides For Day onReceive = ",self.ridesToDisplay.count)
            } else {
                self.ridesToDisplay = []
            }
        }
    }
    
    var view: AnyView {
        AnyView(self)
    }
}

struct RidesForDayFactory: RidesForDayProtocol {
    @Binding var ride: DtrRide?
    @Binding var day: Int
    @Binding var showActivity: Bool
    
    var view: AnyView {
        if self.ride == nil {
            return AnyView(NotRidingRidesForDay(day: self.$day, showActivity: self.$showActivity).view)
        } else {
            return AnyView(RidingRidesForDay(ride: self.ride!, day: self.$day, showActivity: self.$showActivity).view)
        }
    }
}

struct RidesForDay: View {
    let ridesForDay: RidesForDayProtocol
    
    var body: some View {
        ridesForDay.view
    }
}
