//
//  RideNotes.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/29/20.
//

import SwiftUI

struct RideNotes: View {
    @Binding var ride: DtrRide
    @Binding var wasChanged: Bool
    var editable: Bool = false
    
    var body: some View {
        VStack {
            Text("Ride Notes")
        }
        .navigationBarTitle("Notes")
        .navigationBarItems(trailing:
            Button(action: {
                
            }) {
                Text("Update")
            }
            .disabled(!editable)
        )
    }
}

struct RideNotes_Previews: PreviewProvider {
    static var previews: some View {
        RideNotes(ride: .constant(DtrRide.default), wasChanged: .constant(false))
    }
}
