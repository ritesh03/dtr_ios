//
//  RidesCard.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/24/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI

struct MyRideCard: View {
    var ride: DtrRide
    @Binding var showActivity: Bool
    
    var body: some View {
        VStack(alignment: .leading, spacing: 16) {
            Text(ride.summary)
                .lineLimit(/*@START_MENU_TOKEN@*/2/*@END_MENU_TOKEN@*/)
                .padding(.top)

            RideRiders(ride: self.ride)

            HStack(alignment: .top) {
                HStack(alignment: .top) {
                    Image(systemName: "clock")
                    Text(ride.details.startTime)
                }

                HStack(alignment: .top) {
                    Image(systemName: "mappin.and.ellipse")
                    Text(ride.details.startPlace)
                }
                .padding(.leading, 8)

                Spacer()
            }
            .font(.caption)
            
            HStack {
                Spacer()

                NavigationLink (destination: RideContent(ride: self.ride, showActivity: self.$showActivity)) {
                    Spacer()
                    Image(systemName: "ellipsis.circle").font(.title)
                }
            }
        }
    }
}

struct MyRideCard_Previews: PreviewProvider {
    static var previews: some View {
        MyRideCard(ride: DtrRide.default, showActivity: .constant(false))
    }
}
