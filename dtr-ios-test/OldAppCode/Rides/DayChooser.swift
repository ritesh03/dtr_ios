//
//  DayChooser.swift
//  dtr-ios
//
//  Created by Dan Kinney on 12/26/20.
//  Copyright © 2020 Social Active. All rights reserved.
//

import SwiftUI

struct DayChooser: View {
    @Binding var currentDay : Int
    
    var body: some View {
        HStack(alignment: .center, spacing: 4) {
            ForEach(0..<7) { offset in
                DayButton(offset: offset, active: offset == currentDay) { offset in
                    currentDay = offset
                }
            }
        }
    }
}

struct DayChooser_Previews: PreviewProvider {
    static var previews: some View {
        DayChooser(currentDay: .constant(0))
    }
}
