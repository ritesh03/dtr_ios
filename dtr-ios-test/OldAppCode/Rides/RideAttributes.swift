//
//  RideAttributes.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/29/20.
//

import SwiftUI

struct RideAttributes: View {
    @Binding var ride: DtrRide
    @Binding var wasChanged: Bool
    var editable: Bool = false
    
    @State var attributes = [DtrRideAttributeType]()
    
    func rideIsDifferent() -> Bool {
        self.attributes == self.ride.details.attributes
    }
    
    var body: some View {
        List {
            Text(self.attributes.map{$0.name}.joined(separator: " • ")).font(.caption)
            
            ForEach(DtrRideAttributeType.allCases) { attribute in
                HStack {
                    Button(action: {
                        if self.attributes.contains(attribute) {
                            // remove
                            if let index = self.attributes.firstIndex(where: { $0 == attribute }) {
                                self.attributes.remove(at: index)
                            }
                        } else {
                            attributes.append(attribute)
                        }                        
                        
                    }) {
                        Image(systemName: (self.attributes.contains(attribute)) ? "checkmark.circle.fill": "circle.fill").font(.title)
                    }
                    .disabled(!editable)
                    
                    Text(attribute.name).font(.caption)
                    Spacer()
                }
            }
        }
        .onAppear {
            self.attributes = self.ride.details.attributes
        }
        .navigationBarTitle("Ride Attributes")
        .navigationBarItems(trailing:
            Button(action: {
                
            }) {
                Text("Update")
            }
            .disabled(!editable)
        )
    }
}

struct RideAttributes_Previews: PreviewProvider {
    static var previews: some View {
        RideAttributes(ride: .constant(DtrRide.default), wasChanged: .constant(false))
    }
}
