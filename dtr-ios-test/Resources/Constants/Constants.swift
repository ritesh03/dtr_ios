//
//  Constants.swift
//  dtr-ios-test
//
//  Created by apple on 15/07/21.
//

import Foundation


//MARK:- App Information
struct AppInformation {
    static let AppBundleId = "app.socialactive.dtr"
    static let AppStoreId = "1477781219"
    static let AppMinimumVersion = "0.1"
    static let AppDeafultUrl = "https://dtr.lol"
    static let AppDeafultStagingUrl = "https://socialactive.page.link"
}

//MARK:-  Image Constants Name
struct ImageConstantsName {
    static let PlusImg = "plus"
    static let XmarkImg = "xmark"
    static let PhoneImg = "DTR-Phone"
    static let EmailImg = "DTR-Email"
    static let PublicImg = "DTR-Public"
    static let VectorImg = "DTR-Vector"
    static let CrownImg = "DTR-CrownIcon"
    static let ClockImg = "DTR-clockicon"
    static let UpArrowImg = "DTR-UpArrow"
    static let StartImg =  "DTR-StartIcon"
    static let ArrowLeftImg = "arrow.left"
    static let UnVectorImg = "DTR-UnVector"
    static let AboutDtrImg = "DTR-AboutIcon"
    static let searchImg = "magnifyingglass"
    static let MapMarkerImg = "DTR-MapMaker"
    static let CycleIconImg =  "DTR-CycleIcon"
    static let IceTagBagImg = "DTR-IceTagBag1"
    static let HomeTimeImg = "DTR-HomeTimeIcon"
    static let IceTagBag2Img = "DTR-IceTagBag2"
    static let HouseCenterImg =  "house.circle"
    static let IceMainLogoImg = "DTR-IceManLogo"
    static let CameraBtbImg = "DTR-CameraButton"
    static let SettingIconImg = "DTR-SettingIcon"
    static let HeaderCircleImg = "DTR-HandCirecl"
    static let Onboarding7Img =  "DTR-Onboarding7"
    static let Onboarding1Img =  "DTR-Onboarding1"
    static let WifiMarkImg = "wifi.exclamationmark"
    static let EllipseRoundImg = "DTR-EllipseRound"
    static let LocationMarkImg = "DTR-LocationMark"
    static let OutlinedBtnImg = "DTR-Outlinedbutton"
    static let MainCoverBgImg = "DTR-MainCoverImage"
    static let MapMarkerSolidImg = "DTR-MapSolidIcon"
    static let AccetAppIconImg = "DTR-AccentIAppIcon"
    static let DTRShareIconImg = "DTR-ShareIcon-Three"
    static let VisiblityRideImg =  "DTR-VisibiltyIcon"
    static let HomeLocationImg = "DTR-HomeLocationIcon"
    static let ArrowLeftCircleImg =  "arrow.left.circle"
    static let FriendOfFriendImg =  "DTR-FriendOfFriend"
    static let PersonCircleFillImg = "person.circle.fill"
    static let ArrowRightCircleImg =  "arrow.right.circle"
    static let BckWithColorImg = "DTR-BackButtonWithColor"
    static let LocationMapIconImg = "DTR-LocationMap_icon"
    static let PersonPlaceholderUserImg = "DTR-PlaceholderUser"
    static let FriendAndFollowersImg =   "DTR-FriendAndFollowers"
    static let BckWithThreeDotsImg = "DTR-ThreeDotsWithBckground"
    static let MainRideEditCoverImg = "DTR-MainRideEditCoverImage"
}


//MARK:- Chat Screen / SenderUI
struct ImageConstantsNameForChatScreen {
    static let PersonImg = "person.2"
    static let MegaPhoneOnImg = "DTR-Megaphone-on"
    static let LocationIconImg = "DTR-locationicon"
    static let MegaPhoneOffImg = "DTR-Megaphone-off"
    static let DownArrowWithCircleImg = "DTR-DownArrowWithCircle"
}


//MARK:- Colour Name
struct ColorConstantsName {
    static let LimeColour = "DTR-Lime"
    static let AlertColour = "DTR-Alert"
    static let CrownColour = "DTR-Crown"
    static let AccentColour = "DTR-Accent"
    static let PickerColour = "DTR-PickerTitleColor"
    static let AccentTextColour = "DTR-AccentForText"
    static let HeaderBgColour = "DTR-HeaderBackground1"
    static let SectionBgColour = "DTR-SectionBackground"
    static let GradientTopColur = "DTR-GradientTopColour"
    static let ChatSenderBgColour = "DTR-ChatSendrColor"
    static let NotificationColour = "DTR-NotificationColor"
    static let HeaderBackgroundColour = "DTR-HeaderBackground"
    static let HeaderForGrandColour = "DTR-HeaderForeground"
    static let MainThemeBgColour = "DTR-MainThemeBackground"
    static let GradientBottomColur = "DTR-GradientBottomColour"
    static let GroupCreateImageBgColour = "DTR-GroupCreateDumyColour"
    static let ComunityLightBackground = "   DTR-ComunityLightBackground"
    
 
}


//MARK:- Common Strings
struct CommonAllString {
    
    static let OrStr = "OR"
    static let OneStr = "1"
    static let NoStr = "No"
    static let YesStr = "Yes"
    static let EngStr = "en"
    static let DtrStr = "DTR"
    static let BlankStr = ""
    static let NextStr = "NEXT"
    static let NullStr = "<null>"
    static let AdminStr = "Admin"
    static let ForNextLine = "\n"
    static let UpdateStr = "UPDATE"
    static let UnknownStr = "unknown"
    static let UpdateCamlCseStr = "Update"
    static let CreateStr = "CREATE"
    static let DtrCommonStr = "DTR : "
    static let DeleteSmallStr = "Delete"
    static let CancelStr = "Cancel"
    static let DeleteAnywayStr = "Delete Anyway"
    static let AssignLeader = "Assign Leader"
    static let UploadingStr = "Uploading"
    static let AllGroupsStr = "ALL GROUPS"
    static let LocationCapsStr = "Location"
    static let UpdateNowStr = "Update Now"
    static let InitializingStr = "Initializing"
    static let ChangeImageStr = "Change image"
    static let AddGroupStr = "Add group image"
    static let OptionalMesgStr = "Optional Message"
    static let CfBUndleVersionStr = "CFBundleVersion"
    static let TextUserProfilePicStr = "user profile picture"
    static let GroupJoinMesgStr = "grow your followers and friends list by seeing who’s in the group"
}


//MARK:- RideSkillIndicators Class
struct RideSkillsIndicatorsString {
    static let SkillAStr = "DTR-SkiilA"
    static let SkillBStr = "DTR-SkiilB"
    static let SkillCStr = "DTR-SkiilC"
    static let SkillDStr = "DTR-SkiilD"
    static let SkillSStr = "DTR-SkiilS"
}


//MARK:- CreateGroupSection Class
struct CreateGroupSectionString {
    static let JoinGroupStr = "JOIN A GROUP"
    static let JoinGroupSmalStr = "Join a group"
    static let CreateOwnStr = "Or create your own"
    static let JoinStartRidimhStr = "Join a group and start\n  riding with friends today!"
}

//MARK:- ShowOtherRidesTo Class
struct ShowOtherRidesToString {
    static let PublicRideStr = "PUBLIC RIDES"
    static let FriendRideStr = "FRIEND'S RIDES"
    static let OthersRideJoinStr = "Other rides to join"
    static let FollowersRideStr = "FOLLOWERS AND GROUP MEMBERS RIDES"
}


//MARK:- PublicRideLocationChange Class
struct PublicRideLocationChangeString {
    static let SettingStr = "settings."
    static let ChangeLocationStr = "Change location"
    static let YourRideSeenStr = "You’re seeing rides on "
    static let ChangeDefaultLocationStr = "To change your default location go to"
    static let FromNetworkPublicRideStr = " from your\n network, plus public rides near "
}


//MARK:- Group Detail Class
struct GroupDetailClassString {
    static let Joinstr = "Join"
    static let Leavestr = "Leave"
    static let Sharestr = "SHARE"
    static let InTheGroupStr = "IN THIS GROUP"
    static let ShareRideDetailsStr = "Share Group Detail"
}


//MARK:- ReturnGroupTextFieldSection Class

struct ReturnGroupTextFieldSectionString {
    static let GroupNameStr = "Group name"
    static let GroupLocationStr = "Group location"
    static let GroupLocationErrorStr = "Please select location"
    static let CatchNameGroupStr = "A catchy name for your group"
    static let GroupWhereLocatedStr = "Where is your group located?"
    static let GroupNameErrorStr = "Group name must be 3 characters long"
}


//MARK:- DayContainer Class
struct DayContainerString {
    static let DetailStr = "details"
    static let CancelRideStr = "Cancel Ride"
    static let ShareRideStr = "Share Ride"
    static let LeaveRideStr = "Leave Ride"
    static let NotDtrAnyMoreStr = "Not DTR anymore"
}

//MARK:- Chat Screen / SenderUI
struct ChatScreenSenderString {
    static let ToStr = "to"
    static let UserStr = "User"
    static let TimeStr = "time"
    static let JoinedStr = "joined"
    static let LeftStr = "left"
    static let LocationStr = "location"
    static let SystemStr = "system"
    static let RideUpdateStr = "RIDE CHAT"
    static let WriteAMesgStr = "Write a message"
    static let MegaPhoneTurnedOnStr = "Megaphone turned on"
    static let MegaPhoneTurnedStatusStr = "Everyone on this ride will be notified of this message."
}


//MARK:- RideListUI
struct RideListUIString {
    static let PlusStr = "+"
    static let FollowStr = "Follow"
    static let RideLeaderStr = "Ride leader"
    static let FollowingSmallStr = "following"
    static let FollowingFcapsStr = "Following"
    static let RequestFriendStr = "Request Friend"
    static let AlreadyFriendsStr  = "already friends"
    static let RquestFriendSmallStr = "requestedFriend"
    static let FriendRequestSentStr = "Friend request sent"
    static let FollowingWithInviteStr = "follwoingWithInvite"
}


//MARK:- Setting Screen
struct SettingScreenString {
    static let StaffStr = "staff"
    static let OthersStr = "Others"
    static let AccountStr = "Account"
    static let DtrVersionStr = "DTR version "
    static let PreferencesStr = "Preferences"
    static let SendFeedBackStr = "Send feedback"
    static let OpenMyDeviceSettingStr = "Open my device app settings"
    static let ToManageNotificationUsageStr = "to Manage Notifications, Location and Data Usage "
}


//MARK:- TimeEdit Screen
struct TimeEditScreenString {
    static let CancelStr = "Cancel"
    static let TimeStr = "Time"
    static let TimeTBDStr = "Time TBD"
    static let SetTimeStr = "Set Time"
}

//MARK:- UplodImageForRide Screen
struct UplodImageForRideString {
    static let UploadAnImageForRideStr = "Upload an image for your ride:"
    static let UploadStr = "UPLOAD"
    static let SelectRideImgStr = "Select ride image"
    static let WhtDoYouWntToStr = "What do you want to do?"
    static let FrmCameraStr = "From Camera"
    static let FrmGalleryStr = "From Gallery"
}

//MARK:- StaticImagesForRide Screen
struct StaticImagesForRideScreenString  {
    static let ChoseFromOneHereStr = "Choose one from here:"
}

//MARK:- NotesEditScrn Screen
struct NotesEditScrnString {
    static let AboutThisRideStr = "About this ride"
    static let NotesStr = "Notes"
    static let Summarytr = "Summary"
    static let RideIdStr = "rideID"
    static let ForPublicYouMustFillOutStr = "For Public You must fill out location first."
}

//MARK:- ShowPublicTagError Screen
struct ShowPublicTagErrorString  {
    static let PublicStr = "public"
    static let DownToRideStr = "Down to Ride"
    static let DownToRideStrSpace = "DowntoRide"
    static let YourRideIsNotVisibleStr = "Your ride is not visible."
    static let YouMusFillOutAllFieldForPublicStr = "You must fill out all fields for your public \n ride to be visible."
}


//MARK:- Common Error
struct CommonErrorString {
    static let UpdateProfileLocationError = "Enter your zipcode so we can find public rides near you."
    static let CancelRideError = "Are you sure you'd like to cancel the ride?"
    static let ThreeHundCharacError = "\n Message must be less than 300 characters"
    static let DeleteGroupErrorMesg = "All group members will be disassociated to the group and will not be able to view group member rides. Are you sure to delete the group?"
    static let DeleteGroupWIthAsignLeader = "Deleting a group would disconnect all group members and this action is irreversible. We recommend assigning another group member as a leader"
    static let AlertMesgError = "\n Message must be less than 300 characters \n\n\n\n"
}


//MARK:- For Poke Notification
struct ForPokeNotificationString {
    static let PayloadJoinSaturday = "joinThisSaturday"
    static let PayloadJoinSunday = "joinThisSunday"
    static let PayloadGoToWeekend = "goDtrThisWeekend"
    static let PayloadComeBack = "goDtrOnDayWithMostRides"
}

struct FollowScreenString {
    static let EnterRiderNameStr = "Enter rider name above to search"
    static let ClickHereToSearchStr = "Click here to search for a group"
    static let SearchGroupStr = "SEARCH GROUP"
    static let ConnectMorePeopleStr = "Connect with more people"
    static let FindOtherRidersStr = "Find Other Riders"
}

struct  SignupScreenString {
    static let SignupGetRideString = "Sign Up or Login using your phone number"
    static let PhoneCode = "+1"
    static let PhoneErrorString = "Plese enter valid phone number"
    static let NeedHelpString = "Need help?"
    static let EmailUsString = "Email us"
}

//MARK:- All Default URL
struct DefaultUrlString {
    static let defaultRideImageUrl = "https://firebasestorage.googleapis.com/v0/b/socialactive-app.appspot.com/o/dtr%2FridesImages%2Fdefault-image-1.png?alt=media&token=f7735e4b-8f8b-49b0-840f-c1f240585427"
    static let AppsStoreUrlStr = "itms-apps://itunes.apple.com/app/id1477781219"
    static let TermsLink = "https://socialactive.app/terms.html"
    static let PrivacyLink = "https://socialactive.app/privacy.html"
    static let CommunityWebLink = "https://www.medium.com/downtoride"
    
}

