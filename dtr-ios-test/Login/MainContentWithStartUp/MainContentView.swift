//
//  MainContentView.swift
//  dtr-ios-test
//
//  Created by Mobile on 02/04/21.
//

import SwiftUI

struct MainContentView: View {
    @State var showStartup = true
    @State private var statusMessage = CommonAllString.InitializingStr
    @ObservedObject var dtrData = DtrData.sharedInstance
    private var currentBuild: Int = 0
    
    init() {
        let buildNumberString = fromBundle(CommonAllString.CfBUndleVersionStr)
        self.currentBuild = asInt(Int(buildNumberString), defaultValue: 0)
    }
    
    var body: some View {
        Group {
            if showStartup {
                StartupScreen(statusMessage: self.$statusMessage)
                    .onAppear() {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            self.showStartup = false
                        }
                    }
            } else {
                if (self.dtrData.minimumBuild.ios > currentBuild) {
                    OperationalMessage(isPresented: .constant(true), title: self.dtrData.minimumBuild.title(CommonAllString.EngStr), message: self.dtrData.minimumBuild.message(CommonAllString.EngStr))
                } else {
                    HomeViewScreen().environmentObject(dtrData)
                        .environmentObject(UserSettings())
                        .transition(.opacity)
                }
            }
        }
    }
}

struct MainContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainContentView()
    }
}
