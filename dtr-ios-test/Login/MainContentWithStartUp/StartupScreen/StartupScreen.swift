//
//  StartupScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 02/04/21.
//

import SwiftUI

struct StartupScreen: View {
    @Binding var statusMessage: String
    @State var tapCount = 0
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        ZStack {
            StartUpBackgroundScreen()
            VStack {
                Spacer()
                PublishingStartupScreen() {
                    tapCount += 1
                    if tapCount == 5 {
                        dtrData.signOut { (response) in
                        }
                    }
                }
                Spacer()
                Text(statusMessage)
                    .font(.caption)
                    .foregroundColor(.white)
            }
            .padding()
        }
    }
}

struct StartupScreen_Previews: PreviewProvider {
    static var previews: some View {
        StartupScreen(statusMessage: .constant("Huh?"))
    }
}

