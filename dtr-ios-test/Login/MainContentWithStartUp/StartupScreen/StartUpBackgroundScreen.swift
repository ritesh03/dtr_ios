//
//  StartUpBackgroundScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 02/04/21.
//

import SwiftUI

struct StartUpBackgroundScreen: View {
    @State var comesfromMainScreen = false
    
    var body: some View {
        GeometryReader { geometry in
            Rectangle()
            .frame(width: geometry.size.width + geometry.safeAreaInsets.leading + geometry.safeAreaInsets.trailing, height: geometry.size.height + geometry.safeAreaInsets.top + geometry.safeAreaInsets.bottom + 10, alignment: .center)
            .foregroundColor(.clear)
            .background(
                Image(comesfromMainScreen ==  true ? "DTR-Onboarding2" : "DTR-Onboarding1" )
                .resizable()
                .aspectRatio(contentMode: .fill)
                    .ignoresSafeArea()
            )
        }
    }
}

struct StartUpBackgroundScreen_Previews: PreviewProvider {
    static var previews: some View {
        StartUpBackgroundScreen()
    }
}
