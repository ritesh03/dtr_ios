//
//  PublishingStartupScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 02/04/21.
//

import SwiftUI

struct PublishingStartupScreen: View {
    var action: () -> Void = {}
    
    let imageSize: CGFloat = 100
    let outlineSize: CGFloat = 150
    @State private var wave = false
    @State private var wave1 = false
    
    var body: some View {
        ZStack {
            Circle()
            .stroke(lineWidth: 10)
            .frame(width: outlineSize, height: outlineSize)
            .foregroundColor(Color.init( ColorConstantsName.HeaderBackgroundColour))
            .scaleEffect(wave ? 3 : 1)
            .opacity(wave ? 0 : 0.5)
            .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: false).speed(0.5))
            .onAppear {
                self.wave.toggle()
            }

            Circle()
            .stroke(lineWidth: 10)
            .frame(width: outlineSize, height: outlineSize)
            .foregroundColor(Color.init( ColorConstantsName.HeaderBackgroundColour))
            .scaleEffect(wave1 ? 2 : 1)
            .opacity(wave1 ? 0 : 0.5)
            .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: false).speed(0.7))
            .onAppear {
                self.wave1.toggle()
            }

            Circle()
            .frame(width: outlineSize, height: outlineSize)
            .foregroundColor(Color.init( ColorConstantsName.HeaderBackgroundColour))
            .shadow(radius: 25)

            Button(action: { self.action() }) {
               Image(uiImage: UIImage(named:ImageConstantsName.AccetAppIconImg)!)
               .resizable()
               .aspectRatio(contentMode: .fit)
               .frame(width: imageSize, height: imageSize)
               .padding(.bottom, 10)
               .foregroundColor(.white)
               .shadow(radius: 25)
            }
        }
    }
}

struct PublishingStartupScreen_Previews: PreviewProvider {
    static var previews: some View {
        PublishingStartupScreen()
    }
}
