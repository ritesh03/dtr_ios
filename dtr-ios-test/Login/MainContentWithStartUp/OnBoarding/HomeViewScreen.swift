//
//  HomeViewScreen.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/26/20.
//

import SwiftUI
import Firebase

struct HomeViewScreen: View {
    
    @EnvironmentObject var dtrData: DtrData
    
    enum DisplayState {
        case startup
        case onboarding
        case operation
        case updateAppVersion
    }
    
    @State private var day = 0
    @State private var statusMessage = "Starting up..."
    @State private var displayState: DisplayState = .startup
    private var minimumStartupScreenTime = DispatchTimeInterval.seconds(2)
    @ObservedObject var userSearchModel: UserSearchViewModel = .init()
    @ObservedObject var updateTabComunitySlcted = UpdateTbBarSlctedComunity.sharedInstance
    
    @State var days: [HostingController<DayContainerView>] =
    [HostingController(rootView: DayContainerView(day: .constant(0))),
     HostingController(rootView: DayContainerView(day: .constant(1))),
     HostingController(rootView: DayContainerView(day: .constant(2))),
     HostingController(rootView: DayContainerView(day: .constant(3))),
     HostingController(rootView: DayContainerView(day: .constant(4))),
     HostingController(rootView: DayContainerView(day: .constant(5))),
     HostingController(rootView: DayContainerView(day: .constant(6)))]
    
    
    init() {
        
        //This changes the "thumb" that selects between items
        UISegmentedControl.appearance().backgroundColor = UIColor.clear
        UISegmentedControl.appearance().selectedSegmentTintColor =  UIColor.clear
        
        //These lines change the text color for various states
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor : UIColor.init(Color.init(ColorConstantsName.LimeColour))], for: .selected)
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor : UIColor.init(Color.init(ColorConstantsName.PickerColour))], for: .normal)
        
        //For TabBar Custom
        UITabBar.appearance().barTintColor =  UIColor.init(Color.init(ColorConstantsName.MainThemeBgColour))
        UITabBar.appearance().tintColor =  UIColor.white
        if #available(iOS 15.0, *) {
            let appearance = UITabBarAppearance()
            appearance.configureWithDefaultBackground()
            appearance.backgroundColor = UIColor.init(Color.init(ColorConstantsName.MainThemeBgColour))
            UITabBar.appearance().standardAppearance = appearance
            UITabBar.appearance().scrollEdgeAppearance = appearance//ritesh
        }
        
        //For list Custom
        UITableView.appearance().backgroundColor =  UIColor.init(Color.init(ColorConstantsName.MainThemeBgColour))
        UITableViewCell.appearance().backgroundColor =  UIColor.init(Color.init(ColorConstantsName.MainThemeBgColour))
        
        //For Navigation Custom
        UINavigationBar.appearance().barTintColor = .clear
        UINavigationBar.appearance().backgroundColor = .clear
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        
        //For Page Controll Custom
        var pages = [HostingController<DayContainerView>] ()
        let dateCodeFormatter = DateFormatter()
        dateCodeFormatter.dateFormat = "yyyyMMdd"
        
        for index in 0..<7 {
            pages.append(HostingController(rootView: DayContainerView(day: .constant(index))))
        }
        
        self.days = pages
        
        let buildNumberString = fromBundle(CommonAllString.CfBUndleVersionStr)
        self.currentBuild = asInt(Int(buildNumberString), defaultValue: 0)
    }
    
    private var currentBuild: Int = 0
    
    @State private var resetNavigationID = UUID()
    @State private var resetNavigationID1 = UUID()
    @State private var resetNavigationID2 = UUID()
    @State private var resetNavigationID3 = UUID()
    @State private var selection = 0
    
    var body: some View {
        switch displayState {
        case .startup:
            StartupScreen(statusMessage: $statusMessage)
                .onAppear {
                    let when = DispatchTime.now() + self.minimumStartupScreenTime
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        print("onAppear: \(dtrData.statusLogin.label)")
                        switch dtrData.statusLogin {
                        case .signedIn:
                            self.displayState = .startup
                            if UserStore.shared.registrationInProgress1 == 11100 {
                                self.displayState = .onboarding
                                dtrData.statusLogin = .signedOut
                            }
                        case .ready:
                            if (self.dtrData.minimumBuild.ios > currentBuild) {
                                self.displayState = .updateAppVersion
                            }else{
                                self.displayState = .operation
                            }
                        default:
                            self.displayState = .onboarding
                        }
                    }
                }
            
                .onReceive(dtrData.$statusMessage) { value in
                    self.statusMessage = value
                }
            
                .onReceive(dtrData.$statusLogin) { status in
                    if status == .ready {
                        self.displayState = .operation
                    }
                }
        case .onboarding:
            NavigationView {
                OnboradingPageSetupScreen().onAppear{
                    day = 0
                }
                .onReceive(dtrData.$statusLogin) { status in
                    if status == .ready {
                        self.displayState = .operation
                    }
                }.hiddenNavigationBarStyle()
            }
        case .operation:
            var selectable = Binding(
                get: {
                    UserStore.shared.saveTabBarUniqueIdForSecond
                },
                set: {
                    if $0 == 2 {
                        if UserStore.shared.isForSecondTabId == 2 {
                            UserStore.shared.saveTabBarUniqueIdForSecond = $0
                            self.resetNavigationID1 = UUID()
                            UserStore.shared.isForSecondTabId = 3
                            print("Reset Tab = second")
                        }else{
                            UserStore.shared.isForSecondTabId = 2
                        }
                        UserStore.shared.isForFirstTabId = 0
                        UserStore.shared.isForThirdTabId = 0
                        UserStore.shared.isForFourthTabId = 0
                    } else if $0 == 1 {
                        if UserStore.shared.isForFirstTabId == 1 {
                            UserStore.shared.saveTabBarUniqueIdForSecond = $0
                            self.resetNavigationID = UUID()
                            UserStore.shared.isForFirstTabId = 5
                            print("Reset Tab = first")
                        } else {
                            UserStore.shared.isForFirstTabId = 1
                        }
                        UserStore.shared.isForSecondTabId = 0
                        UserStore.shared.isForThirdTabId = 0
                        UserStore.shared.isForFourthTabId = 0
                    } else if $0 == 3 {
                        if UserStore.shared.isForThirdTabId == 3 {
                            if UserStore.shared.isRootClassSearch {
                                UserStore.shared.saveTabBarUniqueIdForSecond = $0
                                self.resetNavigationID2 = UUID()
                                UserStore.shared.isForThirdTabId = 2222
                                UserStore.shared.isRootClassSearch = false
                                userSearchModel.resetValueArray = true
                                print("Reset Tab = Friends")
                            }
                        } else {
                            UserStore.shared.isForThirdTabId = 3
                        }
                        UserStore.shared.isForFirstTabId = 0
                        UserStore.shared.isForSecondTabId = 0
                        UserStore.shared.isForFourthTabId = 0
                    } else if $0 == 4 {
                        if UserStore.shared.isForFourthTabId == 4 {
                            if UserStore.shared.isRootClassSearch {
                                UserStore.shared.saveTabBarUniqueIdForSecond = $0
                                self.resetNavigationID3 = UUID()
                                UserStore.shared.isForFourthTabId = 22222
                                print("Reset Tab = Community")
                            }
                        } else {
                            UserStore.shared.isForFourthTabId = 4
                        }
                        UserStore.shared.isForFirstTabId = 0
                        UserStore.shared.isForSecondTabId = 0
                        UserStore.shared.isForThirdTabId = 0
                    }
                    UserStore.shared.saveTabBarUniqueIdForSecond = $0
                })
            TabView(selection: selectable) {
                MyRidesTabScreen(day:$day, views: $days).id(self.resetNavigationID)
                    .tabItem {
                        Image(systemName: "bicycle")
                        Text("My Rides")
                    }.tag(1)
                NavigationView {
                    FollowerScreen(currentDay: $day,userSearchModel:userSearchModel).id(self.resetNavigationID2)
                }.hiddenNavigationBarStyle()
                    .tabItem {
                        Image(systemName: ImageConstantsNameForChatScreen.PersonImg)
                        Text("Find Riders")
                    }.tag(3)
                NavigationView {
                    ProfileScreen(updatedDismissView:.constant(false),day: $day,userProfile: $dtrData.profile).id(self.resetNavigationID1) .navigationBarTitle("")
                }.hiddenNavigationBarStyle()
                    .tabItem {
                        Image(systemName: "person")
                        Text("My Profile")
                    }.tag(2)
                NavigationView {
                    CommunityWebContentView().id(self.resetNavigationID3) .navigationBarTitle("")
                }.hiddenNavigationBarStyle()
                    .tabItem {
                        Image("DTR-CommunityTabIcon")
                        Text("Community")
                    }.tag(4)
                    .onReceive(updateTabComunitySlcted.$tabSlectedComunity) { tabSlectedID in
                        print("Updates Slected = ",tabSlectedID)
                        if tabSlectedID == 4 {
                            selectable = .constant(4)
                            UserStore.shared.saveTabBarUniqueIdForSecond = 4
                            UserStore.shared.isForFirstTabId = 8
                            
                        }
                        if tabSlectedID == 2 {
                            selectable = .constant(3)
                            UserStore.shared.saveTabBarUniqueIdForSecond = 3
                            UserStore.shared.isForFirstTabId = 8
                        }
                    }
            }.accentColor(.white)
                .onReceive(dtrData.$statusLogin) { status in
                    if status == .signedOut {
                        self.displayState = .onboarding
                    }
                }
            
                .onAppear(perform: {
                    UIScrollView.appearance().bounces = true
                    self.resetNavigationID = UUID()
                    UserStore.shared.saveTabBarUniqueIdForSecond = 0
                    DispatchQueue.main.asyncAfter(deadline: .now()+3.0) {
                        self.dtrData.updateNotificationStatus(notification: AppNotification.init(data: ["":""]), isTypeIndex: 1)
                    }
                    
                    if #available(iOS 15.0, *) {
                        let appearance = UITabBarAppearance()
                        appearance.configureWithDefaultBackground()
                        appearance.backgroundColor = UIColor.init(Color.init(ColorConstantsName.MainThemeBgColour))
                        UITabBar.appearance().standardAppearance = appearance
                        UITabBar.appearance().scrollEdgeAppearance = appearance//ritesh
                    }
                })
        case .updateAppVersion:
            OperationalMessage(isPresented: .constant(true), title: self.dtrData.minimumBuild.title(CommonAllString.EngStr), message: self.dtrData.minimumBuild.message(CommonAllString.EngStr))
        }
    }
}
