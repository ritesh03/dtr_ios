//
//  SignupScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 03/02/21.
//

import SwiftUI
import Firebase
import MessageUI

struct SignupScreen: View {
    
    @State private var showFeedback = ""
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var verificationID = ""
    @State var userPhoneNumber = ""
    @Binding var phoneNumber: String
    @State private var showKeyboard = false
    @State private var showMailScreen = false
    @State var result: Result<MFMailComposeResult, Error>? = nil
    @State private var showVerificationClass = false
    @ObservedObject var signupViewModel: SignupViewModel = .init()    
    
    var body: some View {
        ZStack{
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack(spacing:20) {
                Text(SignupScreenString.SignupGetRideString).font(.system(size: 23, weight: .bold, design: .default))
                    .foregroundColor(.white)
                HStack {
                    Text("+1")
                        .frame(width: 40)
                    Spacer()
                        .frame(width: 1, height: 50)
                        .background(Color.init(ColorConstantsName.MainThemeBgColour))
                CustomTextFieldForSignup(becomeFirstResponder: $showKeyboard, phoneNumber: $phoneNumber, clearAllText: .constant(false), showOnlyNumberPad: .constant(true), isShownbackgroundColor: true)  { (value) in
                    if phoneNumber.count >= 10 {
                        
                        Analytics.setUserProperty(phoneNumber, forName: AnalyticsObjectEvent.start_phone_verification.rawValue)
                        
                        showFeedback = "Verifying phone number \(phoneNumber)"
                        
                        userPhoneNumber = SignupScreenString.PhoneCode + phoneNumber
                        
                        verify { response,verificationId  in
                            print("Verify Number Otp send = ",response)
                        }
                        
                    }else{
                        showFeedback = SignupScreenString.PhoneErrorString
                    }
                }
                }
                .frame(width: UIScreen.main.bounds.width - 30, height: 50)
                .background(Color.init(ColorConstantsName.HeaderBgColour))
                .foregroundColor(.white)
                .cornerRadius(10)
                .onAppear {
                   // DispatchQueue.main.asyncAfter(wallDeadline: .now()+0.5) {
                    DispatchQueue.main.async {
                        if !signupViewModel.alreayFirstResponder {
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                                showKeyboard = true
                                signupViewModel.alreayFirstResponder = true
                            }
                        }
                    }
                    //}
                }
                
                if showFeedback != "" {
                    Text(showFeedback)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                        .foregroundColor(.white)
                }
                HStack{
                    Text(SignupScreenString.NeedHelpString)
                        .foregroundColor(.white)
                    Button(action: {
                        showMailScreen = true
                    }, label: {
                        Text(SignupScreenString.EmailUsString)
                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                    })
                }
                Spacer()
            }.padding()
            .frame(width: UIScreen.screenWidth*0.9, alignment: .center)
            
            NavigationLink(destination:VerficationCodeScreen(phoneNumber: $userPhoneNumber, verificationID: $verificationID), isActive: $showVerificationClass){
                EmptyView()
            }
            
            .fullScreenCover(isPresented: $showMailScreen){
                if MFMailComposeViewController.canSendMail() {
                    MailView(result: self.$result,recipients: ["dtr@socialactive.app"],subject: "[DTR] Help",body: "<p> I have something to say about DTR:</p><p></p><p>", isHTML: true,isRideFeedback:false, rideFeedbackData: DtrRide.default)
                } else {
                    VStack {
                        Text("This device is not setup to send email")
                            .padding()
                        Button(action: { self.showMailScreen = false }) {
                            Text("Dismiss")
                        }
                        .padding()
                    }
                }
            }.hiddenNavigationBarStyle()
        }
        
        .onAppear{
            Analytics.logEvent(AnalyticsObjectEvent.intro_completed.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
            Analytics.logEvent(AnalyticsScreen.verificationPhone.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
        }
        
        .onDisappear{
            showKeyboard = false
        }
    }
   
    func verify(completion: @escaping(Bool,String) -> Void) {
        dtrData.verifyPhone(verificationPhone: "+1" + phoneNumber) { id, error in
            if let error = error {
                if (error as NSError).code == 17042 {
                    self.showFeedback = "Please enter the correct phone number."
                    Analytics.logEvent(AnalyticsObjectEvent.verification_failed.rawValue, parameters: nil)
                } else {
                    self.showFeedback = error.localizedDescription
                }
                completion(false,"")
            } else {
                if let id = id {
                    Analytics.logEvent("start_phone_verification", parameters: nil)
                    self.verificationID = id
                    showVerificationClass = true
                    completion(true,id)
                }
            }
        }
    }
}

class SignupViewModel : ObservableObject {
    @Published var alreayFirstResponder: Bool = false
}
