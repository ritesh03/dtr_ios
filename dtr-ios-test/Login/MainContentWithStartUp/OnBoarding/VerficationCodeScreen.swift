//
//  VerficationCodeScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 03/02/21.
//

import SwiftUI
import Firebase

struct VerficationCodeScreen: View {
    
    @Environment(\.presentationMode) var presentationMode
    @Binding var phoneNumber: String
    @Binding var verificationID: String
    @State private var showFeedback = ""
    @State private var showActivityIndicator = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var resendDone = false
    
    @State var maxDigits: Int = 6
    @State var pin: String = ""
    @State var showPin = true
    @State var activeSheet: ActiveSheetForVerificationClass?
    @State var showKeyboard = false
    
    
    var body: some View {
        LoadingView(isShowing: $showActivityIndicator) {
            ZStack{
                Color.init(ColorConstantsName.MainThemeBgColour)
                    .ignoresSafeArea()
                VStack(spacing:20){
                    Text("Verify your number").font(.system(size: 23, weight: .bold, design: .default))
                        .foregroundColor(.white)
                    Text("Enter the 6 digit verification code we just sent to your number.")
                        .foregroundColor(.white)
                        .multilineTextAlignment(.center)
                    VStack {
                        ZStack {
                            pinDots
                            backgroundField
                        }
                    }
                    
                    if showFeedback != "" {
                        Text(showFeedback)
                            .foregroundColor(.white)
                    }
                    
                    HStack{
                        if !resendDone {
                            Text("Didn’t get the code?")
                                .foregroundColor(.white)
                            Button(action: {
                                sendAgainOtp()
                            }, label: {
                                Text("Send again")
                                    .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                            })
                        }
                    }
                    Spacer()
                }.padding()
                    .hiddenNavigationBarStyle()
                
            }.onAppear{
                DispatchQueue.main.asyncAfter(wallDeadline: .now()+0.5) {
                    showKeyboard = true
                }
                UserStore.shared.saveTabBarUniqueIdForSecond = 0
                Analytics.logEvent(AnalyticsScreen.verification.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
            }
        }
        
        .fullScreenCover(item: $activeSheet) { item in
            switch item {
            case .HomeScreen:
                HomeViewScreen()
                    .environmentObject(self.dtrData)
                    .environmentObject(UserSettings())
            case .locationScreen:
                CreateProfileScreen()
            }
        }
    }
    
    
    func sendAgainOtp() {
        dtrData.verifyPhone(verificationPhone: phoneNumber) { id, error in
            if let error = error {
                self.showFeedback = error.localizedDescription
            } else {
                if let id = id {
                    Analytics.logEvent(AnalyticsObjectEvent.resend_verification_code.rawValue, parameters: nil)
                    self.verificationID = id
                    resendDone = true
                    self.showFeedback = "Otp Resent"
                    self.pin = ""
                }
            }
        }
    }
    
    func verifyPhoneNumberAndLogin(OtpEnderdCode:String, completion: @escaping(Bool,Bool) -> Void) {
        if OtpEnderdCode != "" {
            dtrData.signIn(id: verificationID, code: OtpEnderdCode) { newUserID,error,needToSetupProfile  in
                if let error = error {
                    showActivityIndicator = false
                    self.showFeedback = error.localizedDescription
                    Analytics.logEvent(AnalyticsObjectEvent.verification_failed.rawValue, parameters: nil)
                    completion(false,false)
                } else {
                    if newUserID != nil {
                        if needToSetupProfile == true {
                            completion(true,true)
                        }else{
                            Analytics.logEvent(AnalyticsObjectEvent.verification_completed.rawValue, parameters: nil)
                            completion(true,false)
                        }
                    }
                }
            }
        }else{
            completion(false,false)
        }
    }
    
    private var pinDots: some View {
        HStack {
            Spacer()
            ForEach(0..<maxDigits) { index in
                TextField("", text: .constant(self.getImageName(at: index)))
                    .multilineTextAlignment(.center)
                    .foregroundColor(Color.white)
                    .background(Color.init(ColorConstantsName.HeaderBgColour))
                    .cornerRadius(10)
                    .font(.system(size: 40))
                Spacer()
            }
        }
    }
    
    func getImageName(at index: Int) -> String {
        if index >= self.pin.count {
            return ""
        }
        if self.showPin {
            return self.pin.digits[index].numberString + ""
        }
        return ""
    }
    
    var backgroundField: some View {
        let boundPin = Binding<String>(get: { self.pin }, set: { newValue in
            DispatchQueue.main.async {
                self.pin = newValue
                self.submitPin(OtpStrng: CommonAllString.BlankStr) { response in
                    print(response)
                }
            }
            
        })
        return CustomTxtfldForVerificationCode(text: boundPin, isFirstResponder: showKeyboard)
            .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, maxHeight: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    }
    
    func submitPin(OtpStrng:String,completion: @escaping(String) -> Void) {
        if OtpStrng == "" {
            if pin.count == maxDigits {
                let stringArray = pin.digits.map { String($0) }
                callVerificationMethod(stringArry: stringArray) { response in
                    completion(response)
                }
            }
        }else{
            let stringArray = OtpStrng.digits.map { String($0) }
            callVerificationMethod(stringArry:stringArray) { response in
                completion(response)
            }
        }
    }
    
    func callVerificationMethod(stringArry:[String],completion: @escaping(String) -> Void) {
        showActivityIndicator = true
        print("Verification process start")
        Analytics.logEvent(AnalyticsObjectEvent.submit_verification_code.rawValue, parameters: nil)
        verifyPhoneNumberAndLogin(OtpEnderdCode: stringArry.joined(separator: "")) { (response,isNeedToSetp)  in
            if response == true {
                print("Verification process End")
                activeSheet = nil
                DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                    if isNeedToSetp {
                        activeSheet = .locationScreen
                        UserStore.shared.isFirstTimeUserRegsiter = false
                        completion("First_Time_User")
                    }else{
                        Analytics.logEvent(AnalyticsObjectEvent.submit_verification_code.rawValue, parameters: nil)
                        activeSheet = .HomeScreen
                        dtrData.statusLogin = .ready
                        UserStore.shared.isFirstTimeUserRegsiter = true
                        UserStore.shared.isVerificationComplelteOrNot = true
                        completion("Already_User")
                    }
                    print("Move to Next class")
                    showActivityIndicator = false
                }
            }else if response == false {
                completion("Wrong_OTP")
            }
        }
    }
}
