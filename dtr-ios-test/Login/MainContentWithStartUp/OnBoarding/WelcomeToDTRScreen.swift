//
//  WelcomeToDTRScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 04/02/21.
//

import SwiftUI

struct WelcomeToDTRScreen: View {
    
    @State private var showHomeScreen = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        ZStack {
            Image(ImageConstantsName.Onboarding7Img)
                .renderingMode(.original)
                .resizable()
                .ignoresSafeArea()
                .frame(width: UIScreen.screenWidth, height: UIScreen.screenHeight)
            VStack(alignment: .leading,spacing: 10) {
                Group{
                    Text("Welcome to DTR, enjoy your ride!").font(.system(size: 30, weight: .bold, design: .default))
                        .foregroundColor(.white)
                    Text("Find your friends, get out there and start riding!").font(.system(size: 25, weight: .regular, design: .default))
                        .foregroundColor(.white)
                }
                .padding()
                .padding(.bottom)
                
                
                Text("Get Started")
                .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, maxWidth: .infinity, minHeight: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .foregroundColor(.white)
                .background(Color.init(ColorConstantsName.AccentColour))
                .cornerRadius(UIScreen.screenWidth*0.8/2)
                .padding(.bottom)
                .onTapGesture {
                    UserStore.shared.registrationInProgress1 = 00011
                    dtrData.statusLogin = .ready
                    showHomeScreen = true
                }
                
                VStack(alignment: .center,spacing: 10) {
                    Image(ImageConstantsName.StartImg)
                        .resizable()
                }.frame(width: UIScreen.screenWidth*0.8, height: UIScreen.screenHeight*0.43, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            }.padding()
            
            .fullScreenCover(isPresented: $showHomeScreen){
                HomeViewScreen()
                    .environmentObject(DtrData())
                    .environmentObject(UserSettings())
            }
        }
    }
}

struct WelcomeToDTRScreen_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeToDTRScreen()
    }
}
