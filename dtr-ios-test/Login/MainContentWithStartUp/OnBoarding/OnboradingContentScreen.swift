//
//  OnboradingContentScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 03/02/21.
//

import SwiftUI

struct OnboradingContentScreen: View {
    
    @Binding var tabSelection: Int
    @State var titleArray : String
    @State var descriptionArray : String
    @State var imageArray : String
    @State var backgroundImage : String
    @State var phoneNumberCode : String = CommonAllString.BlankStr
    @State private var showSignUpScreen = false
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Image(backgroundImage)
                    .renderingMode(.original)
                    .resizable()
                    .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                    .frame(width: geometry.size.width, height: geometry.size.height)
                VStack(alignment: .leading,spacing: 10) {
                    Text(titleArray).font(.system(size: 25, weight: .bold, design: .default))
                        .foregroundColor(.white)
                        .padding()
                    Text(descriptionArray).font(.system(size: 22, weight: .regular, design: .default))
                        .foregroundColor(.white)
                        .padding(.leading)
                    Image(imageArray)
                        .resizable()
                        .padding()
                        .frame(width: UIScreen.screenWidth*0.8, height: UIScreen.screenHeight*0.43, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    Spacer()
                    HStack {
                        if self.tabSelection != 3 {
                            Button(action: {
                                showSignUpScreen = true
                            }) {
                            Text("SKIP")
                                .foregroundColor(.white)
                                .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 0))
                        }
                    }
                        Spacer()
                        Text(self.tabSelection != 3 ? CommonAllString.NextStr : "CONTINUE")
                          .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                            .onTapGesture {
                                if self.tabSelection != 3 {
                                    self.tabSelection += 1
                                }else{
                                    showSignUpScreen = true
                                }
                            }
                    }.padding(EdgeInsets(top: 0, leading: 0, bottom: 30, trailing: 0))
                }.padding()
                .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/,alignment: .leading)
                
                NavigationLink(destination:SignupScreen(phoneNumber: $phoneNumberCode), isActive: $showSignUpScreen){
                    EmptyView()
                }
            }.frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: geometry.size.height)
        }
    }
}

struct OnboradingContentScreen_Previews: PreviewProvider {
    static var previews: some View {
        OnboradingContentScreen(tabSelection: .constant(1), titleArray: CommonAllString.BlankStr, descriptionArray: CommonAllString.BlankStr, imageArray: CommonAllString.BlankStr, backgroundImage: CommonAllString.BlankStr)
    }
}

