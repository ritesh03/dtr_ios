//
//  CreateProfileScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 04/02/21.
//

import SwiftUI
import Firebase

struct CreateProfileScreen: View {
    
    @StateObject var viewModel = getOptionToUploadImageSignUp()
    @State private var displayName: String = ""
    @State private var displayLocation: String = ""
    @State private var displayCountry: String = ""
    @State private var showLocation = false
    @State private var startPlace = ""
    @State private var startCountry = ""
    @State private var showingSheet = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State private var showActivityIndicator = false
    @State private var ErrorFoundString = false
    @State private var countCheckLessThen = false
    @State private var checkLocationEntered = false
    @State var activeSheet: ActiveSheet?
    @State var isOnBoradingLat: Double = 0.0
    @State var isOnBoradingLon: Double = 0.0
    
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.init(ColorConstantsName.MainThemeBgColour)
                    .ignoresSafeArea()
                ScrollView{
                    VStack(alignment:.center,spacing:15) {
                        
                        VStack(alignment:.center,spacing:15) {
                            Group {
                                Text("About you")
                                    .font(.system(size: 20, weight: .bold, design: .default))
                                Text("Hello there! Let the community know more about you! ")
                            }
                            
                            .multilineTextAlignment(.center)
                            .fixedSize(horizontal: false, vertical:true )
                            .foregroundColor(.white)
                            
                            Button(action:{
                                self.showingSheet = true
                            }) {
                                imageView(for: viewModel.selectedImage)
                            }
                        }.frame(alignment: .center)
                        Group {
                            Text("Your display name")
                                .foregroundColor(Color.white)
                            TextField("Your Rider Name", text: $displayName)
                                .frame(height: 50)
                                .padding(.horizontal)
                                .background(Color.init(ColorConstantsName.HeaderBgColour))
                                .foregroundColor(.white)
                                .cornerRadius(10)
                                .background(RoundedRectangle(cornerRadius: 10, style: .continuous).stroke(ErrorFoundString ? Color.red : Color.clear, lineWidth: 3))
                            
                            if ErrorFoundString {
                                Text(countCheckLessThen == true ? "Rider name must be 3 characters long" : "Please enter Rider name")
                                    .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                                    .foregroundColor(Color.red)
                            }
                            
                            Group{
                                Text("This is the name that will show up in the app when your friends try to find you.")
                                    .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                                Text("Your location")
                            }.foregroundColor(Color.white)
                            HStack {
                                WrappedTextFieldForLocation(text: $displayLocation, placeholderText: .constant(CommonAllString.LocationCapsStr))
                                    .frame(height: 50)
                                    .padding(.horizontal)
                                    .background(Color.init(ColorConstantsName.HeaderBgColour))
                                    .foregroundColor(.white)
                                    .cornerRadius(10)
                                Image(ImageConstantsName.LocationMarkImg)
                                    .aspectRatio(contentMode: .fit)
                            }.onTapGesture {
                                self.showLocation = true
                                activeSheet = .location
                            }
                            .padding(.trailing)
                            .background(Color.init(ColorConstantsName.HeaderBgColour))
                            .cornerRadius(10)
                            .foregroundColor(.white)
                            Text("We only use location while using the app to change your location for public rides.")
                                .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                                .foregroundColor(.white)
                            if checkLocationEntered {
                                Text(CommonErrorString.UpdateProfileLocationError)
                                    .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                                    .foregroundColor(Color.red)
                            }
                        }.frame(maxWidth: .infinity, alignment: .leading)
                        Spacer()
                    }.padding()
                }
            }.onTapGesture {
                UIApplication.shared.endEditing()
            }
            
            .onAppear{
                UserStore.shared.registrationInProgress1 = 11100
            }
            
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("",displayMode: .inline)
            .navigationBarItems(trailing:
                                    Button(action: {
                ErrorFoundString = false
                countCheckLessThen = false
                checkLocationEntered = false
                if displayName.count >= 0 && displayName.count < 3 {
                    ErrorFoundString = true
                    countCheckLessThen = true
                    return
                }
                
                if displayLocation.isEmpty {
                    checkLocationEntered = true
                    return
                }
                
                if  displayName.count >= 3 && !checkLocationEntered {
                    updateAndCreateNewUser()
                }
            }) {
                if self.showActivityIndicator {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                        .padding()
                    Text("Updating")
                        .font(.system(size: 20, weight: .bold, design: .default))
                        .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                }else{
                    Text("Update")
                        .font(.system(size: 20, weight: .bold, design: .default))
                        .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                }
            })
            .actionSheet(isPresented: $showingSheet) {
                ActionSheet(title: Text("What do you want to do?"), buttons: [.default(Text("From Camera"), action: {
                    viewModel.takePhoto()
                }),.default(Text("From Gallery"), action: {
                    viewModel.choosePhoto()
                }),.default(Text("Cancel"))])
            }
            
            .fullScreenCover(item: $activeSheet) { item in
                switch item {
                case .welcome:
                    WelcomeToDTRScreen()
                case .location:
                    MapScreen(day: .constant(0),fullLocationName:.constant(""),isOnBoradingLat:$isOnBoradingLat,isOnBoradingLon:$isOnBoradingLon,canEditLocation:true,startCountry: $startCountry, startPlace: $startPlace, isOnBoradingProcess: .constant(true), isUpdateRideLocation: .constant(false), showRideLocationScreen: $showLocation, myRideForDay: RideEditViewModel.init(ride: DtrRide.default)){
                        self.showLocation = false
                        self.displayLocation = self.startPlace
                        self.displayCountry =  self.startCountry
                    }
                }
            }
            .background(EmptyView().sheet(isPresented:  $viewModel.isPresentingImagePicker) {
                ImagePicker(sourceType: viewModel.sourceType, completionHandler: viewModel.didSelectImage)
            })
        }
    }
    
    func updateAndCreateNewUser() {
        guard let user = Auth.auth().currentUser else {
            print("invalid user!")
            return
        }
        showActivityIndicator = true
        var newProfile = DtrProfile(id: user.uid)
        newProfile.name = displayName.trimmingCharacters(in: .whitespacesAndNewlines)
        newProfile.cityState = displayLocation.trimmingCharacters(in: .whitespacesAndNewlines)
        newProfile.country = displayCountry.trimmingCharacters(in: .whitespacesAndNewlines)
        newProfile.userLat = isOnBoradingLat
        newProfile.userLon = isOnBoradingLon
        
        print(newProfile)
        newProfile.write(dtrData.db, propagate: true) { profile, error in
            if let error = error {
                print("Profile creation error: \(error.localizedDescription)")
                return
            }
            DispatchQueue.main.async {
                if let selectedImage = viewModel.selectedImage {
                    showActivityIndicator = false
                    activeSheet = .welcome
                    UserStore.shared.isFirstTimeUserRegsiter = true
                    DtrData.sharedInstance.update(profileImage: selectedImage) {_ in
                    }
                } else {
                    showActivityIndicator = false
                    activeSheet = .welcome
                    UserStore.shared.isFirstTimeUserRegsiter = true
                }
                UserStore.shared.isVerificationComplelteOrNot = true
            }
        }
    }
    
    
    @ViewBuilder
    func imageView(for image: UIImage?) -> some View {
        if let image = image {
            Image(uiImage:image)
                .renderingMode(.original)
                .resizable()
                .frame(width: 128, height: 128)
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.gray, lineWidth: 1))
                .accessibility(label: Text("user profile picture"))
                .padding(1)
        } else {
            Text("Add photo")
                .frame(width: 120, height: 120)
                .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                .background(Color.white.opacity(0.12))
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.gray, lineWidth: 1))
        }
    }
}

class getOptionToUploadImage: ObservableObject {
    @Published var selectedImage: UIImage?
    @Published var isPresentingImagePicker = false
    @Published var isRideImage = false
    @Published var isGroupImage = false
    @Published var profileId = ""
    @Published var rideId = ""
    @Published var returnImgUrlString = ""
    var completion: () -> Void = { }
    
    private(set) var sourceType: ImagePicker.SourceType = .camera
    
    func choosePhoto() {
        sourceType = .photoLibrary
        isPresentingImagePicker = true
    }
    
    func takePhoto() {
        sourceType = .camera
        isPresentingImagePicker = true
    }
    
    func didSelectImage(_ image: UIImage?) {
        selectedImage = image
        isPresentingImagePicker = false
        if image != nil {
            if isRideImage {
                DtrData.sharedInstance.updateRideImageOnServer(RideImage: image!, profileId: profileId, rideId: rideId) { response in
                    print("Ride Image Upload = ",response)
                    self.completion()
                }
            }else if isGroupImage {
                DtrData.sharedInstance.updateGroupImageOnServer(GroupImage: image!, profileId: DtrData.sharedInstance.profileID) { response in
                    self.returnImgUrlString = response
                }
                isGroupImage = false
            }else{
                DtrData.sharedInstance.update(profileImage: image!) {_ in
                    
                }
            }
        }
    }
}


class getOptionToUploadImageSignUp: ObservableObject {
    @Published var selectedImage: UIImage?
    @Published var isPresentingImagePicker = false
    @Published var isRideImage = false
    @Published var isGroupImage = false
    @Published var profileId = ""
    @Published var rideId = ""
    @Published var groupImgUrl = ""
    
    private(set) var sourceType: ImagePicker.SourceType = .camera
    
    func choosePhoto() {
        sourceType = .photoLibrary
        isPresentingImagePicker = true
    }
    
    func takePhoto() {
        sourceType = .camera
        isPresentingImagePicker = true
    }
    
    func didSelectImage(_ image: UIImage?) {
        selectedImage = image
        isPresentingImagePicker = false
    }
}

enum ActiveSheet: Identifiable {
    case welcome, location
    var id: Int {
        hashValue
    }
}

enum ActiveSheetForVerificationClass: Identifiable {
    case HomeScreen, locationScreen
    var id: Int {
        hashValue
    }
}


enum ActiveSheetForRideDetailScreen: Identifiable {
    case mapScreen, MailScreen
    var id: Int {
        hashValue
    }
}

enum ActiveSheetForGroupDetailScreen: Identifiable {
    case ProfileScreen, CreateEditGroupScreen
    var id: Int {
        hashValue
    }
}

