//
//  OnboradingPageSetupScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 01/03/21.
//

import SwiftUI
import Firebase

struct OnboradingPageSetupScreen: View {
    @State var titleArray = ["Welcome to DTR, enjoy your ride!", "We Believe", "See who's DTR day-by-day","Share rides to get the word out"]
    @State var descriptionArray = ["Find your friends, get out there and start riding!", "Cycling is an inclusive sport, activity, and social setting. Having a great time is more important than how fast you ride.", "Ride info is organized by the day to see all of your friends, people you follow, and public rides.", "Invite other friends and followers in the app or share on Facebook, Slack, or text groups."]
    @State var imageArray = ["DTR-WelcomeToDtr", "DTR-CoupleCycle","DTR-LocationWithRideDetail","DTR-ShareRideICon"]
    @State var imagbackgroundArray = ["DTR-Onboarding4", "DTR-Onboarding2", "DTR-Onboarding3", "DTR-Onboarding5"]
    @State private var tabSelection = 0
    
     var body: some View {
        ZStack {
            Image(imagbackgroundArray[tabSelection])
                .resizable()
                .ignoresSafeArea()
            TabView (selection: $tabSelection){
                ForEach(0..<4){ index in
                    OnboradingContentScreen(tabSelection: $tabSelection, titleArray: titleArray[index], descriptionArray: descriptionArray[index], imageArray: imageArray[index],backgroundImage:imagbackgroundArray[index]).tag(index)
                }
            }
            .tabViewStyle(PageTabViewStyle())
            .onAppear {
               Analytics.logEvent(AnalyticsScreen.onBoardingScreen.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
                UIScrollView.appearance().bounces = false
            }
            .navigationBarTitle(CommonAllString.BlankStr, displayMode: .inline)
        }
    }
}

struct PageSetup_Previews: PreviewProvider {
    static var previews: some View {
        OnboradingPageSetupScreen()
    }
}
