//
//  OperationalMessage.swift
//  dtr-ios-test
//
//  Created by Mobile on 20/05/21.
//

import SwiftUI

struct OperationalMessage: View {
    @Binding var isPresented: Bool
    var title: String
    var message: String
    let buttonWidth: CGFloat = 200
    let buttonHeight: CGFloat = 50
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        ZStack {
            StartUpBackgroundScreen(comesfromMainScreen:true)
            VStack(alignment: .leading) {
                HStack {
                    Text(title.uppercased()).font(.largeTitle).fontWeight(.bold)
                    Spacer()
                }
                .padding()
                
                HStack {
                    Text(message).font(.body).fontWeight(.bold).foregroundColor(Color.init(ColorConstantsName.AlertColour))
                    Spacer()
                }
                .padding()
                
                HStack {
                    Button(action: {
                        guard let url = URL(string: DefaultUrlString.AppsStoreUrlStr) else {
                            return
                        }
                        UIApplication.shared.open(url)
                    }) {
                        Text(CommonAllString.UpdateNowStr)
                            .fontWeight(.bold)
                            .padding(12)
                            .background(Color.init(ColorConstantsName.CrownColour))
                            .cornerRadius(CGFloat(8))
                            .foregroundColor(.black)
                    }
                    .padding()
                    Spacer()
                }.frame(width: UIScreen.screenWidth)
                Spacer()
            }
            .foregroundColor(.white)
         }
    }
}
