//
//  AllUserInOneTab.swift
//  dtr-ios-test
//
//  Created by Mobile on 24/05/21.
//

import SwiftUI
import Combine
import Firebase

struct AllUserInOneTab: View {
    
    @State var selectedTab: Int = 0
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var profile: DtrProfile
    @StateObject var userListViewModel = UserListViewModel()
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var showActivityIndicator : Bool = false
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .edgesIgnoringSafeArea(.all)
            GeometryReader { geo in
                VStack(spacing: 0) {
                    CustomTabViewHighlight(geoWidth: geo.size.width, selectedTab: $selectedTab)
                    TabView(selection: $selectedTab,content: {
                        UserListTabView(showActivityIndicator: $showActivityIndicator, statusTabId: $selectedTab, profile: $profile)
                            .tag(0)
                        UserListTabView1(showActivityIndicator: $showActivityIndicator, statusTabId: $selectedTab, profile: $profile)
                            .tag(1)
                        UserListTabView2(showActivityIndicator: $showActivityIndicator, statusTabId: $selectedTab, profile: $profile)
                            .tag(2)
                    })
                    .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
                }
                .navigationBarTitle("", displayMode: .inline)
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading:leadingButton,trailing:trainlingButton)
            }
        }
    }
    
    var leadingButton: some View {
        Button(action: {
            self.presentationMode.wrappedValue.dismiss()
        }) {
            HStack {
                Image(systemName: ImageConstantsName.ArrowLeftImg)
            }}
    }
    
    var trainlingButton: some View {
        Button(action: {
        }) {
            if self.showActivityIndicator {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                    .foregroundColor(.white)
                    .padding()
            }
        }
    }
}


struct UserListTabView: View {
    
    @Binding var showActivityIndicator:Bool
    @Binding var statusTabId:Int
    @Binding var profile: DtrProfile
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    @State private var showProfileScreen: Bool = false
    @State var listOfGrouoc: [UserMutualStatusModel] = []
    
    @State  var userFollings = [DtrProfile]()
    @State  var alreadyDone1 = false
    @State var showFriendSearch = false
    @State var receivedResponse = false

    var body: some View {
        ZStack {
        List {
            if statusTabId == 0 {
                if userFollings.count > 0 {
                    ForEach(userFollings.indices, id: \.self) { indexs in
                        UserSearchView(day:.constant(0),userProfiles: $userFollings[indexs], UserMutualStatusModel: $listOfGrouoc,showProfileScreen:$showProfileScreen).listRowInsets(EdgeInsets())
                            .listRowBackground(Color.clear)
                    }
                }
            }
        }.listStyle(PlainListStyle())
        
        if userFollings.count == 0 && statusTabId == 0 && receivedResponse {
            VStack {
                HStack {
                    Spacer()
                    ShowFindFriendsBtnAndText(navigateToFindScreen:$showFriendSearch)
                    Spacer()
                }.padding(.top, 50)
                Spacer()
            }
        }
        NavigationLink(destination:FollowerScreen(currentDay:.constant(0),comesFromChatScreen:true), isActive: $showFriendSearch) {
        }
            
        }
        
        .onAppear {
            DispatchQueue.main.async {
                self.updateInoProfile(returId: self.statusTabId)
            }
        }
    }
    
    func updateInoProfile(returId:Int){
        getFollowings(profile: self.profile)
    }
    
    
    func getFollowings(profile:DtrProfile) {
        if !alreadyDone1 {
            alreadyDone1 = true
            var profilesDict = [String:DtrProfile]()
            showActivityIndicator = true
            dtrData.resolveFollowingProfiles(profile: profile) { resolvedProfile in
                profilesDict = resolvedProfile.followingProfiles
                getMutualConnectionStatus(profileIds: resolvedProfile.following)
                self.userFollings =  Array(profilesDict.values).sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
                self.showActivityIndicator = false
                self.receivedResponse = true
            }
        }
    }
    
    func getMutualConnectionStatus(profileIds:[String]) {
        dtrCommand.getMutualConnectionListOfUsers(userProfilesIds: profileIds) { response in
            if response.result == .ok {
                let dictUsr = [asDataDict(response.output)]
                for i in 0..<dictUsr.count {
                    if let val = dictUsr[i] as? [String:String] {
                        for objectID in val.keys {
                            if let name = val[objectID] {
                                self.listOfGrouoc.append(UserMutualStatusModel(userId: objectID, staus: name))
                            }
                        }
                    }
                }
            }
        }
    }
}

struct UserListTabView1: View {
    
    @Binding var showActivityIndicator:Bool
    @Binding var statusTabId:Int
    @Binding var profile: DtrProfile
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    @State private var showProfileScreen: Bool = false
    @State var listOfGrouoc: [UserMutualStatusModel] = []
    
    
    @State  var userFollowers = [DtrProfile]()
    
    @State  var alreadyDone1 = false
    @State var showFriendSearch = false
    @State var receivedResponse = false
    
    var body: some View {
        ZStack {
            List {
                if statusTabId == 1 {
                    if userFollowers.count > 0 {
                        ForEach(userFollowers.indices, id: \.self) { indexs in
                            UserSearchView(day:.constant(0),userProfiles: $userFollowers[indexs], UserMutualStatusModel: $listOfGrouoc,showProfileScreen:$showProfileScreen).listRowInsets(EdgeInsets())
                                .listRowBackground(Color.clear)
                        }
                    }
                }
            }.listStyle(PlainListStyle())
            
            if userFollowers.count == 0 && statusTabId == 1 && receivedResponse {
                VStack {
                    HStack {
                        Spacer()
                        ShowFindFriendsBtnAndText(navigateToFindScreen:$showFriendSearch)
                        Spacer()
                    }.padding(.top, 50)
                    Spacer()
                }
            }
            NavigationLink(destination:FollowerScreen(currentDay:.constant(0),comesFromChatScreen:true), isActive: $showFriendSearch) {
            }
            
        }
        .onAppear {
            DispatchQueue.main.async {
                self.updateInoProfile(returId: self.statusTabId)
            }
        }
    }
    
    
    func updateInoProfile(returId:Int){
        getFollowers(profile: self.profile)
    }
    
    
    func getFollowers(profile:DtrProfile) {
        if !alreadyDone1 {
            alreadyDone1 = true
            var profilesDict = [String:DtrProfile]()
            showActivityIndicator = true
            dtrData.resolveFollowedProfiles(profile: profile) { resolvedProfile in
                profilesDict = resolvedProfile.followdProfiles
                getMutualConnectionStatus(profileIds: resolvedProfile.followed)
                self.userFollowers =  Array(profilesDict.values).sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
                self.showActivityIndicator = false
                self.receivedResponse = true
            }
        }
    }
    
    func getMutualConnectionStatus(profileIds:[String]) {
        dtrCommand.getMutualConnectionListOfUsers(userProfilesIds: profileIds) { response in
            if response.result == .ok {
                let dictUsr = [asDataDict(response.output)]
                for i in 0..<dictUsr.count {
                    if let val = dictUsr[i] as? [String:String] {
                        for objectID in val.keys {
                            if let name = val[objectID] {
                                self.listOfGrouoc.append(UserMutualStatusModel(userId: objectID, staus: name))
                            }
                        }
                    }
                }
            }
        }
    }
}



struct UserListTabView2: View {
    
    @Binding var showActivityIndicator:Bool
    @Binding var statusTabId:Int
    @Binding var profile: DtrProfile
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var showProfileScreen: Bool = false
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    @State var listOfGrouoc: [UserMutualStatusModel] = []
    
    @State  var userProfiles = [DtrProfile]()
    @State  var alreadyDone2 = false
    @State var showFriendSearch = false
    @State var receivedResponse = false
    
    var body: some View {
//        List {
//            if statusTabId == 2 {
//                if userProfiles.count > 0 {
//                    ForEach(userProfiles.indices, id: \.self) { indexs in
//                        UserSearchView(day:.constant(0),userProfiles: $userProfiles[indexs], UserMutualStatusModel: $listOfGrouoc,showProfileScreen:$showProfileScreen).listRowInsets(EdgeInsets())
//                            .listRowBackground(Color.clear)
//                    }
//                }
//            }
//        }
        ZStack {
            List {
                if statusTabId == 2 {
                    if userProfiles.count > 0 {
                        ForEach(userProfiles.indices, id: \.self) { indexs in
                            UserSearchView(day:.constant(0),userProfiles: $userProfiles[indexs], UserMutualStatusModel: $listOfGrouoc,showProfileScreen:$showProfileScreen).listRowInsets(EdgeInsets())
                                .listRowBackground(Color.clear)
                        }
                    }
                }
            }.listStyle(PlainListStyle())
            
            if userProfiles.count == 0 && statusTabId == 2 && receivedResponse {
                VStack {
                    HStack {
                        Spacer()
                        ShowFindFriendsBtnAndText(navigateToFindScreen:$showFriendSearch)
                        Spacer()
                    }.padding(.top, 50)
                    Spacer()
                }
            }
            NavigationLink(destination:FollowerScreen(currentDay:.constant(0),comesFromChatScreen:true), isActive: $showFriendSearch) {
            }
        }

        .onAppear {
            DispatchQueue.main.async {
                self.updateInoProfile(returId: self.statusTabId)
            }
        }
    }
    
    func updateInoProfile(returId:Int){
        getFriends(profile: self.profile)
    }
    
    func getFriends(profile:DtrProfile) {
        if !alreadyDone2 {
            alreadyDone2 = true
            var profilesDict = [String:DtrProfile]()
            showActivityIndicator = true
            dtrData.resolveFriendProfiles(profile: profile) { resolvedProfile in
                profilesDict = resolvedProfile.friendProfiles
                getMutualConnectionStatus(profileIds: resolvedProfile.friends)
                self.userProfiles =  Array(profilesDict.values).sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
                self.showActivityIndicator = false
                self.receivedResponse = true
            }
        }
    }
    
    func getMutualConnectionStatus(profileIds:[String]) {
        dtrCommand.getMutualConnectionListOfUsers(userProfilesIds: profileIds) { response in
            if response.result == .ok {
                let dictUsr = [asDataDict(response.output)]
                for i in 0..<dictUsr.count {
                    if let val = dictUsr[i] as? [String:String] {
                        for objectID in val.keys {
                            if let name = val[objectID] {
                                self.listOfGrouoc.append(UserMutualStatusModel(userId: objectID, staus: name))
                            }
                        }
                    }
                }
            }
        }
    }
}

