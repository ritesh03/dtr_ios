//
//  ShowSearchBtnAndText.swift
//  dtr-ios-test
//
//  Created by apple on 01/10/21.
//

import SwiftUI
import Firebase

struct ShowSearchBtnAndText: View {
    @Binding var showGruopListScreen: Bool
    var body: some View {
        ZStack {
            VStack(alignment:.center, spacing: 15) {
                Group {
                    Text(FollowScreenString.EnterRiderNameStr)
                    Text(CommonAllString.OrStr)
                    Text(FollowScreenString.ClickHereToSearchStr)
                    CustomButtonCircleWithCustomProperty(btnTitle: FollowScreenString.SearchGroupStr, btnWidth: UIScreen.screenWidth*0.65, btnHeight: 20, backgroundColord: true, txtColorAccent: false, strokeColor: false, textBold: false, lineWidth: false, disabled: false) {
                            showGruopListScreen = true
                        }.padding(.top)
                }
            }.foregroundColor(.white)
            .padding(.top,50)
        }
    }
}


struct ShowFindFriendsBtnAndText: View {
    @Binding var navigateToFindScreen: Bool
    var body: some View {
        ZStack {
            VStack(alignment:.center, spacing: 15) {
                Group {
                    Text(FollowScreenString.ConnectMorePeopleStr)
                    CustomButtonCircleWithCustomProperty(btnTitle: FollowScreenString.FindOtherRidersStr, btnWidth: UIScreen.screenWidth*0.65, btnHeight: 20, backgroundColord: true, txtColorAccent: false, strokeColor: false, textBold: false, lineWidth: false, disabled: false) {
                        navigateToFindScreen = true
                    }.padding(.top)
                }
            }.foregroundColor(.white)
            .padding(.top,50)
        }
    }
}
