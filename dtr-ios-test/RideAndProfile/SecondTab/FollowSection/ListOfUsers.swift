//
//  ListOfUsers.swift
//  dtr-ios-test
//
//  Created by Mobile on 28/01/21.
//

import SwiftUI

struct ListOfUsers: View {
    
    @ObservedObject var dtrData = DtrData.sharedInstance
    @StateObject var userListViewModel = UserListViewModel()
    @State private var profilesDict = [String:DtrProfile]()
    @State private var profilesArray = [DtrProfile]()
    @State var profile: DtrProfile
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var checkStatus = ""
    @State private var checkStatusIsFriend :Bool =  false
    
    var sortedFriends: [DtrProfile] {
        return Array(self.profilesDict.values).sorted(by: {(first: DtrProfile, second: DtrProfile) -> Bool in return first.name.lowercased() < second.name.lowercased() })
    }
    
    var body: some View {
        ZStack{
            Color.init(MAIN_THEME_BG_COLOUR)
                .ignoresSafeArea()
            VStack {
                HStack {
                    Group {
                        Text("Cyclists")
                        Text("\(userListViewModel.userProfiles.count)")
                    }
                    .foregroundColor(Color.white)
                    Spacer()
                    if checkStatus  != "Following" && checkStatus  != "Friends"{
                        Text("Follow All")
                            .foregroundColor(Color.init(DTR_ACCENT_COLOUR))
                            .frame(width: 90, height: 30, alignment: .center)
                    }
                }.padding()
                .background(Color.init("DTR-HeaderBackground1"))
                .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, maxWidth: .infinity,alignment: .topLeading)
                ScrollView{
                    VStack{
                        if userListViewModel.userProfiles.count > 0 {
                            ForEach(userListViewModel.userProfiles.indices, id: \.self) { indexs in
                                UserSearchView(day:.constant(0),userProfiles: $userListViewModel.userProfiles[indexs], UserMutualStatusModel: $listOfGrouoc,,showProfileScreen:.constant(false))
                                    .listRowBackground(Color.clear)
                            }
                        }
                    }
                }
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarItems(leading:
                                Button(action: {
                                    self.presentationMode.wrappedValue.dismiss()
                                }) {
                                    HStack {
                                        Image(systemName: "arrow.left")
                                        Text(checkStatus)
                                    }})
        .onAppear {
            if checkStatus  == "Following" {
                checkStatusIsFriend = true
                if self.profile.id == dtrData.profile.id {
                    userListViewModel.getFollowings(profile: dtrData.profile)
                }else{
                    userListViewModel.getFollowings(profile: self.profile)
                }
            }else  if checkStatus  == "Followers" {
                checkStatusIsFriend = true
                userListViewModel.getFollowers(profile: self.profile)
            }else if checkStatus  == "Friends"{
                checkStatusIsFriend = false
                userListViewModel.getFriends(profile: self.profile)
            }
        }
    }
}

struct ListOfUsers_Previews: PreviewProvider {
    static var previews: some View {
        ListOfUsers(profile: DtrProfile.default)
    }
}
