//
//  UsersSearchView.swift
//  dtr-ios-test
//
//  Created by Mobile on 27/01/21.
//

import SwiftUI
import Firebase

struct UserSearchView: View {
    
    @Binding var day : Int
    @Binding var userProfiles : DtrProfile
    @Binding var UserMutualStatusModel : [UserMutualStatusModel]
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var titleStatus = "Follow"
    @State private var showActivityIndicator = false
    @Binding var showProfileScreen : Bool
    
    var body: some View {
        NavigationLink(destination: ProfileScreen(updatedDismissView:.constant(false),day: $day,userProfile: $userProfiles, showBackButtonOrNot:true).onAppear {
            UserStore.shared.isRootClassSearch = true
        }) {
            VStack {
                HStack(alignment:.top) {
                    ProfileHead(profile: userProfiles, size: .medium)
                    VStack(alignment: .leading, spacing: 10){
                        HStack{
                            if userProfiles.name != "" {
                                Text(userProfiles.name)
                                    .foregroundColor(.white)
                                    .lineLimit(1)
                            }
                            Spacer()
                            Group {
                                if dtrData.profileID != userProfiles.id {
                                    if !dtrData.profile.friends.contains(userProfiles.id) {
                                        if self.showActivityIndicator {
                                            ProgressView()
                                                .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                                .frame(width: 20, height: 20)
                                                .padding(.trailing,5)
                                        }
                                        if titleStatus != "requestedFriend" {
                                            Button(action: {
                                                callFuntionAccToStatus()
                                            }) {
                                                Text(getStatusOfUser(userId: userProfiles.id))
                                                    .multilineTextAlignment(.trailing)
                                                    .lineLimit(2)
                                                    .foregroundColor(titleStatus == "Friend request sent" ? Color.gray : Color.init(ColorConstantsName.AccentColour))
                                                    .frame(width: titleStatus == "Follow" ? 90 : 115, alignment: .center).padding(5)
                                                    .font(.system(size: 15, weight: .bold, design: .default))
                                            }.buttonStyle(BorderlessButtonStyle())
                                                .clipShape(Capsule())
                                                .overlay(Capsule().stroke(Color.init(ColorConstantsName.AccentColour), lineWidth: titleStatus == "Friend request sent" ? 0 : 1))
                                        }
                                    }
                                }
                            }.disabled(showActivityIndicator == true ? true : false)
                        }
                        
                        
                        if userProfiles.description != "" {
                            Text(userProfiles.description).font(.system(size: 14))
                                .lineLimit(2)
                                .foregroundColor(.gray)
                        }
                        HStack(alignment:.center) {
                            Group {
                                Text(getCityCountryOfUser()).font(.system(size: 14))
                            }.foregroundColor(.gray)
                                .opacity(0.65)
                        }
                        
                        HStack(alignment:.center) {
                            Group {
                                Text(UserMutualStatusModelListing(userId: userProfiles.id)).font(.system(size: 14))
                            }.foregroundColor(.white)
                                .opacity(0.65)
                        }
                    }
                    .frame(maxWidth: .infinity ,alignment: .topLeading)
                    //.padding(.horizontal)
                }
                .padding(.all)
                Divider()
                    .background(Color.white)
            }
            .onChange(of: userProfiles.isFollower) { ( newValue ) in
                userProfiles.isFollower = newValue
            }
        }
    }
    
    func UserMutualStatusModelListing(userId:String) -> String {
        var status = ""
        for i in 0..<UserMutualStatusModel.count {
            if let val = UserMutualStatusModel[i] as? UserMutualStatusModel {
                if val.userId == userId {
                    status = val.staus
                }
            }
        }
        return status
    }
    
    func getCityCountryOfUser() -> String {
        var status = ""
        if userProfiles.cityState != ""  &&  userProfiles.cityState != "<null>" {
            status = userProfiles.cityState
        }
        if userProfiles.country != "" && userProfiles.country != "<null>" {
            status += ", "
            status += userProfiles.country
        }
        return status
    }
    
    func getStatusOfUser(userId:String) -> String {
        var status = "Follow"
        if dtrData.relationship(profileID: userId).rawValue == "following" {
            status = "Request Friend"
        }else if dtrData.relationship(profileID: userId).rawValue == "follwoingWithInvite" {
            status = "Friend request sent"
        }else if dtrData.relationship(profileID: userId).rawValue == "requestedFriend" {
            status = "requestedFriend"
        }
        DispatchQueue.main.async {
            titleStatus = status
        }
        return status
    }
    
    
    func callFuntionAccToStatus() {
        if userProfiles.id != "" {
            if titleStatus == "Follow" {
                showActivityIndicator = true
                self.dtrCommands.profileFollow(profileID: userProfiles.id){ respose in
                    respose.debug(methodName:"profileFollow")
                    if respose.feedback.contains("already friends") {
                        titleStatus = "Following"
                        showActivityIndicator = false
                    } else {
                        titleStatus = (respose.result == .error) ? respose.feedback : "Following"
                        if titleStatus == "Following" {
                            userProfiles.isFollower = true
                            userProfiles.followed.append(dtrData.profileID)
                        }
                        showActivityIndicator = false
                    }
                }
            }else if titleStatus == "Request Friend" {
                showActivityIndicator = true
                self.dtrCommands.sendFriendRequest(profileID: userProfiles.id){ respose in
                    respose.debug(methodName:"sendFriendRequest")
                    showActivityIndicator = false
                }
            }
        }
    }
}
