import SwiftUI
import Firebase

struct FollowerScreen: View {
    
    @Binding var currentDay : Int
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var userSearchModel: UserSearchViewModel = .init()
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    @State private var titleDtr: String = ""
    @State private var clearAllText: Bool = false
    @State private var showActivityIndicator = false
    @State private var totalCountArray = 0
    @State var comesFromChatScreen = false
    @State var updateKeyboard = false
    
    @State private var page: Int = 0
    @State private var totalUserCount: Int = 0
    @State private var showListOrNot = false
    
    @State var userProfiles = [DtrProfile]()
    @State var listOfGrouoc: [UserMutualStatusModel] = []
    
    @State private var showGruopListScreen = false
    
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack(spacing: 15) {
                HStack {
                    CustomTxtfldForFollowerScrn(isDeleteAcntScreen: .constant(false), isGroupListScreen: .constant(false), text:  $titleDtr, clearAllText: $clearAllText, isFirstResponder: $updateKeyboard, completion: { (reponse) in
                        updateKeyboard = true
                        if titleDtr.count >= 3 {
                            page = 0
                            showActivityIndicator = true
                            totalCountArray = 0
                            showListOrNot = true
                            userProfiles.removeAll()
                            userSearchModel.searchUsers1(searchKeyboard: titleDtr, data: ["page": "0","hitsPerPage":20], completion: { response,totalCount,profileIds in
                                showActivityIndicator = false
                                userProfiles = response
                                totalUserCount = totalCount
                                getMutualConnectionStatus(profileIds: profileIds)
                            })
                        }else if titleDtr.count <= 3 {
                            DispatchQueue.main.async {
                                page = 0
                                showActivityIndicator = false
                                totalCountArray = 0
                                showListOrNot = false
                                userProfiles.removeAll()
                            }
                        }
                    })
                    
                    if self.showActivityIndicator {
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                            .frame(width: 30, height: 30)
                    }
                    
                    Button(action: {
                        withAnimation {
                            page = 0
                            totalCountArray = 0
                            userProfiles.removeAll()
                            clearAllText = true
                            updateKeyboard = false
                            showListOrNot = false
                        }
                    }) {
                        Image(systemName: ImageConstantsName.XmarkImg)
                            .aspectRatio(contentMode: .fit)
                    }
                }
                .padding()
                .background(Color.init(ColorConstantsName.SectionBgColour))
                .cornerRadius(10)
                .foregroundColor(.white)
                .padding(.all)
                .frame(height: 50)
                
                if titleDtr.count == 0 {
                    ShowSearchBtnAndText(showGruopListScreen:$showGruopListScreen)
                }
                
                VStack(spacing:0) {
                    List {
                        if userProfiles.count > 0 {
                            ForEach(userProfiles.indices, id: \.self, content: row(for:)).background(Color.clear).listRowBackground(Color.clear).listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
                                .buttonStyle(PlainButtonStyle())
                        }
                    }.listStyle(PlainListStyle()).listRowBackground(Color.clear)
                }
            }
            NavigationLink(destination:AllGroupLists(dismissView: $showGruopListScreen), isActive: $showGruopListScreen){
                EmptyView()
            }
        }
        
        .onDisappear{
            updateKeyboard = false
        }
        
        .onReceive(userSearchModel.$resetValueArray, perform: { response in
            if response {
                userProfiles = []
            }
        })
        
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarItems(leading:
                                Button(action: {
                                    UIApplication.shared.endEditing()
                                    
                                    if comesFromChatScreen {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }
                                }) {
                                    HStack {
                                        if comesFromChatScreen {
                                            Image(systemName: ImageConstantsName.ArrowLeftImg)
                                        }
                                        Text("Find and Invite")
                                    }.foregroundColor(.white)}.disabled(!comesFromChatScreen))
    }
    
    func row(for idx: Int) -> some View {
        let isOn = Binding(
            get: {
                idx < self.userProfiles.count ? self.userProfiles[idx] : DtrProfile.default
            },
            set: {
                let isIndexValid = self.userProfiles.indices.contains(idx)
                if isIndexValid {
                    self.userProfiles[idx] = $0
                }
            }
        )
        
        return UserSearchView(day:$currentDay,userProfiles:isOn, UserMutualStatusModel:$listOfGrouoc,showProfileScreen:.constant(false)).listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0)).listRowBackground(Color.clear).onAppear {
            if ((userProfiles.endIndex - 1) == idx) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    if userProfiles.count != totalUserCount && userProfiles.count < totalUserCount{
                        if titleDtr != "" {
                            page += 1
                            userSearchModel.searchUsers1(searchKeyboard: titleDtr, data: ["page": "\(page)","hitsPerPage":20], completion: { response,totalCount,profileIds  in
                                for i in 0..<response.count {
                                    userProfiles.append(response[i])
                                }
                                getMutualConnectionStatus(profileIds: profileIds)
                            })
                        }
                    }
                }
            }
        }.listRowBackground(Color.clear)
    }
    
    func getMutualConnectionStatus(profileIds:[String]) {
        dtrCommand.getMutualConnectionListOfUsers(userProfilesIds: profileIds) { response in
            if response.result == .ok {
                let dictUsr = [asDataDict(response.output)]
                for i in 0..<dictUsr.count {
                    if let val = dictUsr[i] as? [String:String] {
                        for objectID in val.keys {
                            if let name = val[objectID] {
                                self.listOfGrouoc.append(UserMutualStatusModel(userId: objectID, staus: name))
                            }
                        }
                    }
                }
            }
        }
    }
}

struct FollowerScreen_Previews: PreviewProvider {
    static var previews: some View {
        FollowerScreen(currentDay:.constant(0))
    }
}
