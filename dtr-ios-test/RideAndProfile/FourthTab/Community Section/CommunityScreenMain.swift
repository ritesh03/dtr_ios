//
//  CommunityScreenMain.swift
//  dtr-ios-test
//
//  Created by apple on 18/10/21.
//

import SwiftUI

struct CommunityScreenMain: View {
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.HeaderBgColour)
            VStack {
                CommunityScreenBannerUI()
                CommunityScreenCommunityUI()
                List {
                    CommunityScreenListUI(showRigtImage: false)
                    CommunityScreenListUI(showRigtImage: true)
                    CommunityScreenListUI(showRigtImage: false)
                    CommunityScreenListUI(showRigtImage: true)
                }
            }
        }
    }
}

struct CommunityScreenMain_Previews: PreviewProvider {
    static var previews: some View {
        CommunityScreenMain()
    }
}
