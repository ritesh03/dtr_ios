//
//  CommunityWebView.swift
//  dtr-ios-test
//
//  Created by apple on 22/10/21.
//

import WebKit
import SwiftUI
import Combine
import Foundation

struct ProgessViewCustom: View {
    
    @Binding var showActivityIndicator:Bool
    
    var body: some View {
        VStack {
            if showActivityIndicator {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.gray))
                    .frame(width: 20, height: 20)
                    .padding(.trailing,5)
            }
        }
    }
}

struct CommunityWebContentView: View {
    
    @StateObject var viewModel = ViewModel()
    @State var showLoader = false
    @State var message = ""
    @State var webTitle = ""
    
    // For WebView's forward and backward navigation
    var webViewNavigationBar: some View {
        VStack(spacing: 0) {
            Divider()
            HStack {
                Spacer()
                Button(action: {
                    self.viewModel.webViewNavigationPublisher.send(.backward)
                }) {
                    Image(systemName: ImageConstantsName.ArrowLeftCircleImg)
                        .font(.system(size: 20, weight: .regular))
                        .imageScale(.large)
                        .foregroundColor(.blue)
                }
                
                Button(action: {
                    self.viewModel.webViewNavigationPublisher.send(.forward)
                }) {
                    Image(systemName: ImageConstantsName.ArrowRightCircleImg)
                        .font(.system(size: 20, weight: .regular))
                        .imageScale(.large)
                        .foregroundColor(.blue)
                }
                Spacer()
            }.frame(height: 45)
            Divider()
        }
    }
    
    var body: some View {
        ZStack {
            VStack(alignment:.trailing) {
                VStack(spacing: 0) {
                    WebView(url: .publicUrl, viewModel: viewModel).overlay(ProgessViewCustom(showActivityIndicator: $showLoader), alignment: .center)
                    webViewNavigationBar
                }.onReceive(self.viewModel.showLoader.receive(on: RunLoop.main)) { value in
                    self.showLoader = value
                }
            }
        }.hiddenNavigationBarStyle()
    }
}
