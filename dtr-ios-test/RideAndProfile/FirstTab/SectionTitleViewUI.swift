//
//  SectionTitleUI.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/01/21.
//

import SwiftUI

struct SectionTitleViewUI: View {
    var titleString = "2"
    var rightTitleString = "3"
    var countShow = 4
    var comeFromPublicRide = false
    var changeFontSize = false
    var action: (() -> Void)?
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text(titleString).font(.system(size:changeFontSize == true ? 15 : 18, weight: .bold, design: .default))
                    .foregroundColor(.white)
                    .padding(.leading)
                    .fixedSize(horizontal: false, vertical: true)
                if countShow != 0{
                    Text("\(countShow)").font(.system(size: 16, weight: .bold, design: .default))
                        .foregroundColor(.gray)
                        .padding(.leading,0)
                }
                Spacer()
                if comeFromPublicRide {
                    Image(ImageConstantsName.MapMarkerImg)
                        .resizable()
                        .frame(width: 15, height: 19, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    showTitleText
                        .fixedSize(horizontal: false, vertical: true)
                        .multilineTextAlignment(.trailing)
                }else{
                    showTitleText
                }
            }.padding(5)
            .frame(height: 45)
            .background(Color.init(ColorConstantsName.SectionBgColour))
        }.listRowBackground(Color.clear)
    }
    
    var showTitleText: some View {
        Group {
            if rightTitleString != CommonAllString.BlankStr {
                Button(action: self.action!) {
                    HStack {
                        Text(rightTitleString).font(.system(size: 18, weight: .bold, design: .default))
                            .foregroundColor(Color.init(comeFromPublicRide ==  true ? ColorConstantsName.HeaderForGrandColour : ColorConstantsName.AccentColour))
                            .padding(.trailing,15)
                    }}
            }
        }
    }
}

struct SectionTitleUI_Previews: PreviewProvider {
    static var previews: some View {
        SectionTitleViewUI()
    }
}


struct SectionTitleViewUIForInviteGroup: View {
    var titleString = "2"
    var rightTitleString = "3"
    var countShow = 4
    var comeFromPublicRide = false
    var changeFontSize = false
    var isOpen = false
    var action: (() -> Void)?
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            HStack {
                Text(titleString).font(.system(size:changeFontSize == true ? 15 : 18, weight: .bold, design: .default))
                    .foregroundColor(.white)
                    .padding(.leading)
                    .fixedSize(horizontal: false, vertical: true)
                if countShow != 0{
                    Text("\(countShow)").font(.system(size: 16, weight: .bold, design: .default))
                        .foregroundColor(.gray)
                        .padding(.leading,0)
                }
                Spacer()
                if comeFromPublicRide {
                    Image(ImageConstantsName.MapMarkerImg)
                        .resizable()
                        .frame(width: 15, height: 19, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    showTitleText
                        .fixedSize(horizontal: false, vertical: true)
                        .multilineTextAlignment(.trailing)
                }else{
                    showTitleText
                }
            }.padding(5)
            .frame(height: 55)
            .background(Color.init(ColorConstantsName.SectionBgColour))
            
            HStack() {
                Spacer()
                    .frame(width: 12)
                Text("")
                    .frame(height: 0.5)
                    .frame(maxWidth: .infinity)
                    .background(Color.white)
            }
            
            
        }.listRowBackground(Color.clear)
            
    }
    
    var showTitleText: some View {
        Group {
            HStack {
                Button(action: self.action!) {
                        if rightTitleString != CommonAllString.BlankStr {

                        Text(rightTitleString).font(.system(size: 18, weight: .bold, design: .default))
                            .foregroundColor(Color.init(comeFromPublicRide ==  true ? ColorConstantsName.HeaderForGrandColour : ColorConstantsName.AccentColour))
                            .padding(.trailing,10)
                        
                    }}.onTapGesture(perform: self.action!)
                
                
                    Image(isOpen ? "dropdownUpArrow" : "dropdownArrow").foregroundColor(.white)
                        .padding(.trailing, 5)
            }
        }
    }
}
