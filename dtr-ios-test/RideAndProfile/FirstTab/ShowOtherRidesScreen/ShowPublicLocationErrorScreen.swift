//
//  ShowPublicLocationErrorScreen.swift
//  dtr-ios-test
//
//  Created by apple on 14/12/21.
//

import SwiftUI

struct ShowPublicLocationErrorScreen: View {
    @Binding  var showPublicLocationChange : Bool
    
    var body: some View {
        Group{
            HStack {
                VStack(alignment: .leading) {
                    Text("Public rides near you won’t appear until you set a default location, click here to set one.").fixedSize(horizontal: false, vertical: true).font(.system(size: 15))
                    Spacer()
                }.padding(10).onTapGesture {
                    showPublicLocationChange = true
                }
                Spacer()
            }.background(Color.init(UIColor.init(red: 253/255, green: 8/255, blue: 7/255, alpha: 1.0))).padding(.horizontal,10)
            
        }
    }
}

