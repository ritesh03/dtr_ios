//
//  ShowFollowersRidesScreen.swift
//  dtr-ios-test
//
//  Created by apple on 14/12/21.
//

import SwiftUI

struct ShowFollowersRidesScreen: View {
    @Binding  var othersRidesToDisplay : [DtrRide]
    @Binding  var day : Int
    @Binding  var currentRideUserDay : DtrRide
    
    var body: some View {
        if self.othersRidesToDisplay.count > 0 {
            SectionTitleViewUI(titleString: ShowOtherRidesToString.FollowersRideStr,rightTitleString: CommonAllString.BlankStr, countShow:  0, comeFromPublicRide:false,changeFontSize:true) {}
            ForEach(self.othersRidesToDisplay.indices, id: \.self) { ride in
                let isOn = Binding(
                    get: {
                        ride < self.othersRidesToDisplay.count ? self.othersRidesToDisplay[ride] : DtrRide.default
                    },
                    set: {
                        let isIndexValid = self.othersRidesToDisplay.indices.contains(ride)
                        if isIndexValid {
                            self.othersRidesToDisplay[ride] = $0
                        }
                    }
                )
                NavigationLink(destination: RideDetail(day:$day, myRideForDay: isOn,presentModelOldHistoryScreen: .constant(false),myRideForDay1: RideEditViewModel.init(ride: isOn.wrappedValue))) {
                    OtherUserRideMiniViewScreen(day: $day,RideForDayDetail: RideEditViewModel.init(ride: isOn.wrappedValue), currentRideUserDay: $currentRideUserDay,isComesFromProfileScreen:false)
                }
            }
        }
    }
}
