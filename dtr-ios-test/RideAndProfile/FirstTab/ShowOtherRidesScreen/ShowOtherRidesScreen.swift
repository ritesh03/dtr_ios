//
//  ShowOtherRidesScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 15/03/21.
//

import SwiftUI

struct ShowOtherRidesScreen: View {
    @Binding  var allRidesToDisplay : [DtrRide]
    @Binding  var friendsRidesToDisplay : [DtrRide]
    @Binding  var othersRidesToDisplay : [DtrRide]
    @Binding  var publicRidesToDisplay : [DtrRide]
    @Binding  var isPublicRideLocAvaible : Bool
    @Binding  var currentRideUserDay : DtrRide
    
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    @Binding var day: Int
    @State var showPublicLocationChange = false
    
    @State private var startPlace = CommonAllString.BlankStr
    @State private var startCountry = CommonAllString.BlankStr
    @State var startPlaceLat = 0.0
    @State var startPlaceLon = 0.0
    
    @State var setUserLocationOnMap = false
    
    @Binding  var startPlaceForShow :String
    @Binding  var startCountryForShow :String
    @Binding  var startPlaceLatForShow :Double
    @Binding  var startPlaceLonForShow :Double
    
    let timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "hh:mm a"
        return formatter
    }()
    
    var body: some View {
        if (self.friendsRidesToDisplay.count + self.othersRidesToDisplay.count) != 0 {
        if self.friendsRidesToDisplay.count > 0 || self.othersRidesToDisplay.count > 0 {
            HStack{
                Text(ShowOtherRidesToString.OthersRideJoinStr).font(.system(size: 18, weight: .bold, design: .default))
                    .foregroundColor(.white)
                    .padding(.horizontal)
                Spacer()
            }.padding(5)
            .padding(.bottom,10)
        }
        
        if self.friendsRidesToDisplay.count > 0 {
            ShowFriendsRidesScreen(friendsRidesToDisplay: self.$friendsRidesToDisplay, day:$day, currentRideUserDay: $currentRideUserDay )
        }
        
        if self.othersRidesToDisplay.count > 0 {
            ShowFollowersRidesScreen(othersRidesToDisplay: self.$othersRidesToDisplay, day:$day, currentRideUserDay: $currentRideUserDay )
        }
        
    } else {
        ConnectWithRidersAndGroupsView()
    }
        
        SectionTitleViewUI(titleString: ShowOtherRidesToString.PublicRideStr,rightTitleString: dtrData.profile.userLon == 0.0 && dtrData.profile.userLon == 0.0 ? CommonAllString.BlankStr : "\(startPlaceForShow)", countShow:  0, comeFromPublicRide:dtrData.profile.userLon == 0.0 && dtrData.profile.userLon == 0.0 ? false : true,changeFontSize:true) {
            showPublicLocationChange = true
        }
        
        .onAppear{
            if dtrData.profile.userLon == 0.0 && dtrData.profile.userLon == 0.0 {
                setUserLocationOnMap = true
                isPublicRideLocAvaible = false
            }else{
                setUserLocationOnMap = false
            }
        }
        
        .fullScreenCover(isPresented: $showPublicLocationChange) {
            MapScreen(day: $day,fullLocationName:.constant(""),showPublicRideLocationPopUp:dtrData.profile.userLon == 0.0 && dtrData.profile.userLon == 0.0 ? false : true,isOnBoradingLat:$startPlaceLat,isOnBoradingLon:$startPlaceLon,canEditLocation:true,startCountry: $startCountry, startPlace: $startPlace, isOnBoradingProcess: .constant(true), isUpdateRideLocation: .constant(false), showRideLocationScreen: .constant(false), canSetPublicLocation:isPublicRideLocAvaible, myRideForDay: RideEditViewModel.init(ride: DtrRide.default)) {
                
                if dtrData.profile.userLon == 0.0 && dtrData.profile.userLon == 0.0 {
                   updateMesgWithTime()
                }else{
                    var resultCountry = startCountry
                    if startCountry.contains("Location TBD") {
                        resultCountry =   startCountry.replacingOccurrences(of: "Location TBD", with: "")
                    }
                    
                    let _ = UserStore.shared.saveDateCodeWithUserLocation(city: startPlace, country: resultCountry, lat: "\(startPlaceLat)", long: "\(startPlaceLon)", dateCode: dateCode(offset: self.day),isSetData:true)
                    
                    getLocalDataForPublicRide(dateCodeStr: dateCode(offset: self.day))
                }
            }
        }
        
        if dtrData.profile.userLon != 0.0 && dtrData.profile.userLon != 0.0 {
            ShowPublicRidesScreen(publicRidesToDisplay: self.$publicRidesToDisplay, day:$day, currentRideUserDay: $currentRideUserDay ).id(UUID())
        }else{
            ShowPublicLocationErrorScreen(showPublicLocationChange: $showPublicLocationChange)
        }
        
        if publicRidesToDisplay.count == 0{
            ExploreCommunityScreen()
        }
    }
    func getLocalDataForPublicRide(dateCodeStr:String) {
        let dict = UserStore.shared.saveDateCodeWithUserLocation(city: CommonAllString.BlankStr, country: CommonAllString.BlankStr, lat: CommonAllString.BlankStr, long: CommonAllString.BlankStr, dateCode: dateCodeStr,isSetData:false)
        
        if dict.0 != CommonAllString.BlankStr && dict.1 != CommonAllString.BlankStr && dict.2 != CommonAllString.BlankStr && dict.3 != CommonAllString.BlankStr {
            startPlaceForShow = dict.0
            startCountryForShow = dict.1
            startPlaceLatForShow = Double(dict.2) ?? 0.0
            startPlaceLonForShow = Double(dict.3) ?? 0.0
            isPublicRideLocAvaible = true
        } else {
            startPlaceForShow = dtrData.profile.cityState
            startCountryForShow = dtrData.profile.country
            startPlaceLatForShow = dtrData.profile.userLat
            startPlaceLonForShow = dtrData.profile.userLon
            isPublicRideLocAvaible = false
        }
        
        if dtrData.profile.userLon == 0.0 && dtrData.profile.userLon == 0.0 {
            isPublicRideLocAvaible = false
        }
        
        dtrCommand.getPublicRides(dateCode: dateCode(offset: day), currentlat: CGFloat(startPlaceLatForShow), currentlon: CGFloat(startPlaceLonForShow)){ response in
            if let responseRide = response.output["publicRides"] as? DataDict {
                if let pblRideAry = responseRide["hits"] as? NSArray {
                    var publicRideIds = [String]()
                    for i in 0..<pblRideAry.count {
                        let rideDict = asDataDict(pblRideAry[i])
                        let dataDict = ["id":rideDict["id"]!,"leader":rideDict["leader"]!,"summary":rideDict["summary"]!,"dateCode":rideDict["dateCode"]!,"details":["visibility":rideDict["visibility"]]] as [String:Any]
                        let rideDetail = DtrRide.init(data: dataDict)
                        if rideDetail.leader != dtrData.profile.id {
                            publicRideIds.append(rideDetail.id)
                        }
                    }
                    getPublicRidesById(ids: publicRideIds)
                }
            }
        }
    }
        
        func getPublicRidesById(ids:[String]) {
            for i in 0..<ids.count {
                getPublicRidesFromIds(publicRide: ids[i]) { tempPubcRide, allRidesArrys in
                    publicRidesToDisplay = tempPubcRide
                    allRidesToDisplay = allRidesArrys
                    getFriendAndFollowersRides()
                }
            }
            getFriendAndFollowersRides()
        }
        
        
        func getPublicRidesFromIds(publicRide:String, completion: @escaping ([DtrRide],[DtrRide]) -> Void){
            var tempPublicRideArray = publicRidesToDisplay
            var tempAllRidesArray = allRidesToDisplay
            dtrData.getRideDetailForSpecificRide(rideID:publicRide) { rideDetail in
                if rideDetail != nil {
                    // Check ride if allrides Contain the public ride
                    if !tempAllRidesArray.contains(rideDetail!) {
                        if !rideDetail!.riders.contains(dtrData.profile.id) {
                            if dtrData.profile.friends.contains(rideDetail!.leader) || dtrData.profile.followed.contains(rideDetail!.leader) {
                                tempAllRidesArray.append(rideDetail!)
                            } else {
                                if let rideIndex = tempPublicRideArray.firstIndex(where: { $0.id == rideDetail!.id }) {
                                    tempPublicRideArray.remove(at: rideIndex)
                                    tempPublicRideArray.append(rideDetail!)
                                }else{
                                    tempPublicRideArray.append(rideDetail!)
                                }
                            }
                        }else{
                            if rideDetail!.riders.contains(dtrData.profile.id) {
                                if let rideIndex = tempPublicRideArray.firstIndex(where: { $0.id == rideDetail!.id }) {
                                    tempPublicRideArray.remove(at: rideIndex)
                                }
                            }
                        }
                    }else{
                        //If public ride change the relationship in current session, then we use same function for updating the ride detail's, otherwise suggested ride updating automatically.
                        if tempAllRidesArray.count > 0 {
                            if let rideIndex = tempAllRidesArray.firstIndex(where: { $0.id == rideDetail!.id }) {
                                tempAllRidesArray.remove(at: rideIndex)
                                tempAllRidesArray.append(rideDetail!)
                            }
                        }
                        
                        if let rideIndex = tempPublicRideArray.firstIndex(where: { $0.id == rideDetail!.id }) {
                            tempPublicRideArray.remove(at: rideIndex)
                        }
                    }
                    completion(tempPublicRideArray,tempAllRidesArray)
                }
            }
        }
        
        
        
        
        func getFriendAndFollowersRides() {
            friendsRidesToDisplay.removeAll()
            othersRidesToDisplay.removeAll()
            
            var tempAllRidesArray = allRidesToDisplay
            
            var tempfriendsRidesToDisplay = friendsRidesToDisplay
            var tempfollowerAndGroupRidesToDisplay = othersRidesToDisplay
            
            var ridesSuggestedForDate = Set<DtrRide>()
            if let todayRides = dtrData.allUserRidesForWeek[dateCode(offset: day)] {
                ridesSuggestedForDate = todayRides
            }
            
            if tempAllRidesArray.count > 0 {
                for i in 0..<tempAllRidesArray.count {
                    if tempAllRidesArray.indices.contains(i) {
                        if (tempAllRidesArray[i].leader != dtrData.profile.id && !tempAllRidesArray[i].riders.contains(dtrData.profile.id)) {
                            if dtrData.profile.friends.contains(tempAllRidesArray[i].leader) {
                                if !tempAllRidesArray[i].riders.contains(dtrData.profile.id) {
                                    if let rideIndex = tempfriendsRidesToDisplay.firstIndex(where: { $0.id == tempAllRidesArray[i].id }) {
                                        tempfriendsRidesToDisplay.remove(at: rideIndex)
                                        tempfriendsRidesToDisplay.append(tempAllRidesArray[i])
                                    }else{
                                        if ridesSuggestedForDate.contains(tempAllRidesArray[i]){
                                            tempfriendsRidesToDisplay.append(tempAllRidesArray[i])
                                        } else {
                                            if let rideIndex = tempAllRidesArray.firstIndex(where: { $0.id == tempAllRidesArray[i].id }) {
                                                tempAllRidesArray.remove(at: rideIndex)
                                            }
                                        }
                                    }
                                }
                            } else {
                                if !tempAllRidesArray[i].riders.contains(dtrData.profile.id) {
                                    if let rideIndex = tempfollowerAndGroupRidesToDisplay.firstIndex(where: { $0.id == tempAllRidesArray[i].id }) {
                                        tempfollowerAndGroupRidesToDisplay.remove(at: rideIndex)
                                        tempfollowerAndGroupRidesToDisplay.append(tempAllRidesArray[i])
                                    }else{
                                        if ridesSuggestedForDate.contains(tempAllRidesArray[i]){
                                            tempfollowerAndGroupRidesToDisplay.append(tempAllRidesArray[i])
                                        }else{
                                            if let rideIndex = tempAllRidesArray.firstIndex(where: { $0.id == tempAllRidesArray[i].id }) {
                                                tempAllRidesArray.remove(at: rideIndex)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                if tempfriendsRidesToDisplay.count > 0 {
                    tempfriendsRidesToDisplay = tempfriendsRidesToDisplay.sorted(by: {
                        timeFormatter.date(from: $0.details.startTime) ?? Date.distantFuture <  timeFormatter.date(from: $1.details.startTime) ?? Date.distantFuture
                    })
                }
                
                if tempfollowerAndGroupRidesToDisplay.count > 0 {
                    tempfollowerAndGroupRidesToDisplay = tempfollowerAndGroupRidesToDisplay.sorted(by: {
                        timeFormatter.date(from: $0.details.startTime) ?? Date.distantFuture <  timeFormatter.date(from: $1.details.startTime) ?? Date.distantFuture
                    })
                }
                
                friendsRidesToDisplay =  tempfriendsRidesToDisplay
                othersRidesToDisplay =  tempfollowerAndGroupRidesToDisplay
                allRidesToDisplay =  tempAllRidesArray
                
                //print("friendsRidesToDisplay = ",friendsRidesToDisplay.count,tempfriendsRidesToDisplay.count,"\n","followerAndGroupRidesToDisplay = ",othersRidesToDisplay.count,tempfollowerAndGroupRidesToDisplay.count,"\n","tempAllRidesArray = ",allRidesToDisplay.count,tempAllRidesArray.count)
            }
            
            if publicRidesToDisplay.count > 0 {
                publicRidesToDisplay = publicRidesToDisplay.sorted(by: {
                    timeFormatter.date(from: $0.details.startTime) ?? Date.distantFuture <  timeFormatter.date(from: $1.details.startTime) ?? Date.distantFuture
                })
            }
        }
    
    func updateMesgWithTime() {
        let updatedProfile =  dtrData.profile
        let dict = ["id":updatedProfile.id,"name" : dtrData.profile.name,"activated":updatedProfile.activated.timeIntervalSince1970,"country": startCountry, "cityState" : startPlace,"friends":updatedProfile.friends,"pushToken":updatedProfile.pushToken ?? "","verificationDeclined":updatedProfile.verificationDeclined,"defaultStatus":updatedProfile.defaultStatus,"linkCode":updatedProfile.linkCode,"description"  : updatedProfile.description,"tags":updatedProfile.tags,"platform":updatedProfile.platform, "phone": updatedProfile.phone ?? CommonAllString.BlankStr,"email": dtrData.profile.email ?? CommonAllString.BlankStr,"pictureHash":updatedProfile.pictureHash,"userLat":startPlaceLat,"userLon":startPlaceLon] as [String : Any]
        dtrCommand.profileUpdate(profileID: dtrData.profile.id, data: dict) { (result) in
            result.debug(methodName:"profileUpdate")
        }
    }
}
