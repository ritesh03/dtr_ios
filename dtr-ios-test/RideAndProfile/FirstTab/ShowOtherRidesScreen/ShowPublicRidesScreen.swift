//
//  ShowPublicRidesScreen.swift
//  dtr-ios-test
//
//  Created by apple on 14/12/21.
//

import SwiftUI

struct ShowPublicRidesScreen: View {
    @Binding  var publicRidesToDisplay : [DtrRide]
    @Binding  var day : Int
    @Binding  var currentRideUserDay : DtrRide
    
    var body: some View {
        ForEach(self.publicRidesToDisplay.indices, id: \.self) { ride in
            let isOn = Binding(
                get: {
                    ride < self.publicRidesToDisplay.count ? self.publicRidesToDisplay[ride] : DtrRide.default
                },
                set: {
                    let isIndexValid = self.publicRidesToDisplay.indices.contains(ride)
                    if isIndexValid {
                        self.publicRidesToDisplay[ride] = $0
                    }
                }
            )
            NavigationLink(destination: RideDetail(day:$day, myRideForDay: isOn,presentModelOldHistoryScreen: .constant(false),myRideForDay1: RideEditViewModel.init(ride: isOn.wrappedValue))) {
                OtherUserRideMiniViewScreen(day: $day,RideForDayDetail: RideEditViewModel.init(ride: isOn.wrappedValue), currentRideUserDay: $currentRideUserDay,isComesFromProfileScreen:false)
            }
        }
    }
}
