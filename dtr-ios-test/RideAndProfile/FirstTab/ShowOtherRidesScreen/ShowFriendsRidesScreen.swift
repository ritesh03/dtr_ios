//
//  ShowFriendsRidesScreen.swift
//  dtr-ios-test
//
//  Created by apple on 14/12/21.
//

import SwiftUI

struct ShowFriendsRidesScreen: View {
    @Binding  var friendsRidesToDisplay : [DtrRide]
    @Binding  var day : Int
    @Binding  var currentRideUserDay : DtrRide
    
    var body: some View {
        if self.friendsRidesToDisplay.count > 0 {
            SectionTitleViewUI(titleString: ShowOtherRidesToString.FriendRideStr, rightTitleString: CommonAllString.BlankStr, countShow:  0,changeFontSize:true) {}
            ForEach(self.friendsRidesToDisplay.indices, id: \.self) { ride in
                let isOn = Binding(
                    get: {
                        ride < self.friendsRidesToDisplay.count ? self.friendsRidesToDisplay[ride] : DtrRide.default
                    },
                    set: {
                        let isIndexValid = self.friendsRidesToDisplay.indices.contains(ride)
                        if isIndexValid {
                            self.friendsRidesToDisplay[ride] = $0
                        }
                    }
                )
                NavigationLink(destination: RideDetail(day:$day, myRideForDay: isOn,presentModelOldHistoryScreen: .constant(false),myRideForDay1: RideEditViewModel.init(ride: isOn.wrappedValue))) {
                    OtherUserRideMiniViewScreen(day: $day, RideForDayDetail: RideEditViewModel.init(ride: isOn.wrappedValue), currentRideUserDay: $currentRideUserDay,isComesFromProfileScreen:false)
                }
            }
        }
    }
}
