//
//  OtherUserRideMiniViewScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/01/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI


struct OtherUserRideMiniViewScreen: View {
    
    @Binding var day : Int
    @ObservedObject var RideForDayDetail : RideEditViewModel
    @Binding  var currentRideUserDay : DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    @State var isComesFromProfileScreen = false
    
    var body: some View {
        VStack(spacing:0){
            VStack(alignment:.leading) {
                HStack(alignment:.top) {
                    VStack (alignment: .leading){
                        Text(RideForDayDetail.rideEdit.summary).font(.system(size: 16, weight: .heavy, design: .default))
                            .foregroundColor(.white)
                            .fixedSize(horizontal: false, vertical: true)
                            .multilineTextAlignment(.leading)
                    }
                    Spacer()
                    if RideForDayDetail.rideEdit.rideCoverImage != "" {
                        WebImage(url:URL(string: RideForDayDetail.rideEdit.rideCoverImage), options: [.progressiveLoad], isAnimating: .constant(true))
                            .purgeable(true)
                            .placeholder(Image(uiImage: UIImage(named: ImageConstantsName.MainRideEditCoverImg)!))
                            .renderingMode(.original)
                            .resizable()
                            .frame(width: 65 , height: 65, alignment: .center)
                            .clipped()
                            .cornerRadius(15)
                    }
                }
                
                RidersHorizontalImages(ride: $RideForDayDetail.rideEdit, showLeader: true).padding(.top,RideForDayDetail.rideEdit.rideCoverImage == "" ? 10 : -20)
                
                HStack {
                    
                    Group {
                        Text(dtrData.profileInfo(profileID: RideForDayDetail.rideEdit.leader).name).font(.system(size: 15, weight: .bold))
                            .foregroundColor(.gray)
                            .fixedSize(horizontal: false, vertical: true)
                    }
                    
                    if RideForDayDetail.rideEdit.riders.count > 1 {
                        Circle()
                            .frame(width: 5, height: 5)
                            .foregroundColor(Color.gray)
                        Text("\(RideForDayDetail.rideEdit.riders.count) riders").font(.system(size: 15))
                            .foregroundColor(Color.gray)
                    }
                }
                
                HStack {
                    if isComesFromProfileScreen {
                        Group {
                            Text((monthAndDateName(dateCode: RideForDayDetail.rideEdit.dateCode)))
                        }
                        
                        Circle()
                            .frame(width: 5, height: 5)
                            .foregroundColor(Color.gray)
                    }
                    
                    Group {
                        if RideForDayDetail.rideEdit.details.startTime != "" {
                            Text(RideForDayDetail.rideEdit.details.startTime)
                        }else{
                            Text("Time TBD")
                        }
                    }
                    
                    Circle()
                        .frame(width: 5, height: 5)
                        .foregroundColor(Color.gray)
                    
                    Group {
                        if RideForDayDetail.rideEdit.details.startPlace != "" {
                            Text(RideForDayDetail.rideEdit.details.startPlace).fixedSize(horizontal: false, vertical: true).multilineTextAlignment(.leading)
                        }else{
                            Text("Location TBD")
                        }
                    }.foregroundColor(.white)
                    Spacer()
                    if !isComesFromProfileScreen {
                        if RideForDayDetail.rideEdit.riders.count == 1 {
                            let statusInvited = getAlreadySentInvite(userId: RideForDayDetail.rideEdit.leader)
                            CustomButtonCircleWithCustomProperty(btnTitle: statusInvited, btnWidth: 80, btnHeight: 10, backgroundColord: statusInvited == "Invite" ? true : false, txtColorAccent: false, strokeColor: statusInvited == "Invite" ? false : true, textBold: true, lineWidth: false, disabled: statusInvited == "Invite" ? false : true) {
                                let mesg = "😎 \(dtrData.profile.name) has invited you to \(currentRideUserDay.summary) on \(dayName(dateCode: currentRideUserDay.dateCode)) "
                                dtrCommand.sendRideInvitation(profileID: currentRideUserDay.id, data: ["message":mesg,"recipients":[RideForDayDetail.rideEdit.leader]]) { (result) in
                                    if result.result == .ok {
                                        //UserStore.shared.rideInviteForFriendViewIds.append(myRideForDay1.rideEdit.leader)
                                    }
                                }
                            }.padding(.bottom).buttonStyle(PlainButtonStyle()).disabled(statusInvited == "Invite" ? false : true)
                        }
                    }
                }
                
                RideSkillIndicators(ride: $RideForDayDetail.rideEdit).padding(.top,10)
                
                HStack {
                    if RideForDayDetail.rideEdit.details.visibility.rawValue == "public" {
                        Text("Public Ride").font(.system(size: 14, weight: .bold, design: .default))
                            .foregroundColor(Color.init(ColorConstantsName.LimeColour))
                    }
                }.padding(.top,10)
                Divider()
                    .background(Color.white)
                    .offset(x: 0, y:8)
            }
            .padding(10)
            .padding(.horizontal,10)
        }.background(returnBackroundView).clipped()
    }
    
    
    func getAlreadySentInvite(userId:String) -> String {
        var status = "Invite"
        let filteredSentRideInvite = self.dtrData.sentRideInviteNotifications.filter { $0.payloadID == "\(currentRideUserDay.leader)+\(dateCode(offset: self.day))"}
        if filteredSentRideInvite.count > 0 {
            let userSentRideInvite = filteredSentRideInvite.filter { $0.target == userId}
            if userSentRideInvite.count > 0 {
                status = "Invited"
            }
        }
        return status
    }

    
    var returnBackroundView: some View {
        Group {
            if RideForDayDetail.rideEdit.details.tags.contains("KfqH2PdeUY4nyp9V2nPQ") {
                Image(ImageConstantsName.IceTagBagImg)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .opacity(0.1)
            } else {
                Color.init(ColorConstantsName.MainThemeBgColour)
            }
        }
    }
}
