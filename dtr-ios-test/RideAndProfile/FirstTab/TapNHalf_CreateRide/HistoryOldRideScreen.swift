//
//  HistoryOldRideScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 22/01/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct HistoryOldRideScreen: View {
    
    @ObservedObject var dtrData = DtrData.sharedInstance
    @Binding  var oldRidesToDisplay : [DtrRide]
    @Binding  var selectedIndex : Int
    @Binding var myRideForDay : DtrRide
    
    var body: some View {
        VStack {
            Text("Or copy details from previous days - \n all ride info also copies over 🤙")
                .fixedSize(horizontal: false, vertical: true)
                .foregroundColor(.white)
                .multilineTextAlignment(.center)
                .padding()
            Divider()
                .background(Color.white)
        }
        ScrollView {
            VStack {
                ForEach(oldRidesToDisplay.indices, id: \.self) { ride in
                    HStack(alignment:.top) {
                        VStack {
                            Group {
                                if  oldRidesToDisplay[ride].rideCoverImage != "" {
                                    WebImage(url:URL(string:oldRidesToDisplay[ride].rideCoverImage), isAnimating: .constant(true))
                                        .purgeable(true)
                                        .placeholder(Image(uiImage: UIImage(named: ImageConstantsName.MainCoverBgImg)!))
                                        .renderingMode(.original)
                                        .resizable()
                                        .indicator(.activity)
                                        .scaledToFill()
                                        .frame(width:70,height: 70, alignment: .center)
                                        .clipped()
                                        .cornerRadius(10)
                                }else{
                                    Image(ImageConstantsName.MainCoverBgImg)
                                        .resizable()
                                        .scaledToFill()
                                        .frame(width:70,height: 70, alignment: .center)
                                        .clipped()
                                        .cornerRadius(10)
                                }
                                Group {
                                    Text((dateMonthYearRetrun(dateCode: oldRidesToDisplay[ride].dateCode))).font(.system(size: 14)).padding(.top,7)
                                        .foregroundColor(.gray)
                                }
                            }
                        }
                        
                        VStack(alignment: .leading, spacing: 13){
                            Text(oldRidesToDisplay[ride].summary).font(.system(size: 16, weight: .bold))
                                .foregroundColor(.white)
                                .lineLimit(2)
                            
                            Group {
                                HStack {
                                    Group {
                                        if oldRidesToDisplay[ride].details.hasStartTime  {
                                            Text(oldRidesToDisplay[ride].details.startTime)
                                        }else{
                                            Text("Time TBD")
                                        }
                                    }.foregroundColor(.white)
                                    .font(.system(size: 14, weight: .regular))
                                    Circle()
                                        .frame(width: 5, height: 5)
                                        .foregroundColor(Color.gray)
                                    Group {
                                        if  oldRidesToDisplay[ride].details.hasStartPlace {
                                            Text(oldRidesToDisplay[ride].details.startPlace)
                                        }else{
                                            Text("Location TBD")
                                        }
                                    }.foregroundColor(.white)
                                    .font(.system(size: 14, weight: .regular))
                                }
                            }
                            
                            Group {
                                RideSkillIndicators(ride: $oldRidesToDisplay[ride])
                            }
                            Spacer()
                        }
                        .padding(.leading,13)
                        Spacer()
                            .background(Color.red)
                        VStack{
                            Group {
                                Image(selectedIndex == ride ? "DTR-Vector" : "DTR-EllipseRound")
                                    .resizable()
                                    .frame(width: 20, height: 20,alignment: .top)
                                    .cornerRadius(10)
                                Spacer()
                            }
                        }
                    }.padding()
                    .background(selectedIndex == ride ? Color.clear : Color.init(ColorConstantsName.HeaderBgColour))
                    .onTapGesture {
                        if self.selectedIndex == ride {
                            self.selectedIndex = -1
                            return;
                        }
                        self.selectedIndex = ride
                        myRideForDay = oldRidesToDisplay[self.selectedIndex]
                    }
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(selectedIndex == ride ? Color.init(ColorConstantsName.AccentColour) : Color.clear, lineWidth: 1)
                    )
                }.cornerRadius(10)
            }
        }
    }
}

struct OHistoryOldRideScreen_Previews: PreviewProvider {
    static var previews: some View {
        HistoryOldRideScreen(oldRidesToDisplay: .constant([DtrRide.default]), selectedIndex: .constant(0), myRideForDay: .constant(DtrRide.default))
    }
}
