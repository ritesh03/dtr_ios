//
//  CreateRideScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 20/01/21.
//

import SwiftUI
import Firebase

struct CreateRideScreen: View {
    
    @Binding var day : Int
    @Binding var presentedAsModal: Bool
    @State var isComeFromLeaveRide: Bool
    @State var afterLeaveRideDefaultRide: DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    @State private var showActivityIndicator = false
    @State private var selectedIndex : Int = -1
    @State private var oldRidesToDisplay = [DtrRide]()
    @State private var titleDtr: String = ""
    @State private var myRideForDay: DtrRide = DtrRide.default
    @State private var defaultRideInfo: DtrRide = DtrRide.default
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack(alignment: .center, spacing:15){
                HStack() {
                    Button(action: {
                        if isComeFromLeaveRide {
                            notDtrCurrentRideIfComesFromLeavingScreen(isComesFromBackBtn: true) { (returnReponse) in}
                        }
                        self.presentedAsModal = false
                    }) {
                        Image(systemName:ImageConstantsName.XmarkImg)
                            .resizable()
                            .frame(width: 10, height: 10)
                            .foregroundColor(.white)
                    }
                    Spacer()
                    Button(action: {
                        showActivityIndicator = true
                      
                        Analytics.logEvent(AnalyticsObjectEvent.down_to_ride_update.rawValue, parameters: nil)
                        callEditAndCreateRide()
                    }) {
                        if self.showActivityIndicator {
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                .foregroundColor(.white)
                                .padding()
                          
                        }
                        Text("Update")
                            .font(.system(size: 20, weight: .bold, design: .default))
                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                    }.accessibility(identifier: "Update_Create_Ride").disabled((titleDtr.count == 0 ? selectedIndex == -1 : false))
                }.disabled(showActivityIndicator == true ? true : false)
                
                ScrollView{
                    Text("Sweet, you’re DTR now!").font(.system(size: 24, weight: .heavy, design: .default))
                        .foregroundColor(.white)
                        .padding(.bottom)
                    Text("Select a custom status or ride title to let \n your friends know what you're interested \n in.").font(.system(size: 17, weight: .medium, design: .default))
                        .foregroundColor(.white)
                        .multilineTextAlignment(.center)
                    Text("For example, \" looking to ride after work \"").font(.system(size: 17, weight: .medium, design: .default))
                        .foregroundColor(.white)
                    
                    HStack {
                        TextField(" Down to Ride", text: self.$titleDtr)
                        Button(action: {
                            titleDtr = ""
                        }) {
                            Image(systemName: ImageConstantsName.XmarkImg)
                                .aspectRatio(contentMode: .fit)
                        }
                    }
                    .padding()
                    .background(Color.init(ColorConstantsName.SectionBgColour))
                    .cornerRadius(10)
                    .foregroundColor(.white)
                    .padding(.all)
                    .frame(height: 50)
                    if oldRidesToDisplay.count > 0 {
                        HistoryOldRideScreen(oldRidesToDisplay: $oldRidesToDisplay, selectedIndex: $selectedIndex, myRideForDay:$myRideForDay)
                    } else {
                        EmptyView()
                        Spacer()
                    }
                }
            }
            .onTapGesture {
                UIApplication.shared.endEditing()
            }
            .onAppear{
                dtrData.oldHistoryRides { (rides) in
                    oldRidesToDisplay = rides
                }
                
                let rideDetail = self.dtrData.myRidesByDate[dateCode(offset: day)]
                if rideDetail != nil{
                    afterLeaveRideDefaultRide = rideDetail!
                }
            }
            .padding()
        }
    }
    
    func notDtrCurrentRideIfComesFromLeavingScreen(isComesFromBackBtn:Bool,completion: @escaping (_ keys: Bool) -> Void) {
        self.dtrCommand.rideCancel(rideID: self.afterLeaveRideDefaultRide.id,dateCode:afterLeaveRideDefaultRide.dateCode, message: "") { result in
            if result.result == .ok {
                if isComesFromBackBtn {
                    self.presentedAsModal = false
                }
                completion(true)
            }
        }
    }
    
    func createSimpleRideWithVisibliCheck() {
        var dictCreate = [String:Any]()
        if dtrData.profile.defaultRideVisibility != "" {
            dictCreate = ["_geoloc":["lat":defaultRideInfo.details.startPlaceLat,"lng":defaultRideInfo.details.startPlaceLon],"summary": titleDtr,"rideCoverImage":defaultRideInfo.rideCoverImage,"details":["tags":defaultRideInfo.details.tags,"hasStartPlace":defaultRideInfo.details.hasStartPlace,"hasStartTime":defaultRideInfo.details.hasStartTime,"notes":defaultRideInfo.details.notes,"skillA":defaultRideInfo.details.skillA,"skillB":defaultRideInfo.details.skillB,"skillC":defaultRideInfo.details.skillC,"skillD":defaultRideInfo.details.skillD,"skillS":defaultRideInfo.details.skillS,"startPlace":defaultRideInfo.details.startPlace,"startPlaceLat":defaultRideInfo.details.startPlaceLat,"startPlaceLon":defaultRideInfo.details.startPlaceLon,"startTime":defaultRideInfo.details.startTime,"visibility":dtrData.profile.defaultRideVisibility]] as [String : Any]
        } else {
            dictCreate = ["_geoloc":["lat":defaultRideInfo.details.startPlaceLat,"lng":defaultRideInfo.details.startPlaceLon],"summary": titleDtr,"rideCoverImage":defaultRideInfo.rideCoverImage,"details":["tags":defaultRideInfo.details.tags,"hasStartPlace":defaultRideInfo.details.hasStartPlace,"hasStartTime":defaultRideInfo.details.hasStartTime,"notes":defaultRideInfo.details.notes,"skillA":defaultRideInfo.details.skillA,"skillB":defaultRideInfo.details.skillB,"skillC":defaultRideInfo.details.skillC,"skillD":defaultRideInfo.details.skillD,"skillS":defaultRideInfo.details.skillS,"startPlace":defaultRideInfo.details.startPlace,"startPlaceLat":defaultRideInfo.details.startPlaceLat,"startPlaceLon":defaultRideInfo.details.startPlaceLon,"startTime":defaultRideInfo.details.startTime,"visibility":"followers"]] as [String : Any]
        }
        
        self.dtrCommand.rideCreate(dateCode: dateCode(offset: day), data: dictCreate){ result in
            result.debug(methodName:"rideCreate")
            showActivityIndicator = false
            self.presentedAsModal = false
        }
    }
    
    func createOldHistoryRide() {
        self.dtrCommand.rideCreate(dateCode: dateCode(offset: day), data: ["_geoloc":["lat":myRideForDay.details.startPlaceLat,"lng":myRideForDay.details.startPlaceLon],"summary": myRideForDay.summary,"rideCoverImage":myRideForDay.rideCoverImage,"details":["tags":myRideForDay.details.tags,"hasStartPlace":myRideForDay.details.hasStartPlace,"hasStartTime":myRideForDay.details.hasStartTime,"notes":myRideForDay.details.notes,"skillA":myRideForDay.details.skillA,"skillB":myRideForDay.details.skillB,"skillC":myRideForDay.details.skillC,"skillD":myRideForDay.details.skillD,"skillS":myRideForDay.details.skillS,"startPlace":myRideForDay.details.startPlace,"startPlaceLat":myRideForDay.details.startPlaceLat,"startPlaceLon":myRideForDay.details.startPlaceLon,"startTime":myRideForDay.details.startTime,"visibility":myRideForDay.details.visibility.rawValue]]) { result in
            result.debug(methodName:"rideCreate")
            showActivityIndicator = false
            self.presentedAsModal = false
        }
    }
    
    func callEditAndCreateRide() {
        if selectedIndex == -1  {
            if isComeFromLeaveRide {
                notDtrCurrentRideIfComesFromLeavingScreen(isComesFromBackBtn: false) { (returnRespne) in
                    if  returnRespne {
                        createSimpleRideWithVisibliCheck()
                    }
                }
            }else{
                createSimpleRideWithVisibliCheck()
            }
        } else {
            if isComeFromLeaveRide {
                notDtrCurrentRideIfComesFromLeavingScreen(isComesFromBackBtn: false) { (returnRespne) in
                    if  returnRespne {
                        createOldHistoryRide()
                    }
                }
            }else{
                createOldHistoryRide()
            }
        }
    }
}

struct CreateRideScreen_Previews: PreviewProvider {
    static var previews: some View {
        CreateRideScreen(day: .constant(0), presentedAsModal: .constant(false), isComeFromLeaveRide: false, afterLeaveRideDefaultRide: DtrRide.default)
    }
}

