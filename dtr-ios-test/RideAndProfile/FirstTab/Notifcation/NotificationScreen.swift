//
//  NotificationsScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 02/03/21.
//

import SwiftUI
import Firebase

struct NotificationsScreen: View {
    
    @Binding var updatedDismissView:Bool
    @Binding var day:Int
    @Environment(\.presentationMode) var presentationMode
    @State var notifications: [AppNotification] = []
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var rideInviteArray :  [AppNotification] = []
    @State private var anoucmentArray :  [AppNotification] = []
    @State private var friendOtherMessageArray :  [AppNotification] = []
    @State private var friendInviteArray :  [AppNotification] = []
    @State private var rideAllOtherUpdateArray :  [AppNotification] = []
    @State private var groupUpdateArray :  [AppNotification] = []
    @State private var showActivityIndicator = false
    @State private var pressBackButton = false
    @State private var forwardButton = false
    @State var comesFromSheet = false
    @State var updateFriendRequestStatus = false
    @State var updateGroupRequestStatus = false
    @State var selectMaybe =  [String]()
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.init(ColorConstantsName.MainThemeBgColour)
                    .ignoresSafeArea()
                VStack(spacing:0) {
                    List {
                        if anoucmentArray.count > 0 {
                            AnnouncementScreen(showActivityIndicator:$showActivityIndicator,anoucmentArray:$anoucmentArray).listRowInsets(EdgeInsets())
                        }
                        
                        if friendInviteArray.count > 0 {
                            FriendInviteNotificationScreen(comesFromSheetFriendInvite :$comesFromSheet,updateFriendRequestStatus:$updateFriendRequestStatus,showActivityIndicator:$showActivityIndicator,friendInviteArray:$friendInviteArray).listRowInsets(EdgeInsets())
                        }
                       
                        if groupUpdateArray.count > 0 {
                            GroupSuggestionInviteView(comesFromSheetFriendInvite :$comesFromSheet,updateFriendRequestStatus:$updateFriendRequestStatus,showActivityIndicator:$showActivityIndicator,gorupInviteArray:$groupUpdateArray).listRowInsets(EdgeInsets())
                        }
                        
                        if rideAllOtherUpdateArray.count > 0 || rideInviteArray.count > 0 {
                            RideUpdateNotificationScreen(comesFromSheet:comesFromSheet,day:$day, showActivityIndicator: $showActivityIndicator, rideAllOtherUpdates: $rideAllOtherUpdateArray, rideInviteArray: $rideInviteArray, selectMaybe: $selectMaybe).listRowInsets(EdgeInsets())
                        }
                        
                        if friendOtherMessageArray.count > 0 {
                            FriendMessageNotificationScreen(day:$day,showActivityIndicator:$showActivityIndicator, friendOtherMessageArray: $friendOtherMessageArray).listRowInsets(EdgeInsets())
                        }
                        
                    }.listStyle(PlainListStyle()).background(Color.clear)
                        .listRowBackground(Color.clear)
                        .padding(.top,10)
                }
            }
            .onDisappear{
                updatedDismissView = true
            }
            .onChange(of: updateFriendRequestStatus, perform: { responseStatus in
                //this change because if we call direct method then the notification screen showing multiple time, so we can dismiss first and after call the method
                if updateFriendRequestStatus {
                    presentationMode.wrappedValue.dismiss()
                    updateFriendRequestStatus = false
                }
            })
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true,isSystemImage: true, isText:true, textTitle:"Notifications", systemImageName: ImageConstantsName.ArrowLeftImg, action: {
                pressBackButton = true
                getAllTypesOfNotifcation()
                presentationMode.wrappedValue.dismiss()
            })),trailing:AnyView(trailingButton))
        }
        
        .onAppear {
            if UserStore.shared.notificationMayBeDisabledData.count > 0 {
                self.selectMaybe = UserStore.shared.notificationMayBeDisabledData
            }
            Analytics.logEvent(AnalyticsScreen.notification.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
        }
        
        .onReceive(self.dtrData.$notifications) { getLatestNotification in
            DispatchQueue.main.async {
                notifications.removeAll()
                for index in 0..<getLatestNotification.count {
                    let notificationData = getLatestNotification[index]
                    notifications.append(notificationData)
                }
                
                if notifications.count == 0 && forwardButton {
                    presentationMode.wrappedValue.dismiss()
                }
                
                forwardButton = true
                DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                    getAllTypesOfNotifcation()
                }
            }
        }
    }
    
    var trailingButton: some View {
        Button(action: {
        }) {
            if self.showActivityIndicator {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                    .foregroundColor(.white)
                    .padding()
            }
        }
    }
    
    func getAllTypesOfNotifcation(){
        rideInviteArray.removeAll()
        rideAllOtherUpdateArray.removeAll()
        friendInviteArray.removeAll()
        anoucmentArray.removeAll()
        friendOtherMessageArray.removeAll()
        groupUpdateArray.removeAll()
        
        var temprideInviteArray = [AppNotification]()
        var temprideUpdateArray = [AppNotification]()
        //var temprideGroupMessageArray = [AppNotification]()
        var tempanoucmentArray = [AppNotification]()
        var tempfriendInviteArray = [AppNotification]()
        var tempfriendOtherMessageArray = [AppNotification]()
        var tempgroupUpdateArray = [AppNotification]()
        
        for index in 0..<notifications.count {
            if (notifications[index].type == .friendMessage || notifications[index].type == .message || notifications[index].type == .follow) && !(notifications[index].message.contains("join the group")) {
                if notifications[index].action != .groupLeft && notifications[index].action != .groupLeaderMessage && !notifications[index].message.contains(CommonAllString.GroupJoinMesgStr) {
                    tempfriendOtherMessageArray.append(notifications[index])
                }
            }
            
            if notifications[index].type == .friendInvite {
                tempfriendInviteArray.append(notifications[index])
            }
            
            if notifications[index].type == .announcement {
                tempanoucmentArray.append(notifications[index])
            }
            
            if notifications[index].type == .rideInvite {
                temprideInviteArray.append(notifications[index])
            }
            
            if notifications[index].type == .groupUpdate || notifications[index].message.contains("join the group") || notifications[index].action == .groupLeft || notifications[index].action == .groupLeaderMessage || notifications[index].message.contains(CommonAllString.GroupJoinMesgStr){
                tempgroupUpdateArray.append(notifications[index])
            }
            
            if notifications[index].type == .rideUpdate || notifications[index].type == .rideLocation || notifications[index].type == .rideTime || notifications[index].type == .rideCancelled {
                temprideUpdateArray.append(notifications[index])
            }
            
            if pressBackButton {
                if notifications[index].status == .unread {
                    if notifications[index].id != "" {
                        self.dtrData.updateNotificationStatus(notification:notifications[index], isTypeIndex: 0)
                    }
                }
            }
        }
        
        if tempfriendOtherMessageArray.count > 0 {
            friendOtherMessageArray = tempfriendOtherMessageArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
        
        if tempfriendInviteArray.count > 0 {
            friendInviteArray = tempfriendInviteArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
        
        if  tempanoucmentArray.count > 0 {
            anoucmentArray = tempanoucmentArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
        
        if  temprideInviteArray.count > 0 {
            rideInviteArray = temprideInviteArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
        
        if  temprideUpdateArray.count > 0 {
            rideAllOtherUpdateArray = temprideUpdateArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
        
        if tempgroupUpdateArray.count > 0 {
            groupUpdateArray = tempgroupUpdateArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
        
        pressBackButton = false
    }
}

struct NotificationsScreen_Previews: PreviewProvider {
    static var previews: some View {
        NotificationsScreen(updatedDismissView: .constant(false), day: .constant(0))
    }
}

