//
//  AnnouncementNotificationView.swift
//  dtr-ios-test
//
//  Created by Mobile on 11/03/21.
//

import SwiftUI

struct AnnouncementNotificationView: View {
    var notificationInfo :AppNotification
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var showAnoucmentScreen = false
    @State private var showTextEditOnly = false
    
    var body: some View {
        VStack {
            HStack(alignment:.top) {
                NavigationLink(destination: AnnouncementDetailView(announcementID: notificationInfo.payloadID,showOnlyTextEdit:showTextEditOnly, notificationInfo: notificationInfo)){
                    ProfileHead(profile: dtrData.profileInfo(profileID: notificationInfo.source), size: .medium,comeFromAnoucmentScreen:true)
                    VStack(alignment: .leading, spacing: 10){
                        HStack(alignment:.top){
                            Text("\(notificationInfo.message)")
                                .foregroundColor(.white)
                                .lineLimit(3)
                        }
                        .fixedSize(horizontal: false, vertical: true)
                        Group{
                            Text(notificationInfo.getNotifictionDay) + Text(" at ") + Text(notificationInfo.getNotifictionTime)
                        }
                        .font(.system(size: 15.0))
                        .foregroundColor(.gray)
                    }
                    .frame(maxWidth: .infinity ,alignment: .topLeading)
                }
            }
            .padding(.all)
            Divider()
                .background(Color.white)
                .offset(x: 0, y:0)
        }.background(notificationInfo.status == .unread ? Color.init(ColorConstantsName.NotificationColour) : Color.clear).listRowBackground(Color.clear)
    }
}
