//
//  AnnouncementView.swift
//  dtr-ios-test
//
//  Created by Mobile on 10/03/21.
//

import SwiftUI

import SwiftUI

struct AnnouncementView: View {
    var announcementID: String
    @State var showOnlyTextEdit = false
    @State var notificationInfo :AppNotification
    @Environment(\.presentationMode) var presentationMode
    
    
    var announcement: AnnouncementsModel {
        if let index = DtrData.sharedInstance.announcements.firstIndex(where: { $0.id == announcementID }) {
            return DtrData.sharedInstance.announcements[index]
        }
        return AnnouncementsModel(id: "error", title: "Error", message: "Error message goes here", expires: Date().startOfDayPrevious)
    }
    
    var textToRender: String {
        let result = announcement.message.replacingOccurrences(of: "\\\\n", with: "\n\n")
        return result
    }
    
    var body: some View {
        ZStack{
            Color.init(ColorConstantsName.SectionBgColour)
                .ignoresSafeArea()
            VStack {
                TextView(comesFromDetailAndEditScreen: .constant(true), isEditable: .constant(false), text:  .constant(showOnlyTextEdit == true ? textToRender: announcementID), textStyle: .constant(UIFont.TextStyle.body), comesFromEditScreen: .constant(false), didStartEditing: .constant(false),placeHolderText:"")
                    .padding()
                Spacer()
                if showOnlyTextEdit {
                    CustomButtonCircleWithCustomProperty(btnTitle: "Accept", btnWidth: 100, btnHeight: 20, backgroundColord: true, txtColorAccent: false, strokeColor: true) {
                        presentationMode.wrappedValue.dismiss()
                    }
                    .padding(.bottom)
                }
            }
        }.onAppear{
            if notificationInfo.type == .announcement {
                showOnlyTextEdit = true
            }
            
            if notificationInfo.type == .rideCancelled {
                showOnlyTextEdit = false
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle(showOnlyTextEdit == true ? announcement.title: "", displayMode: .inline)
        .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,textTitle: "", systemImageName: ImageConstantsName.ArrowLeftImg, action: {
            presentationMode.wrappedValue.dismiss()
        })))
    }
}

struct AnnouncementView_Previews: PreviewProvider {
    static var previews: some View {
        AnnouncementView(announcementID: "PREVIEW", notificationInfo: AppNotification.init(data: ["bgdfjkg":"g ghdfj"]))
    }
}


struct AnnouncementDetailView: View {
    var announcementID: String
    @State var showOnlyTextEdit = false
    @State var notificationInfo :AppNotification
    @Environment(\.presentationMode) var presentationMode
    
    
    var announcement: AnnouncementsModel {
        if let index = DtrData.sharedInstance.announcements.firstIndex(where: { $0.id == announcementID }) {
            return DtrData.sharedInstance.announcements[index]
        }
        return AnnouncementsModel(id: notificationInfo.id, title: "Announcement", message: notificationInfo.message, expires: Date().startOfDayPrevious)
    }
    
    var textToRender: String {
        let result = announcement.message.replacingOccurrences(of: "\\\\n", with: "\n\n")
        return result
    }
    
    var body: some View {
        ZStack{
            Color.init(ColorConstantsName.SectionBgColour)
                .ignoresSafeArea()
            VStack {
                TextView(comesFromDetailAndEditScreen: .constant(true), isEditable: .constant(false), text:  .constant(showOnlyTextEdit == true ? textToRender: announcementID), textStyle: .constant(UIFont.TextStyle.body), comesFromEditScreen: .constant(false), didStartEditing: .constant(false),placeHolderText:"")
                    .padding()
                Spacer()
            }
        }.onAppear{
            if notificationInfo.type == .announcement {
                showOnlyTextEdit = true
            }
            
            if notificationInfo.type == .rideCancelled {
                showOnlyTextEdit = false
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle(showOnlyTextEdit == true ? announcement.title: "", displayMode: .inline)
        .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,textTitle: "", systemImageName: ImageConstantsName.ArrowLeftImg, action: {
            presentationMode.wrappedValue.dismiss()
        })))
    }
}
