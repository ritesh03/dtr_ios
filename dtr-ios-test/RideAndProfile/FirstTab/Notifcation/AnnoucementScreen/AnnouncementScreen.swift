//
//  AnnouncementScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 14/04/21.
//

import SwiftUI
import Firebase

struct AnnouncementScreen: View {
    
    @Binding var showActivityIndicator : Bool
    @Binding var anoucmentArray : [AppNotification]
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State var arrayForDeletenotifications: [AppNotification] = []
    @State var notifications: [AppNotification] = []
    
    var body: some View {
        Group {
            if self.anoucmentArray.count > 0{
                SectionTitleViewUI(titleString: "ANNOUNCEMENT", rightTitleString: "", countShow:  0) {}
                ForEach(self.anoucmentArray.indices, id: \.self) { notification in
                    AnnouncementNotificationView(notificationInfo: self.anoucmentArray[notification]).listRowBackground(Color.clear)
                }.onDelete(perform: deleteReceivedNotification)
            }
        }
        .onAppear{
            Analytics.logEvent(AnalyticsScreen.announcements.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
        }
        .navigationBarHidden(false)
    }
    
    func deleteReceivedNotification(at offsets: IndexSet) {
        showActivityIndicator = true
        for index in offsets {
            arrayForDeletenotifications.removeAll()
            arrayForDeletenotifications.append(anoucmentArray[index])
            anoucmentArray.remove(at: index)
        }
        
        if arrayForDeletenotifications.count > 0 {
            self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":arrayForDeletenotifications[0].payloadID,"source":arrayForDeletenotifications[0].source,"type":arrayForDeletenotifications[0].type.rawValue,"target":arrayForDeletenotifications[0].target]) { (result) in
                showActivityIndicator = false
            }
        }
    }
}
