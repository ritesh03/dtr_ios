//
//  GroupSuggestionInviteView.swift
//  dtr-ios-test
//
//  Created by Vaibhav Thakkar on 23/12/21.
//

import SwiftUI

struct GroupSuggestionInviteView: View {
    
    @Binding var comesFromSheetFriendInvite : Bool
    @Binding var updateFriendRequestStatus : Bool
    @Binding var showActivityIndicator : Bool
    @Binding var gorupInviteArray : [AppNotification]
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var arrayForDeletenotifications : [AppNotification] = []
    
    
    var body: some View {
        Group {
            if self.gorupInviteArray.count > 0{
                SectionTitleViewUI(titleString: "Group Update", rightTitleString: "", countShow:  0) {}
                ForEach(self.gorupInviteArray.indices, id: \.self) { notification in
                    GroupRequestNotificationView(comesFromSheetFriendInvite:$comesFromSheetFriendInvite,updateFriendRequestStatus:$updateFriendRequestStatus,notificationInfo: self.gorupInviteArray[notification], groupInviteArray: gorupInviteArray)
                        .onDisappear {
                            UITableViewCell.appearance().isSelected = false
                        }
                        .listRowBackground(Color.clear)
                }.onDelete(perform: deleteReceivedNotification)
            }
        }
        .navigationBarHidden(false)
    }
    
    func deleteReceivedNotification(at offsets: IndexSet) {
        showActivityIndicator = true
        for index in offsets {
            arrayForDeletenotifications.removeAll()
            arrayForDeletenotifications.append(gorupInviteArray[index])
            gorupInviteArray.remove(at: index)
        }
        
        if arrayForDeletenotifications.count > 0 {
            self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":arrayForDeletenotifications[0].payloadID,"source":arrayForDeletenotifications[0].source,"type":arrayForDeletenotifications[0].type.rawValue,"target":arrayForDeletenotifications[0].target]) { (result) in
                showActivityIndicator = false
            }
        }
    }
}
