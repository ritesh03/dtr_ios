//
//  GroupNotificationScreenView.swift
//  dtr-ios-test
//
//  Created by apple on 14/12/21.
//

import SwiftUI

struct GroupNotificationScreenView: View {
    var notificationInfo :AppNotification
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        VStack {
            HStack(alignment:.top) {
                ProfileHeadForGroupSection(group: self.dtrData.groupsDirectory[dynamicMember: notificationInfo.payloadID], size: .medium)
                NavigationLink(destination:GroupDetailScreen(dismissView: .constant(false),comeFromNotificaionScreen:true, groupDetailEdit:GroupEditViewModel.init(group: self.dtrData.groupsDirectory[dynamicMember: notificationInfo.payloadID]))){
                    VStack(alignment: .leading, spacing: 10){
                        HStack(alignment:.top){
                            Text("\(notificationInfo.message)")
                                .foregroundColor(.white)
                        }
                        .fixedSize(horizontal: false, vertical: true)
                        Group{
                            Text(notificationInfo.getNotifictionDay) + Text(" at ") + Text(notificationInfo.getNotifictionTime)
                        }
                        .font(.system(size: 15.0))
                        .foregroundColor(.gray)
                    }
                    .frame(maxWidth: .infinity ,alignment: .topLeading)
                }
            }
            .padding(.all)
            Divider()
                .background(Color.white)
                .offset(x: 0, y:0)
        }.background(notificationInfo.status == .unread ? Color.init(ColorConstantsName.NotificationColour) : Color.clear).listRowBackground(Color.clear)
    }
}
