//
//  GroupInviteDeclineScreen.swift
//  dtr-ios-test
//
//  Created by apple on 09/12/21.
//

import SwiftUI

struct GroupInviteDeclineScreen: View {
    
    var notificationInfo :AppNotification = AppNotification.default
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    
    var body: some View {
        VStack {
            HStack(alignment:.top) {
                ProfileHeadForGroupSection(group: self.dtrData.groupsDirectory[dynamicMember: notificationInfo.payloadID], size: .medium)
                NavigationLink(destination:GroupDetailScreen(dismissView: .constant(false),comeFromNotificaionScreen:true, groupDetailEdit:GroupEditViewModel.init(group: self.dtrData.groupsDirectory[dynamicMember: notificationInfo.payloadID]))){
                    VStack(alignment: .center, spacing: 10){
                        Text(notificationInfo.message)
                            .foregroundColor(.gray)
                            .padding(.bottom,10)
                        HStack(spacing:20) {
                            CustomButtonCircleWithCustomProperty(btnTitle: "Yes", btnWidth: 90, btnHeight:10, backgroundColord: false, txtColorAccent: true, strokeColor: true) {
                                dtrCommands.groupJoin(groupID: notificationInfo.payloadID) { response in
                                    print("Response = ",response)
                                }
                            }.buttonStyle(BorderlessButtonStyle())
                            CustomButtonCircleWithCustomProperty(btnTitle: "No", btnWidth: 90, btnHeight:10, backgroundColord: false, txtColorAccent: false, strokeColor: false){
                                dtrCommands.groupDeclineComand(groupID: notificationInfo.payloadID, groupLeaderID: notificationInfo.source) { response in
                                    print("No,",response)
                                }
                            }.buttonStyle(BorderlessButtonStyle())
                        }
                    }
                }
                .frame(maxWidth: .infinity ,alignment: .topLeading)
            }
            .padding(.all,15)
            Divider()
                .background(Color.white)
                .offset(x: 0, y:0)
        }.background(notificationInfo.status == .unread ? Color.init(ColorConstantsName.NotificationColour) : Color.clear).listRowBackground(Color.clear)
    }
}

struct GroupInviteDeclineScreen_Previews: PreviewProvider {
    static var previews: some View {
        GroupInviteDeclineScreen()
    }
}
