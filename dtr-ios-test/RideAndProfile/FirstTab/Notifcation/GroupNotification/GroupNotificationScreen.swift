//
//  GroupNotificationScreen.swift
//  dtr-ios-test
//
//  Created by apple on 03/08/21.
//

import Foundation
import SwiftUI

struct GroupNotificationScreen: View {
    
    @Binding var showActivityIndicator :  Bool
    @Binding var groupMessageArray :   [AppNotification]
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var arrayForDeletenotifications : [AppNotification] = []
    @State var notifications: [AppNotification] = []
    
    var body: some View {
        Group {
            if groupMessageArray.count > 0  {
                SectionTitleViewUI(titleString: "GROUP MESSAGES", rightTitleString: "", countShow:  0) {}
                ForEach(self.groupMessageArray.indices, id: \.self) { notification in
                    GroupNotificationScreenView(notificationInfo: self.groupMessageArray[notification]).listRowBackground(Color.clear)
                }.onDelete(perform: deleteReceivedNotification)
            }
        }
    }
    
    func deleteReceivedNotification(at offsets: IndexSet) {
        
        showActivityIndicator = true
        for index in offsets {
            arrayForDeletenotifications.removeAll()
            arrayForDeletenotifications.append(groupMessageArray[index])
            groupMessageArray.remove(at: index)
        }
        
        if arrayForDeletenotifications.count > 0 {
            self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":arrayForDeletenotifications[0].payloadID,"source":arrayForDeletenotifications[0].source,"type":arrayForDeletenotifications[0].type.rawValue,"target":arrayForDeletenotifications[0].target]) { (result) in
                showActivityIndicator = false
                result.debug(methodName:"clearNotifcationFromServer")
            }
        }
    }
}
