//
//  GroupRequestNotificationView.swift
//  dtr-ios-test
//
//  Created by Vaibhav Thakkar on 23/12/21.
//


import SwiftUI
import Firebase

struct GroupRequestNotificationView: View {
    
    @Binding var comesFromSheetFriendInvite:Bool
    @Binding var updateFriendRequestStatus:Bool
    var notificationInfo :AppNotification
    var groupInviteArray :[AppNotification]
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State var showRequestrProfileScreen = false
    
    var body: some View {
        VStack {
            HStack(alignment:.top) {
                ProfileHeadForGroupSection(group: self.dtrData.groupsDirectory[dynamicMember: notificationInfo.payloadID], size: .medium)
                
                NavigationLink(destination:GroupDetailScreen(dismissView: .constant(false),comeFromNotificaionScreen:true, groupInviteArray: groupInviteArray, groupDetailEdit:GroupEditViewModel.init(group: self.dtrData.groupsDirectory[dynamicMember: notificationInfo.payloadID]))){
                    
                    VStack(alignment: .leading, spacing: 10){
                        Group{
                            Text(notificationInfo.message).font(.system(size: 17.0)).fixedSize(horizontal: false, vertical: true)
                        } .foregroundColor(.gray)
                        if notificationInfo.action != .groupLeft && notificationInfo.action != .groupLeaderMessage && !notificationInfo.message.contains(CommonAllString.GroupJoinMesgStr) {
                            HStack {
                                CustomButtonCircleWithCustomProperty(btnTitle: "Yes", btnWidth: 90, btnHeight:10, backgroundColord: false, txtColorAccent: true, strokeColor: true) {
                                    updateGroupRequestStatusMethod(indexType: 0)
                                }.buttonStyle(BorderlessButtonStyle())
                                CustomButtonCircleWithCustomProperty(btnTitle: "No", btnWidth: 90, btnHeight:10, backgroundColord: false, txtColorAccent: false, strokeColor: false){
                                    updateGroupRequestStatusMethod(indexType:1)
                                }.buttonStyle(BorderlessButtonStyle())
                            }
                        }
                    }
                    .frame(maxWidth: .infinity ,alignment: .topLeading)
                }
            }
            .padding(.all,15)
            .onAppear() {
                let hasJoinedGroup = DtrData.sharedInstance.profile.joinedgroups.contains(notificationInfo.payloadID)
                if hasJoinedGroup {
                    updateGroupRequestStatusMethod(indexType: 0)
                }
            }
            Divider()
                .background(Color.white)
                .offset(x: 0, y:0)
        }.background(notificationInfo.status == .unread ? Color.init(ColorConstantsName.NotificationColour) : Color.clear).listRowBackground(Color.clear)
    }
    
    func updateGroupRequestStatusMethod(indexType:Int){
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            if indexType == 0 {
                self.dtrCommands.groupJoin(groupID: notificationInfo.payloadID) { (result) in
                    let notifications = groupInviteArray.filter({$0.payloadID == notificationInfo.payloadID})
                    for notification in notifications {
                        self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":notification.payloadID,"source":notification.source,"type":notification.type.rawValue,"target":notification.target]) { (result) in
                        }
                    }
                }
            }else{
                self.dtrCommands.groupJoinDecline(groupID: notificationInfo.payloadID, requestedProfileID: notificationInfo.source) { (result) in
                    self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":notificationInfo.payloadID,"source":notificationInfo.source,"type":notificationInfo.type.rawValue,"target":notificationInfo.target]) { (result) in
                    }
                }
            }
        }
    }
}
