//
//  InviteScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 13/03/21.
//

import SwiftUI

struct InviteScreen: View {
    @Binding  var showDeatilPopUp: Bool
    var ride: DtrRide
    @StateObject var userListViewModel = UserListViewModel()
    @State var profile: DtrProfile
    @Environment(\.presentationMode) var presentationMode
    
    @State  var selectFriends: [String]
    @State  var selectFollowers: [String]
    @State  var enableDisableInviteBtn:Bool = true
    
    
    var body: some View {
        ZStack {
            Color.init("DTR-MainThemeBackground")
                .ignoresSafeArea()
            VStack{
                ScrollView {
                    LazyVStack(spacing:0){
                        Group {
                            UserFriendInvite( selectFriends: $selectFriends, enableDisableInviteBtn: $enableDisableInviteBtn)
                            UserFollowerInvite( selectFollowers: $selectFollowers, enableDisableInviteBtn: $enableDisableInviteBtn)
                        }
                    }
                }
                CustomButtonCircleRS(btnTitle: "Invite", btnWidth: UIScreen.screenWidth-100, btnHeight: 10, backgroundColord: true, txtColorAccent: false, strokeColor: false) {
                    let recpitIds = selectFriends.filter { $0 != "" } + selectFollowers.filter { $0 != "" }
                    let mesg = " invited you to a ride on \(dayName(dateCode: self.ride.dateCode)) \(self.ride.summary)"
                    DtrCommand.sharedInstance.sendRideInvitation(profileID: self.ride.id, data: ["message":mesg,"recipients":Array(Set(recpitIds))]) { (result) in
                        if result.result == .ok {
                            showDeatilPopUp = true
                            presentationMode.wrappedValue.dismiss()
                        }
                    }
                }.disabled(enableDisableInviteBtn)
                .padding(.bottom)
            }.onAppear{
                
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,txtTitle: "Invite friends", systemImgName: "arrow.left", action: {
                presentationMode.wrappedValue.dismiss()
            })))
        }
    }
}

struct InviteScreen_Previews: PreviewProvider {
    static var previews: some View {
        InviteScreen(showDeatilPopUp: .constant(false), ride: DtrRide.default, profile: DtrProfile.default, selectFriends: [""], selectFollowers: [""])
    }
}
