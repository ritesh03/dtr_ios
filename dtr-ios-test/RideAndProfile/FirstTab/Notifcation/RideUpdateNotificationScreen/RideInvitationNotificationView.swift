//
//  RideInvitationNotificationView.swift
//  dtr-ios-test
//
//  Created by Mobile on 11/03/21.
//

import SwiftUI
import Firebase

struct RideInvitationNotificationView: View {
    
    @Binding var day:Int
    var dateCode: String
    var source: String
    var notificationInfo :AppNotification
    
    
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State private var ride: DtrRide = DtrRide(profileID: "NOT", dateCode: "NOT", summary: "Not")
    @State private var hasRide = false
    @State private var showMainButtonAfterClick = true
    @State private var joinButtonDisable = false
    @State private var mayBeButtonDisable = false
    @State private var thnxButtnoDisable = false
    @State private var titleMainButtonAfterClick = ""
    @State private var showRideDetailScreen = false
    @Environment(\.presentationMode) var presentationMode
    @State private var showRideDetailSheetScreen = false
    @State var comesFromSheet = false
    @Environment (\.modalMode) var modalMode
    var isSelected: Bool =  false
    var action: (Int) -> Void
    
    var body: some View {
        VStack {
            HStack(alignment:.top) {
                ProfileHead(profile: dtrData.profileInfo(profileID: notificationInfo.source), size: .medium, comeFromNotificationScreen:true)
                VStack(alignment: .leading, spacing: 15){
                    HStack(alignment:.top) {
                        NavigationLink(destination: RideDetail(day:$day, myRideForDay: $ride,rideFromNotifcaitonScreen:ride,rideFromNotifcaitonBool:true,presentModelOldHistoryScreen: .constant(false), myRideForDay1: RideEditViewModel.init(ride: ride)), isActive: $showRideDetailScreen){
                            Text("\(notificationInfo.message)")
                                .foregroundColor(.gray)
                                .font(.system(size: 16.0))
                        }.disabled(hasRide ==  true ? false : true)
                    }.onTapGesture {
                        if comesFromSheet {
                            showRideDetailSheetScreen = true
                        }else{
                            showRideDetailScreen = true
                        }
                    }.disabled(hasRide ==  true ? false : true)
                        .fixedSize(horizontal: false, vertical: true)
                    if showMainButtonAfterClick {
                        HStack {
                            CustomButtonCircleWithCustomProperty(btnTitle: "Join", btnWidth: 60, btnHeight:10, backgroundColord: false, txtColorAccent: true, strokeColor: true) {
                                joinButtonDisable = true
                                if hasRide {
                                    if let myRide = dtrData.myRidesByDate[ride.dateCode] {
                                        if myRide.leader != self.dtrData.profile.id && myRide.leader != self.ride.leader {
                                            showRideDetailScreen = true
                                            return
                                        }
                                    }
                                    showMainButtonAfterClick = false
                                    titleMainButtonAfterClick = " Joined 🎉 "
                                    self.day = toOffset(dateCode: String(self.ride.dateCode))
                                    
                                    self.dtrCommands.rideJoin(rideID: self.ride.id,dateCode:self.ride.dateCode ) { result in
                                        joinButtonDisable = false
                                        if result.result == .ok {
                                            //self.dtrCommands.sendRideMessage(rideID:self.ride.id,message:"\(dtrData.profile.name) has joined the ride") { ( result) in
                                            self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":notificationInfo.payloadID,"source":notificationInfo.source,"type":notificationInfo.type.rawValue,"target":notificationInfo.target]) { (result) in
                                                self.dtrCommands.sendSimpleNotification(profileID: self.ride.id, data: ["recipients":[ self.ride.leader],"type":"rideUpdate","message":"\(dtrData.profile.name) joined your ride on \(dayName(dateCode: self.ride.dateCode)) for \(self.ride.summary)"]) { ( result) in
                                                    result.debug(methodName:"sendSimpleNotification")
                                                }
                                            }
                                            //}
                                            self.modalMode.wrappedValue = false
                                            presentationMode.wrappedValue.dismiss()
                                        }
                                    }
                                }
                            }.buttonStyle(BorderlessButtonStyle()).disabled(joinButtonDisable == true ? true :false)
                            
                            CustomButtonCircleWithCustomProperty(btnTitle: "Maybe", btnWidth: 60, btnHeight:10, backgroundColord: false, txtColorAccent: false, strokeColor: false,disabled: isSelected) {
                                titleMainButtonAfterClick = " May Be "
                                showMainButtonAfterClick = false
                                self.dtrCommands.sendSimpleNotification(profileID: ride.id, data: ["recipients":[notificationInfo.source],"type":"rideUpdate","message":"😉 \(dtrData.profile.name) said maybe to your ride on \(dayName(dateCode: self.ride.dateCode))"]) { ( result) in
                                    result.debug(methodName:"sendSimpleNotification")
                                    if result.result == .ok {
                                        Analytics.logEvent(AnalyticsEvent.notificationSent.rawValue, parameters: ["notificationID": notificationInfo.id, "type": notificationInfo.type.data])
                                    }
                                    self.action(1)
                                }
                            }
                            .disabled(isSelected)
                            .buttonStyle(BorderlessButtonStyle()).disabled(mayBeButtonDisable == true ? true :false)
                            
                            CustomButtonCircleWithCustomProperty(btnTitle: "No Thx", btnWidth: 60, btnHeight:10, backgroundColord: false, txtColorAccent: false, strokeColor: false) {
                                DispatchQueue.main.async {
                                    self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":notificationInfo.payloadID,"source":notificationInfo.source,"type":notificationInfo.type.rawValue,"target":notificationInfo.target]) { (result) in
                                        result.debug(methodName:"clearNotifcationFromServer")
                                        self.dtrCommands.sendSimpleNotification(profileID: ride.id, data: ["recipients":[notificationInfo.source],"type":"rideUpdate","message":"😉 \(dtrData.profile.name) can't make it to your ride on \(dayName(dateCode: self.ride.dateCode))"]) { ( result) in
                                            DispatchQueue.main.async {
                                                showMainButtonAfterClick = false
                                                titleMainButtonAfterClick = " Said No 🙃 "
                                                result.debug(methodName:"sendSimpleNotification")
                                            }
                                            if result.result == .ok {
                                                Analytics.logEvent(AnalyticsEvent.notificationSent.rawValue, parameters: ["notificationID": notificationInfo.id, "type": notificationInfo.type.data])
                                            }
                                        }
                                    }
                                }
                            }.buttonStyle(BorderlessButtonStyle()).disabled(thnxButtnoDisable == true ? true :false)
                                .animation(.spring())
                        }
                    } else{
                        CustomButtonCircleWithCustomProperty(btnTitle: titleMainButtonAfterClick, btnWidth: UIScreen.screenWidth-200, btnHeight:10, backgroundColord: false, txtColorAccent: true, strokeColor: true) {
                        }.padding(.leading,30)
                    }
                    Group{
                        Text(notificationInfo.getNotifictionDay) + Text(" at ") + Text(notificationInfo.getNotifictionTime)
                    }
                    .font(.system(size: 15.0))
                    .foregroundColor(.gray)
                }
                .frame(maxWidth: .infinity ,alignment: .topLeading)
            }
            .padding(.all)
            Divider()
                .background(Color.white)
                .offset(x: 0, y:0)
        }.background(notificationInfo.status == .unread ? Color.init(ColorConstantsName.NotificationColour) : Color.clear).listRowBackground(Color.clear)
            .background(EmptyView().sheet(isPresented: self.$showRideDetailSheetScreen) {
                Modal(isPresented: self.$showRideDetailSheetScreen, title:self.notificationInfo.type.label) {
                    RideDetail(day: $day, myRideForDay: $ride, rideFromNotifcaitonScreen: ride, rideFromNotifcaitonBool: true, rideFromPushNotification:true, presentModelOldHistoryScreen:  .constant(false), myRideForDay1: RideEditViewModel.init(ride: ride))
                }
            })
            .onAppear {
                self.dtrData.upcomingRideWithSourceCode(profileID: notificationInfo.source, dateCode: notificationInfo.payloadID) { ride in
                    if let ride = ride {
                        self.ride = ride
                        self.hasRide = true
                    }else {
                        self.dtrData.upcomingRideWithId(profileID: notificationInfo.source, dateCode: notificationInfo.payloadID) { ride in
                            if let ride = ride {
                                self.ride = ride
                                self.hasRide = true
                            }
                        }
                    }
                }
            }
    }
}

struct RideInvitationNotificationView_Previews: PreviewProvider {
    static var previews: some View {
        RideInvitationNotificationView(day: .constant(0), dateCode: "123125", source: "123", notificationInfo: AppNotification.init(data: asDataDict(["w":"w"])), action: {_ in })
    }
}
