//
//  RideUpdateNotificationView.swift
//  dtr-ios-test
//
//  Created by apple on 14/12/21.
//

import SwiftUI

struct RideUpdateNotificationView: View {
    var notificationInfo :AppNotification
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var ride: DtrRide = DtrRide(profileID: "NOT", dateCode: "NOT", summary: "Not")
    @State private var hasRide = false
    @State private var showRideDetailScreen = false
    @State private var showRideDetailSheetScreen = false
    @State var comesFromSheet = false
    
    var body: some View {
        VStack {
            HStack(alignment:.top) {
                ProfileHeadForOnlyRideUpdateView(profile: dtrData.profileInfo(profileID: notificationInfo.source), size: .medium,comeFromRideUpdatNotifcaiton:true)
                VStack(alignment: .leading, spacing: 10){
                    HStack(alignment:.top) {
                        NavigationLink(destination: RideDetail(day:.constant(0), myRideForDay: $ride,rideFromNotifcaitonScreen:ride,rideFromNotifcaitonBool:true,presentModelOldHistoryScreen: .constant(false), myRideForDay1: RideEditViewModel.init(ride: ride)), isActive: $showRideDetailScreen){
                            Text("\(notificationInfo.message)")
                                .foregroundColor(.white)
                        }.disabled(hasRide ==  true ? false : true)
                    }.onTapGesture {
                        if comesFromSheet {
                            showRideDetailSheetScreen = true
                        }else{
                            showRideDetailScreen = true
                        }
                    }.disabled(hasRide ==  true ? false : true)
                    .fixedSize(horizontal: false, vertical: true)
                    Group{
                        Text(notificationInfo.getNotifictionDay) + Text(" at ") + Text(notificationInfo.getNotifictionTime)
                    }
                    .font(.system(size: 15.0))
                    .foregroundColor(.gray)
                }
                .frame(maxWidth: .infinity ,alignment: .topLeading)
            }
            .padding(.all)
            Divider()
                .background(Color.white)
                .offset(x: 0, y:0)
        }.background(notificationInfo.status == .unread ? Color.init(ColorConstantsName.NotificationColour) : Color.clear).listRowBackground(Color.clear)
        
        .background(EmptyView().sheet(isPresented: self.$showRideDetailSheetScreen) {
            Modal(isPresented: self.$showRideDetailSheetScreen, title:self.notificationInfo.type.label) {
                RideDetail(day: .constant(0), myRideForDay: $ride, rideFromNotifcaitonScreen: ride, rideFromNotifcaitonBool: true, rideFromPushNotification:true, presentModelOldHistoryScreen:  .constant(false), myRideForDay1: RideEditViewModel.init(ride: ride))
            }
        })
        
        .onAppear {
            if !notificationInfo.message.contains("maybe") &&  !notificationInfo.message.contains("make it") {
                self.dtrData.upcomingRideWithSourceCode(profileID: notificationInfo.source, dateCode: notificationInfo.payloadID) { ride in
                    if let ride = ride {
                        self.ride = ride
                        self.hasRide = true
                    } else {
                        self.dtrData.upcomingRideWithId(profileID: notificationInfo.source, dateCode: notificationInfo.payloadID) { ride in
                            if let ride = ride {
                                self.ride = ride
                                self.hasRide = true
                            }
                        }
                    }
                }
            } else {
                self.dtrData.upcomingRideWithSourceCode(profileID: notificationInfo.target, dateCode: notificationInfo.payloadID) { ride in
                    if let ride = ride {
                        self.ride = ride
                        self.hasRide = true
                    }
                }
            }
        }
    }
}
