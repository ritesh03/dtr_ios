//
//  RideUpdateNotificationScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 14/04/21.
//

import SwiftUI

struct RideUpdateNotificationScreen: View {
    @State var comesFromSheet = false
    @Binding var day:Int
    @Binding  var showActivityIndicator :  Bool
    @Binding  var rideAllOtherUpdates :  [AppNotification]
    @Binding  var rideInviteArray :  [AppNotification]
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var arrayForDeletenotifications: [AppNotification] = []
    @Binding var selectMaybe: [String]
    
    var body: some View {
        Group {
            if self.rideInviteArray.count > 0 || rideAllOtherUpdates.count > 0 {
                SectionTitleViewUI(titleString: "RIDE UPDATE", rightTitleString: "", countShow:  0) {}
                ForEach(self.rideInviteArray.indices, id: \.self) { notification in
                    RideInvitationNotificationView(day:$day,dateCode: rideInviteArray[notification].payloadID, source: rideInviteArray[notification].source, notificationInfo: self.rideInviteArray[notification],comesFromSheet:comesFromSheet,isSelected: selectMaybe.contains(rideInviteArray[notification].id),action: { index in
                        if index == 0 {
                            
                        }
                        if index == 1 {
                            self.selectMaybe.append(rideInviteArray[notification].id)
                            UserStore.shared.notificationMayBeDisabledData = self.selectMaybe
                        }
                        
                    }).id(UUID())
                        .listRowBackground(Color.clear)
                }.onDelete(perform: deleteFromInviteArray)
                
                ForEach(self.rideAllOtherUpdates.indices, id: \.self) { notification in
                    RideUpdateNotificationView(notificationInfo: self.rideAllOtherUpdates[notification],comesFromSheet:comesFromSheet).id(notification)
                        .listRowBackground(Color.clear)
                }.onDelete(perform: deleteFromUpdateArray)
            }
        }.navigationBarHidden(false)
    }
    
    func deleteFromInviteArray(at offsets: IndexSet) {
        showActivityIndicator = true
        for index in offsets {
            arrayForDeletenotifications.removeAll()
            arrayForDeletenotifications.append(rideInviteArray[index])
            rideInviteArray.remove(at: index)
        }
        
        if arrayForDeletenotifications.count > 0 {
            self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":arrayForDeletenotifications[0].payloadID,"source":arrayForDeletenotifications[0].source,"type":arrayForDeletenotifications[0].type.rawValue,"target":arrayForDeletenotifications[0].target]) { (result) in
                showActivityIndicator = false
                result.debug(methodName:"clearNotifcationFromServer")
            }
        }
    }
    
    func deleteFromUpdateArray(at offsets: IndexSet) {
        showActivityIndicator = true
        for index in offsets {
            arrayForDeletenotifications.removeAll()
            arrayForDeletenotifications.append(rideAllOtherUpdates[index])
            rideAllOtherUpdates.remove(at: index)
        }
        if arrayForDeletenotifications.count > 0 {
            self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":arrayForDeletenotifications[0].payloadID,"source":arrayForDeletenotifications[0].source,"type":arrayForDeletenotifications[0].type.rawValue,"target":arrayForDeletenotifications[0].target]) { (result) in
                showActivityIndicator = false
                //result.debug()
            }
        }
    }
}
