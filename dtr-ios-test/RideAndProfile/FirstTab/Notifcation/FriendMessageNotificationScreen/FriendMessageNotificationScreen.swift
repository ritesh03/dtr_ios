//
//  FriendMessageNotificationScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 14/04/21.
//

import SwiftUI

struct FriendMessageNotificationScreen: View {
    
    @Binding var day :  Int
    @Binding var showActivityIndicator :  Bool
    @Binding var friendOtherMessageArray :  [AppNotification]
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State private var arrayForDeletenotifications : [AppNotification] = []
    
    var body: some View {
        Group {
            if friendOtherMessageArray.count > 0  {
                SectionTitleViewUI(titleString: "FRIEND MESSAGES", rightTitleString: "", countShow:  0) {}
            }
            if friendOtherMessageArray.count > 0 {
                ForEach(self.friendOtherMessageArray.indices, id: \.self) { notification in
//                    if self.friendOtherMessageArray[notification].message.contains ("is requesting you to join") {
//                        GroupInviteDeclineScreen(notificationInfo: self.friendOtherMessageArray[notification]).listRowBackground(Color.clear)
//                    }else
                    //if self.friendOtherMessageArray[notification].action != .groupLeaderMessage && self.friendOtherMessageArray[notification].action != .groupLeft {
                        FriendMessageView(day:$day,notificationInfo: self.friendOtherMessageArray[notification]).listRowBackground(Color.clear)
                    
//                    } else {
//                        GroupNotificationScreenView(notificationInfo: self.friendOtherMessageArray[notification]).listRowBackground(Color.clear)
//                    }
                    
//                    if !self.friendOtherMessageArray[notification].message.contains("grow your followers and friends list by seeing who’s in the group") && !self.friendOtherMessageArray[notification].message.contains ("have joined") && !self.friendOtherMessageArray[notification].message.contains ("has joined") && !self.friendOtherMessageArray[notification].message.contains ("has left the") {
//                        FriendMessageView(day:$day,notificationInfo: self.friendOtherMessageArray[notification]).listRowBackground(Color.clear)
//                    } else {
//                        GroupNotificationScreenView(notificationInfo: self.friendOtherMessageArray[notification]).listRowBackground(Color.clear)
//                    }
                }.onDelete(perform: deleteReceivedNotification)
            }
        }.navigationBarHidden(false)
    }
    
    func deleteReceivedNotification(at offsets: IndexSet) {
        showActivityIndicator = true
        for index in offsets {
            arrayForDeletenotifications.removeAll()
            arrayForDeletenotifications.append(friendOtherMessageArray[index])
            friendOtherMessageArray.remove(at: index)
        }
        if arrayForDeletenotifications.count > 0 {
            self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":arrayForDeletenotifications[0].payloadID,"source":arrayForDeletenotifications[0].source,"type":arrayForDeletenotifications[0].type.rawValue,"target":arrayForDeletenotifications[0].target]) { (result) in
                showActivityIndicator = false
                result.debug(methodName:"clearNotifcationFromServer")
            }
        }
    }
}


struct FriendMessageView: View {
    @Binding var day :  Int
    var notificationInfo :AppNotification
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State private var showAnoucmentScreen = false
    @State private var showTextEditOnly = false
    @State var titleStatus = "Follow"
    @State private var showActivityIndicator = false
    
    var body: some View {
        VStack {
            HStack(alignment:.top) {
                NavigationLink(destination: ProfileScreen(updatedDismissView:.constant(false),day: $day,userProfile: .constant(self.dtrData.profileInfo(profileID: notificationInfo.source)), showBackButtonOrNot:true)){
                    ProfileHead(profile: dtrData.profileInfo(profileID: notificationInfo.source), size: .medium, comeFromNotificationScreen:true)
                    VStack(alignment: .leading, spacing: 10){
                        HStack(alignment:.top){
                            Text("\(notificationInfo.message)")
                                .foregroundColor(.white)
                        }
                        .fixedSize(horizontal: false, vertical: true)
                        Group{
                            Text(notificationInfo.getNotifictionDay) + Text(" at ") + Text(notificationInfo.getNotifictionTime)
                        }
                        .font(.system(size: 15.0))
                        .foregroundColor(.gray)
                    }
                    .frame(maxWidth: .infinity ,alignment: .topLeading)
                    if notificationInfo.type == .follow || notificationInfo.type == .friendInvite {
                        if notificationInfo.message.contains("is now following you") || notificationInfo.message.contains("would like to be friends") {
                            if !dtrData.profile.friends.contains(notificationInfo.source) {
                                if self.showActivityIndicator {
                                    ProgressView()
                                        .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                        .frame(width: 20, height: 20)
                                        .padding(.trailing,5)
                                }
                                if titleStatus != "requestedFriend" {
                                    Button(action: {
                                        callFuntionAccToStatus(userId: notificationInfo.source)
                                    }) {
                                        Text(getStatusOfUser(userId: notificationInfo.source))
                                            .multilineTextAlignment(.trailing)
                                            .fixedSize(horizontal: false, vertical: true)
                                            .lineLimit(2)
                                            .foregroundColor(titleStatus == "Friend request sent" ? Color.gray : Color.init(ColorConstantsName.AccentColour))
                                            .frame(width: titleStatus == "Follow" ? 90 : 120, alignment: .center).padding(5)
                                            .font(.system(size: 15, weight: .bold, design: .default))
                                    }.buttonStyle(BorderlessButtonStyle())
                                        .clipShape(Capsule())
                                        .overlay(Capsule().stroke(Color.init(ColorConstantsName.AccentColour), lineWidth: titleStatus == "Friend request sent" ? 0 : 1))
                                }
                            }
                        }
                    }
                }
            }
            .padding(.all)
            Divider()
                .background(Color.white)
                .offset(x: 0, y:0)
        }.background(notificationInfo.status == .unread ? Color.init(ColorConstantsName.NotificationColour) : Color.clear).listRowBackground(Color.clear)
        .onReceive(self.dtrData.$profile) { getLatestNotification in
            if notificationInfo.message.contains("following you") {
                titleStatus = dtrData.followerRelationship(profileID: notificationInfo.source) == .following ? "Following" : "Follow"
                if dtrData.profile.friends.contains(notificationInfo.source) {
                    titleStatus = "Friends"
                }
            }
        }
    }
    
    func getStatusOfUser(userId:String) -> String {
        var status = "Follow"
        if dtrData.relationship(profileID: userId).rawValue == "following" {
            status = "Request Friend"
        }else if dtrData.relationship(profileID: userId).rawValue == "follwoingWithInvite" {
            status = "Friend request sent"
        }else if dtrData.relationship(profileID: userId).rawValue == "requestedFriend" {
            status = "requestedFriend"
        }
        DispatchQueue.main.async {
            titleStatus = status
        }
        return status
    }
    
    func callFuntionAccToStatus(userId:String){
        if titleStatus == "Follow" {
            showActivityIndicator = true
            self.dtrCommands.profileFollow(profileID: userId){ respose in
                respose.debug(methodName:"profileFollow")
                if respose.feedback.contains("already friends") {
                    titleStatus = "Following"
                    showActivityIndicator = false
                } else {
                    titleStatus = (respose.result == .error) ? respose.feedback : "Following"
                    showActivityIndicator = false
                }
                
            }
        }else if titleStatus == "Request Friend" {
            showActivityIndicator = true
            self.dtrCommands.sendFriendRequest(profileID: userId){ respose in
                respose.debug(methodName:"sendFriendRequest")
                showActivityIndicator = false
            }
        }
    }
}
