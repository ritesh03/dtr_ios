//
//  FriendRequestNotificationView.swift
//  dtr-ios-test
//
//  Created by Mobile on 10/03/21.
//

import SwiftUI
import Firebase

struct FriendRequestNotificationView: View {
    
    @Binding var comesFromSheetFriendInvite:Bool
    @Binding var updateFriendRequestStatus:Bool
    var notificationInfo :AppNotification
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State var showRequestrProfileScreen = false
    
    var body: some View {
        VStack {
            HStack(alignment:.top) {
                ProfileHead(profile: dtrData.profileInfo(profileID: notificationInfo.source),size: .medium, comeFromNotificationScreen:true)
                    .onTapGesture {
                        showRequestrProfileScreen = true
                    }
                VStack(alignment: .leading, spacing: 10){
                    Group{
                        Text(self.dtrData.profileInfo(profileID: notificationInfo.source).name).font(.system(size: 17.0)) + Text("  sent you a friend request").font(.system(size: 14.0))
                    } .foregroundColor(.gray)
                    Text(self.dtrData.profileInfo(profileID: notificationInfo.source).description)
                        .foregroundColor(.gray)
                        .lineLimit(1)
                        .padding(.bottom,10)
                    HStack {
                        CustomButtonCircleWithCustomProperty(btnTitle: "Accept", btnWidth: 90, btnHeight:10, backgroundColord: false, txtColorAccent: true, strokeColor: true) {
                            if comesFromSheetFriendInvite {
                                updateFriendRequestStatus = true
                            }
                            updateFriendRequestStatusMethod(indexType: 0)
                        }.buttonStyle(BorderlessButtonStyle())
                        CustomButtonCircleWithCustomProperty(btnTitle: "Ignore", btnWidth: 90, btnHeight:10, backgroundColord: false, txtColorAccent: false, strokeColor: false){
                            if comesFromSheetFriendInvite {
                                updateFriendRequestStatus = true
                            }
                            updateFriendRequestStatusMethod(indexType:1)
                        }.buttonStyle(BorderlessButtonStyle())
                    }
                }
                .frame(maxWidth: .infinity ,alignment: .topLeading)
            }
            .padding(.all,15)
            NavigationLink(destination: ProfileScreen(updatedDismissView:.constant(false),day: .constant(0),userProfile: .constant(self.dtrData.profileInfo(profileID: notificationInfo.source)), notificationInfo:notificationInfo, showBackButtonOrNot:true), isActive: $showRequestrProfileScreen){
            }.opacity(0.0)
            Divider()
                .background(Color.white)
                .offset(x: 0, y:0)
        }.background(notificationInfo.status == .unread ? Color.init(ColorConstantsName.NotificationColour) : Color.clear).listRowBackground(Color.clear)
    }
    
    func updateFriendRequestStatusMethod(indexType:Int){
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            if indexType == 0 {
                self.dtrCommands.createFriendShipRelation(profileID: notificationInfo.source) { (result) in
                    if result.result == .ok {
                        Analytics.logEvent(AnalyticsEvent.notificationAccepted.rawValue, parameters: ["notificationID": notificationInfo.id, "type": notificationInfo.type.data])
                        self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":notificationInfo.payloadID,"source":notificationInfo.source,"type":notificationInfo.type.rawValue,"target":notificationInfo.target]) { (result) in
                        }
                    }
                }
            }else{
                Analytics.logEvent(AnalyticsEvent.notificationCancelled.rawValue, parameters: ["notificationID": notificationInfo.id, "type": notificationInfo.type.data])
                
                self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":notificationInfo.payloadID,"source":notificationInfo.source,"type":notificationInfo.type.rawValue,"target":notificationInfo.target]) { (result) in
                    result.debug(methodName: "clearNotifcationFromServer")
                }
            }
        }
    }
}

struct FriendRequestNotificationView_Previews: PreviewProvider {
    static var previews: some View {
        FriendRequestNotificationView(comesFromSheetFriendInvite: .constant(false), updateFriendRequestStatus: .constant(false), notificationInfo: AppNotification.init(data: asDataDict([""])))
    }
}
