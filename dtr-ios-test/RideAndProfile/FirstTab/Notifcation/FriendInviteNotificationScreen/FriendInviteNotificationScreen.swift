//
//  FriendInviteNotificationScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 14/04/21.
//

import SwiftUI

struct FriendInviteNotificationScreen: View {
    
    @Binding var comesFromSheetFriendInvite : Bool
    @Binding var updateFriendRequestStatus : Bool
    @Binding var showActivityIndicator : Bool
    @Binding var friendInviteArray : [AppNotification]
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var arrayForDeletenotifications : [AppNotification] = []
    
    
    var body: some View {
        Group {
            if self.friendInviteArray.count > 0{
                SectionTitleViewUI(titleString: "FRIEND REQUESTS", rightTitleString: "", countShow:  0) {}
                ForEach(self.friendInviteArray.indices, id: \.self) { notification in
                    FriendRequestNotificationView(comesFromSheetFriendInvite:$comesFromSheetFriendInvite,updateFriendRequestStatus:$updateFriendRequestStatus,notificationInfo: self.friendInviteArray[notification])
                        .listRowBackground(Color.clear)
                }.onDelete(perform: deleteReceivedNotification)
            }
        }
        .navigationBarHidden(false)
    }
    
    func deleteReceivedNotification(at offsets: IndexSet) {
        showActivityIndicator = true
        for index in offsets {
            arrayForDeletenotifications.removeAll()
            arrayForDeletenotifications.append(friendInviteArray[index])
            friendInviteArray.remove(at: index)
        }
        
        if arrayForDeletenotifications.count > 0 {
            self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":arrayForDeletenotifications[0].payloadID,"source":arrayForDeletenotifications[0].source,"type":arrayForDeletenotifications[0].type.rawValue,"target":arrayForDeletenotifications[0].target]) { (result) in
                showActivityIndicator = false
            }
        }
    }
}
