//
//  MyRidesTabScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/01/21.
//

import SwiftUI
import Firebase

class UserSettings: ObservableObject {
    @Published var scrollViewContentOffset = CGFloat(0)
}

enum ActiveSheetForFullScreen: Identifiable {
    case rideDetailScreen ,notificationScreen, groupDetailScreen
    var id: Int {
        hashValue
    }
}


struct MyRidesTabScreen: View {
    
    @Binding var day: Int
    @Binding var views: [HostingController<DayContainerView>]
    @EnvironmentObject var settings: UserSettings
    @State private var showRequestOpenProfileScreen = false
    @State private var scrollViewContentOffset = CGFloat(0)
    @State private var receivedNotification = AppNotification.default
    @State private var ride: DtrRide = DtrRide(profileID: "NOT", dateCode: "NOT", summary: "Not")
    @State private var hasRide = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var activeSheetForScreen: ActiveSheetForFullScreen?
    @State private var reachableByNetwork = true
    @State private var updatedVaribleValues = false
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.init(ColorConstantsName.MainThemeBgColour)
                    .edgesIgnoringSafeArea(.all)
                VStack {
                    if settings.scrollViewContentOffset <= 80  {
                        HomeScreenTopView(currentDay: self.$day)
                            .offset(x: 0, y: -settings.scrollViewContentOffset)
                            .padding(.bottom , -settings.scrollViewContentOffset)
                    } else {
                        PageControl(numberOfPages: 7, currentPage: self.$day)
                            .frame(width: CGFloat(7 * 18))
                            .padding(.trailing)
                    }
                    PageViewController(pages: views, currentPage: $day).padding(.leading,-3)
                }
            }
            
            .onReceive(dtrData.network.$reachable) { value in
                self.reachableByNetwork = value
            }
            
            .onAppear {
                localDyanamicLinkForFirstTime()
            }
            
            .navigationBarTitle("")
            .navigationBarHidden(true)
        }
        
        .onReceive(self.dtrData.$receivedNotification) { notification in
            if let notification = notification {
                self.receivedNotification = notification
                switch notification.type {
                case .rideInvite, .rideTime,.rideLocation,.rideUpdate:
                    if  receivedNotification.type != .unknown {
                        if let range = notification.payloadID.range(of:  "+") {
                            let phone = notification.payloadID[range.upperBound...]
                            self.day = toOffset(dateCode: String(phone))
                        }
                        var rideid = notification.payloadID
                        if notification.payloadID.count < 9 {
                            rideid =  "\(notification.source)+\(notification.payloadID)"
                        }
                        
                        if let dotRange = rideid.range(of: "+") {
                            rideid.removeSubrange(dotRange.lowerBound..<rideid.endIndex)
                        }
                        UserStore.shared.isComeFromDyanamic = true
                        DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                            if self.dtrData.dyanamicLinkRide.id != "RIDER_ID" &&  self.dtrData.dyanamicLinkRide.summary !=  "Test" {
                                self.ride = self.dtrData.dyanamicLinkRide
                                self.hasRide = true
                                print(ride)
                                activeSheetForScreen = .rideDetailScreen
                                self.receivedNotification = AppNotification.default
                                self.dtrData.receivedNotification = AppNotification.default
                                self.dtrData.dyanamicLinkRide = DtrRide(profileID: "RIDER_ID", dateCode: "1912542", summary: "Test")
                                print(ride)
                            }else {
                                self.dtrData.upcomingRideWithSourceCode(profileID: rideid, dateCode: notification.payloadID) { ride in
                                    if let ride = ride {
                                        DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                                            self.hasRide = true
                                            self.ride = ride
                                            activeSheetForScreen = .rideDetailScreen
                                            self.dtrData.receivedNotification = AppNotification.default
                                        }
                                    } else {
                                        self.dtrData.upcomingRideWithId(profileID: rideid, dateCode: notification.payloadID) { ride in
                                            if let ride = ride {
                                                DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                                                    self.ride = ride
                                                    self.hasRide = true
                                                    activeSheetForScreen = .rideDetailScreen
                                                    self.receivedNotification = AppNotification.default
                                                    self.dtrData.receivedNotification = AppNotification.default
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                case .rideCancelled, .friendMessage,.friendInvite,.follow,.message:
                    if  receivedNotification.type != .unknown {
                        if receivedNotification.message == "Friend Profile Link" {
                            if receivedNotification.source != dtrData.profile.id {
                                showRequestOpenProfileScreen = true
                            }
                        } else if receivedNotification.message == "Group_Detail" {
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                                activeSheetForScreen = .groupDetailScreen
                            }
                        } else {
                            DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                                if activeSheetForScreen != .groupDetailScreen {
                                    activeSheetForScreen = .notificationScreen
                                }
                            }
                        }
                    }
                case .poke:
                    if  receivedNotification.payloadID == ForPokeNotificationString.PayloadGoToWeekend {
                        Analytics.logEvent(AnalyticsScreen.poke_go_dtr_weekend_launch.rawValue, parameters: ["source":dtrData.profile.id,"type":receivedNotification.payloadID])
                        callMethodAccordingToNotifcation(dayIndex: 0)
                    }
                    
                    else if  receivedNotification.payloadID == ForPokeNotificationString.PayloadJoinSaturday {
                        Analytics.logEvent(AnalyticsScreen.poke_join_weekend_launch.rawValue, parameters: ["source":dtrData.profile.id,"type":receivedNotification.payloadID])
                        callMethodAccordingToNotifcation(dayIndex: 1)
                    }
                    
                    else if  receivedNotification.payloadID == ForPokeNotificationString.PayloadJoinSunday {
                        Analytics.logEvent(AnalyticsScreen.poke_join_weekend_launch.rawValue, parameters: ["source":dtrData.profile.id,"type":receivedNotification.payloadID])
                        callMethodAccordingToNotifcation(dayIndex: 0)
                    }
                    
                    else if  receivedNotification.payloadID == ForPokeNotificationString.PayloadComeBack {
                        Analytics.logEvent(AnalyticsScreen.poke_go_dtr_weekend_launch.rawValue, parameters: ["source":dtrData.profile.id,"type":receivedNotification.payloadID])
                        callMethodAccordingToNotifcation(dayIndex: 2)
                    }
                    
                case .groupJoin,.groupLeadernotify:
                    activeSheetForScreen = .groupDetailScreen
                default:
                    self.receivedNotification = AppNotification.default
                }
            } else {
                self.receivedNotification = AppNotification.default
            }
        }
        
        .onReceive(self.dtrData.$myRidesByDate) { rideDict in
            if let rideDetail = rideDict[dateCode(offset: day)] {
                if rideDetail.id == dtrData.profile.id {
                    self.ride = DtrRide.default
                    if activeSheetForScreen == .rideDetailScreen {
                        self.ride = rideDetail
                    }
                }
            }
        }
        
        .fullScreenCover(item: $activeSheetForScreen) { item in
            switch item {
            case .rideDetailScreen:
                NavigationView {
                    RideDetail(day:.constant(0), myRideForDay: $ride,receivedNotification:receivedNotification,
                               rideFromNotifcaitonScreen:ride,rideFromNotifcaitonBool:true,rideFromPushNotification:false,presentModelOldHistoryScreen: .constant(false), myRideForDay1: RideEditViewModel.init(ride: ride))
                        .onAppear{
                            Analytics.logEvent(AnalyticsScreen.rideDetails.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
                        }
                        .onDisappear {
                            if !UserStore.shared.isComeFromDyanamic {
                                ride = DtrRide.default
                                activeSheetForScreen = nil
                                self.receivedNotification = AppNotification.default
                                self.dtrData.receivedNotification = AppNotification.default
                            }
                        }
                }.accentColor(.white)
            case .notificationScreen:
                NotificationsScreen(updatedDismissView:$updatedVaribleValues,day: $day,comesFromSheet:true)
                    .onAppear {
                        Analytics.logEvent(AnalyticsScreen.notification.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modalNav.rawValue])
                    }
                    .onDisappear{
                        activeSheetForScreen = nil
                        self.receivedNotification = AppNotification.default
                        self.dtrData.receivedNotification = AppNotification.default
                    }
            case .groupDetailScreen:
                NavigationView {
                    GroupDetailScreen(dismissView: .constant(false),comeFromNotificaionScreen:true, groupDetailEdit:GroupEditViewModel.init(group: self.dtrData.groupsDirectory[dynamicMember: receivedNotification.payloadID]))
                        .onDisappear{
                            self.receivedNotification = AppNotification.default
                            self.dtrData.receivedNotification = AppNotification.default
                        }
                }
            }
        }
        
        .background(EmptyView().sheet(isPresented: self.$showRequestOpenProfileScreen) {
            Modal(isPresented: self.$showRequestOpenProfileScreen, title:self.receivedNotification.type.label) {
                ProfileScreen(updatedDismissView:$updatedVaribleValues,day: $day, userProfile: .constant(self.dtrData.profileInfo(profileID: receivedNotification.source)), showPopUpForSendRequestFollow:receivedNotification.source != dtrData.profile.id ? true : false, showBackButtonOrNot:true)
            }
            .onAppear{
                Analytics.logEvent(AnalyticsScreen.notification.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modalNav.rawValue])
            }
            .onDisappear{
                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                    if updatedVaribleValues {
                        self.receivedNotification = AppNotification.default
                        self.dtrData.receivedNotification = AppNotification.default
                    }
                }
            }
        })
        
        .onAppear{
            Analytics.logEvent(AnalyticsScreen.notification.rawValue, parameters: [AnalyticsParameterScreenName: AnalyticsScreen.modalNav.rawValue])
            updateFCMToken()
        }
    }
    
    
    func callMethodAccordingToNotifcation(dayIndex:Int) {
        if dayIndex == 0 {
            let dateCodeSun = getSunDatCode()
            let dateCodeSat = getSatDatCode()
            let indexSat = getIndexSunDatCode()
            day = indexSat
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                if dtrData.myRidesByDate[dateCodeSun] == nil {
                    DtrCommand.sharedInstance.rideCreateWithDefaultValue(defaultRideInfo: DtrRide.default, dateCode: dateCodeSun) { response in
                    }
                }
                
                if dtrData.myRidesByDate[dateCodeSat] == nil {
                    DtrCommand.sharedInstance.rideCreateWithDefaultValue(defaultRideInfo: DtrRide.default, dateCode: dateCodeSat) { response in
                    }
                }
            }
        }else if dayIndex == 1 {
            let dateCode = getSatDatCode()
            let indexSat = getIndexSatDatCode()
            day = indexSat
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                if dtrData.myRidesByDate[dateCode] == nil {
                    DtrCommand.sharedInstance.rideCreateWithDefaultValue(defaultRideInfo: DtrRide.default, dateCode: dateCode) { response in
                    }
                }
            }
        }else if dayIndex == 2 {
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                var counts: [Int: Int] = [:]
                var dateCode1 = ""
                for i in 0...6 {
                    if let rides = self.dtrData.suggestedRidesByDate[dateCode(offset:i)] {
                        counts[i] = rides.count
                    }
                }
                
                if let mzxValue = counts.values.max() {
                    for i in 0...6 {
                        if counts[i] == mzxValue {
                            dateCode1 = dateCode(offset: i)
                            day = i
                            break
                        }
                    }
                    if dtrData.myRidesByDate[dateCode1] == nil {
                        DtrCommand.sharedInstance.rideCreateWithDefaultValue(defaultRideInfo: DtrRide.default, dateCode: dateCode1) { response in
                            response.debug(methodName: "rideCreateWithDefaultValue")
                        }
                    }
                }
            }
        }
    }
    
    
    
    func updateFCMToken(){
        // Update Firebase token because some time fcm token not updating through login.
        if UserStore.shared.updateOnlyOneTimeFCMToken {
            if let token = Messaging.messaging().fcmToken {
                if let currentToken = dtrData.profile.pushToken {
                    if currentToken != token {
                        dtrData.updatePushToken(token: token)
                    }else{
                        dtrData.updatePushToken(token: token)
                    }
                } else {
                    dtrData.updatePushToken(token: token)
                }
            }
            UserStore.shared.updateOnlyOneTimeFCMToken = false
        }
    }
    
    func localDyanamicLinkForFirstTime(){
        if UserStore.shared.isFirstTimeUserRegsiter {
            DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
                let result1 = UserStore.shared.setDyanamicLinkLocal(type: "", source: "", data: "", isSetValue: false)
                if result1.0 != "" && result1.1 != "" {
                    UserStore.shared.isComeFromDyanamic = true
                    if result1.0 == "ride" {
                        self.dtrData.processRideLink(type: result1.0 , source: result1.1, rideID: result1.2)
                    }else if result1.0 == "friend" {
                        self.dtrData.processFriendLink(type: result1.0 , source: result1.1)
                    }
                    UserDefaults.standard.removeObject(forKey: "Save_Dyanamic_Link_Local")
                }
            }
        }
    }
}
