//
//  DayContainerView.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/01/21.
//

import SwiftUI
import Combine
import Firebase

struct DayContainerView: View {
    
    @Binding var day: Int
    @State private var shareRideDetail = false
    @State private var myRideForDay : DtrRide?
    
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    
    @State private var offset = CGFloat.zero
    
    @State private var arrayCustom:[String] = [""]
    @State private var showRideDetailScreen = false
    @State private var showAlertNotDTR = false
    
    @State private var hidePopupEveryTime = false
    @State var presentModelOldHistoryScreen = false
    @State private var errorMesgCountCheck = "\n Message must be less than 300 characters"
    @State private var errorMesgExceedCount = false
    
    
    //MARK:- Ride varible's
    @State  private var allRidesToDisplay = [DtrRide]()
    @State  private var friendsRidesToDisplay = [DtrRide]()
    @State  private var followerAndGroupRidesToDisplay = [DtrRide]()
    @State  private var publicRidesToDisplay = [DtrRide]()
    
    //MARK:- Public Ride varible's
    @State private var startPlaceForShow = CommonAllString.BlankStr
    @State private var startCountryForShow = CommonAllString.BlankStr
    @State private var startPlaceLatForShow = 0.0
    @State private var startPlaceLonForShow = 0.0
    @State private var isPublicRideLocationAvailable = false
    @State private var showTimrScreen = false
    
    //MARK:- Chat varible's
    @State private var showMainChatScreen = false
    @State private var totalCountUnreadMesg = 0
    
    @State var message: DtrRideMessage = DtrRideMessage(sender: "", message: "")
    @State var messages =  [DtrRideMessage]()
    
    @State var presentingModal = false

    var senderIsMe: Bool {
        self.message.sender == self.dtrData.profile.id
    }
    
    var sender: DtrProfile {
        if senderIsMe {
            return self.dtrData.profile
        }
        return self.dtrData.profileDirectory[dynamicMember: self.message.sender]
    }
    
    let timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "hh:mm a"
        return formatter
    }()
    
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
            
            if myRideForDay == nil || myRideForDay?.summary.lowercased() == "Down To Ride".lowercased() {
                GeometryReader { geo in
                    ScrollView {
                        VStack {
                            VStack(spacing: 0) {
                                Image(ImageConstantsName.MainCoverBgImg)
                                    .resizable()
                                    .scaledToFill()
                                    .frame(width:UIScreen.screenWidth-30,height: 120, alignment: .center)
                                    .clipped()
                                    .cornerRadius(20, corners: [.topLeft, .topRight])
                                    .padding(.bottom)
                                
                                Button(action: {
                                    presentingModal = true
                                }) {
                                    Text("LEAD A RIDE")
                                        .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: 50, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealHeight: 44, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                        .foregroundColor(.white)
                                        .background(Color.init(ColorConstantsName.AccentColour))
                                        .cornerRadius(UIScreen.screenWidth*0.8/2)
                                        .padding(EdgeInsets(top: 0, leading: 20, bottom: 20, trailing: 20))
                                }
                            }
                            .onAppear() {
                                print("Summary ---->", myRideForDay?.summary.lowercased())
                            }
                            ShowOtherRidesScreen(allRidesToDisplay: $allRidesToDisplay, friendsRidesToDisplay: $friendsRidesToDisplay, othersRidesToDisplay: $followerAndGroupRidesToDisplay,publicRidesToDisplay:$publicRidesToDisplay, isPublicRideLocAvaible: $isPublicRideLocationAvailable,currentRideUserDay:$myRideForDay.toUnwrapped(defaultValue: DtrRide.default),day: $day, startPlaceLat:startPlaceLatForShow,startPlaceLon:startPlaceLonForShow, startPlaceForShow: $startPlaceForShow, startCountryForShow: $startCountryForShow, startPlaceLatForShow: $startPlaceLatForShow, startPlaceLonForShow: $startPlaceLonForShow)
                            
                            DayContainerExtraViewForChat(day: $day)
                        }
                        .hiddenNavigationBarStyle()
                        .background(GeometryReader {
                            Color.clear.preference(key: ViewOffsetKey.self,value: -$0.frame(in: .named("scroll")).origin.y)
                        }).onPreferenceChange(ViewOffsetKey.self) {_ in
                            //print("offset >> \($0)")
                        }
                    }.coordinateSpace(name: "scroll")
                }
                
                if showTimrScreen {
                    TimeEditScreen(isUpdateRideTime: .constant(false), showTimrScreen: $showTimrScreen, myRideForDay: RideEditViewModel.init(ride:myRideForDay!))
                }
                
                BottomChatViewMini(day: $day, totalCountUnreadMesg: $totalCountUnreadMesg, showMainChatScreen: $showMainChatScreen, sender: .constant(sender), message: $message)
                
                    .fullScreenCover(isPresented: $presentModelOldHistoryScreen){
                        CreateRideScreen(day: $day, presentedAsModal: self.$presentModelOldHistoryScreen, isComeFromLeaveRide: true, afterLeaveRideDefaultRide: myRideForDay!)
                    }
                
                    .background(EmptyView().sheet(isPresented: self.$shareRideDetail) {
                        Modal(isPresented: self.$shareRideDetail, title: DayContainerString.ShareRideStr,isShareRideScreen: true) {
                            ShareScreen(isPresented: self.$shareRideDetail, ride: self.myRideForDay ?? DtrRide.default)
                        }.onAppear{
                            Analytics.logEvent(AnalyticsScreen.rideLink.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
                        }
                    })
                
                
                
                if dtrData.myRidesByDate[dateCode(offset: self.day)] != nil {
                    NavigationLink(destination: ChatScreenList(totalCountUnreadMesg: $totalCountUnreadMesg, dateCode: dateCode(offset: day), myRideForDay: dtrData.myRidesByDate[dateCode(offset: self.day)]!), isActive: $showMainChatScreen){
                        EmptyView()
                    }
                    
                }
            } else {
                GeometryReader { geo in
                    ScrollView {
                        VStack {
                            if myRideForDay!.details.tags.contains("KfqH2PdeUY4nyp9V2nPQ") {
                                DayConatinerHomeCustomView(day: $day, myRideForDay: $myRideForDay.toUnwrapped(defaultValue: DtrRide.default), shareRideDetail: $shareRideDetail, hidePopupEveryTime: $hidePopupEveryTime, widthScreen: .constant(geo.size.width), showTimrScreen: $showTimrScreen, arrayCustom: $arrayCustom, showAlertNotDTR: $showAlertNotDTR, presentModelOldHistoryScreen: $presentModelOldHistoryScreen, showRideDetailScreen: $showRideDetailScreen)
                            } else {
                                DayConatinerRideCenterView(day: $day, myRideForDay: $myRideForDay, shareRideDetail: $shareRideDetail, hidePopupEveryTime: $hidePopupEveryTime, widthScreen: .constant(geo.size.width), showTimrScreen: $showTimrScreen, arrayCustom: $arrayCustom, showAlertNotDTR: $showAlertNotDTR, presentModelOldHistoryScreen: $presentModelOldHistoryScreen, showRideDetailScreen: $showRideDetailScreen)
                            }
                            
                            ShowOtherRidesScreen(allRidesToDisplay: $allRidesToDisplay, friendsRidesToDisplay: $friendsRidesToDisplay, othersRidesToDisplay: $followerAndGroupRidesToDisplay,publicRidesToDisplay:$publicRidesToDisplay, isPublicRideLocAvaible: $isPublicRideLocationAvailable,currentRideUserDay:$myRideForDay.toUnwrapped(defaultValue: DtrRide.default),day: $day, startPlaceLat:startPlaceLatForShow,startPlaceLon:startPlaceLonForShow, startPlaceForShow: $startPlaceForShow, startCountryForShow: $startCountryForShow, startPlaceLatForShow: $startPlaceLatForShow, startPlaceLonForShow: $startPlaceLonForShow)
                            
                            DayContainerExtraViewForChat(day: $day)
                        }
                        .hiddenNavigationBarStyle()
                        .background(GeometryReader {
                            Color.clear.preference(key: ViewOffsetKey.self,value: -$0.frame(in: .named("scroll")).origin.y)
                        }).onPreferenceChange(ViewOffsetKey.self) {_ in
                            //print("offset >> \($0)")
                        }
                    }.coordinateSpace(name: "scroll")
                }
                
                if showTimrScreen {
                    TimeEditScreen(isUpdateRideTime: .constant(false), showTimrScreen: $showTimrScreen, myRideForDay: RideEditViewModel.init(ride:myRideForDay!))
                }
                
                BottomChatViewMini(day: $day, totalCountUnreadMesg: $totalCountUnreadMesg, showMainChatScreen: $showMainChatScreen, sender: .constant(sender), message: $message)
                
                    .fullScreenCover(isPresented: $presentModelOldHistoryScreen){
                        CreateRideScreen(day: $day, presentedAsModal: self.$presentModelOldHistoryScreen, isComeFromLeaveRide: true, afterLeaveRideDefaultRide: myRideForDay!)
                    }
                
                    .background(EmptyView().sheet(isPresented: self.$shareRideDetail) {
                        Modal(isPresented: self.$shareRideDetail, title: DayContainerString.ShareRideStr,isShareRideScreen: true) {
                            ShareScreen(isPresented: self.$shareRideDetail, ride: self.myRideForDay ?? DtrRide.default)
                        }.onAppear{
                            Analytics.logEvent(AnalyticsScreen.rideLink.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
                        }
                    })
                
                
                
                if dtrData.myRidesByDate[dateCode(offset: self.day)] != nil {
                    NavigationLink(destination: ChatScreenList(totalCountUnreadMesg: $totalCountUnreadMesg, dateCode: dateCode(offset: day), myRideForDay: dtrData.myRidesByDate[dateCode(offset: self.day)]!), isActive: $showMainChatScreen){
                        EmptyView()
                    }
                    
                    NavigationLink(destination: RideDetail(day:$day, myRideForDay: $myRideForDay.toUnwrapped(defaultValue: DtrRide.default),presentModelOldHistoryScreen: $presentModelOldHistoryScreen,myRideForDay1: RideEditViewModel.init(ride: myRideForDay!)), isActive: $showRideDetailScreen){
                        EmptyView()
                    }
                }
            }
        }
        .sheet(isPresented: $presentingModal) {
            CreateRideScreen(day: $day, presentedAsModal: self.$presentingModal, isComeFromLeaveRide: false, afterLeaveRideDefaultRide: DtrRide.default)
        }
        .hiddenNavigationBarStyle()
        
        .onAppear {
            getLocalDataForPublicRide(dateCodeStr: dateCode(offset: day))
            Analytics.logEvent(dtrData.profileID+dateCode(offset: day), parameters: [AnalyticsParameterScreenName:AnalyticsScreen.main.rawValue])
            UserStore.shared.registrationInProgress1 = 00011
            UserStore.shared.updateOnlyOneTimeForINviteAndShare = false
            getFriendAndFollowersRides()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                print("Summary On Appear---->", myRideForDay?.summary.lowercased())
                if myRideForDay == nil {
                    createSimpleRideWithVisibliCheck()
                }
            }
        }
        
        .onDisappear {
            hidePopupEveryTime = false
        }
        
        .onReceive(self.dtrData.$suggestedRidesByDate) { ridesForDate in
            if let rides = ridesForDate[dateCode(offset: self.day)] {
                //self.allRidesToDisplay.removeAll()
                self.allRidesToDisplay = rides.map{$0}
                self.getFriendAndFollowersRides()
            } else {
                self.allRidesToDisplay = []
                friendsRidesToDisplay  = []
                followerAndGroupRidesToDisplay  = []
            }
            
            checkUnreadMesgCount()
            
            updateThreeDotFunctionality()
            
            getLastMesgIfAvalible(dayUpdated:day)
        }
        
        .onReceive(self.dtrData.$allRidesForWeek) { ridesForDate in
            getLocalDataForPublicRide(dateCodeStr: dateCode(offset: self.day))
            
            getLastMesgIfAvalible(dayUpdated:day)
            
            self.getFriendAndFollowersRides()
        }
        
        .onReceive(self.dtrData.$myRidesByDate) { rideDict in
            self.myRideForDay = rideDict[dateCode(offset: day)]
            print("Summary On Receive ---->", myRideForDay?.summary.lowercased())
            checkUnreadMesgCount()
            
            updateThreeDotFunctionality()
            
            getLastMesgIfAvalible(dayUpdated:day)
            
            getLocalDataForPublicRide(dateCodeStr: dateCode(offset: self.day))
        }
        
        
        .onReceive(self.dtrData.$deletedRides, perform: { deltdRidesId in
            let deleedsRides = deltdRidesId as [String]
            for i in 0..<deleedsRides.count {
                if let rideIndex = publicRidesToDisplay.firstIndex(where: { $0.id == deleedsRides[i] }) {
                    publicRidesToDisplay.remove(at: rideIndex)
                }
            }
        })
        
        .onChange(of: self.showAlertNotDTR) { updatedShowAlertStatus in
            if updatedShowAlertStatus ==  true {
                showAlertNotDTR = false
                alertView()
            }
        }
    }
    
    func createSimpleRideWithVisibliCheck() {
        var dictCreate = [String:Any]()
        if dtrData.profile.defaultRideVisibility != "" {
            dictCreate = ["_geoloc":["lat":DtrRide.default.details.startPlaceLat,"lng":DtrRide.default.details.startPlaceLon],"summary": "Down to Ride","rideCoverImage":DtrRide.default.rideCoverImage,"details":["tags":DtrRide.default.details.tags,"hasStartPlace":DtrRide.default.details.hasStartPlace,"hasStartTime":DtrRide.default.details.hasStartTime,"notes":DtrRide.default.details.notes,"skillA":DtrRide.default.details.skillA,"skillB":DtrRide.default.details.skillB,"skillC":DtrRide.default.details.skillC,"skillD":DtrRide.default.details.skillD,"skillS":DtrRide.default.details.skillS,"startPlace":DtrRide.default.details.startPlace,"startPlaceLat":DtrRide.default.details.startPlaceLat,"startPlaceLon":DtrRide.default.details.startPlaceLon,"startTime":DtrRide.default.details.startTime,"visibility":dtrData.profile.defaultRideVisibility]] as [String : Any]
        } else {
            dictCreate = ["_geoloc":["lat":DtrRide.default.details.startPlaceLat,"lng":DtrRide.default.details.startPlaceLon],"summary": "Down to Ride","rideCoverImage":DtrRide.default.rideCoverImage,"details":["tags":DtrRide.default.details.tags,"hasStartPlace":DtrRide.default.details.hasStartPlace,"hasStartTime":DtrRide.default.details.hasStartTime,"notes":DtrRide.default.details.notes,"skillA":DtrRide.default.details.skillA,"skillB":DtrRide.default.details.skillB,"skillC":DtrRide.default.details.skillC,"skillD":DtrRide.default.details.skillD,"skillS":DtrRide.default.details.skillS,"startPlace":DtrRide.default.details.startPlace,"startPlaceLat":DtrRide.default.details.startPlaceLat,"startPlaceLon":DtrRide.default.details.startPlaceLon,"startTime":DtrRide.default.details.startTime,"visibility":"followers"]] as [String : Any]
        }
        
        DtrCommand.sharedInstance.rideCreate(dateCode: dateCode(offset: day), data: dictCreate){ result in
            result.debug(methodName:"rideCreate")
        }
    }
    
    
    
    func getLastMesgIfAvalible(dayUpdated:Int){
        hidePopupEveryTime = false
        
        if self.myRideForDay != nil {
            if self.myRideForDay?.chat.messages.count ?? 0 > 0 {
                message = (self.myRideForDay?.chat.messages.last)!
            }else{
                message =  DtrRideMessage(sender: "", message: "")
            }
        }
        
        messages.removeAll()
        messages = getLatestMesg(updatedDay: dayUpdated)
    }
    
    
    func getLatestMesg(updatedDay:Int)-> [DtrRideMessage] {
        let plan = dtrData.plansMessage[dynamicMember: dateCode(offset: updatedDay)]
        if let ride = plan.ride {
            return ride.chat.messages
        }
        return [DtrRideMessage]()
    }
    
    
    func getLocalDataForPublicRide(dateCodeStr:String) {
        let dict = UserStore.shared.saveDateCodeWithUserLocation(city: CommonAllString.BlankStr, country: CommonAllString.BlankStr, lat: CommonAllString.BlankStr, long: CommonAllString.BlankStr, dateCode: dateCodeStr,isSetData:false)
        
        if dict.0 != CommonAllString.BlankStr && dict.1 != CommonAllString.BlankStr && dict.2 != CommonAllString.BlankStr && dict.3 != CommonAllString.BlankStr {
            startPlaceForShow = dict.0
            startCountryForShow = dict.1
            startPlaceLatForShow = Double(dict.2) ?? 0.0
            startPlaceLonForShow = Double(dict.3) ?? 0.0
            isPublicRideLocationAvailable = true
        } else {
            startPlaceForShow = dtrData.profile.cityState
            startCountryForShow = dtrData.profile.country
            startPlaceLatForShow = dtrData.profile.userLat
            startPlaceLonForShow = dtrData.profile.userLon
            isPublicRideLocationAvailable = false
        }
        
        if dtrData.profile.userLon == 0.0 && dtrData.profile.userLon == 0.0 {
            isPublicRideLocationAvailable = false
        }
        
        dtrCommands.getPublicRides(dateCode: dateCode(offset: day), currentlat: CGFloat(startPlaceLatForShow), currentlon: CGFloat(startPlaceLonForShow)){ response in
            if let responseRide = response.output["publicRides"] as? DataDict {
                if let pblRideAry = responseRide["hits"] as? NSArray {
                    var publicRideIds = [String]()
                    for i in 0..<pblRideAry.count {
                        let rideDict = asDataDict(pblRideAry[i])
                        let dataDict = ["id":rideDict["id"]!,"leader":rideDict["leader"]!,"summary":rideDict["summary"]!,"dateCode":rideDict["dateCode"]!,"details":["visibility":rideDict["visibility"]]] as [String:Any]
                        let rideDetail = DtrRide.init(data: dataDict)
                        if rideDetail.leader != dtrData.profile.id {
                            publicRideIds.append(rideDetail.id)
                        }
                    }
                    getPublicRidesById(ids: publicRideIds)
                }
            }
        }
    }
    
    func getPublicRidesById(ids:[String]) {
        for i in 0..<ids.count {
            getPublicRidesFromIds(publicRide: ids[i]) { tempPubcRide, allRidesArrys in
                publicRidesToDisplay = tempPubcRide
                allRidesToDisplay = allRidesArrys
                getFriendAndFollowersRides()
            }
        }
        getFriendAndFollowersRides()
    }
    
    
    func getPublicRidesFromIds(publicRide:String, completion: @escaping ([DtrRide],[DtrRide]) -> Void){
        var tempPublicRideArray = publicRidesToDisplay
        var tempAllRidesArray = allRidesToDisplay
        dtrData.getRideDetailForSpecificRide(rideID:publicRide) { rideDetail in
            if rideDetail != nil {
                // Check ride if allrides Contain the public ride
                if !tempAllRidesArray.contains(rideDetail!) {
                    if !rideDetail!.riders.contains(dtrData.profile.id) {
                        if dtrData.profile.friends.contains(rideDetail!.leader) || dtrData.profile.followed.contains(rideDetail!.leader) {
                            tempAllRidesArray.append(rideDetail!)
                        } else {
                            if let rideIndex = tempPublicRideArray.firstIndex(where: { $0.id == rideDetail!.id }) {
                                tempPublicRideArray.remove(at: rideIndex)
                                tempPublicRideArray.append(rideDetail!)
                            }else{
                                tempPublicRideArray.append(rideDetail!)
                            }
                        }
                    }else{
                        if rideDetail!.riders.contains(dtrData.profile.id) {
                            if let rideIndex = tempPublicRideArray.firstIndex(where: { $0.id == rideDetail!.id }) {
                                tempPublicRideArray.remove(at: rideIndex)
                            }
                        }
                    }
                }else{
                    //If public ride change the relationship in current session, then we use same function for updating the ride detail's, otherwise suggested ride updating automatically.
                    if tempAllRidesArray.count > 0 {
                        if let rideIndex = tempAllRidesArray.firstIndex(where: { $0.id == rideDetail!.id }) {
                            tempAllRidesArray.remove(at: rideIndex)
                            tempAllRidesArray.append(rideDetail!)
                        }
                    }
                    
                    if let rideIndex = tempPublicRideArray.firstIndex(where: { $0.id == rideDetail!.id }) {
                        tempPublicRideArray.remove(at: rideIndex)
                    }
                }
                completion(tempPublicRideArray,tempAllRidesArray)
            }
        }
    }
    
    
    
    
    func getFriendAndFollowersRides() {
        friendsRidesToDisplay.removeAll()
        followerAndGroupRidesToDisplay.removeAll()
        
        var tempAllRidesArray = allRidesToDisplay
        
        var tempfriendsRidesToDisplay = friendsRidesToDisplay
        var tempfollowerAndGroupRidesToDisplay = followerAndGroupRidesToDisplay
        
        var ridesSuggestedForDate = Set<DtrRide>()
        if let todayRides = dtrData.allUserRidesForWeek[dateCode(offset: day)] {
            ridesSuggestedForDate = todayRides
        }
        
        if tempAllRidesArray.count > 0 {
            for i in 0..<tempAllRidesArray.count {
                if tempAllRidesArray.indices.contains(i) {
                    if (tempAllRidesArray[i].leader != dtrData.profile.id && !tempAllRidesArray[i].riders.contains(dtrData.profile.id)) {
                        if dtrData.profile.friends.contains(tempAllRidesArray[i].leader) {
                            if !tempAllRidesArray[i].riders.contains(dtrData.profile.id) {
                                if let rideIndex = tempfriendsRidesToDisplay.firstIndex(where: { $0.id == tempAllRidesArray[i].id }) {
                                    tempfriendsRidesToDisplay.remove(at: rideIndex)
                                    tempfriendsRidesToDisplay.append(tempAllRidesArray[i])
                                }else{
                                    if ridesSuggestedForDate.contains(tempAllRidesArray[i]){
                                        tempfriendsRidesToDisplay.append(tempAllRidesArray[i])
                                    } else {
                                        if let rideIndex = tempAllRidesArray.firstIndex(where: { $0.id == tempAllRidesArray[i].id }) {
                                            tempAllRidesArray.remove(at: rideIndex)
                                        }
                                    }
                                }
                            }
                        } else {
                            if !tempAllRidesArray[i].riders.contains(dtrData.profile.id) {
                                if let rideIndex = tempfollowerAndGroupRidesToDisplay.firstIndex(where: { $0.id == tempAllRidesArray[i].id }) {
                                    tempfollowerAndGroupRidesToDisplay.remove(at: rideIndex)
                                    tempfollowerAndGroupRidesToDisplay.append(tempAllRidesArray[i])
                                }else{
                                    if ridesSuggestedForDate.contains(tempAllRidesArray[i]){
                                        tempfollowerAndGroupRidesToDisplay.append(tempAllRidesArray[i])
                                    }else{
                                        if let rideIndex = tempAllRidesArray.firstIndex(where: { $0.id == tempAllRidesArray[i].id }) {
                                            tempAllRidesArray.remove(at: rideIndex)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if tempfriendsRidesToDisplay.count > 0 {
                tempfriendsRidesToDisplay = tempfriendsRidesToDisplay.sorted(by: {
                    timeFormatter.date(from: $0.details.startTime) ?? Date.distantFuture <  timeFormatter.date(from: $1.details.startTime) ?? Date.distantFuture
                })
            }
            
            if tempfollowerAndGroupRidesToDisplay.count > 0 {
                tempfollowerAndGroupRidesToDisplay = tempfollowerAndGroupRidesToDisplay.sorted(by: {
                    timeFormatter.date(from: $0.details.startTime) ?? Date.distantFuture <  timeFormatter.date(from: $1.details.startTime) ?? Date.distantFuture
                })
            }
            
            friendsRidesToDisplay =  tempfriendsRidesToDisplay
            followerAndGroupRidesToDisplay =  tempfollowerAndGroupRidesToDisplay
            allRidesToDisplay =  tempAllRidesArray
            
            //print("friendsRidesToDisplay = ",friendsRidesToDisplay.count,tempfriendsRidesToDisplay.count,"\n","followerAndGroupRidesToDisplay = ",followerAndGroupRidesToDisplay.count,tempfollowerAndGroupRidesToDisplay.count,"\n","tempAllRidesArray = ",allRidesToDisplay.count,tempAllRidesArray.count)
        }
        
        if publicRidesToDisplay.count > 0 {
            publicRidesToDisplay = publicRidesToDisplay.sorted(by: {
                timeFormatter.date(from: $0.details.startTime) ?? Date.distantFuture <  timeFormatter.date(from: $1.details.startTime) ?? Date.distantFuture
            })
        }
    }
    
    
    func alertView() {
        let attributedString = NSAttributedString(string: CommonErrorString.AlertMesgError, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15),NSAttributedString.Key.foregroundColor : errorMesgExceedCount == true ? UIColor.red : UIColor.black ])
        let alertController = UIAlertController(title: CommonErrorString.CancelRideError, message: CommonErrorString.AlertMesgError, preferredStyle: .alert)
        let rect        = CGRect(x: 15, y: 120, width: 240, height: 55.0)
        let textView    = UITextView(frame: rect)
        textView.layer.cornerRadius = 5.0
        textView.autocapitalizationType = .none
        textView.autocorrectionType = .no
        textView.spellCheckingType = .no
        alertController.view.addSubview(textView)
        alertController.setValue(attributedString, forKey: "attributedMessage")
        let cancellButton = UIAlertAction(title: CommonAllString.NoStr, style: .destructive) { (_) in
            UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: false, completion: nil)
        }
        let okButton = UIAlertAction(title: CommonAllString.YesStr, style: .default) { (_) in
            errorMesgExceedCount = false
            callRideCanceMethod(result: textView.text ?? CommonAllString.BlankStr)
            UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: false, completion: nil)
        }
        alertController.addAction(cancellButton)
        alertController.addAction(okButton)
        UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: false, completion: nil)
        UIApplication.shared.windows.first?.rootViewController?.present(alertController, animated: true, completion: {
        })
    }
    
    func callRideCanceMethod(result:String?) {
        showAlertNotDTR = false
        if myRideForDay != nil {
            var textMesg = ""
            if let text = result {
                if text != "" {
                    textMesg = text
                }
            }
            if textMesg.count > 300 {
                errorMesgCountCheck = CommonErrorString.ThreeHundCharacError
                errorMesgExceedCount = true
                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                    self.alertView()
                }
                return;
            } else {
                self.dtrCommands.rideCancel(rideID: myRideForDay!.id,dateCode:myRideForDay!.dateCode, message: textMesg) { result in
                    result.debug(methodName:"Day Container rideCancel")
                }
            }
        }
    }
    
    
    func updateThreeDotFunctionality() {
        if myRideForDay != nil {
            arrayCustom = [DayContainerString.NotDtrAnyMoreStr]
            if myRideForDay!.riders.count <= 1 {
                if myRideForDay!.leader == dtrData.profileID {
                    arrayCustom[0] = DayContainerString.NotDtrAnyMoreStr
                }else{
                    arrayCustom[0] = DayContainerString.LeaveRideStr
                }
            } else {
                if myRideForDay!.leader == dtrData.profileID {
                    if myRideForDay!.riders.count >= 1 {
                        arrayCustom[0] = DayContainerString.CancelRideStr
                    }
                }else{
                    arrayCustom[0] = DayContainerString.LeaveRideStr
                }
            }
        }
    }
    
    func checkUnreadMesgCount() {
        if let loadedTimeStmp = UserDefaults.standard.dictionary(forKey: UserStore.shared.AllSaveTimeStampRideKey) {
            for item in loadedTimeStmp {
                if item.key ==  myRideForDay?.id {
                    let timsStp = item.value as! Double
                    guard let minIndex = messages.firstIndex(where: {$0.timestamp > timsStp}) else {
                        setLastMesgToMiniChat(showCount: false)
                        return
                    }
                    if messages.last?.sender != dtrData.profileID {
                        totalCountUnreadMesg = messages.count - minIndex
                        message =  messages.last!
                    }
                } else {
                    setLastMesgToMiniChat(showCount: false)
                }
            }
        }else{
            //if userdefault nil signout case
            setLastMesgToMiniChat(showCount: true)
        }
    }
    
    func setLastMesgToMiniChat(showCount:Bool) {
        if !messages.isEmpty {
            message =  messages.last!
            if showCount {
                totalCountUnreadMesg = messages.count
            }
        }
    }
}


struct ViewOffsetKey: PreferenceKey {
    typealias Value = CGFloat
    static var defaultValue = CGFloat.zero
    static func reduce(value: inout Value, nextValue: () -> Value) {
        value += nextValue()
    }
}
