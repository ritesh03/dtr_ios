//
//  DayContainerExtraViewForChat.swift
//  dtr-ios-test
//
//  Created by apple on 27/08/21.
//

import SwiftUI

struct DayContainerExtraViewForChat: View {
    
    @Binding var day: Int
    @ObservedObject var dtrData = DtrData.sharedInstance
    var body: some View {
        if dtrData.myRidesByDate[dateCode(offset: self.day)] != nil {
            if dtrData.myRidesByDate[dateCode(offset: self.day)]!.riders.count > 1 {
                VStack{
                    EmptyView().background(Color.red)
                }.background(Color.red).frame(height: 130, alignment: .center)
            }
        }
    }
}
