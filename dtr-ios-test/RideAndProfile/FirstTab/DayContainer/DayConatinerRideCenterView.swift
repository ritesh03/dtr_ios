//
//  DayConatinerRideCenterView.swift
//  dtr-ios-test
//
//  Created by apple on 27/08/21.
//

import SwiftUI

struct DayConatinerRideCenterView: View {
    @Binding var day:Int
    @Binding var myRideForDay : DtrRide?
    @Binding var shareRideDetail : Bool
    @Binding var hidePopupEveryTime :Bool
    @Binding var widthScreen :CGFloat
    @Binding var showTimrScreen :Bool
    @Binding var arrayCustom :[String]
    @Binding var showAlertNotDTR :Bool
    @Binding var presentModelOldHistoryScreen :Bool
    @Binding var showRideDetailScreen :Bool
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        RiderCenterView(day:$day,screenWidth:$widthScreen, showTimrScreen:$showTimrScreen,ridesToDisplay: $myRideForDay, showPopUp: $hidePopupEveryTime,showAlertNotDTR:$showAlertNotDTR,shareRide:$shareRideDetail, arrayCustom: $arrayCustom, presentModelOldHistoryScreen: $presentModelOldHistoryScreen)
            .onTapGesture {
                hidePopupEveryTime = false
                if dtrData.myRidesByDate[dateCode(offset: self.day)]?.riders.count ?? 0 > 1 {
                    showRideDetailScreen  = true
                }else{
                    UserDefaults.standard.removeObject(forKey: UserStore.shared.MyRideDetailLocally)
                }
            }
            .padding(.all,20)
    }
}
