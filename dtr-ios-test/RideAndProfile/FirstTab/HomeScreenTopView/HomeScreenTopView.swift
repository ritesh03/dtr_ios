//
//  HomeScreenTopView.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/01/21.
//

import SwiftUI
import Firebase

struct HomeScreenTopView: View {
    
    @ObservedObject var dtrData = DtrData.sharedInstance
    @Binding var currentDay : Int
    @State private var showingSheet = false
    @State private var showingFollowingSheet = false
    
    var body: some View {
        Group {
            VStack {
                HStack {
                    Text("My Rides").font(.system(size: 20, weight: .heavy, design: .default))
                        .foregroundColor(.white)
                        .padding(.horizontal)
                    Spacer()
                    Image(systemName: ImageConstantsNameForChatScreen.PersonImg) .foregroundColor(.white)
                        .padding(.trailing)
                        .onTapGesture {
                            self.showingFollowingSheet = true
                        }.fullScreenCover(isPresented: $showingFollowingSheet, content: {
                            NavigationView {
                                FollowerScreen(currentDay:$currentDay,comesFromChatScreen:true).accentColor(.white)
                            }
                        })
                    ZStack {
                        Image(systemName: "bell")
                            .foregroundColor(.white)
                            .padding(.trailing)
                        if dtrData.unreadNotifications > 0 {
                            ZStack {
                                Circle()
                                    .frame(width: 20, height:20)
                                    .foregroundColor(Color.init(UIColor.init(displayP3Red: 255/255, green: 92/255, blue: 210/255, alpha: 1.0)))
                                ZStack {
                                    Text("\(dtrData.unreadNotifications )")
                                        .font(.system(size: 10, weight: .bold, design: .default))
                                        .foregroundColor(.black)
                                }
                            }.offset(x: 0, y: -10)
                        }
                    }.onTapGesture {
                        self.showingSheet = true
                    }.fullScreenCover(isPresented: $showingSheet, content: {
                        NotificationsScreen(updatedDismissView:.constant(false),day: $currentDay, comesFromSheet: false).environment(\.modalMode, self.$showingSheet).accentColor(.white)
                    })
                }
                .padding(.top)
                HStack(alignment: .center, spacing: 6) {
                    ForEach(0..<7) { offset in
                        DayButton(offset: offset, active: offset == currentDay) { offset in
                            currentDay = offset
                        }
                    }
                }
                .padding(.top)
                .padding(.bottom)
                Spacer().frame(height: 5)
            }
        }.background(Color.init(ColorConstantsName.MainThemeBgColour))
    }
}


// define env key to store our modal mode values
struct ModalModeKey: EnvironmentKey {
    static let defaultValue = Binding<Bool>.constant(false) // < required
}

// define modalMode value
extension EnvironmentValues {
    var modalMode: Binding<Bool> {
        get {
            return self[ModalModeKey.self]
        }
        set {
            self[ModalModeKey.self] = newValue
        }
    }
}
