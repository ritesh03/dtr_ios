//
//  App_Header_C.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/01/21.
//

import SwiftUI


struct App_Header_C: View {
    
    @Binding var currentDay : Int
    
    var body: some View {
        Group {
            VStack {
                HStack {
                    Text("My Rides").font(.system(size: 20, weight: .heavy, design: .default))
                        .foregroundColor(.white)
                        .padding(.horizontal)
                    Spacer()
                    Button(action: {
                    }) {
                        Image(systemName: "person.2")
                            .padding(.trailing)
                    }
                    Button(action: {
                    }) {
                        Image(systemName: "bell")
                            .padding(.trailing)
                    }
                }
                .padding(.top)
                HStack(alignment: .center, spacing: 6) {
                    ForEach(0..<7) { offset in
                        DayButton(offset: offset, active: offset == currentDay) { offset in
                            currentDay = offset
                        }
                    }
                }
                .padding(.top)
                .padding(.bottom)
                Spacer().frame(height: 5)
            }
        }.background(Color.init("DTR-MainThemeBackground"))
    }
}


struct App_Header_C_Previews: View {
    var body: some View {
        App_Header_C(currentDay: .constant(0))
    }
}
