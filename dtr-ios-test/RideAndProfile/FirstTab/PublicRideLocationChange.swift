//
//  PublicRideLocationChange.swift
//  dtr-ios-test
//
//  Created by apple on 20/07/21.
//

import SwiftUI

struct PublicRideLocationChange: View {
    
    @Binding var showPublicRideLocationPopUp : Bool
    @Binding var day : Int
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var titleDtr: String = CommonAllString.BlankStr
    @State var showMapScreenForChange = false
    @Environment(\.presentationMode) var presentationMode
    
    @State  var startPlaceForShow = ""
    @State  var startCountryForShow = ""
    @State  var startPlaceLatForShow : Double = 0.0
    @State  var startPlaceLonForShow :Double  = 0.0
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            
            VStack(alignment: .center, spacing:30){
                HStack{
                    Button(action: {
                        showPublicRideLocationPopUp = false
                        UserStore.shared.publicViewAlreadyShowOrNot = true
                    }, label: {
                        HStack{
                            Image(systemName: ImageConstantsName.XmarkImg)
                            Text(PublicRideLocationChangeString.ChangeLocationStr)
                        }.foregroundColor(.white)
                    })
                    Spacer()
                }.padding(.top)
                
                VStack(alignment: .center, spacing:30){
                    Image(ImageConstantsName.MapMarkerSolidImg)
                        .resizable()
                        .frame(width: 42, height: 56, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .padding(.top)
                    Text("\(startPlaceForShow) \(startCountryForShow)").font(.system(size: 22, weight: .bold, design: .default))
                        .foregroundColor(.white)
                        .fixedSize(horizontal: false, vertical: true)
                        .multilineTextAlignment(.center)
                    Text("\(PublicRideLocationChangeString.YourRideSeenStr) \(dayName(dateCode: dateFormatGetDat(offset: day))) \(PublicRideLocationChangeString.FromNetworkPublicRideStr) \(startPlaceForShow),\(CommonAllString.ForNextLine) \(startCountryForShow)")
                        .foregroundColor(.gray)
                        .fixedSize(horizontal: false, vertical: true)
                        .multilineTextAlignment(.center)
                    VStack(alignment: .center, spacing:5){
                        Text(PublicRideLocationChangeString.ChangeDefaultLocationStr)
                            .foregroundColor(.gray)
                        Text(PublicRideLocationChangeString.SettingStr)
                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                            .onTapGesture {
                                showPublicRideLocationPopUp = false
                                UserStore.shared.publicViewAlreadyShowOrNot = true
                            }
                    }
                }
                Spacer()
            }.onAppear{
                getLocalDataForPublicRide()
            }
            .padding()
        }
    }
    
    func getLocalDataForPublicRide() {
        let dict = UserStore.shared.saveDateCodeWithUserLocation(city: "", country: "", lat: "", long: "", dateCode: dateCode(offset: self.day),isSetData:false)
        if dict.0 != "" && dict.1 != "" && dict.2 != "" && dict.3 != "" {
            startPlaceForShow = dict.0
            startCountryForShow = dict.1
            startPlaceLatForShow = Double(dict.2) ?? 0.0
            startPlaceLonForShow = Double(dict.3) ?? 0.0
        } else {
            startPlaceForShow = dtrData.profile.cityState
            startCountryForShow = dtrData.profile.country
            startPlaceLatForShow = dtrData.profile.userLat
            startPlaceLonForShow = dtrData.profile.userLon
        }
    }
}

struct PublicRideLocationChange_Previews: PreviewProvider {
    static var previews: some View {
        PublicRideLocationChange(showPublicRideLocationPopUp: .constant(false), day: .constant(0))
    }
}
