//
//  ExploreCommunityScreen.swift
//  dtr-ios-test
//
//  Created by apple on 26/11/21.
//

import SwiftUI
import UIKit

struct ExploreCommunityScreen: View {
    
    var body: some View {
        VStack {
            
            VStack(alignment: .center,spacing: 10) {
                Text("Connect with other cyclist to join a ride!").font(.system(size: 22, weight: .bold, design: .default))
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .multilineTextAlignment(.center)
                Image("DTR-CoupleCycle")
                    .resizable()
                    .padding()
                    .frame(width: UIScreen.screenWidth*0.8, height: UIScreen.screenHeight*0.43, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                Spacer()
                Text("EXPLORE DTR COMMUNITY")
                    .font(.system(size: 15).weight(.bold))
                    .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, maxWidth: .infinity, minHeight: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .foregroundColor(.white)
                    .background(Color.init(ColorConstantsName.AccentColour))
                    .cornerRadius(UIScreen.screenWidth*0.8/2)
                    .padding(.bottom,60)
                    .onTapGesture {
                        UpdateTbBarSlctedComunity.sharedInstance.tabSlectedComunity = 4
                    }
            }.padding()
                .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/,alignment: .center)
                .onAppear {
                }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
    }
}

struct ExpExploreCommunityScreen_Previews: PreviewProvider {
    static var previews: some View {
        ExploreCommunityScreen()
    }
}

class UpdateTbBarSlctedComunity: ObservableObject {
    
    @Published var tabSlectedComunity : Int = 0
    
    static let sharedInstance: UpdateTbBarSlctedComunity = {
        let instance = UpdateTbBarSlctedComunity()
        return instance
    }()
}
