//
//  UplodImageForRide.swift
//  dtr-ios-test
//
//  Created by Mobile on 09/02/21.
//

import SwiftUI

struct UplodImageForRide: View {
    
    @ObservedObject var myRideForDay : RideEditViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @StateObject var viewModel = getOptionToUploadImage()
    @State private var showingSheet = false
    @State private var selectedIndexImage : String = ""
    @State private var changeRightBtnName : String = ""
    @State private var pushAutomaticallyDone = false
    @State private var pushBackAutomaticallyDone = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var imagArray = [""]
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack (alignment: .leading){
                VStack (alignment: .leading,spacing:20){
                    Text(UplodImageForRideString.UploadAnImageForRideStr)
                        .foregroundColor(.white)
                    Button(action: {
                        showingSheet = true
                    }, label: {
                        Text(UplodImageForRideString.UploadStr)
                            .frame(maxWidth: .infinity, maxHeight: 50, alignment: .center)
                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                            .overlay(
                                RoundedRectangle(cornerRadius: 20)
                                    .stroke(Color.init(ColorConstantsName.AccentColour))
                            )})
                    Divider()
                        .background(Color.white)
                }.padding()
                StaticImagesForRideScreen(imagArray: $imagArray, selectedIndex: $selectedIndexImage)
            }
        }
        
        .onAppear {
            //imagArray.removeAll()
            // if dtrData.profile.ridesImages.count > 0 {
            imagArray = [CommonAllString.OneStr]
            imagArray.append(contentsOf: dtrData.profile.ridesImages)
            // }
        }
        
        .onReceive(dtrData.$profile, perform: { responseImages in
            if dtrData.profile.ridesImages != imagArray {
                imagArray = [CommonAllString.OneStr]
                if dtrData.profile.ridesImages.count > 0 {
                    imagArray.append(contentsOf: dtrData.profile.ridesImages)
                }
            }
        })
        
        .onReceive(dtrData.$profileImageUploading, perform: { responseOutPut in
            if responseOutPut {
                changeRightBtnName = CommonAllString.UploadingStr
                // pushAutomaticallyDone = true
                if pushBackAutomaticallyDone == true{
                    pushAutomaticallyDone = true
                }
            } else {
                changeRightBtnName = CommonAllString.UpdateCamlCseStr
                if pushAutomaticallyDone == true {
                    self.presentationMode.wrappedValue.dismiss()
                }
                pushBackAutomaticallyDone = false
            }
        })
        
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle(CommonAllString.BlankStr, displayMode: .inline)
        .navigationBarItems(leading:
                                Button(action: {
                                    UserStore.shared.saveLocallyRideDetail(myRideForDay: myRideForDay.rideEdit, isSetData: true) { response in }
                                    UserStore.shared.updateOnlyOneTimeForINviteAndShare = true
                                    self.presentationMode.wrappedValue.dismiss()
                                }) {
                                    HStack {
                                        Image(systemName: ImageConstantsName.ArrowLeftImg)
                                        Text(UplodImageForRideString.SelectRideImgStr)
                                    }},
                            trailing:
                                Button(action: {
                                    pushBackAutomaticallyDone = true
                                    if changeRightBtnName == CommonAllString.UpdateCamlCseStr {
                                        if selectedIndexImage != CommonAllString.BlankStr {
                                            UserStore.shared.saveLocallyRideDetail(myRideForDay: myRideForDay.rideEdit, isSetData: true) { response in }
                                            UserStore.shared.updateOnlyOneTimeForINviteAndShare = true
                                            dtrData.updateTheRideImageUrl(rideImageUrl: selectedIndexImage, rideId: myRideForDay.rideEdit.id) { responeUpload in
                                            }
                                        }
                                    } else {
                                        viewModel.isRideImage = false
                                        viewModel.profileId = CommonAllString.BlankStr
                                        viewModel.rideId = CommonAllString.BlankStr
                                    }
                                }) {
                                    if dtrData.profileImageUploading {
                                        ProgressView()
                                            .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                            .padding()
                                        Text(changeRightBtnName)
                                            .font(.system(size: 20, weight: .bold, design: .default))
                                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                                    } else {
                                        Text(changeRightBtnName)
                                            .font(.system(size: 20, weight: .bold, design: .default))
                                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                                    }
                                }).disabled(dtrData.profileImageUploading == true ? true : false)
        .actionSheet(isPresented: $showingSheet) {
            UserStore.shared.saveLocallyRideDetail(myRideForDay: myRideForDay.rideEdit, isSetData: true) { response in }
            UserStore.shared.updateOnlyOneTimeForINviteAndShare = true
            return ActionSheet(title: Text(UplodImageForRideString.WhtDoYouWntToStr), buttons: [.default(Text(UplodImageForRideString.FrmCameraStr), action: {
                viewModel.isRideImage = true
                viewModel.profileId = myRideForDay.rideEdit.leader
                viewModel.rideId = myRideForDay.rideEdit.id
                viewModel.takePhoto()
            }),.default(Text(UplodImageForRideString.FrmGalleryStr), action: {
                viewModel.isRideImage = true
                viewModel.profileId = myRideForDay.rideEdit.leader
                viewModel.rideId = myRideForDay.rideEdit.id
                viewModel.choosePhoto()
            }),.default(Text(TimeEditScreenString.CancelStr))])
        }.fullScreenCover(isPresented: $viewModel.isPresentingImagePicker, content: {
            ImagePicker(sourceType: viewModel.sourceType, completionHandler: viewModel.didSelectImage)
        })
    }
}


struct UplodImageForRide_Previews: PreviewProvider {
    static var previews: some View {
        UplodImageForRide(myRideForDay: RideEditViewModel.init(ride: DtrRide.default))
    }
}
