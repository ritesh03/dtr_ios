//
//  StaticImagesForRideScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 09/02/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI



struct StaticImagesForRideScreen: View {
    
    @Binding var imagArray: [String]
    @Binding var selectedIndex : String
    let columns = [GridItem(.flexible()),GridItem(.flexible())]
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    
    var body: some View {
        VStack(alignment: .leading, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/){
            Text(StaticImagesForRideScreenString.ChoseFromOneHereStr)
                .foregroundColor(.white)
                .padding()
            ScrollView {
                LazyVGrid(columns: columns, spacing: 20) {
                    ForEach(imagArray, id: \.self) { item in
                        Button {
                            selectedIndex = item
                        } label: {
                            WebImage(url:URL(string:item), isAnimating: .constant(true))
                                .purgeable(true)
                                .placeholder(Image(ImageConstantsName.MainRideEditCoverImg))
                                .renderingMode(.original)
                                .resizable()
                                .indicator(.activity)
                                .scaledToFill()
                                .frame(width: 162, height: 108)
                                .overlay(Image(selectedIndex == item ? ImageConstantsName.VectorImg : ImageConstantsName.EllipseRoundImg).padding(15), alignment: .topTrailing)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 20)
                                        .stroke(selectedIndex == item ? Color.init(ColorConstantsName.AccentColour) : Color.clear, lineWidth: 1)
                                )
                        }
                        .frame(width: 162, height: 108)
                        .cornerRadius(20, corners: [.allCorners])
                    }
                }
                .padding(.horizontal)
            }
        }
    }
}

struct StaticImagesForRideScreen_Previews: PreviewProvider {
    static var previews: some View {
        StaticImagesForRideScreen(imagArray: .constant([""]), selectedIndex: .constant(""))
    }
}
