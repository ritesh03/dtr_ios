//
//  RideVisiblityUI.swift
//  dtr-ios-test
//
//  Created by Mobile on 01/02/21.
//

import SwiftUI
import Firebase

struct RideVisiblity: View {
    @Binding var dismissView : Bool
    @State var comingFromSettingScreen : Bool = false
    @Binding var isUpdateRide : Bool
    @Binding var isVisbilityUpdate : Bool
    @State private var selectedIndex : Int = -1
    @State private var bckUpselectedIndex : Int = -1
    @State private var showActivityIndicator = false
    @State var showError = false
    @ObservedObject var myRideForDay : RideEditViewModel
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @ObservedObject var dtrData = DtrData.sharedInstance
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State private var postArray = ["friends","followers","public"]
    @State private var imagArray = ["DTR-FriendOfFriend","DTR-FriendAndFollowers","DTR-Public"]
    @State private var titleArray = ["Friends of friends","Friends of Friends, Followers and Group members","Public"]
    @State private var dscriptArray = ["Anyone that is friends with someone on your ride can see and join this ride.","Any of your friends or friends with someone on the ride, followers or members of the group you are a part of, can see and join this ride.","Your ride is available to everyone to see and join."]
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack(alignment: .leading,spacing:30) {
                if comingFromSettingScreen {
                    Text("Set your default Visiblity status")
                        .foregroundColor(.white)
                }else{
                    Text("Who can see and join this ride?")
                        .foregroundColor(.white)
                }
                VStack(alignment: .center, spacing:15){
                    RideVisblityUI(showError: $showError, titleArray: $titleArray, dscriptArray: $dscriptArray, imagArray: $imagArray, selectedIndex: $selectedIndex, totalCountToVisible: myRideForDay.rideEdit.visibleTo.count)
                }
            }.padding()
            .navigationBarHidden(false)
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,textTitle: "Ride Visibility", systemImageName: "arrow.left", action: {
                if comingFromSettingScreen {
                    dismissView = false
                    updateMesgWithTime()
                }else{
                    if selectedIndex != -1 {
                        if bckUpselectedIndex != selectedIndex {
                            myRideForDay.rideEdit.details.visibility = DtrRideVisibilityState(rawValue: postArray[selectedIndex])!
                            isUpdateRide = true
                            UserStore.shared.saveLocallyRideDetail(myRideForDay: myRideForDay.rideEdit, isSetData: true) { response in }
                            if selectedIndex == 2 {
                                isVisbilityUpdate = true
                            }
                            updateNotesAndSummary()
                        }
                    }
                    self.presentationMode.wrappedValue.dismiss()
                }
            })),trailing:AnyView(NavigtionItemBar(isImage: false, isSystemImage: false, isText:true,textTitle: showActivityIndicator == true ? "Updating" : "Update", systemImageName: "",textColorDiffernt:true,textColorCode:ColorConstantsName.AccentColour, action: {
                showActivityIndicator = true
                showError = false
                if selectedIndex != -1 {
                    if comingFromSettingScreen {
                        showActivityIndicator = true
                        updateMesgWithTime()
                    }else{
                        if selectedIndex != -1 {
                            myRideForDay.rideEdit.details.visibility = DtrRideVisibilityState(rawValue: postArray[selectedIndex])!
                            isUpdateRide = true
                            UserStore.shared.saveLocallyRideDetail(myRideForDay: myRideForDay.rideEdit, isSetData: true) { response in }
                            if selectedIndex == 2 {
                                isVisbilityUpdate = true
                            }
                            updateNotesAndSummary()
                        }
                    }
                }
            })).disabled(showActivityIndicator == true ? true : false).hidden())
        }.onAppear{
            Analytics.logEvent(AnalyticsScreen.rideVisiblity.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
            
            if comingFromSettingScreen {
                if dtrData.profile.defaultRideVisibility == "friends" {
                    selectedIndex = 0
                }else if dtrData.profile.defaultRideVisibility == "followers" {
                    selectedIndex = 1
                }else if dtrData.profile.defaultRideVisibility == "public" {
                    selectedIndex = 2
                }else{
                    selectedIndex = 1
                }
                bckUpselectedIndex = selectedIndex
            }else{
                if myRideForDay.rideEdit.details.visibility.rawValue == "friends" {
                    selectedIndex = 0
                }else if myRideForDay.rideEdit.details.visibility.rawValue == "followers" {
                    selectedIndex = 1
                }else if myRideForDay.rideEdit.details.visibility.rawValue == "public" {
                    selectedIndex = 2
                }
                bckUpselectedIndex = selectedIndex
            }
            
            if comingFromSettingScreen {
                postArray.removeLast()
                imagArray.removeLast()
                titleArray.removeLast()
                dscriptArray.removeLast()
            }
        }
    }
    
    func updateMesgWithTime(){
        let updatedProfile =  dtrData.profile
        let dict = ["id":updatedProfile.id,"name" : updatedProfile.name,"activated":updatedProfile.activated.timeIntervalSince1970,"country": updatedProfile.country, "cityState" : updatedProfile.cityState,"friends":updatedProfile.friends,"pushToken":updatedProfile.pushToken ?? "","verificationDeclined":updatedProfile.verificationDeclined,"defaultStatus":updatedProfile.defaultStatus,"linkCode":updatedProfile.linkCode,"description"  : updatedProfile.description,"tags":updatedProfile.tags,"platform":updatedProfile.platform, "phone": updatedProfile.phone ?? "","email": updatedProfile.email ?? "","pictureHash":updatedProfile.pictureHash,"defaultRideVisibility":postArray[selectedIndex]] as [String : Any]
        dtrCommands.profileUpdate(profileID: dtrData.profile.id, data: dict) { (result) in
            result.debug(methodName:"profileUpdate")
            showActivityIndicator = false
            if result.result == .ok {
                dismissView = false
            }
        }
    }
    
    func updateNotesAndSummary(){
        dtrCommands.rideUpdate(rideID:myRideForDay.rideEdit.id,data:["_geoloc":["lat":myRideForDay.rideEdit.details.startPlaceLat,"lng":myRideForDay.rideEdit.details.startPlaceLon],"summary":myRideForDay.rideEdit.summary,"details":["tags":myRideForDay.rideEdit.details.tags,"hasStartPlace":myRideForDay.rideEdit.details.hasStartPlace,"hasStartTime":myRideForDay.rideEdit.details.hasStartTime,"notes":myRideForDay.rideEdit.details.notes,"skillA":myRideForDay.rideEdit.details.skillA,"skillB":myRideForDay.rideEdit.details.skillB,"skillC":myRideForDay.rideEdit.details.skillC,"skillD":myRideForDay.rideEdit.details.skillD,"skillS":myRideForDay.rideEdit.details.skillS,"startPlace":myRideForDay.rideEdit.details.startPlace,"startPlaceLat":myRideForDay.rideEdit.details.startPlaceLat,"startPlaceLon":myRideForDay.rideEdit.details.startPlaceLon,"startTime":myRideForDay.rideEdit.details.startTime,"visibility":myRideForDay.rideEdit.details.visibility.rawValue]]){ result in
            result.debug(methodName: "rideUpdate")
            showActivityIndicator = false
            if result.result != .ok {
                if result.feedback.contains("Start location") {
                    showError = true
                }
            }else{
                self.presentationMode.wrappedValue.dismiss()
            }
        }
    }
}

