//
//  RideVisblityUI.swift
//  dtr-ios-test
//
//  Created by Mobile on 01/02/21.
//

import SwiftUI

struct RideVisblityUI: View {
    
    @Binding  var showError : Bool
    @Binding  var titleArray : [String]
    @Binding  var dscriptArray : [String]
    @Binding  var imagArray : [String]
    @Binding  var selectedIndex : Int
    @State var totalCountToVisible : Int
    
    var body: some View {
        ScrollView {
            VStack {
                ForEach(titleArray.indices, id: \.self) { ride in
                    HStack(alignment:.top) {
                        Image(imagArray[ride])
                            .resizable()
                            .frame(width: 70, height: 70, alignment: .leading)
                            .cornerRadius(10)
                        VStack(alignment: .leading, spacing: 10){
                            Group {
                                Text(titleArray[ride])
                                    .foregroundColor(.white)
                                Text(dscriptArray[ride])
                                    .foregroundColor(.gray)
                                if ride == 0 && selectedIndex == 0 {
                                    if totalCountToVisible <= 10 {
                                        if totalCountToVisible != 0 {
                                            Text("Only \(totalCountToVisible) people can see this ride. We suggest making it open to friends of friends and followers to reach more people.")
                                                .foregroundColor(.red)
                                        }  else {
                                            Text("No one see this ride. We suggest making it open to friends of friends and followers to reach more people.")
                                                .foregroundColor(.red)
                                        }
                                    }
                                }else{
                                    EmptyView()
                                }
                                Spacer()
                            }
                        }.fixedSize(horizontal: false, vertical: true)
                        .padding(.leading)
                        Spacer()
                            .background(Color.red)
                        VStack {
                            Image(selectedIndex == ride ? "DTR-Vector" : "DTR-EllipseRound")
                                .resizable()
                                .frame(width: 20, height: 20,alignment: .top)
                                .cornerRadius(10)
                            Spacer()
                        }
                    }.padding()
                    .background(selectedIndex == ride ? Color.clear : Color.init(ColorConstantsName.HeaderBgColour))
                    .onTapGesture {
                        if self.selectedIndex == ride {
                            self.selectedIndex = -1
                            return;
                        }
                        self.selectedIndex = ride
                    }
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(selectedIndex == ride ? Color.init(ColorConstantsName.AccentColour) : Color.clear, lineWidth: 1)
                    )
                }.cornerRadius(10)
                if showError {
                    VStack(alignment:.leading,spacing:10) {
                        Group {
                            Text("For Public You must fill out location first.").font(.system(size: 17, weight: .bold, design: .default))
                        }.foregroundColor(.red)
                        .multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    }.padding(.top,25)
                }
            }
        }
    }
}

struct RideVisblityUI_Previews: PreviewProvider {
    static var previews: some View {
        RideVisblityUI(showError: .constant(false), titleArray: .constant(["String"]), dscriptArray: .constant(["String"]), imagArray: .constant(["String"]), selectedIndex: .constant(1), totalCountToVisible: 5)
    }
}
