//
//  SearhbarScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 12/02/21.
//

import SwiftUI

struct LongTapGestureVw: View {
    @State private var titleDtr: String = ""
    @State var isFromProfile = false
    var body: some View {
        ZStack{
            VStack {
                HStack {
                    HStack(spacing:15) {
                        if isFromProfile {
                        Text("Select your general location - all of your rides will be nearby to this location.")
                            .foregroundColor(.white)
                        } else {
                            Image(ImageConstantsName.HeaderCircleImg)
                            Text("Long press to set the start location of your ride.")
                                .foregroundColor(.white)
                        }
                    }
                }.padding()
                .background(Color.black)
                .opacity(0.75)
                .ignoresSafeArea()
                .cornerRadius(15)
            }
        }
    }
}
struct LongTapGestureVw_Previews: PreviewProvider {
    static var previews: some View {
        LongTapGestureVw()
    }
}
