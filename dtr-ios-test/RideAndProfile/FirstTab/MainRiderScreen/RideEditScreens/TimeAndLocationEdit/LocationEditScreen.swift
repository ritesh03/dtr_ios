//
//  LocationEditScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 12/02/21.
//

import SwiftUI
import MapKit
import CoreLocation


struct LocationEditScreen: View {
    var canEdit: Bool = true
    @Binding var isUpdateRideLocation:Bool
    @Binding var showLocation:Bool
    @Binding var hasLocation: Bool
    @State var mapReset: Bool = false
    @State private var startPlace = ""
    @State var selectionCity: String = ""
    @State private var startPlaceLat = 0.0
    @State private var startPlaceLon = 0.0
    @State private var titleDtr: String = ""
    @State var selectionCountry: String = ""
    @State private var hasStartPlace = false
    @State var mapMode: MKMapType = .standard
    @State var isFirstTimeShowVwTap: Bool = false
    @State private var locationWasChanged = false
    @ObservedObject var myRideForDay : RideEditViewModel
    @State var selectionCoordinate = CLLocationCoordinate2D()
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    
    var body: some View {
        ZStack {
            VStack {
                MapChooser(selectionCity: $selectionCity, selectionCountry: $selectionCountry, selectionCoordinate: $selectionCoordinate, mapType: self.$mapMode, mapReset: self.$mapReset, ride: myRideForDay.rideEdit, canSetLocation: canEdit) { hasStartPlace, cityName, startPlaceCoord,countryName  in
                    self.locationWasChanged = true
                    self.hasStartPlace = hasStartPlace
                    self.startPlace = cityName
                    self.selectionCountry = countryName
                    self.startPlaceLat = startPlaceCoord.latitude
                    self.startPlaceLon = startPlaceCoord.longitude
                }
            }
            VStack {
                HStack {
                    HStack {
                        Button(action: {
                            showLocation.toggle()
                        }) {
                            HStack {
                                Image(systemName: "arrow.left")
                                    .aspectRatio(contentMode: .fit)
                                    .foregroundColor(.white)
                            }}
                        HStack {
                            TextField("", text: $titleDtr, onEditingChanged: { isEditing in
                            }, onCommit: {
                                
                            })
                            Button(action: {
                            }) {
                                Image(systemName: "magnifyingglass")
                                    .aspectRatio(contentMode: .fit)
                            }
                        }
                        .padding()
                        .background(Color.init(SECTION_BG_COLOUR))
                        .cornerRadius(10)
                        .foregroundColor(.white)
                        .padding(.all)
                    }
                }.padding()
                .background(Color.init(MAIN_THEME_BG_COLOUR))
                .ignoresSafeArea()
                
                if isFirstTimeShowVwTap {
                    LongTapGestureVw()
                    Spacer()
                }
                Spacer()
            }
            VStack {
                Spacer()
                HStack {
                    HStack {
                        CustomButtonCircleRS(btnTitle: "Clear location", btnWidth: 135, btnHeight: 30, action:{
                            mapReset = true
                        })
                        Spacer()
                        CustomButtonCircleRS(btnTitle: "Set location", btnWidth: 135, btnHeight: 30,backgroundColord:true, action:{
                            updateMesgWithLocation()
                        })
                    }
                }.padding()
                .background(Color.black)
                .opacity(0.75)
            }.offset(y: -60)
        }.onTapGesture {
            isFirstTimeShowVwTap = false
        }.hiddenNavigationBarStyle()
    }
    
    func updateMesgWithLocation(){
//        var updatedRide = self.myRideForDay
//        updatedRide.details.hasStartPlace = self.locationWasChanged
//        updatedRide.details.startPlaceLat = self.startPlaceLat
//        updatedRide.details.startPlaceLon = self.startPlaceLon
//        updatedRide.details.startPlace = self.startPlace
//
        
        myRideForDay.rideEdit.details.hasStartPlace = self.locationWasChanged
        myRideForDay.rideEdit.details.startPlaceLat = self.startPlaceLat
        myRideForDay.rideEdit.details.startPlaceLon = self.startPlaceLon
        myRideForDay.rideEdit.details.startPlace = self.startPlace
        isUpdateRideLocation = true
        showLocation.toggle() 
    }
}
