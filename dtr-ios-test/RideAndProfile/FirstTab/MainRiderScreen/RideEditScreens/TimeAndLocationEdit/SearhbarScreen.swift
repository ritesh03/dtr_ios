//
//  SearhbarScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 12/02/21.
//

import SwiftUI



struct LongTapGestureVw: View {
    @State private var titleDtr: String = ""
    var body: some View {
        ZStack{
            VStack {
                HStack {
                    HStack(spacing:15) {
                        Image("DTR-HandCirecl")
                        Text("Long press to set the start location of your ride.")
                            .foregroundColor(.white)
                    }
                }.padding()
                .background(Color.black)
                .opacity(0.75)
                .ignoresSafeArea()
                .cornerRadius(15)
            }
        }
    }
}
struct LongTapGestureVw_Previews: PreviewProvider {
    static var previews: some View {
        LongTapGestureVw()
    }
}
