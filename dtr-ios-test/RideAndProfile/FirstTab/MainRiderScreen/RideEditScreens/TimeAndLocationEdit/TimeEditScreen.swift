//
//  TimeEditScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 11/02/21.
//

import SwiftUI
import Firebase

struct TimeEditScreen: View {
    @State private var hasTime = false
    @Binding var isUpdateRideTime:Bool
    @State private var startTime = ""
    @State private var bckUpstartTime = ""
    @State private var bckUphasTime = false
    @Binding var showTimrScreen :Bool
    @ObservedObject var myRideForDay : RideEditViewModel
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State private var currentDate = Date().roundDate(toNext: 15, in: .minutes)!
    
    
    func updateMesgWithTime(){
        if self.bckUpstartTime != self.startTime || self.bckUphasTime != self.hasTime {
            myRideForDay.rideEdit.details.hasStartTime = self.hasTime
            myRideForDay.rideEdit.details.startTime = self.startTime
            isUpdateRideTime = true
            updateNotesAndSummary()
        }
        showTimrScreen.toggle()
    }
    
    private var dateProxy:Binding<Date> {
        Binding<Date>(
            get: {self.currentDate},set: {
                self.currentDate = $0
                let formatter = DateFormatter()
                formatter.timeStyle = .short
                formatter.timeZone = TimeZone.current
                self.startTime = formatter.string(from: self.currentDate)
            }
        )
    }
    
    var body: some View {
        ZStack {
            Color.black
                .opacity(0.4)
            VStack(spacing:20){
                HStack{
                    Spacer()
                    Button(action: {
                        showTimrScreen.toggle()
                    }) {
                        Text(TimeEditScreenString.CancelStr)
                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                    }
                }
                
                HStack {
                    DatePicker(TimeEditScreenString.TimeStr,selection: dateProxy,displayedComponents: [.hourAndMinute])
                        .datePickerStyle(DefaultDatePickerStyle())
                        .accentColor(.white)
                        .labelsHidden()
                        .accentColor(.white)
                        .environment(\.colorScheme, .dark) // <- This modifier
                }.frame(width: 200, alignment: .center)
                
                HStack{
                    CustomButtonCircleWithCustomProperty(btnTitle: TimeEditScreenString.TimeTBDStr, btnWidth: 100, btnHeight: 15, backgroundColord: false) {
                        hasTime = false
                        self.startTime = TimeEditScreenString.TimeTBDStr
                        updateMesgWithTime()
                        UserStore.shared.saveLocallyRideDetail(myRideForDay: myRideForDay.rideEdit, isSetData: true) { response in }
                    }
                    Spacer()
                    CustomButtonCircleWithCustomProperty(btnTitle: TimeEditScreenString.SetTimeStr, btnWidth: 100, btnHeight: 15, backgroundColord: true) {
                        self.hasTime = true
                        if self.startTime == TimeEditScreenString.TimeTBDStr {
                            let formatter = DateFormatter()
                            formatter.timeStyle = .short
                            formatter.timeZone = TimeZone.current
                            self.startTime = formatter.string(from: self.currentDate)
                        }
                        updateMesgWithTime()
                    }
                }
            }.padding()
            .background(Color.init(ColorConstantsName.HeaderBgColour))
            .cornerRadius(20)
            .frame(width: 350, height: 200, alignment: .center)
            .onAppear {
                Analytics.logEvent(AnalyticsScreen.rideTime.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
                UISegmentedControl.appearance().selectedSegmentTintColor =  UIColor(named: ColorConstantsName.AccentColour)
                self.hasTime = self.myRideForDay.rideEdit.details.hasStartTime
                if self.hasTime {
                    let formatter = DateFormatter()
                    formatter.timeStyle = .short
                    formatter.timeZone = TimeZone.current
                    let rideDate = toDate(timeString: self.myRideForDay.rideEdit.details.startTime, on: self.myRideForDay.rideEdit.dateCode)
                    self.currentDate = rideDate
                    self.startTime = formatter.string(from: rideDate)
                } else {
                    self.startTime = TimeEditScreenString.TimeTBDStr
                    Analytics.logEvent(AnalyticsEvent.rideAbandonTime.rawValue, parameters: ["rideID": self.myRideForDay.rideEdit.id])
                }
                self.bckUpstartTime = self.startTime
                self.bckUphasTime = self.hasTime
            }
            .onDisappear{
                UISegmentedControl.appearance().selectedSegmentTintColor =  UIColor.clear
            }
        }
    }
    
    func updateNotesAndSummary(){
        DispatchQueue.main.async {
            dtrCommands.rideUpdateTime(rideID:myRideForDay.rideEdit.id,data:["details":["tags":myRideForDay.rideEdit.details.tags,"hasStartPlace":myRideForDay.rideEdit.details.hasStartPlace,"hasStartTime":myRideForDay.rideEdit.details.hasStartTime,"notes":myRideForDay.rideEdit.details.notes,"skillA":myRideForDay.rideEdit.details.skillA,"skillB":myRideForDay.rideEdit.details.skillB,"skillC":myRideForDay.rideEdit.details.skillC,"skillD":myRideForDay.rideEdit.details.skillD,"skillS":myRideForDay.rideEdit.details.skillS,"startPlace":myRideForDay.rideEdit.details.startPlace,"startPlaceLat":myRideForDay.rideEdit.details.startPlaceLat,"startPlaceLon":myRideForDay.rideEdit.details.startPlaceLon,"startTime":myRideForDay.rideEdit.details.startTime,"visibility":myRideForDay.rideEdit.details.visibility.rawValue]]){ result in
                result.debug(methodName: "rideUpdateTime")
                Analytics.logEvent(AnalyticsEvent.rideUpdateTime.rawValue, parameters: ["rideID": myRideForDay.rideEdit.id])
            }
        }
    }
}


struct TimeEditScreen_Previews: PreviewProvider {
    static var previews: some View {
        TimeEditScreen(isUpdateRideTime: .constant(false), showTimrScreen: .constant(false), myRideForDay: RideEditViewModel.init(ride: DtrRide.default))
    }
}
