//
//  SkillUI.swift
//  dtr-ios-test
//
//  Created by Mobile on 09/02/21.
//

import SwiftUI

struct SkillUI: View {
    
    var title: String
    var descrption: String
    var imgName: String
    
    var isSelected: Bool
    var action: () -> Void
    
    var body: some View {
        ScrollView {
            VStack {
                Button(action: self.action) {
                    HStack(alignment:.top) {
                        Image(imgName)
                            .resizable()
                            .frame(width: 40, height: 40, alignment: .leading)
                            .cornerRadius(10)
                        VStack(alignment: .leading, spacing: 10){
                            Text(title)
                                .multilineTextAlignment(.leading)
                                .foregroundColor(.white)
                            Text(descrption).frame(maxWidth: .infinity, alignment: .leading)
                                .foregroundColor(.gray)
                                .multilineTextAlignment(.leading)
                            Spacer()
                        }
                        .padding(.leading)
                        Spacer()
                            .background(Color.red)
                        VStack {
                            Image(isSelected ? "DTR-Vector" : "DTR-EllipseRound")
                                .resizable()
                                .frame(width: 20, height: 20,alignment: .top)
                                .cornerRadius(10)
                            Spacer()
                        }
                    }.padding()
                    .background(isSelected ? Color.clear : Color.init(ColorConstantsName.HeaderBgColour))
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(isSelected ? Color.init(ColorConstantsName.AccentColour) : Color.clear, lineWidth: 1)
                    )
                }.cornerRadius(10)
            }
        }
    }
}
