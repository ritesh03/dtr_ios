//
//  SetSkillScrren.swift
//  dtr-ios-test
//
//  Created by Mobile on 09/02/21.
//

import SwiftUI
import Firebase


struct SetSkillScrren: View {
    @State var showActivityIndicator = false
    @State private  var showError = false
    @Binding var isUpdateRide:Bool
    @State var selections: [String] = []
    @State var backUpselections: [String] = []
    @ObservedObject var myRideForDay : RideEditViewModel
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State var skilsSortArry: [String] = ["A", "B", "C", "D", "S"]
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var imgArry: [String] = ["DTR-SkiilA", "DTR-SkiilB", "DTR-SkiilC", "DTR-SkiilD", "DTR-SkiilS"]
    @State var titleArray : [String] = ["Expert / Elite", "Expert and Fast Sport", "Sport and Fast Beginner", "Beginner - No Drop", "Social ride or other non-ride event"]
    @State var dscrpArry: [String] = ["Reserved for the ultra-fast and experienced cycling crowd. Rarely stops unless stated in the ride details.", "Always fast, sometimes regroups. Flat road speeds ~20MPH.", "Most common group ride speed. Typically has fast spots, slower spots, regroups, and sometimes no-drop.", "Designed to make sure that nobody gets left behind - regardless of speed. Usually has a ride sweeper pulling up the rear.", ""]
    
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack(alignment: .leading,spacing:30) {
                Text("Select the skill levels that you’re expecting for the ride. You can include multiple levels.")
                    .foregroundColor(.gray)
                ScrollView {
                    VStack {
                        ForEach(skilsSortArry.indices, id: \.self) { ride in
                            SkillUI(title: titleArray[ride], descrption: dscrpArry[ride], imgName: imgArry[ride], isSelected: self.selections.contains(skilsSortArry[ride])) {
                                if self.selections.contains(skilsSortArry[ride]) {
                                    self.selections.removeAll(where: { $0 == skilsSortArry[ride] })
                                } else {
                                    self.selections.append(skilsSortArry[ride])
                                }
                            }
                        }
                    }
                }.onAppear {
                    if myRideForDay.rideEdit.details.skillA == true {
                        self.selections.append("A")
                    }
                    
                    if myRideForDay.rideEdit.details.skillB == true {
                        self.selections.append("B")
                    }
                    
                    if myRideForDay.rideEdit.details.skillC == true  {
                        self.selections.append("C")
                    }
                    
                    if myRideForDay.rideEdit.details.skillD == true  {
                        self.selections.append("D")
                    }
                    
                    if myRideForDay.rideEdit.details.skillS == true {
                        self.selections.append("S")
                    }
                    self.backUpselections = self.selections
                    Analytics.logEvent(AnalyticsScreen.aboutSkills.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
                }
                if showError {
                    VStack(alignment:.leading,spacing:10) {
                        Group {
                            Text("For Public You must fill out location first.").font(.system(size: 17, weight: .bold, design: .default))
                        }.foregroundColor(.red)
                            .multilineTextAlignment(.leading)
                            .fixedSize(horizontal: false, vertical: true)
                    }.padding(.top,25)
                }
                
            }.padding()
                .navigationBarBackButtonHidden(true)
                .navigationBarTitle("", displayMode: .inline)
                .navigationBarItems(leading:
                                        Button(action: {
                    if self.backUpselections != self.selections {
                        updateSkill()
                    }
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    HStack {
                        Image(systemName: ImageConstantsName.ArrowLeftImg)
                        Text("Set skill level")
                    }})
        }
    }
    
    func updateSkill() {
        if self.selections.contains("A") {
            myRideForDay.rideEdit.details.skillA = true
        }else{
            myRideForDay.rideEdit.details.skillA = false
        }
        if self.selections.contains("B") {
            myRideForDay.rideEdit.details.skillB = true
        }else{
            myRideForDay.rideEdit.details.skillB = false
        }
        if self.selections.contains("C") {
            myRideForDay.rideEdit.details.skillC = true
        }else{
            myRideForDay.rideEdit.details.skillC = false
        }
        if self.selections.contains("D") {
            myRideForDay.rideEdit.details.skillD = true
        }else{
            myRideForDay.rideEdit.details.skillD = false
        }
        if self.selections.contains("S") {
            myRideForDay.rideEdit.details.skillS = true
        }else{
            myRideForDay.rideEdit.details.skillS = false
        }
        isUpdateRide = true
        Analytics.logEvent(AnalyticsEvent.rideUpdateSkills.rawValue, parameters: ["rideID": myRideForDay.rideEdit.id])
        
        UserStore.shared.saveLocallyRideDetail(myRideForDay: myRideForDay.rideEdit, isSetData: true) { response in }
        showError = false
        updateNotesAndSummary()
    }
    
    func updateNotesAndSummary(){
        dtrCommands.rideUpdate(rideID:myRideForDay.rideEdit.id,data:["_geoloc":["lat":myRideForDay.rideEdit.details.startPlaceLat,"lng":myRideForDay.rideEdit.details.startPlaceLon],"summary":myRideForDay.rideEdit.summary,"details":["tags":myRideForDay.rideEdit.details.tags,"hasStartPlace":myRideForDay.rideEdit.details.hasStartPlace,"hasStartTime":myRideForDay.rideEdit.details.hasStartTime,"notes":myRideForDay.rideEdit.details.notes,"skillA":myRideForDay.rideEdit.details.skillA,"skillB":myRideForDay.rideEdit.details.skillB,"skillC":myRideForDay.rideEdit.details.skillC,"skillD":myRideForDay.rideEdit.details.skillD,"skillS":myRideForDay.rideEdit.details.skillS,"startPlace":myRideForDay.rideEdit.details.startPlace,"startPlaceLat":myRideForDay.rideEdit.details.startPlaceLat,"startPlaceLon":myRideForDay.rideEdit.details.startPlaceLon,"startTime":myRideForDay.rideEdit.details.startTime,"visibility":myRideForDay.rideEdit.details.visibility.rawValue]]){ result in
            result.debug(methodName: "rideUpdate")
            showActivityIndicator = false
            if result.result != .ok {
                if result.feedback.contains("Start location") {
                    showError = true
                }
            }else{
                self.presentationMode.wrappedValue.dismiss()
            }
        }
    }
}
