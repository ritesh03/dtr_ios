//
//  NotesEditScrn.swift
//  dtr-ios-test
//
//  Created by Mobile on 12/02/21.
//

import SwiftUI
import Firebase

struct NotesEditScrn: View {
    
    @State var showActivityIndicator = false
    @State  var isSummaryAvallible = ""
    @Binding var isUpdateRide:Bool
    @State private var notes = ""
    @State private var summary = ""
    @State private var backUpString = ""
    @Binding var scrennType :String
    @State private  var message : String = ""
    @State private  var showError = false
    
    @State private var textStyle = UIFont.TextStyle.body
    @ObservedObject var myRideForDay : RideEditViewModel
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    
    func updateMesgWithTime() {
        if scrennType  == NotesEditScrnString.NotesStr {
            myRideForDay.rideEdit.summary = summary
            myRideForDay.rideEdit.details.notes = message
            isUpdateRide = true
            Analytics.logEvent(AnalyticsEvent.rideUpdateNotes.rawValue, parameters: [NotesEditScrnString.RideIdStr: myRideForDay.rideEdit.id])
        } else if scrennType  == NotesEditScrnString.Summarytr {
            myRideForDay.rideEdit.summary = message
            myRideForDay.rideEdit.details.notes = notes
            isUpdateRide = true
            Analytics.logEvent(AnalyticsEvent.rideUpdateSummary.rawValue, parameters: [NotesEditScrnString.RideIdStr: myRideForDay.rideEdit.id])
        }
        updateNotesAndSummary()
        //UserStore.shared.saveLocallyRideDetail(myRideForDay: myRideForDay.rideEdit, isSetData: true) { response in }
    }
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack{
                TextView(comesFromDetailAndEditScreen: .constant(true), isEditable: .constant(true), text:  $message, textStyle:$textStyle, comesFromEditScreen: .constant(false), didStartEditing: .constant(false),placeHolderText:CommonAllString.BlankStr)
                    .cornerRadius(10)
                    .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, maxHeight: 500)
                Spacer()
                if showError {
                    VStack(alignment:.leading,spacing:10) {
                        Group {
                            Text(NotesEditScrnString.ForPublicYouMustFillOutStr).font(.system(size: 17, weight: .bold, design: .default))
                        }.foregroundColor(.red)
                        .multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    }.padding(.top,25)
                }
            }.padding()
        }.onAppear{
            Analytics.logEvent(AnalyticsScreen.rideNotes.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
            summary =  myRideForDay.rideEdit.summary
            notes = myRideForDay.rideEdit.details.notes
            if scrennType == NotesEditScrnString.NotesStr {
                message = notes
                backUpString = notes
            }else{
                message = summary
                backUpString = summary
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarItems(leading:
                                Button(action: {
                                    updateMesgWithTime()
                                    self.presentationMode.wrappedValue.dismiss()
                                }) {
                                    HStack {
                                        Image(systemName: ImageConstantsName.ArrowLeftImg)
                                        Text(scrennType == NotesEditScrnString.NotesStr ? NotesEditScrnString.AboutThisRideStr : isSummaryAvallible)
                                            .frame(width: 230, alignment: .leading)
                                            .lineLimit(1)
                                    }})
    }
    
    func updateNotesAndSummary(){
        if scrennType  == NotesEditScrnString.NotesStr {
            if myRideForDay.rideEdit.details.notes.lowercased() == backUpString.lowercased() {return;}
        } else if scrennType  == NotesEditScrnString.Summarytr {
            if myRideForDay.rideEdit.summary.lowercased() == backUpString.lowercased() {return;}
        }
        dtrCommands.rideUpdate(rideID:myRideForDay.rideEdit.id,data:["_geoloc":["lat":myRideForDay.rideEdit.details.startPlaceLat,"lng":myRideForDay.rideEdit.details.startPlaceLon],"summary":myRideForDay.rideEdit.summary,"details":["tags":myRideForDay.rideEdit.details.tags,"hasStartPlace":myRideForDay.rideEdit.details.hasStartPlace,"hasStartTime":myRideForDay.rideEdit.details.hasStartTime,"notes":myRideForDay.rideEdit.details.notes,"skillA":myRideForDay.rideEdit.details.skillA,"skillB":myRideForDay.rideEdit.details.skillB,"skillC":myRideForDay.rideEdit.details.skillC,"skillD":myRideForDay.rideEdit.details.skillD,"skillS":myRideForDay.rideEdit.details.skillS,"startPlace":myRideForDay.rideEdit.details.startPlace,"startPlaceLat":myRideForDay.rideEdit.details.startPlaceLat,"startPlaceLon":myRideForDay.rideEdit.details.startPlaceLon,"startTime":myRideForDay.rideEdit.details.startTime,"visibility":myRideForDay.rideEdit.details.visibility.rawValue]]){ result in
            result.debug(methodName: "rideUpdate")
            showActivityIndicator = false
            if result.result != .ok {
                if result.feedback.contains("Start location") {
                    showError = true
                }
            }else{
                self.presentationMode.wrappedValue.dismiss()
            }
        }
    }
}

struct NotesEditScrn_Previews: PreviewProvider {
    static var previews: some View {
        NotesEditScrn(isUpdateRide: .constant(false), scrennType: .constant(""), myRideForDay: RideEditViewModel.init(ride: DtrRide.default))
    }
}
