//
//  ShowPublicTagError.swift
//  dtr-ios-test
//
//  Created by Mobile on 07/04/21.
//

import SwiftUI

struct ShowPublicTagError: View {
    
    @Binding var rideDetailDay : DtrRide
    
    var body: some View {
        Group {
            if rideDetailDay.details.visibility.rawValue == ShowPublicTagErrorString.PublicStr {
                if !rideDetailDay.details.hasStartTime || !rideDetailDay.details.hasStartPlace {
                    if  !rideDetailDay.details.skillA && !rideDetailDay.details.skillB  &&  !rideDetailDay.details.skillC   && !rideDetailDay.details.skillD && !rideDetailDay.details.skillS  {
                        showTextErrorMesg()
                    }else if !rideDetailDay.details.hasStartTime || !rideDetailDay.details.hasStartPlace {
                        showTextErrorMesg()
                    }
                }else if  !rideDetailDay.details.skillA && !rideDetailDay.details.skillB  &&  !rideDetailDay.details.skillC   && !rideDetailDay.details.skillD && !rideDetailDay.details.skillS  {
                    showTextErrorMesg()
                }else {
                    let title = rideDetailDay.summary.replacingOccurrences(of: " ", with: "")
                    if title.lowercased() == ShowPublicTagErrorString.DownToRideStrSpace.lowercased() {
                        showTextErrorMesg(errorMessageForTitle: true)
                    }
                    
                }
            }
        }
    }
}


func showTextErrorMesg(errorMessageForTitle:Bool = false) -> AnyView {
    return AnyView (
        VStack(alignment:.leading,spacing:10) {
            Group {
                Text(ShowPublicTagErrorString.YourRideIsNotVisibleStr).font(.system(size: 16, weight: .bold, design: .default))
                if errorMessageForTitle {
                    Text("Enter a custom name for your public ride.").font(.system(size: 15, weight: .regular, design: .default))
                } else {
                    Text(ShowPublicTagErrorString.YouMusFillOutAllFieldForPublicStr).font(.system(size: 15, weight: .regular, design: .default))
                }
            }.foregroundColor(.red)
            .multilineTextAlignment(/*@START_MENU_TOKEN@*/.leading/*@END_MENU_TOKEN@*/)
            .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
        }
    )
}

struct ShowPublicTagError_Previews: PreviewProvider {
    static var previews: some View {
        ShowPublicTagError(rideDetailDay:.constant(DtrRide.default))
    }
}
