//
//  TagScreenUI.swift
//  dtr-ios-test
//
//  Created by Mobile on 11/02/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct TagScreenUI: View {
    
    var title: String
    var icon: String
    var isSelected: Bool
    var action: () -> Void
    
    var body: some View {
        Button(action: self.action) {
            HStack {
                if !icon.isEmpty {
                    WebImage(url:URL(string: icon), options: [.progressiveLoad], isAnimating: .constant(true))
                        .purgeable(true)
                        .placeholder(Image(systemName: "person.circle.fill"))
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 25, height: 25)
                        .clipShape(Circle())
                        .padding(1)
                }
                Text(title)
                    .foregroundColor(.white)
                Spacer()
                    .background(Color.red)
                VStack {
                    Image(isSelected ? "DTR-Vector" : "DTR-EllipseRound")
                        .resizable()
                        .frame(width: 15, height: 15)
                        .cornerRadius(10)
                }
            }.padding()
            .background(isSelected ? Color.clear : Color.init(ColorConstantsName.HeaderBgColour))
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(isSelected ? Color.init(ColorConstantsName.AccentColour) : Color.clear, lineWidth: 1)
            )
            .cornerRadius(10)
        }
    }
}
