//
//  RideTagScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 11/02/21.
//

import SwiftUI

struct RideTagScreen: View {
    
    @State var selections: [String] = []
    @StateObject var model = ContentViewModel()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var rideDetailDay : RideEditViewModel
    @State private var showActivityIndicator = false
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack(alignment: .leading,spacing:30) {
                Text("You can select multiple ride tags for this ride.")
                    .foregroundColor(.white)
                ScrollView {
                    FlexibleView(data: model.originalItems,spacing: model.spacing,alignment: model.alignment) { item in
                        TagScreenUI(title: item.name, icon: item.iconFile, isSelected: self.selections.contains(item.id)) {
                            if self.selections.contains(item.id) {
                                self.selections.removeAll(where: { $0 == item.id })
                            } else {
                                self.selections.append(item.id)
                            }
                        }
                    }
                    .padding(.horizontal, model.padding)
                }.navigationBarBackButtonHidden(true)
                .navigationBarTitle("", displayMode: .inline)
                .navigationBarItems(leading:
                                        Button(action: {
                                            callUpdateDetail()
                                        }) {
                                            HStack {
                                                Image(systemName: ImageConstantsName.ArrowLeftImg)
                                                Text("Ride tags")
                                            }},
                                    trailing:
                                        Group {
                                            if self.showActivityIndicator {
                                                ProgressView()
                                                    .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                                    .padding()
                                            }
                                    })
            }.padding(.init(top: 10, leading: 20, bottom: 0, trailing: 0))
            .onReceive(model.$showActivityIndicator) { responseActivit in
                if responseActivit {
                    self.showActivityIndicator = true
                }else {
                    self.showActivityIndicator = false
                }
            }
        }
    }
    
    func callUpdateDetail() {
        showActivityIndicator = true
        DtrCommand.sharedInstance.rideUpdate(rideID:rideDetailDay.rideEdit.id,data:["_geoloc":["lat":rideDetailDay.rideEdit.details.startPlaceLat,"lng":rideDetailDay.rideEdit.details.startPlaceLon],"summary":rideDetailDay.rideEdit.summary,"details":["tags":selections,"hasStartPlace":rideDetailDay.rideEdit.details.hasStartPlace,"hasStartTime":rideDetailDay.rideEdit.details.hasStartTime,"notes":rideDetailDay.rideEdit.details.notes,"skillA":rideDetailDay.rideEdit.details.skillA,"skillB":rideDetailDay.rideEdit.details.skillB,"skillC":rideDetailDay.rideEdit.details.skillC,"skillD":rideDetailDay.rideEdit.details.skillD,"skillS":rideDetailDay.rideEdit.details.skillS,"startPlace":rideDetailDay.rideEdit.details.startPlace,"startPlaceLat":rideDetailDay.rideEdit.details.startPlaceLat,"startPlaceLon":rideDetailDay.rideEdit.details.startPlaceLon,"startTime":rideDetailDay.rideEdit.details.startTime,"visibility":rideDetailDay.rideEdit.details.visibility.rawValue]]){ result in
            result.debug(methodName: "rideUpdate")
            result.debug(methodName: "rideUpdate")
            self.showActivityIndicator = false
            self.presentationMode.wrappedValue.dismiss()
        }
    }
    
}

struct RideTagScreen_Previews: PreviewProvider {
    static var previews: some View {
        RideTagScreen(rideDetailDay: RideEditViewModel.init(ride: DtrRide.default))
    }
}
