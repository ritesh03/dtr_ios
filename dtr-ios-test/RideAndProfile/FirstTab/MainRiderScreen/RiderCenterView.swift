//
//  RiderCenterView.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/01/21.
//

import SwiftUI
import Firebase

struct RiderCenterView: View {
    @Binding var day : Int
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @Binding var ridesToDisplay : DtrRide!
    @Binding var showPopUp: Bool
    @Binding var showRideEdit : Bool
    @Binding var showAlertNotDTR : Bool
    @Binding var shareRide : Bool
    @Binding var arrayCustom:[String]
    @Binding var presentModelOldHistoryScreen : Bool
    @State var UpdatedRideDetailForImages : DtrRide = DtrRide.default
    
    var body: some View {
        ZStack {
            if ridesToDisplay != nil {
                VStack  (alignment: .center) {
                    if ridesToDisplay.rideCoverImage != "" {
                        returnRideCoverImage(rideImageUrl:ridesToDisplay.rideCoverImage,className:"RideCenterView")
                    }else{
                        Image("DTR-MainCoverImage")
                            .resizable()
                            .scaledToFill()
                            .frame(width:UIScreen.screenWidth-30,height: 120, alignment: .center)
                            .clipped()
                            .cornerRadius(20, corners: [.topLeft, .topRight])
                            .padding(.bottom)
                    }
                    VStack  (alignment: .leading , spacing: 20) {
                        HStack {
                            Text(ridesToDisplay.summary).font(.system(size: 20, weight: .heavy, design: .default))
                                .foregroundColor(.white)
                            Button(action: {
                                self.showRideEdit = true
                            }) {
                                if ridesToDisplay.riders.count <= 1 {
                                    Image("DTR-UnVector")
                                        .resizable()
                                        .frame(width: 15,height: 15)
                                }else if ridesToDisplay.riders.count > 1 {
                                    if ridesToDisplay.leader ==  self.dtrData.profileID {
                                        Image("DTR-UnVector")
                                            .resizable()
                                            .frame(width: 15,height: 15)
                                    }
                                }
                            }
                        }
                        
                        
                        if ridesToDisplay.riders.count > 1 {
                            if ridesToDisplay.fromAction == .leave || ridesToDisplay.fromAction == .cancel {
                                Text("Riding with \(self.dtrData.profileInfo(profileID: ridesToDisplay.leader).name)")
                                    .foregroundColor(.gray)
                                    .font(.system(size: 14))
                            }else{
                                Text(self.dtrData.profileInfo(profileID: ridesToDisplay.leader).name).font(.system(size: 14))
                                    .foregroundColor(.gray)
                                    .fixedSize(horizontal: false, vertical: true)
                            }
                        }else{
                            Text(self.dtrData.profileInfo(profileID: ridesToDisplay.leader).name).font(.system(size: 14))
                                .foregroundColor(.gray)
                                .fixedSize(horizontal: false, vertical: true)
                        }
                        
                        if ridesToDisplay != nil {
                            RidersHorizontalImages(ride: $UpdatedRideDetailForImages, showLeader: true)
                        }
                        
                        HStack(alignment:.top) {
                            Image(systemName: "clock")
                                .resizable()
                                .frame(width: 20,height: 20)
                                .foregroundColor(.white)
                            Text(ridesToDisplay.details.startTime == "" ? "TBD" : ridesToDisplay.details.startTime)
                                .foregroundColor(.white)
                                .padding(.trailing)
                            Image(systemName: "mappin.and.ellipse")
                                .resizable()
                                .frame(width: 20,height: 20)
                                .foregroundColor(.white)
                            Text(ridesToDisplay.details.startPlace == "" ? "Location TBD" : ridesToDisplay.details.startPlace)
                                .foregroundColor(.white)
                        }
                        
                        HStack {
                            RideSkillIndicators(ride: .constant(ridesToDisplay!))
                        }
                        
                        Group {
                            if ridesToDisplay.details.visibility.rawValue == "public" {
                                Text("Public Ride")
                                    .foregroundColor(Color.init("DTR-Lime"))
                            }
                            NavigationLink(destination:RideTagScreen.init(selections: ridesToDisplay.tags, rideDetailDay: RideEditViewModel(ride: ridesToDisplay)),label: {
                                Text("Add ride tags")
                                    .foregroundColor(Color.init(DTR_ACCENT_COLOUR))
                            })
                        }.fixedSize(horizontal: false, vertical: true)
                        
                        HStack  {
                            NavigationLink(destination:InviteMainScreen(day: $day, showDeatilPopUp: .constant(false),ride:ridesToDisplay, profile: dtrData.profile,  resetLocalValue: true, selectFriends: [""], selectFollowers: [""]),label: {
                                Text("INVITE")
                                    .frame(width: 120,height:40)
                                    .foregroundColor(.white)
                                    .background(Color.init(DTR_ACCENT_COLOUR))
                                    .cornerRadius(40)
                            })
                            Button(action: {
                                shareRide = true
                            }) {
                                Text("SHARE")
                                    .frame(width: 120,height:40)
                                    .foregroundColor(.white)
                                    .background(Color.init(DTR_ACCENT_COLOUR))
                                    .cornerRadius(40)
                            }
                            
                            Button(action: {
                                showPopUp = true
                            }) {
                                Image("DTR-Outlinedbutton")
                                    .resizable()
                                    .frame(width: 40, height: 40)
                            }.padding(.leading)
                        }
                        .padding(.bottom)
                    }
                    .padding(.horizontal)
                }
                .background(Color.init("DTR-HeaderBackground1"))
                .cornerRadius(20)
                .clipped()
                CustomWidthPoup(arrayCustom: $arrayCustom, show: $showPopUp,ridesToDisplay:ridesToDisplay) { (index) in
                    if index == "Edit Ride" {
                        self.showRideEdit = true
                    }else if index == "Not DTR anymore" {
                        if self.ridesToDisplay.riders.count > 1 {
                            showAlertNotDTR = true
                        }else{
//                            if UserDefaults.standard.object(forKey: self.ridesToDisplay.dateCode) != nil{
//                                UserDefaults.standard.removeObject(forKey: self.ridesToDisplay.dateCode)
//                            }
                            self.dtrCommands.rideCancel(rideID: self.ridesToDisplay.id,dateCode:self.ridesToDisplay.dateCode, message: "") { result in
                                result.debug(methodName:"rideCancel")
                            }
                        }
                    }else if index == "Leave Ride" {
                        self.dtrCommands.rideLeave(rideID: self.ridesToDisplay.id, dateCode: self.ridesToDisplay.dateCode) { (result) in
                            //if result.result == .ok {
                                presentModelOldHistoryScreen = true
                           // }
                        }
                    }
                }.offset(x: 20, y: 120)
            }
        }
        
        .onReceive(self.dtrData.$myRidesByDate) { rideDict in
            let dict = rideDict[dateCode(offset: day)]
            UpdatedRideDetailForImages = DtrRide.default
            if dict != nil {
                self.ridesToDisplay = dict!
                UpdatedRideDetailForImages = self.ridesToDisplay
            }
        }
        
        .background(EmptyView().sheet(isPresented: self.$shareRide) {
            Modal(isPresented: self.$shareRide, title: "Share Ride") {
                ShareScreen(isPresented: self.$shareRide, ride: self.ridesToDisplay)
            }
            .onAppear{
                Analytics.logEvent(AnalyticsScreen.rideLink.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
            }
        })
    }
}

struct CenterView_Previews: PreviewProvider {
    static var previews: some View {
        RiderCenterView(day: .constant(0), ridesToDisplay: .constant(DtrRide.default), showPopUp: .constant(false), showRideEdit: .constant(false), showAlertNotDTR: .constant(false), shareRide: .constant(false), arrayCustom: .constant([""]), presentModelOldHistoryScreen: .constant(false))
    }
}
