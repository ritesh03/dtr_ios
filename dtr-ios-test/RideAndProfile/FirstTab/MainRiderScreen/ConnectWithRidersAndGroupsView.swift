//
//  ConnectWithRidersAndGroupsView.swift
//  dtr-ios-test
//
//  Created by Vaibhav Thakkar on 12/01/22.
//

import SwiftUI

struct ConnectWithRidersAndGroupsView: View {
    @State var showGruopListScreen = false
    @State var showFriendSearch = false

    var body: some View {
        VStack(spacing: 0) {
            Text("Connect with people to see their rides")
                .padding(.vertical)
            
            Button(action: {
                UpdateTbBarSlctedComunity.sharedInstance.tabSlectedComunity = 2
            }) {
                Text("Find More Riders")
                    .foregroundColor(Color.init(ColorConstantsName.AccentColour))
            }
            
            Text(CommonAllString.OrStr)
                .padding(.vertical)
            
            Button(action: {
                showGruopListScreen = true
            }) {
                Text("Join more groups")
                    .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                    .padding(.bottom, 50)
            }
            
            
            NavigationLink(destination:AllGroupLists(dismissView: $showGruopListScreen, ifIsFromNoDtr: true), isActive: $showGruopListScreen){
                EmptyView()
            }

        }.foregroundColor(.white)
    }
}


