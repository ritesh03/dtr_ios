//
//  NoRidingDay.swift
//  dtr-ios-test
//
//  Created by Mobile on 20/01/21.
//

import SwiftUI
import Firebase

struct NoRidingDay: View {
    
    @Binding var day: Int
    @State var presentingModal = false
    
    var body: some View {
        VStack(spacing: 10) {
            Image(ImageConstantsName.CycleIconImg)
                .padding(.vertical)
            Text("Tell your friends you're \n Down to Ride today!").font(.system(size: 24, weight: .heavy, design: .default))
                .foregroundColor(.white).multilineTextAlignment(.center).fixedSize(horizontal: false, vertical: true)
            Text("You'll be able to see who else \n is down to ride on this day.").font(.system(size: 16, weight: .regular, design: .default)).multilineTextAlignment(.center).fixedSize(horizontal: false, vertical: true)
                .foregroundColor(.gray)
                .padding(.vertical)
            Button(action: {
                self.presentingModal = true
                Analytics.logEvent(AnalyticsObjectEvent.down_to_ride_click.rawValue, parameters: nil)
            }) {
                Text("I'M DOWN TO RIDE!")
                    .frame(width: UIScreen.screenWidth*0.8, height: 20)
                    .padding()
                    .foregroundColor(.white)
                    .background(Color.init(ColorConstantsName.AccentColour))
                    .cornerRadius(40)
            }.accessibility(identifier: "I'M DOWN TO RIDE!")
                .sheet(isPresented: $presentingModal) {
                    CreateRideScreen(day: $day, presentedAsModal: self.$presentingModal, isComeFromLeaveRide: false, afterLeaveRideDefaultRide: DtrRide.default)
                }
            ConnectWithRidersAndGroupsView()
            Spacer()
        }
    }
}

struct NoRidingDay_Previews: PreviewProvider {
    static var previews: some View {
        NoRidingDay(day: .constant(0))
    }
}
