//
//  RideDetailImageView.swift
//  dtr-ios-test
//
//  Created by apple on 31/08/21.
//

import SwiftUI

struct RideDetailImageView: View {
    
    @Binding var myRideForDay: DtrRide
    
    var body: some View {
        if myRideForDay.rideCoverImage != "" {
            returnRideCoverImage(rideImageUrl:myRideForDay.rideCoverImage,className:"RideDetailView")
        }else{
            Image(ImageConstantsName.MainRideEditCoverImg)
                .resizable()
                .scaledToFill()
                .frame(width:UIScreen.screenWidth,height: 180, alignment: .center)
                .clipped()
        }
    }
}
