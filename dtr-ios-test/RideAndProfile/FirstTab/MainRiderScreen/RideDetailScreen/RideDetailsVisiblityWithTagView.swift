//
//  RideDetailsVisiblityWithTagView.swift
//  dtr-ios-test
//
//  Created by apple on 31/08/21.
//

import SwiftUI

struct RideDetailsVisiblityWithTagView: View {
    
    @Binding var myRideForDay: DtrRide
    @State private var tagsCollection = [""]
    
    var body: some View {
        Group {
            if myRideForDay.details.visibility.rawValue == "public" {
                if myRideForDay.details.tags.count > 0 {
                    VStack {
                        ShowTagUIView(tags: $tagsCollection,comeFromDetailScreen:false, ridesToDisplay: $myRideForDay)
                    }
                } else {
                    Text("Public Ride")
                        .foregroundColor(Color.init(ColorConstantsName.LimeColour))
                }
            }else if myRideForDay.details.visibility.rawValue == "friends" {
                Text("Friends of Friends")
                    .foregroundColor(.white)
            }else if myRideForDay.details.visibility.rawValue == "followers" {
                Text("Friends of Friends, Followers and Group members")
                    .foregroundColor(.white)
            }
        }.fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
        .onAppear{
            tagsCollection.removeAll()
            if myRideForDay.details.tags.count > 0 {
                tagsCollection = myRideForDay.details.tags
                if myRideForDay.details.visibility.rawValue == "public" {
                    if tagsCollection.count > 0  {
                        if !self.tagsCollection.contains("Public_Ride") {
                            self.tagsCollection.insert("Public_Ride", at: 0)
                        }
                    }
                }
            }
        }
    }
}
