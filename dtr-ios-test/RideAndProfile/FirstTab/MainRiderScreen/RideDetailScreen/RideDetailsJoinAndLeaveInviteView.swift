//
//  RideDetailsJoinAndLeaveInviteView.swift
//  dtr-ios-test
//
//  Created by apple on 31/08/21.
//

import SwiftUI
import Firebase

struct RideDetailsJoinAndLeaveInviteView: View {
    
    @Binding var day: Int
    @Binding var myRideForDay: DtrRide
    @Binding var showPopUpForError: Bool
    @Binding var rideFromNotifcaitonScreen: DtrRide
    @Binding var errorAndSuccessApiRetrunMessgae: String
    @Binding var rideWeekNameForCancelRide: String
    @Binding var showPopUp: Bool
    @Binding var shareRide: Bool
    @Binding var dismissViewForceFully: Bool
    
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        if myRideForDay.leader != dtrData.profileID {
            Group {
                if myRideForDay.fromAction == .leave {
                    showInviteAndShareButton
                }else{
                    Group {
                        HStack(alignment:.center,spacing: 10) {
                            CustomButtonCircleWithCustomProperty(btnTitle: myRideForDay.fromAction.label, btnWidth: self.myRideForDay.details.visibility == .public ?  UIScreen.screenWidth*0.33 :  UIScreen.screenWidth*0.8, btnHeight: 20, backgroundColord: true) {
                                if myRideForDay.fromAction == .none {
                                    if let myRide = dtrData.myRidesByDate[dateCode(offset: toOffset(dateCode: myRideForDay.dateCode))] {
                                        if myRide.leader != dtrData.profile.id && myRide.leader != rideFromNotifcaitonScreen.leader {
                                            alertView()
                                            return
                                        }
                                    }
                                    self.dtrCommands.rideJoin(rideID:self.myRideForDay.id,dateCode:self.myRideForDay.dateCode) { result in
                                        pokeAnalytics()
                                        result.debug(methodName:"rideJoin")
                                        if result.result == .ok {
                                           // self.dtrCommands.sendRideMessage(rideID:UserStore.shared.getRideDetailDetails().0,message:"\(dtrData.profile.name) has joined the ride") { ( result) in
                                                //result.debug(methodName: "sendRideMessage")
                                                self.dtrCommands.sendSimpleNotification(profileID: UserStore.shared.getRideDetailDetails().0, data: ["recipients":[ UserStore.shared.getRideDetailDetails().1],"type":"rideUpdate","message":"\(dtrData.profile.name) joined your ride on \(dayName(dateCode: UserStore.shared.getRideDetailDetails().3)) for \(UserStore.shared.getRideDetailDetails().2)"]) { ( result) in
                                                    result.debug(methodName:"sendSimpleNotification")
                                                }
                                           // }
                                            self.day = toOffset(dateCode: self.myRideForDay.dateCode)
                                            myRideForDay.fromAction = DtrRideFromActionType(rawValue: 2)!
                                            presentationMode.wrappedValue.dismiss()
                                            dismissViewForceFully = true
                                        }else{
                                            errorAndSuccessApiRetrunMessgae = result.feedback
                                            showPopUpForError = true
                                        }
                                    }
                                }
                            }
                            if self.myRideForDay.details.visibility == .public {
                                Button(action: {
                                    shareRide = true
                                }) {
                                    Text("SHARE")
                                        .frame(minWidth: 0, idealWidth: 40, maxWidth: .infinity, minHeight: 0, idealHeight: 50, maxHeight:50, alignment: .center)
                                        .foregroundColor(.white)
                                        .background(Color.init(ColorConstantsName.AccentColour))
                                        .cornerRadius(40)
                                }
                            }
                        }
                    }
                }
            }
        }
        else{
            Group {
                Group {
                    HStack(alignment:.bottom){
                        ShowPopUpMesg(titleMesg: "Invitation sent!", customMesg: "Your friends and followers will be notified of your invitation to this ride.", show: $showPopUp, opticity: .constant(false))
                    }
                }
                showInviteAndShareButton
            }
        }
    }
    
    var showInviteAndShareButton: some View {
        Group {
            HStack(alignment:.center,spacing: 10) {
                NavigationLink(destination:InviteMainScreen(day:$day,showDeatilPopUp: $showPopUp,ride:$myRideForDay, profile: dtrData.profile, opacity: 1.0, resetLocalValue: true, selectFriends: [""], selectFollowers: [""], selectGroupMembers: [""]),label: {
                    Text("INVITE")
                        .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: 40, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealHeight: 50, maxHeight: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .foregroundColor(.white)
                        .background(Color.init(ColorConstantsName.AccentColour))
                        .cornerRadius(40)
                })
                Button(action: {
                    Analytics.logEvent(AnalyticsObjectEvent.share_ride.rawValue, parameters: nil)
                    shareRide = true
                }) {
                    Text("SHARE")
                        .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: 40, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealHeight: 50, maxHeight:50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .foregroundColor(.white)
                        .background(Color.init(ColorConstantsName.AccentColour))
                        .cornerRadius(40)
                }
            }
        }
    }
    
    func alertView() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "", message: "Joining this ride will overwrite your other ride for \(rideWeekNameForCancelRide)", preferredStyle: .alert)
            let cancellButton = UIAlertAction(title: "Cancel", style: .destructive) { (_) in}
            let okButton = UIAlertAction(title: "Confirm", style: .default) { (_) in
                okBtnAlert()
            }
            alert.addAction(okButton)
            alert.addAction(cancellButton)
            
            UIApplication.shared.keyWindow!.rootViewController!.topMostViewController().present(alert, animated: true, completion: {
                print("Error Not Present")
            })
        }
    }
    
    func okBtnAlert() {
        if let myRide = dtrData.myRidesByDate[dateCode(offset: toOffset(dateCode: myRideForDay.dateCode))] {
            if myRide.leader != self.dtrData.profile.id {
                let joinRideID = self.myRideForDay.id
                self.dtrCommands.rideLeave(rideID: myRide.id,dateCode:myRide.dateCode) { (result) in
                    self.dtrCommands.rideJoin(rideID: joinRideID,dateCode:self.myRideForDay.dateCode) { result in
                        pokeAnalytics()
                        result.debug(methodName:"rideJoin")
                        if result.result == .ok {
                            self.day = toOffset(dateCode: self.myRideForDay.dateCode)
                            myRideForDay.fromAction = DtrRideFromActionType(rawValue: 2)!
                            //  self.dtrCommands.sendRideMessage(rideID:UserStore.shared.getRideDetailDetails().0,message:"\(dtrData.profile.name) has joined the ride") { ( result) in
                            result.debug(methodName: "sendRideMessage")
                            self.dtrCommands.sendSimpleNotification(profileID: UserStore.shared.getRideDetailDetails().0, data: ["recipients":[ UserStore.shared.getRideDetailDetails().1],"type":"rideUpdate","message":"\(dtrData.profile.name) joined your ride on \(dayName(dateCode: UserStore.shared.getRideDetailDetails().3)) for \(UserStore.shared.getRideDetailDetails().2)"]) { ( result) in
                                result.debug(methodName:"sendSimpleNotification")
                            }
                            //}
                            presentationMode.wrappedValue.dismiss()
                        }else{
                            errorAndSuccessApiRetrunMessgae = result.feedback
                            showPopUpForError = true
                        }
                    }
                }
            } else {
                self.dtrCommands.rideJoin(rideID: self.myRideForDay.id,dateCode:self.myRideForDay.dateCode) { result in
                    //pokeAnalytics()
                    result.debug(methodName:"rideJoin")
                    if result.result == .ok {
                        self.day = toOffset(dateCode: self.myRideForDay.dateCode)
                        myRideForDay.fromAction = DtrRideFromActionType(rawValue: 2)!
                        //  self.dtrCommands.sendRideMessage(rideID:UserStore.shared.getRideDetailDetails().0,message:"\(dtrData.profile.name) has joined the ride") { ( result) in
                        result.debug(methodName: "sendRideMessage")
                        self.dtrCommands.sendSimpleNotification(profileID: UserStore.shared.getRideDetailDetails().0, data: ["recipients":[ UserStore.shared.getRideDetailDetails().1],"type":"rideUpdate","message":"\(dtrData.profile.name) joined your ride on \(dayName(dateCode: UserStore.shared.getRideDetailDetails().3)) for \(UserStore.shared.getRideDetailDetails().2)"]) { ( result) in
                            result.debug(methodName:"sendSimpleNotification")
                        }
                        // }
                        presentationMode.wrappedValue.dismiss()
                    }else{
                        errorAndSuccessApiRetrunMessgae = result.feedback
                        showPopUpForError = true
                    }
                }
            }
        }
    }
    
    func pokeAnalytics() {
        if  UserStore.shared.shouldRecordComeBackRide == true {
            Analytics.logEvent(AnalyticsScreen.poke_come_back_ride.rawValue, parameters: ["source":dtrData.profile.id])
            UserStore.shared.shouldRecordComeBackRide = false
        }else if UserStore.shared.shouldRecordJoinWeekendRide == true {
            Analytics.logEvent(AnalyticsScreen.poke_join_weekend_ride.rawValue, parameters: ["source":dtrData.profile.id])
            UserStore.shared.shouldRecordJoinWeekendRide = false
        }else if UserStore.shared.shouldRecordGoDTRWeekendRide == true {
            Analytics.logEvent(AnalyticsScreen.poke_go_dtr_weekend_ride.rawValue, parameters: ["source":dtrData.profile.id])
            UserStore.shared.shouldRecordGoDTRWeekendRide = false
        }
    }
}

extension UIViewController {
    @objc func topMostViewController() -> UIViewController {
        // Handling Modal views
        if let presentedViewController = self.presentedViewController {
            return presentedViewController.topMostViewController()
        }
        // Handling UIViewController's added as subviews to some other views.
        else {
            for view in self.view.subviews
            {
                // Key property which most of us are unaware of / rarely use.
                if let subViewController = view.next {
                    if subViewController is UIViewController {
                        let viewController = subViewController as! UIViewController
                        return viewController.topMostViewController()
                    }
                }
            }
            return self
        }
    }
}

extension UITabBarController {
    override func topMostViewController() -> UIViewController {
        return self.selectedViewController!.topMostViewController()
    }
}

extension UINavigationController {
    override func topMostViewController() -> UIViewController {
        return self.visibleViewController!.topMostViewController()
    }
}
