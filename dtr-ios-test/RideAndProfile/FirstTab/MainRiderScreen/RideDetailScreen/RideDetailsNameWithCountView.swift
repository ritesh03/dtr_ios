//
//  RideDetailsNameWithCountView.swift
//  dtr-ios-test
//
//  Created by apple on 31/08/21.
//

import SwiftUI

struct RideDetailsNameWithCountView: View {
    
    @Binding var myRideForDay: DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        Group {
            if myRideForDay.riders.count > 1 {
                if myRideForDay.fromAction == .leave || myRideForDay.fromAction == .cancel  {
                    Text("Riding with \(self.dtrData.profileInfo(profileID: myRideForDay.leader).name)")
                        .font(.system(size: 14))
                        .foregroundColor(.gray) .fixedSize(horizontal: false, vertical: true)
                }else{
                    Text(self.dtrData.profileInfo(profileID: myRideForDay.leader).name).font(.system(size: 14))
                        .foregroundColor(.gray)
                        .fixedSize(horizontal: false, vertical: true)
                }
            }else{
                Text(self.dtrData.profileInfo(profileID: myRideForDay.leader).name).font(.system(size: 14))
                    .foregroundColor(.gray)
                    .fixedSize(horizontal: false, vertical: true)
            }
        }
    }
}
