//
//  RideDetailAboutSectionView.swift
//  dtr-ios-test
//
//  Created by apple on 31/08/21.
//

import SwiftUI

struct RideDetailAboutSectionView: View {
    
    @Binding var myRideForDay: DtrRide
    @State private var textStyle = UIFont.TextStyle.body
    
    var body: some View {
        Group {
            VStack(alignment: .leading, spacing: 20) {
                Divider()
                    .background(Color.white)
                Text("ABOUT THIS RIDE")
                Group {
                    VStack(alignment: .leading) {
                        if myRideForDay.details.notes != "" {
                            TextView(comesFromDetailAndEditScreen: .constant(false), isEditable: .constant(false), text:  .constant(myRideForDay.details.notes), textStyle:$textStyle, comesFromEditScreen: .constant(false), didStartEditing: .constant(false),placeHolderText:"")
                                .cornerRadius(10)
                                .frame(width: UIScreen.screenWidth-30, height: 400)
                                .padding(.bottom,50)
                            
                        }
                    }
                    if myRideForDay.details.tags.contains("KfqH2PdeUY4nyp9V2nPQ") {
                        VStack{
                            EmptyView().background(Color.red)
                        }.frame(height: 150, alignment: .center)
                    }
                }
            }
        }
    }
}
