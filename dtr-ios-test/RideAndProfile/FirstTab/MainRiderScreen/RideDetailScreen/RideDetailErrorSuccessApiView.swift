//
//  RideDetailErrorSuccessApiView.swift
//  dtr-ios-test
//
//  Created by apple on 31/08/21.
//

import SwiftUI

struct RideDetailErrorSuccessApiView: View {
    
    @Binding var showPopUpForError: Bool
    @Binding var errorAndSuccessApiRetrunMessgae: String
    
    var body: some View {
        Group {
            Group {
                HStack(alignment:.bottom){
                    ShowPopUpMesg(titleMesg: "", customMesg: errorAndSuccessApiRetrunMessgae, show: $showPopUpForError, opticity: .constant(false))
                }
            }
        }
    }
}
