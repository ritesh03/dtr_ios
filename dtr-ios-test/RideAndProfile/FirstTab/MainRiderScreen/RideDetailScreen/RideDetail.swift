//
//  RideDetail.swift
//  dtr-ios-test
//
//  Created by Mobile on 21/01/21.
//

import SwiftUI
import MessageUI
import Firebase

struct RideDetail: View {
    
    @Binding var day: Int
    @Binding var myRideForDay : DtrRide
    @State  var receivedNotification = AppNotification.default
    @State  var rideFromNotifcaitonScreen: DtrRide = DtrRide.default
    @State  var rideFromNotifcaitonBool: Bool = false
    @State  var rideFromPushNotification: Bool = false
    @Binding var presentModelOldHistoryScreen : Bool
    @ObservedObject var myRideForDay1 : RideEditViewModel
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @ObservedObject var dtrData = DtrData.sharedInstance
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State private var showPopUp: Bool = false
    @State private var rideWeekNameForCancelRide = ""
    @State private var shareRide = false
    @State private var showPopUpFlgInapp = false
    @State private var showRideLocationScreen: Bool = false
    @State private var isUpdateRideLocation = false
    @State private var canEditLocation = false
    @State var activeSheet: ActiveSheetForRideDetailScreen?
    @State private var showMailCompose = false
    @State var result: Result<MFMailComposeResult, Error>? = nil
    //@State private var showRiderProfileScreen = false
    @State private var customArrayForRightMenu = ["Flag as inappropriate"]
    @State private var errorAndSuccessApiRetrunMessgae = ""
    @State private var showMapScreenDetail = false
    @State private var dismissViewForceFully = false
    
    
    var body: some View {
        ZStack {
            if myRideForDay.details.tags.contains("KfqH2PdeUY4nyp9V2nPQ") {
                Image(ImageConstantsName.IceTagBag2Img)
                    .renderingMode(.original)
                    .resizable()
                    .edgesIgnoringSafeArea(.top)
                    .layoutPriority(1)
                    .aspectRatio(1125/2613, contentMode: .fill)
                    .padding(.top,50)
            }
            ScrollView {
                VStack {
                    if !myRideForDay.details.tags.contains("KfqH2PdeUY4nyp9V2nPQ") {
                        RideDetailImageView(myRideForDay:$myRideForDay)
                    }else{
                        VStack{
                            EmptyView().background(Color.red)
                        }.background(Color.red).frame(height: 290, alignment: .center)
                    }
                    RideDetailsAllView(day: $day, myRideForDay: $myRideForDay, showPopUp: $showPopUp, shareRide: $shareRide, showRideLocationScreen: $showRideLocationScreen, rideWeekNameForCancelRide: $rideWeekNameForCancelRide, rideFromNotifcaitonScreen: $rideFromNotifcaitonScreen, errorAndSuccessApiRetrunMessgae: $errorAndSuccessApiRetrunMessgae,showMapScreenDetai:$showMapScreenDetail, dismissViewForceFully: $dismissViewForceFully)
                        .padding(.horizontal).background(Color.init(ColorConstantsName.MainThemeBgColour).opacity(0.9))
                }
                Spacer()
            }
           
            Spacer()
        }
        
        .onChange(of: showMapScreenDetail, perform: { response in
            if response == true {
                activeSheet = .mapScreen
            }
        })
        
        .onChange(of: dismissViewForceFully, perform: { response in
            if response == true {
                if rideFromNotifcaitonBool {
                    presentationMode.wrappedValue.dismiss()
                }
            }
        })
        
        .onAppear {
            Analytics.logEvent(AnalyticsScreen.rideDetails.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
            if rideFromNotifcaitonBool {
                if rideFromNotifcaitonScreen.leader != self.dtrData.profile.id || rideFromNotifcaitonScreen.leader != self.myRideForDay.leader || rideFromNotifcaitonScreen.riders.count > 1 {
                    rideWeekNameForCancelRide = dayName(dateCode: rideFromNotifcaitonScreen.dateCode)
                }
            } else {
                if let myRide = dtrData.myRidesByDate[dateCode(offset: toOffset(dateCode: myRideForDay.dateCode))] {
                    rideWeekNameForCancelRide = dayName(dateCode: myRide.dateCode)
                }
            }
            
            canEditLocation = false
            
            if myRideForDay.riders.count > 1 {
                if customArrayForRightMenu.contains("Leave Ride") {
                    customArrayForRightMenu.removeAll()
                    customArrayForRightMenu.append("Flag as inappropriate")
                    customArrayForRightMenu.append("Leave Ride")
                }else{
                    if myRideForDay.riders.contains(dtrData.profileID) && myRideForDay.leader != dtrData.profileID {
                        customArrayForRightMenu.append("Leave Ride")
                    }
                }
            }
            UserStore.shared.saveLocallyRideDetail(myRideForDay: myRideForDay, isSetData: true) { response in }
        }
        
        .background(Color.init(ColorConstantsName.MainThemeBgColour))
        .foregroundColor(.white)
        .edgesIgnoringSafeArea(.all)
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarItems(leading:leadingButton.padding(5),trailing: trailingButton.padding(5))
//        NavigationLink(destination: ProfileScreen(updatedDismissView:.constant(false),day: $day,userProfile: .constant(self.dtrData.profileInfo(profileID: myRideForDay.leader)), showBackButtonOrNot:true), isActive: $showRiderProfileScreen){
//            EmptyView()
//        }
        
        .fullScreenCover(item: $activeSheet) { item in
            switch item {
            case .MailScreen:
                if MFMailComposeViewController.canSendMail() {
                    MailView(result: self.$result,recipients: ["dtr@socialactive.app"],subject: "🚩\(myRideForDay.summary) by \(self.dtrData.profileInfo(profileID: myRideForDay.leader).name)",body: "<p>Hi,</p> This ride is inappropriate because,</p><p></p><p>&nbsp</p><p>&nbsp</p> Flagged \(myRideForDay.summary) created by \(self.dtrData.profileInfo(profileID: myRideForDay.leader).name) for \(monthAndDateName(dateCode: myRideForDay.dateCode))<p>Senders \(self.dtrData.profileID)<p> ", isHTML: true,isRideFeedback:true,rideFeedbackData:myRideForDay)
                } else {
                    VStack {
                        Text("This device is not setup to send email")
                            .padding()
                        Button(action: { self.showMailCompose = false }) {
                            Text("Dismiss")
                        }
                        .padding()
                    }
                }
            case .mapScreen:
                MapScreen(day: .constant(0),fullLocationName:.constant(""),isOnBoradingLat:.constant(0.0),isOnBoradingLon:.constant(0.0),canEditLocation:canEditLocation,startCountry: .constant(""), startPlace: .constant(""), isOnBoradingProcess: .constant(true), isUpdateRideLocation: $isUpdateRideLocation, showRideLocationScreen: $showRideLocationScreen, myRideForDay: RideEditViewModel.init(ride: myRideForDay))
            }
        }
        
        .background(EmptyView().sheet(isPresented: self.$shareRide) {
            Modal(isPresented: self.$shareRide, title: "Share Ride",isShareRideScreen: true) {
                ShareScreen(isPresented: self.$shareRide, ride: self.myRideForDay)
            }
            .onAppear{
                Analytics.logEvent(AnalyticsScreen.rideLink.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
            }
        })
    }
    
    
    var trailingButton: some View {
        HStack {
            if !rideFromPushNotification {
                if showPopUpFlgInapp != true {
                    if myRideForDay.leader != dtrData.profileID {
                        Button(action: {
                            showPopUpFlgInapp = true
                        }) {
                            HStack {
                                Image(ImageConstantsName.BckWithThreeDotsImg)
                            }
                        }
                    }else{
                        EmptyView()
                    }
                }else if showPopUpFlgInapp == true {
                    CustomPoupForLeaveRide(arrayCustom: $customArrayForRightMenu, show: $showPopUpFlgInapp) { (index) in
                        if index == "Flag as inappropriate" {
                            showPopUpFlgInapp = false
                            showMailCompose = true
                            activeSheet = .MailScreen
                        }else if index == "Leave Ride" {
                            if myRideForDay.fromAction == .leave {
                                self.dtrCommands.rideLeave(rideID: self.myRideForDay.id, dateCode: self.myRideForDay.dateCode) { (result) in
                                    myRideForDay.fromAction = DtrRideFromActionType(rawValue: 0)!
                                    presentModelOldHistoryScreen = true
                                    presentationMode.wrappedValue.dismiss()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    var leadingButton: some View {
        HStack {
            if !rideFromPushNotification {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    HStack {
                        Image(ImageConstantsName.BckWithColorImg)
                    }}
            }
        }
    }
    
    func pokeAnalytics() {
        if  UserStore.shared.shouldRecordComeBackRide == true {
            Analytics.logEvent(AnalyticsScreen.poke_come_back_ride.rawValue, parameters: ["source":dtrData.profile.id])
            UserStore.shared.shouldRecordComeBackRide = false
        }else if UserStore.shared.shouldRecordJoinWeekendRide == true {
            Analytics.logEvent(AnalyticsScreen.poke_join_weekend_ride.rawValue, parameters: ["source":dtrData.profile.id])
            UserStore.shared.shouldRecordJoinWeekendRide = false
        }else if UserStore.shared.shouldRecordGoDTRWeekendRide == true {
            Analytics.logEvent(AnalyticsScreen.poke_go_dtr_weekend_ride.rawValue, parameters: ["source":dtrData.profile.id])
            UserStore.shared.shouldRecordGoDTRWeekendRide = false
        }
    }
}


struct TrailingNavigationBarItems<InputContent: View>: ViewModifier {
    var inputContent: () -> InputContent
    func body(content: Content) -> some View {
        return content
            .overlay(inputContent(), alignment: .topTrailing)
    }
}

struct LeadingNavigationBarItems<InputContent: View>: ViewModifier {
    var inputContent: () -> InputContent
    func body(content: Content) -> some View {
        return content
            .overlay(inputContent(), alignment: .topLeading)
    }
}


extension View {
    func trailingNavigationBarItems<InputContent: View>(_ inputContent: @escaping () -> InputContent) -> some View {
        return self.modifier(TrailingNavigationBarItems(inputContent: inputContent))
    }
    
    func leadingNavigationBarItems<InputContent: View>(_ inputContent: @escaping () -> InputContent) -> some View {
        return self.modifier(LeadingNavigationBarItems(inputContent: inputContent))
    }
}


