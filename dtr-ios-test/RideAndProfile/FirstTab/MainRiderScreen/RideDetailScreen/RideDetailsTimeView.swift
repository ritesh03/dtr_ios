//
//  RideDetailsTimeView.swift
//  dtr-ios-test
//
//  Created by apple on 31/08/21.
//

import SwiftUI

struct RideDetailsTimeView: View {
    
    @Binding var myRideForDay: DtrRide
    
    var body: some View {
        Group {
            HStack{
                Image(systemName: "clock")
                Group {
                    Text((monthAndDateName(dateCode: myRideForDay.dateCode)))
                }
                
                Circle()
                    .frame(width: 5, height: 5)
                    .foregroundColor(Color.gray)
                
                if myRideForDay.details.startTime != "" {
                    Text(myRideForDay.details.startTime)
                }else{
                    Text("TBD")
                }
            }
        }
    }
}
