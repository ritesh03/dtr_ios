//
//  RideDetailsAllView.swift
//  dtr-ios-test
//
//  Created by apple on 31/08/21.
//

import SwiftUI

struct RideDetailsAllView: View {
    
    @Binding var day: Int
    @Binding var myRideForDay: DtrRide
    @Binding var showPopUp:Bool
    @Binding var shareRide:Bool
    @State private var showPopUpForError = false
    @Binding var showRideLocationScreen: Bool
    @Binding var rideWeekNameForCancelRide:String
    @Binding var rideFromNotifcaitonScreen:DtrRide
    @Binding var errorAndSuccessApiRetrunMessgae:String
    @Binding var showMapScreenDetai: Bool
    @Binding var dismissViewForceFully: Bool
    
    
    var body: some View {
        VStack(alignment:.leading,spacing:25) {
            Text(myRideForDay.summary).font(.system(size: 20, weight: .heavy, design: .default)).multilineTextAlignment(.leading)
                .fixedSize(horizontal: false, vertical: true).padding(.top)
            Group {
                RideDetailsNameWithCountView(myRideForDay: $myRideForDay)
                
                RidersHorizontalImages(ride: $myRideForDay, showLeader: true)
            }
            
            RideDetailsTimeView(myRideForDay: $myRideForDay)
            
            RideDetailsLocationView(myRideForDay: $myRideForDay, showRideLocationScreen: $showRideLocationScreen, showMapScreenDetai: $showMapScreenDetai)
            
            RideSkillIndicators(ride: $myRideForDay)
            
            RideDetailsVisiblityWithTagView(myRideForDay: $myRideForDay)
            
            RideDetailsTagsView(myRideForDay: $myRideForDay)
            
            RideDetailsJoinAndLeaveInviteView(day: $day, myRideForDay: $myRideForDay, showPopUpForError: $showPopUpForError, rideFromNotifcaitonScreen: $rideFromNotifcaitonScreen, errorAndSuccessApiRetrunMessgae: $errorAndSuccessApiRetrunMessgae, rideWeekNameForCancelRide: $rideWeekNameForCancelRide, showPopUp: $showPopUp, shareRide: $shareRide, dismissViewForceFully: $dismissViewForceFully)
            
            Group {
                RideDetailErrorSuccessApiView(showPopUpForError: $showPopUpForError, errorAndSuccessApiRetrunMessgae: $errorAndSuccessApiRetrunMessgae)
                
                RideDetailAboutSectionView(myRideForDay: $myRideForDay)
                
            }
            Spacer()
        }
        .padding(.horizontal)
    }
}
