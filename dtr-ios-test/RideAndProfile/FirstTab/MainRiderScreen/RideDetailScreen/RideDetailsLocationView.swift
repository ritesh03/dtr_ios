//
//  RideDetailsLocationView.swift
//  dtr-ios-test
//
//  Created by apple on 31/08/21.
//

import SwiftUI

struct RideDetailsLocationView: View {
    
    @Binding var myRideForDay: DtrRide
    @Binding var showRideLocationScreen: Bool
    @Binding var showMapScreenDetai: Bool
    
    var body: some View {
        Group {
            HStack {
                Image(systemName: "location")
                if myRideForDay.details.startPlace != "" {
                    Text(myRideForDay.details.startPlace)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                        .foregroundColor(myRideForDay.details.startPlace != "Location TBD" ? Color.init(ColorConstantsName.AccentColour): Color.white)
                        .onTapGesture {
                            if myRideForDay.details.startPlace != "Location TBD" && myRideForDay.details.startPlace != "TBD" {
                                showRideLocationScreen = true
                                showMapScreenDetai = true
                            }
                        }
                        .onTapGesture(count: 2) {
                            if myRideForDay.details.startPlace != "Location TBD" && myRideForDay.details.startPlace != "TBD" {
                                UIPasteboard.general.string = myRideForDay.details.startPlace
                            }
                        }
                }else{
                    Text("Location TBD")
                }
            }
        }
    }
}
