//
//  RideDetailsTagsView.swift
//  dtr-ios-test
//
//  Created by apple on 31/08/21.
//

import SwiftUI

struct RideDetailsTagsView: View {
    
    @Binding var myRideForDay: DtrRide
    
    var body: some View {
        if myRideForDay.details.visibility.rawValue != "public" {
            VStack {
                ShowTagUIView(tags: .constant(myRideForDay.details.tags),comeFromDetailScreen:false, ridesToDisplay: $myRideForDay)
            }
        }
    }
}
