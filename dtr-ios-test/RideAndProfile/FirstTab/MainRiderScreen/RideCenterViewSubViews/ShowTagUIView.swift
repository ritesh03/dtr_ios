//
//  ShowTagUIView.swift
//  dtr-ios-test
//
//  Created by apple on 19/08/21.
//

import SwiftUI

struct ShowTagUIView: View {
    @Binding var tags: [String]

    @State private var totalHeight = CGFloat.zero
    @State var comeFromDetailScreen = true
    @Binding var ridesToDisplay : DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        VStack {
            GeometryReader { geometry in
                self.generateContent(in: geometry)
            }
        }
        .frame(height: totalHeight)
    }
    
    private func item(for text: String,totalArry:[String]) -> some View {
        HStack {
            Text(text == "Public_Ride" ? "Public Ride" : text)
                .padding(.all, 5)
                .font(.body)
                .foregroundColor(text == "Public_Ride" ? Color.init(ColorConstantsName.LimeColour) : Color.white)
                .cornerRadius(5)
            if let lastTask = dtrData.tagsDirectory[dynamicMember:totalArry.last ?? ""].name {
                if lastTask == text {
                    if comeFromDetailScreen {
                        Image(ImageConstantsName.UnVectorImg).opacity(dtrData.profile.id == ridesToDisplay.leader ? 1 : 0)
                    }
                } else {
                    Circle()
                        .frame(width: 5, height: 5)
                        .foregroundColor(Color.gray)
                }
            }
        }
    }
 
    private func generateContent(in g: GeometryProxy) -> some View {
        var width = CGFloat.zero
        var height = CGFloat.zero

        return ZStack(alignment: .topLeading) {
            ForEach(self.tags, id: \.self) { tag in
                self.item(for: tag == "Public_Ride" ? "Public_Ride" : dtrData.tagsDirectory[dynamicMember:tag].name, totalArry: self.tags)
                    .padding([.horizontal, .vertical], 4)
                    .alignmentGuide(.leading, computeValue: { d in
                        if (abs(width - d.width) > g.size.width) {
                            width = 0
                            height -= d.height
                        }
                        let result = width
                        if tag == self.tags.last! {
                            width = 0
                        } else {
                            width -= d.width
                        }
                        return result
                    })
                    
                    .alignmentGuide(.top, computeValue: {d in
                        let result = height
                        if tag == self.tags.last! {
                            height = 0
                        }
                        return result
                    })
            }
        }.background(viewHeightReader($totalHeight))
    }

    private func viewHeightReader(_ binding: Binding<CGFloat>) -> some View {
        return GeometryReader { geometry -> Color in
            let rect = geometry.frame(in: .local)
            DispatchQueue.main.async {
                binding.wrappedValue = rect.size.height
            }
            return .clear
        }
    }
}
