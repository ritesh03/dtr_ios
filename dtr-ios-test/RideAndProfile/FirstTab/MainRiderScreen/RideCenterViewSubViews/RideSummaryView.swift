//
//  RideSummaryView.swift
//  dtr-ios-test
//
//  Created by apple on 17/08/21.
//

import SwiftUI

struct RideSummaryView: View {
    @Binding var ridesToDisplay : DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    var body: some View {
        Group {
            NavigationLink(destination: NotesEditScrn(isSummaryAvallible:ridesToDisplay.summary,isUpdateRide: .constant(false), scrennType: .constant("Summary"), myRideForDay: RideEditViewModel.init(ride:ridesToDisplay))) {
                HStack {
//                    Text(UserStore.shared.dyanamicLinkNotWorking).font(.system(size: 20, weight: .heavy, design: .default))
//                        .foregroundColor(returnColur())
                    Text(ridesToDisplay.summary).font(.system(size: 20, weight: .heavy, design: .default)).multilineTextAlignment(.leading)
                        .foregroundColor(returnColur())
                    if ridesToDisplay.riders.count <= 1 {
                        Group {
                            Image(ImageConstantsName.UnVectorImg)
                                .resizable()
                                .frame(width: 15,height: 15)
                        }.opacity(dtrData.profile.id == ridesToDisplay.leader ? 1 : 0)
                    }else if ridesToDisplay.riders.count > 1 {
                        if ridesToDisplay.leader ==  self.dtrData.profileID {
                            Group {
                                Image(ImageConstantsName.UnVectorImg)
                                    .resizable()
                                    .frame(width: 15,height: 15)
                            }.opacity(dtrData.profile.id == ridesToDisplay.leader ? 1 : 0)
                        }
                    }
                }
            }.disabled(dtrData.profile.id == ridesToDisplay.leader ? false :true)
        }
    }
    
    func returnColur () -> Color {
        if ridesToDisplay.details.visibility == .public {
            if dtrData.profile.id == ridesToDisplay.leader {
                let title = ridesToDisplay.summary.replacingOccurrences(of: " ", with: "")
                if title.lowercased() == "DowntoRide".lowercased() {
                    return .red
                }
            }
        }
        return .white
    }
}
