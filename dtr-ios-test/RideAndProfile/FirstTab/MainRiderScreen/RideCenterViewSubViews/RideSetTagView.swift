//
//  RideSetTagView.swift
//  dtr-ios-test
//
//  Created by apple on 18/08/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct RideSetTagView: View {
    
    @Binding var day : Int
    @Binding var ridesToDisplay : DtrRide
    @State private var tagsCollections =  [""]
    @State var originalItems = [DtrRideTags]()
    @State var tagsToDisplay = [String]()
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        Group {
            if ridesToDisplay.details.tags.contains("KfqH2PdeUY4nyp9V2nPQ") {
                RideIceManTagView(ridesToDisplay: $ridesToDisplay)
            }
            
            if tagsToDisplay.count > 0 {
                HStack {
                    NavigationLink(destination:RideTagScreen.init(selections: ridesToDisplay.details.tags, rideDetailDay: RideEditViewModel(ride: ridesToDisplay)),label: {
                        VStack {
                            ShowTagUIView(tags: .constant(tagsToDisplay), ridesToDisplay: $ridesToDisplay)
                        }
                    }).disabled(dtrData.profile.id == ridesToDisplay.leader ? false : true)
                }
            }else{
                if tagsToDisplay.count <= 0 {
                    if dtrData.profile.id == ridesToDisplay.leader {
                        NavigationLink(destination:RideTagScreen.init(selections: ridesToDisplay.details.tags, rideDetailDay: RideEditViewModel(ride: ridesToDisplay)),label: {
                            Text("Add ride tags" )
                                .foregroundColor(Color.init(ColorConstantsName.AccentTextColour))
                        }).disabled(dtrData.profile.id == ridesToDisplay.leader ? false : true)
                    } else{
                        EmptyView().frame(height: 0)
                    }
                }
            }
        }
        .onAppear{
            updateTagsWhenRideUpdate(tagsCollection: ridesToDisplay.details.tags)
            getTags()
        }
    }
    
    func updateTagsWhenRideUpdate(tagsCollection:[String]) {
        tagsCollections.removeAll()
        if let lastTask = dtrData.tagsDirectory[dynamicMember:tagsCollection.last ?? ""].name as? String {
            if lastTask == "Iceman Cometh Challenge" {
                tagsCollections = tagsCollection
                tagsCollections.removeLast()
            } else {
                if tagsCollection.contains("KfqH2PdeUY4nyp9V2nPQ") {
                    tagsCollections = tagsCollection
                    if let indesx = tagsCollection.firstIndex(where: { $0 == "KfqH2PdeUY4nyp9V2nPQ"}) {
                        tagsCollections.remove(at: indesx)
                    }
                }else{
                    tagsCollections = tagsCollection
                }
            }
        }
    }
    
    
    func getTags() {
        var tagItems = [DtrRideTags]()
        let callData: DataDict = ["name": "tag.read","objectID":DtrData.sharedInstance.profileID]
        DtrCommand.sharedInstance.dtrCommandWrapper(callData: callData) { result in
            let dictUsr = asDataDict(result.output)
            if let defaultTags = dictUsr["defaultTags"] as? NSArray {
                for i in 0..<defaultTags.count {
                    tagItems.append(DtrRideTags(data: defaultTags[i] as! DataDict))
                }
            }
            if let customTags = dictUsr["customTags"] as? NSArray {
                for i in 0..<customTags.count {
                    tagItems.append(DtrRideTags(data: customTags[i] as! DataDict))
                }
            }
            
            
            tagsToDisplay.removeAll()
            for tag in ridesToDisplay.details.tags {
                if tagItems.filter({$0.id == tag}).count != 0, tagItems.filter({$0.id == tag}).first?.status == 1 {
                    tagsToDisplay.append(tag)
                }
            }
        }
    }
}
