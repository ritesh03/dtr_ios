//
//  RideSetVisiblityView.swift
//  dtr-ios-test
//
//  Created by apple on 18/08/21.
//

import SwiftUI

struct RideSetVisiblityView: View {
    
    @Binding var ridesToDisplay : DtrRide
    @Binding var isVisbilityUpdate:Bool
    
    @ObservedObject var dtrData = DtrData.sharedInstance

    var body: some View {
        Group {
            NavigationLink(destination: RideVisiblity(dismissView:.constant(false),isUpdateRide: .constant(false),isVisbilityUpdate: $isVisbilityUpdate, myRideForDay: RideEditViewModel.init(ride:ridesToDisplay))){
                HStack {
                    Image(ImageConstantsName.VisiblityRideImg)
                        .resizable()
                        .frame(width: 20 , height: 15, alignment: .center)
                    if ridesToDisplay.details.visibility.rawValue == "public" {
                        Text("Public Ride")
                            .foregroundColor(Color.init(ColorConstantsName.LimeColour))
                    }else if ridesToDisplay.details.visibility.rawValue == "friends" {
                        Text("Friends of Friends")
                            .foregroundColor(.white)
                    }else if ridesToDisplay.details.visibility.rawValue == "followers" {
                        Text("Friends of Friends, Followers and Group members").fixedSize(horizontal: false, vertical: true).multilineTextAlignment(.leading)
                            .foregroundColor(.white)
                    }
                    Image(ImageConstantsName.UnVectorImg).opacity(dtrData.profile.id == ridesToDisplay.leader ? 1 : 0)
                }
            }.disabled(dtrData.profile.id == ridesToDisplay.leader ? false :true)
        }
    }
}
