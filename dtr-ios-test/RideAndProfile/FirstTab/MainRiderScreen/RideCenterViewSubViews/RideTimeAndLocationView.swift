//
//  RideTimeAndLocationView.swift
//  dtr-ios-test
//
//  Created by apple on 18/08/21.
//

import SwiftUI
import MapKit
import MobileCoreServices

struct RideTimeAndLocationView: View {
    
    @Binding var ridesToDisplay : DtrRide
    @Binding var showTimrScreen:Bool
    @Binding var showPublicLocationChange :Bool
    @State private var showingSheet = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        Group {
            VStack(spacing:30) {
                Group {
                    HStack {
                        Image(ImageConstantsName.HomeTimeImg)
                            .resizable()
                            .frame(width: 18,height: 18)
                            .foregroundColor(.white)
                        Text(ridesToDisplay.details.startTime == "" ? "Time TBD" : ridesToDisplay.details.startTime)
                            .foregroundColor(returnColur(index: 0))
                            .padding(.trailing)
                        Group {
                            Image(ImageConstantsName.UnVectorImg)
                                .resizable()
                                .frame(width: 15,height: 15)
                        }.opacity(dtrData.profile.id == ridesToDisplay.leader ? 1 : 0)
                        Spacer()
                    }
                }.onTapGesture {
                    if dtrData.profile.id == ridesToDisplay.leader {
                        showTimrScreen.toggle()
                    }
                }
                
                Group {
                    HStack {
                        Image(ImageConstantsName.HomeLocationImg)
                            .resizable()
                            .frame(width: 16,height: 20)
                            .foregroundColor(.white)
                        Text(ridesToDisplay.details.startPlace == "" ? "Location TBD" : ridesToDisplay.details.startPlace)
                            .foregroundColor(returnColur(index: 1))
                            .fixedSize(horizontal: false, vertical: true)
                        Group {
                            Image(ImageConstantsName.UnVectorImg)
                                .resizable()
                                .frame(width: 15,height: 15)
                        }.opacity(dtrData.profile.id == ridesToDisplay.leader ? 1 : 0).onTapGesture {
                            if dtrData.profile.id == ridesToDisplay.leader {
                                showPublicLocationChange = true
                            }
                        }
                        Spacer()
                    }
                }.onTapGesture {
                    if ridesToDisplay.details.hasStartPlace {
                        showingSheet = true
                    }
                }
            }
            .actionSheet(isPresented: $showingSheet) {
                ActionSheet(title: Text("\(ridesToDisplay.details.startPlace)"), buttons: [.default(Text("Copy Location"), action: {
                    UIPasteboard.general.setValue(ridesToDisplay.details.startPlace,forPasteboardType: kUTTypePlainText as String)
                    showingSheet = false
                }),.default(Text("Open in Apple Map"),action: {
                    openMapAppDefault(latDouble: ridesToDisplay.details.startPlaceLat, LogDouble: ridesToDisplay.details.startPlaceLon, placeName: ridesToDisplay.details.startPlace)
                    showingSheet = false
                }),.default(Text("Open in Google Map"),action: {
                    showingSheet = false
                    openGoogleMapLocation()
                }),.default(Text("Cancel"),action: {
                    showingSheet = false
                })])
            }
        }
    }

    
    func openGoogleMapLocation(){
        let googleMapsInstalled = UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
        if googleMapsInstalled {
            UIApplication.shared.open(URL(string: "comgooglemaps-x-callback://" +
                                          "?daddr=\(ridesToDisplay.details.startPlaceLat),\(ridesToDisplay.details.startPlaceLon)&directionsmode=bicycling&zoom=17")!)
        } else {
            let url = URL(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(String(describing: ridesToDisplay.details.startPlaceLat)),\(String(describing: ridesToDisplay.details.startPlaceLon))")
            if #available(iOS 10, *) {
                UIApplication.shared.open(url!)
            } else {
                UIApplication.shared.openURL(url!)
            }
        }
    }
    
    
    
    
    func openMapAppDefault(latDouble:Double,LogDouble:Double,placeName:String){
        let latitude: CLLocationDegrees = latDouble
        let longitude: CLLocationDegrees = LogDouble
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = placeName
        mapItem.openInMaps(launchOptions: options)
    }
    
    func returnColur(index:Int) -> Color {
        if ridesToDisplay.details.visibility == .public {
            if dtrData.profile.id == ridesToDisplay.leader {
                if index == 0 {
                    if !ridesToDisplay.details.hasStartTime {
                        return .red
                    }
                }else{
                    if !ridesToDisplay.details.hasStartPlace {
                        return .red
                    }
                }
            }
        }
        return .white
    }
}
