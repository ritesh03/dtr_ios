//
//  RideCoverImageView.swift
//  dtr-ios-test
//
//  Created by apple on 17/08/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct RideCoverImageView: View {
    
    @Binding var ridesToDisplay : DtrRide
    @Binding var cameraButtonClicked : Bool
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            if ridesToDisplay.rideCoverImage != CommonAllString.BlankStr {
                WebImage(url:URL(string:ridesToDisplay.rideCoverImage), isAnimating: .constant(true))
                    .purgeable(true)
                    .placeholder(Image(ImageConstantsName.MainRideEditCoverImg))
                    .renderingMode(.original)
                    .resizable()
                    .indicator(.activity)
                    .scaledToFill()
                    .frame(width:UIScreen.screenWidth-30,height: 120, alignment: .center)
                    .clipped()
                    .cornerRadius(20, corners: [.topLeft, .topRight])
                    .padding(.bottom)
            }else{
                Image(ImageConstantsName.MainCoverBgImg)
                    .resizable()
                    .scaledToFill()
                    .frame(width:UIScreen.screenWidth-30,height: 120, alignment: .center)
                    .clipped()
                    .cornerRadius(20, corners: [.topLeft, .topRight])
                    .padding(.bottom)
            }
            
            NavigationLink(destination: UplodImageForRide(myRideForDay: RideEditViewModel.init(ride: ridesToDisplay))){
                Image(ImageConstantsName.CameraBtbImg)
                    .frame(width: 40, height: 40)
                    .cornerRadius(20)
                    .padding()
                    .padding(.bottom,5)
            }.opacity(dtrData.profile.id == ridesToDisplay.leader ? 1 : 0)
        }
    }
}
