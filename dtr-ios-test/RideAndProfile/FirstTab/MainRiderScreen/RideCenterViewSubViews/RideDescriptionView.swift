//
//  RideDescriptionView.swift
//  dtr-ios-test
//
//  Created by apple on 18/08/21.
//

import SwiftUI

struct RideDescriptionView: View {
    @Binding var ridesToDisplay : DtrRide
    @Binding var screenWidth :CGFloat
    @Binding var heightText :CGFloat
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        HStack {
            if ridesToDisplay.details.notes == "" {
                NavigationLink(destination: NotesEditScrn(isSummaryAvallible:ridesToDisplay.details.notes,isUpdateRide: .constant(false), scrennType: .constant("Notes"), myRideForDay: RideEditViewModel.init(ride:ridesToDisplay))) {
                    if dtrData.profile.id == ridesToDisplay.leader  {
                        Text("Add details about this ride")
                            .foregroundColor(Color.init(ColorConstantsName.AccentTextColour))
                            .padding(.bottom)
                    }else{
                        EmptyView().frame(height: 0)
                    }
                }
                Spacer()
            } else {
                VStack(alignment: .leading) {
                    VStack {
                        ZStack{
                            Color.gray
                        }
                    }
                    .frame(height: 0.5)
                    HStack{
                        Text("ABOUT THIS RIDE").font(.system(size: 14, weight: .bold, design: .default))
                            .foregroundColor(Color.gray).padding(.vertical,10)
                        NavigationLink(destination: NotesEditScrn(isSummaryAvallible:ridesToDisplay.details.notes,isUpdateRide: .constant(false), scrennType: .constant("Notes"), myRideForDay: RideEditViewModel.init(ride:ridesToDisplay))) {
                            Image(ImageConstantsName.UnVectorImg)
                        }.opacity(dtrData.profile.id == ridesToDisplay.leader ? 1 : 0).disabled(dtrData.profile.id == ridesToDisplay.leader ? false :true)
                    }
                    TextWithAttributedString(heightText:$heightText,attributedString: NSAttributedString(string: ridesToDisplay.details.notes), fixedWidth:screenWidth - 40 * 2)
                }.padding(.bottom)
            }
        }
    }
}
