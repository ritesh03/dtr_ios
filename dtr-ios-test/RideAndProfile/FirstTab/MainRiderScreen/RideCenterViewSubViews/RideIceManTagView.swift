//
//  RideIceManTagView.swift
//  dtr-ios-test
//
//  Created by apple on 02/09/21.
//

import SwiftUI

struct RideIceManTagView: View {
    
    @Binding var ridesToDisplay : DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        Group {
            VStack(alignment:.leading) {
                HStack {
                    Image(ImageConstantsName.IceMainLogoImg)
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 35, height: 35)
                        .clipShape(Circle())
                        .padding(1)
                    NavigationLink(destination:RideTagScreen.init(selections: ridesToDisplay.details.tags, rideDetailDay: RideEditViewModel(ride: ridesToDisplay)),label: {
                        Text("Iceman Cometh Challenge")
                            .foregroundColor(.white)
                        Image(ImageConstantsName.UnVectorImg).opacity(dtrData.profile.id == ridesToDisplay.leader && ridesToDisplay.details.tags.count == 1 ? 1 : 0)
                    }).disabled(dtrData.profile.id == ridesToDisplay.leader && ridesToDisplay.details.tags.count == 1 ? false : true)
                }
            }
        }
    }
}
