//
//  CustomPopUpForRideOptions.swift
//  dtr-ios-test
//
//  Created by apple on 20/08/21.
//

import SwiftUI

struct CustomPopUpForRideOptions: View {
    
    @Binding var ridesToDisplay:DtrRide
    @Binding var arrayCustom:[String]
    @Binding var showPopUp:Bool
    @Binding var showAlertNotDTR:Bool
    @Binding var presentModelOldHistoryScreen:Bool
    @Binding var comeFromCustomView:Bool
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    
    
    var body: some View {
        CustomWidthPoup(arrayCustom: $arrayCustom, show: $showPopUp,ridesToDisplay:ridesToDisplay,comeFromCustomView:comeFromCustomView) { (index) in
            if index == "Edit Ride" {
            }else if index == DayContainerString.NotDtrAnyMoreStr || index == DayContainerString.CancelRideStr {
                if self.ridesToDisplay.riders.count > 1 {
                    showAlertNotDTR = true
                } else {
                    self.dtrCommands.rideCancel(rideID: self.ridesToDisplay.id,dateCode:self.ridesToDisplay.dateCode, message: "") { result in
                        result.debug(methodName:"rideCancel")
                    }
                }
            }else if index == "Leave Ride" {
                self.dtrCommands.rideLeave(rideID: self.ridesToDisplay.id, dateCode: self.ridesToDisplay.dateCode) { (result) in
                    result.debug(methodName:"rideLeave")
                    presentModelOldHistoryScreen = true
                }
            }
        }
    }
}
