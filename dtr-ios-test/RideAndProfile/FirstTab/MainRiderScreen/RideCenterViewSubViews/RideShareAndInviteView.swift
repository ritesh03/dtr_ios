//
//  RideShareAndInviteView.swift
//  dtr-ios-test
//
//  Created by apple on 17/08/21.
//

import SwiftUI

struct RideShareAndInviteView: View {
    @Binding var day : Int
    @Binding var ridesToDisplay : DtrRide
    @Binding var shareRide:Bool
    @Binding var showPopUp:Bool
    @State var comeFromCutomScreen = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        HStack(alignment:.center)  {
            NavigationLink(destination:InviteMainScreen(day: $day, showDeatilPopUp: .constant(false),ride:$ridesToDisplay, profile: dtrData.profile,  resetLocalValue: true, selectFriends: [""], selectFollowers: [""], selectGroupMembers: [""]),label: {
                Text("INVITE")
                    .frame(width: 120,height:40)
                    .foregroundColor(.white)
                    .background(Color.init(ColorConstantsName.AccentColour))
                    .cornerRadius(40)
            })
            
            Button(action: {
                shareRide = true
            }) {
                Text("SHARE")
                    .frame(width: 120,height:40)
                    .foregroundColor(.white)
                    .background(Color.init(ColorConstantsName.AccentColour))
                    .cornerRadius(40)
            }
            
            Button(action: {
                showPopUp = true
            }) {
                Image(comeFromCutomScreen == false ? "DTR-Outlinedbutton": "DTR-OutlinedbuttonBlackDot")
                    .resizable()
                    .frame(width: 40, height: 40)
            }.padding(.leading)
        }
        //.padding(.horizontal)
    }
}
