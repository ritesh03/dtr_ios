//
//  RiderCenterView.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/01/21.
//

import SwiftUI
import Firebase

struct RiderCenterView: View {
    @Binding var day : Int
    @Binding var screenWidth : CGFloat
    
    @Binding var showTimrScreen:Bool
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @Binding var ridesToDisplay : DtrRide!
    @Binding var showPopUp: Bool
    @Binding var showAlertNotDTR : Bool
    @Binding var shareRide : Bool
    @Binding var arrayCustom:[String]
    @Binding var presentModelOldHistoryScreen : Bool
    @State var UpdatedRideDetailForImages : DtrRide = DtrRide.default
    
    @State private var cameraButtonClicked = false
    
    @State private var showPublicLocationChange = false
    @State private var showRideLocationScreen = false
    @State var isVisbilityUpdate = false
    @State private var isUpdateRideLocation = false
    
    
    @State private var startPlace = CommonAllString.BlankStr
    @State private var startCountry = CommonAllString.BlankStr
    @State private var locationName = CommonAllString.BlankStr
    @State var startPlaceLat = 0.0
    @State var startPlaceLon = 0.0
    @State private var tagsCollection = [CommonAllString.BlankStr]
    
    
    var body: some View {
        ZStack {
            if ridesToDisplay != nil {
                VStack (alignment: .center) {
                    RideCoverImageView(ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default),cameraButtonClicked:$cameraButtonClicked)
                    VStack (alignment: .leading , spacing: 20) {
                        Group {
                            RideSummaryView(ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default))
                            
                            RideShareAndInviteView(day:$day,ridesToDisplay:$ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default),shareRide:$shareRide,showPopUp:$showPopUp)
                        }
                        
                        Group {
                            ZStack{
                                CustomPopUpForRideOptions(ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default), arrayCustom: $arrayCustom, showPopUp: $showPopUp, showAlertNotDTR: $showAlertNotDTR, presentModelOldHistoryScreen: $presentModelOldHistoryScreen,comeFromCustomView:.constant(false)).offset(x: 100, y: -10)
                            }.frame(height: 10)
                        }
                        
                        if ridesToDisplay.leader == dtrData.profileID {
                            Group {
                                VStack {
                                    if isVisbilityUpdate ||  ridesToDisplay.details.visibility.rawValue == "public" {
                                        ShowPublicTagError(rideDetailDay:$ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default))
                                    }
                                }
                            }
                        }
                        
                        Group {
                            RideNameAndProfileImageView(ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default), UpdatedRideDetailForImages: $UpdatedRideDetailForImages)
                            
                            RideTimeAndLocationView(ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default), showTimrScreen: $showTimrScreen, showPublicLocationChange: $showPublicLocationChange)
                        }
                        
                        Group {
                            if (ridesToDisplay.details.skillA || ridesToDisplay.details.skillB || ridesToDisplay.details.skillC || ridesToDisplay.details.skillD || ridesToDisplay.details.skillS) {
                                NavigationLink(destination: SetSkillScrren(isUpdateRide: .constant(false), myRideForDay: RideEditViewModel.init(ride:ridesToDisplay))) {
                                    RideSkillIndicators(ride: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default),comeFromHomeScreen:true)
                                }.disabled(dtrData.profile.id == ridesToDisplay.leader ? false : true)
                            }else{
                                if dtrData.profile.id == ridesToDisplay.leader {
                                    NavigationLink(destination: SetSkillScrren(isUpdateRide: .constant(false), myRideForDay: RideEditViewModel.init(ride:ridesToDisplay))) {
                                        Text("Set skill level")
                                            .foregroundColor(returnColur())
                                    }.opacity(dtrData.profile.id == ridesToDisplay.leader ? 1 : 0).disabled(dtrData.profile.id == ridesToDisplay.leader ? false : true)
                                }
                            }
                        }
                        
                        if ridesToDisplay.riders.count > 1 {
                            if ridesToDisplay.leader == dtrData.profile.id {
                                RideSetVisiblityView(ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default), isVisbilityUpdate: $isVisbilityUpdate).padding(.vertical,5)
                                RideSetTagView(day: $day, ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default))
                            }else{
                                if ridesToDisplay.details.visibility.rawValue != "public" {
                                    RideSetVisiblityView(ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default), isVisbilityUpdate: $isVisbilityUpdate).padding(.vertical,5)
                                }
                                if tagsCollection.count > 0 {
                                    VStack {
                                        ShowTagUIView(tags: $tagsCollection, ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default))
                                    }
                                }
                            }
                        }else{
                            RideSetVisiblityView(ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default), isVisbilityUpdate: $isVisbilityUpdate).padding(.vertical,5)
                            RideSetTagView(day: $day, ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default))
                        }
                        
                        Group {
                            RideDescriptionView(ridesToDisplay: $ridesToDisplay.toUnwrapped(defaultValue: DtrRide.default),screenWidth:$screenWidth, heightText: .constant(0.0))
                        }
                    }
                    .padding(.horizontal)
                }
                .background(Color.init(ColorConstantsName.HeaderBgColour))
                .cornerRadius(20)
                .clipped()
            }
        }
        
        .onAppear {
            if ridesToDisplay != nil {
                updateRideTagsWithUpdation()
                UserDefaults.standard.removeObject(forKey: "MyRideDetailLocally")
                UserStore.shared.saveLocallyRideDetail(myRideForDay: ridesToDisplay, isSetData: true) { response in }
                print("Updated Ride Details Without iceMan")
            }
        }
        
        .onReceive(self.dtrData.$myRidesByDate) { rideDict in
            let dict = rideDict[dateCode(offset: day)]
            UpdatedRideDetailForImages = DtrRide.default
            if dict != nil {
                self.ridesToDisplay = dict!
                UpdatedRideDetailForImages = self.ridesToDisplay
                updateRideTagsWithUpdation()
            }
        }
        
        .fullScreenCover(isPresented: $showPublicLocationChange) {
            MapScreen(day: $day,fullLocationName:$locationName,isOnBoradingLat:$startPlaceLat,isOnBoradingLon:$startPlaceLon,canEditLocation:true,startCountry: $startCountry, startPlace: $startPlace, isOnBoradingProcess: .constant(true), isUpdateRideLocation: $isUpdateRideLocation, showRideLocationScreen: $showRideLocationScreen, updateRide: true, myRideForDay: RideEditViewModel.init(ride: ridesToDisplay)) {
                if startPlaceLat != 0.0 && startPlaceLon != 0.0 {
                    updateRideLocationAndReset(hasStartPlace: true, startPlace: locationName, startPlaceLat: startPlaceLat, startPlaceLon: startPlaceLon)
                }else{
                    updateRideLocationAndReset(hasStartPlace: false, startPlace: locationName, startPlaceLat: startPlaceLat, startPlaceLon: startPlaceLon)
                }
            }
        }
        
        .background(EmptyView().sheet(isPresented: self.$shareRide) {
            Modal(isPresented: self.$shareRide, title: "Share Ride",isShareRideScreen: true) {
                ShareScreen(isPresented: self.$shareRide, ride: self.ridesToDisplay)
            }
            .onAppear{
                Analytics.logEvent(AnalyticsScreen.rideLink.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
            }
        })
    }
    
    func returnColur () -> Color {
        if ridesToDisplay.details.visibility == .public {
            if dtrData.profile.id == ridesToDisplay.leader {
                if (!ridesToDisplay.details.skillA || !ridesToDisplay.details.skillB || !ridesToDisplay.details.skillC || !ridesToDisplay.details.skillD || !ridesToDisplay.details.skillS) {
                    return .red
                }
            }
        }
        return Color.init(ColorConstantsName.AccentTextColour)
    }
    
    func updateRideLocationAndReset(hasStartPlace:Bool,startPlace:String,startPlaceLat:Double,startPlaceLon:Double){
        dtrCommands.rideUpdateLocation(rideID:ridesToDisplay.id,data:["details":["tags":ridesToDisplay.details.tags,"hasStartPlace":hasStartPlace,"hasStartTime":ridesToDisplay.details.hasStartTime,"notes":ridesToDisplay.details.notes,"skillA":ridesToDisplay.details.skillA,"skillB":ridesToDisplay.details.skillB,"skillC":ridesToDisplay.details.skillC,"skillD":ridesToDisplay.details.skillD,"skillS":ridesToDisplay.details.skillS,"startPlace":startPlace,"startPlaceLat":startPlaceLat,"startPlaceLon":startPlaceLon,"startTime":ridesToDisplay.details.startTime,"visibility":ridesToDisplay.details.visibility.rawValue]]){ result in
            Analytics.logEvent(AnalyticsEvent.rideUpdateLocation.rawValue, parameters: ["rideID": ridesToDisplay.id])
            result.debug(methodName: "rideUpdateLocation")
        }
    }
    
    func updateRideTagsWithUpdation() {
        tagsCollection.removeAll()
        if ridesToDisplay.details.tags.count > 0 {
            tagsCollection = ridesToDisplay.details.tags
            if ridesToDisplay.details.visibility.rawValue == "public" {
                if tagsCollection.count > 0  {
                    if ridesToDisplay.leader != dtrData.profile.id {
                        if !self.tagsCollection.contains("Public_Ride") {
                            self.tagsCollection.insert("Public_Ride", at: 0)
                            if let indesx = tagsCollection.firstIndex(where: { $0 == "KfqH2PdeUY4nyp9V2nPQ"}) {
                                tagsCollection.remove(at: indesx)
                            }
                        }
                    }
                }
            }
            if let indesx = tagsCollection.firstIndex(where: { $0 == "KfqH2PdeUY4nyp9V2nPQ"}) {
                tagsCollection.remove(at: indesx)
            }
        }
    }
}

extension Binding {
    func toUnwrapped<T>(defaultValue: T) -> Binding<T> where Value == Optional<T>  {
        Binding<T>(get: { self.wrappedValue ?? defaultValue }, set: { self.wrappedValue = $0 })
    }
}
