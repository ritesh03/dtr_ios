//
//  BottomChatViewMini.swift
//  dtr-ios-test
//
//  Created by apple on 20/08/21.
//

import SwiftUI

struct BottomChatViewMini: View {
    
    @Binding var day: Int
    @Binding var totalCountUnreadMesg: Int
    @Binding var showMainChatScreen: Bool
    @Binding var sender: DtrProfile
    @Binding var message: DtrRideMessage
    @ObservedObject var dtrData = DtrData.sharedInstance    
    
    var body: some View {
        VStack {
            Spacer()
            if dtrData.myRidesByDate[dateCode(offset: self.day)] != nil {
                if dtrData.myRidesByDate[dateCode(offset: self.day)]!.fromAction == .leave ||  dtrData.myRidesByDate[dateCode(offset: self.day)]!.fromAction == .cancel {
                    if dtrData.myRidesByDate[dateCode(offset: self.day)]!.riders.count > 1 {
                        VStack(alignment:.leading) {
                            HStack {
                                Text(ChatScreenSenderString.RideUpdateStr)
                                    .foregroundColor(.black)
                                    .fontWeight(.bold)
                                if totalCountUnreadMesg > 0 {
                                    ZStack {
                                        Circle()
                                            .frame(width: 20, height:20)
                                            .foregroundColor(Color.init(UIColor.init(displayP3Red: 255/255, green: 92/255, blue: 210/255, alpha: 1.0)))
                                        ZStack {
                                            Text("\(totalCountUnreadMesg)")
                                                .font(.system(size: 12,weight: .bold))
                                        }
                                    }
                                }
                                Spacer()
                                Image(ImageConstantsName.UpArrowImg)
                                    .resizable()
                                    .frame(width: 20,height: 20)
                            }
                            .onTapGesture {
                                totalCountUnreadMesg = 0
                                self.showMainChatScreen = true
                            }
                            Divider()
                            if let range = self.message.message.range(of: DayContainerString.DetailStr) {
                                let locationGet = self.message.message[range.upperBound...].trimmingCharacters(in: .whitespaces)
                                Text(CommonAllString.DtrCommonStr + self.message.message.replacingOccurrences(of: locationGet, with: ""))
                                    .lineLimit(4)
                                    .fixedSize(horizontal: false, vertical: true)
                                    .foregroundColor(.black)
                                    .opacity(0.65)
                            } else {
                                Text("\(sender.name == "" ? CommonAllString.DtrStr: sender.name): \(message.message)")
                                    .lineLimit(4)
                                    .fixedSize(horizontal: false, vertical: true)
                                    .foregroundColor(.black)
                                    .opacity(0.65)
                            }
                        }
                        .padding()
                        .background(RoundedRectangle(cornerRadius: 20)
                        .fill(Color.init(ColorConstantsName.HeaderForGrandColour))
                        )
                    }
                }
            }
        }.padding(EdgeInsets(top: 0, leading: 20, bottom: 20, trailing: 20))
    }
}
