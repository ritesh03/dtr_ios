//
//  RideNameAndProfileImageView.swift
//  dtr-ios-test
//
//  Created by apple on 18/08/21.
//

import SwiftUI

struct RideNameAndProfileImageView: View {
    
    @Binding var ridesToDisplay : DtrRide
    @Binding var UpdatedRideDetailForImages: DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        Group {
            if ridesToDisplay.riders.count > 1 {
                if ridesToDisplay.fromAction == .leave || ridesToDisplay.fromAction == .cancel {
                    Text("Riding with \(self.dtrData.profileInfo(profileID: ridesToDisplay.leader).name)")
                        .foregroundColor(.gray)
                        .font(.system(size: 14))
                }else{
                    Text("Riding with \(self.dtrData.profileInfo(profileID: ridesToDisplay.leader).name)").font(.system(size: 14))
                        .foregroundColor(.gray)
                        .fixedSize(horizontal: false, vertical: true)
                }
            }else{
                Text("Riding with \(self.dtrData.profileInfo(profileID: ridesToDisplay.leader).name)").font(.system(size: 14))
                    .foregroundColor(.gray)
                    .fixedSize(horizontal: false, vertical: true)
            }
            
            RidersHorizontalImages(ride: $UpdatedRideDetailForImages, showLeader: true)
            
        }
    }
}
