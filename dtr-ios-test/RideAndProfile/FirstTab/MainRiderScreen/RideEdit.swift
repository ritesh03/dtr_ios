//
//  RideEdit.swift
//  dtr-ios-test
//
//  Created by Mobile on 02/02/21.
//

import SwiftUI
import Firebase
import SDWebImage
import SDWebImageSwiftUI

struct RideEdit: View {
    
    @Binding var day : Int
    @Binding var myRideForDay : DtrRide!
    @State private var textStyle = UIFont.TextStyle.body
    @Binding var showingAlertForFinalEdit : Bool
    @State var isVisbilityUpdate = false
    @State private var showTimrScreen = false
    @State private var isUpdateRide = false
    @State private var showInvitePopUp = false
    @State private var shareRideDetail = false
    @State private var isUpdateRideTime = false
    @State private var showUpdateImageScrn = false
    @State private var isUpdateRideLocation = false
    @State private var showRideLocationScreen = false
    @State private var showPopContentForRideAction = ""
    @State private var showPopCancelLeaveRide = false
    @State private var showRiderList = false
    @State private var showRiderProfileScreen = false
    @State private var updateRideDetailAfterShare = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var rideDetailDay : RideEditViewModel
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State private var  arrayRiderSendPush = [""]
    @State private var image = Image(ImageConstantsName.MainRideEditCoverImg)
    @State private var showInviteScreen = false
    
    @State private var showAlertNotDTR = false
    @State var activeSheet: ActiveSheet?
    var body: some View {
        ZStack {
            ScrollView {
                VStack {
                    if myRideForDay != nil {
                        if rideDetailDay.rideEdit.rideCoverImage != "" {
                            returnRideCoverImage(rideImageUrl:myRideForDay.rideCoverImage,className:"rideEdit")
                        }else{
                            Image(ImageConstantsName.MainRideEditCoverImg)
                                .resizable()
                                .scaledToFill()
                                .frame(width:UIScreen.screenWidth,height: 180, alignment: .center)
                                .clipped()
                        }
                    }

                    VStack(alignment:.leading,spacing:25) {
                        Group {
                            HStack (alignment: .top) {
                                Text(rideDetailDay.rideEdit.summary).font(.system(size: 20, weight: .heavy, design: .default)).multilineTextAlignment(.leading)
                                    .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                                NavigationLink(destination: NotesEditScrn(isSummaryAvallible:rideDetailDay.rideEdit.summary,isUpdateRide: $isUpdateRide, scrennType: .constant("Summary"), myRideForDay: rideDetailDay)){
                                    Image(ImageConstantsName.UnVectorImg)
                                }
                            }

                            Group {
                                if myRideForDay != nil {
                                    if rideDetailDay.rideEdit.riders.count > 1 {
                                        if rideDetailDay.rideEdit.fromAction == .leave || rideDetailDay.rideEdit.fromAction == .cancel {
                                            Text("Riding with \(self.dtrData.profileInfo(profileID: rideDetailDay.rideEdit.leader).name)")
                                                .foregroundColor(.gray)
                                                .font(.system(size: 14))
                                        }else{
                                            Text(self.dtrData.profileInfo(profileID: rideDetailDay.rideEdit.leader).name).font(.system(size: 14))
                                                .foregroundColor(.gray)
                                                .fixedSize(horizontal: false, vertical: true)
                                        }
                                    }else{
                                        Text(self.dtrData.profileInfo(profileID: rideDetailDay.rideEdit.leader).name).font(.system(size: 14))
                                            .foregroundColor(.gray)
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                }
                            }

                            Group {
                                RidersHorizontalImages(ride: $rideDetailDay.rideEdit, showLeader: true)
                            }

                            Group {
                                VStack {
                                    if isVisbilityUpdate ||  rideDetailDay.rideEdit.details.visibility.rawValue == "public" {
                                        ShowPublicTagError(rideDetailDay:$rideDetailDay.rideEdit)
                                    }
                                }
                            }

                            Group {
                                HStack {
                                    Image( "DTR-clockicon")
                                    Text(rideDetailDay.rideEdit.details.startTime)
                                    if rideDetailDay.rideEdit.details.startTime == "" {
                                        Text("Time TBD")
                                    }
                                    Image(ImageConstantsName.UnVectorImg)
                                }.onTapGesture {
                                    UserStore.shared.updateOnlyOneTimeForINviteAndShare = true
                                    showTimrScreen.toggle()
                                }

                                HStack {
                                    Image( "DTR-locationicon")
                                    Text(rideDetailDay.rideEdit.details.startPlace)
                                    if rideDetailDay.rideEdit.details.startPlace == "" {
                                        Text("Location TBD")
                                    }
                                    Image(ImageConstantsName.UnVectorImg)
                                }.onTapGesture {
                                    activeSheet = .location
                                }
                            }

                            Group {
                                NavigationLink(destination: SetSkillScrren(isUpdateRide: $isUpdateRide, myRideForDay: rideDetailDay)) {
                                    HStack {
                                        skillsNotesPresentation(ride: rideDetailDay.rideEdit)
                                        Text("Set skill level")
                                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                                        Spacer()
                                    }
                                }
                            }

                            Group {
                                NavigationLink(destination: RideVisiblity(dismissView:.constant(false),isUpdateRide: $isUpdateRide,isVisbilityUpdate: $isVisbilityUpdate, myRideForDay: rideDetailDay)){
                                    HStack {
                                        if rideDetailDay.rideEdit.details.visibility.rawValue == "public" {
                                            Image(ImageConstantsName.PublicImg)
                                                .resizable()
                                                .frame(width: 30 , height: 30, alignment: .center)
                                            Text("Public Ride")
                                                .foregroundColor(Color.init(ColorConstantsName.LimeColour))
                                        }else if rideDetailDay.rideEdit.details.visibility.rawValue == "friends" {
                                            Image(ImageConstantsName.FriendOfFriendImg)
                                                .resizable()
                                                .frame(width: 30 , height: 30, alignment: .center)
                                            Text("Friends of Friends")
                                                .foregroundColor(.white)
                                        }else if rideDetailDay.rideEdit.details.visibility.rawValue == "followers" {
                                            Image(ImageConstantsName.FriendAndFollowersImg)
                                                .resizable()
                                                .frame(width: 30 , height: 30, alignment: .center)
                                            Text("Friends of Friends, Followers and Group members")
                                                .foregroundColor(.white)
                                        }
                                        Image(ImageConstantsName.UnVectorImg)
                                    }
                                }
                            }

                            Group {
                                HStack {
                                    Button(action: {
                                        showInviteScreen = true
                                        UserStore.shared.saveLocallyRideDetail(myRideForDay: rideDetailDay.rideEdit, isSetData: true) { response in }
                                        UserStore.shared.updateOnlyOneTimeForINviteAndShare = true
                                    }) {
                                        Text("INVITE")
                                            .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: 50, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealHeight: 50, maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                            .foregroundColor(.white)
                                            .background(Color.init(ColorConstantsName.AccentColour))
                                            .cornerRadius(UIScreen.screenWidth*0.8/2)
                                    }

                                    Button(action: {
                                        shareRideDetail = true
                                        activeSheet = .welcome
                                    }) {
                                        Text("SHARE")
                                            .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: 50, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealHeight: 50, maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                            .foregroundColor(.white)
                                            .background(Color.init(ColorConstantsName.AccentColour))
                                            .cornerRadius(UIScreen.screenWidth*0.8/2)
                                    }
                                }
                            }
                        }
                        Group{
                            VStack(alignment: .leading, spacing: 10){
                                Divider()
                                    .background(Color.white)
                                Group {
                                    if rideDetailDay.rideEdit.details.notes == "" {
                                        VStack(alignment: .leading){
                                            Text("ABOUT THIS RIDE")
                                            TextView(comesFromDetailAndEditScreen: .constant(true), isEditable: .constant(false), text:  .constant(rideDetailDay.rideEdit.details.notes), textStyle:$textStyle, comesFromEditScreen: .constant(false), didStartEditing: .constant(false),placeHolderText:"")

                                            NavigationLink(destination: NotesEditScrn(isUpdateRide: $isUpdateRide, scrennType: .constant("Notes"), myRideForDay: rideDetailDay)){
                                                Text("Add details about this ride")
                                                    .padding(.bottom,50)
                                                    .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                                            }
                                        }
                                    }else{
                                        VStack(alignment: .leading) {
                                            HStack{
                                                Text("ABOUT THIS RIDE")
                                                NavigationLink(destination: NotesEditScrn(isUpdateRide: $isUpdateRide, scrennType: .constant("Notes"), myRideForDay: rideDetailDay)){
                                                    Image(ImageConstantsName.UnVectorImg)
                                                }
                                            }

                                            TextView(comesFromDetailAndEditScreen: .constant(false), isEditable: .constant(false), text:  .constant(rideDetailDay.rideEdit.details.notes), textStyle:$textStyle, comesFromEditScreen: .constant(false), didStartEditing: .constant(false),placeHolderText:"")
                                                .cornerRadius(10)
                                                .frame(width: UIScreen.screenWidth-20, height: 400)
                                                .padding(.bottom,50)
                                        }
                                    }
                                }.fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                            }
                        }
                        Spacer()
                    }.padding()
                }
            }
            
            
            if showTimrScreen {
                TimeEditScreen(isUpdateRideTime: $isUpdateRideTime, showTimrScreen: $showTimrScreen, myRideForDay: rideDetailDay)
            }
            
            NavigationLink(destination: UplodImageForRide(myRideForDay: rideDetailDay), isActive: $showUpdateImageScrn){}
            if myRideForDay != nil {
                NavigationLink(destination: InviteMainScreen(day:$day,showDeatilPopUp: $showInvitePopUp,ride:$myRideForDay.toUnwrapped(defaultValue: DtrRide.default), profile: dtrData.profile, opacity: 1.0 , resetLocalValue: true, selectFriends: [""], selectFollowers: [""], selectGroupMembers: [""]), isActive: $showInviteScreen){}
            }
        }
        
        .onAppear{
            DispatchQueue.main.async {
                if UserStore.shared.updateOnlyOneTimeForINviteAndShare {
                    UserStore.shared.saveLocallyRideDetail(myRideForDay: rideDetailDay.rideEdit, isSetData: false) { response in
                        self.rideDetailDay.rideEdit = response
                        self.myRideForDay = response
                    }
                }
            }
        }
        
        .onChange(of: myRideForDay, perform: { (respo) in
            if respo == nil {
                self.presentationMode.wrappedValue.dismiss()
            }
        })
        
        .background(Color.init(ColorConstantsName.MainThemeBgColour))
        .foregroundColor(.white)
        .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.all)
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .leadingNavigationBarItems { leadingButton.padding() }
        .trailingNavigationBarItems { trailingButton.padding() }
        
        .fullScreenCover(item: $activeSheet) { item in
            switch item {
            case .welcome:
                ShareScreen(isPresented: self.$shareRideDetail, ride: self.myRideForDay, showBackBtn: true)
                    .onAppear{
                        Analytics.logEvent(AnalyticsScreen.rideLink.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
                    }
            case .location:
                MapScreen(day: .constant(0),fullLocationName:.constant(""),isOnBoradingLat:.constant(0.0),isOnBoradingLon:.constant(0.0),canEditLocation:true,startCountry: .constant(""), startPlace: .constant(""), isOnBoradingProcess: .constant(true), isUpdateRideLocation: $isUpdateRideLocation, showRideLocationScreen: $showRideLocationScreen, myRideForDay: rideDetailDay)
            }
        }
    }
    
    var leadingButton: some View {
        NavigtionItemBar(isImage: true,isSystemImage: false, imageName: ImageConstantsName.BckWithColorImg, action: {
            callUpadateDetail()
        })
    }
    
    var trailingButton: some View {
        HStack{
            if myRideForDay != nil {
                if showPopCancelLeaveRide != true {
                    Button(action: {
                        showUpdateImageScrn = true
                        UserStore.shared.updateOnlyOneTimeForINviteAndShare = true
                    }) {
                        Image( "DTR-ChangeImage")
                    }.padding(.trailing)
                    
                    if rideDetailDay.rideEdit.fromAction != .none {
                        Button(action: {
                            if rideDetailDay.rideEdit.fromAction == .cancel {
                                showPopContentForRideAction = DayContainerString.NotDtrAnyMoreStr
                            }else if rideDetailDay.rideEdit.fromAction == .leave {
                                showPopContentForRideAction = "Leave Ride"
                            }
                            showPopCancelLeaveRide = true
                        }) {
                            Image(ImageConstantsName.BckWithThreeDotsImg)
                        }
                    }
                }else if  showPopCancelLeaveRide ==  true {
                    ShowFlagInApprView(stringTitle:showPopContentForRideAction,showPopUpFlgInapp:$showPopCancelLeaveRide,action: {
                        showPopCancelLeaveRide = false
                        if rideDetailDay.rideEdit.fromAction == .cancel {
                            if rideDetailDay.rideEdit.riders.count > 1 {
                                alertView()
                            }else{
//                                if UserDefaults.standard.object(forKey: rideDetailDay.rideEdit.dateCode) != nil{
//                                    UserDefaults.standard.removeObject(forKey: rideDetailDay.rideEdit.dateCode)
//                                }
                                self.dtrCommands.rideCancel(rideID: rideDetailDay.rideEdit.id,dateCode:rideDetailDay.rideEdit.dateCode, message: "") { result in
                                    result.debug(methodName:"rideCancel")
                                }
                            }
                        }else if rideDetailDay.rideEdit.fromAction == .leave {
                            self.dtrCommands.rideLeave(rideID: self.rideDetailDay.rideEdit.id, dateCode: self.rideDetailDay.rideEdit.dateCode) { (result) in
                                result.debug(methodName:"rideLeave")
                            }
                        }
                    })
                }
            }
        }
    }
    
    func skillsNotesPresentation(ride: DtrRide?) -> AnyView {
        if let ride = ride {
            if (ride.details.skillA || ride.details.skillB || ride.details.skillC || ride.details.skillD || ride.details.skillS || ride.details.notes != "") {
                return AnyView(
                    HStack {
                        if ride.details.skillA {
                            showSkilsImage(imgName: "DTR-SkiilA")
                        }
                        
                        if ride.details.skillB {
                            showSkilsImage(imgName: "DTR-SkiilB")
                        }
                        
                        if ride.details.skillC {
                            showSkilsImage(imgName: "DTR-SkiilC")
                        }
                        
                        if ride.details.skillD {
                            showSkilsImage(imgName: "DTR-SkiilD")
                        }
                        
                        if ride.details.skillS {
                            showSkilsImage(imgName: "DTR-SkiilS")
                        }
                    }
                )
            }
        }
        return AnyView(EmptyView())
    }
    
    func showSkilsImage(imgName:String) -> AnyView {
        return AnyView(
            Image(imgName)
                .resizable()
                .frame(width: 25, height: 25, alignment: .center)
                .cornerRadius(10)
        )
    }
    
    
    
    func checkIfRideIsBuild() -> Bool {
        if rideDetailDay.rideEdit.details.visibility.rawValue == "public" {
            if rideDetailDay.rideEdit.details.hasStartTime || rideDetailDay.rideEdit.details.hasStartPlace || rideDetailDay.rideEdit.details.notes != "" {
                if  rideDetailDay.rideEdit.details.skillA || rideDetailDay.rideEdit.details.skillB  ||  rideDetailDay.rideEdit.details.skillC   || rideDetailDay.rideEdit.details.skillD || rideDetailDay.rideEdit.details.skillS  {
                    isVisbilityUpdate = false
                    return true
                }
            }
        }
        return false
    }
    
    func callUpadateDetail() {
        if isUpdateRide {
            DispatchQueue.main.async {
                dtrCommands.rideUpdate(rideID:rideDetailDay.rideEdit.id,data:["_geoloc":["lat":rideDetailDay.rideEdit.details.startPlaceLat,"lng":rideDetailDay.rideEdit.details.startPlaceLon],"summary":rideDetailDay.rideEdit.summary,"details":["tags":rideDetailDay.rideEdit.details.tags,"hasStartPlace":rideDetailDay.rideEdit.details.hasStartPlace,"hasStartTime":rideDetailDay.rideEdit.details.hasStartTime,"notes":rideDetailDay.rideEdit.details.notes,"skillA":rideDetailDay.rideEdit.details.skillA,"skillB":rideDetailDay.rideEdit.details.skillB,"skillC":rideDetailDay.rideEdit.details.skillC,"skillD":rideDetailDay.rideEdit.details.skillD,"skillS":rideDetailDay.rideEdit.details.skillS,"startPlace":rideDetailDay.rideEdit.details.startPlace,"startPlaceLat":rideDetailDay.rideEdit.details.startPlaceLat,"startPlaceLon":rideDetailDay.rideEdit.details.startPlaceLon,"startTime":rideDetailDay.rideEdit.details.startTime,"visibility":rideDetailDay.rideEdit.details.visibility.rawValue]]){ result in
                    result.debug(methodName: "rideUpdate")
                }
            }
        }else if isUpdateRideTime {
            dtrCommands.rideUpdateTime(rideID:rideDetailDay.rideEdit.id,data:["details":["tags":rideDetailDay.rideEdit.details.tags,"hasStartPlace":rideDetailDay.rideEdit.details.hasStartPlace,"hasStartTime":rideDetailDay.rideEdit.details.hasStartTime,"notes":rideDetailDay.rideEdit.details.notes,"skillA":rideDetailDay.rideEdit.details.skillA,"skillB":rideDetailDay.rideEdit.details.skillB,"skillC":rideDetailDay.rideEdit.details.skillC,"skillD":rideDetailDay.rideEdit.details.skillD,"skillS":rideDetailDay.rideEdit.details.skillS,"startPlace":rideDetailDay.rideEdit.details.startPlace,"startPlaceLat":rideDetailDay.rideEdit.details.startPlaceLat,"startPlaceLon":rideDetailDay.rideEdit.details.startPlaceLon,"startTime":rideDetailDay.rideEdit.details.startTime,"visibility":rideDetailDay.rideEdit.details.visibility.rawValue]]){ result in
                result.debug(methodName: "rideUpdateTime")
                Analytics.logEvent(AnalyticsEvent.rideUpdateTime.rawValue, parameters: ["rideID": rideDetailDay.rideEdit.id])
            }
        }else if isUpdateRideLocation {
            dtrCommands.rideUpdateLocation(rideID:rideDetailDay.rideEdit.id,data:["details":["tags":rideDetailDay.rideEdit.details.tags,"hasStartPlace":rideDetailDay.rideEdit.details.hasStartPlace,"hasStartTime":rideDetailDay.rideEdit.details.hasStartTime,"notes":rideDetailDay.rideEdit.details.notes,"skillA":rideDetailDay.rideEdit.details.skillA,"skillB":rideDetailDay.rideEdit.details.skillB,"skillC":rideDetailDay.rideEdit.details.skillC,"skillD":rideDetailDay.rideEdit.details.skillD,"skillS":rideDetailDay.rideEdit.details.skillS,"startPlace":rideDetailDay.rideEdit.details.startPlace,"startPlaceLat":rideDetailDay.rideEdit.details.startPlaceLat,"startPlaceLon":rideDetailDay.rideEdit.details.startPlaceLon,"startTime":rideDetailDay.rideEdit.details.startTime,"visibility":rideDetailDay.rideEdit.details.visibility.rawValue]]){ result in
                Analytics.logEvent(AnalyticsEvent.rideUpdateLocation.rawValue, parameters: ["rideID": rideDetailDay.rideEdit.id])
                result.debug(methodName: "rideUpdateLocation")
            }
        }
        self.presentationMode.wrappedValue.dismiss()
    }
    
    func alertView() {
        UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: false, completion: nil)
        let alert = UIAlertController(title: "Are you sure you'd like to cancel the ride?", message: CommonErrorString.ThreeHundCharacError, preferredStyle: .alert)
        alert.addTextField { (testTextField) in
            testTextField.placeholder = "Optional Message"
            testTextField.returnKeyType = .done
        }
        let cancellButton = UIAlertAction(title: "No", style: .destructive) { (_) in}
        let okButton = UIAlertAction(title: "Yes", style: .default) { (_) in
            callRideCanceMethod(result: alert.textFields?[0].text ?? "")
        }
        alert.addAction(okButton)
        alert.addAction(cancellButton)
        UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true, completion: {
        })
    }
    
    
    
    func callRideCanceMethod(result:String?) {
        if myRideForDay != nil {
            var textMesg = ""
            if let text = result {
                if text != "" {
                    textMesg = text
                }
            }
            if textMesg.count > 300 {
                alertView()
                return;
            } else {
                self.dtrCommands.rideCancel(rideID: rideDetailDay.rideEdit.id,dateCode:rideDetailDay.rideEdit.dateCode, message: textMesg) { result in
                    result.debug(methodName:"rideCancel")
                }
            }
        }
    }
}

struct RideEdit_Previews: PreviewProvider {
    static var previews: some View {
        RideEdit(day:.constant(0),myRideForDay: .constant(DtrRide.default), showingAlertForFinalEdit: .constant(false), isVisbilityUpdate: false, rideDetailDay: RideEditViewModel.init(ride: DtrRide.default))
    }
}



func returnRideCoverImage(rideImageUrl:String,className:String) -> AnyView {
    return AnyView(
        Group {
            if className == "rideEdit" {
                WebImage(url:URL(string:rideImageUrl), isAnimating: .constant(true))
                    .purgeable(true)
                    .placeholder(Image(ImageConstantsName.MainRideEditCoverImg))
                    .renderingMode(.original)
                    .resizable()
                    .indicator(.activity)
                    .scaledToFill()
                    .frame(width:UIScreen.screenWidth,height: 180, alignment: .center)
                    .clipped()
            }else if className == "RideCenterView" {
                WebImage(url:URL(string:rideImageUrl), isAnimating: .constant(true))
                    .purgeable(true)
                    .placeholder(Image(ImageConstantsName.MainRideEditCoverImg))
                    .renderingMode(.original)
                    .resizable()
                    .indicator(.activity)
                    .scaledToFill()
                    .frame(width:UIScreen.screenWidth-30,height: 120, alignment: .center)
                    .clipped()
                    .cornerRadius(20, corners: [.topLeft, .topRight])
                    .padding(.bottom)
            }else if className == "RideDetailView" {
                WebImage(url:URL(string:rideImageUrl), isAnimating: .constant(true))
                    .purgeable(true)
                    .placeholder(Image(ImageConstantsName.MainRideEditCoverImg))
                    .renderingMode(.original)
                    .resizable()
                    .indicator(.activity)
                    .scaledToFill()
                    .frame(width:UIScreen.screenWidth,height: 180, alignment: .center)
                    .clipped()
            }
        }
    )
}
