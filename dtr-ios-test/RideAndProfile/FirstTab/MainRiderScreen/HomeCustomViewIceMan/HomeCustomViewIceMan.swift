//
//  HomeCustomViewIceMan.swift
//  dtr-ios-test
//
//  Created by apple on 19/08/21.
//

import SwiftUI
import Firebase

struct HomeCustomViewIceMan: View {
    
    @Binding var day: Int
    @Binding var ridesToDisplay: DtrRide
    @Binding var shareRide:Bool
    @Binding var showPopUp:Bool
    @Binding var widthScreen :CGFloat
    @Binding var showTimrScreen:Bool
    
    @Binding var arrayCustom: [String]
    @Binding var showAlertNotDTR:Bool
    @Binding var presentModelOldHistoryScreen : Bool
    
    @State var isVisbilityUpdate = false
    @State private var showPublicLocationChange = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var UpdatedRideDetailForImages : DtrRide = DtrRide.default
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    @State private var startPlace = CommonAllString.BlankStr
    @State private var startCountry = CommonAllString.BlankStr
    @State private var locationName = CommonAllString.BlankStr
    @State var startPlaceLat = 0.0
    @State var startPlaceLon = 0.0
    @State private var isUpdateRideLocation = false
    @State private var showRideLocationScreen = false
    @State private var tagsCollection = [""]
    @State private var heightText:CGFloat = 0.0
    
    
    var body: some View {
        ZStack {
            VStack{
                Image(ImageConstantsName.IceTagBag2Img)
                    .renderingMode(.original)
                    .resizable()
                    .edgesIgnoringSafeArea(.top)
                    .layoutPriority(1)
                    .aspectRatio(1125/2613, contentMode: .fill)
                    .frame(width:widthScreen)
                if ridesToDisplay.details.notes != CommonAllString.BlankStr {
                    ZStack {
                        VStack{
                            Image(ImageConstantsName.IceTagBagImg)
                                .renderingMode(.original)
                                .resizable()
                                .edgesIgnoringSafeArea(.top)
                                .layoutPriority(1)
                                .frame(width:widthScreen,height: heightText)
                        }
                    }.padding(.top,-20)
                }
            }
            
            VStack(alignment: .center,spacing:10) {
                VStack{
                    EmptyView().background(Color.red)
                }.background(Color.red).frame(height: 240, alignment: .center)
                RideShareAndInviteView(day:$day,ridesToDisplay:$ridesToDisplay,shareRide:$shareRide,showPopUp:$showPopUp,comeFromCutomScreen:true).padding().background(Color.white).cornerRadius(16)
                    .padding(.bottom,-20)
                ZStack{
                    CustomPopUpForRideOptions(ridesToDisplay: $ridesToDisplay, arrayCustom: $arrayCustom, showPopUp: $showPopUp, showAlertNotDTR: $showAlertNotDTR, presentModelOldHistoryScreen: $presentModelOldHistoryScreen,comeFromCustomView:.constant(true)).offset(x: 40, y: -10)
                }.frame(height: 10)
                
                VStack (alignment: .leading , spacing: 20) {
                    
                    RideSummaryView(ridesToDisplay: $ridesToDisplay)
                    
                    RideNameAndProfileImageView(ridesToDisplay: $ridesToDisplay, UpdatedRideDetailForImages: $UpdatedRideDetailForImages)
                    
                    if ridesToDisplay.leader == dtrData.profileID {
                        Group {
                            VStack {
                                if isVisbilityUpdate ||  ridesToDisplay.details.visibility.rawValue == "public" {
                                    ShowPublicTagError(rideDetailDay:$ridesToDisplay)
                                }
                            }
                        }
                    }
                    
                    RideTimeAndLocationView(ridesToDisplay: $ridesToDisplay, showTimrScreen: $showTimrScreen, showPublicLocationChange: $showPublicLocationChange)
                    
                    if (ridesToDisplay.details.skillA || ridesToDisplay.details.skillB || ridesToDisplay.details.skillC || ridesToDisplay.details.skillD || ridesToDisplay.details.skillS) {
                        NavigationLink(destination: SetSkillScrren(isUpdateRide: .constant(false), myRideForDay: RideEditViewModel.init(ride:ridesToDisplay))) {
                            RideSkillIndicators(ride: $ridesToDisplay,comeFromHomeScreen:true)
                        }.disabled(dtrData.profile.id == ridesToDisplay.leader ? false : true)
                    }else{
                        if dtrData.profile.id == ridesToDisplay.leader {
                            NavigationLink(destination: SetSkillScrren(isUpdateRide: .constant(false), myRideForDay: RideEditViewModel.init(ride:ridesToDisplay))) {
                                Text("Set skill level")
                                    .foregroundColor(returnColur())
                            }.opacity(dtrData.profile.id == ridesToDisplay.leader ? 1 : 0).disabled(dtrData.profile.id == ridesToDisplay.leader ? false : true)
                        }
                    }
                    
                    if ridesToDisplay.riders.count > 1 {
                        if ridesToDisplay.details.visibility.rawValue != "public" || ridesToDisplay.leader == dtrData.profile.id {
                            RideSetVisiblityView(ridesToDisplay: $ridesToDisplay, isVisbilityUpdate: $isVisbilityUpdate).padding(.vertical,5)
                        }
                        
                        if ridesToDisplay.details.tags.contains("KfqH2PdeUY4nyp9V2nPQ") {
                            RideIceManTagView(ridesToDisplay: $ridesToDisplay).padding(.top,5)
                        }
                        
                        VStack {
                            NavigationLink(destination:RideTagScreen.init(selections: ridesToDisplay.details.tags, rideDetailDay: RideEditViewModel(ride: ridesToDisplay)),label: {
                                VStack {
                                    ShowTagUIView(tags: $tagsCollection, ridesToDisplay: $ridesToDisplay)
                                }
                            }).disabled(dtrData.profile.id == ridesToDisplay.leader ? false : true)
                        }
                    }else{
                        RideSetVisiblityView(ridesToDisplay: $ridesToDisplay, isVisbilityUpdate: $isVisbilityUpdate).padding(.vertical,5)
                        
                        RideSetTagView(day: $day, ridesToDisplay: $ridesToDisplay)
                    }
                    RideDescriptionView(ridesToDisplay: $ridesToDisplay,screenWidth:$widthScreen, heightText: $heightText)
                }.padding(.all)
                .background(Color.init(ColorConstantsName.HeaderBgColour))
                .cornerRadius(20)
                .clipped()
                .padding(.all)
            }
            
            .onAppear{
                updateRideTagsWithUpdation()
            }
            
            
            .onReceive(self.dtrData.$myRidesByDate) { rideDict in
                let dict = rideDict[dateCode(offset: day)]
                UpdatedRideDetailForImages = DtrRide.default
                if dict != nil {
                    self.ridesToDisplay = dict!
                    UpdatedRideDetailForImages = self.ridesToDisplay
                    updateRideTagsWithUpdation()
                }
            }
            
            .fullScreenCover(isPresented: $showPublicLocationChange) {
                MapScreen(day: $day,fullLocationName:$locationName,isOnBoradingLat:$startPlaceLat,isOnBoradingLon:$startPlaceLon,canEditLocation:true,startCountry: $startCountry, startPlace: $startPlace, isOnBoradingProcess: .constant(true), isUpdateRideLocation: $isUpdateRideLocation, showRideLocationScreen: $showRideLocationScreen, myRideForDay: RideEditViewModel.init(ride: ridesToDisplay)) {
                    if startPlaceLat != 0.0 && startPlaceLon != 0.0 {
                        updateRideLocationAndReset(hasStartPlace: true, startPlace: locationName, startPlaceLat: startPlaceLat, startPlaceLon: startPlaceLon)
                    }else{
                        updateRideLocationAndReset(hasStartPlace: false, startPlace: locationName, startPlaceLat: startPlaceLat, startPlaceLon: startPlaceLon)
                    }
                }
            }
        }
    }
    
    func returnColur () -> Color {
        if ridesToDisplay.details.visibility == .public {
            if dtrData.profile.id == ridesToDisplay.leader {
                if (!ridesToDisplay.details.skillA || !ridesToDisplay.details.skillB || !ridesToDisplay.details.skillC || !ridesToDisplay.details.skillD || !ridesToDisplay.details.skillS) {
                    return .red
                }
            }
        }
        return Color.init(ColorConstantsName.AccentTextColour)
    }    
    
    func updateRideTagsWithUpdation() {
        tagsCollection.removeAll()
        if ridesToDisplay.details.tags.count > 0 {
            tagsCollection = ridesToDisplay.details.tags
            if ridesToDisplay.details.visibility.rawValue == "public" {
                if tagsCollection.count > 0  {
                    if ridesToDisplay.leader != dtrData.profile.id {
                        if !self.tagsCollection.contains("Public_Ride") {
                            self.tagsCollection.insert("Public_Ride", at: 0)
                            if let indesx = tagsCollection.firstIndex(where: { $0 == "KfqH2PdeUY4nyp9V2nPQ"}) {
                                tagsCollection.remove(at: indesx)
                            }
                        }
                    }
                }
            }
            if let indesx = tagsCollection.firstIndex(where: { $0 == "KfqH2PdeUY4nyp9V2nPQ"}) {
                tagsCollection.remove(at: indesx)
            }
        }
    }
    
    func updateRideLocationAndReset(hasStartPlace:Bool,startPlace:String,startPlaceLat:Double,startPlaceLon:Double){
        dtrCommands.rideUpdateLocation(rideID:ridesToDisplay.id,data:["details":["tags":ridesToDisplay.details.tags,"hasStartPlace":hasStartPlace,"hasStartTime":ridesToDisplay.details.hasStartTime,"notes":ridesToDisplay.details.notes,"skillA":ridesToDisplay.details.skillA,"skillB":ridesToDisplay.details.skillB,"skillC":ridesToDisplay.details.skillC,"skillD":ridesToDisplay.details.skillD,"skillS":ridesToDisplay.details.skillS,"startPlace":startPlace,"startPlaceLat":startPlaceLat,"startPlaceLon":startPlaceLon,"startTime":ridesToDisplay.details.startTime,"visibility":ridesToDisplay.details.visibility.rawValue]]){ result in
            Analytics.logEvent(AnalyticsEvent.rideUpdateLocation.rawValue, parameters: ["rideID": ridesToDisplay.id])
            result.debug(methodName: "rideUpdateLocation")
        }
    }
}
