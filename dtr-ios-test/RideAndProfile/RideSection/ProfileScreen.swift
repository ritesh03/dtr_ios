//
//  ProfileScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 21/01/21.
//

import SwiftUI
import LinkPresentation
import Firebase
import FirebaseDynamicLinks
import MobileCoreServices

struct ProfileScreen: View {
    
    @State var creatingLinks = false
    @State private var links = [Any]()
    @State private var copyURlLink = ""
    @State private var isShareLinkOn = false
    @Binding var userProfile : DtrProfile
    @State  var profileImage =  Image("")
    @State var showBackButtonOrNot = false
    @State private var isAlreadyFriend = false
    @State private var upcomingRides = [DtrRide]()
    @State private var showProfileImageViwer = false
    @State private var showdividerPhoneEmail = false
    @State private var showPopUpAfterFollowBtn = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var activeSheet: ActiveSheetForProfileScreen?
    @Environment(\.presentationMode) var presentationMode
    @State private var copyLinkCodeBtnText = "COPY MY FRIEND LINK"
    
    
    var body: some View {
        ZStack {
            Color.init("DTR-MainThemeBackground")
                .ignoresSafeArea()
            ScrollView {
                VStack(alignment: .leading) {
                    VStack(alignment: .leading, spacing: 20) {
                        HStack(alignment: .top) {
                            ProfileHead(profile: userProfile, size: .large)
                                .onTapGesture {
                                    activeSheet = .pfoileImgVwScreen
                                }
                            VStack(alignment: .leading,spacing: 10) {
                                Text(userProfile.name)
                                Text(userProfile.defaultStatus)
                            }
                        }
                        
                        HStack(alignment:.center) {
                            Text(userProfile.cityState)
                                .foregroundColor(.white)
                            Circle()
                                .frame(width: 5, height: 5)
                                .foregroundColor(Color.gray)
                            Text("DTR since \(memberSince(self.userProfile.activated))")
                                .foregroundColor(.white)
                        }
                        
                        if dtrData.profileID == userProfile.id {
                            VStack{
                                CustomButtonCircleRS(btnTitle: "EDIT PROFILE", btnWidth: UIScreen.screenWidth*0.8, btnHeight: 10, backgroundColord: false, txtColorAccent:true,strokeColor: true) {
                                    activeSheet = .editprofileScreen
                                }
                                CustomButtonCircleRS(btnTitle: copyLinkCodeBtnText, btnWidth: UIScreen.screenWidth*0.8, btnHeight: 10, backgroundColord: false, txtColorAccent:true,strokeColor: true) {
                                    if self.dtrData.profile.linkCode != "" {
                                        self.dtrData.createFriendLink() { link in
                                            self.constructLinks(isCodeCopyForNote: true)
                                        }
                                    }else{
                                        self.constructLinks(isCodeCopyForNote: true)
                                    }
                                }
                            }
                        }
                    }.padding()
                    
                    FollowerUnFollowCount(profile: $userProfile, showPopUpSendReq: $showPopUpAfterFollowBtn, isAlreadyFriend: $isAlreadyFriend)
                    
                    if userProfile.phone != "" ||  userProfile.email != "" {
                        Divider()
                            .background(Color.white)
                    }
                    ZStack{
                        HStack {
                            ShowPopUpMesg(titleMesg: "", customMesg: "You can send a friend request to see friends-only rides and information.", show: $showPopUpAfterFollowBtn, opticity: .constant(true))
                                .padding()
                        }
                        
                        if isAlreadyFriend {
                            HStack {
                                if userProfile.phone != "" {
                                    VStack(alignment: .leading, spacing: 15) {
                                        Image("DTR-Phone")
                                        Text(userProfile.phone ?? "No Number")
                                    }
//                                    .onAppear{
//                                        if userProfile.phone != "" {
//                                            showdividerPhoneEmail = true
//                                        }else{
//                                            showdividerPhoneEmail = false
//                                        }
//                                    }
                                    Spacer()
                                }
                                
                                if userProfile.email != ""  {
                                    VStack(alignment: .leading, spacing: 15) {
                                        Image("DTR-Email")
                                        Text(userProfile.email ?? "No Email")
                                    }
//                                    .onAppear{
//                                        if userProfile.email != "" {
//                                            showdividerPhoneEmail = true
//                                        }else{
//                                            showdividerPhoneEmail = false
//                                        }
//                                    }
                                }
                                
                                if userProfile.phone == "" {
                                    Spacer()
                                }
                            }
                        }
                    }
                    .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
                    .padding(.horizontal)
                    if userProfile.description != "" {
                        Divider()
                            .background(Color.white)
                        HStack {
                            VStack(alignment: .leading, spacing: 15) {
                                Text("BIO")
                                Text(userProfile.description)
                                    .fixedSize(horizontal: false, vertical:true )
                            }.padding(.horizontal)
                        }
                    }
                    
                    VStack(alignment: .leading, spacing: 20) {
                        Divider()
                            .background(Color.white)
                        if self.upcomingRides.count > 0 {
                            Text("UPCOMING RIDES")
                                .padding(.leading)
                            Divider()
                                .background(Color.white)
                        }
                    }
                    if self.upcomingRides.count > 0 {
                        ForEach(self.upcomingRides, id: \.self) { ride in
                            NavigationLink(destination: RideDetail(presentModelOldHistoryScreen: .constant(false),myRideForDay1: RideEditViewModel.init(ride: ride))) {
                                FriendRideView(myRideForDay: ride)
                            }
                        }
                    }
                }
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarItems(leading:AnyView(leadingButton),trailing:AnyView(self.trailingButton))
        .foregroundColor(Color.white)
        
        .onAppear {
            self.dtrData.upcomingRides(for: userProfile) { profileRides in
                self.upcomingRides = profileRides
            }
            if dtrData.profileID == userProfile.id {
                isAlreadyFriend = true
            }
        }
        
        .fullScreenCover(item: $activeSheet) { item in
            switch item {
            case .editprofileScreen:
                ProfileEditScrn(message: .constant(""), rideDetailDay: RideEditViewModel.init(ride: DtrRide.default), displayName: dtrData.profile.name, displayLocation: dtrData.profile.cityState, displayCountry: dtrData.profile.country, cyclicMantra: "", bio: dtrData.profile.description, email: dtrData.profile.email ?? "", phone: dtrData.profile.phone ?? "")
            case .settingScreen:
                SettingScreen()
            case .pfoileImgVwScreen:
                ProfileImageScreen(imagaName: Image(uiImage: self.dtrData.profilePicture(profileID: self.userProfile.id)))
            }
        }
        
        .background(EmptyView().sheet(isPresented: self.$isShareLinkOn) {
            Modal(isPresented: self.$isShareLinkOn, title: "Share Profile") {
                ShareProfile(isPresented: self.$isShareLinkOn, links: links)
            }
        })
    }
    
    var leadingButton: some View {
        HStack{
            if showBackButtonOrNot {
                NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,txtTitle: "", systemImgName: "arrow.left", action: {
                    presentationMode.wrappedValue.dismiss()
                })
            }else if dtrData.profileID == userProfile.id {
                Text("Profile")
            }
        }
    }
    
    var trailingButton: some View {
        HStack{
            if dtrData.profileID == userProfile.id {
                Button(action: {
                    if self.dtrData.profile.linkCode != "" {
                        self.dtrData.createFriendLink() { link in
                            self.constructLinks(isCodeCopyForNote: false)
                        }
                    }else{
                        self.constructLinks(isCodeCopyForNote: false)
                    }
                }) {
                    Image("DTR-ShareIcon-Three")
                        .resizable().frame(width: 20, height: 20)
                }.padding(.trailing)
                Button(action: {
                    activeSheet = .settingScreen
                }) {
                    Image("DTR-SettingIcon")
                        .resizable().frame(width: 20, height: 20)
                }
            }else{
                EmptyView()
            }
        }
    }
    
    
    func constructLinks(isCodeCopyForNote:Bool) {
        self.creatingLinks = true
        
        guard let link = URL(string: "https://socialactive.app/friend/\(self.dtrData.profile.linkCode)") else {
            print("ERROR constructing initial URL")
            return
        }
        
        let dynamicLinksDomainURIPrefix = "https://dtr.lol"
        let appBundleID = "app.socialactive.dtr-ios-test"
        
        if let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix) {
            linkBuilder.navigationInfoParameters = DynamicLinkNavigationInfoParameters()
            linkBuilder.navigationInfoParameters?.isForcedRedirectEnabled = true
            linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: appBundleID)
            //linkBuilder.iOSParameters?.appStoreID = "1477781219"
            linkBuilder.iOSParameters?.minimumAppVersion = "1.1.0"
            linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: appBundleID)
            linkBuilder.androidParameters?.minimumVersion = 0
            
            guard let longDynamicLink = linkBuilder.url else {
                print("ERROR in linkBuilder")
                return
            }
            DynamicLinkComponents.shortenURL(longDynamicLink, options: nil) { url, warnings, error in
                if let error = error {
                    print("error shortening link: \(error.localizedDescription)")
                    return
                }
                if let url = url {
                    let message = "\(self.dtrData.profile.name) would like to be friends within the Down to Ride (DTR) app"
                    let itemSource = ShareActivityItemSource(type: .friend, message: message, url: url)
                    self.links = [url, itemSource]
                    self.creatingLinks = false
                    copyURlLink = url.absoluteString
                    if isCodeCopyForNote {
                        UIPasteboard.general.setValue(copyURlLink,forPasteboardType: kUTTypePlainText as String)
                        copyLinkCodeBtnText = "COPIED"
                        DispatchQueue.main.asyncAfter(deadline: .now()+3.0) {
                            copyLinkCodeBtnText = "COPY MY FRIEND LINK"
                        }
                    }else{
                        isShareLinkOn = true
                    }
                    return
                }
            }
        }
    }
}

struct ProfileScreen_Previews: PreviewProvider {
    static var previews: some View {
        ProfileScreen(userProfile: .constant(DtrProfile.default))
    }
}

enum ActiveSheetForProfileScreen: Identifiable {
    case settingScreen, editprofileScreen,pfoileImgVwScreen
    var id: Int {
        hashValue
    }
}
