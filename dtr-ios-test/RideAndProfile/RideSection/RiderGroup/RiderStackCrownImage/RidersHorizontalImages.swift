//
//  RidersHorizontalImages.swift
//  dtr-ios-test
//
//  Created by Mobile on 07/04/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct RidersHorizontalImages: View {
    @Binding var ride: DtrRide
    var showLeader: Bool = true
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var showRiderListScreen = false
    @State var showRiderProfileScreen = false
    @State var showRiderNameFriendView = false
    @State private var image = Image(ImageConstantsName.PersonPlaceholderUserImg)
    @State var badgeDetails = DtrBadge.default
    
    var friendRiders: [String] {
        var result = [String]()
        for rider in self.ride.riders {
            if rider == dtrData.profileID || isFriend(id: rider) {
                if rider != CommonAllString.BlankStr {
                    if rider != self.ride.leader {
                        result.append(rider)
                    }
                }
            }
        }
        return result
    }
    
    var nonFriendRiders: [String] {
        var result = [String]()
        for rider in self.ride.riders {
            if rider != CommonAllString.BlankStr {
                if rider != dtrData.profileID && !isFriend(id: rider) && rider != ride.leader {
                    result.append(rider)
                }
            }
        }
        return result
    }
    
    func isFriend(id: String) -> Bool {
        return dtrData.profile.friends.contains(id)
    }
    
    var body: some View {
        HStack(spacing:-5) {
            Group {
                RidersHorizontalImagesForLeader(ride: $ride, friendRidersTotalCount: .constant(friendRiders.count), nonFriendRidersTotalCount: .constant(nonFriendRiders.count))
                
                showFriendsList()
                
                RidersHorizontalImagesForNonFriends(ride: $ride)
                
                if ride.riders.count > 1 {
                    if showRiderNameFriendView {
                        Circle()
                            .frame(width: 5, height: 5)
                            .foregroundColor(Color.gray)
                            .padding(.leading)
                    }
                    Text("\(ride.riders.count) riders").font(.system(size: 13))
                        .foregroundColor(Color.gray)
                        .padding(.leading)
                }
            }
                
            .onTapGesture {
                if ride.riders.count > 1 {
                    showRiderListScreen = true
                }else{
                    showRiderProfileScreen = true
                }
            }
            
            NavigationLink(destination: RiderList(ride: ride), isActive: $showRiderListScreen){
            }.accentColor(.white)
            
            NavigationLink(destination: ProfileScreen(updatedDismissView:.constant(false),day: .constant(0),userProfile: .constant(self.dtrData.profileInfo(profileID: ride.leader)), showBackButtonOrNot:true), isActive: $showRiderProfileScreen){
                EmptyView()
            }
            Spacer()
        }.padding(.vertical)
        
    }
     
    func showFriendsList() -> AnyView {
        return AnyView(
            ForEach(self.friendRiders.indices, id: \.self) { rider in
                if rider < 5 {
                    Group {
                        if dtrData.profileDirectory[dynamicMember: friendRiders[rider]].profileImage != CommonAllString.BlankStr {
                            Group {
                                showFriendImageWithImage(friendCount: friendRiders.count, NonFriendCount: nonFriendRiders.count, indexRider: rider)
                            }
                        } else {
                            Group {
                                showFriendImageWithoutImage(friendCount: friendRiders.count, NonFriendCount: nonFriendRiders.count, indexRider: rider)
                            }
                        }
                    }
                }
            }
        )
    }
    
    func showFriendImageWithImage(friendCount:Int,NonFriendCount:Int,indexRider:Int) -> AnyView {
        return AnyView(
            Group {
                WebImage(url:URL(string: dtrData.profileDirectory[dynamicMember: friendRiders[indexRider]].profileImage), isAnimating: .constant(true))
                    .purgeable(true)
                    .placeholder(image)
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: 34, height: 34)
                    .clipShape(Circle())
                    .aspectRatio(contentMode: .fill)
                    .zIndex(Double(friendCount + NonFriendCount - indexRider))
                    .overlay(showBadegImage(rideId: friendRiders[indexRider])
                    .frame(width: 15, height:15).offset(x: 3, y: 3),alignment: .bottomTrailing)
            }
        )
    }
    
    func showFriendImageWithoutImage(friendCount:Int,NonFriendCount:Int,indexRider:Int) -> AnyView {
        return AnyView(
            Group {
                Image(uiImage: dtrData.profilePictureWithName(profileID: friendRiders[indexRider], inLarge: false))
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: 34, height: 34)
                    .clipShape(Circle())
                    .aspectRatio(contentMode: .fill)
                    .zIndex(Double(friendCount + NonFriendCount - indexRider))
                    .overlay(showBadegImage(rideId: friendRiders[indexRider])
                    .frame(width: 15, height:15).offset(x: 3, y: 3),alignment: .bottomTrailing)
            }
        )
    }
    
    func showBadegImage(rideId: String) -> AnyView {
        if dtrData.profileDirectory[dynamicMember: rideId].badge != "" {
            if dtrData.badgesDirectory[dynamicMember: dtrData.profileDirectory[dynamicMember: rideId].badge].iconInput != "" {
                return AnyView(
                    WebImage(url:URL(string: dtrData.badgesDirectory[dynamicMember: dtrData.profileDirectory[dynamicMember: rideId].badge].iconInput), options: [.progressiveLoad], isAnimating: .constant(true))
                        .purgeable(true)
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 20, height: 20)
                        .clipShape(Circle())
                )
            }
        }
        return AnyView(EmptyView())
    }
}

struct RidersHorizontalImagesForLeader: View {
    @Binding var ride: DtrRide
    @Binding var friendRidersTotalCount: Int
    @Binding var nonFriendRidersTotalCount: Int
    var showLeader: Bool = true
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var overlay: Bool = false
    @State private var image = Image(ImageConstantsName.PersonPlaceholderUserImg)
    
    var body: some View {
        Group {
            Group {
                if showLeader {
                    if dtrData.profileDirectory[dynamicMember: self.ride.leader].profileImage != CommonAllString.BlankStr {
                        Group {
                            WebImage(url:URL(string: dtrData.profileDirectory[dynamicMember: self.ride.leader].profileImage), options: [.progressiveLoad], isAnimating: .constant(true))
                                .purgeable(true)
                                .placeholder(image)
                                .renderingMode(.original)
                                .resizable()
                                .frame(width: 50, height: 50)
                                .background(Color.gray)
                                .clipShape(Circle())
                                .aspectRatio(contentMode: .fill)
                                .zIndex(Double(friendRidersTotalCount + nonFriendRidersTotalCount + 1))
                                .overlay(showCrownImage(ride: ride))
                                .overlay(showBadegImage(ride: ride)
                                .frame(width: 20, height:20).offset(x: 3, y: 3),alignment: .bottomTrailing)
                        }
                    } else {
                        Group {
                            Image(uiImage:  dtrData.profilePictureWithName(profileID: self.ride.leader, inLarge: false))
                                .renderingMode(.original)
                                .resizable()
                                .frame(width: 50, height: 50)
                                .background(Color.gray)
                                .clipShape(Circle())
                                .aspectRatio(contentMode: .fill)
                                .zIndex(Double(friendRidersTotalCount + nonFriendRidersTotalCount + 1))
                                .overlay(showCrownImage(ride: ride))
                                .overlay(showBadegImage(ride: ride)
                                .frame(width: 20, height:20).offset(x: 3, y: 3),alignment: .bottomTrailing)
                        }
                    }
                }
            }
        }
    }
    
    func showCrownImage(ride: DtrRide) -> AnyView {
        if ride.riders.count > 1 {
            return AnyView(
                Image(ImageConstantsName.CrownImg)
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 23, height: 23)
                    .shadow(radius: 1)
                    .offset(x: 0, y: -25)
                    .rotationEffect(.degrees(12))
            )
        }
        return AnyView(EmptyView())
    }
    
    func showBadegImage(ride: DtrRide) -> AnyView {
        if dtrData.profileDirectory[dynamicMember: ride.leader].badge != "" {
            if dtrData.badgesDirectory[dynamicMember: dtrData.profileDirectory[dynamicMember: ride.leader].badge].iconInput != "" {
                return AnyView(
                    WebImage(url:URL(string: dtrData.badgesDirectory[dynamicMember: dtrData.profileDirectory[dynamicMember: ride.leader].badge].iconInput), options: [.progressiveLoad], isAnimating: .constant(true))
                        .purgeable(true)
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 20, height: 20)
                        .clipShape(Circle())
                )
            }
        }
        return AnyView(EmptyView())
    }
}


struct RidersHorizontalImagesForNonFriends: View {
    @Binding var ride: DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var image = Image(ImageConstantsName.PersonPlaceholderUserImg)
    var nonFriendRiders: [String] {
        var result = [String]()
        for rider in self.ride.riders {
            if rider != CommonAllString.BlankStr {
                if rider != dtrData.profileID && !isFriend(id: rider) && rider != ride.leader {
                    result.append(rider)
                }
            }
        }
        return result
    }
    
    func isFriend(id: String) -> Bool {
        return dtrData.profile.friends.contains(id)
    }
    
    var body: some View {
        Group {
            Group {
                ForEach(self.nonFriendRiders.indices, id: \.self) { rider in
                    if rider < 5 {
                        if dtrData.profileDirectory[dynamicMember: nonFriendRiders[rider]].profileImage != CommonAllString.BlankStr {
                            Group {
                                WebImage(url:URL(string: dtrData.profileDirectory[dynamicMember: nonFriendRiders[rider]].profileImage), isAnimating: .constant(true))
                                    .purgeable(true)
                                    .placeholder(image)
                                    .renderingMode(.original)
                                    .resizable()
                                    .frame(width: 34, height: 34)
                                    .clipShape(Circle())
                                    .aspectRatio(contentMode: .fill)
                                    .zIndex(Double(nonFriendRiders.count - rider))
                                    .overlay(showBadegImage(rideId: nonFriendRiders[rider])
                                    .frame(width: 15, height:15).offset(x: 3, y: 3),alignment: .bottomTrailing)
                            }
                        } else {
                            Group {
                                Image(uiImage: dtrData.profilePictureWithName(profileID: nonFriendRiders[rider], inLarge: false ))
                                    .renderingMode(.original)
                                    .resizable()
                                    .frame(width: 34, height: 34)
                                    .clipShape(Circle())
                                    .aspectRatio(contentMode: .fill)
                                    .zIndex(Double(nonFriendRiders.count - rider))
                                    .overlay(showBadegImage(rideId: nonFriendRiders[rider])
                                    .frame(width: 15, height:15).offset(x: 3, y: 3),alignment: .bottomTrailing)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func showBadegImage(rideId: String) -> AnyView {
        if dtrData.profileDirectory[dynamicMember: rideId].badge != "" {
            if dtrData.badgesDirectory[dynamicMember: dtrData.profileDirectory[dynamicMember: rideId].badge].iconInput != "" {
                return AnyView(
                    WebImage(url:URL(string: dtrData.badgesDirectory[dynamicMember: dtrData.profileDirectory[dynamicMember: rideId].badge].iconInput), options: [.progressiveLoad], isAnimating: .constant(true))
                        .purgeable(true)
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 20, height: 20)
                        .clipShape(Circle())
                )
            }
        }
        return AnyView(EmptyView())
    }
}

