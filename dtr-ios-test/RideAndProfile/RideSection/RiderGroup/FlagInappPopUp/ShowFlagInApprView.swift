//
//  ShowFlagInApprView.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/03/21.
//

import SwiftUI


struct ShowFlagInApprView: View {
    @State var stringTitle: String
    @Binding var showPopUpFlgInapp: Bool
    var action: () -> Void
    var body: some View {
        if showPopUpFlgInapp {
            ZStack {
                VStack(alignment:.leading,spacing:30) {
                    Button(action: action, label: {
                        Text(stringTitle)
                    })
                }
            }
            .padding(20)
            .background(Color.init(UIColor.init(displayP3Red: 29/255, green: 33/255, blue: 33/255, alpha: 1.0)))
            .foregroundColor(.white)
            .cornerRadius(10)
        }
    }
}
