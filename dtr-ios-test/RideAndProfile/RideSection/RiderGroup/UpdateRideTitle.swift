//
//  UpdateTitleRide.swift
//  dtr-ios-test
//
//  Created by Mobile on 20/01/21.
//

import SwiftUI

struct UpdateRideTitle: View {
    
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    @State private var myRideForDay: DtrRide = DtrRide.default
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    @State private var oldRidesToDisplay = [DtrRide]()
    
    @Binding var presentedAsModal: Bool
    @Binding var day : Int
    @State private var titleDtr: String = "Down to Ride"
    
    @State private var selectedIndex : Int = -1
    
    var body: some View {
        ZStack {
            VStack(alignment: .center, spacing:15){
                HStack() {
                    Button(action: {
                        print("dismiss view")
                        self.presentedAsModal = false
                    }) {
                        Image(systemName:"xmark")
                            .resizable()
                            .frame(width: 10, height: 10)
                            .foregroundColor(.white)
                    }
                    Spacer()
                    Button(action: {
                        callEditAndCreateRide()
                    }) {
                        Text("Update")
                            .font(.system(size: 20, weight: .bold, design: .default))
                            .foregroundColor(Color.init("DTR-Accent"))
                    }
                }
                Text("Sweet, you’re DTR now!").font(.system(size: 24, weight: .heavy, design: .default))
                    .foregroundColor(.white)
                    .padding(.bottom)
                Text("Select a custom status or ride title to let \n your friends know what you're interested \n in.").font(.system(size: 17, weight: .medium, design: .default))
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                Text("For example,looking to ride after work").font(.system(size: 17, weight: .medium, design: .default))
                    .foregroundColor(.white)
                //ResponderTextField("Down to Ride", text: self.$titleDtr, isFirstResponder: .constant(true), keyboardType: .default, showDone: true)
                TextField(" Down to Ride", text: self.$titleDtr)
                    .frame(height: 40)
                    .padding(.horizontal, 6)
                    .background(Color.init("DTR-HeaderBackground1"))
                    .foregroundColor(.white)
                    .cornerRadius(10)
                if oldRidesToDisplay.count > 0 {
                    OldRideScreen(oldRidesToDisplay: $oldRidesToDisplay, selectedIndex: $selectedIndex, myRideForDay:$myRideForDay)
                } else {
                    VStack{
                        Spacer()
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                        Spacer()
                    }
                }
            }
            .onAppear{
                dtrData.oldHistoryRides { (rides) in
                   oldRidesToDisplay = rides
                }
            }
            .padding()
        }.background(Color.init("DTR-MainThemeBackground"))
    }
    
    func callEditAndCreateRide() {
        if selectedIndex == -1  {
            self.dtrCommand.rideCreate(dateCode: dateCode(offset: day), data: ["summary": titleDtr]){ result in
                result.debug()
                UIApplication.shared.endEditing()
            }
        }else{
            self.dtrCommand.rideCreate(dateCode: dateCode(offset: day), data: ["summary": titleDtr,"details":["hasStartPlace":myRideForDay.details.hasStartPlace,"hasStartTime":myRideForDay.details.hasStartTime,"notes":myRideForDay.details.notes,"skillA":myRideForDay.details.skillA,"skillB":myRideForDay.details.skillB,"skillC":myRideForDay.details.skillC,"skillD":myRideForDay.details.skillD,"skillS":myRideForDay.details.skillS,"startPlace":myRideForDay.details.startPlace,"startPlaceLat":myRideForDay.details.startPlaceLat,"startPlacelon":myRideForDay.details.startPlaceLon,"startTime":myRideForDay.details.startTime]]) { result in
                result.debug()
                UIApplication.shared.endEditing()
            }
        }
    }
}

//struct UpdateRideTitle_Previews: PreviewProvider {
//    static var previews: some View {
//        UpdateRideTitle(presentedAsModal: .constant(false), day: .constant(0), profile: DtrProfile.default)
//    }
//}

