//
//  RiderList.swift
//  dtr-ios-test
//
//  Created by Mobile on 16/03/21.
//

import SwiftUI
import Firebase

struct RiderList: View {
    
    var ride: DtrRide
    @State var getAllDoneDone: Bool = false
    @State var getOneTimeApiDone: Bool = false
    @State var riderList: [String] = []
    @State var usersFriends: [String] = []
    @State var usersFollowers: [String] = []
    @ObservedObject var dtrData = DtrData.sharedInstance
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State var listOfGrouoc: [UserMutualStatusModel] = []
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack{
                ScrollView {
                    VStack(spacing:0){
                        Group {
                            if getAllDoneDone {
                                ShowLeaderDetail(ride: ride, mutualConnectionAllRiderStatus: $listOfGrouoc)
                                ShowCommonFriends(ride: ride, commanFriends: usersFriends, mutualConnectionAllRiderStatus:$listOfGrouoc)
                                ShowCommonFollowers(ride: ride,commanFollowers: usersFollowers, mutualConnectionAllRiderStatus: $listOfGrouoc)
                                ShowCommonAllRiders(ride: ride, riderList: riderList,mutualConnectionAllRiderStatus: $listOfGrouoc)
                            }
                        }
                    }
                }
            }.onAppear {
                Analytics.logEvent(AnalyticsScreen.rideRiders.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modalNav.rawValue])
                usersFriends.removeAll()
                usersFollowers.removeAll()
                riderList.removeAll()
                for riderIds in ride.riders {
                    if dtrData.profile.followed.contains(riderIds) {
                        if ride.leader != riderIds {
                            usersFollowers.append(riderIds)
                        }
                    }else if dtrData.profile.friends.contains(riderIds){
                        if ride.leader != riderIds {
                            usersFriends.append(riderIds)
                        }
                    }else{
                        if ride.leader != riderIds {
                            riderList.append(riderIds)
                        }
                    }
                }
                getMutualConnectionStatus(profileIds: ride.riders)
                getAllDoneDone = true
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }) {
                                        HStack {
                                            Image(systemName: ImageConstantsName.ArrowLeftImg)
                                            Text("Riders (\(self.ride.riders.count)) ")
                                        }})
        }
    }
    
    func getMutualConnectionStatus(profileIds:[String]) {
        dtrCommand.getMutualConnectionListOfUsers(userProfilesIds: profileIds) { response in
            if response.result == .ok {
                let dictUsr = [asDataDict(response.output)]
                for i in 0..<dictUsr.count {
                    if let val = dictUsr[i] as? [String:String] {
                        for objectID in val.keys {
                            if let name = val[objectID] {
                                self.listOfGrouoc.append(UserMutualStatusModel(userId: objectID, staus: name))
                            }
                        }
                    }
                }
            }
        }
    }
}

struct ShowLeaderDetail: View {
    var ride: DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    @Binding var mutualConnectionAllRiderStatus :  [UserMutualStatusModel]
    
    
    var body: some View {
        ForEach(self.ride.riders, id: \.self) { riderId in
            Group {
                if riderId == ride.leader  {
                    if riderId == dtrData.profile.id {
                        RiderListUI(UserMutualStatusModel:$mutualConnectionAllRiderStatus,myRideForDay: ride, userProfiles: dtrData.profile, showRideLeader: .constant(true))
                    }else{
                        RiderListUI(UserMutualStatusModel:$mutualConnectionAllRiderStatus,myRideForDay: ride, userProfiles: dtrData.profileDirectory[dynamicMember: riderId], showRideLeader: .constant(true))
                    }
                }
            }
        }
    }
}

struct ShowCommonFriends: View {
    var ride: DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var commanFriends: [String]
    @Binding var mutualConnectionAllRiderStatus :  [UserMutualStatusModel]
    
    var body: some View {
        if commanFriends.count > 0 {
            SectionTitleViewUI(titleString: "Friends", rightTitleString: "", countShow:  commanFriends.count) {}
            ForEach(commanFriends, id: \.self) { friendsId in
                RiderListUI(UserMutualStatusModel: $mutualConnectionAllRiderStatus, myRideForDay: ride, userProfiles: dtrData.profileDirectory[dynamicMember: friendsId], showRideLeader: .constant(false))
            }
        }
    }
}


struct ShowCommonFollowers: View {
    var ride: DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var commanFollowers: [String] = []
    @Binding var mutualConnectionAllRiderStatus :  [UserMutualStatusModel]
    var body: some View {
        if commanFollowers.count > 0 {
            SectionTitleViewUI(titleString: "Followers", rightTitleString: "", countShow:  commanFollowers.count) {}
            ForEach(commanFollowers, id: \.self) { followersId in
                RiderListUI(UserMutualStatusModel: $mutualConnectionAllRiderStatus, myRideForDay: ride, userProfiles: dtrData.profileDirectory[dynamicMember: followersId], showRideLeader: .constant(false))
            }
        }
    }
}

struct ShowCommonAllRiders: View {
    var ride: DtrRide
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var riderList: [String] = []
    @Binding var mutualConnectionAllRiderStatus :  [UserMutualStatusModel]
    var body: some View {
        if riderList.count > 0 {
            SectionTitleViewUI(titleString: "Riders", rightTitleString: "", countShow:  riderList.count-1) {}
            ForEach(riderList, id: \.self) { riderId in
                if riderId != ride.leader {
                    if riderId == dtrData.profile.id  {
                        RiderListUI(UserMutualStatusModel: $mutualConnectionAllRiderStatus, myRideForDay: ride, userProfiles: dtrData.profile, showRideLeader: .constant(false))
                    }else{
                        RiderListUI(UserMutualStatusModel: $mutualConnectionAllRiderStatus, myRideForDay: ride, userProfiles: dtrData.profileDirectory[dynamicMember: riderId], showRideLeader: .constant(false))
                    }
                }
            }
        }
    }
}



