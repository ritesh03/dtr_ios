//
//  RiderListUI.swift
//  dtr-ios-test
//
//  Created by Mobile on 16/03/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI


struct RiderListUI: View {
    @Binding var UserMutualStatusModel : [UserMutualStatusModel]
    var myRideForDay : DtrRide
    @State var userProfiles : DtrProfile
    @Binding var showRideLeader : Bool
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var titleStatus = RideListUIString.FollowStr
    @State var isFriendScreen = false
    @State var showUserProfile = false
    @State private var showActivityIndicator = false
    
    var body: some View {
        NavigationLink(destination: ProfileScreen(updatedDismissView:.constant(false),day: .constant(0),userProfile: $userProfiles,showBackButtonOrNot:true)){
            VStack {
                HStack(alignment:.top) {
                    if showRideLeader {
                        leaderHead(ride: myRideForDay, size: .medium)
                    }else{
                        ProfileHead(profile: userProfiles, size: .medium)
                    }
                    VStack(alignment: .leading, spacing: 10){
                        if showRideLeader {
                            Text(RideListUIString.RideLeaderStr).font(.system(size: 13))
                                .foregroundColor(.gray)
                        }
                        if userProfiles.name != CommonAllString.BlankStr {
                            Text(userProfiles.name).font(.system(size: 16))
                                .foregroundColor(.white)
                        }
                        if userProfiles.description != CommonAllString.BlankStr {
                            Text(userProfiles.description).font(.system(size: 13))
                                .foregroundColor(.gray)
                                .lineLimit(1)
                        }
                        
                        if userProfiles.id != dtrData.profile.id {
                            HStack(alignment:.center) {
                                Group {
                                    Text(getMutualConnectionStatus(userId: userProfiles.id)).font(.system(size: 14))
                                }.foregroundColor(.white)
                                .opacity(0.65)
                            }
                        }
                    }
                    .frame(maxWidth: .infinity ,alignment: .topLeading)
                    .padding(.horizontal)
                    if dtrData.profileID != userProfiles.id {
                        if !dtrData.profile.friends.contains(userProfiles.id) {
                            if self.showActivityIndicator {
                                ProgressView()
                                    .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                    .frame(width: 20, height: 20)
                                    .padding(.trailing,5)
                            }
                            
                            if titleStatus != RideListUIString.RquestFriendSmallStr {
                                Button(action: {
                                    callFuntionAccToStatus()
                                }) {
                                    Text(getStatusOfUser(userId: userProfiles.id))
                                        .multilineTextAlignment(.trailing)
                                        .lineLimit(2)
                                        .foregroundColor(titleStatus == RideListUIString.FriendRequestSentStr ? Color.gray : Color.init(ColorConstantsName.AccentColour))
                                        .frame(width: titleStatus == RideListUIString.FollowStr ? 90 : 115, alignment: .center).padding(5)
                                        .font(.system(size: 16, weight: .bold, design: .default))
                                }.buttonStyle(BorderlessButtonStyle())
                                .clipShape(Capsule())
                                .overlay(Capsule().stroke(Color.init(ColorConstantsName.AccentColour), lineWidth: titleStatus == RideListUIString.FriendRequestSentStr ? 0 : 1))
                            }
                        }
                    }
                }
                .padding(.all)
                Divider()
                    .background(Color.white)
            }
            .onChange(of: userProfiles.isFollower) { ( newValue ) in
                userProfiles.isFollower = newValue
            }
        }
    }
    
    func getMutualConnectionStatus(userId:String) -> String {
        var status = ""
        for i in 0..<UserMutualStatusModel.count {
            if let val = UserMutualStatusModel[i] as? UserMutualStatusModel {
                if val.userId == userId {
                    status = val.staus
                }
            }
        }
        return status
    }
    
    func getStatusOfUser(userId:String) -> String {
        var status = RideListUIString.FollowStr
        if dtrData.relationship(profileID: userId).rawValue == RideListUIString.FollowingSmallStr {
            status = RideListUIString.RequestFriendStr
        }else if dtrData.relationship(profileID: userId).rawValue == RideListUIString.FollowingWithInviteStr {
            status = RideListUIString.FriendRequestSentStr
        }else if dtrData.relationship(profileID: userId).rawValue == RideListUIString.RquestFriendSmallStr {
            status = RideListUIString.RquestFriendSmallStr
        }
        DispatchQueue.main.async {
            titleStatus = status
        }
        return status
    }    
    
    func callFuntionAccToStatus() {
        if titleStatus == RideListUIString.FollowStr {
            showActivityIndicator = true
            self.dtrCommands.profileFollow(profileID: userProfiles.id){ respose in
                if respose.feedback.contains(RideListUIString.AlreadyFriendsStr) {
                    titleStatus = RideListUIString.FollowingFcapsStr
                    showActivityIndicator = false
                } else {
                    titleStatus = (respose.result == .error) ? respose.feedback : RideListUIString.FollowingFcapsStr
                    if titleStatus == RideListUIString.FollowingFcapsStr {
                        userProfiles.isFollower = true
                        userProfiles.followed.append(dtrData.profileID)
                    }
                    showActivityIndicator = false
                }
            }
        } else if titleStatus == RideListUIString.RequestFriendStr {
            showActivityIndicator = true
            self.dtrCommands.sendFriendRequest(profileID: userProfiles.id){ respose in
                showActivityIndicator = false
            }
        }
    }
    
    
    func leaderHead(ride: DtrRide?, size: ProfileSize) -> AnyView {
        if let ride = ride {
            var leaderProfile : String?
            leaderProfile = ride.riderProfiles[ride.leader]?.id
            if leaderProfile == nil {
                leaderProfile = ride.id
            }
            let crownSize: CGFloat = (size == .medium) ? 25 : 33
            let offsetX: CGFloat = (size == .medium) ? 0 : 0
            let offsetY: CGFloat = (size == .medium) ? -25 : -10
            if leaderProfile != CommonAllString.BlankStr && leaderProfile != nil {
                if let dotRange = leaderProfile!.range(of: RideListUIString.PlusStr) {
                    leaderProfile!.removeSubrange(dotRange.lowerBound..<leaderProfile!.endIndex)
                }
            }
            
            return AnyView(
                ZStack {
                    if leaderProfile != CommonAllString.BlankStr && leaderProfile != nil {
                        if dtrData.profileDirectory[dynamicMember: leaderProfile!].profileImage != CommonAllString.BlankStr {
                            WebImage(url:URL(string: dtrData.profileDirectory[dynamicMember: leaderProfile!].profileImage), options: [.progressiveLoad], isAnimating: .constant(true))
                                .purgeable(true)
                                .placeholder(Image(ImageConstantsName.PersonPlaceholderUserImg))
                                .renderingMode(.original)
                                .resizable()
                                .frame(width: 48, height: 48)
                                .clipShape(Circle())
                                .padding(1)
                                .overlay( showBadegImage(rideId: leaderProfile!)
                                            .frame(width: 20, height:20)
                                            .offset(x: 2, y: -2),alignment: .bottomTrailing)
                        } else {
                            Image(uiImage: self.dtrData.profilePictureWithName(profileID: leaderProfile!, inLarge: false))
                                .renderingMode(.original)
                                .resizable()
                                .frame(width: 48, height: 48)
                                .clipShape(Circle())
                                .padding(1)
                                .overlay( showBadegImage(rideId: leaderProfile!)
                                            .frame(width: 20, height:20)
                                            .offset(x: 2, y: -2),alignment: .bottomTrailing)
                        }
                    }
                    if ride.riders.count > 1 {
                        ZStack {
                            Image(ImageConstantsName.CrownImg)
                                .renderingMode(.original)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: crownSize, height: crownSize)
                                .shadow(radius: 1)
                        }
                        .offset(x: offsetX, y: offsetY)
                        .rotationEffect(.degrees(13))
                    }
                }
            )
        }
        return AnyView(EmptyView())
    }
    
    func riderHead(profileID: String, size: ProfileSize) -> AnyView {
        return AnyView(
            ProfilePicture(picture: dtrData.profilePicture(profileID: profileID), size: size)
        )
    }
    
    func showBadegImage(rideId: String) -> AnyView {
        if dtrData.profileDirectory[dynamicMember: rideId].badge != "" {
            if dtrData.badgesDirectory[dynamicMember: dtrData.profileDirectory[dynamicMember: rideId].badge].iconInput != "" {
                return AnyView(
                    WebImage(url:URL(string: dtrData.badgesDirectory[dynamicMember: dtrData.profileDirectory[dynamicMember: rideId].badge].iconInput), options: [.progressiveLoad], isAnimating: .constant(true))
                        .purgeable(true)
                        .renderingMode(.original)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 20, height: 20)
                        .clipShape(Circle())
                )
            }
        }
        return AnyView(EmptyView())
    }

}
