//
//  SenderReciverUI.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/02/21.
//

import SwiftUI

struct SenderReciverUI: View {
    var message: DtrRideMessage
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var showProfile = false
    
    var senderIsMe: Bool {
        self.message.sender == self.dtrData.profile.id
    }
    
    var sender: DtrProfile {
        if senderIsMe {
            return self.dtrData.profile
        }
        return self.dtrData.profileDirectory[dynamicMember: self.message.sender]
    }
    
    var body: some View {
        Group {
            if senderIsMe  {
                Group {
                    HStack(alignment: .bottom){
                        Spacer()
                        Text(self.message.time).font(.system(size: 10))
                            .foregroundColor(.gray)
                        HStack {
                            let urlContain = checkMesgCOntainUrlOrNot(inputString: self.message.message,isOpenUrl: false)
                            if urlContain != CommonAllString.BlankStr {
                                Group {
                                    if urlContain.lowercased() != self.message.message.lowercased() {
                                        Text(self.message.message)
                                            .contextMenu(ContextMenu(menuItems: {
                                              Button("Copy", action: {
                                                UIPasteboard.general.string = self.message.message
                                              })
                                            }))
                                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                                            .multilineTextAlignment(.trailing)
                                            .padding(10)
                                            .background(Color.init(ColorConstantsName.HeaderBgColour))
                                            .cornerRadius(10.0)
                                    } else {
                                        Text(urlContain)
                                            .contextMenu(ContextMenu(menuItems: {
                                              Button("Copy", action: {
                                                UIPasteboard.general.string = urlContain
                                              })
                                            }))
                                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                                            .multilineTextAlignment(.trailing)
                                            .padding(10)
                                            .background(Color.init(ColorConstantsName.HeaderBgColour))
                                            .cornerRadius(10.0)
                                    }
                                }.onTapGesture {
                                    let _ = checkMesgCOntainUrlOrNot(inputString: urlContain,isOpenUrl: true)
                                }
                            }else{
                                Group {
                                    Text(self.message.message)
                                        .contextMenu(ContextMenu(menuItems: {
                                          Button("Copy", action: {
                                            UIPasteboard.general.string = self.message.message
                                          })
                                        }))
                                        .foregroundColor(.white)
                                        .multilineTextAlignment(.trailing)
                                        .padding(10)
                                        .background(Color.init(ColorConstantsName.HeaderBgColour))
                                        .cornerRadius(10.0)
                                }
                            }
                        }
                        .background(Color.init(ColorConstantsName.ChatSenderBgColour))
                        .cornerRadius(10.0)
                    }.padding(.vertical,5)
                }
            } else {
                if message.sender == ChatScreenSenderString.SystemStr {
                    VStack(alignment: .center,spacing:10){
                        Text(self.message.time)
                            .foregroundColor(.white)
                            .opacity(0.32)
                        if self.message.message.contains(ChatScreenSenderString.TimeStr) {
                            if let range = self.message.message.range(of: ChatScreenSenderString.ToStr) {
                                let timeGet = self.message.message[range.upperBound...].trimmingCharacters(in: .whitespaces)
                                Text(self.message.message.replacingOccurrences(of: timeGet, with: CommonAllString.BlankStr))
                                    .contextMenu(ContextMenu(menuItems: {
                                      Button("Copy", action: {
                                        UIPasteboard.general.string = self.message.message.replacingOccurrences(of: timeGet, with: CommonAllString.BlankStr)
                                      })
                                    }))
                                    .foregroundColor(.white)
                                    .opacity(0.65)
                                Group{
                                    HStack{
                                        Image(ImageConstantsName.ClockImg)
                                        Text(timeGet)
                                    }
                                }.foregroundColor(.white)
                            }
                        }else if  self.message.message.contains(ChatScreenSenderString.LocationStr) {
                            Group {
                                if let range = self.message.message.range(of: ChatScreenSenderString.ToStr) {
                                    let locationGet = self.message.message[range.upperBound...].trimmingCharacters(in: .whitespaces)
                                    Text(self.message.message.replacingOccurrences(of: locationGet, with: CommonAllString.BlankStr))
                                        .contextMenu(ContextMenu(menuItems: {
                                          Button("Copy", action: {
                                            UIPasteboard.general.string = self.message.message.replacingOccurrences(of: locationGet, with: CommonAllString.BlankStr)
                                          })
                                        }))
                                        .foregroundColor(.white)
                                        .opacity(0.65)
                                    Group{
                                        HStack{
                                            Image(ImageConstantsNameForChatScreen.LocationIconImg)
                                            Text(locationGet)
                                        }
                                    }.foregroundColor(.white)
                                }
                            }
                        }else {
                            Group {
                                if message.message.contains(ChatScreenSenderString.LeftStr) || message.message.contains(ChatScreenSenderString.JoinedStr) {
                                    Text(self.message.message)
                                        .foregroundColor(.white)
                                        .opacity(0.65)
                                }else{
                                    if let range = self.message.message.range(of:DayContainerString.DetailStr) {
                                        Group {
                                            let locationGet = self.message.message[range.upperBound...].trimmingCharacters(in: .whitespaces)
                                            Text(self.message.message.replacingOccurrences(of: locationGet, with: CommonAllString.BlankStr))
                                                .contextMenu(ContextMenu(menuItems: {
                                                  Button("Copy", action: {
                                                    UIPasteboard.general.string = self.message.message.replacingOccurrences(of: locationGet, with: CommonAllString.BlankStr)
                                                  })
                                                }))
                                                .foregroundColor(.white)
                                                .opacity(0.65)
                                        }
                                    }else {
                                        Group {
                                            if let range = self.message.message.range(of:"updated tags") {
                                                let locationGet = self.message.message[range.upperBound...].trimmingCharacters(in: .whitespaces)
                                                Text(self.message.message.replacingOccurrences(of: locationGet, with: CommonAllString.BlankStr))
                                                    .contextMenu(ContextMenu(menuItems: {
                                                      Button("Copy", action: {
                                                        UIPasteboard.general.string = self.message.message.replacingOccurrences(of: locationGet, with: CommonAllString.BlankStr)
                                                      })
                                                    }))
                                                    .foregroundColor(.white)
                                                    .opacity(0.65)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }.frame(maxWidth: .infinity, alignment: .center)
                    .padding()
                    .padding(.leading,20)
                    .font(.system(size: 14))
                } else {
                    HStack(alignment: .bottom){
                        ProfileHead(profile: sender, size: .medium,comeFromChatScreen:true)
                            .onTapGesture {
                                if message.sender != ChatScreenSenderString.SystemStr  {
                                    self.showProfile = true
                                }
                            }
                        VStack(alignment: .leading,spacing:5) {
                            Text(sender.name).font(.system(size: 10))
                                .foregroundColor(.gray)
                            HStack(alignment: .bottom) {
                                let urlContain = checkMesgCOntainUrlOrNot(inputString: self.message.message,isOpenUrl: false)
                                if urlContain != CommonAllString.BlankStr {
                                    Group{
                                        if urlContain.lowercased() != self.message.message.lowercased() {
                                            Text(self.message.message)
                                                .contextMenu(ContextMenu(menuItems: {
                                                  Button("Copy", action: {
                                                    UIPasteboard.general.string = self.message.message
                                                  })
                                                }))
                                                .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                                                .multilineTextAlignment(.leading)
                                                .padding(10)
                                                .background(Color.init(ColorConstantsName.HeaderBgColour))
                                                .cornerRadius(10.0)
                                        }else{
                                            Text(urlContain)
                                                .contextMenu(ContextMenu(menuItems: {
                                                  Button("Copy", action: {
                                                    UIPasteboard.general.string = urlContain
                                                  })
                                                }))
                                                .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                                                .multilineTextAlignment(.leading)
                                                .padding(10)
                                                .background(Color.init(ColorConstantsName.HeaderBgColour))
                                                .cornerRadius(10.0)
                                        }
                                    }.onTapGesture {
                                        let _ = checkMesgCOntainUrlOrNot(inputString: urlContain,isOpenUrl: true)
                                    }
                                }else{
                                    Text(self.message.message)
                                        .contextMenu(ContextMenu(menuItems: {
                                          Button("Copy", action: {
                                            UIPasteboard.general.string = self.message.message
                                          })
                                        }))
                                        .foregroundColor(.white)
                                        .multilineTextAlignment(.leading)
                                        .padding(10)
                                        .background(Color.init(ColorConstantsName.HeaderBgColour))
                                        .cornerRadius(10.0)
                                }
                                HStack{
                                    Text(self.message.time).font(.system(size: 10))
                                        .foregroundColor(.gray)
                                        .padding(.trailing)
                                }
                            }
                        }
                        Spacer()
                    }.padding(.vertical,5)
                    .sheet(isPresented: self.$showProfile) {
                        Modal(isPresented: self.$showProfile, title: ChatScreenSenderString.UserStr) {
                            ProfileScreen(updatedDismissView:.constant(false),day: .constant(0),userProfile: .constant(sender), showBackButtonOrNot:true)
                        }
                    }
                }
            }
        }
    }
    
    func checkMesgCOntainUrlOrNot(inputString:String,isOpenUrl:Bool) -> (String) {
        var url = CommonAllString.BlankStr
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: inputString, options: [], range: NSRange(location: 0, length: inputString.utf16.count))
        for match in matches {
            guard let range = Range(match.range, in: inputString) else { continue }
            url = String(inputString[range])
            if isOpenUrl {
                UIApplication.shared.open(URL(string: url.lowercased())!) { (response) in
                    if response != true {
                        if !url.contains("https") {
                            let newUrl = "https://\(url)"
                            UIApplication.shared.open(URL(string: newUrl.lowercased())!) { (response) in
                            }
                        }
                    }
                }
            }
        }
        return (url)
    }
}
