//
//  ChatScreenList.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/02/21.
//

import SwiftUI
import Firebase

struct ChatScreenList: View {
    @Binding var totalCountUnreadMesg:Int
    var dateCode: String
    @State var myRideForDay : DtrRide
    @State private var sendPush = false
    @State private var update: String = CommonAllString.BlankStr
    @State private var notificatinStatus = CommonAllString.BlankStr
    @State private var isAllReadMesg: Bool = false
    @State private var totalCountOnScreen: Int = 0
    @State private var screenChatHeight: CGFloat = 0.0
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @StateObject var scrollviewDirectionModel = ScrollToModel()
    
    
    var messages: [DtrRideMessage] {
        let plan = dtrData.plansMessage[dynamicMember: dateCode]
        if let ride = plan.ride {
            return ride.chat.messages
        }
        return [DtrRideMessage]()
    }
    
    func sendUpdate() {
        let message = self.update.trimmingCharacters(in: .whitespacesAndNewlines)
        if message == "" {
            return
        }
        let notificationMessage = "\(self.dtrData.profile.name) 📣: \(message)"
        let plan = dtrData.plansMessage[dynamicMember: dateCode]
        if let ride = plan.ride {
            Analytics.logEvent(AnalyticsEvent.rideUpdateMessage.rawValue, parameters: ["rideID": ride.id])
            self.dtrCommands.sendMesgTopUpdateRideInfo(rideID: ride.id, message: message, completion: { response in
                if self.sendPush {
                    for profileID in ride.riders {
                        if profileID == self.dtrData.profile.id {
                            continue
                        }
                        self.dtrCommands.sendSimpleNotification(profileID: ride.id, data: ["recipients":[profileID],"type":"rideUpdate","message":notificationMessage]) { ( result) in
                            result.debug(methodName:"sendSimpleNotification")
                            if result.result == .ok {
                                
                            }
                        }
                    }
                }
                sendPush = false
                let dictUsr = DtrRide.init(data: response.output)
                UserStore.shared.UserDeafult.removeObject(forKey: UserStore.shared.SaveTimeStampKey)
                UserStore.shared.SaveTimeStamp = dictUsr.chat.messages.last?.timestamp ?? 0.0
                scrollviewDirectionModel.direction = .end
            })
        }
        self.update = CommonAllString.BlankStr
    }
    
    var body: some View {
        //        NavigationView{
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack(spacing: 0) {
                Divider()
                    .background(Color.white)
                GeometryReader { geo in
                    ScrollViewReader { value in
                        ZStack{
                            ScrollView {
                                VStack{
                                    ForEach(messages.indices.reversed(), id: \.self) { rider in
                                        SenderReciverUI(message: messages[rider])
                                            .scaleEffect(x: 1, y: -1, anchor: .center)
                                            .listRowBackground(Color.clear)
                                    }
                                }.onReceive(scrollviewDirectionModel.$direction) { action in
                                    guard !messages.isEmpty else { return }
                                    withAnimation {
                                        value.scrollTo(messages.count-1, anchor: .bottom)
                                    }
                                }.background(GeometryReader {
                                    Color.clear.preference(key: ViewOffsetKey.self,value: -$0.frame(in: .named("scroll")).origin.y)
                                }).onPreferenceChange(ViewOffsetKey.self) {
                                    screenChatHeight = $0
                                }
                            }.coordinateSpace(name: "scroll").padding()
                                .listStyle(PlainListStyle())
                                .offset(x: 0, y: -1)
                                .scaleEffect(x: 1, y: -1, anchor: .center)
                            Spacer()
                                .foregroundColor(Color.init(ColorConstantsName.HeaderBgColour))
                            if screenChatHeight >= 100.0 {
                                HStack(alignment:.bottom){
                                    Button(action: {
                                        isAllReadMesg = true
                                        withAnimation {
                                            value.scrollTo(messages.count-1, anchor: .bottom)
                                        }
                                    }, label: {
                                        CustomCircleImageWithCustomProperty(imgName: ImageConstantsNameForChatScreen.DownArrowWithCircleImg, imgWidth: 30, imgHeight: 30)
                                    })
                                }.frame(maxWidth:.infinity,maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .bottomTrailing)
                                    .padding()
                            }
                            
                            HStack(alignment:.bottom){
                                ShowPopUpMesg(titleMesg: ChatScreenSenderString.MegaPhoneTurnedOnStr, customMesg: ChatScreenSenderString.MegaPhoneTurnedStatusStr, show: $sendPush, opticity: .constant(false))
                            }
                            .frame(maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .bottomTrailing)
                            .padding()
                        }
                    }
                }
                
                HStack {
                    Button(action: {
                        sendPush.toggle()
                    }) {
                        if  sendPush {
                            Image(uiImage: UIImage(named: ImageConstantsNameForChatScreen.MegaPhoneOnImg)!)
                                .renderingMode(.original)
                        }else{
                            Image(uiImage: UIImage(named: ImageConstantsNameForChatScreen.MegaPhoneOffImg)!)
                                .renderingMode(.original)
                        }
                    }
                    .padding(.trailing, 6)
                    MultilineTextField(ChatScreenSenderString.WriteAMesgStr, text: self.$update, onCommit: {
                        sendUpdate()
                    })
                }.padding()
                    .background(Color.init(ColorConstantsName.HeaderBgColour))
            }
        }
        .onAppear {
            Analytics.logEvent(AnalyticsScreen.user.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modalNav.rawValue])
            if messages.last != nil {
                if UserStore.shared.SaveTimeStamp != 0.0 {
                    UserStore.shared.UserDeafult.removeObject(forKey: UserStore.shared.SaveTimeStampKey)
                    UserStore.shared.SaveTimeStamp = messages.last!.timestamp
                }else{
                    UserStore.shared.SaveTimeStamp = messages.last!.timestamp
                }
                
                UserStore.shared.setTimeStmpWithRideid(rideiD: myRideForDay.id, timeStmp: messages.last!.timestamp)
                
                isAllReadMesg = true
            }
        }
        
        .onDisappear {
            totalCountUnreadMesg = 0
        }
        
//        .onReceive(self.dtrData.11$myRidesByDate) { rideDict in
//            checkUnreadMesg()
//        }
        
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle(CommonAllString.BlankStr, displayMode: .inline)
        .navigationBarItems(leading: btnBack, trailing: goToFollowScreen)
        //        }
    }
    
    func checkUnreadMesg() {
        guard let minIndex = messages.firstIndex(where: {$0.timestamp > UserStore.shared.SaveTimeStamp}) else { return }
        let totalCount = messages.count - minIndex
        if totalCount > 0 {
            if messages.last?.sender != dtrData.profileID {
                isAllReadMesg = false
            }
        }
    }
    
    var btnBack: some View {
        Button(action: {
            self.presentationMode.wrappedValue.dismiss()
        }) {
            VStack{
                HStack(alignment:.top) {
                    Image(systemName: ImageConstantsName.ArrowLeftImg)
                        .aspectRatio(contentMode: .fit)
                    VStack(alignment:.leading,spacing:5){
                        Text(ChatScreenSenderString.RideUpdateStr)
                        Text(dayName(dateCode: dateCode)).font(.system(size: 10))
                            .padding(.bottom,5)
                    }
                }.foregroundColor(.white)
            }
        }
    }
    
    var goToFollowScreen: some View {
        NavigationLink(destination: FollowerScreen(currentDay:.constant(0),comesFromChatScreen:true)) {
            Image(systemName: ImageConstantsNameForChatScreen.PersonImg)
                .padding(.trailing)
        }
    }
}

class ScrollToModel: ObservableObject {
    enum Action {
        case end
    }
    @Published var direction: Action? = nil
}
