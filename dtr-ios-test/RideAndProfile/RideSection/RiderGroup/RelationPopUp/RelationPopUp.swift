//
//  RelationPopUp.swift
//  dtr-ios-test
//
//  Created by Mobile on 29/01/21.
//

import SwiftUI

struct RelationPopUp: View {
    var arrayCustom:[String]
    @Binding var show: Bool
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    var completion : ((String)->Void)? = nil
    
    var body: some View {
        if show {
            ZStack {
                VStack(alignment:.leading,spacing:30) {
                    ForEach(self.arrayCustom.indices, id: \.self) { index in
                        Text(self.arrayCustom[index])
                            .frame(maxWidth: 150, alignment: .leading)
                            .background(Color(colorScheme == .light ? .white : .black))
                            .onTapGesture {
                                show = false
                                completion?(self.arrayCustom[index])
                            }
                    }
                }
            }.padding()
            .background(Color(colorScheme == .light ? .white : .black))
            .foregroundColor(colorScheme == .light ? .black : .white)
            .cornerRadius(10)
        }
    }
}


struct CustomWidthPoup: View {
    @Binding var arrayCustom:[String]
    @Binding var show: Bool
    @State var ridesToDisplay : DtrRide
    @State var comeFromCustomView = false
    var completion : ((String)->Void)? = nil
    
    var body: some View {
        if show {
            ZStack {
                VStack(alignment:.leading,spacing:30) {
                    ForEach(self.arrayCustom.indices, id: \.self) { index in
                        Text(self.arrayCustom[index]).foregroundColor(comeFromCustomView == true ? Color.white : Color.black)
                            .frame(maxWidth: 150, alignment: .leading)
                            .background(comeFromCustomView == true ? Color.init(ColorConstantsName.HeaderBgColour) : Color.white)
                            .onTapGesture {
                                show = false
                                completion?(self.arrayCustom[index])
                            }
                    }
                }
            }.padding()
            .background(comeFromCustomView == true ? Color.init(ColorConstantsName.HeaderBgColour) : Color.white)
            .cornerRadius(10)
        }
    }
}

struct CustomPoupForLeaveRide: View {
    
    @Binding var arrayCustom:[String]
    @Binding var show: Bool
    var completion : ((String)->Void)? = nil
    
    var body: some View {
        if show {
            ZStack {
                VStack(alignment:.leading,spacing:20) {
                    ForEach(self.arrayCustom.indices, id: \.self) { index in
                        Text(self.arrayCustom[index])
                            .frame(maxWidth: 2000, alignment: .leading)
                            .background(Color.init(UIColor.init(displayP3Red: 29/255, green: 33/255, blue: 33/255, alpha: 1.0)))
                            .onTapGesture {
                                show = false
                                completion?(self.arrayCustom[index])
                            }
                    }
                }
                
            }.padding(20)
            .background(Color.init(UIColor.init(displayP3Red: 29/255, green: 33/255, blue: 33/255, alpha: 1.0)))
            .foregroundColor(.white)
            .cornerRadius(10)
        }
    }
}
