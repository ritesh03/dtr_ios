//
//  NotificationSettingScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 26/03/21.
//

import SwiftUI

struct NotificationSettingScreen: View {
    @State var titleArray : [String] = ["Enable friend requests", "Enable friend messages", "Enable ride invites", "Enable ride updates","Enable ride cancellations"]
    @State var selections: [String] = []
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            ScrollView {
                VStack {
                    ForEach(titleArray.indices, id: \.self) { ride in
                        NotificationScreenUI(title: titleArray[ride], isSelected: self.selections.contains(titleArray[ride])) {
                            if self.selections.contains(titleArray[ride]) {
                                self.selections.removeAll(where: { $0 == titleArray[ride] })
                            } else {
                                self.selections.append(titleArray[ride])
                            }
                        }
                    }
                }
            }
        }
    }
}

struct NotificationSettingScreen_Previews: PreviewProvider {
    static var previews: some View {
        NotificationSettingScreen()
    }
}
