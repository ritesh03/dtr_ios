//
//  NotificationScreenUI.swift
//  dtr-ios-test
//
//  Created by Mobile on 26/03/21.
//

import SwiftUI

struct NotificationScreenUI: View {
    var title: String
    var isSelected: Bool
    var action: () -> Void
    
    var body: some View {
        ScrollView {
            VStack {
                Button(action: self.action) {
                    HStack(alignment:.top) {
                        Toggle(isOn:  .constant(isSelected)){
                            Text(title)
                                .foregroundColor(.white)
                        }.toggleStyle(SwitchToggleStyle(tint: Color.init(ColorConstantsName.AccentColour)))
                    }.padding(10)
                }.cornerRadius(10)
            }
        }
    }
}

struct NotificationScreenUI_Previews: PreviewProvider {
    static var previews: some View {
        NotificationScreenUI(title: "Testing", isSelected: false, action: {
            
        })
    }
}
