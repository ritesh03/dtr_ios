//
//  MapScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 24/02/21.
//

import SwiftUI
import Mapbox
import MapKit
import MapboxGeocoder
import Firebase
import CoreLocation

struct MapScreen: View {
    @Binding var day: Int
    @Binding var fullLocationName: String
    @State var showPublicRideLocationPopUp: Bool = false
    @Binding var isOnBoradingLat: Double
    @Binding var isOnBoradingLon: Double
    @State var canEditLocation: Bool = false
    @Binding var startCountry: String
    @Binding var startPlace: String
    @Binding var isOnBoradingProcess:Bool
    @Binding var isUpdateRideLocation:Bool
    @Binding var showRideLocationScreen:Bool
    @State var isFromProfile = false
    var ride: DtrRide = DtrRide.default
    @State var startPlaceNew: String  = ""
    @State var mapReset: Bool = false
    @State var selectionCity: String = ""
    @State var selectionCountry: String = ""
    @State var canShowLongPrsVw: Bool = false
    @State var canSetSrchLocation: Bool = false
    @State var canSetPublicLocation: Bool = false
    @State var srchCoordinate = CLLocationCoordinate2D()
    @State var selectionAnnotation = [MGLPointAnnotation]()
    @State var selectionCoordinate = CLLocationCoordinate2D()
    
    @State private var titleDtr = ""
    @State private var showListng = false
    @State private var clickOnReturnBtn = false
    @State private var startPlaceLat: Double = 0.0
    @State private var startPlaceLon: Double = 0.0
    @State private var hasStartPlace: Bool = false
    @State private var addNewAnottionSrchBr = false
    @State private var locationWasChanged: Bool = false
    @State private var hasLocationPermission: Bool = false
    @State private var selectedValue: Bool = false
    @State private var searchResults: [GeocodedPlacemark] = [GeocodedPlacemark].init()
    @State private var selectedResult: [GeocodedPlacemark] = [GeocodedPlacemark].init()
    var updateRide = false
    
    @ObservedObject var myRideForDay : RideEditViewModel
    @Environment(\.presentationMode) var presentationMode
    private let mapView: MGLMapView = MGLMapView(frame: .zero, styleURL: MGLStyle.streetsStyleURL)
    
    var completion: () -> Void = { }
    
    private let locationManager = CLLocationManager()
    
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack {
                MapView(selectionCity: $selectionCity, addNewAnottionSrchBr: $addNewAnottionSrchBr, selectionCountry: $selectionCountry, selectionCoordinate: $selectionCoordinate, serchCordinator: $srchCoordinate,   ride: ride, canSetLocation:true, updateRide: updateRide,canSetSrchLocation:$canSetSrchLocation, mapReset: $mapReset, annotations:selectionAnnotation) { hasStartPlace, cityName, startPlaceCoord,countryName  in
                    self.locationWasChanged = true
                    self.hasStartPlace = hasStartPlace
                    self.startPlaceNew = cityName
                    self.startPlace =  cityName
                    self.startCountry = countryName
                    self.selectionCountry = countryName
                    self.startPlaceLat = startPlaceCoord.latitude
                    self.startPlaceLon = startPlaceCoord.longitude
                }
            }
            
            VStack {
                navSrchBarWithBackIcon(canShowLongPrsVw:$canShowLongPrsVw,showPublicRideLocationPopUp:$showPublicRideLocationPopUp,startPlaceLat:$startPlaceLat,startPlaceLon:$startPlaceLon,showRideLocationScreen: $showRideLocationScreen, isOnBoradingProcess: $isOnBoradingProcess, titleDtr: $titleDtr, showListng: $showListng,clickOnReturnBtn:$clickOnReturnBtn, searchResults: $searchResults)
                    .frame(height: 60, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .padding()
                    .background(Color.init(ColorConstantsName.MainThemeBgColour))
                if showListng {
                    ZStack {
                        if searchResults.count > 0 {
                            ScrollView {
                                VStack(alignment:.leading,spacing:10){
                                    ForEach(searchResults.indices, id: \.self) { index in
                                        HStack {
                                            Image(ImageConstantsName.LocationMapIconImg)
                                                .padding(.horizontal,8)
                                            VStack(alignment:.leading,spacing:10) {
                                                Group {
                                                    Text(searchResults[index].name)
                                                        .foregroundColor(Color.black)
                                                        .padding(.top,10)
                                                        .font(.system(size: 14))
                                                    Group {
                                                        if searchResults[index].postalAddress?.state != "" {
                                                            if searchResults[index].postalAddress?.city != "" {
                                                                Text(searchResults[index].postalAddress?.city ?? "")  + Text( ", ") + Text(searchResults[index].postalAddress?.state ?? "") + Text( ", ") + Text(searchResults[index].country?.name ?? "")
                                                            }else{
                                                                Text(searchResults[index].postalAddress?.state ?? "") + Text( ", ") + Text(searchResults[index].country?.name ?? "")
                                                            }
                                                        }else {
                                                            if searchResults[index].country?.name != "" && searchResults[index].country?.name != nil {
                                                                if searchResults[index].postalAddress?.city != "" {
                                                                    Text(searchResults[index].postalAddress?.city ?? "") + Text( ", ") + Text(searchResults[index].country?.name ?? "")
                                                                }else{
                                                                    Text(searchResults[index].country?.name ?? "")
                                                                }
                                                            }else{
                                                                EmptyView()
                                                            }
                                                        }
                                                    }
                                                    .foregroundColor(Color.gray)
                                                    .font(.system(size: 12))
                                                    Divider()
                                                        .background(Color.gray)
                                                        .padding(.bottom,5)
                                                }
                                                .foregroundColor(.white)
                                                .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                                            }
                                        }.background(Color.white)
                                        .onTapGesture {
                                            titleDtr = searchResults[index].name
                                            selectedResult.removeAll()
                                            selectedResult.append(searchResults[index])
                                            srchCoordinate =  CLLocationCoordinate2D(latitude: (searchResults[index].location?.coordinate.latitude)!, longitude: (searchResults[index].location?.coordinate.longitude)!)
                                            showListng = false
                                            addNewAnottionSrchBr = true
                                            canSetSrchLocation = true
                                            selectedValue = true
                                            clickOnReturnBtn = false
                                            print("Tap Tap Tap Gesture")
                                        }
                                    }
                                }.padding()
                                .background(Color.white)
                                .cornerRadius(10.0)
                            }
                        }
                    }
                    .padding(.top,-40)
                    .padding(.horizontal,30.5)
                    .padding(.leading,29.5)
                    //Spacer()
                    .padding(.bottom,clickOnReturnBtn == true ? 80 : 0)
                }
                if canEditLocation {
                    if !canShowLongPrsVw {
                        LongTapGestureVw(isFromProfile: isFromProfile)
                            .padding(.horizontal)
                    }
                }
                Spacer()
            }
            
            if  !UserStore.shared.publicViewAlreadyShowOrNot {
                if showPublicRideLocationPopUp {
                    ZStack{
                        HStack {
                            PublicRideLocationChange(showPublicRideLocationPopUp: $showPublicRideLocationPopUp, day: $day).frame(width:UIScreen.screenWidth*0.95,height:UIScreen.screenHeight*0.65, alignment: .center).cornerRadius(20).onAppear {
                                canShowLongPrsVw = true
                            }
                        }
                    }
                }
            }
            
            ZStack{
                HStack {
                    ShowAlertIfPermissonNotAvaible(titleMesg: #"Allow "Down to Ride" to access your location"#, customMesg: "DTR requires permission to access your location to fetch public rides, view or set ride location. ", show: $hasLocationPermission, opticity: .constant(true)).padding()
                }
            }
            
            VStack {
                Spacer()
                if !showListng {
                    returnClearAndSaveButtonView
                }
                
                if clickOnReturnBtn && searchResults.count > 0 {
                    returnClearAndSaveButtonView
                }
            }

        }.onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
            canShowLongPrsVw = true
            showPublicRideLocationPopUp = false
        })
        
        .onChange(of: selectedValue, perform: { repsosne in
                if  repsosne {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        selectedResult.removeAll()
                        showListng = false
                        selectedValue = false
                    }
                }
            })
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
            if CLLocationManager.locationServicesEnabled() {
                switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied:
                    hasLocationPermission = true
                case .authorizedAlways, .authorizedWhenInUse:
                    hasLocationPermission = false
                @unknown default:
                    break
                }
            }
        }
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                if CLLocationManager.locationServicesEnabled() {
                    switch locationManager.authorizationStatus {
                    case .notDetermined, .restricted, .denied:
                        hasLocationPermission = true
                    case .authorizedAlways, .authorizedWhenInUse:
                        hasLocationPermission = false
                    @unknown default:
                        break
                    }
                } else {
                    print("Location services are not enabled")
                }
            }
            
            Analytics.logEvent(AnalyticsScreen.rideLocation.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
            if !canEditLocation {
                canSetSrchLocation = true
                srchCoordinate =  CLLocationCoordinate2D(latitude: myRideForDay.rideEdit.details.startPlaceLat, longitude: myRideForDay.rideEdit.details.startPlaceLon)
                addNewAnottionSrchBr = true
            }
            
            if myRideForDay.rideEdit.details.hasStartPlace {
                canSetSrchLocation = true
                srchCoordinate =  CLLocationCoordinate2D(latitude: myRideForDay.rideEdit.details.startPlaceLat, longitude: myRideForDay.rideEdit.details.startPlaceLon)
                addNewAnottionSrchBr = true
            }
            
            if canSetPublicLocation {
                canSetSrchLocation = true
                srchCoordinate =  CLLocationCoordinate2D(latitude: isOnBoradingLat, longitude: isOnBoradingLon)
                addNewAnottionSrchBr = true
            }
        }
        .hiddenNavigationBarStyle()
    }
    
    var returnClearAndSaveButtonView: some View {
        VStack {
            Spacer()
            Group {
                HStack {
                    if canEditLocation {
                        HStack {
                            CustomButtonCircleWithCustomProperty(btnTitle: "Clear location", btnWidth: 135, btnHeight: 20, action:{
                                mapReset = true
                                titleDtr = ""
                                searchResults.removeAll()
                                self.locationWasChanged = false
                                self.startPlaceLat = 0.0
                                self.startPlaceLon  = 0.0
                                self.startPlaceNew = "Location TBD"
                                self.startPlace =  ""
                                self.startCountry = ""
                            })
                            Spacer()
                            CustomButtonCircleWithCustomProperty(btnTitle: "Set location", btnWidth: 135, btnHeight: 20,backgroundColord:true, action:{
                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
                                myRideForDay.rideEdit.details.hasStartPlace = self.locationWasChanged
                                myRideForDay.rideEdit.details.startPlaceLat = self.startPlaceLat
                                myRideForDay.rideEdit.details.startPlaceLon = self.startPlaceLon
                                let fullAddress = "\(titleDtr)\(titleDtr == "" ? "" : ", ")" + "\(self.startPlaceNew == "Location TBD" ? "" : self.startPlaceNew)"
                                myRideForDay.rideEdit.details.startPlace = fullAddress
                                fullLocationName = fullAddress
                                isUpdateRideLocation = true
                                isOnBoradingLat = self.startPlaceLat
                                isOnBoradingLon = self.startPlaceLon
                                if canSetPublicLocation {
                                    if startPlaceLat == 0.0 &&  startPlaceLat == 0.0 {
                                        self.startPlaceLat = DtrData.sharedInstance.profile.userLat
                                        self.startPlaceLon  = DtrData.sharedInstance.profile.userLon
                                        self.startCountry = DtrData.sharedInstance.profile.country
                                        self.startPlace = DtrData.sharedInstance.profile.cityState
                                        self.isOnBoradingLat = DtrData.sharedInstance.profile.userLat
                                        self.isOnBoradingLon = DtrData.sharedInstance.profile.userLon
                                    }
                                }
                                self.completion()
                                if isOnBoradingProcess == true {
                                    presentationMode.wrappedValue.dismiss()
                                } else {
                                    showRideLocationScreen.toggle()
                                }
                            })
                        }
                    }else{
                        Button(action: {
                            let coordinate = CLLocationCoordinate2D(latitude: myRideForDay.rideEdit.details.startPlaceLat, longitude: myRideForDay.rideEdit.details.startPlaceLon)
                            let span = MKCoordinateRegion(center: coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
                            let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
                            let mapItem = MKMapItem(placemark: placemark)
                            mapItem.name = myRideForDay.rideEdit.summary
                            mapItem.openInMaps(launchOptions: [
                                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: span.center)
                            ])
                        }) {
                            Text("Get Directions")
                                .padding(20)
                                .background(Color.init(ColorConstantsName.AccentColour))
                                .cornerRadius(CGFloat(8))
                                .foregroundColor(.white)
                        }
                        .disabled(!myRideForDay.rideEdit.details.hasStartPlace)
                    }
                }.frame(height: 50,alignment: .bottom)
                    .padding()
                    .background(canEditLocation ? Color.black:Color.clear)
                    .opacity(0.75)
            }
        }
    }
    
    
    func openSettingWhenLocationDisable(){
        let alert = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: .alert)
        
        let cancellButton = UIAlertAction(title: "No", style: .destructive) { (_) in
            UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: false, completion: nil)
        }
        let okButton = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })
        alert.addAction(okButton)
        alert.addAction(cancellButton)
        UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true, completion: {
        })
    }
    
    func getLocation(from address: String, completion: @escaping (_ location: CLLocationCoordinate2D?)-> Void) {
        let geocoder = Geocoder(accessToken: "sk.eyJ1IjoiZG93bnRvcmlkZSIsImEiOiJja2xjZnV3dHQ0MG42MnBwbHBjaWt5em9yIn0.BSwmoCG-nGcmjcsC5r5QHw")
        let foptions = ForwardGeocodeOptions(query: address)
        foptions.focalLocation = CLLocation(latitude: startPlaceLat, longitude: startPlaceLon)
        geocoder.geocode(foptions) { (placemarks, attribution ,error) in
            if placemarks?.count ?? 0 > 0 {
                if ((placemarks?.contains(selectedResult[0])) != nil){
                    let filtered = placemarks!.filter { $0 == selectedResult[0] }
                    if filtered.count > 0 {
                        let location = filtered[0].location?.coordinate
                        completion(location)
                    }else{
                        let location = placemarks!.first?.location?.coordinate
                        completion(location)
                    }
                }else{
                    for i in 0..<placemarks!.count {
                        if placemarks![i].postalAddress?.state == selectedResult[0].postalAddress?.state {
                            let location = placemarks![i].location?.coordinate
                            completion(location)
                            break
                        }
                        
                        if placemarks![i].postalAddress?.city == selectedResult[0].postalAddress?.city {
                            let location = placemarks![i].location?.coordinate
                            completion(location)
                            break
                        }
                    }
                }
            }else{
                completion(nil)
                return
            }
        }
    }
}

struct MapScreen_Previews: PreviewProvider {
    static var previews: some View {
        MapScreen(day: .constant(0),fullLocationName:.constant(""), isOnBoradingLat:.constant(0.0),isOnBoradingLon:.constant(0.0),startCountry: .constant(""), startPlace: .constant(""), isOnBoradingProcess: .constant(false), isUpdateRideLocation: .constant(false), showRideLocationScreen: .constant(false), myRideForDay: RideEditViewModel.init(ride: DtrRide.default))
    }
}

struct navSrchBarWithBackIcon: View {
    @Binding  var canShowLongPrsVw: Bool
    @Binding  var showPublicRideLocationPopUp: Bool
    @Binding  var startPlaceLat: Double
    @Binding  var startPlaceLon: Double
    @Binding  var showRideLocationScreen:Bool
    @Binding  var isOnBoradingProcess:Bool
    @Binding  var titleDtr: String
    @Binding  var showListng :Bool
    @Binding  var clickOnReturnBtn :Bool
    @Binding  var searchResults: [GeocodedPlacemark]
    @Environment(\.presentationMode) var presentationMode
    
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack {
                HStack {
                    HStack {
                        Button(action: {
                            if isOnBoradingProcess == true {
                                presentationMode.wrappedValue.dismiss()
                            }else{
                                showRideLocationScreen.toggle()
                            }
                        }) {
                            HStack {
                                Image(systemName: ImageConstantsName.ArrowLeftImg)
                                    .aspectRatio(contentMode: .fit)
                                    .foregroundColor(.white)
                            }}
                        HStack {
                            let binding = Binding<String>(get: {
                                self.titleDtr
                            }, set: {
                                self.titleDtr = $0
                                if titleDtr.count == 0 {
                                    showListng = false
                                }else{
                                    showListng = true
                                    clickOnReturnBtn = false
                                }
                                findResults(address: titleDtr)
                            })
                            
                            TextField("Search for a location", text: binding,onCommit: {
                                if showListng {
                                    clickOnReturnBtn = true
                                }
                            })
                                .foregroundColor(.white)
                            Button(action: {
                                showListng = false
                                titleDtr = ""
                                searchResults.removeAll()
                            }) {
                                Image(systemName:ImageConstantsName.XmarkImg)
                                    .aspectRatio(contentMode: .fit)
                                    .foregroundColor(.white)
                            }
                        }
                        .padding()
                        .background(Color.init(ColorConstantsName.SectionBgColour))
                        .cornerRadius(10)
                        .foregroundColor(.black)
                        .padding(.all)
                    }
                }
                Spacer()
            }.onTapGesture(count: /*@START_MENU_TOKEN@*/1/*@END_MENU_TOKEN@*/, perform: {
                canShowLongPrsVw = true
                showPublicRideLocationPopUp = false
            })
        }.hiddenNavigationBarStyle()
    }
    
    func findResults(address: String) {
        let geocoder = Geocoder(accessToken: "sk.eyJ1IjoiZG93bnRvcmlkZSIsImEiOiJja2xjZnV3dHQ0MG42MnBwbHBjaWt5em9yIn0.BSwmoCG-nGcmjcsC5r5QHw")
        let foptions = ForwardGeocodeOptions(query: address)
        foptions.focalLocation = CLLocation(latitude: startPlaceLat, longitude: startPlaceLon)
        foptions.maximumResultCount = 10
        geocoder.geocode(foptions) { (placemarks, attribution ,error) in
            guard let placemarks = placemarks else {
                return
            }
            self.searchResults = []
            for placemark in placemarks {
                self.searchResults.append(placemark)
            }
            if searchResults.count == 0 {
                showListng = false
            }
        }
    }
}
