//
//  ShowAlertIfPermissonNotAvaible.swift
//  dtr-ios-test
//
//  Created by apple on 13/07/21.
//

import SwiftUI

struct ShowAlertIfPermissonNotAvaible: View {
    var titleMesg:String
    var customMesg:String
    @Binding var show: Bool
    @Binding var opticity: Bool
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        if show {
            ZStack {
                VStack(alignment:.center,spacing:20) {
                    Group{
                        HStack(alignment: .top){
                            Text(titleMesg).frame(maxWidth: .infinity, alignment: .center)
                                .font(.system(size: 16, weight: .bold, design: .default))
                            Spacer()
                        }
                        HStack(alignment: .top){
                            Text(customMesg).frame(maxWidth: .infinity, alignment: .center)
                                .font(.system(size: 14, weight: .regular, design: .default))
                            Spacer()
                        }
                    }.foregroundColor(Color.white)
                    HStack {
                        HStack {
                            CustomButtonCircleWithCustomProperty(btnTitle: "Allow", btnWidth: 135, btnHeight: 20, action:{
                                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
                            })
                            Spacer()
                            CustomButtonCircleWithCustomProperty(btnTitle: "Don't allow", btnWidth: 135, btnHeight: 20, action:{
                                self.presentationMode.wrappedValue.dismiss()
                            })
                        }
                    }.frame(height: 50)
                    .padding(10)
                    .opacity(0.75)
                }
            }.padding(10)
            .background( Color.init(ColorConstantsName.MainThemeBgColour))
            .opacity(opticity == false ? 0.95:1.0)
            .foregroundColor(.black)
            .cornerRadius(10)
        }
    }
}

struct ShowAlertIfPermissonNotAvaible_Previews: PreviewProvider {
    static var previews: some View {
        ShowAlertIfPermissonNotAvaible(titleMesg: "", customMesg: "", show: .constant(true), opticity: .constant(true))
    }
}
