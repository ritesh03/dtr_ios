//
//  MapBox.swift
//  dtr-ios-test
//
//  Created by Mobile on 24/02/21.
//

import SwiftUI
import Mapbox

typealias MapBoxViewCompletion = ((Bool, String, CLLocationCoordinate2D,String) -> Void)

struct MapView: UIViewRepresentable {
    
    private let mapView: MGLMapView = MGLMapView(frame: .zero, styleURL: MGLStyle.streetsStyleURL)
    @Binding var selectionCity: String
    @Binding var selectionCountry: String
    @Binding var selectionCoordinate: CLLocationCoordinate2D
    @Binding var addNewAnottionSrchBr: Bool
    var canSetLocation: Bool
    var updateRide: Bool
    @Binding var canSetSrchLocation: Bool
    var action: MapBoxViewCompletion
    @Binding var mapReset: Bool
    @State private var hasSelection: Bool
    @State private var city: String
    @State private var Country: String
    @State private var address: String
    @State private var coordinate: CLLocationCoordinate2D
    @Binding  var serchCordinator: CLLocationCoordinate2D
    
    @State private var currentLocationCity = ""
    @State private var currentLocationCountry = ""
    @State private var currentLocationCoordinate = CLLocationCoordinate2D()
    @Binding var annotations: [MGLPointAnnotation]
    
    init(selectionCity: Binding<String>,addNewAnottionSrchBr: Binding<Bool>,selectionCountry: Binding<String>, selectionCoordinate: Binding<CLLocationCoordinate2D>, serchCordinator: Binding<CLLocationCoordinate2D>, ride: DtrRide, canSetLocation: Bool = false, updateRide: Bool = false, canSetSrchLocation: Binding<Bool>, mapReset: Binding<Bool>,annotations: [MGLPointAnnotation], completion: @escaping MapChooserCompletion) {
        _selectionCity = selectionCity
        _selectionCountry = selectionCountry
        _selectionCoordinate = selectionCoordinate
        _serchCordinator = serchCordinator
        _addNewAnottionSrchBr  = addNewAnottionSrchBr
        self.canSetLocation = canSetLocation
        _canSetSrchLocation = canSetSrchLocation
        _mapReset = mapReset
        self.action = completion
        let initialCoordinate = CLLocationCoordinate2D(latitude: ride.details.startPlaceLat, longitude: ride.details.startPlaceLon)
        _hasSelection = State(initialValue: ride.details.hasStartPlace)
        _city = State(initialValue: ride.details.startPlace)
        _Country = State(initialValue: ride.details.startPlace)
        _address = State(initialValue: "")
        _coordinate = State(initialValue: initialCoordinate)
        _annotations = .constant([MGLPointAnnotation].init())
        self.updateRide = updateRide
//        print("updateRide-->",updateRide)
    }
    
    func isChange(city: String, coordinate: CLLocationCoordinate2D) -> Bool {
        if city != self.city {
            return true
        }
        if coordinate.latitude != self.coordinate.latitude {
            return true
        }
        if coordinate.longitude != self.coordinate.longitude {
            return true
        }
        return false
    }
    
    
    // MARK: - Configuring UIViewRepresentable protocol
    func makeUIView(context: UIViewRepresentableContext<MapView>) -> MGLMapView {
        mapView.showsScale = true
        mapView.userLocation?.title = ""
        mapView.delegate = context.coordinator
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
        
        if self.canSetLocation {
            let gesture = UILongPressGestureRecognizer(target: context.coordinator, action: #selector(Coordinator.gesture))
            gesture.minimumPressDuration = 0.5
            mapView.addGestureRecognizer(gesture)
        }
        
        if self.coordinate.latitude != 0 && self.coordinate.longitude != 0 {
            let annotation = MGLPointAnnotation()
            annotation.coordinate = self.coordinate
            annotation.title = self.city
            let location = CLLocation(latitude: self.coordinate.latitude, longitude: self.coordinate.longitude)
            CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
                guard error == nil else { return }
                let city = resolvedCityState(from: placemarks?.first)
                annotation.title = city
                
                if let theAddress = resolvedAddress(from: placemarks?.first) {
                    annotation.subtitle = theAddress
                }
            }
            mapView.addAnnotation(annotation)
            mapView.showAnnotations(mapView.annotations!, animated: false)
            DispatchQueue.main.asyncAfter(wallDeadline: .now() + .seconds(1)) {
                mapView.selectAnnotation(annotation, animated: true) {
                }
            }
        }
        return mapView
    }
    
    
    func updateUIView(_ uiView: MGLMapView, context: UIViewRepresentableContext<MapView>) {
        if let currentAnnotations = uiView.annotations {
            if currentAnnotations.count > 0 {
                if canSetSrchLocation {
                    setSrchLocationOnMap(uiView: uiView)
                }else{
                    mapView.addAnnotations(annotations)
                }
            }
        }else{
            if canSetSrchLocation {
                setSrchLocationOnMap(uiView: uiView)
            }else{
                mapView.addAnnotations(annotations)
            }
        }
        
        if self.mapReset {
            let scrchCntrCor = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
            uiView.setCenter(scrchCntrCor, animated: true)
            uiView.removeAnnotations(uiView.annotations ?? [])
            uiView.userTrackingMode = .follow
            DispatchQueue.main.async {
                self.mapReset = false
            }
        }
    }
    
    func setSrchLocationOnMap(uiView:MGLMapView){
        DispatchQueue.main.async {
            uiView.userTrackingMode = .none
            if let currentAnnotations = uiView.annotations {
                if currentAnnotations.count > 0 {
                    uiView.removeAnnotation(currentAnnotations.last!)
                }
            }
            let annotation = MGLPointAnnotation()
            let scrchCntrCor = CLLocationCoordinate2D(latitude: serchCordinator.latitude, longitude: serchCordinator.longitude)
            uiView.zoomLevel = 14.0
            annotation.coordinate = scrchCntrCor
            uiView.addAnnotation(annotation)
            uiView.setCenter(scrchCntrCor, animated: true)
            canSetSrchLocation = false
            
            let location = CLLocation(latitude: serchCordinator.latitude, longitude: serchCordinator.longitude)
            DispatchQueue.main.asyncAfter(wallDeadline: .now() + .seconds(1)) {
                uiView.selectAnnotation(annotation, animated: true) {
                }
            }
            
            CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
                guard error == nil else { return }
                let city = resolvedCityState(from: placemarks?.first)
                annotation.title = city
                let country = resolvedCountry(from: placemarks?.first)
                if let theAddress = resolvedAddress(from: placemarks?.first) {
                    annotation.subtitle = theAddress
                }
                self.city = city
                self.Country = country
                self.coordinate = location.coordinate
                self.selectionCity = city
                self.selectionCountry  = country
                self.selectionCoordinate = location.coordinate
                if self.updateRide {
                    self.action(true, "\(placemarks?.first?.name ?? "") \(placemarks?.first?.name == "" ? "": ",") \(self.city)", self.coordinate, self.Country)
                } else {
                    self.action(true, "\(self.city)", self.coordinate, self.Country)
                }
                //self.prints(placemarks:(placemarks?.first)!)
            }
        }
    }
    
//    func prints(placemarks:CLPlacemark){
//        print("name = ",placemarks.name,
//              "thoroughfare = ",placemarks.thoroughfare,
//              "subThoroughfare = ",placemarks.subThoroughfare,
//              "locality = ",placemarks.locality,
//              "subLocality = ",placemarks.subLocality,
//              "administrativeArea = ",placemarks.administrativeArea,
//              "subAdministrativeArea = ",placemarks.subAdministrativeArea,
//              "postalCode = ",placemarks.postalCode,
//              "isoCountryCode = ",placemarks.isoCountryCode,
//              "country = ",placemarks.country)
//    }
    
    func makeCoordinator() -> MapView.Coordinator {
        Coordinator(self)
    }
    
    // MARK: - Configuring MGLMapView
    func styleURL(_ styleURL: URL) -> MapView {
        mapView.styleURL = styleURL
        return self
    }
    
    func centerCoordinate(_ centerCoordinate: CLLocationCoordinate2D) -> MapView {
        mapView.centerCoordinate = centerCoordinate
        return self
    }
    
    func zoomLevel(_ zoomLevel: Double) -> MapView {
        mapView.zoomLevel = zoomLevel
        return self
    }
    
    
    //MARK: - Implementing MGLMapViewDelegate
    final class Coordinator: NSObject, MGLMapViewDelegate {
        var control: MapView
        var annotation: MGLPointAnnotation?
        init(_ control: MapView) {
            self.control = control
        }
        
        func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        }
        
        func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
            return true
        }
        
        func mapView(_ mapView: MGLMapView, didFailToLocateUserWithError error: Error) {
            print("Location Error: \(error.localizedDescription)")
        }
        
        func mapView(_ mapView: MGLMapView, didUpdate userLocation: MGLUserLocation?) {
            if let location = userLocation?.location {
                CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
                    guard error == nil else { return }
                    self.control.currentLocationCoordinate = location.coordinate
                    let city = resolvedCityState(from: placemarks?.first)
                    self.control.currentLocationCity = city
                    let country = resolvedCountry(from: placemarks?.first)
                    self.control.currentLocationCountry = country
                    if self.control.selectionCity == "" {
                        self.control.selectionCity = city
                        self.control.Country = country
                        self.control.selectionCoordinate = location.coordinate
                        self.control.coordinate = location.coordinate
                        self.control.city = self.resolveValue(city)
                        self.control.address = self.resolveValue(country)
                        self.control.Country = self.resolveValue(country)
                        self.control.selectionCity =  self.control.city
                        self.control.selectionCoordinate =  self.control.coordinate
                        self.control.selectionCountry =  self.control.Country
                        if self.control.updateRide {
                            self.control.action(true, "\(placemarks?.first?.name ?? "") \(placemarks?.first?.name == "" ? "": ",") \(self.control.city)", self.control.coordinate, self.control.Country)
                        } else {
                            self.control.action(true, "\(self.control.city)", self.control.coordinate, self.control.Country)
                        }
                        //self.prints(placemarks: (placemarks?.first)!)
                    }
                }
            }
        }
        
        func mapView(_ mapView: MGLMapView, didSelect annotationView: MGLAnnotationView) {
            if annotationView.annotation is MGLUserLocation { return }
            guard let annotation = annotationView.annotation else { return }
            control.hasSelection = true
            let city = resolveValue(annotation.title)
            if control.isChange(city: city, coordinate: annotation.coordinate) {
                control.coordinate = annotation.coordinate
                control.city = resolveValue(annotation.title)
                control.address = resolveValue(annotation.subtitle)
                control.selectionCity = control.city
                control.selectionCoordinate = control.coordinate
                control.selectionCountry = control.Country
                control.action(true, control.city, control.coordinate, control.Country)
            }
        }
        
        private func resolveValue(_ value: String??) -> String {
            if let hasValue = value {
                if let value = hasValue {
                    return value
                }
            }
            return ""
        }
        
//        func prints(placemarks:CLPlacemark){
//            print("name = ",placemarks.name,
//                  "thoroughfare = ",placemarks.thoroughfare,
//                  "subThoroughfare = ",placemarks.subThoroughfare,
//                  "locality = ",placemarks.locality,
//                  "subLocality = ",placemarks.subLocality,
//                  "administrativeArea = ",placemarks.administrativeArea,
//                  "subAdministrativeArea = ",placemarks.subAdministrativeArea,
//                  "postalCode = ",placemarks.postalCode,
//                  "isoCountryCode = ",placemarks.isoCountryCode,
//                  "country = ",placemarks.country)
//        }
        
        
        @objc func gesture(recognizer: UIGestureRecognizer) {
            if let map = recognizer.view as? MGLMapView {
                let point = recognizer.location(in: map)
                let coordinate = map.convert(point, toCoordinateFrom: map)
                switch recognizer.state {
                case UIGestureRecognizer.State.began:
                    map.userTrackingMode = .none
                    if let currentAnnotations = map.annotations {
                        if currentAnnotations.count > 0 {
                            map.removeAnnotation(currentAnnotations.last!)
                        }
                    }
                    let annotation = MGLPointAnnotation()
                    annotation.coordinate = coordinate
                    map.addAnnotation(annotation)
                    self.annotation = annotation
                case .changed:
                    if let annotation = self.annotation {
                        annotation.coordinate = coordinate
                    }
                case .ended:
                    if let annotation = self.annotation {
                        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                        DispatchQueue.main.asyncAfter(wallDeadline: .now() + .seconds(1)) {
                            map.selectAnnotation(annotation, animated: true) {
                            }
                        }
                        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
                            guard error == nil else { return }
                            let city = resolvedCityState(from: placemarks?.first)
                            annotation.title = city
                            let country = resolvedCountry(from: placemarks?.first)
                            if let theAddress = resolvedAddress(from: placemarks?.first) {
                                annotation.subtitle = theAddress
                            }
                            self.control.city = city
                            self.control.Country = country
                            self.control.coordinate = location.coordinate
                            self.control.selectionCity = city
                            self.control.selectionCountry  = country
                            self.control.selectionCoordinate = location.coordinate
                            if self.control.updateRide {
                                self.control.action(true, "\(placemarks?.first?.name ?? "") \(placemarks?.first?.name == "" ? "": ",") \(self.control.city)", self.control.coordinate, self.control.Country)
                            } else {
                                self.control.action(true, "\(self.control.city)", self.control.coordinate, self.control.Country)
                            }
                            //self.prints(placemarks: (placemarks?.first)!)
                        }
                    }
                default:
                    _ = "nothing"
                }
            }
        }
    }
}
