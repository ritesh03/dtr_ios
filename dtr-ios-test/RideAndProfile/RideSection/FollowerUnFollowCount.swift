//
//  FollowerUnFollowCount.swift
//  dtr-ios-test
//
//  Created by Mobile on 21/01/21.
//

import SwiftUI

struct FollowerUnFollowCount: View {
    
    @Binding var profile : DtrProfile
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State private var relationBtnHidden = false
    @State private var showPopUp: Bool = false
    @Binding var showPopUpSendReq : Bool
    @Binding var isAlreadyFriend : Bool
    @State private var alreadyShowingSendReqVw: Bool = false
    @State private var userActionShow: Bool = false
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading) {
                HStack {
                    NavigationLink(destination:ListOfUsers(profile: profile, checkStatus: "Following")) {
                        VStack(alignment:.leading,spacing:10) {
                            Text("\(profile.following.count)")
                                .foregroundColor(.white)
                            Text("Following")
                                .foregroundColor(.gray)
                        }
                    }
                    
                    NavigationLink(destination:ListOfUsers(profile: profile, checkStatus: "Followers")) {
                        VStack(alignment:.leading,spacing:10) {
                            Text("\(profile.followed.count)")
                                .foregroundColor(.white)
                            Text("Followers")
                                .foregroundColor(.gray)
                        }
                    }
                    
                    NavigationLink(destination:ListOfUsers(profile: profile, checkStatus: "Friends")) {
                        VStack(alignment:.leading,spacing:10) {
                            Text("\(profile.friends.count)")
                                .foregroundColor(.white)
                            Text("Friends")
                                .foregroundColor(.gray)
                        }
                    }
                    Spacer()
                    if dtrData.profileID != profile.id {
                        Text(returnRelation().0).font(.system(size: 15, weight: .bold, design: .default))
                            .foregroundColor(Color.init("DTR-Crown"))
                            .multilineTextAlignment(.trailing)
                            .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    }
                }.padding()
                .background(Color.init("DTR-SectionBackground"))
                if dtrData.profileID != profile.id {
                    VStack(alignment: .leading, spacing: 20) {
                        Text(returnRelation().4)
                            .fixedSize(horizontal: false, vertical:true )
                            .foregroundColor(.white)
                        HStack(spacing:20){
                            if !returnRelation().2 {
                                if returnRelation().1 != "" {
                                    Button(action: {
                                        updateRelationStatus(returnRelation().1)
                                    }) {
                                        Text(returnRelation().1)
                                            .frame(width: 200, height: 25)
                                            .padding()
                                            .foregroundColor(.white)
                                            .background(Color.init("DTR-Accent"))
                                            .cornerRadius(200/2)
                                    }
                                }else{
                                    Spacer()
                                }
                            }
                            
                            if returnRelation().3.count > 0 {
                                Button(action: {
                                    showPopUpSendReq = false
                                   // withAnimation(.linear(duration: 0.3)) {
                                        if returnRelation().3.count > 0 {
                                            showPopUp.toggle()
                                        }
                                    //}
                                }){
                                    Image("Outlined button (5)")
                                        .resizable()
                                        .frame(width: 40, height: 40)
//                                    Circle()
//                                        .strokeBorder(Color.init("DTR-Accent"),lineWidth: 2)
//                                        .background(
//                                            Image("Outlined button (5)")
//                                                .resizable()
//                                                .frame(width: 30, height: 30)
//                                        )
//                                        .frame(width: 50, height: 50)
                                }
                            }
                        }
                    }.padding(.all)
                }
            }.background(Color.init("DTR-MainThemeBackground"))
            if userActionShow {
                RelationPopUp(arrayCustom: returnRelation().3, show: $showPopUp) { (index) in
                    updateRelationStatus(index)
                }.offset(x: 50, y: 50)
            }
        }.onTapGesture {
            if showPopUp == true {
                showPopUp.toggle()
            }
        }
    }
    
    func updateRelationStatus(_ index:String){
        if index == "Cancel friend request" {
            self.dtrCommands.userDecline(profileID: profile.id){ respose in
                respose.debug()
            }
        }else if index == "Unfollow" {
            self.dtrCommands.profileUnfollow(profileID: profile.id){ respose in
                respose.debug()
                profile.isFollower = false
                for _ in (0..<profile.followed.count) {
                     if profile.followed.contains(dtrData.profileID) {
                        profile.followed.removeAll(where: { $0 == dtrData.profileID})
                    }
                }
            }
        }else if index == "UnFriend" {
            self.dtrCommands.userUnfriend(profileID: profile.id){ respose in
                respose.debug()
            }
        } else if index == "Follow" {
            if self.dtrData.relationship(profileID: profile.id) == Relationship.none || self.dtrData.relationship(profileID: profile.id) == Relationship.followed {
                self.dtrCommands.profileFollow(profileID: profile.id){ respose in
                    respose.debug()
                    profile.isFollower = true
                    profile.followed.append(dtrData.profileID)
                }
            }
        } else if index == "SEND FRIEND REQUEST" {
            if self.dtrData.relationship(profileID: profile.id) == Relationship.following {
                self.dtrCommands.sendFriendRequest(profileID: profile.id){ respose in
                    respose.debug()
                }
            }
        }
    }
    
    func returnRelation() -> (String,String,Bool,[String],String) {
        var status = ""
        var status1 = ""
        var relationBtnHidden = false
        var arrayCustom = [String]()
        var txtCenterDescrp = ""
        
        if self.dtrData.relationship(profileID: profile.id) == Relationship.none {
            status = "Not connected"
            status1 = "Follow"
            txtCenterDescrp = "Follow \(profile.name) to connect and see his rides for followers."
        }
        
        // if user follow me but me not follow again 
        if self.dtrData.relationship(profileID: profile.id) == Relationship.followed {
            status = "Not connected"
            status1 = "Follow"
            txtCenterDescrp = "Follow \(profile.name) to connect and see his rides for followers."
        }
        
        if self.dtrData.relationship(profileID: profile.id) == Relationship.following {
            status = "Following"
            status1 = "SEND FRIEND REQUEST"
            txtCenterDescrp = "Send friend request to \(profile.name) to see friends-only rides and information."
            arrayCustom.append("Unfollow")
            DispatchQueue.main.async {
                userActionShow = true
                if !alreadyShowingSendReqVw {
                    showPopUpSendReq = true
                    alreadyShowingSendReqVw = true
                }
            }
        }
        
        if self.dtrData.relationship(profileID: profile.id) == Relationship.invitedFriend {
            status = "Friend request sent"
            arrayCustom.append("Cancel friend request")
            DispatchQueue.main.async {
                userActionShow = true
            }
            status1 = "CANCEL REQUEST"
            
            relationBtnHidden = true
            txtCenterDescrp = "You sent \(profile.name) a friend request"
        }
        
        if self.dtrData.relationship(profileID: profile.id) == Relationship.follwoingWithInvite {
            status = "Following, Friend request sent"
            arrayCustom.append("Unfollow")
            arrayCustom.append("Cancel friend request")
            DispatchQueue.main.async {
                userActionShow = true
            }
            status1 = "CANCEL REQUEST"
            txtCenterDescrp = "You sent \(profile.name) a friend request"
        }
        
        if self.dtrData.relationship(profileID: profile.id) == Relationship.acceptedFriend {
            status = "Friend"
            //status1 = "ADD TO FAVORITE" version 2.0
            status1 = ""
            arrayCustom.append("UnFriend")
            
            txtCenterDescrp = "Get notified whenever \(profile.name) has new updates by adding them to your favorite list."
            DispatchQueue.main.async {
                userActionShow = true
                isAlreadyFriend = true
            }
        }
        return (status,status1,relationBtnHidden,arrayCustom,txtCenterDescrp)
    }
}


struct FollowerUnFollowCount_Previews: PreviewProvider {
    static var previews: some View {
        FollowerUnFollowCount(profile: .constant(DtrProfile.default), showPopUpSendReq: .constant(false), isAlreadyFriend: .constant(false))
    }
}
