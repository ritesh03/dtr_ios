//
//  FollowerScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 27/01/21.
//

import SwiftUI

struct FollowerScreen: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var dtrData = DtrData.sharedInstance
    @StateObject var userSearchModel = UserSearchViewModel()
    @State var userProfiles = [DtrProfile]()
    @State private var titleDtr: String = ""
    @State private var showSearchTxtFld: Bool = false
    
    var body: some View {
        ZStack {
            Color.init("DTR-MainThemeBackground")
                .ignoresSafeArea()
            VStack(spacing: 15) {
                if showSearchTxtFld {
                    HStack {
                        TextField("", text: $titleDtr, onEditingChanged: { isEditing in
                        }, onCommit: {
                           dtrData.searchUsers(nameStrng: titleDtr) { (profileUser) in
                            }
                        })
                        Button(action: {
                            withAnimation {
                                self.showSearchTxtFld.toggle()
                                titleDtr = ""
                            }
                        }) {
                            Image(systemName: "xmark")
                                .aspectRatio(contentMode: .fit)
                        }
                    }
                    .padding()
                    .background(Color.init("DTR-SectionBackground"))
                    .cornerRadius(10)
                    .foregroundColor(.white)
                    .padding(.all)
                }
                ScrollView {
                    LazyVStack{
                        ForEach(dtrData.userProfiles.indices, id: \.self) { indexs in
                            UserSearchView(userProfiles:$dtrData.userProfiles[indexs],isFriendScreen: true)
                                .listRowBackground(Color.clear)
                        }
                    }
                }
            }
            .onReceive(self.userSearchModel.$userProfiles) { ridesForDate in
                userProfiles = ridesForDate
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarItems(leading:
                                Button(action: {
                                    self.presentationMode.wrappedValue.dismiss()
                                }) {
                                    HStack {
                                        Image(systemName: "arrow.left")
                                        Text("Find and Invite")
                                    }},
                            trailing:
                                !self.showSearchTxtFld ? Button(action: {
                                    withAnimation {
                                        self.showSearchTxtFld.toggle()
                                    }
                                }) {
                                    Image(systemName: "magnifyingglass")
                                        .aspectRatio(contentMode: .fit)
                                } : nil
        )
    }
}

//struct FollowerScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        FollowerScreen()
//    }
//}
