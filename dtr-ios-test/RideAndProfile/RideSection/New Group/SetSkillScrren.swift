//
//  SetSkillScrren.swift
//  dtr-ios-test
//
//  Created by Mobile on 09/02/21.
//

import SwiftUI


struct SetSkillScrren: View {
    @State var titleArray : [String] = ["Expert / Elite", "Expert and Fast Sport", "Sport and Fast Beginner", "Beginner - No Drop", "Social ride or other non-ride event"]
    @State var dscrpArry: [String] = ["Reserved for the ultra-fast and experienced cycling crowd. Rarely stops unless stated in the ride details.", "Always fast, sometimes regroups. Flat road speeds ~20MPH.", "Most common group ride speed. Typically has fast spots, slower spots, regroups, and sometimes no-drop.", "Designed to make sure that nobody gets left behind - regardless of speed. Usually has a ride sweeper pulling up the rear.", ""]
    @State var imgArry: [String] = ["DTR-SkiilA", "DTR-SkiilB", "DTR-SkiilC", "DTR-SkiilD", "DTR-SkiilS"]
    @State var skilsSortArry: [String] = ["A", "B", "C", "D", "S"]
    @State var selections: [String] = []
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Binding var ride : DtrRide!
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    var body: some View {
        ZStack {
            Color.init("DTR-MainThemeBackground")
                .ignoresSafeArea()
            VStack(alignment: .leading,spacing:30) {
                Text("Who can see and join this ride?")
                    .foregroundColor(.white)
                ScrollView {
                    VStack {
                        ForEach(skilsSortArry.indices, id: \.self) { ride in
                            SkillUI(title: titleArray[ride], descrption: dscrpArry[ride], imgName: imgArry[ride], isSelected: self.selections.contains(skilsSortArry[ride])) {
                                if self.selections.contains(skilsSortArry[ride]) {
                                    self.selections.removeAll(where: { $0 == skilsSortArry[ride] })
                                } else {
                                    self.selections.append(skilsSortArry[ride])
                                }
                            }
                        }
                    }
                }
                .onAppear{
                    if ride.details.skillA == true {
                        self.selections.append("A")
                    }
                    if ride.details.skillB == true {
                        self.selections.append("B")
                    }
                    if ride.details.skillC == true  {
                        self.selections.append("C")
                    }
                    if ride.details.skillD == true  {
                        self.selections.append("D")
                    }
                    if ride.details.skillS == true {
                        self.selections.append("S")
                    }
                }
            }.padding()
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }) {
                                        HStack {
                                            Image(systemName: "arrow.left")
                                            Text("Set skill level")
                                        }},
                                trailing:
                                    Button(action: {
                                        if self.selections.contains("A") {
                                            ride.details.skillA = true
                                        }else{
                                            ride.details.skillA = false
                                        }
                                        if self.selections.contains("B") {
                                            ride.details.skillB = true
                                        }else{
                                            ride.details.skillB = false
                                        }
                                        if self.selections.contains("C") {
                                            ride.details.skillC = true
                                        }else{
                                            ride.details.skillC = false
                                        }
                                        if self.selections.contains("D") {
                                            ride.details.skillD = true
                                        }else{
                                            ride.details.skillD = false
                                        }
                                        if self.selections.contains("S") {
                                            ride.details.skillS = true
                                        }else{
                                            ride.details.skillS = false
                                        }
                                        dtrCommands.rideUpdate(rideID: ride.id, data: ["summary": ride.summary,"details":["hasStartPlace":ride.details.hasStartPlace,"hasStartTime":ride.details.hasStartTime,"notes":ride.details.notes,"skillA":ride.details.skillA,"skillB":ride.details.skillB,"skillC":ride.details.skillC,"skillD":ride.details.skillD,"skillS":ride.details.skillS,"startPlace":ride.details.startPlace,"startPlaceLat":ride.details.startPlaceLat,"startPlacelon":ride.details.startPlaceLon,"startTime":ride.details.startTime,"visibility":ride.details.visibility.rawValue]]) { result in
                                            result.debug()
                                            if result.result == .ok {
                                                self.presentationMode.wrappedValue.dismiss()
                                            }
                                        }
                                    }) {
                                        Text("Update")
                                            .font(.system(size: 20, weight: .bold, design: .default))
                                            .foregroundColor(Color.init("DTR-Accent"))
                                    })
        }
        
    }
}
