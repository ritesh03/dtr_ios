//
//  ShareProfile.swift
//  dtr-ios-test
//
//  Created by Mobile on 01/04/21.
//

import SwiftUI
import Firebase

struct ShareProfile: View {
    @Binding var isPresented: Bool
    @State var links = [Any]()
    
    var body: some View {
        VStack {
            HStack {
                Text("Share this link with your friends.")
            }
            .padding()
            ShareSheet(activityItems: self.links) { activityType, finished, returnedItems, error in
                if let error = error {
                    print("share sheet error: \(error.localizedDescription)")
                    return
                }
                if finished {
                    let params: DataDict = [
                        "content_type": activityType?.rawValue ?? "unknown",
                        "item_type": "friendLink",
                        "item_id": DtrData.sharedInstance.profile.id
                    ]
                    Analytics.logEvent(AnalyticsEventShare, parameters: params)
                }
            }
            Spacer()
        }
    }
}
