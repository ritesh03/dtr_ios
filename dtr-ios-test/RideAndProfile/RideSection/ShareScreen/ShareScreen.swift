//
//  ShareScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 15/03/21.
//

import SwiftUI
import Firebase

struct ShareScreen: View {
    
    @Binding var isPresented: Bool
    var ride: DtrRide
    @State var showBackBtn = false
    @EnvironmentObject var session: DtrData
    @State var showRider = false
    @State var creatingLinks = false
    @State private var links = [Any]()
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    var displayedDateString: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMMM d"
        let date = toDate(timeString: ride.details.startTime, on: ride.dateCode)
        return formatter.string(from: date)
    }
    
    func constructLinks() {
        self.creatingLinks = true
        if UserStore.shared.isLocalBuild == true {
            guard let link = URL(string: "https://socialactive.page.link/ride/\(self.session.profile.id)/\(self.ride.id)") else {
                print("ERROR constructing initial URL")
                return
            }
            let dynamicLinksDomainURIPrefix = AppInformation.AppDeafultStagingUrl
            createLinkShare(dynamicLinksDomainURIPrefix:dynamicLinksDomainURIPrefix,link:link)
        } else {
            guard let link = URL(string: "https://socialactive.app/ride/\(self.session.profile.id)/\(self.ride.id)") else {
                print("ERROR constructing initial URL")
                return
            }
            let dynamicLinksDomainURIPrefix = AppInformation.AppDeafultUrl
            createLinkShare(dynamicLinksDomainURIPrefix:dynamicLinksDomainURIPrefix,link:link)
        }
    }
    
    func createLinkShare(dynamicLinksDomainURIPrefix:String,link:URL) {
        let appBundleID = AppInformation.AppBundleId
        if let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix) {
            linkBuilder.navigationInfoParameters = DynamicLinkNavigationInfoParameters()
            linkBuilder.navigationInfoParameters?.isForcedRedirectEnabled = true
            linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: appBundleID)
            linkBuilder.iOSParameters?.appStoreID = AppInformation.AppStoreId
            linkBuilder.iOSParameters?.minimumAppVersion = AppInformation.AppMinimumVersion
            linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: appBundleID)
            linkBuilder.androidParameters?.minimumVersion = 0
            linkBuilder.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
            linkBuilder.socialMetaTagParameters!.title = "\(ride.summary) on Down to Ride App."
            linkBuilder.socialMetaTagParameters!.descriptionText = "\(self.session.profileInfo(profileID: ride.leader).name) has invited to you to join their ride, \(ride.summary) on \(dateMonthYearRetrun(dateCode: ride.dateCode))"
            linkBuilder.socialMetaTagParameters!.imageURL = URL(string:ride.rideCoverImage == "" ? DefaultUrlString.defaultRideImageUrl : ride.rideCoverImage)
            guard let longDynamicLink = linkBuilder.url else {
                print("ERROR in linkBuilder")
                return
            }
            DynamicLinkComponents.shortenURL(longDynamicLink, options: nil) { url, warnings, error in
                if let error = error {
                    print("error shortening link: \(error.localizedDescription)")
                    return
                }
                if let url = url {
                    let itemSource = ShareActivityItemSource(type: .ride, message: self.ride.inviteMessage, url: url, imageURl: ride.rideCoverImage == "" ? DefaultUrlString.defaultRideImageUrl : ride.rideCoverImage)
                    self.links = [url, itemSource]
                    self.creatingLinks = false
                    DynamicLinks.performDiagnostics(completion: nil)
                    print("Share Url = ",url)
                    return
                }
            }
        }
    }
    
    var body: some View {
        VStack {
            if showBackBtn {
                HStack(alignment: .top){
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        HStack {
                            Image(systemName:ImageConstantsName.ArrowLeftImg)
                                .foregroundColor(colorScheme == .light ? .black : .white)
                        }}
                    Spacer()
                }.padding()
            }
            HStack {
                VStack(alignment: .leading) {
                    Text(ride.summary).font(.title).fontWeight(.bold)
                    Text(displayedDateString).font(.caption)
                }
                Spacer()
            }
            .padding()
            HStack {
                Text("Share this link with your friends to have them join you.")
            }
            .padding()
            
            if self.creatingLinks {
                ActivityIndicator(isAnimating: self.$creatingLinks, style: .large)
            } else {
                ShareSheet(activityItems: self.links) { activityType, finished, returnedItems, error in
                    if let error = error {
                        print("share sheet error: \(error.localizedDescription)")
                        return
                    }
                    
                    if finished {
                        let _: DataDict = [
                            "content_type": activityType?.rawValue ?? "unknown",
                            "item_type": "rideLink",
                            "item_id": self.ride.id
                        ]
                    }
                }
            }
            Spacer()
        }
        
        .onAppear {
            Analytics.logEvent(AnalyticsObjectEvent.share_ride.rawValue, parameters: nil)
            Analytics.logEvent(AnalyticsScreen.friendLink.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
            self.constructLinks()
        }
    }
}

struct ShareScreen_Previews: PreviewProvider {
    static var previews: some View {
        ShareScreen(isPresented: .constant(false), ride: DtrRide.default)
    }
}
