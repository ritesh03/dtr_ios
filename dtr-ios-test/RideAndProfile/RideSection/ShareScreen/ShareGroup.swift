//
//  ShareGroup.swift
//  dtr-ios-test
//
//  Created by apple on 26/07/21.
//

import SwiftUI
import Firebase

struct ShareGroup: View {
    
    @Binding var isPresented: Bool
    var groupDetails: DtrGroup
    @State var creatingLinks = false
    @State private var links = [Any]()
    
    var body: some View {
        VStack {
            HStack {
                VStack(alignment: .leading) {
                    Text(groupDetails.name).font(.title).fontWeight(.bold)
                }
                Spacer()
            }
            .padding()
            HStack {
                Text("Share this link with your friends to have them join you.")
            }
            .padding()
            
            if self.creatingLinks {
                ActivityIndicator(isAnimating: self.$creatingLinks, style: .large)
            } else {
                ShareSheet(activityItems: self.links) { activityType, finished, returnedItems, error in
                    if let error = error {
                        print("share sheet error: \(error.localizedDescription)")
                        return
                    }
                }
            }
            Spacer()
        }
        
        .onAppear {
          
            Analytics.logEvent(AnalyticsObjectEvent.share_group.rawValue, parameters: nil)
            self.creatingLinks = true
            let itemSource = ShareActivityItemSource(type: .group, message: self.groupDetails.inviteMessage, url: URL(string: groupDetails.groupLink)!, imageURl: groupDetails.image == "" ? DefaultUrlString.defaultRideImageUrl : groupDetails.image)
            self.links = [URL(string: groupDetails.groupLink)!, itemSource]
            DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                self.creatingLinks = false
            }
            print("Group Link =  ",groupDetails.groupLink)
        }
    }
}
