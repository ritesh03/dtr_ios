//
//  InviteMainScreen.swift
//  dtr-ios-test
//
//  Created by apple on 09/08/21.
//

import SwiftUI
import Firebase

enum UserType: Int {
    case friend = 0
    case follower
    case groupMember
}

struct InviteMainScreen: View {
    
    @Binding  var day: Int
    @Binding  var showDeatilPopUp: Bool
    @Binding var ride: DtrRide
    @State var profile: DtrProfile
    @State var opacity:Double = 1.0
    @State var resetLocalValue = false
    @State var selectFriends: [String]
    @State var errorStatus: String = ""
    @State var selectFollowers: [String]
    @State var selectGroupMembers: [String]
    @State var enableDisableInviteBtn:Bool = true
    @State var sendPushNotiftionToInvite: [String] = [""]
    @State var groupID: String = ""
    @State private var friendFollowdRides = [DtrRide]()
    @State var alreadyCalled = false
    
    @State private var isShowSearchbar = false
    @State private var clearAllText: Bool = false
    @State private var showHomeScreen = false
    @State private var isFirstResponder = true
    @State private var titleDtr: String = ""
    
    @State private var slctedDeslctedFriendTittle = "Select all friends"
    @State private var slctedDeslctedFollowersittle = "Select all followers"
    @State var selectedTabType: UserType = .friend
    @StateObject var userListViewModel = UserListViewModel()
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    
    @State var updateKeyboard = false
    @State private var page: Int = 0
    @State private var showListOrNot = false
    @State private var showActivityIndicator = false
    @State var userProfiles = [DtrProfile]()
    @State var listOfGrouoc: [UserMutualStatusModel] = []
    @State private var totalUserCount: Int = 0

    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack(spacing:0) {
                if isShowSearchbar {
                    HStack {
                        CustomTxtfldForFollowerScrn(isDeleteAcntScreen: .constant(false),isGroupListScreen: .constant(false), text:  $titleDtr, clearAllText: $clearAllText, isFirstResponder: $isFirstResponder, completion: { (reponse) in
                            print("response")
                            getSearchListForFirstTime()
                        })
                        if self.showActivityIndicator {
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                .frame(width: 30, height: 30)
                        }
                        
                        
                        Button(action: {
                            withAnimation {
                                page = 0
                                userProfiles.removeAll()
                                clearAllText = true
                                updateKeyboard = false
                                showListOrNot = false
                            }
                        }) {
                            Image(systemName: ImageConstantsName.XmarkImg)
                                .aspectRatio(contentMode: .fit)
                        }
                    }
                        .padding()
                        .background(Color.init(ColorConstantsName.SectionBgColour))
                        .cornerRadius(10)
                        .foregroundColor(.white)
                        .padding(.horizontal, 6)
                        .frame(height: 40)
                        .padding(15)
                }
                HStack {
                    VStack {
                        Button {
                            selectedTabType = .friend
                            getSearchListForFirstTime()
                        } label: {
                            Text("FRIENDS")
                                .foregroundColor(selectedTabType == .friend ? Color.init(ColorConstantsName.LimeColour) : Color.init(UIColor.init(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.32)))
                                .font(Font.system(size: 14, weight: .semibold))
                        }
                        selectedTabType == .friend ?
                        Color.init(ColorConstantsName.LimeColour).frame(height:2) : Color.clear.frame(height:2)
                    }
                    Spacer()
                        VStack {
                            Button {
                                selectedTabType = .groupMember
                                getSearchListForFirstTime()
                            } label: {
                                Text("GROUP MEMBERS")
                                    .foregroundColor(selectedTabType == .groupMember ? Color.init(ColorConstantsName.LimeColour) : Color.init(UIColor.init(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.32)))
                                    .frame(width:150)
                                    .font(Font.system(size: 14, weight: .semibold))
                            }
                            selectedTabType == .groupMember ?
                            Color.init(ColorConstantsName.LimeColour).frame(height:2) : Color.clear.frame(height:2)
                        }.fixedSize()
                        Spacer()
                    VStack {
                        Button {
                            selectedTabType = .follower
                            getSearchListForFirstTime()
                        } label: {
                            Text("FOLLOWERS")
                                .foregroundColor(selectedTabType == .follower ? Color.init(ColorConstantsName.LimeColour) : Color.init(UIColor.init(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.32)))
                                .font(Font.system(size: 14, weight: .semibold))
                            
                        }
                        selectedTabType == .follower ?
                        Color.init(ColorConstantsName.LimeColour).frame(height:2) : Color.clear.frame(height:2)
                    }
                }
                .padding(.top,25)
                .padding(.bottom,-10)
                VStack (alignment:.leading,spacing:0) {
                    List {
                        getUserTypeView()
                    }.listStyle(PlainListStyle())
                }.padding(.top,10)
                
                if errorStatus != "" {
                    Text(errorStatus)
                        .foregroundColor(.white)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                        .padding()
                }
                VStack (alignment:.center,spacing:0) {
                    CustomButtonCircleWithCustomProperty(btnTitle: "Invite", btnWidth: UIScreen.screenWidth-100, btnHeight: 10, backgroundColord: true, txtColorAccent: false, strokeColor: false) {
                        errorStatus = "Sending Invites"
                        enableDisableInviteBtn = true
                        opacity = 0.5
                        let recpitIds = selectFriends.filter { $0 != "" } + selectFollowers.filter { $0 != "" } + selectGroupMembers.filter { $0 != "" }
                        
                            let mesg = "😎 \(dtrData.profile.name) has invited you to \(self.ride.summary) on \(dayName(dateCode: self.ride.dateCode)) "
                            sendPushNotiftionToInvite.removeAll()
                            sendPushNotiftionToInvite = recpitIds
                            
                            DtrCommand.sharedInstance.sendRideInvitation(profileID: self.ride.id, data: ["message":mesg,"recipients":Array(Set(recpitIds))]) { (result) in
                                if result.result == .ok {
                                    errorStatus = ""
                                    showDeatilPopUp = true
                                    removeLocalDataAndPopClas()
                                } else {
                                    opacity = 1.0
                                    enableDisableInviteBtn = false
                                    if result.feedback.contains("already on") {
                                        errorStatus = "All Selected Recipients of Ride Invites are already on the ride."
                                    }else{
                                        errorStatus = result.feedback
                                    }
                                    DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
                                        errorStatus = ""
                                    }
                                }
                            }
                    }.disabled(enableDisableInviteBtn)
                    .padding(.bottom)
                    .opacity(opacity)
                }
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading: AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,textTitle: "Invite", systemImageName: ImageConstantsName.ArrowLeftImg, action: {
                removeLocalDataAndPopClas()
            })).font(Font.system(size: 20, weight: .semibold)), trailing: AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, systemImageName: ImageConstantsName.searchImg, action: {
                self.titleDtr = ""
                self.isShowSearchbar.toggle()
            })).foregroundColor(.white))
        }.onAppear {
            
            selectFriends.removeAll()
            userListViewModel.getFriendsForInvite(profile: dtrData.profile)
            
            selectFollowers.removeAll()
            userListViewModel.getFollowersForInvite(profile: dtrData.profile)
            
            selectGroupMembers.removeAll()
            userListViewModel.getGroupMembersForInvite(profile: dtrData.profile, listOfSelectedMembers: (ride.riders))
            
            Analytics.logEvent(AnalyticsObjectEvent.invite_friend_click.rawValue, parameters: nil)
            Analytics.logEvent(AnalyticsScreen.rideAddRider.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
            if resetLocalValue {
                UserStore.shared.isLocallSelectFriends.removeAll()
                UserStore.shared.isLocallSelectFollowers.removeAll()
                UserStore.shared.isLocallSelectGroupMembers.removeAll()
            }
            
            if UserStore.shared.isLocallSelectFriends.count > 0 {
                DispatchQueue.main.async {
                    selectFriends = UserStore.shared.isLocallSelectFriends
                    UserStore.shared.isLocallSelectFriends.removeAll()
                }
            }
            
            if UserStore.shared.isLocallSelectFollowers.count > 0 {
                DispatchQueue.main.async {
                    selectFollowers = UserStore.shared.isLocallSelectFollowers
                    UserStore.shared.isLocallSelectFollowers.removeAll()
                }
            }
            
            if UserStore.shared.isLocallSelectGroupMembers.count > 0 {
                DispatchQueue.main.async {
                    selectGroupMembers = UserStore.shared.isLocallSelectGroupMembers
                    UserStore.shared.isLocallSelectGroupMembers.removeAll()
                }
            }
            
            getAllRidesForFriendsAndFollowers()
        }
        
        .onDisappear{
            if selectFriends.count > 0 {
                UserStore.shared.isLocallSelectFriends = selectFriends
            }
            
            if selectFollowers.count > 0 {
                UserStore.shared.isLocallSelectFollowers = selectFollowers
            }
            
            if selectGroupMembers.count > 0 {
                UserStore.shared.isLocallSelectGroupMembers = selectGroupMembers
            }
            
            resetLocalValue = false
        }
    }
    
    func getAllRidesForFriendsAndFollowers(){
        let groupUserIds = userListViewModel.userGroups.flatMap({$0.members})
        let userIds = Set(dtrData.profile.friends+dtrData.profile.followed+groupUserIds)
        if !alreadyCalled {
            alreadyCalled = true
            friendFollowdRides.removeAll()
            for document in userIds {
                dtrCommand.getRideInvite(dateCode: dateCode(offset: self.day), userID: document) { response in
                    if let rideDetail =  response.output["ride"] as? DataDict {
                        let dictUsr = DtrRide(data:rideDetail)
                        if dictUsr.id != "+" {
                            friendFollowdRides.append(dictUsr)
                            alreadyCalled = false
                        }
                    }
                }
            }
        }
    }
    
    
    func removeLocalDataAndPopClas() {
        selectFollowers.removeAll()
        selectFriends.removeAll()
        selectGroupMembers.removeAll()
        UserStore.shared.isLocallSelectFriends.removeAll()
        UserStore.shared.isLocallSelectFollowers.removeAll()
        UserStore.shared.isLocallSelectGroupMembers.removeAll()
        presentationMode.wrappedValue.dismiss()
    }
    
    var returnSearchUserList: some View {
        Group{
            let selectedMembers = selectFriends + selectGroupMembers + selectFollowers
            
            ForEach(userProfiles.indices, id: \.self) { indexs in
                InviteScreenUI(day:$day,otherUserRides:$friendFollowdRides,ride:ride,userProfiles: .constant(userProfiles[indexs]), isSelected: selectedMembers.contains(userProfiles[indexs].id), action: {
                    if selectedMembers.contains(userProfiles[indexs].id) {
                        self.selectFriends.removeAll(where: { $0 == userProfiles[indexs].id })
                        
                        self.selectGroupMembers.removeAll(where: { $0 == userProfiles[indexs].id })
                        
                        self.selectFollowers.removeAll(where: { $0 == userProfiles[indexs].id })
                    } else {
                        if !ride.riders.contains(userProfiles[indexs].id) {
                            self.selectFriends.append(userProfiles[indexs].id)
                        }
                    }
                    
                    
                    if selectFriends.count > 0 {
                        enableDisableInviteBtn = false
                    } else if selectFriends.count <= 0 {
                        enableDisableInviteBtn = true
                    }
                }).listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
            }
        }
    }
    
    var returnFriendsUserLists: some View {
        Group{
            if userListViewModel.userProfiles.count > 0 {
                let selectedMembers = selectFriends + selectGroupMembers + selectFollowers
                SectionTitleViewUI(titleString: "Friends", rightTitleString: slctedDeslctedFriendTittle, countShow: userListViewModel.userProfiles.count) {
                    if slctedDeslctedFriendTittle != "Deselect all friends" {
                        selectFriends.removeAll()
                        for number in userListViewModel.userProfiles {
                            if !ride.riders.contains(number.id) {
                                selectFriends.append(number.id)
                            }
                        }
                        slctedDeslctedFriendTittle = "Deselect all friends"
                    }else{
                        for member in userListViewModel.userProfiles {
                            self.selectFollowers.removeAll(where: { $0 == member.id })
                            
                            self.selectFriends.removeAll(where: { $0 == member.id })
                            
                            self.selectGroupMembers.removeAll(where: { $0 == member.id })

                        }
                        slctedDeslctedFriendTittle = "Select all friends"
                    }
                    
                    if selectFriends.count == userListViewModel.userProfiles.count {
                        slctedDeslctedFriendTittle = "Deselect all friends"
                    }else{
                        slctedDeslctedFriendTittle = "Select all friends"
                    }
                    
                    if selectFriends.count > 0 {
                        enableDisableInviteBtn = false
                    } else if selectFriends.count <= 0 {
                        enableDisableInviteBtn = true
                    }
                }.listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
                    .onAppear {
                        let listSetOfSelectedMembers = Set(selectedMembers)
                        let findListSetOfFriends = Set(userListViewModel.userProfiles.map({$0.id}))
                        let containAllElementOfFriends = findListSetOfFriends.isSubset(of: listSetOfSelectedMembers)
                        if containAllElementOfFriends {
                            slctedDeslctedFriendTittle = "Deselect all friends"
                        } else {
                            slctedDeslctedFriendTittle = "Select all friends"
                        }
                    }
                            
                ForEach(userListViewModel.userProfiles.indices, id: \.self) { indexs in
                    InviteScreenUI(day:$day,otherUserRides:$friendFollowdRides,ride:ride,userProfiles: $userListViewModel.userProfiles[indexs], isSelected: selectedMembers.contains(userListViewModel.userProfiles[indexs].id), action: {
                        if selectedMembers.contains(userListViewModel.userProfiles[indexs].id) {
                            self.selectFriends.removeAll(where: { $0 == userListViewModel.userProfiles[indexs].id })
                            
                            self.selectGroupMembers.removeAll(where: { $0 == userListViewModel.userProfiles[indexs].id })
                            
                            self.selectFollowers.removeAll(where: { $0 == userListViewModel.userProfiles[indexs].id })
                        } else {
                            if !ride.riders.contains(userListViewModel.userProfiles[indexs].id) {
                                self.selectFriends.append(userListViewModel.userProfiles[indexs].id)
                            }
                        }
                        
                        if selectFriends.count == userListViewModel.userProfiles.count {
                            slctedDeslctedFriendTittle = "Deselect all friends"
                        }else{
                            slctedDeslctedFriendTittle = "Select all friends"
                        }
                        
                        if selectFriends.count > 0 {
                            enableDisableInviteBtn = false
                        } else if selectFriends.count <= 0 {
                            enableDisableInviteBtn = true
                        }
                    }).listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
                }
            }
        }
    }
    
    
    var returnFollowerUserLists: some View {
        Group{
            if userListViewModel.userFollowers.count > 0 {
                let selectedMembers = selectFriends + selectGroupMembers + selectFollowers

                SectionTitleViewUI(titleString: "Followers", rightTitleString: slctedDeslctedFollowersittle, countShow: userListViewModel.userFollowers.count) {
                    if slctedDeslctedFollowersittle != "Deselect all followers" {
                        for number in userListViewModel.userFollowers {
                            if !ride.riders.contains(number.id) {
                                selectFollowers.append(number.id)
                            }
                        }
                        slctedDeslctedFollowersittle = "Deselect all followers"
                    }else{
                        for member in userListViewModel.userFollowers {
                            self.selectFollowers.removeAll(where: { $0 == member.id })
                            
                            self.selectFriends.removeAll(where: { $0 == member.id })
                            
                            self.selectGroupMembers.removeAll(where: { $0 == member.id })
                        }
                        slctedDeslctedFollowersittle = "Select all followers"
                    }
                    if selectFollowers.count == userListViewModel.userFollowers.count {
                        slctedDeslctedFollowersittle = "Deselect all followers"
                    }else{
                        slctedDeslctedFollowersittle = "Select all followers"
                    }
                    
                    if selectFollowers.count > 0 {
                        enableDisableInviteBtn = false
                    } else if selectFollowers.count <= 0 {
                        enableDisableInviteBtn = true
                    }
                }.listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
                    .onAppear {
                        let listSetOfSelectedMembers = Set(selectedMembers)
                        let findListSetOfUserFollowers = Set(userListViewModel.userFollowers.map({$0.id}))
                        let containAllElementsOfFollowers = findListSetOfUserFollowers.isSubset(of: listSetOfSelectedMembers)
                        if containAllElementsOfFollowers {
                            slctedDeslctedFollowersittle = "Deselect all followers"
                        }else{
                            slctedDeslctedFollowersittle = "Select all followers"
                        }
                    }
                
                if userListViewModel.userFollowers.count > 0 {
                    ForEach(userListViewModel.userFollowers.indices, id: \.self) { indexs in
                        InviteScreenUI(day:$day,otherUserRides:$friendFollowdRides,ride:ride,userProfiles: $userListViewModel.userFollowers[indexs], isSelected: selectedMembers.contains(userListViewModel.userFollowers[indexs].id), action: {
                            if selectedMembers.contains(userListViewModel.userFollowers[indexs].id) {
                                self.selectFollowers.removeAll(where: { $0 == userListViewModel.userFollowers[indexs].id })
                                
                                self.selectFriends.removeAll(where: { $0 == userListViewModel.userFollowers[indexs].id })
                                
                                self.selectGroupMembers.removeAll(where: { $0 == userListViewModel.userFollowers[indexs].id })
                            } else {
                                if !ride.riders.contains(userListViewModel.userFollowers[indexs].id) {
                                    self.selectFollowers.append(userListViewModel.userFollowers[indexs].id)
                                }
                            }
                            
                            if selectFollowers.count == userListViewModel.userFollowers.count {
                                slctedDeslctedFollowersittle = "Deselect all followers"
                            }else{
                                slctedDeslctedFollowersittle = "Select all followers"
                            }
                            if selectFollowers.count > 0 {
                                enableDisableInviteBtn = false
                            }else if selectFollowers.count <= 0 {
                                enableDisableInviteBtn = true
                            }
                            
                        }).listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
                    }
                }
            }
        }
    }
    
    var returnGroupUserLists: some View {
        Group {
            if userListViewModel.userGroups.count > 0 {
                
                let selectedMembers = selectFriends + selectGroupMembers + selectFollowers

                ForEach(userListViewModel.userGroups.indices, id: \.self) { indexs in
                    if userListViewModel.userGroups.count > 0 {
                        
                        SectionTitleViewUIForInviteGroup(titleString: userListViewModel.userGroups[indexs].name, rightTitleString: userListViewModel.userGroups[indexs].selectDeselectTitle, countShow: userListViewModel.userGroups[indexs].members.count, isOpen: userListViewModel.userGroups[indexs].isExpanded) {
                            
                            if userListViewModel.userGroups[indexs].selectDeselectTitle != "Deselect all" {
                                for number in userListViewModel.userGroups[indexs].members {
                                    if !ride.riders.contains(number) {
                                        selectGroupMembers.append(number)
                                    }
                                }
                                userListViewModel.userGroups[indexs].selectDeselectTitle = "Deselect all"
                            }else {
                                for member in userListViewModel.userGroups[indexs].members {
                                    self.selectGroupMembers.removeAll(where: { $0 == member })
                                    
                                    self.selectFollowers.removeAll(where: { $0 == member })
                                    
                                    self.selectFriends.removeAll(where: { $0 == member })
                                }
                                if userListViewModel.userGroups[indexs].selectDeselectTitle != ""  {
                                    userListViewModel.userGroups[indexs].selectDeselectTitle = "Select all"
                                }
                            }
                            
                            
                            let listSetOfSelectedMembers = Set(selectFriends + selectGroupMembers + selectFollowers + ride.riders)
                            let findListsetOfSelecectedGoup = Set(userListViewModel.userGroups[indexs].members)
                            let containAllElementsOfCurrentGroup = findListsetOfSelecectedGoup.isSubset(of: listSetOfSelectedMembers)
                            
                            if userListViewModel.userGroups[indexs].leader == dtrData.profileID {
                                if containAllElementsOfCurrentGroup {
                                    userListViewModel.userGroups[indexs].selectDeselectTitle = "Deselect all"
                                }else{
                                    if userListViewModel.userGroups[indexs].selectDeselectTitle != ""  {
                                        userListViewModel.userGroups[indexs].selectDeselectTitle = "Select all"
                                    }
                                }
                            }
                            
                            if selectGroupMembers.count > 0 {
                                enableDisableInviteBtn = false
                            } else if selectGroupMembers.count <= 0 {
                                enableDisableInviteBtn = true
                            }
                        }.listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
                            .onTapGesture(perform: {
                                if userListViewModel.userGroups[indexs].isExpanded {
                                    userListViewModel.userGroups[indexs].isExpanded.toggle()
                                } else {
                                    for i in 0...(userListViewModel.userGroups.count - 1) {
                                        userListViewModel.userGroups[i].isExpanded = false
                                    }
                                    userListViewModel.userGroups[indexs].isExpanded.toggle()
                                }
                            })
                        
                        if userListViewModel.userGroups[indexs].members.count > 0 && userListViewModel.userGroups[indexs].isExpanded {
                            ForEach(userListViewModel.userGroups[indexs].members.indices, id: \.self) { indexes in
                                if userListViewModel.userGroups.count > 0 {
                                    
                                    InviteScreenUI(day:$day,otherUserRides:$friendFollowdRides,ride:ride,userProfiles: .constant(dtrData.profileDirectory[dynamicMember: userListViewModel.userGroups[indexs].members[indexes]]), isSelected: selectedMembers.contains(self.userListViewModel.userGroups[indexs].members[indexes]), action: {
                                        
                                        if selectedMembers.contains(userListViewModel.userGroups[indexs].members[indexes]) {
                                            self.selectGroupMembers.removeAll(where: { $0 == userListViewModel.userGroups[indexs].members[indexes] })
                                            
                                            self.selectFollowers.removeAll(where: { $0 == userListViewModel.userGroups[indexs].members[indexes] })
                                            
                                            self.selectFriends.removeAll(where: { $0 == userListViewModel.userGroups[indexs].members[indexes] })
                                            
                                        } else {
                                            if !ride.riders.contains(userListViewModel.userGroups[indexs].members[indexes]) {
                                                self.selectGroupMembers.append(userListViewModel.userGroups[indexs].members[indexes])
                                            }
                                        }
                                        
                                        
                                        let listSetOfSelectedMembers = Set(selectFriends + selectGroupMembers + selectFollowers + ride.riders)
                                        let findListsetOfSelecectedGoup = Set(userListViewModel.userGroups[indexs].members)
                                        let containAllElementsOfCurrentGroup = findListsetOfSelecectedGoup.isSubset(of: listSetOfSelectedMembers)
                                        
                                        if userListViewModel.userGroups[indexs].leader == dtrData.profileID {
                                            if containAllElementsOfCurrentGroup {
                                                userListViewModel.userGroups[indexs].selectDeselectTitle = "Deselect all"
                                            }else{
                                                if userListViewModel.userGroups[indexs].selectDeselectTitle != ""  {
                                                    userListViewModel.userGroups[indexs].selectDeselectTitle = "Select all"
                                                }
                                            }
                                        }
                                        if selectGroupMembers.count > 0 {
                                            enableDisableInviteBtn = false
                                        }else if selectGroupMembers.count <= 0 {
                                            enableDisableInviteBtn = true
                                        }
                                    }).listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func row(for idx: Int) -> some View {
        let isOn = Binding(
            get: {
                idx < self.userProfiles.count ? self.userProfiles[idx] : DtrProfile.default
            },
            set: {
                let isIndexValid = self.userProfiles.indices.contains(idx)
                if isIndexValid {
                    self.userProfiles[idx] = $0
                }
            }
        )
        
        return UserSearchView(day:$day,userProfiles:isOn, UserMutualStatusModel:$listOfGrouoc,showProfileScreen:.constant(false)).listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0)).listRowBackground(Color.clear).onAppear {
            if ((userProfiles.endIndex - 1) == idx) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    if userProfiles.count != totalUserCount && userProfiles.count < totalUserCount{
                        if titleDtr != "" {
                            page += 1
                            userListViewModel.SearchInviteFriends(isForType: selectedTabType, searchKeyboard: titleDtr, data: ["page": "\(page)","hitsPerPage":20], completion: { response,totalCount,profileIds  in
                                for i in 0..<response.count {
                                    userProfiles.append(response[i])
                                }
                            })
                        }
                    }
                }
            }
        }.listRowBackground(Color.clear)
    }
    
    func getSearchListForFirstTime() {
        updateKeyboard = true
        if titleDtr.count >= 2 {
            page = 0
            showActivityIndicator = true
            showListOrNot = true
            userProfiles.removeAll()
            userListViewModel.SearchInviteFriends(isForType: selectedTabType ,searchKeyboard: titleDtr, data: ["page": "0","hitsPerPage":20], completion: { response,totalCount,profileIds in
                showActivityIndicator = false
                userProfiles = response
                totalUserCount = totalCount
            })
        }else if titleDtr.count <= 2 {
            DispatchQueue.main.async {
                page = 0
                showActivityIndicator = false
                showListOrNot = false
                userProfiles.removeAll()
            }
        }
    }
    
    private func getUserTypeView() -> AnyView {
        if isShowSearchbar && titleDtr != "" {
            return AnyView(returnSearchUserList)
        } else {
            switch selectedTabType {
            case .friend:
                return AnyView(returnFriendsUserLists)
            case .follower:
                return AnyView(returnFollowerUserLists)
            case .groupMember:
                return AnyView(returnGroupUserLists)
            }
        }
    }
    
}
