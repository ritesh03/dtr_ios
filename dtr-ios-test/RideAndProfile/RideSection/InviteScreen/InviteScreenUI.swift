//
//  InviteScreenUI.swift
//  dtr-ios-test
//
//  Created by Mobile on 15/03/21.
//

import SwiftUI

struct InviteScreenUI: View {
    @Binding var day : Int
    @Binding var otherUserRides : [DtrRide]
    @State var ride: DtrRide
    @Binding var userProfiles : DtrProfile
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var showProfileScreen = false
    @State var isForGroupInvite = false
    @State var alreadyAddedMembers = [""]
    var isSelected: Bool =  false
    var action: () -> Void
    
    var body: some View {
        VStack {
            HStack(alignment:.top) {
                ProfileHead(profile: userProfiles, size: .medium)
                    .onTapGesture {
                        showProfileScreen = true
                    }
                VStack(alignment: .leading, spacing: 10){
                    if userProfiles.name != "" {
                        Text(userProfiles.name)
                            .foregroundColor(.white)
                    }
                    
                    if isForGroupInvite && alreadyAddedMembers.contains(userProfiles.id) {
                        Text("Already a member").font(.system(size: 15, weight: .bold, design: .default))
                            .lineLimit(1)
                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))

                    } else if ride.riders.contains(userProfiles.id) {
                        Text("Already on this ride").font(.system(size: 15, weight: .bold, design: .default))
                            .lineLimit(1)
                            .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                    } else {
                        Text(getInviteUserRideStatus(userId: userProfiles.id))
                            .lineLimit(1)
                            .foregroundColor(.white)
                    }
                }.padding(.leading)
                Spacer()
                HStack {
                    Spacer()
                    if !ride.riders.contains(userProfiles.id) && !(isForGroupInvite && alreadyAddedMembers.contains(userProfiles.id)) {
                        if getAlreadySentInvite(userId: userProfiles.id) != "Already invited" {
                            Button(action: self.action) {
                                Image(isSelected ? "DTR-Vector" : "DTR-EllipseRound")
                                    .resizable()
                                    .frame(width: 20, height: 20,alignment: .center)
                                    .cornerRadius(10)
                            }.buttonStyle(PlainButtonStyle())
                        }
                    }
                }
                .frame(maxWidth: 20 ,alignment: .topLeading)
                .padding(.horizontal)
            }
            .padding(.all)
            NavigationLink(destination: ProfileScreen(updatedDismissView:.constant(false),day: .constant(0),userProfile: $userProfiles,showBackButtonOrNot:true),isActive:$showProfileScreen){
                EmptyView()
            }
        }.onAppear {
            showProfileScreen = false
        }
    }
    
    
    func getInviteUserRideStatus(userId:String) -> String {
        var status = ""
        let filteredUserRideInvite = otherUserRides.filter { $0.riders.contains(userId)}
        if filteredUserRideInvite.count > 0 {
            let riderCount = filteredUserRideInvite[0].riders.count
            if riderCount > 0 {
                status = filteredUserRideInvite[0].summary + " on \(dayName(dateCode:dateCode(offset: self.day)))"
            }
        }
        return status
    }
    
    
    func getAlreadySentInvite(userId:String) -> String {
        var status = ""
        let filteredSentRideInvite = self.dtrData.sentNotifications.filter { $0.payloadID == "\(dtrData.profileID)+\(dateCode(offset: self.day))"}
        if filteredSentRideInvite.count > 0 {
            let userSentRideInvite = filteredSentRideInvite.filter { $0.target == userId}
            if userSentRideInvite.count > 0 {
                status = "Already invited"
            }
        }
        return status
    }
}
