//
//  UserFollowerInvite.swift
//  dtr-ios-test
//
//  Created by Mobile on 15/03/21.
//

import SwiftUI

struct UserFollowerInvite: View {
    @StateObject var userListViewModel = UserListViewModel()
    @Environment(\.presentationMode) var presentationMode
    @Binding var selectFollowers: [String]
    @State private var allSlctedFollowersittle = "Select all followers"
    
    
    @Binding  var enableDisableInviteBtn:Bool
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var ride: DtrRide
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack {
                Group {
                    if userListViewModel.userFollowers.count > 0 {
                        SectionTitleUI(titleStrng: "Followers", rightTitleString: allSlctedFollowersittle, countShow: userListViewModel.userFollowers.count) {
                            if allSlctedFollowersittle != "Deselect all followers" {
                                for number in userListViewModel.userFollowers {
                                    if !ride.riders.contains(number.id) {
                                        selectFollowers.append(number.id)
                                    }
                                }
                                allSlctedFollowersittle = "Deselect all followers"
                            }else{
                                for _ in userListViewModel.userFollowers {
                                    selectFollowers.removeAll()
                                }
                                allSlctedFollowersittle = "Select all followers"
                            }
                            if selectFollowers.count == userListViewModel.userFollowers.count {
                                allSlctedFollowersittle = "Deselect all followers"
                            }else{
                                allSlctedFollowersittle = "Select all followers"
                            }
                            
                            if selectFollowers.count > 0 {
                                enableDisableInviteBtn = false
                            } else if selectFollowers.count <= 0 {
                                enableDisableInviteBtn = true
                            }
                        }
                        if userListViewModel.userFollowers.count > 0 {
                            ForEach(userListViewModel.userFollowers.indices, id: \.self) { indexs in
                                InviteScreenUI(ride:ride,userProfiles: $userListViewModel.userFollowers[indexs], isSelected: self.selectFollowers.contains(userListViewModel.userFollowers[indexs].id), action: {
                                    if self.selectFollowers.contains(userListViewModel.userFollowers[indexs].id) {
                                        self.selectFollowers.removeAll(where: { $0 == userListViewModel.userFollowers[indexs].id })
                                    } else {
                                        if !ride.riders.contains(userListViewModel.userFollowers[indexs].id) {
                                            self.selectFollowers.append(userListViewModel.userFollowers[indexs].id)
                                        }
                                    }
                                    
                                    if selectFollowers.count == userListViewModel.userFollowers.count {
                                        allSlctedFollowersittle = "Deselect all followers"
                                    }else{
                                        allSlctedFollowersittle = "Select all followers"
                                    }
                                    if selectFollowers.count > 0 {
                                        enableDisableInviteBtn = false
                                    }else if selectFollowers.count <= 0 {
                                        enableDisableInviteBtn = true
                                    }
                                })
                                .listRowBackground(Color.clear)
                            }
                        }
                    }else{
                        //Text("No followers")
                    }
                }
            }
            .onAppear {
                selectFollowers.removeAll()
                userListViewModel.getFollowersForInvite(profile: dtrData.profile)
            }
        }
    }
}

struct UserFollowerInvite_Previews: PreviewProvider {
    static var previews: some View {
        UserFollowerInvite( selectFollowers: .constant([""]), enableDisableInviteBtn: .constant(false), ride: DtrRide.default)
    }
}
