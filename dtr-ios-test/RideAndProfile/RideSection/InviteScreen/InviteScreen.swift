//
//  InviteScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 13/03/21.
//

import SwiftUI
import Firebase

struct InviteScreen: View {
    
    @Binding  var day: Int
    @Binding  var showDeatilPopUp: Bool
    @State  var resetLocalValue = false
    @State var ride: DtrRide
    @StateObject var userListViewModel = UserListViewModel()
    @State var profile: DtrProfile
    @Environment(\.presentationMode) var presentationMode
    @State  var errorStatus: String = ""
    @State  var selectFriends: [String]
    @State private var sendPushNotiftionToInvite: [String] = [""]
    @State  var selectFollowers: [String]
    @State  var enableDisableInviteBtn:Bool = true
    @State  var opacity:Double = 1.0
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack {
                ScrollView {
                    VStack(spacing:0){
                        Group {
                            UserFriendInvite( selectFriends: $selectFriends, enableDisableInviteBtn: $enableDisableInviteBtn,ride:ride)
                            UserFollowerInvite( selectFollowers: $selectFollowers, enableDisableInviteBtn: $enableDisableInviteBtn,ride:ride)
                        }
                    }
                }
                
                if errorStatus != "" {
                    Text(errorStatus)
                        .foregroundColor(.white)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                        .padding()
                }
                
                CustomButtonCircleRS(btnTitle: "Invite", btnWidth: UIScreen.screenWidth-100, btnHeight: 10, backgroundColord: true, txtColorAccent: false, strokeColor: false) {
                    errorStatus = "Sending Invites"
                    enableDisableInviteBtn = true
                    opacity = 0.5
                    let recpitIds = selectFriends.filter { $0 != "" } + selectFollowers.filter { $0 != "" }
                    let mesg = "😎 \(dtrData.profile.name) has invited you to \(self.ride.summary) on \(dayName(dateCode: self.ride.dateCode)) "
                    sendPushNotiftionToInvite.removeAll()
                    sendPushNotiftionToInvite = recpitIds
                    if sendPushNotiftionToInvite.count > 0 {
                        sendPushNotification(riderSendPush: sendPushNotiftionToInvite.last!, textMesg: mesg)
                    }
                    
                    DtrCommand.sharedInstance.sendRideInvitation(profileID: self.ride.id, data: ["message":mesg,"recipients":Array(Set(recpitIds))]) { (result) in
                        if result.result == .ok {
                            errorStatus = ""
                            showDeatilPopUp = true
                            removeLocalDataAndPopClas()
                        } else {
                            if result.feedback.contains("already on") {
                                errorStatus = "All Selected Recipients of Ride Invites are already on the ride."
                            }else{
                                errorStatus = result.feedback
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
                                errorStatus = ""
                            }
                        }
                    }
                }.disabled(enableDisableInviteBtn)
                .padding(.bottom)
                .opacity(opacity)
            }.padding(.top,10)
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,txtTitle: "Invite friends", systemImgName: ImageConstantsName.ArrowLeftImg, action: {
                removeLocalDataAndPopClas()
            })))
            
            .onAppear {
                Analytics.logEvent(AnalyticsScreen.rideAddRider.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
                if resetLocalValue {
                    UserStore.shared.isLocallSelectFriends.removeAll()
                    UserStore.shared.isLocallSelectFollowers.removeAll()
                }
                
                if UserStore.shared.isLocallSelectFriends.count > 0 {
                    DispatchQueue.main.async {
                        selectFriends = UserStore.shared.isLocallSelectFriends
                        UserStore.shared.isLocallSelectFriends.removeAll()
                    }
                }
                
                if UserStore.shared.isLocallSelectFollowers.count > 0 {
                    DispatchQueue.main.async {
                        selectFollowers = UserStore.shared.isLocallSelectFollowers
                        UserStore.shared.isLocallSelectFollowers.removeAll()
                    }
                }
            }
            
            .onDisappear{
                if selectFriends.count > 0 {
                    UserStore.shared.isLocallSelectFriends = selectFriends
                }
                
                if selectFollowers.count > 0 {
                    UserStore.shared.isLocallSelectFollowers = selectFollowers
                }
                resetLocalValue = false
            }
        }
    }
    
    func removeLocalDataAndPopClas() {
        selectFollowers.removeAll()
        selectFriends.removeAll()
        UserStore.shared.isLocallSelectFriends.removeAll()
        UserStore.shared.isLocallSelectFollowers.removeAll()
        presentationMode.wrappedValue.dismiss()
    }
    
    func sendPushNotification(riderSendPush:String,textMesg:String){
        sendPushNotiftionToInvite.removeLast()
        if sendPushNotiftionToInvite.count > 0 {
            sendPushNotification(riderSendPush: sendPushNotiftionToInvite.last!, textMesg: textMesg)
        }
    }
}
