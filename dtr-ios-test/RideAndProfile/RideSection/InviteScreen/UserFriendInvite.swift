//
//  UserFriendInvite.swift
//  dtr-ios-test
//
//  Created by Mobile on 15/03/21.
//

import SwiftUI

struct UserFriendInvite: View {
    @StateObject var userListViewModel = UserListViewModel()
    @Environment(\.presentationMode) var presentationMode
    @Binding var selectFriends: [String]
    @State private var allSlctedFriendTittle = "Select all friends"
    @Binding  var enableDisableInviteBtn:Bool 
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State var ride: DtrRide
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack{
                Group {
                    if userListViewModel.userProfiles.count > 0 {
                        SectionTitleUI(titleStrng: "Friends", rightTitleString: allSlctedFriendTittle, countShow: userListViewModel.userProfiles.count) {
                            if allSlctedFriendTittle != "Deselect all friends" {
                                selectFriends.removeAll()
                                for number in userListViewModel.userProfiles {
                                    if !ride.riders.contains(number.id) {
                                        selectFriends.append(number.id)
                                    }
                                }
                                allSlctedFriendTittle = "Deselect all friends"
                            }else{
                                for _ in userListViewModel.userProfiles {
                                    selectFriends.removeAll()
                                }
                                allSlctedFriendTittle = "Select all friends"
                            }
                            
                            if selectFriends.count == userListViewModel.userProfiles.count {
                                allSlctedFriendTittle = "Deselect all friends"
                            }else{
                                allSlctedFriendTittle = "Select all friends"
                            }
                            
                            if selectFriends.count > 0 {
                                enableDisableInviteBtn = false
                            } else if selectFriends.count <= 0 {
                                enableDisableInviteBtn = true
                            }
                        }
                        
                        if userListViewModel.userProfiles.count > 0 {
                            ForEach(userListViewModel.userProfiles.indices, id: \.self) { indexs in
                                InviteScreenUI(ride:ride,userProfiles: $userListViewModel.userProfiles[indexs], isSelected: self.selectFriends.contains(userListViewModel.userProfiles[indexs].id), action: {
                                    
                                    if self.selectFriends.contains(userListViewModel.userProfiles[indexs].id) {
                                        self.selectFriends.removeAll(where: { $0 == userListViewModel.userProfiles[indexs].id })
                                    } else {
                                        if !ride.riders.contains(userListViewModel.userProfiles[indexs].id) {
                                            self.selectFriends.append(userListViewModel.userProfiles[indexs].id)
                                        }
                                    }
                                    
                                    if selectFriends.count == userListViewModel.userProfiles.count {
                                        allSlctedFriendTittle = "Deselect all friends"
                                    }else{
                                        allSlctedFriendTittle = "Select all friends"
                                    }
                                    
                                    if selectFriends.count > 0 {
                                        enableDisableInviteBtn = false
                                    } else if selectFriends.count <= 0 {
                                        enableDisableInviteBtn = true
                                    }
                                })
                                .listRowBackground(Color.clear)
                            }
                        }
                    }else{
                        Text("No friends")
                    }
                }
            }
            .onAppear {
                selectFriends.removeAll()
                userListViewModel.getFriendsForInvite(profile: dtrData.profile)
            }
        }
    }
    func checkStatus(){
        
    }
}

struct UserFriendInvite_Previews: PreviewProvider {
    static var previews: some View {
        UserFriendInvite(selectFriends: .constant([""]), enableDisableInviteBtn: .constant(false), ride: DtrRide.default)
    }
}
