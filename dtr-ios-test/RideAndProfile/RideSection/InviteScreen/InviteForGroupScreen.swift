//
//  InviteForGroupScreen.swift
//  dtr-ios-test
//
//  Created by Vaibhav Thakkar on 27/12/21.
//

import SwiftUI
import Firebase

struct InviteForGroupScreen: View {
        @Binding var ride: DtrRide
        @State var alreadyAddedMembers: [String]
        @State var day: Int = 0
        @State var profile: DtrProfile = DtrProfile()
        @State var opacity:Double = 1.0
        @State var resetLocalValue = false
        @State var selectFriends: [String] = [""]
        @State var errorStatus: String = ""
        @State var selectFollowings: [String] = [""]
        @State var enableDisableInviteBtn:Bool = true
        @State var sendPushNotiftionToInvite: [String] = [""]
        @State var groupID: String
        @State var groupName: String
        @State private var friendFollowdRides = [DtrRide]()
        @State var alreadyCalled = false
        
        @State private var isShowSearchbar = false
        @State private var clearAllText: Bool = false
        @State private var showHomeScreen = false
        @State private var isFirstResponder = true
        @State private var titleDtr: String = ""
        
        @State private var slctedDeslctedFriendTittle = "Select all friends"
        @State private var slctedDeslctedFollowingsittle = "Select all following"
        @State var selectedTabType: UserType = .friend
        @StateObject var userListViewModel = UserListViewModel()
        @ObservedObject var dtrData = DtrData.sharedInstance
        @ObservedObject var dtrCommand = DtrCommand.sharedInstance
        
        @Environment(\.presentationMode) var presentationMode
        
        var body: some View {
            ZStack {
                Color.init(ColorConstantsName.MainThemeBgColour)
                    .ignoresSafeArea()
                VStack(spacing:0) {
                    if isShowSearchbar {
                        CustomTxtfldForFollowerScrn(isDeleteAcntScreen: .constant(true),isGroupListScreen: .constant(false), text:  $titleDtr, clearAllText: $clearAllText, isFirstResponder: $isFirstResponder, completion: { (reponse) in
                            print("response")
                        })
                            .padding()
                            .background(Color.init(ColorConstantsName.SectionBgColour))
                            .cornerRadius(10)
                            .foregroundColor(.white)
                            .padding(.horizontal, 6)
                            .frame(height: 40)
                            .padding(15)
                    }
                    HStack {
                        VStack {
                            Button {
                                selectedTabType = .friend
                            } label: {
                                Text("FRIENDS")
                                    .foregroundColor(selectedTabType == .friend ? Color.init(ColorConstantsName.LimeColour) : Color.init(UIColor.init(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.32)))
                                    .font(Font.system(size: 14, weight: .semibold))
                            }
                            selectedTabType == .friend ?
                            Color.init(ColorConstantsName.LimeColour).frame(height:2) : Color.clear.frame(height:2)
                        }
                        Spacer()
                        VStack {
                            Button {
                                selectedTabType = .follower
                            } label: {
                                Text("FOLLOWING")
                                    .foregroundColor(selectedTabType == .follower ? Color.init(ColorConstantsName.LimeColour) : Color.init(UIColor.init(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.32)))
                                    .font(Font.system(size: 14, weight: .semibold))
                                
                            }
                            selectedTabType == .follower ?
                            Color.init(ColorConstantsName.LimeColour).frame(height:2) : Color.clear.frame(height:2)
                        }
                    }
                    .padding(.top,25)
                    .padding(.bottom,-10)
                    VStack (alignment:.leading,spacing:0) {
                        List {
                            getUserTypeView()
                        }.listStyle(PlainListStyle())
                    }.padding(.top,10)
                    
                    if errorStatus != "" {
                        Text(errorStatus)
                            .foregroundColor(.white)
                            .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                            .padding()
                    }
                    VStack (alignment:.center,spacing:0) {
                        CustomButtonCircleWithCustomProperty(btnTitle: "Invite", btnWidth: UIScreen.screenWidth-100, btnHeight: 10, backgroundColord: true, txtColorAccent: false, strokeColor: false) {
                            errorStatus = "Sending Invites"
                            enableDisableInviteBtn = true
                            opacity = 0.5
                            let recpitIds = selectFriends.filter { $0 != "" } + selectFollowings.filter { $0 != "" }
                            
                                let mesg = "\(dtrData.profile.name) is requesting you to join \(groupName). Down to join the group?"

                                DtrCommand.sharedInstance.sendGroupInvitation(profileID: profile.id, groupId: groupID, data: ["message":mesg, "recipients":Array(Set(recpitIds))]) { (result) in
                                    if result.result == .ok {
                                        errorStatus = ""
                                        removeLocalDataAndPopClas()
                                    } else {
                                        opacity = 1.0
                                        enableDisableInviteBtn = false
                                        if result.feedback.contains("already on") {
                                            errorStatus = "All Selected Recipients of Group Invites are already on the Group."
                                        }else{
                                            errorStatus = result.feedback
                                        }
                                        DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
                                            errorStatus = ""
                                        }
                                    }
                                }
                                
                                
                                print("Invite for group", Array(Set(recpitIds)))
                            
                        }.disabled(enableDisableInviteBtn)
                        .padding(.bottom)
                        .opacity(opacity)
                    }
                }
                .navigationBarBackButtonHidden(true)
                .navigationBarTitle("", displayMode: .inline)
                .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,textTitle: "Invite", systemImageName: ImageConstantsName.ArrowLeftImg, action: {
                    removeLocalDataAndPopClas()
                })).font(Font.system(size: 20, weight: .semibold)))
                
            }.onAppear {
                selectFriends.removeAll()
                userListViewModel.getFriendsForInvite(profile: dtrData.profile)
                
                selectFollowings.removeAll()
                userListViewModel.getFollowingsForInvite(profile: dtrData.profile)
                                
                Analytics.logEvent(AnalyticsObjectEvent.invite_friend_click.rawValue, parameters: nil)
                Analytics.logEvent(AnalyticsScreen.rideAddRider.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
                
                if resetLocalValue {
                    UserStore.shared.isLocallSelectFriends.removeAll()
                    UserStore.shared.isLocallSelectFollowers.removeAll()
                    UserStore.shared.isLocallSelectGroupMembers.removeAll()
                }
                
                if UserStore.shared.isLocallSelectFriends.count > 0 {
                    DispatchQueue.main.async {
                        selectFriends = UserStore.shared.isLocallSelectFriends
                        UserStore.shared.isLocallSelectFriends.removeAll()
                    }
                }
                
                if UserStore.shared.isLocallSelectFollowers.count > 0 {
                    DispatchQueue.main.async {
                        selectFollowings = UserStore.shared.isLocallSelectFollowers
                        UserStore.shared.isLocallSelectFollowers.removeAll()
                    }
                }
                
                getAllRidesForFriendsAndFollowers()
            }
            
            .onDisappear{
                if selectFriends.count > 0 {
                    UserStore.shared.isLocallSelectFriends = selectFriends
                }
                
                if selectFollowings.count > 0 {
                    UserStore.shared.isLocallSelectFollowers = selectFollowings
                }
                
                
                resetLocalValue = false
            }
        }
        
        func getAllRidesForFriendsAndFollowers(){
            let groupUserIds = userListViewModel.userGroups.flatMap({$0.members})
            let userIds = Set(dtrData.profile.friends+dtrData.profile.followed+groupUserIds)
            if !alreadyCalled {
                alreadyCalled = true
                friendFollowdRides.removeAll()
                for document in userIds {
                    dtrCommand.getRideInvite(dateCode: dateCode(offset: self.day), userID: document) { response in
                        if let rideDetail =  response.output["ride"] as? DataDict {
                            let dictUsr = DtrRide(data:rideDetail)
                            if dictUsr.id != "+" {
                                friendFollowdRides.append(dictUsr)
                                alreadyCalled = false
                            }
                        }
                    }
                }
            }
        }
        
        
        func removeLocalDataAndPopClas() {
            selectFollowings.removeAll()
            selectFriends.removeAll()
            UserStore.shared.isLocallSelectFriends.removeAll()
            UserStore.shared.isLocallSelectFollowers.removeAll()
            UserStore.shared.isLocallSelectGroupMembers.removeAll()
            presentationMode.wrappedValue.dismiss()
        }
        
    var returnFriendsUserLists: some View {
        Group{
            if userListViewModel.userProfiles.count > 0 {
                let selectedMembers = selectFriends + selectFollowings
                SectionTitleViewUI(titleString: "Friends", rightTitleString: "", countShow: userListViewModel.userProfiles.count) {
                    if slctedDeslctedFriendTittle != "Deselect all friends" {
                        selectFriends.removeAll()
                        for number in userListViewModel.userProfiles {
                            if !ride.riders.contains(number.id) {
                                selectFriends.append(number.id)
                            }
                        }
                        slctedDeslctedFriendTittle = "Deselect all friends"
                    }else{
                        for member in userListViewModel.userProfiles {
                            self.selectFollowings.removeAll(where: { $0 == member.id })
                            
                            self.selectFriends.removeAll(where: { $0 == member.id })
                            
                        }
                        slctedDeslctedFriendTittle = "Select all friends"
                    }
                    
                    if selectFriends.count == userListViewModel.userProfiles.count {
                        slctedDeslctedFriendTittle = "Deselect all friends"
                    }else{
                        slctedDeslctedFriendTittle = "Select all friends"
                    }
                    
                    if selectFriends.count > 0 {
                        enableDisableInviteBtn = false
                    } else if selectFriends.count <= 0 {
                        enableDisableInviteBtn = true
                    }
                }.listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
                    .onAppear {
                        let listSetOfSelectedMembers = Set(selectedMembers)
                        let findListSetOfFriends = Set(userListViewModel.userProfiles.map({$0.id}))
                        let containAllElementOfFriends = findListSetOfFriends.isSubset(of: listSetOfSelectedMembers)
                        if containAllElementOfFriends {
                            slctedDeslctedFriendTittle = "Deselect all friends"
                        } else {
                            slctedDeslctedFriendTittle = "Select all friends"
                        }
                    }
                            
                ForEach(userListViewModel.userProfiles.indices, id: \.self) { indexs in
                    InviteScreenUI(day:$day,otherUserRides:$friendFollowdRides,ride:ride,userProfiles: $userListViewModel.userProfiles[indexs], isForGroupInvite: true, alreadyAddedMembers: alreadyAddedMembers, isSelected: selectedMembers.contains(userListViewModel.userProfiles[indexs].id), action: {
                        if selectedMembers.contains(userListViewModel.userProfiles[indexs].id) {
                            self.selectFriends.removeAll(where: { $0 == userListViewModel.userProfiles[indexs].id })
                                                        
                            self.selectFollowings.removeAll(where: { $0 == userListViewModel.userProfiles[indexs].id })
                        } else {
                            if !ride.riders.contains(userListViewModel.userProfiles[indexs].id) {
                                self.selectFriends.append(userListViewModel.userProfiles[indexs].id)
                            }
                        }
                        
                        if selectFriends.count == userListViewModel.userProfiles.count {
                            slctedDeslctedFriendTittle = "Deselect all friends"
                        }else{
                            slctedDeslctedFriendTittle = "Select all friends"
                        }
                        
                        if selectFriends.count > 0 {
                            enableDisableInviteBtn = false
                        } else if selectFriends.count <= 0 {
                            enableDisableInviteBtn = true
                        }
                    }).listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
                }
            }
        }
    }
    
    
    var returnFollowerUserLists: some View {
        Group{
            if userListViewModel.userFollowings.count > 0 {
                let selectedMembers = selectFriends + selectFollowings

                SectionTitleViewUI(titleString: "Following", rightTitleString: "", countShow: userListViewModel.userFollowings.count) {
                    if slctedDeslctedFollowingsittle != "Deselect all following" {
                        for number in userListViewModel.userFollowings {
                            if !ride.riders.contains(number.id) {
                                selectFollowings.append(number.id)
                            }
                        }
                        slctedDeslctedFollowingsittle = "Deselect all following"
                    }else{
                        for member in userListViewModel.userFollowings {
                            self.selectFollowings.removeAll(where: { $0 == member.id })
                            
                            self.selectFriends.removeAll(where: { $0 == member.id })
                            
                        }
                        slctedDeslctedFollowingsittle = "Select all following"
                    }
                    if selectFollowings.count == userListViewModel.userFollowings.count {
                        slctedDeslctedFollowingsittle = "Deselect all following"
                    }else{
                        slctedDeslctedFollowingsittle = "Select all following"
                    }
                    
                    if selectFollowings.count > 0 {
                        enableDisableInviteBtn = false
                    } else if selectFollowings.count <= 0 {
                        enableDisableInviteBtn = true
                    }
                }.listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
                    .onAppear {
                        let listSetOfSelectedMembers = Set(selectedMembers)
                        let findListSetOfUserFollowers = Set(userListViewModel.userFollowings.map({$0.id}))
                        let containAllElementsOfFollowers = findListSetOfUserFollowers.isSubset(of: listSetOfSelectedMembers)
                        if containAllElementsOfFollowers {
                            slctedDeslctedFollowingsittle = "Deselect all followers"
                        }else{
                            slctedDeslctedFollowingsittle = "Select all followers"
                        }
                    }
                
                if userListViewModel.userFollowings.count > 0 {
                    ForEach(userListViewModel.userFollowings.indices, id: \.self) { indexs in
                        InviteScreenUI(day:$day,otherUserRides:$friendFollowdRides,ride:ride,userProfiles: $userListViewModel.userFollowings[indexs], isForGroupInvite: true, alreadyAddedMembers: alreadyAddedMembers, isSelected: selectedMembers.contains(userListViewModel.userFollowings[indexs].id), action: {
                            if selectedMembers.contains(userListViewModel.userFollowings[indexs].id) {
                                self.selectFollowings.removeAll(where: { $0 == userListViewModel.userFollowings[indexs].id })
                                
                                self.selectFriends.removeAll(where: { $0 == userListViewModel.userFollowings[indexs].id })
                                
                            } else {
                                if !ride.riders.contains(userListViewModel.userFollowings[indexs].id) {
                                    self.selectFollowings.append(userListViewModel.userFollowings[indexs].id)
                                }
                            }
                            
                            if selectFollowings.count == userListViewModel.userFollowings.count {
                                slctedDeslctedFollowingsittle = "Deselect all followers"
                            }else{
                                slctedDeslctedFollowingsittle = "Select all followers"
                            }
                            if selectFollowings.count > 0 {
                                enableDisableInviteBtn = false
                            }else if selectFollowings.count <= 0 {
                                enableDisableInviteBtn = true
                            }
                            
                        }).listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
                    }
                }
            }
        }
    }
        
        
        private func getUserTypeView() -> AnyView {
            switch selectedTabType {
            case .friend:
            return AnyView(returnFriendsUserLists)
            case .follower:
                return AnyView(returnFollowerUserLists)
            case .groupMember:
                return AnyView(EmptyView())
            }
        }
}

struct InviteForGroupScreen_Previews: PreviewProvider {
    static var previews: some View {
        InviteForGroupScreen(ride: .constant(DtrRide(data: DataDict())), alreadyAddedMembers: [""], groupID: "", groupName: "")
    }
}
