//
//  ShareScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 15/03/21.
//

import SwiftUI
import Firebase

struct ShareScreen: View {
    @Binding var isPresented: Bool
    var ride: DtrRide
    
    @EnvironmentObject var session: DtrData
    @State var showRider = false
    @State var creatingLinks = false
    @State private var links = [Any]()
    
    var displayedDateString: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMMM d"
        let date = toDate(timeString: ride.details.startTime, on: ride.dateCode)
        return formatter.string(from: date)
    }
    
    
    func constructLinks() {
        self.creatingLinks = true
        guard let link = URL(string: "https://socialactive.app/ride/\(self.session.profile.id)/\(self.ride.id)") else {
            print("ERROR constructing initial URL")
            return
        }
        let dynamicLinksDomainURIPrefix = "https://dtr.lol"
        let appBundleID = "app.socialactive.dtr"
        if let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix) {
            linkBuilder.navigationInfoParameters = DynamicLinkNavigationInfoParameters()
            linkBuilder.navigationInfoParameters?.isForcedRedirectEnabled = true
            linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: appBundleID)
            linkBuilder.iOSParameters?.appStoreID = "1477781219"
            linkBuilder.iOSParameters?.minimumAppVersion = "0.1"
            linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: appBundleID)
            linkBuilder.androidParameters?.minimumVersion = 0
            guard let longDynamicLink = linkBuilder.url else {
                print("ERROR in linkBuilder")
                return
            }
            DynamicLinkComponents.shortenURL(longDynamicLink, options: nil) { url, warnings, error in
                if let error = error {
                    print("error shortening link: \(error.localizedDescription)")
                    return
                }
                if let url = url {
                    let itemSource = ShareActivityItemSource(type: .ride, message: self.ride.inviteMessage, url: url)
                    self.links = [url, itemSource]
                    self.creatingLinks = false
                    return
                }
            }
        }
    }
    
    var body: some View {
        VStack {
            HStack {
                VStack(alignment: .leading) {
                    Text(ride.summary).font(.title).fontWeight(.bold)
                    Text(displayedDateString).font(.caption)
                }
                Spacer()
            }
            .padding()
            HStack {
                Text("Share this link with your friends to have them join you.")
            }
            .padding()
            
            if self.creatingLinks {
                ActivityIndicator(isAnimating: self.$creatingLinks, style: .large)
            } else {
                ShareSheet(activityItems: self.links) { activityType, finished, returnedItems, error in
                   if let error = error {
                       print("share sheet error: \(error.localizedDescription)")
                       return
                   }
                   
                   if finished {
                       let params: DataDict = [
                           "content_type": activityType?.rawValue ?? "unknown",
                           "item_type": "rideLink",
                           "item_id": self.ride.id
                       ]
                    print(params)
                   }
               }
            }
            Spacer()
        }
        .onAppear {
            self.constructLinks()
        }
    }
}

struct ShareScreen_Previews: PreviewProvider {
    static var previews: some View {
        ShareScreen(isPresented: .constant(false), ride: DtrRide.default)
    }
}
