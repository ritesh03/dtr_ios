//
//  NotificationsScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 02/03/21.
//

import SwiftUI
import Firebase


struct NotificationsScreen: View {

    @Binding var day:Int
    @Environment(\.presentationMode) var presentationMode
    @State var notifications: [AppNotification] = []
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var rideInviteArray :  [AppNotification] = []
    @State private var rideUpdateArray :  [AppNotification] = []
    @State private var rideLocationArray :  [AppNotification] = []
    @State private var rideTimeArray :  [AppNotification] = []
    @State private var rideCancelledArray :  [AppNotification] = []
    @State private var showActivity = false
    @State private var pressBckBtn = false
    @State private var fwdButton = false
    @State var comesFromSheet = false
    @State var showOnlyMainNotification = ""
    
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.init(MAIN_THEME_BG_COLOUR)
                    .ignoresSafeArea()
                VStack(spacing:0) {
                    List {
                        AnnouncementScreen(showActivity:$showActivity).listRowInsets(EdgeInsets())
                        FriendInviteScreen(showActivity:$showActivity).listRowInsets(EdgeInsets())
                        RideUpdateNotificationScreen(comesFromSheet:comesFromSheet,day:$day, showActivity: $showActivity, rideTimeArray: $rideTimeArray, rideInviteArray: $rideInviteArray, rideUpdateArray: $rideUpdateArray, rideLocationArray: $rideLocationArray, rideCancelledArray: $rideCancelledArray).listRowInsets(EdgeInsets())
                        FriendMessageScreen(day:$day,showActivity:$showActivity).listRowInsets(EdgeInsets())
                    }
                    .padding(.top,10)
                }
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true,isSystemImage: true, isText:true, txtTitle:"Notifications", systemImgName: "arrow.left", action: {
                pressBckBtn = true
                getAllTypesOfNotifcation()
                presentationMode.wrappedValue.dismiss()
            })),trailing:AnyView(trailingButton))
        }
        
        .onAppear {
            Analytics.logEvent(AnalyticsScreen.notification.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
        }
        
        .onReceive(self.dtrData.$notifications) { getLatestNotification in
            notifications.removeAll()
            for index in 0..<getLatestNotification.count {
                let notificationData = getLatestNotification[index]
                notifications.append(notificationData)
            }
            if notifications.count == 0 && fwdButton {
                presentationMode.wrappedValue.dismiss()
            }
            fwdButton = true
            DispatchQueue.main.async {
                getAllTypesOfNotifcation()
            }
        }
    }
    
    var trailingButton: some View {
        Button(action: {
        }) {
            if self.showActivity {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                    .foregroundColor(.white)
                    .padding()
            }
        }
    }
    
    func getAllTypesOfNotifcation(){
        rideInviteArray.removeAll()
        rideUpdateArray.removeAll()
        rideLocationArray.removeAll()
        rideTimeArray.removeAll()
        rideCancelledArray.removeAll()
        
        var temprideInviteArray = [AppNotification]()
        var temprideUpdateArray = [AppNotification]()
        var temprideLocationArray = [AppNotification]()
        var temprideTimeArray = [AppNotification]()
        var temprideCancelledArray = [AppNotification]()
        
        for index in 0..<notifications.count {
            if notifications[index].type == .rideInvite {
                temprideInviteArray.append(notifications[index])
                if pressBckBtn {
                    if notifications[index].status == .unread {
                        self.dtrData.updateNotificationStatus(notification: notifications[index])
                    }
                }
            }
            
            if notifications[index].type == .rideUpdate {
                temprideUpdateArray.append(notifications[index])
                if pressBckBtn {
                    if notifications[index].status == .unread {
                        self.dtrData.updateNotificationStatus(notification: notifications[index])
                    }
                }
            }
            
            if notifications[index].type == .rideLocation{
                temprideLocationArray.append(notifications[index])
                if pressBckBtn {
                    if notifications[index].status == .unread {
                        self.dtrData.updateNotificationStatus(notification: notifications[index])
                    }
                }
            }
            
            if notifications[index].type == .rideTime{
                temprideTimeArray.append(notifications[index])
                if pressBckBtn {
                    if notifications[index].status == .unread {
                        self.dtrData.updateNotificationStatus(notification: notifications[index])
                    }
                }
            }
            
            if notifications[index].type == .rideCancelled {
                temprideCancelledArray.append(notifications[index])
                if pressBckBtn {
                    if notifications[index].status == .unread {
                        self.dtrData.updateNotificationStatus(notification: notifications[index])
                    }
                }
            }
            
            if notifications[index].type == .friendInvite {
                if pressBckBtn {
                    if notifications[index].status == .unread {
                        self.dtrData.updateNotificationStatus(notification: notifications[index])
                    }
                }
            }
            
            if notifications[index].type == .announcement{
                if pressBckBtn {
                    if notifications[index].status == .unread {
                        self.dtrData.updateNotificationStatus(notification: notifications[index])
                    }
                }
            }
            
            if notifications[index].type == .friendMessage {
                if pressBckBtn {
                    if notifications[index].status == .unread {
                        self.dtrData.updateNotificationStatus(notification: notifications[index])
                    }
                }
            }
            
            if notifications[index].type == .message{
                if pressBckBtn {
                    if notifications[index].status == .unread {
                        self.dtrData.updateNotificationStatus(notification: notifications[index])
                    }
                }
            }
            
            if notifications[index].type == .follow {
                if pressBckBtn {
                    if notifications[index].status == .unread {
                        self.dtrData.updateNotificationStatus(notification: notifications[index])
                    }
                }
            }
        }
        
        if  temprideInviteArray.count > 0 {
            rideInviteArray = temprideInviteArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
        
        if  temprideUpdateArray.count > 0 {
            rideUpdateArray = temprideUpdateArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
        
        if  temprideLocationArray.count > 0 {
            rideLocationArray = temprideLocationArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
        
        if  temprideTimeArray.count > 0 {
            rideTimeArray = temprideTimeArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
        
        if  temprideCancelledArray.count > 0 {
            rideCancelledArray = temprideCancelledArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
        pressBckBtn = false
    }
}

struct NotificationsScreen_Previews: PreviewProvider {
    static var previews: some View {
        NotificationsScreen(day: .constant(0))
    }
}

