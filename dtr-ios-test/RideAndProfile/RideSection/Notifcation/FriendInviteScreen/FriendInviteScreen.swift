//
//  FriendInviteScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 14/04/21.
//

import SwiftUI

struct FriendInviteScreen: View {
    @Binding var showActivity : Bool
    @State var friendInviteArray : [AppNotification] = []
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var arrayForDeletenotifications : [AppNotification] = []
    @State var notifications: [AppNotification] = []
    
    
    var body: some View {
        Group {
            if self.friendInviteArray.count > 0{
                SectionTitleUI(titleStrng: "FRIEND REQUESTS", rightTitleString: "", countShow:  0) {}
                ForEach(self.friendInviteArray.indices, id: \.self) { notification in
                    FriendRequestNotificationView(notificationInfo: self.friendInviteArray[notification])
                        .listRowBackground(Color.clear)
                }.onDelete(perform: deleteReceivedNotification)
            }
        }
        
        .onReceive(self.dtrData.$notifications) { getLatestNotification in
            notifications.removeAll()
            for index in 0..<getLatestNotification.count {
                let notificationData = getLatestNotification[index]
                notifications.append(notificationData)
            }
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                getAllTypesOfNotifcation()
            }
        }
    }
    
    func getAllTypesOfNotifcation(){
        friendInviteArray.removeAll()
        var tempfriendInviteArray = [AppNotification]()
        for index in 0..<notifications.count {
            if notifications[index].type == .friendInvite {
                tempfriendInviteArray.append(notifications[index])
            }
        }
        if  tempfriendInviteArray.count > 0 {
            friendInviteArray = tempfriendInviteArray.sorted(by: { $0.timestamp > $1.timestamp})
        }
    }
    
    func deleteReceivedNotification(at offsets: IndexSet) {
        showActivity = true
        for index in offsets {
            arrayForDeletenotifications.removeAll()
            arrayForDeletenotifications.append(friendInviteArray[index])
            friendInviteArray.remove(at: index)
        }
        
        if arrayForDeletenotifications.count > 0 {
            self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":arrayForDeletenotifications[0].payloadID,"source":arrayForDeletenotifications[0].source,"type":arrayForDeletenotifications[0].type.rawValue,"target":arrayForDeletenotifications[0].target]) { (result) in
                showActivity = false
            }
        }
    }
}
