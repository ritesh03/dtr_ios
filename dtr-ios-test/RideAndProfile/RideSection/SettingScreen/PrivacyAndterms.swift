//
//  PrivacyAndterms.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/03/21.
//

import SwiftUI
import Firebase

struct PrivacyAndterms: View {
    @Binding var dismissView: Bool
    var isPrivacyScreen:Bool
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Environment(\.colorScheme) var colorScheme: ColorScheme

    var body: some View {
        VStack{
            HStack{
                Button(action: {
                }) {
                    HStack {
                    }}
                Spacer()
            }
            if isPrivacyScreen {
                WebViewForSetting(filename: "privacy")
            }else{
                WebViewForSetting(filename: "terms")
            }
        }.navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarItems(leading:AnyView(NavigtionItemBar(isComeFromPrivacyTermsScreen:true,isImage: true, isSystemImage: true, isText:true,textTitle: isPrivacyScreen == true ? "Privacy policy" : "Terms of service", systemImageName: ImageConstantsName.ArrowLeftImg,textColorDiffernt:true,textColorCode: colorScheme == .light ?  ColorConstantsName.HeaderBackgroundColour : ColorConstantsName.HeaderForGrandColour, action: {
            dismissView = false
        })))
        .onAppear{
            if isPrivacyScreen {
                Analytics.logEvent(AnalyticsScreen.privacy.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
            }else{
               Analytics.logEvent(AnalyticsScreen.terms.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.modal.rawValue])
            }
        }
    }
}

struct PrivacyAndterms_Previews: PreviewProvider {
    static var previews: some View {
        PrivacyAndterms(dismissView: .constant(false), isPrivacyScreen: false)
    }
}
