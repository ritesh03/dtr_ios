//
//  SettingScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 10/03/21.
//

import SwiftUI
import MessageUI
import Firebase

struct SettingScreen: View {
    
    @Binding var dismissView : Bool
    @State var comesFromDirectHomeScreen = false
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var forSectionFirst:  [String] = ["Send feedback","Switch to Local"]
    @State var forSectionSecond: [String] = ["Default ride visibility settings","Open my device app settings"]
    @State var forSectionThird:  [String] = ["About","Privacy policy", "Terms of service","Sign out","Delete my account"]
    
    @State private var showSingoutScreen: Bool = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    
    @State var buildVersion:String = ""
    @State var buildId:String = ""
    @State var result: Result<MFMailComposeResult, Error>? = nil
    @State private var showPrivacy = false
    @State private var showRideVisiblityClass = false
    @State private var showAbouDTRScreen = false
    @State private var airplaneMode = false
    @State private var showPrivacyTermsScreen = false
    @State private var showDeleteAccountScreen = false
    
    @State var activeSheetForSetting: ActiveSheetItemForSettingScreen?    
    
    var body: some View {
        NavigationView {
            
            ZStack {
                Color.init(ColorConstantsName.MainThemeBgColour)
                    .ignoresSafeArea()
                VStack(alignment:.leading) {
                    ScrollView{
                        VStack(alignment:.leading,spacing:20){
                            SectionTitleViewUI(titleString: SettingScreenString.AccountStr, rightTitleString: CommonAllString.BlankStr, countShow: 0)
                            ForEach(0..<forSectionFirst.count){ indexs in
                                if forSectionFirst[indexs] == SettingScreenString.SendFeedBackStr {
                                    VStack(alignment:.leading,spacing:0) {
                                        Group {
                                            Text(forSectionFirst[indexs]).font(.system(size: 16)).foregroundColor(.white)
                                            Group {
                                                Text(SettingScreenString.DtrVersionStr) + Text(buildVersion) + Text(" (\(buildId))")
                                            }.font(.system(size: 13)).foregroundColor(.gray)
                                        }
                                    }
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .background(Color.init(ColorConstantsName.MainThemeBgColour))
                                    .onTapGesture {
                                        if indexs == 0 {
                                            activeSheetForSetting = .sheetMail
                                        }
                                    }
                                    .padding(.vertical,3)
                                    Divider()
                                        .foregroundColor(.white)
                                        .offset(x: 0, y:14)
                                }else{
                                    if self.dtrData.staffRole.roles.contains(SettingScreenString.StaffStr) {
                                        HStack {
                                            Text(forSectionFirst[indexs])
                                            Spacer()
                                            Toggle("", isOn: $airplaneMode)
                                                .labelsHidden()
                                        }
                                        
                                        Divider()
                                            .foregroundColor(.white)
                                            .offset(x: 0, y:14)
                                    }
                                }
                            }.padding(.horizontal)
                            SectionTitleViewUI(titleString: SettingScreenString.PreferencesStr, rightTitleString: CommonAllString.BlankStr, countShow: 0)
                            ForEach(0..<forSectionSecond.count){ indexs in
                                if forSectionSecond[indexs] == SettingScreenString.OpenMyDeviceSettingStr {
                                    VStack(alignment:.leading,spacing:0) {
                                        Group {
                                            Text(forSectionSecond[indexs]).font(.system(size: 16)).foregroundColor(.white)
                                            Group {
                                                Text(SettingScreenString.ToManageNotificationUsageStr)
                                            }.font(.system(size: 13)).foregroundColor(.gray)
                                        }.frame(maxWidth: .infinity,alignment: .leading)
                                            .background(Color.init(ColorConstantsName.MainThemeBgColour))
                                            .onTapGesture {
                                                if indexs == 1 {
                                                    if let url = URL(string: UIApplication.openSettingsURLString) {
                                                        DispatchQueue.main.async {
                                                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                                        }
                                                    }
                                                }
                                            }
                                    }
                                }else{
                                    Text(forSectionSecond[indexs])
                                        .frame(maxWidth: .infinity,alignment: .leading)
                                        .background(Color.init(ColorConstantsName.MainThemeBgColour))
                                        .onTapGesture {
                                            if indexs == 0 {
                                                showRideVisiblityClass = true
                                            }
                                        }
                                }
                                Divider()
                                    .foregroundColor(.white)
                                    .offset(x: 0, y:14)
                            }.padding(.horizontal)
                            SectionTitleViewUI(titleString: SettingScreenString.OthersStr, rightTitleString: CommonAllString.BlankStr, countShow: 0)
                            ForEach(0..<forSectionThird.count){ indexs in
                                Text(forSectionThird[indexs])
                                    .frame(maxWidth: .infinity,alignment: .leading)
                                    .background(Color.init(ColorConstantsName.MainThemeBgColour))
                                    .onTapGesture {
                                        if indexs == 0 {
                                            showAbouDTRScreen = true
                                        }else if indexs == 1 {
                                            showPrivacy = true
                                            showPrivacyTermsScreen = true
                                        }else if indexs == 2 {
                                            showPrivacy = false
                                            showPrivacyTermsScreen = true
                                        }else if indexs == 3 {
                                            showSingoutScreen = true
                                            print("Start Signout process")
                                        }else if indexs == 4 {
                                            showDeleteAccountScreen = true
                                        }
                                    }
                                Divider()
                                    .foregroundColor(.white)
                                    .offset(x: 0, y:8)
                            }.padding(.horizontal)
                        }.onChange(of: airplaneMode) { value in
                            if UserStore.shared.isLocalBuild != value {
                                updateFirebaseConfigration(isTrue:value)
                            }
                        }
                    }
                }.padding(.top,10)
                    .foregroundColor(.white)
                
                NavigationLink(destination:  RideVisiblity(dismissView:$showRideVisiblityClass,comingFromSettingScreen:true,isUpdateRide: .constant(false),isVisbilityUpdate: .constant(false), myRideForDay: RideEditViewModel.init(ride: DtrRide.default)), isActive: $showRideVisiblityClass){
                    EmptyView()
                }.isDetailLink(false)
                
                NavigationLink(destination:  AboutDTR(dismissView:$showAbouDTRScreen), isActive: $showAbouDTRScreen){
                    EmptyView()
                }.isDetailLink(false)
                
                NavigationLink(destination:  PrivacyAndterms(dismissView:$showPrivacyTermsScreen,isPrivacyScreen:showPrivacy), isActive: $showPrivacyTermsScreen){
                    EmptyView()
                }.isDetailLink(false)
                
                NavigationLink(destination:  DeleteAccount(dismissView:$showDeleteAccountScreen), isActive: $showDeleteAccountScreen){
                    EmptyView()
                }.isDetailLink(false)
            }.navigationBarBackButtonHidden(true)
                .navigationBarTitle("", displayMode: .inline)
                .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,textTitle: "Settings", systemImageName: ImageConstantsName.ArrowLeftImg, action: {
                    dismissView = false
                    if comesFromDirectHomeScreen {
                        self.presentationMode.wrappedValue.dismiss()
                    }
                })))
        }.hiddenNavigationBarStyle()

        .foregroundColor(.white)
        .onAppear{
            if UserStore.shared.isLocalBuild {
                airplaneMode = true
            } else {
                airplaneMode = false
            }
            Analytics.logEvent(AnalyticsScreen.setting.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
            buildVersion =  Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            buildId =  Bundle.main.infoDictionary?["CFBundleVersion"] as! String
        }
        
        .actionSheet(isPresented: $showSingoutScreen, content: { self.confirmationSignOut } )
        .fullScreenCover(item: $activeSheetForSetting) { item in
            switch item {
            case .sheetMail:
                if MFMailComposeViewController.canSendMail() {
                    MailView(result: self.$result,
                             recipients: ["dtr@socialactive.app"],
                             subject: "[DTR] Feedback",
                             body: "<p>I have something to say about DTR:</p><p></p><p>", isHTML: true,isRideFeedback:false,rideFeedbackData:DtrRide.default
                    )
                } else {
                    Text("This device is unable to send email")
                }
            case .home:
                HomeViewScreen()
                    .environmentObject(self.dtrData)
                    .environmentObject(UserSettings())
            }
        }
    }
    
    func updateFirebaseConfigration(isTrue:Bool) {
        if UserStore.shared.isLocalBuild == !isTrue {
            if isTrue  {
                UserStore.shared.isLocalBuild = true
                dtrData.signOut { (response) in
                    if response == true {
                        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                            exit(0)
                        }
                        
                    }
                }
            }else{
                UserStore.shared.isLocalBuild = false
                dtrData.signOut { (response) in
                    if response == true {
                        DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                            exit(0)
                        }
                    }
                }
            }
        }
    }
    
    var confirmationSignOut: ActionSheet {
        ActionSheet(title: Text("Are you sure?"), buttons: [.destructive(Text("Sign Out of DTR"), action: {
            dtrData.signOut(completion: { success in })
            self.showSingoutScreen = false
            UserDefaults.standard.removeObject(forKey: "Save_Dyanamic_Link_Local")
            activeSheetForSetting = .home
            print("After Click on Butotn End Signout process")
        }),.default(Text("Cancel"))])
    }
}

struct SettingScreen_Previews: PreviewProvider {
    static var previews: some View {
        SettingScreen(dismissView:.constant(false))
    }
}

enum ActiveSheetItemForSettingScreen: Identifiable {
    case home,sheetMail
    var id: Int {
        hashValue
    }
}
