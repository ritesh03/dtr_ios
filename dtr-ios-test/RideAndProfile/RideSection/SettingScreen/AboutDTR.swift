//
//  AboutDTR.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/03/21.
//

import SwiftUI

struct AboutDTR: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Binding var dismissView :Bool
    var versionString: String {
        var result = "Version"
        result.append(": ")
        result.append("\(fromBundle("CFBundleShortVersionString"))")
        return result
    }
    
    var buildString: String {
        var result = "Build"
        result.append(": ")
        result.append("\(fromBundle("CFBundleVersion"))")
        return result
    }
    
    var body: some View {
        ZStack{
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack{
                Image(ImageConstantsName.AboutDtrImg)
                    .resizable()
                    .frame(width: 80, height: 120,alignment: .top)
                    .padding(.top,50)
                    .padding(.bottom,30)
                Spacer()
                VStack(alignment: .leading,spacing: 10) {
                    HStack(spacing: 10) {
                        Text("Down to Ride (DTR)")
                            .font(.system(size: 20, weight: .bold))
                    }
                    HStack {
                        VStack(alignment: .leading,spacing: 10) {
                            Group{
                                Text(fromBundle("CFBundleIdentifier"))
                                Text(versionString)
                                Text(buildString)
                            }.font(.system(size: 18))
                        }
                        .font(.caption)
                    }
                    HStack {
                        Text("© 2021 Social Active LLC")
                            .font(.system(size: 19))
                    }
                    HStack {
                        Text("Built in Metro Detroit, Michigan")
                            .font(.system(size: 19))
                    }
                    Spacer()
                }
                .foregroundColor(.white)
                .padding()
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,textTitle: "About", systemImageName:ImageConstantsName.ArrowLeftImg, action: {
            dismissView = false
        })))
    }
}

struct AboutDTR_Previews: PreviewProvider {
    static var previews: some View {
        AboutDTR(dismissView: .constant(false))
    }
}
