//
//  ReturnGroupUserDetailList.swift
//  dtr-ios-test
//
//  Created by apple on 25/08/21.
//

import SwiftUI

struct ReturnGroupUserDetailList: View {
    @Binding var userGroups : DtrGroup
    var body: some View {
        Group {
            ProfileHeadGroup(group: userGroups, size: .medium)
            VStack(alignment: .leading, spacing: 10){
                HStack (spacing:10){
                    if userGroups.name != CommonAllString.BlankStr {
                        Text(userGroups.name)
                            .foregroundColor(.white)
                            .lineLimit(2)
                    }
                }
                HStack(alignment:.center) {
                    Group {
                        Group {
                            Text(getCityCountryOfUser()).font(.system(size: 14))
                        }.foregroundColor(.gray)
                       
                    }.foregroundColor(.white)
                    .opacity(0.65)
                }
            }
            .frame(maxWidth: .infinity ,alignment: .topLeading)
            .padding(.horizontal)
        }
    }
    
    func getCityCountryOfUser() -> String {
        var status = ""
        if userGroups.cityState != CommonAllString.BlankStr  &&  userGroups.cityState != CommonAllString.NullStr {
            status = userGroups.cityState
        }
//        if userGroups.country != CommonAllString.BlankStr && userGroups.country != CommonAllString.NullStr {
//            status += userGroups.country
//        }
        return status
    }
}
