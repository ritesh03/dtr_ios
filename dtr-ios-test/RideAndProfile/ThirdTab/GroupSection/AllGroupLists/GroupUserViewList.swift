//
//  GroupUserViewList.swift
//  dtr-ios-test
//
//  Created by apple on 25/08/21.
//

import SwiftUI
import Firebase

struct GroupUserViewList: View {
    @State var titleStatus = GroupDetailClassString.Joinstr
    @Binding var userGroup : DtrGroup
    @Binding var selectedGroup : DtrGroup
    @State var showActivityIndicator = false
    @State var isShowGroupDetail = false
    @Binding var showProfileScreen:Bool
    @Binding var showGroupDetailScreen :Bool
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    
    var body: some View {
        ZStack{
            VStack {
                HStack(alignment:.top) {
                    ReturnGroupUserDetailList(userGroups: $userGroup).onTapGesture {
                        if isShowGroupDetail {
                            selectedGroup = userGroup
                            showGroupDetailScreen = true
                        }else{
                            showProfileScreen = true
                        }
                    }
                    
                    if userGroup.leader != dtrData.profile.id {
                        Group {
                            if self.showActivityIndicator {
                                ProgressView()
                                    .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                    .frame(width: 20, height: 20)
                                    .padding(.trailing,5)
                            }
                            Button(action: {
                                callFuntionAccToStatus()
                                
                            }) {
                                Text(getStatusOfUser(groupId: userGroup.id))
                                    .multilineTextAlignment(.trailing)
                                    .lineLimit(2)
                                    .foregroundColor(titleStatus == CommonAllString.BlankStr ? Color.gray : Color.init(ColorConstantsName.AccentColour))
                                    .frame(width: titleStatus == GroupDetailClassString.Joinstr ? 80 : 105, alignment: .center).padding(3)
                                    .font(.system(size: 16, weight: .bold, design: .default))
                            }.buttonStyle(BorderlessButtonStyle())
                            .clipShape(Capsule())
                            .overlay(Capsule().stroke(Color.init(ColorConstantsName.AccentColour), lineWidth: titleStatus == "" ? 0 : 1))
                        }.disabled(showActivityIndicator == true ? true : false)
                    }
                }
                .padding(.all)
                Divider()
                    .background(Color.white)
            }
        }
    }
    
    func callFuntionAccToStatus(){
        showActivityIndicator = true
        if titleStatus == GroupDetailClassString.Joinstr {
            Analytics.logEvent(AnalyticsObjectEvent.group_join.rawValue, parameters: nil)
            self.dtrCommands.groupJoin(groupID: userGroup.id) { response in
                response.debug(methodName: "groupJoin")
                showActivityIndicator = false
            }
        }else{
            self.dtrCommands.groupLeft(groupID: userGroup.id) { response in
                response.debug(methodName: "groupLeft")
                showActivityIndicator = false
            }
        }
    }
    
    func getStatusOfUser(groupId:String) -> String {
        var status = GroupDetailClassString.Joinstr
        if dtrData.profile.joinedgroups.contains(groupId) {
            status = GroupDetailClassString.Leavestr
        }
        DispatchQueue.main.async {
            titleStatus = status
        }
        return status
    }
}
