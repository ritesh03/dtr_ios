//
//  ProfileHeadGroup.swift
//  dtr-ios-test
//
//  Created by apple on 25/08/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct ProfileHeadGroup: View {
    var group: DtrGroup
    var size: ProfileSize = .small
    
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    private var frameSize: CGFloat {
        switch size {
        case .small:
            return 32
        case .medium:
            return 48
        case .large:
            return 64
        case .extralarge:
            return 128
        }
    }
    @State private var image = Image(ImageConstantsName.PersonPlaceholderUserImg)
    
    var body: some View {
        Group {
            if group.image != "" {
                WebImage(url:URL(string:  group.image), options: [.progressiveLoad], isAnimating: .constant(true))
                    .purgeable(true)
                    .placeholder(image)
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: self.frameSize, height: self.frameSize)
                    .clipShape(Circle())
                    .padding(1)
            } else {
                self.image
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: self.frameSize, height: self.frameSize)
                    .clipShape(Circle())
                    .padding(1)
                
            }
        }
    }
}
