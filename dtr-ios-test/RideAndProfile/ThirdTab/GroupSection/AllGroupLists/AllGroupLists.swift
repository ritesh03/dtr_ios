//
//  AllGroupLists.swift
//  dtr-ios-test
//
//  Created by apple on 21/07/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct AllGroupLists: View {
    @Binding var dismissView:Bool
    @State var showUserProfileScreen = false
    @State var showGroupDetailScreen = false
    @State var showActivityIndicator = false
    @State var ifIsFromNoDtr = false
    
    @State var allGroupList = [DtrGroup]()
    @State var allTempGroupList = [DtrGroup]()
    @State var selectedGroupDetail = DtrGroup()
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var groupDetailEdit : GroupEditViewModel =  GroupEditViewModel.init(group: DtrGroup.default)
    @State var updateKeyboard = true
    @State private var titleDtr: String = ""
    @State private var clearAllText: Bool = false
    @ObservedObject var groupSearchModel: GroupSearchViewModel = .init()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    @State private var page: Int = 0
    @State private var totalUserCount: Int = 0
    @State private var showListOrNot = false
    private let pageSize: Int = 20
    @State private var totalCountArray = 0
    
    var body: some View {
        ZStack{
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            GeometryReader { geo in
                ScrollView(.vertical) {
                    VStack(spacing:20) {
                        Text("Type in the unique group code or group name to find a group:")
                            .foregroundColor(.white)
                            .fixedSize(horizontal: false, vertical: true)
                        HStack {
                            CustomTxtfldForFollowerScrn(isDeleteAcntScreen: .constant(false), isGroupListScreen: .constant(true), text:  $titleDtr, clearAllText: $clearAllText, isFirstResponder: $updateKeyboard, completion: { (reponse) in
                                updateKeyboard = true
                                if titleDtr.count >= 1 {
                                    page = 0
                                    showActivityIndicator = true
                                    totalCountArray = 0
                                    showListOrNot = true
                                    allGroupList.removeAll()
                                    groupSearchModel.groupSearch(searchKeyboard: titleDtr, data: ["page": "0","hitsPerPage":20], completion: { response,totalCount in
                                        showActivityIndicator = false
                                        allGroupList = response
                                        //  allGroupList = allGroupList.sorted { $0.name < $1.name }
                                        totalUserCount = totalCount
                                        
                                    })
                                }else{
                                    DispatchQueue.main.async {
                                        page = 0
                                        showActivityIndicator = false
                                        totalCountArray = 0
                                        showListOrNot = false
                                        allGroupList.removeAll()
                                        allGroupList = allTempGroupList
                                        // allGroupList = allGroupList.sorted { $0.name < $1.name }
                                    }
                                }
                            })
                            Button(action: {
                                withAnimation {
                                    page = 0
                                    totalCountArray = 0
                                    allGroupList.removeAll()
                                    allGroupList = allTempGroupList
                                    //  allGroupList = allGroupList.sorted { $0.name < $1.name }
                                    clearAllText = true
                                    updateKeyboard = false
                                    showListOrNot = false
                                }
                            }) {
                                Image(systemName: ImageConstantsName.XmarkImg)
                                    .aspectRatio(contentMode: .fit)
                            }
                        }
                        .padding()
                        .background(Color.init(ColorConstantsName.SectionBgColour))
                        .cornerRadius(10)
                        .foregroundColor(.white)
                        .frame(height: 50)
                    }.padding(.all)
                    
                    SectionTitleViewUI(titleString: CommonAllString.AllGroupsStr, rightTitleString: CommonAllString.BlankStr, countShow:  allGroupList.count) { }
                    
                    VStack(spacing:0) {
                        if allGroupList.count > 0 {
                            ForEach(allGroupList.indices, id: \.self, content: row(for:)).background(Color.clear).listRowBackground(Color.clear).listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
                        }
                    }
                    
                    Spacer()
                }.padding(.top, 5)
            }
            .onDisappear{
                updateKeyboard = false
            }
            
            .onAppear {
                getAllGroupListingWithPaging(firstTime: true, lastDocId: "")
            }
        }
        
        NavigationLink(destination: GroupDetailScreen(dismissView: $showGroupDetailScreen, showInviteScreen: true, groupDetailEdit: GroupEditViewModel.init(group: selectedGroupDetail)), isActive: $showGroupDetailScreen) {
            EmptyView()
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle(CommonAllString.BlankStr, displayMode: .inline)
        .navigationViewStyle(.stack)
        .navigationBarItems(leading:AnyView(leadingButton),trailing: AnyView(trailingButton))
    }
    
    func getAllGroupListingWithPaging(firstTime:Bool,lastDocId:String){
        if allGroupList.count == 0 && titleDtr == "" || firstTime == false {
            showActivityIndicator = true
            dtrCommand.getAllgroupListNew(lastDocId: firstTime == false ? lastDocId : CommonAllString.BlankStr) { response in
                if response.result == .ok {
                    if firstTime {
                        allGroupList.removeAll()
                        allTempGroupList.removeAll()
                    }
                    
                    if let userProfileArry = response.output["result"] as? NSArray {
                        for i in 0..<userProfileArry.count {
                            let dictGroup = DtrGroup.init(data:userProfileArry[i] as! DataDict )
                            allGroupList.append(dictGroup)
                            allTempGroupList.append(dictGroup)
                        }
                        allGroupList = allGroupList.sorted { $0.name.lowercased() < $1.name.lowercased() }
                        //allGroupList =   Array(Set(allGroupList.sorted(by: {(first: DtrGroup, second: DtrGroup) -> Bool in return first.name.lowercased() < second.name.lowercased() })))
                        //allTempGroupList = Array(Set(allTempGroupList.sorted(by: {(first: DtrGroup, second: DtrGroup) -> Bool in return first.name.lowercased() < second.name.lowercased() })))
                        //allGroupList =   Array(Set(allGroupList))
                        //allTempGroupList =   Array(Set(allTempGroupList))
                        showActivityIndicator = false
                    }
                }
            }
        }
    }
    
    func row(for idx: Int) -> some View {
        let isOn = Binding(
            get: {
                idx < self.allGroupList.count ? self.allGroupList[idx] : DtrGroup.default
            },
            set: {
                let isIndexValid = self.allGroupList.indices.contains(idx)
                if isIndexValid {
                    self.allGroupList[idx] = $0
                }
            }
        )
        
        return GroupUserViewList(userGroup: isOn, selectedGroup: $selectedGroupDetail, isShowGroupDetail:true, showProfileScreen: .constant(false), showGroupDetailScreen: $showGroupDetailScreen)  .listRowBackground(Color.clear).onAppear {
            if ((allGroupList.endIndex - 1) == idx) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    if titleDtr != "" {
                        if allGroupList.count != totalUserCount && allGroupList.count < totalUserCount {
                            page += 1
                            groupSearchModel.groupSearch(searchKeyboard: titleDtr, data: ["page": "\(page)","hitsPerPage":20], completion: { response,totalCount  in
                                for i in 0..<response.count {
                                    allGroupList.append(response[i])
                                }
                            })
                            //allGroupList = allGroupList.sorted { $0.name < $1.name }
                        }
                    } else {
                        if let lastElement = allTempGroupList[allTempGroupList.count-1] as? DtrGroup {
                            getAllGroupListingWithPaging(firstTime: false, lastDocId: lastElement.id)
                        }
                    }
                }
            }
        }.listRowBackground(Color.clear)
        
    }
    
    var leadingButton: some View {
        Button(action: {
            dismissView = false
            if ifIsFromNoDtr {
                self.presentationMode.wrappedValue.dismiss()
            }
                                            
        }) {
            HStack {
                Image(systemName: ImageConstantsName.ArrowLeftImg)
                Text(CreateGroupSectionString.JoinGroupSmalStr)
            }.foregroundColor(.white)}
    }
    
    var trailingButton: some View {
        Group {
            if self.showActivityIndicator {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                    .padding()
            }
        }
    }
}

struct AllGroupLists_Previews: PreviewProvider {
    static var previews: some View {
        AllGroupLists(dismissView: .constant(false))
    }
}
