//
//  AssignNewGroupLeaderScreen.swift
//  dtr-ios-test
//
//  Created by Vaibhav Thakkar on 11/01/22.
//

import SwiftUI

struct AssignNewGroupLeaderScreen: View {
    
    @State var day: Int = 0
    @State var profile: DtrProfile
    @State var groupMembers: [String]
    @State var opacity:Double = 1.0
    @State var errorStatus: String = ""
    @State var selectLeader: [String]
    @State var groupId: String
    @State var enableDisableMakeGroupLeaderBtn:Bool = true
    @State var isForAddNewLeaderToGroup = false
    @State private var friendFollowdRides = [DtrRide]()
    
    
    @StateObject var userListViewModel = UserListViewModel()
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack(spacing:0) {
                VStack (alignment:.leading,spacing:0) {
                    List {
                        returnGroupUserLists
                    }.listStyle(PlainListStyle())
                }.padding(.top,10)
                
                if errorStatus != "" {
                    Text(errorStatus)
                        .foregroundColor(.white)
                        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                        .padding()
                }
                VStack (alignment:.center,spacing:0) {
                    CustomButtonCircleWithCustomProperty(btnTitle: isForAddNewLeaderToGroup ? "Make Group Admin" : "Make Group Leader", btnWidth: UIScreen.screenWidth-100, btnHeight: 10, backgroundColord: true, txtColorAccent: false, strokeColor: false) {
                        
                        if isForAddNewLeaderToGroup {
                            
                            
                        } else {
                            
                            DtrCommand.sharedInstance.createNewGroupLeader(groupId: groupId, currentLeader: dtrData.profile.id, newLeader: selectLeader.first!) { (result) in
                                if result.result == .ok {
                                    errorStatus = ""
                                    removeLocalDataAndPopClas()
                                } else {
                                    opacity = 1.0
                                    enableDisableMakeGroupLeaderBtn = false
                                    if result.feedback.contains("already on") {
                                        errorStatus = "Invalid selection."
                                    }else{
                                        errorStatus = result.feedback
                                    }
                                    DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
                                        errorStatus = ""
                                    }
                                }
                            }
                        }
                    }.disabled(enableDisableMakeGroupLeaderBtn)
                    .padding(.bottom)
                    .opacity(opacity)
                }
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,textTitle: isForAddNewLeaderToGroup ? "Make Group Admin" : "Assign Group Leader", systemImageName: ImageConstantsName.ArrowLeftImg, action: {
                removeLocalDataAndPopClas()
            })))
        }.onAppear {
            let friends = dtrData.profile.friends
            let followers = dtrData.profile.followed
            var groupFriends = [String]()
            var groupFollowers = [String]()
            var otherGroupMembers = [String]()
            for members in groupMembers {
                if friends.contains(members) {
                    groupMembers.remove(at: groupMembers.firstIndex(of: members)!)
                    groupFriends.append(members)
                } else if followers.contains(members) {
                    groupMembers.remove(at: groupMembers.firstIndex(of: members)!)
                    groupFollowers.append(members)
                } else if members == dtrData.profile.id {
                    groupMembers.remove(at: groupMembers.firstIndex(of: members)!)
                } else {
                    otherGroupMembers.append(members)
                }
            }
            groupMembers.removeAll()
            groupMembers.append(contentsOf: groupFriends)
            groupMembers.append(contentsOf: groupFollowers)
            groupMembers.append(contentsOf: otherGroupMembers)
        }
        
    }
    
    
    
    func removeLocalDataAndPopClas() {
        presentationMode.wrappedValue.dismiss()
    }
    
    var returnGroupUserLists: some View {
        Group {
            if groupMembers.count > 0 {
                ForEach(groupMembers.indices, id: \.self) { indexs in
                    InviteScreenUI(day:$day,otherUserRides:$friendFollowdRides,ride:DtrRide(data: DataDict()),userProfiles: .constant(dtrData.profileDirectory[dynamicMember: groupMembers[indexs]]), isSelected: self.selectLeader.contains(groupMembers[indexs]), action: {
                        if self.selectLeader.contains(groupMembers[indexs]) {
                            selectLeader.removeAll()
                        } else {
                            selectLeader.removeAll()
                            selectLeader.append(groupMembers[indexs])
                        }
                        
                        if selectLeader.count > 0 {
                            enableDisableMakeGroupLeaderBtn = false
                        } else if selectLeader.count <= 0 {
                            enableDisableMakeGroupLeaderBtn = true
                        }
                    }).listRowInsets(EdgeInsets()).listRowBackground(Color.clear)
                }
            }
        }
    }
}
