//
//  ReturnGroupUserDetail.swift
//  dtr-ios-test
//
//  Created by apple on 22/07/21.
//

import SwiftUI

struct ReturnGroupUserDetail: View {
    @Binding var userProfiles : DtrProfile
    @Binding var groupDetail : DtrGroup
    var body: some View {
        Group {
            ProfileHead(profile: userProfiles, size: .medium)
            VStack(alignment: .leading, spacing: 10){
                HStack (spacing:10){
                    if userProfiles.name != CommonAllString.BlankStr {
                        Text(userProfiles.name)
                            .foregroundColor(.white)
                            .lineLimit(2)
                    }
                    
                    if groupDetail.leader == userProfiles.id {
                        Text(CommonAllString.AdminStr)
                            .foregroundColor(Color.init(ColorConstantsName.CrownColour))
                            .lineLimit(2)
                    }
                }
                if userProfiles.description != CommonAllString.BlankStr {
                    Text(userProfiles.description)
                        .lineLimit(1)
                        .foregroundColor(.gray)
                }
                HStack(alignment:.center) {
                    Group {
                        Text(getCityCountryOfUser()).font(.system(size: 14))
                    }.foregroundColor(.white)
                    .opacity(0.65)
                }
            }
            .frame(maxWidth: .infinity ,alignment: .topLeading)
            .padding(.horizontal)
        }
    }
    
    func getCityCountryOfUser() -> String {
        var status = ""
        if userProfiles.cityState != CommonAllString.BlankStr  &&  userProfiles.cityState != CommonAllString.NullStr {
            status = userProfiles.cityState
        }
        if userProfiles.country != CommonAllString.BlankStr && userProfiles.country != CommonAllString.NullStr {
            status += userProfiles.country
        }
        return status
    }
}
