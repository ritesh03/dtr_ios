//
//  GroupUserView.swift
//  dtr-ios-test
//
//  Created by apple on 22/07/21.
//

import SwiftUI

struct GroupUserView: View {
    @State var titleStatus = GroupDetailClassString.Joinstr
    @Binding var userProfiles : DtrProfile
    @Binding var selectedProfile : DtrProfile
    @Binding var groupDetail : DtrGroup
    @State var showActivityIndicator = false
    @State var isShowGroupDetail = false
    @Binding var showProfileScreen:Bool
    @Binding var showGroupDetailScreen :Bool
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    
    var body: some View {
        ZStack{
            VStack {
                HStack(alignment:.top) {
                    ReturnGroupUserDetail(userProfiles: $userProfiles,groupDetail:$groupDetail).onTapGesture {
                        if isShowGroupDetail {
                            showGroupDetailScreen = true
                        }else{
                            showProfileScreen = true
                            selectedProfile = userProfiles
                        }
                    }
                    if isShowGroupDetail {
                        Group {
                            if self.showActivityIndicator {
                                ProgressView()
                                    .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                    .frame(width: 20, height: 20)
                                    .padding(.trailing,5)
                            }
                            Button(action: {
                                showActivityIndicator = true
                            }) {
                                Text(titleStatus)
                                    .multilineTextAlignment(.trailing)
                                    .lineLimit(2)
                                    .foregroundColor(titleStatus == CommonAllString.BlankStr ? Color.gray : Color.init(ColorConstantsName.AccentColour))
                                    .frame(width: titleStatus == GroupDetailClassString.Joinstr ? 80 : 105, alignment: .center).padding(3)
                                    .font(.system(size: 16, weight: .bold, design: .default))
                            }.buttonStyle(BorderlessButtonStyle())
                            .clipShape(Capsule())
                            .overlay(Capsule().stroke(Color.init(ColorConstantsName.AccentColour), lineWidth: titleStatus == "" ? 0 : 1))
                        }.disabled(showActivityIndicator == true ? true : false)
                    }else {
                        Group {
                            if dtrData.profileID != userProfiles.id {
                                if !dtrData.profile.friends.contains(userProfiles.id) {
                                    if self.showActivityIndicator {
                                        ProgressView()
                                            .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                            .frame(width: 20, height: 20)
                                            .padding(.trailing,5)
                                    }
                                    if titleStatus != "requestedFriend" {
                                        Button(action: {
                                            callFuntionAccToStatus()
                                        }) {
                                            Text(getStatusOfUser(userId: userProfiles.id))
                                                .multilineTextAlignment(.trailing)
                                                .lineLimit(2)
                                                .foregroundColor(titleStatus == "Friend request sent" ? Color.gray : Color.init(ColorConstantsName.AccentColour))
                                                .frame(width: titleStatus == "Follow" ? 90 : 115, alignment: .center).padding(5)
                                                .font(.system(size: 15, weight: .bold, design: .default))
                                        }.buttonStyle(BorderlessButtonStyle())
                                        .clipShape(Capsule())
                                        .overlay(Capsule().stroke(Color.init(ColorConstantsName.AccentColour), lineWidth: titleStatus == "Friend request sent" ? 0 : 1))
                                    }
                                }
                            }
                        }.disabled(showActivityIndicator == true ? true : false)
                    }
                }
                .padding(.all)
                Divider()
                    .background(Color.white)
            }.navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle(CommonAllString.BlankStr, displayMode: .inline)
        }
    }
    
    func getStatusOfUser(userId:String) -> String {
        var status = "Follow"
        if dtrData.relationship(profileID: userId).rawValue == "following" {
            status = "Request Friend"
        }else if dtrData.relationship(profileID: userId).rawValue == "follwoingWithInvite" {
            status = "Friend request sent"
        }else if dtrData.relationship(profileID: userId).rawValue == "requestedFriend" {
            status = "requestedFriend"
        }
        DispatchQueue.main.async {
            titleStatus = status
        }
        return status
    }
    
    func callFuntionAccToStatus() {
        if userProfiles.id != "" {
            if titleStatus == "Follow" {
                showActivityIndicator = true
                self.dtrCommands.profileFollow(profileID: userProfiles.id){ respose in
                    respose.debug(methodName:"profileFollow")
                    if respose.feedback.contains("already friends") {
                        titleStatus = "Following"
                        showActivityIndicator = false
                    } else {
                        titleStatus = (respose.result == .error) ? respose.feedback : "Following"
                        if titleStatus == "Following" {
                            userProfiles.isFollower = true
                            userProfiles.followed.append(dtrData.profileID)
                        }
                        showActivityIndicator = false
                    }
                }
            }else if titleStatus == "Request Friend" {
                showActivityIndicator = true
                self.dtrCommands.sendFriendRequest(profileID: userProfiles.id){ respose in
                    respose.debug(methodName:"sendFriendRequest")
                    showActivityIndicator = false
                }
            }
        }
    }
}
