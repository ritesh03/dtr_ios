//
//  GroupUserView.swift
//  dtr-ios-test
//
//  Created by apple on 21/07/21.
//

import SwiftUI

struct GroupUserView: View {
    @State var titleStatus = "Join"
    @Binding var userProfiles : DtrProfile
    @State private var showActivity = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    
    var body: some View {
        
        NavigationLink(destination: GroupDetailScreen(dismissView: .constant(false))) {
//        NavigationLink(destination: ProfileScreen(day: .constant(0),userProfile: $userProfiles, showBackButtonOrNot:true)) {
            VStack {
                HStack(alignment:.top) {
                    ProfileHead(profile: userProfiles, size: .medium)
                    
                    VStack(alignment: .leading, spacing: 10){
                        HStack (spacing:10){
                            if userProfiles.name != "" {
                                Text(userProfiles.name)
                                    .foregroundColor(.white)
                                    .lineLimit(2)
                            }
                            Text("Admin")
                                .foregroundColor(Color.init("DTR-Crown"))
                                .lineLimit(2)
                        }
                        if userProfiles.description != "" {
                            Text(userProfiles.description)
                                .lineLimit(1)
                                .foregroundColor(.gray)
                        }
                        HStack(alignment:.center) {
                            Group {
                                if userProfiles.cityState != ""  &&  userProfiles.cityState != "<null>" {
                                    Text(userProfiles.cityState).lineLimit(1)
                                }
                                if userProfiles.country != "" && userProfiles.country != "<null>" {
                                    Text(userProfiles.country.prefix(5)) + Text ("...")
                                }
                            }.foregroundColor(.white)
                            .opacity(0.65)
                        }
                    }
                    .frame(maxWidth: .infinity ,alignment: .topLeading)
                    .padding(.horizontal)
                    Group {
                        if self.showActivity {
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                .frame(width: 20, height: 20)
                                .padding(.trailing,5)
                        }
                        Button(action: {
                            showActivity = true
                        }) {
                            Text(titleStatus)
                                .multilineTextAlignment(.trailing)
                                .lineLimit(2)
                                .foregroundColor(titleStatus == "" ? Color.gray : Color.init(DTR_ACCENT_COLOUR))
                                .frame(width: titleStatus == "Join" ? 90 : 115, alignment: .center).padding(5)
                                .font(.system(size: 16, weight: .bold, design: .default))
                        }.buttonStyle(BorderlessButtonStyle())
                        .clipShape(Capsule())
                        .overlay(Capsule().stroke(Color.init(DTR_ACCENT_COLOUR), lineWidth: titleStatus == "" ? 0 : 1))
                    }.disabled(showActivity == true ? true : false)
                }
                .padding(.all)
                Divider()
                    .background(Color.white)
            }
        }
    }
}
