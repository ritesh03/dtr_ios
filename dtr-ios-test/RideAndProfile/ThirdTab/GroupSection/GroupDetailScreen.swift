//
//  GroupDetailScreen.swift
//  dtr-ios-test
//
//  Created by apple on 21/07/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI
import Firebase

struct GroupDetailScreen: View {
    @Binding var dismissView:Bool
    @State var comeFromNotificaionScreen = false
    @State var shareGroupDetail = false
    @State var showLeaveGroupView = false
    @State var showOptionForGroupView = ""
    @State var showProfileScreen = false
    @State var showGroupEditScreen = false
    @State var showInviteScreen = false
    @State var showAsignGroupLeaderScreen = false
    @State var showAddNewLeaderToGroup = false
    @State var updatedNameGroup = ""
    @State var updatedGroupImgString = ""
    @State var updateCommandRunOrNot = false
    @State var groupJoinOrLeave =  GroupDetailClassString.Joinstr
    @State var selectedProfile = DtrProfile()
    @State var groupInviteArray = [AppNotification]()
    @ObservedObject var groupDetailEdit : GroupEditViewModel
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @Environment(\.presentationMode) var presentationMode
    @State private var  userActionShow = false
    @State var activeSheet: ActiveSheetForGroupDetailScreen?
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack {
                ZStack(alignment:.top) {
                    LinearGradient(gradient: Gradient(colors: [Color.init(ColorConstantsName.GradientTopColur),Color.init(ColorConstantsName.GradientBottomColur)]), startPoint: .top, endPoint: .bottom)
                        .frame(width: UIScreen.main.bounds.width, height: 130, alignment: .center)
                        .clipped()
                        .overlay(updatedGroupImgString == "" ? returnBlnkImageForGroup(): showSelectedImageForGroup()).padding(.bottom)
                        .edgesIgnoringSafeArea(.all)
                    
                }
                VStack(spacing:10) {
                    Group{
                        Text(updatedNameGroup).font(.system(size: 20, weight: .bold, design: .default)).fixedSize(horizontal: false, vertical: true)
                        Text(groupDetailEdit.grouEdit.groupLink).font(.system(size: 17, weight: .bold, design: .default))
                    }.foregroundColor(.white)
                    
                    if groupJoinOrLeave == "Share" {
                        
                        Group {
                            HStack {
                                Button(action: {
                                    UserStore.shared.updateOnlyOneTimeForINviteAndShare = true
                                    showInviteScreen = true
                                }) {
                                    Text("INVITE")
                                        .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: 50, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                        .padding(10)
                                        .foregroundColor(.white)
                                        .background(Color.init(ColorConstantsName.AccentColour))
                                        .cornerRadius(UIScreen.screenWidth*0.8/2)
                                }
                                
                                Button(action: {
                                    shareGroupDetail = true
                                }) {
                                    Text("SHARE")
                                        .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: 50, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                        .padding(10)
                                        .foregroundColor(.white)
                                        .background(Color.init(ColorConstantsName.AccentColour))
                                        .cornerRadius(UIScreen.screenWidth*0.8/2)
                                }
                            }.padding(.init(top: 10, leading: 10, bottom: 0, trailing: 10))
                        }
                        
                    } else {
                        
                        if dtrData.profileID == groupDetailEdit.grouEdit.leader {
                            HStack {
                                CustomButtonCircleWithCustomProperty(btnTitle: GroupDetailClassString.Sharestr, btnWidth: UIScreen.screenWidth*0.8, btnHeight: 10, backgroundColord: true, txtColorAccent:false,strokeColor:true,textBold:true) {
                                    shareGroupDetail = true
                                }
                            }.padding(.top)
                        }else {
                            CustomButtonCircleWithCustomProperty(btnTitle: groupJoinOrLeave, btnWidth: UIScreen.screenWidth*0.8, btnHeight: 10, backgroundColord: true, txtColorAccent:false,strokeColor:true,textBold:true) {
                                if groupJoinOrLeave == "Share" {
                                    shareGroupDetail = true
                                }else{
                                    callFuntionAccToStatus()
                                }
                            }.padding(.top)
                        }
                    }
                }.padding(.top)
                
                SectionTitleViewUI(titleString: GroupDetailClassString.InTheGroupStr, rightTitleString: CommonAllString.BlankStr, countShow:  groupDetailEdit.grouEdit.members.count) {}.padding(.top)
                                
                VStack(spacing:0) {
                    List {
                        if groupDetailEdit.grouEdit.members.count > 0 {
                            ForEach(self.groupDetailEdit.grouEdit.members.indices, id: \.self) { groupIndex in
                                GroupUserView(userProfiles: .constant(dtrData.profileDirectory[dynamicMember: groupDetailEdit.grouEdit.members[groupIndex]]),selectedProfile: $selectedProfile, groupDetail:$groupDetailEdit.grouEdit,isShowGroupDetail:false, showProfileScreen: $showProfileScreen, showGroupDetailScreen: .constant(false))
                            }.background(Color.clear).listRowBackground(Color.clear).listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
                        }
                    }.listStyle(PlainListStyle()).listRowBackground(Color.clear)
                }
                Spacer()
            }
            
            .onChange(of: showProfileScreen, perform: { responseShow in
                if responseShow {
                    activeSheet = .ProfileScreen
                    showProfileScreen = false
                }
            })
            
            .fullScreenCover(item: $activeSheet) { item in
                switch item {
                case .ProfileScreen:
                    NavigationView{
                        ProfileScreen(updatedDismissView:.constant(false),day: .constant(0), userProfile: $selectedProfile,showBackButtonOrNot:true)
                    }.accentColor(.white)
                case .CreateEditGroupScreen:
                    NavigationView{
                        CreateGroupScreen(dismissView: $showGroupEditScreen,updateCommandRunOrNot:$updateCommandRunOrNot,  comeFomEditScreen:true,displayImg2:updatedGroupImgString, displayName:updatedNameGroup, groupDetailEdit: groupDetailEdit).onDisappear{
                            if updateCommandRunOrNot {
                                updatedNameGroup = groupDetailEdit.grouEdit.name
                                updatedGroupImgString = groupDetailEdit.grouEdit.image
                            }else{
                                updatedNameGroup = updatedNameGroup
                                updatedGroupImgString = updatedGroupImgString
                            }
                        }
                    }.accentColor(.white)
                }
            }
            
            .background(EmptyView().sheet(isPresented:self.$shareGroupDetail) {
                Modal(isPresented: self.$shareGroupDetail, title: GroupDetailClassString.ShareRideDetailsStr) {
                    ShareGroup(isPresented: self.$shareGroupDetail, groupDetails:groupDetailEdit.grouEdit)
                }
            })
            
            NavigationLink(destination: AssignNewGroupLeaderScreen(profile: selectedProfile, groupMembers: groupDetailEdit.grouEdit.members, selectLeader: [""], groupId: groupDetailEdit.grouEdit.id, isForAddNewLeaderToGroup: true), isActive: $showAddNewLeaderToGroup) {
                EmptyView()
            }
            
            NavigationLink(destination: AssignNewGroupLeaderScreen(profile: selectedProfile, groupMembers: groupDetailEdit.grouEdit.members, selectLeader: [""], groupId: groupDetailEdit.grouEdit.id), isActive: $showAsignGroupLeaderScreen){
                EmptyView()
            }
            
            if groupJoinOrLeave == "Share" {
                NavigationLink(destination: InviteForGroupScreen(ride: .constant(DtrRide(data: DataDict())), alreadyAddedMembers: groupDetailEdit.grouEdit.members, groupID: groupDetailEdit.grouEdit.id, groupName: updatedNameGroup), isActive: $showInviteScreen){
                    EmptyView()
                }
                
                NavigationLink(destination: EmptyView()) {
                    EmptyView()
                }
            }
        }
        .onTapGesture(perform: {
            showLeaveGroupView = false
        })
        
        .onAppear{
            showInviteScreen = false
            
            if dtrData.profile.joinedgroups.contains(groupDetailEdit.grouEdit.id) || groupDetailEdit.grouEdit.members.contains(dtrData.profile.id){
                groupJoinOrLeave = "Share"
            }

            updateGetGroupDetail()
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle(CommonAllString.BlankStr, displayMode: .inline)
        .leadingNavigationBarItems { leadingButton.padding() }
        .trailingNavigationBarItems { trailingButton.padding() }
        //.navigationBarItems(leading:AnyView(leadingButton),trailing:AnyView(trailingButton))
    }
    
    func updateGetGroupDetail(){
        dtrCommands.getParticularGroupDetail(groupID: groupDetailEdit.grouEdit.id, completion: { response in
            if response.result == .ok {
                groupDetailEdit.grouEdit = DtrGroup.init(data: response.output["group"] as! DataDict)
                updatedNameGroup = groupDetailEdit.grouEdit.name
                updatedGroupImgString = groupDetailEdit.grouEdit.image
            }
        })
    }
    
    var leadingButton: some View {
        NavigtionItemBar(isImage: true,isSystemImage: false, imageName: ImageConstantsName.BckWithColorImg, action: {
            if comeFromNotificaionScreen {
                self.presentationMode.wrappedValue.dismiss()
            }else{
                self.presentationMode.wrappedValue.dismiss()
                dismissView = false
            }
        })
    }
    
    var trailingButton: some View {
        HStack{
            if showLeaveGroupView == true {
                ShowFlagInApprView(stringTitle:showOptionForGroupView,showPopUpFlgInapp:$showLeaveGroupView,action: {
                    showLeaveGroupView = false
                    if showOptionForGroupView == "Leave Group" {
                        callFuntionAccToStatus()
                    }else{
                        if groupDetailEdit.grouEdit.members.count > 2 {
                            deleteGoupActionSheetView()
                        } else {
                            alertView()
                        }
                    }
                }).cornerRadius(10)
            }else{
                if groupDetailEdit.grouEdit.members.contains(dtrData.profile.id) || groupJoinOrLeave == "Share" {
                    if groupDetailEdit.grouEdit.leader != dtrData.profile.id {
                        Button(action: {
                            showOptionForGroupView = "Leave Group"
                            showLeaveGroupView = true
                        }) {
                            Image(ImageConstantsName.BckWithThreeDotsImg)
                        }
                    }
                    else if groupDetailEdit.grouEdit.leader == dtrData.profile.id {
                        if !userActionShow {
                            Button(action: {
                                //showOptionForGroupView = "Delete Group"
                                userActionShow = true
                            }) {
                                Image(ImageConstantsName.BckWithThreeDotsImg)
                            }
                        }else{

                            Button(action: {
                                //showOptionForGroupView = "Delete Group"
                                userActionShow = true
                            }) {
                                Image(ImageConstantsName.BckWithThreeDotsImg)
                            } .onAppear {
                                userActionShow = false
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                    groupOptionsActionSheetView()
                                }
                                
                            }
                        }
                    }
                }
            }
        }
    }
    
    func alertView() {
        let alert = UIAlertController(title: CommonAllString.BlankStr, message: CommonErrorString.DeleteGroupErrorMesg, preferredStyle: .alert)
        let cancellButton = UIAlertAction(title: CommonAllString.NoStr, style: .default) { (_) in
            UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: false, completion: nil)
        }
        let okButton = UIAlertAction(title: CommonAllString.DeleteSmallStr, style: .destructive) { (_) in
            callDeleteGroupCommand()
            UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: false, completion: nil)
        }
        alert.addAction(okButton)
        alert.addAction(cancellButton)
        UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: false, completion: nil)
        UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true, completion: {
        })
    }
    
    func deleteGoupActionSheetView() {
        let alert = UIAlertController(title: CommonAllString.BlankStr, message: CommonErrorString.DeleteGroupWIthAsignLeader, preferredStyle: .actionSheet)
       
        let asignLeaderButton = UIAlertAction(title: CommonAllString.AssignLeader, style: .default) { (_) in
            showAsignGroupLeaderScreen = true
        }
        
        let deleteAnywayButton = UIAlertAction(title: CommonAllString.DeleteAnywayStr, style: .destructive) { (_) in
            callDeleteGroupCommand()
            UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: false, completion: nil)
        }
        
        let cancellButton = UIAlertAction(title: CommonAllString.CancelStr, style: .cancel) { (_) in
            
        }
        alert.addAction(asignLeaderButton)
        alert.addAction(deleteAnywayButton)
        alert.addAction(cancellButton)
        UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: false, completion: nil)
        UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true, completion: {
        })
    }
    
    func groupOptionsActionSheetView() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
       
        let groupEditButton = UIAlertAction(title: "Group Edit", style: .default) { (_) in
            activeSheet = .CreateEditGroupScreen
            showGroupEditScreen = true
        }
        
        let makeAdminButton = UIAlertAction(title: "Add New Admin", style: .default) { (_) in
            showAddNewLeaderToGroup = true
        }
        
        let deleteGroupButton = UIAlertAction(title: "Delete Group", style: .destructive) { (_) in
            if groupDetailEdit.grouEdit.members.count > 2 {
                deleteGoupActionSheetView()
            } else {
                alertView()
            }
        }
        
        let cancellButton = UIAlertAction(title: CommonAllString.CancelStr, style: .cancel) { (_) in
            
        }
        alert.addAction(groupEditButton)
        alert.addAction(makeAdminButton)
        alert.addAction(deleteGroupButton)
        alert.addAction(cancellButton)
        UIApplication.shared.windows.first?.rootViewController?.dismiss(animated: false, completion: nil)
        UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true, completion: {
        })
    }
    
    func showSelectedImageForGroup() -> AnyView {
        return AnyView(
            WebImage(url:URL(string: updatedGroupImgString), options: [.progressiveLoad], isAnimating: .constant(true))
                .purgeable(true)
                .placeholder(Image(ImageConstantsName.PersonPlaceholderUserImg))
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 128, height: 128)
                .clipShape(Circle())
                .padding(1)
                .offset(y: 65)
        )
    }
    
    func returnBlnkImageForGroup() -> AnyView {
        return AnyView(
            Image(ImageConstantsName.PersonPlaceholderUserImg)
                .renderingMode(.original)
                .resizable()
                .frame(width: 100, height: 100)
                .aspectRatio(contentMode: .fit)
                .offset(y: 65)
        )
    }
    
    func clearGroupInviteNotifications(groupId: String) {
        let notifications = groupInviteArray.filter({$0.payloadID == groupId})
        for notification in notifications {
            self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":notification.payloadID,"source":notification.source,"type":notification.type.rawValue,"target":notification.target]) { (result) in
            }
        }
    }
    
    func callFuntionAccToStatus() {
        if groupJoinOrLeave == "Join" {
            Analytics.logEvent(AnalyticsObjectEvent.group_join.rawValue, parameters: nil)
            
            self.dtrCommands.groupJoin(groupID: groupDetailEdit.grouEdit.id) { response in
                response.debug(methodName: "groupJoin")
                groupJoinOrLeave = "Share"
                dismissView = false
                clearGroupInviteNotifications(groupId: groupDetailEdit.grouEdit.id)
            }
        }else{
            self.dtrCommands.groupLeft(groupID: groupDetailEdit.grouEdit.id) { response in
                self.dtrCommands.sendSimpleNotification(profileID: self.groupDetailEdit.grouEdit.id, data: ["recipients":[ self.groupDetailEdit.grouEdit.leader],"type":"message","message":"\(dtrData.profile.name) has left the group \(self.updatedNameGroup)","action":"groupLeft"]) { ( result) in
                    result.debug(methodName:"sendSimpleNotification")
                    response.debug(methodName: "groupLeft")
                    groupJoinOrLeave = "Join"
                    dismissView = false
                }
            }
        }
    }
    
    func callDeleteGroupCommand() {
        self.dtrCommands.groupDelete(groupID: groupDetailEdit.grouEdit.id) { response in
            response.debug(methodName: "groupLeft")
            dismissView = false
        }
    }
}
struct GroupDetailScreen_Previews: PreviewProvider {
    static var previews: some View {
        GroupDetailScreen(dismissView: .constant(false), groupDetailEdit: GroupEditViewModel.init(group: DtrGroup.default))
    }
}
