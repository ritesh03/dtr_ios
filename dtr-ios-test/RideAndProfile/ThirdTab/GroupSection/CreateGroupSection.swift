//
//  CreateGroupSection.swift
//  dtr-ios-test
//
//  Created by apple on 21/07/21.
//

import SwiftUI

struct CreateGroupSection: View {
    @State var showGruopListScreen = false
    @State var showCreateGroupScreen = false
    
    var body: some View {
        ZStack(alignment:.center) {
            Color.init(ColorConstantsName.SectionBgColour)
            VStack(alignment:.center,spacing:25){
                Text(CreateGroupSectionString.JoinStartRidimhStr).multilineTextAlignment(.center).font(.system(size: 18, weight:.semibold, design: .default)).foregroundColor(.white).padding(.top)
                    .fixedSize(horizontal: false, vertical: true)
                
                CustomButtonCircleWithCustomProperty(btnTitle: CreateGroupSectionString.JoinGroupStr, btnWidth: UIScreen.screenWidth*0.8, btnHeight: 10, backgroundColord:true, txtColorAccent:false,strokeColor:false,textBold:true) {
                    showGruopListScreen = true
                }.padding(.bottom)
            }
            
            NavigationLink(destination:AllGroupLists(dismissView: $showGruopListScreen), isActive: $showGruopListScreen){
                EmptyView()
            }
            
            NavigationLink(destination:CreateGroupScreen(dismissView: $showCreateGroupScreen, updateCommandRunOrNot: .constant(false),  comeFomEditScreen:false,groupDetailEdit: GroupEditViewModel.init(group: DtrGroup.default)), isActive: $showCreateGroupScreen){
                EmptyView()
            }
        }
    }
}

