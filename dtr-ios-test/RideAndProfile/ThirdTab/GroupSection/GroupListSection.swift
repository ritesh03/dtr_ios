//
//  GroupListSection.swift
//  dtr-ios-test
//
//  Created by apple on 21/07/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct GroupListSection: View {
    @State var image = Image(ImageConstantsName.PersonPlaceholderUserImg)
    @State var uploadedImgUrl = CommonAllString.BlankStr
    @State var showAddBtn = true
    var body: some View {
        ZStack(alignment:.center) {
            VStack(alignment:.center,spacing:25){
                if !showAddBtn {
                    if uploadedImgUrl != CommonAllString.BlankStr {
                        WebImage(url:URL(string: uploadedImgUrl == CommonAllString.BlankStr ? DefaultUrlString.defaultRideImageUrl:uploadedImgUrl), options: [.progressiveLoad], isAnimating: .constant(true))
                            .purgeable(true)
                            .placeholder(image)
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 40, height: 40)
                            .clipShape(Circle())
                            .padding(1)
                    }else{
                        image
                            .resizable()
                            .frame(width: 40, height: 40)
                            .aspectRatio(contentMode: .fill)
                            .foregroundColor(.white)
                            .clipShape(Circle())
                            .padding(1)
                    }
                }else{
                    Image(systemName: ImageConstantsName.searchImg)
                        .resizable()
                        .frame(width: 20, height: 20)
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(.white)
                        .clipShape(Circle())
                        .padding(10)
                        .overlay(RoundedRectangle(cornerRadius:20).stroke(Color.white,lineWidth: 1))
                }
            }
        }
    }
}

struct GroupListSection_Previews: PreviewProvider {
    static var previews: some View {
        GroupListSection()
    }
}
