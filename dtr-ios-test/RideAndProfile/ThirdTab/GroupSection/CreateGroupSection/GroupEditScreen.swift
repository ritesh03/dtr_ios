//
//  GroupEditScreen.swift
//  dtr-ios-test
//
//  Created by apple on 26/07/21.
//

import SwiftUI

struct GroupEditScreen: View {
    @Binding var dismissView:Bool
    @State var showActivity = false
    @State var isOwnGroup = false
    @State var showGroupDetailScreen = false
    @State var isComeFromDetailScreen = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    
    @State private var startPlace = BLANK_STR
    @State private var startCountry = BLANK_STR
    @State private var startPlaceLat = 0.0
    @State private var startPlaceLon = 0.0
    @State private var showLocationChangeMap = false
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    
    @State var displayImg: String = BLANK_STR
    @State var displayName: String = BLANK_STR
    @State var displayLocation: String = BLANK_STR
    @State var showDisplayErrorForName =  false
    @State var showDisplayErrorForLocation = false
    @State var showDisplayErrorForImg = false
    @State var groupDetail = DtrGroup()
            
    var body: some View {
        ZStack {
            Color.init(MAIN_THEME_BG_COLOUR)
                .ignoresSafeArea()
            VStack {
                VStack {
                    ReturnGroupImage(uploadedImgUrl: $displayImg, showDisplayErrorForImg:$showDisplayErrorForImg)
                }
                VStack(alignment:.leading) {
                    ReturnGroupTextFieldSection(showMapScreen: $showLocationChangeMap, displayName: $displayName, displayLocation: $displayLocation, showDisplayErrorForName: $showDisplayErrorForName, showDisplayErrorForLocation: $showDisplayErrorForLocation)
                }.padding(30)
                Spacer()
            }
        }
        
        .fullScreenCover(isPresented: $showLocationChangeMap){
            MapScreen(day: .constant(0),showPublicRideLocationPopUp:false,isOnBoradingLat:$startPlaceLat,isOnBoradingLon:$startPlaceLon,canEditLocation:true,startCountry: $startCountry, startPlace: $startPlace, isOnBoradingProcess: .constant(true), isUpdateRideLocation: .constant(false), showRideLocationScreen: .constant(false), myRideForDay: RideEditViewModel.init(ride: DtrRide.default)) {
                print(startCountry,startPlace,startPlaceLat,startPlaceLon)
                if !startPlace.isEmpty {
                    showDisplayErrorForLocation = false
                }
                displayLocation = "\(startPlace), \(startCountry)"
            }
        }
        
        .onReceive(dtrData.$profileImageUploading, perform: { responseOutPut in
            if responseOutPut {
                self.showActivity = true
            } else {
                self.showActivity = false
            }
        })
        
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle(BLANK_STR, displayMode: .inline)
        .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true,isSystemImage: true,isText:true,txtTitle: isOwnGroup == true ? "Edit group" : "Create a group", systemImgName: ARROW_LEFT_IMG, action: {
            dismissView = false
        })),trailing:AnyView(trailingButton))
        .foregroundColor(.white)
        
//        if !isComeFromDetailScreen {
//            NavigationLink(destination:GroupDetailScreen(dismissView: $showGroupDetailScreen,groupDetail:groupDetail,comeFromCreateScreen:true), isActive: $showGroupDetailScreen){
//                EmptyView()
//            }
//        }
    }
    
    
    var trailingButton: some View {
        Group {
            Button(action: {
                checkFieldsNotEmptyAndCallApi()
            }) {
                if self.showActivity {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                        .foregroundColor(.white)
                        .padding()
                }
                if dtrData.profileImageUploading {
                    Text(UPLOADING_STR)
                        .font(.system(size: 20, weight: .bold, design: .default))
                        .foregroundColor(Color.init(DTR_ACCENT_COLOUR))
                } else {
                    Text(CREATE_STR)
                        .font(.system(size: 20, weight: .bold, design: .default))
                        .foregroundColor(Color.init(DTR_ACCENT_COLOUR))
                }
            }
        }.disabled(showActivity == true ? true : false)
    }
    
    func checkFieldsNotEmptyAndCallApi(){
        showDisplayErrorForName = false
        showDisplayErrorForLocation = false
        showDisplayErrorForImg = false
        if displayName.count < 3 {
            showDisplayErrorForName = true
        }
        
        if displayLocation.isEmpty {
            showDisplayErrorForLocation = true
        }
        
        if showDisplayErrorForName || showDisplayErrorForLocation {
            return
        }
        
        callAPIForCreateGroup()
    }
    
    func callAPIForCreateGroup() {
        showActivity = true
        let dict = ["name":displayName,"cityState":startPlace,"country":startCountry,"details":["LocationLat":startPlaceLat,"LocationLon":startPlaceLon],"image":displayImg] as [String : Any]
        dtrCommand.groupCreate(data: dict) { response in
            response.debug(methodName: "groupCreate")
            if response.result == .ok {
                groupDetail = DtrGroup.init(data: response.output["group"] as! DataDict)
                print(groupDetail)
                showActivity = false
                showGroupDetailScreen = true
                displayImg = BLANK_STR
                displayName = BLANK_STR
                displayLocation = BLANK_STR
            }
        }
    }
}
