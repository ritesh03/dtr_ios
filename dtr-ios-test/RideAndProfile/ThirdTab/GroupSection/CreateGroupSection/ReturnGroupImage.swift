//
//  ReturnGroupImage.swift
//  dtr-ios-test
//
//  Created by apple on 22/07/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI

struct ReturnGroupImage: View {
    @Binding var uploadedImgUrl:String
    @Binding var showDisplayErrorForImage:Bool
    @Binding var comeFomEditScreen:Bool
    @State var showingSheet = false
    @State private var imgTextUploadingOrChane = CommonAllString.AddGroupStr
    @StateObject var viewModel = getOptionToUploadImage()
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        VStack {
            if  uploadedImgUrl.isEmpty {
                returnDefaultImageForGroup
            }else{
                returnSelectedImageForGroup
            }
        }.padding(.top)
        
        VStack {
            Text(imgTextUploadingOrChane).font(.system(size: 14, design: .default))
                .foregroundColor(showDisplayErrorForImage == false ? Color.init(ColorConstantsName.AccentColour):.red)
        }.onTapGesture {
            showingSheet = true
            viewModel.isGroupImage = true
        }
        
        .onAppear{
            if viewModel.selectedImage == nil {
                if comeFomEditScreen {
                    imgTextUploadingOrChane = CommonAllString.ChangeImageStr
                }else{
                    imgTextUploadingOrChane = CommonAllString.AddGroupStr
                }
            }else{
                imgTextUploadingOrChane =  CommonAllString.ChangeImageStr
            }
        }
        
        .onReceive(viewModel.$returnImgUrlString, perform: { imgUrlStr in
            uploadedImgUrl = imgUrlStr
            showDisplayErrorForImage = false
        })
        
        .actionSheet(isPresented: $showingSheet) {
            ActionSheet(title: Text("What do you want to do?"), buttons: [.default(Text("From Camera"), action: {
                viewModel.takePhoto()
            }),.default(Text("From Gallery"), action: {
                viewModel.choosePhoto()
            }),.default(Text("Cancel"))])
        }
        
        .background(EmptyView().sheet(isPresented:  $viewModel.isPresentingImagePicker) {
            ImagePicker(sourceType: viewModel.sourceType, completionHandler: viewModel.didSelectImage)
        })
    }
    
    
    var returnDefaultImageForGroup: some View {
        Group {
            if comeFomEditScreen {
                WebImage(url:URL(string: uploadedImgUrl), options: [.progressiveLoad], isAnimating: .constant(true))
                    .purgeable(true)
                    .placeholder(Image(ImageConstantsName.MainRideEditCoverImg))
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 128, height: 128)
                    .clipShape(Circle())
                    .padding(1)
            } else {
                //Image(ImageConstantsName.MainRideEditCoverImg)
                //.renderingMode(.original)
                //.resizable()
                //.aspectRatio(contentMode: .fill)
                returnDefaultGroupImage
            }
        }
    }
    
    var returnDefaultGroupImage: some View {
        Group {
            Circle().fill()
                .foregroundColor(Color.init(ColorConstantsName.GroupCreateImageBgColour))
                .frame(width: 128, height: 128)
                .padding(1)
        }
    }
    
    var returnSelectedImageForGroup: some View {
        Group {
            if comeFomEditScreen {
                returnDefaultImageForGroup
            }else{
                Image(uiImage:viewModel.selectedImage!)
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: 128, height: 128)
                    .clipShape(Circle())
                    .padding(1)
            }
        }
    }
}
