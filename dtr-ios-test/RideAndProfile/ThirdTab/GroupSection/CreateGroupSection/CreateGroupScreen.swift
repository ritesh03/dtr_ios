//
//  CreateGroupScreen.swift
//  dtr-ios-test
//
//  Created by apple on 22/07/21.
//

import SwiftUI

struct CreateGroupScreen: View {
    @Binding var dismissView:Bool
    @Binding var updateCommandRunOrNot:Bool
    @State var showActivityIndicator = false
    @State var comeFomEditScreen = false
    @State var showGroupDetailScreen = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    
    @State private var startPlace = CommonAllString.BlankStr
    @State private var startCountry = CommonAllString.BlankStr
    @State private var startPlaceLat = 0.0
    @State private var startPlaceLon = 0.0
    @State private var showLocationChangeMap = false
    
    @State var displayImg: String = CommonAllString.BlankStr
    @State var displayImg2: String = CommonAllString.BlankStr
    @State var displayName: String = CommonAllString.BlankStr
    @State var displayLocation: String = CommonAllString.BlankStr
    @State var showDisplayErrorForName =  false
    @State var showDisplayErrorForLocation = false
    @State var showDisplayErrorForImage = false
    @State var displayLocationEditable = false
    @ObservedObject var groupDetailEdit : GroupEditViewModel
    @Environment(\.presentationMode) var presentationMode
    
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack {
                VStack {
                    ReturnGroupImage(uploadedImgUrl: $displayImg, showDisplayErrorForImage:$showDisplayErrorForImage,comeFomEditScreen:$comeFomEditScreen)
                }
                VStack(alignment:.leading) {
                    ReturnGroupTextFieldSection(showMapScreen: $showLocationChangeMap, displayName: $displayName, displayLocation: $displayLocation, showDisplayErrorForName: $showDisplayErrorForName, showDisplayErrorForLocation: $showDisplayErrorForLocation, displayLocationEditable: $displayLocationEditable)
                }.padding(30)
                Spacer()
            }
        }
        
        .fullScreenCover(isPresented: $showLocationChangeMap){
            MapScreen(day: .constant(0),fullLocationName:.constant(""),showPublicRideLocationPopUp:false,isOnBoradingLat:$startPlaceLat,isOnBoradingLon:$startPlaceLon,canEditLocation:true,startCountry: $startCountry, startPlace: $startPlace, isOnBoradingProcess: .constant(true), isUpdateRideLocation: .constant(false), showRideLocationScreen: .constant(false), myRideForDay: RideEditViewModel.init(ride: DtrRide.default)) {
                if !startPlace.isEmpty {
                    showDisplayErrorForLocation = false
                }
                displayLocation = "\(startPlace), \(startCountry)"
            }
        }
        
        
        .onAppear{
            if comeFomEditScreen {
                displayImg = displayImg2
                displayLocation = "\(groupDetailEdit.grouEdit.cityState) \(groupDetailEdit.grouEdit.country)"
                
                startPlace = groupDetailEdit.grouEdit.cityState
                startCountry = groupDetailEdit.grouEdit.country
                
                startPlaceLat = groupDetailEdit.grouEdit.details.LocationLat
                startPlaceLon = groupDetailEdit.grouEdit.details.LocationLon
                displayLocationEditable = true
                updateCommandRunOrNot = false
            }else{
                displayLocationEditable = false
            }
        }
        
        .onReceive(dtrData.$profileImageUploading, perform: { responseOutPut in
            if responseOutPut {
                self.showActivityIndicator = true
            } else {
                self.showActivityIndicator = false
            }
        })
        
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle(CommonAllString.BlankStr, displayMode: .inline)
        .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true,isSystemImage: true,isText:true,textTitle: comeFomEditScreen == true ? "Edit group" : "Create a group", systemImageName: ImageConstantsName.ArrowLeftImg, action: {
            if comeFomEditScreen {
                presentationMode.wrappedValue.dismiss()
            }
            dismissView = false
        })),trailing:AnyView(trailingButton))
        .foregroundColor(.white)
    }
    
    
    var trailingButton: some View {
        Group {
            Button(action: {
                checkFieldsNotEmptyAndCallApi()
            }) {
                if self.showActivityIndicator {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                        .foregroundColor(.white)
                        .padding()
                }
                if dtrData.profileImageUploading {
                    Text(CommonAllString.UploadingStr)
                        .font(.system(size: 20, weight: .bold, design: .default))
                        .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                } else {
                    Text(comeFomEditScreen == true ? CommonAllString.UpdateStr:CommonAllString.CreateStr)
                        .font(.system(size: 20, weight: .bold, design: .default))
                        .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                }
            }
        }.disabled(showActivityIndicator == true ? true : false)
    }
    
    func checkFieldsNotEmptyAndCallApi() {
        showDisplayErrorForName = false
        showDisplayErrorForLocation = false
        showDisplayErrorForImage = false
        if displayName.count < 3 {
            showDisplayErrorForName = true
        }
        
        if displayLocation.isEmpty {
            showDisplayErrorForLocation = true
        }
        
        if displayImg.isEmpty {
            showDisplayErrorForImage = true
        }
        
        if showDisplayErrorForName || showDisplayErrorForLocation ||  showDisplayErrorForImage {
            return
        }
        
        if comeFomEditScreen {
            callAPIForUpdateGroup()
        }else{
            callAPIForCreateGroup()
        }
    }
    
    func callAPIForCreateGroup() {
        showActivityIndicator = true
        let dict = ["name":displayName,"cityState":startPlace,"country":startCountry,"details":["LocationLat":startPlaceLat,"LocationLon":startPlaceLon],"image":displayImg] as [String : Any]
        dtrCommands.groupCreate(data: dict) { response in
            response.debug(methodName: "groupCreate")
            if response.result == .ok {
                dismissView = false
            }
        }
    }
    
    func callAPIForUpdateGroup(){
        showActivityIndicator = true
        let dict = ["name":displayName,"cityState":startPlace,"country":startCountry,"details":["LocationLat":startPlaceLat,"LocationLon":startPlaceLon],"image":displayImg] as [String : Any]
        dtrCommands.getUpdateGroupDetails(groupID: groupDetailEdit.grouEdit.id, data: dict) { response in
            if response.result == .ok {
                showActivityIndicator = false
                updateCommandRunOrNot = true
                if let editedGroup = response.output["group"] as? DataDict {
                    groupDetailEdit.grouEdit = DtrGroup.init(data: editedGroup)
                }
                presentationMode.wrappedValue.dismiss()
            }
        }
    }
}

