//
//  ReturnGroupTextFieldSection.swift
//  dtr-ios-test
//
//  Created by apple on 22/07/21.
//

import SwiftUI

struct ReturnGroupTextFieldSection: View {
    @Binding var showMapScreen:Bool
    @Binding var displayName: String
    @Binding  var displayLocation: String
    @Binding var showDisplayErrorForName:Bool
    @Binding var showDisplayErrorForLocation:Bool
    @Binding var displayLocationEditable:Bool    
    
    var body: some View {
        VStack(alignment:.leading,spacing:15) {
           ReturnSingleText(textName: ReturnGroupTextFieldSectionString.GroupNameStr)
            
            CustomTextField(userInput: $displayName, placholerText: ReturnGroupTextFieldSectionString.CatchNameGroupStr, isBackgroundColor: true, BackgroundColor: ColorConstantsName.HeaderBgColour, custmHeight: 50, custmAlignment: .leading, cornorRadius: 10).background(RoundedRectangle(cornerRadius: 10, style: .continuous).stroke(showDisplayErrorForName ? Color.red : Color.clear, lineWidth: 3))
            
            if showDisplayErrorForName {
                Text(ReturnGroupTextFieldSectionString.GroupNameErrorStr)
                    .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    .foregroundColor(Color.red)
            }
            
            ReturnSingleText(textName: ReturnGroupTextFieldSectionString.GroupLocationStr).padding(.top)

            HStack {
                WrappedTextFieldForLocation(text: .constant(displayLocation), placeholderText: .constant(ReturnGroupTextFieldSectionString.GroupWhereLocatedStr))
                    .frame(height: 50)
                    .padding(.horizontal)
                    .background(Color.init(ColorConstantsName.HeaderBgColour))
                    .foregroundColor(.white)
                    .cornerRadius(10)
                    Image(ImageConstantsName.LocationMarkImg)
                        .aspectRatio(contentMode: .fit)
            }.onTapGesture {
                showMapScreen = true
            }
            .disabled(displayLocationEditable)
            .padding(.trailing)
            .background(Color.init(ColorConstantsName.HeaderBgColour))
            .cornerRadius(10)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(showDisplayErrorForLocation ? Color.red : Color.clear, lineWidth: 1))
            
            if showDisplayErrorForLocation {
                Text(ReturnGroupTextFieldSectionString.GroupLocationErrorStr)
                    .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    .foregroundColor(Color.red)
            }
        }
    }
}

struct ReturnGroupTextFieldSection_Previews: PreviewProvider {
    static var previews: some View {
        ReturnGroupTextFieldSection(showMapScreen: .constant(false), displayName: .constant(""), displayLocation: .constant(""), showDisplayErrorForName: .constant(false), showDisplayErrorForLocation: .constant(false), displayLocationEditable: .constant(false))
    }
}
