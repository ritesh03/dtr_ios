//
//  DeleteAccount.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/03/21.
//

import SwiftUI
import Firebase

struct DeleteAccount: View {
    @Binding var dismissView:Bool
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommand = DtrCommand.sharedInstance
    @State private var showActivityIndicator = false
    @State private var titleDtr: String = ""
    @State private var feedBack: String = ""
    @State private var clearAllText: Bool = false
    @State private var showHomeScreen = false
    @State private var isFirstResponder = true
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack(alignment: .center, spacing:10){
                Text(DeleteAccountConstants.SORRY_SEE_TO_GO_STR).font(.system(size: 24, weight: .heavy, design: .default))
                    .foregroundColor(.white)
                Text(DeleteAccountConstants.ARE_YOU_SURE_DELETE_STR).font(.system(size: 17, weight: .regular, design: .default))
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                Text(DeleteAccountConstants.YOUR_ACCOUNT_TYPE_STR).font(.system(size: 17, weight: .regular, design: .default))
                    .foregroundColor(.white)
                Text(DeleteAccountConstants.TO_CONFIRM_STR).font(.system(size: 17, weight: .regular, design: .default))
                    .foregroundColor(.white)
                    .padding(.bottom,30)
                CustomTxtfldForFollowerScrn(isDeleteAcntScreen: .constant(true),isGroupListScreen: .constant(false), text:  $titleDtr, clearAllText: $clearAllText, isFirstResponder: $isFirstResponder, completion: { (reponse) in
                })
                .padding()
                .background(Color.init(ColorConstantsName.SectionBgColour))
                .cornerRadius(10)
                .foregroundColor(.white)
                .padding(.horizontal, 6)
                .frame(height: 40)
                
                if feedBack != CommonAllString.BlankStr {
                    Text(feedBack)
                        .foregroundColor(.white)
                        .padding(.top,30)
                }
                Spacer()
            }
            .padding()
            .fullScreenCover(isPresented: $showHomeScreen){
                HomeViewScreen()
                    .environmentObject(DtrData())
                    .environmentObject(UserSettings())
            }
            
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle(CommonAllString.BlankStr, displayMode: .inline)
            .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true,isSystemImage: true, systemImageName: ImageConstantsName.XmarkImg, action: {
                dismissView = false
                isFirstResponder = false
            })),trailing:AnyView(trailingButton))
            
            
        }.onTapGesture {
            UIApplication.shared.endEditing()
        }
        .onAppear{
            Analytics.logEvent(AnalyticsScreen.deleteAccount.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
        }
    }
    
    
    var trailingButton: some View {
        Button(action: {
            if titleDtr == DeleteAccountConstants.DELETE_STR {
                isFirstResponder = false
                showActivityIndicator = true
                DtrCommand.sharedInstance.deleteUserPermemtly(profileID: dtrData.profileID) { (response) in
                    response.debug(methodName: "deleteUserPermemtly")
                    if response.result == .ok {
                        showActivityIndicator = false
                        dtrData.signOut { (response) in
                            if response == true {
                                showHomeScreen = true
                            }
                        }
                    }else{
                        feedBack = DeleteAccountConstants.PLEASE_TRY_AGAIN_LATER_STR
                        showActivityIndicator = false
                    }
                }
            }
        }) {
            if self.showActivityIndicator {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                    .foregroundColor(.white)
                    .padding()
            }
            Text(DeleteAccountConstants.DELETE_ACCOUNT_STR)
                .font(.system(size: 18, weight: .bold, design: .default))
                .foregroundColor(.red)
        }
    }
}

struct DeleteAccount_Previews: PreviewProvider {
    static var previews: some View {
        DeleteAccount(dismissView: .constant(false))
    }
}

struct DeleteAccountConstants {
    
    static let DELETE_STR = "DELETE"
    static let DELETE_ACCOUNT_STR = "DELETE ACCOUNT"
    static let PLEASE_TRY_AGAIN_LATER_STR = "Please try again later"
    
    static let SORRY_SEE_TO_GO_STR = "Sorry to see you go! \n"
    static let ARE_YOU_SURE_DELETE_STR = "Are you sure you want to delete"
    static let YOUR_ACCOUNT_TYPE_STR = "your account? If so, type in DELETE"
    static let TO_CONFIRM_STR = "to confirm."
}

