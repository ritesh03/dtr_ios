//
//  RiderCenterView.swift
//  dtr-ios-test
//
//  Created by Mobile on 18/01/21.
//

import SwiftUI

struct RiderCenterView: View {
    @ObservedObject var dtrData = DtrData.sharedInstance
    @Binding  var ridesToDisplay : DtrRide!
    @Binding var showRideDetail : Bool
    @State private var showPopUp: Bool = false
    @Binding var showRideEdit : Bool
    
    var body: some View {
        ZStack {
            if ridesToDisplay != nil {
                VStack  (alignment: .leading) {
                    Image("DTR-CentrImg")
                        .resizable()
                        .frame( height: 100)
                        .cornerRadius(20, corners: [.topLeft, .topRight])
                        .padding(.bottom)
                    VStack  (alignment: .leading , spacing: 20) {
                        HStack {
                            Text(ridesToDisplay.summary).font(.system(size: 20, weight: .heavy, design: .default))
                                .foregroundColor(.white)
                            Button(action: {
                                self.showRideEdit = true
                            }) {
                                Image("DTR-UnVector")
                                    .resizable()
                                    .frame(width: 15,height: 15)
                            }
                        }
                        
                        Text("Riding with \(ridesToDisplay.riderProfiles[ridesToDisplay.leader]?.name ?? "" )").fontWeight(.semibold)
                            .foregroundColor(.white)
                        Image(uiImage: dtrData.profilePicture(profileID: ridesToDisplay.riderProfiles[ridesToDisplay.leader]?.id ?? "" ))
                        .renderingMode(.original)
                        .resizable()
                        .frame(width: 70, height: 70)
                        .clipShape(Circle())
                        .padding(1)
                        HStack {
                            Image(systemName: "clock")
                                .resizable()
                                .frame(width: 20,height: 20)
                                .foregroundColor(.white)
                            Text(ridesToDisplay.details.startTime)
                                .foregroundColor(.white)
                                .padding(.trailing)
                            Image(systemName: "mappin.and.ellipse")
                                .resizable()
                                .frame(width: 20,height: 20)
                                .foregroundColor(.white)
                            Text(ridesToDisplay.details.startPlace)
                                .foregroundColor(.white)
                        }
                        
                        HStack {
                            RideSkillIndicators(ride: self.ridesToDisplay)
                        }
                        
                        HStack   {
                            Button(action: {
                            }) {
                                Text("INVITE")
                                    .frame(width: 120,height:40)
                                    .foregroundColor(.white)
                                    .background(Color.init("DTR-Accent"))
                                    .cornerRadius(40)
                            }
                            
                            Button(action: {
                                
                            }) {
                                Text("SHARE")
                                    .frame(width: 120,height:40)
                                    .foregroundColor(.white)
                                    .background(Color.init("DTR-Accent"))
                                    .cornerRadius(40)
                            }.padding(.leading)
                            
                            Button(action: {
                                withAnimation(.linear(duration: 0.3)) {
                                    showPopUp.toggle()
                                }
                            }) {
                                Circle()
                                    .strokeBorder(Color.init("DTR-Accent"),lineWidth: 2)
                                    .background(
                                        Image("DTR-ShareIcon")
                                            .resizable()
                                            .frame(width: 30, height: 30)
                                    )
                                    .frame(width: 35, height: 35)
                            }.padding(.leading)
                        }
                        .padding(.bottom)
                    }
                    .padding(.horizontal)
                }
                .background(Color.init("DTR-HeaderBackground1"))
                .cornerRadius(20)
                .clipped()
                RelationPopUp(arrayCustom: ["Edit Ride","Other riders to join","Not DTR anymore"], show: $showPopUp) { (index) in
                    if index == "Edit Ride" {
                        self.showRideDetail = true
                    }
                }.offset(x: 50, y: 100)
            }
        }
    }
}

//struct CenterView_Previews: PreviewProvider {
//    static var previews: some View {
//        RiderCenterView()
//    }
//}
