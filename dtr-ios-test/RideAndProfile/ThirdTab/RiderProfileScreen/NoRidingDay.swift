//
//  NoRidingDay.swift
//  dtr-ios-test
//
//  Created by Mobile on 20/01/21.
//

import SwiftUI

struct NoRidingDay: View {
    
    @Binding var day: Int
    @State var presentingModal = false
    
    var body: some View {
        VStack(spacing: 10) {
            Image("DTR-CycleIcon")
                .padding(.vertical)
            Text("Tell your friends you're \n Down to Ride today!").font(.system(size: 25, weight: .heavy, design: .default))
                .foregroundColor(.white)
            Text("You'll be able to see who else \n is down to ride on this day.").font(.system(size: 15, weight: .heavy, design: .default))
                .foregroundColor(.gray)
                .padding(.vertical)
            
            
            Button(action: {
                print("I'm Down to ride")
                self.presentingModal = true
            }) {
                
                Text("I'M DOWN TO RIDE!")
                    .frame(width: UIScreen.screenWidth*0.8, height: 20)
                    .padding()
                    .foregroundColor(.white)
                    .background(Color.init("DTR-Accent"))
                    .cornerRadius(40)
            }.sheet(isPresented: $presentingModal) {
                UpdateRideTitle(presentedAsModal: self.$presentingModal, day: $day)
            }
            Spacer()
        }
    }
}

struct NoRidingDay_Previews: PreviewProvider {
    static var previews: some View {
        NoRidingDay(day: .constant(0))
    }
}
