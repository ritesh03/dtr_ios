//
//  OldRideScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 22/01/21.
//

import SwiftUI

struct OldRideScreen: View {
    
    @ObservedObject var dtrData = DtrData.sharedInstance
    @Binding  var oldRidesToDisplay : [DtrRide]
    @Binding  var selectedIndex : Int
    @Binding var myRideForDay : DtrRide
    
    var body: some View {
        VStack {
            Text("Or copy details from previous days - \n all ride info also copes over 🤙")
                .foregroundColor(.white)
                .multilineTextAlignment(.center)
                .padding()
            Divider()
                .background(Color.white)
        }
        ScrollView {
            VStack {
                ForEach(oldRidesToDisplay.indices, id: \.self) { ride in
                    HStack(alignment:.top) {
                        Image("DTR-CoffeCup")
                            .resizable()
                            .frame(width: 70, height: 70, alignment: .leading)
                            .cornerRadius(10)
                        VStack(alignment: .leading, spacing: 10){
                            Text(oldRidesToDisplay[ride].summary)
                                .foregroundColor(.white)
                            Text(dayName(dateCode: oldRidesToDisplay[ride].dateCode))
                                .foregroundColor(.gray)
                            Spacer()
                        }
                        .padding(.leading)
                        Spacer()
                            .background(Color.red)
                        VStack {
                            Image(selectedIndex == ride ? "DTR-Vector" : "DTR-EllipseRound")
                                .resizable()
                                .frame(width: 20, height: 20,alignment: .top)
                                .cornerRadius(10)
                            Spacer()
                        }
                    }.padding()
                    .background(selectedIndex == ride ? Color.clear : Color.init("DTR-HeaderBackground1"))
                    .onTapGesture {
                        if self.selectedIndex == ride {
                            self.selectedIndex = -1
                            return;
                        }
                        self.selectedIndex = ride
                        myRideForDay = oldRidesToDisplay[self.selectedIndex]
                    }
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(selectedIndex == ride ? Color.init("DTR-Accent") : Color.clear, lineWidth: 1)
                    )
                }.cornerRadius(10)
            }
        }
    }
    
    func dayName(dateCode: String) -> String{
        let input = DateFormatter()
        input.dateFormat = "yyyyMMdd"
        if let date = input.date(from: dateCode) {
            let output = DateFormatter()
            output.dateFormat = "E MMM d, yyyy"
            return output.string(from: date)
        }
        return dateCode
    }
    
    
}

extension Array where Element: Equatable {
    func removing(_ obj: Element) -> [Element] {
        return filter { $0 != obj }
    }
}



//struct OldRideScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        OldRideScreen(myRideForDay: DtrRide.default)
//    }
//}
