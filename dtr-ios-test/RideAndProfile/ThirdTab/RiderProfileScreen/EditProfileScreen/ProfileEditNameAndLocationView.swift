//
//  ProfileEditNameAndLocationView.swift
//  dtr-ios-test
//
//  Created by apple on 25/08/21.
//

import SwiftUI

struct ProfileEditNameAndLocationView: View {
    @Binding  var displayName: String
    @Binding  var displayLocation: String
    @Binding  var showLocation: Bool
    @Binding  var countCheckLessThen: Bool
    @Binding  var countLocationIsAvalible: Bool
    
    @Binding var didStartEditing : Bool
    @Binding var bio: String
    @State private var textStyle = UIFont.TextStyle.body
    
    var body: some View {
        VStack (alignment:.leading,spacing:20) {
            VStack (alignment:.leading,spacing:20) {
                
                Group {
                    ReturnSingleText(textName: "Your display name")
                    
                    CustomTextField(userInput: $displayName, placholerText: "Enter Name", isBackgroundColor: true, BackgroundColor:ColorConstantsName.HeaderBgColour, custmHeight: 50, custmAlignment: .leading, cornorRadius: 10).background(RoundedRectangle(cornerRadius: 10, style: .continuous).stroke(countCheckLessThen ? Color.red : Color.clear, lineWidth: 3))
                    
                    if countCheckLessThen {
                        Text("Rider name must be 3 characters long")
                            .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                            .foregroundColor(Color.red)
                    }
                    
                    
                    ReturnSingleText(textName: "This is the name that will show up in the app when your friends try to find you.")
                        .padding(.bottom)
                    
                    ReturnSingleText(textName: "Your Neighborhood")
                    
                    HStack {
                        WrappedTextFieldForLocation(text: $displayLocation, placeholderText: .constant(CommonAllString.LocationCapsStr))
                            .frame(height: 50)
                            .padding(.horizontal)
                            .background(Color.init(ColorConstantsName.HeaderBgColour))
                            .foregroundColor(.white)
                            .cornerRadius(10)
                        Image(ImageConstantsName.LocationMarkImg)
                            .aspectRatio(contentMode: .fit)
                    }.onTapGesture {
                        showLocation = true
                    }
                    .padding(.trailing)
                    .background(Color.init(ColorConstantsName.HeaderBgColour))
                    .cornerRadius(10)
                    ReturnSingleText(textName: "We only use location while using the app to change your location for public rides.")
                        .padding(.bottom)
                    
                    if countLocationIsAvalible {
                        Text(ReturnGroupTextFieldSectionString.GroupLocationErrorStr)
                            .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                            .foregroundColor(Color.red)
                    }
                }.fixedSize(horizontal: false, vertical: true)
                
                
                VStack (alignment:.leading,spacing:20) {
                    
                    ReturnSingleText(textName:"Bio")
                    
                    VStack (alignment:.center){
                        TextView(comesFromDetailAndEditScreen: .constant(true), isEditable: .constant(true), text:  $bio, textStyle: $textStyle, comesFromEditScreen: .constant(true), didStartEditing: $didStartEditing,placeHolderText:"Bio")
                            .cornerRadius(10)
                            .frame(width: UIScreen.screenWidth*0.9, height: 108, alignment: .center)
                            .onTapGesture {
                                didStartEditing = true
                            }
                    }
                    ReturnSingleText(textName:"Tell the community a little bit about yourself!")
                }
            }.padding()
        }.padding()
    }
}
