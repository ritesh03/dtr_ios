//
//  ProfileImageForEditScreen.swift
//  dtr-ios-test
//
//  Created by apple on 25/08/21.
//

import SwiftUI

struct ProfileImageForEditScreen: View {
    @Binding var showingSheet :Bool
    @ObservedObject var dtrData = DtrData.sharedInstance
    @Binding var viewModel : getOptionToUploadImage
    
    var body: some View {
        Group {
            if viewModel.selectedImage == nil {
                ProfileHead(profile: dtrData.profile, size: .extralarge)
            } else {
                Image(uiImage:viewModel.selectedImage!)
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 128, height: 128)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.gray, lineWidth: 1))
                    .padding(1)
            }
            
            VStack {
                ReturnSingleText(textName: "Change Photo",isaccentColor:true)
            }.onTapGesture {
                showingSheet = true
            }
        }
    }
}

