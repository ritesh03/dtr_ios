//
//  ProfileEditScrn.swift
//  dtr-ios-test
//
//  Created by Mobile on 16/02/21.
//

import SwiftUI
import Firebase

struct ProfileEditScrn: View {
    
    @Binding var dismissView : Bool
    @ObservedObject var rideDetailDay : RideEditViewModel
    @StateObject var viewModel = getOptionToUploadImage()
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var didStartEditing = false
    @State var displayName: String
    @State var displayLocation: String
    @State var displayCountry: String
    @State var bio: String
    @State var email: String
    @State var phone: String
    
    @State private var showingSheet = false
    @State private var showLocation = false
    @State private var showActivityIndicator = false
    @State private var startPlace = ""
    @State private var startCountry = ""
    @State private var countCheckLessThen = false
    @State private var isUpdateRideLocation = false
    @State private var countLocationIsAvalible = false
    
    @State var isOnBoradingLat: Double = 0.0
    @State var isOnBoradingLon: Double = 0.0
    
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            ScrollView {
                VStack(spacing:20) {
                    ProfileImageForEditScreen(showingSheet: $showingSheet, viewModel: .constant(viewModel))
                    
                    ProfileEditNameAndLocationView(displayName:$displayName,displayLocation:$displayLocation,showLocation:$showLocation,countCheckLessThen:$countCheckLessThen,countLocationIsAvalible:$countLocationIsAvalible, didStartEditing: $didStartEditing, bio: $bio)
                    
                    ProfileContactEmailAndPhoneView(email: $email, phone: $phone)
                }
            }
            .onTapGesture {
                UIApplication.shared.endEditing()
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,textTitle: "Profile", systemImageName: ImageConstantsName.ArrowLeftImg, action: {
                dismissView = false
            })),trailing:AnyView(trailingButton))
//            .navigationBarItems(leading:AnyView(NavigtionItemBar(isImage: true,isSystemImage: true, systemImgName: ImageConstantsName.ArrowLeftImg, action: {
//                dismissView = false
//            })),trailing:AnyView(trailingButton))
            .padding(.top,10)
            .foregroundColor(.white)
           
        }
        
        .onAppear{
            Analytics.logEvent(AnalyticsScreen.profile.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
        }
        
        .onReceive(dtrData.$profileImageUploading) { (responseProfileUploading) in
            if responseProfileUploading == true {
                self.showActivityIndicator = true
            }else{
                self.showActivityIndicator = false
            }
        }
        
        .actionSheet(isPresented: $showingSheet) {
            ActionSheet(title: Text("What do you want to do?"), buttons: [.default(Text("From Camera"), action: {
                viewModel.takePhoto()
            }),.default(Text("From Gallery"), action: {
                viewModel.choosePhoto()
            }),.default(Text("Cancel"))])
        }
        
        .fullScreenCover(isPresented: $showLocation){
            MapScreen(day: .constant(0),fullLocationName:.constant(""),isOnBoradingLat:$isOnBoradingLat,isOnBoradingLon:$isOnBoradingLon,canEditLocation:true,startCountry: $startCountry, startPlace: $startPlace, isOnBoradingProcess: .constant(true), isUpdateRideLocation: $isUpdateRideLocation, showRideLocationScreen: $showLocation, isFromProfile: true, myRideForDay: rideDetailDay) {
                self.showLocation = false
                self.displayLocation = self.startPlace
                self.displayCountry =  self.startCountry
                countLocationIsAvalible = false
            }
        }
        
        .background(EmptyView().sheet(isPresented:  $viewModel.isPresentingImagePicker) {
            ImagePicker(sourceType: viewModel.sourceType, completionHandler: viewModel.didSelectImage)
        })
    }
    
    
    var trailingButton: some View {
        Group {
            Button(action: {
                updateMesgWithTime()
            }) {
                if self.showActivityIndicator {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                        .foregroundColor(.white)
                        .padding()
                }
                if dtrData.profileImageUploading {
                    Text("Uploading")
                        .font(.system(size: 20, weight: .bold, design: .default))
                        .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                } else {
                    Text("Update")
                        .font(.system(size: 20, weight: .bold, design: .default))
                        .foregroundColor(Color.init(ColorConstantsName.AccentColour))
                }
            }
        }.disabled(showActivityIndicator == true ? true : false)
    }
    
    func updateMesgWithTime() {
        countCheckLessThen = false
        countLocationIsAvalible = false
        if displayName.count < 3 {
            countCheckLessThen = true
            return
        }
        
        if displayLocation.isEmpty {
            countLocationIsAvalible = true
            return
        }
        
        showActivityIndicator = true
        
        let updatedProfile =  dtrData.profile
        let dict = ["id":updatedProfile.id,"name" : displayName,"activated":updatedProfile.activated.timeIntervalSince1970,"country": displayCountry, "cityState" : displayLocation,"friends":updatedProfile.friends,"pushToken":updatedProfile.pushToken ?? "","verificationDeclined":updatedProfile.verificationDeclined,"defaultStatus":updatedProfile.defaultStatus,"linkCode":updatedProfile.linkCode,"description"  : bio,"tags":updatedProfile.tags,"platform":updatedProfile.platform, "phone": phone,"email": email,"pictureHash":updatedProfile.pictureHash,"userLat":isOnBoradingLat == 0 ? updatedProfile.userLat : isOnBoradingLat,"userLon":isOnBoradingLon == 0 ? updatedProfile.userLon : isOnBoradingLon] as [String : Any]
        dtrCommands.profileUpdate(profileID: dtrData.profile.id, data: dict) { (result) in
            result.debug(methodName:"profileUpdate")
            if result.result == .ok {
                showActivityIndicator = false
                presentationMode.wrappedValue.dismiss()
            }
        }
    }
}
