//
//  ProfileContactEmailAndPhoneView.swift
//  dtr-ios-test
//
//  Created by apple on 25/08/21.
//

import SwiftUI

struct ProfileContactEmailAndPhoneView: View {
    
    @Binding var email:String
    @Binding var phone:String
    @State private var isEmailValid : Bool   = true
    
    var body: some View {
        VStack{
            ZStack {
                Color.gray.frame(width: UIScreen.screenWidth, height: 1)
            }
        }
        VStack (alignment:.leading,spacing:20) {
            ReturnSingleText(textName:"OPTIONAL CONTACT INFO")
            ReturnSingleText(textName:"This will be visible to only your friends so they can contact you about rides. Your followers will not see your contact information. We will never share or sell your information.")
            ReturnSingleText(textName:"Your email")
            TextField("Enter email", text: $email, onEditingChanged: { (isChanged) in
                if !isChanged {
                    if textFieldValidatorEmail(self.email) {
                        self.isEmailValid = true
                    } else {
                        self.isEmailValid = false
                    }
                }
            }).frame(height: CGFloat(50), alignment: .leading)
            .foregroundColor(.white)
            .padding(.horizontal)
            .background(Color.init(ColorConstantsName.HeaderBgColour))
            .cornerRadius(CGFloat(10))
            
            if !self.isEmailValid {
                Text("Email is Not Valid")
                    .font(.callout)
                    .foregroundColor(Color.red)
            }
            
            ReturnSingleText(textName:"Your phone number")
            CustomTextField(userInput: $phone, placholerText: "Enter phone number", isBackgroundColor: true, BackgroundColor:ColorConstantsName.HeaderBgColour, custmHeight: 50, custmAlignment: .leading, cornorRadius: 10)
        }.fixedSize(horizontal: false, vertical: true)
        .padding()
        .padding(.horizontal)
    }
    
    func textFieldValidatorEmail(_ string: String) -> Bool {
        if string.count > 100 {
            return false
        }
        let emailFormat = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" + "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" + "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: string)
    }
}
