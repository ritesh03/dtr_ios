//
//  RideDetail.swift
//  dtr-ios-test
//
//  Created by Mobile on 21/01/21.
//

import SwiftUI

struct RideDetail: View {
    
    var myRideForDay : DtrRide
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
 
    var body: some View {
        ZStack {
            ScrollView {
                VStack {
                    Image("DTR-HeaderImage")
                        .resizable()
                        .aspectRatio(16/9, contentMode: .fit)
                    VStack(alignment:.leading,spacing:25) {
                        Text(myRideForDay.summary).font(.system(size: 20, weight: .heavy, design: .default)).multilineTextAlignment(.leading)
                        Text("Riding with \(myRideForDay.riderProfiles[myRideForDay.leader]?.name ?? "Someone")")
                        RidersHorizontal(ride: myRideForDay, showLeader: true)
                        HStack{
                            Image(systemName: "clock")
                            Text(myRideForDay.details.startTime)
                        }
                        HStack {
                            Image(systemName: "location")
                            Text(myRideForDay.details.startPlace)
                        }
                        
                        skillsNotesPresentation(ride: myRideForDay)
                        
                        Text("Public ride * Women Only * eBike Friendly Slow Roll * First Time Group Roder Friendly * Drop * No-Drop * Weekly Ride * Lights Required")
                        
                        Button(action: {
                            
                        }) {
                            Text("Join")
                                .padding(.horizontal)
                                .frame(width: UIScreen.screenWidth*0.8, height: 30)
                                .padding()
                                .foregroundColor(.white)
                                .background(Color.init("DTR-Accent"))
                                .cornerRadius(UIScreen.screenWidth*0.8/2)
                        }
                        VStack(alignment: .leading, spacing: 20){
                            Divider()
                                .background(Color.white)
                            Text("ABOUT THIS RIDE")
                            Text(myRideForDay.details.notes)
                        }.padding(.horizontal)
                        Spacer()
                    }
                    .padding(.horizontal)
                }
            }
        }
        .background(Color.init("DTR-MainThemeBackground"))
        .foregroundColor(.white)
        .navigationBarTitle("")
        .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
    
    func skillsNotesPresentation(ride: DtrRide?) -> AnyView {
        if let ride = ride {
            if (ride.details.skillA || ride.details.skillB || ride.details.skillC || ride.details.skillD || ride.details.skillS || ride.details.notes != "") {
                return AnyView(
                    HStack {
                        if ride.details.skillA {
                            Image(systemName: "a.circle.fill").foregroundColor(Color.init("DTR-SkillA"))
                        }
                        
                        if ride.details.skillB {
                            Image(systemName: "b.circle.fill").foregroundColor(Color.init("DTR-SkillB"))
                        }
                        
                        if ride.details.skillC {
                            Image(systemName: "c.circle.fill").foregroundColor(Color.init("DTR-SkillC"))
                        }
                        
                        if ride.details.skillD {
                            Image(systemName: "d.circle.fill").foregroundColor(Color.init("DTR-SkillD"))
                        }
                        
                        if ride.details.skillS {
                            Image(systemName: "s.circle.fill").foregroundColor(Color.init("DTR-SkillS"))
                        }
                        Spacer()
                    }
                )
            }
        }
        return AnyView(EmptyView())
    }
}

//struct RideDetail_Previews: PreviewProvider {
//    static var previews: some View {
//        RideDetail()
//    }
//}
