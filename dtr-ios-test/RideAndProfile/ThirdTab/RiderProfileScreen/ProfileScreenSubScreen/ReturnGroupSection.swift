//
//  ReturnGroupSection.swift
//  dtr-ios-test
//
//  Created by apple on 21/07/21.
//

import SwiftUI

struct ReturnGroupSection: View {
    @State var userProfile = DtrProfile()
    @State var showCreateGroupScreen = false
    @State var showGroupListScreen = false
    @State var showGroupDetailScreen = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    @State private var groupDetail = DtrGroup()
    
    @State var createdGroups = [String]()
    @State var joinedgroups = [String]()
    
    var body: some View {
        Group {
            if dtrData.profile.createdGroups.count > 0 || dtrData.profile.joinedgroups.count > 0 {
                VStack(alignment: .leading,spacing:15) {
                    returnGroupTitle
                    HStack {
                        VStack(spacing:0) {
                            HStack {
                                ScrollView (.horizontal, showsIndicators: false) {
                                    HStack {
                                        if dtrData.profileID == userProfile.id {
                                            returnOurGroup
                                        }else{
                                            returnViewerGroup
                                        }
                                        
                                        if dtrData.profileID == userProfile.id {
                                            if (dtrData.profile.createdGroups.count + dtrData.profile.joinedgroups.count) < 4 {
                                                GroupListSection(showAddBtn:true).onTapGesture {
                                                    showGroupListScreen = true
                                                }
                                                Spacer()
                                            }
                                        }
                                    }
                                }.frame(height: 50)
                                if dtrData.profileID == userProfile.id {
                                    if (dtrData.profile.createdGroups.count + dtrData.profile.joinedgroups.count) >= 4 {
                                        GroupListSection(showAddBtn:true).onTapGesture {
                                            showGroupListScreen = true
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }.padding()
            } else {
                VStack{
                    if dtrData.profileID == userProfile.id {
                        CreateGroupSection().frame(width: UIScreen.screenWidth, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    }
                }
            }
            
            NavigationLink(destination:AllGroupLists(dismissView: $showGroupListScreen), isActive: $showGroupListScreen){
                EmptyView()
            }
            
            
            NavigationLink(destination:GroupDetailScreen(dismissView: $showGroupDetailScreen,groupDetailEdit:GroupEditViewModel.init(group: groupDetail)), isActive: $showGroupDetailScreen){
                EmptyView()
            }
            .onAppear{
                loadViewUserGroups()
            }
        }
    }
    
    var returnOurGroup: some View {
        Group {
            if dtrData.profile.createdGroups.count > 0 {
                ForEach(self.dtrData.profile.createdGroups.indices, id: \.self) { groupIndex in
                    GroupListSection(uploadedImgUrl: dtrData.groupsDirectory[dynamicMember: dtrData.profile.createdGroups[groupIndex]].image,showAddBtn:false)
                        .onTapGesture {
                            groupDetail = dtrData.groupsDirectory[dynamicMember: dtrData.profile.createdGroups[groupIndex]]
                            showGroupDetailScreen = true
                        }
                }
            }
            
            if dtrData.profile.joinedgroups.count > 0 {
                ForEach(self.dtrData.profile.joinedgroups.indices, id: \.self) { groupIndex in
                    GroupListSection(uploadedImgUrl: dtrData.groupsDirectory[dynamicMember: dtrData.profile.joinedgroups[groupIndex]].image,showAddBtn:false).onTapGesture {
                        groupDetail = dtrData.groupsDirectory[dynamicMember: dtrData.profile.joinedgroups[groupIndex]]
                        showGroupDetailScreen = true
                    }
                }
            }
        }
    }
    
    var returnViewerGroup: some View {
        Group {
            if createdGroups.count > 0 {
                ForEach(createdGroups.indices, id: \.self) { groupIndex in
                    GroupListSection(uploadedImgUrl: dtrData.groupsDirectory[dynamicMember: createdGroups[groupIndex]].image,showAddBtn:false)
                        .onTapGesture {
                            groupDetail = dtrData.groupsDirectory[dynamicMember: createdGroups[groupIndex]]
                            showGroupDetailScreen = true
                        }
                }
            }
            
            if joinedgroups.count > 0 {
                ForEach(joinedgroups.indices, id: \.self) { groupIndex in
                    GroupListSection(uploadedImgUrl: dtrData.groupsDirectory[dynamicMember: joinedgroups[groupIndex]].image,showAddBtn:false).onTapGesture {
                        groupDetail = dtrData.groupsDirectory[dynamicMember: joinedgroups[groupIndex]]
                        showGroupDetailScreen = true
                    }
                }
            }
        }
    }
    
    var returnGroupTitle: some View {
        Group {
            if dtrData.profileID == userProfile.id {
                if dtrData.profile.createdGroups.count > 0 || dtrData.profile.joinedgroups.count > 0 {
                    ReturnSingleText(textName: "GROUPS")
                }
            }else {
                if createdGroups.count > 0 || joinedgroups.count > 0 {
                    ReturnSingleText(textName: "GROUPS")
                }
            }
        }
    }
    
    
    func loadViewUserGroups(){
        if dtrData.profileID != userProfile.id {
            dtrData.loadProfile(profileID: userProfile.id) { response in
                if response != nil {
                    createdGroups = response?.createdGroups ?? []
                    joinedgroups = response?.joinedgroups ?? []
                }
            }
        }
    }
}
