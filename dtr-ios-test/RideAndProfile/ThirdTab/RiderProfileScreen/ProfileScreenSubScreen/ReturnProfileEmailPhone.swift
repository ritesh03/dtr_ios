//
//  ReturnProfileEmailPhone.swift
//  dtr-ios-test
//
//  Created by apple on 21/07/21.
//

import SwiftUI
import MobileCoreServices
import MessageUI

struct ReturnProfileEmailPhone: View {
    @Binding var isAlreadyFriend :Bool
    @Binding var userProfile: DtrProfile
    @Binding  var showPopUpAfterFollowButton : Bool
    @State private var showPhoneActions = false
    @State private var showEmailActions = false
    @State private var showMailScreen = false
    @State var result: Result<MFMailComposeResult, Error>? = nil
    
    var body: some View {
        ZStack{
            HStack {
                ShowPopUpMesg(titleMesg: "", customMesg: "You can send a friend request to see friends-only rides and information.", show: $showPopUpAfterFollowButton, opticity: .constant(true)).padding()
            }
            
            if isAlreadyFriend {
                HStack {
                    if userProfile.phone != "" {
                        VStack(alignment: .leading, spacing: 15) {
                            Image(ImageConstantsName.PhoneImg)
                            Text(userProfile.phone ?? "No Number")
                        }.onTapGesture {
                            self.showPhoneActions = true
                        }.disabled(userProfile.phone == nil)
                        .actionSheet(isPresented: $showPhoneActions, content: { self.phoneActionSheet } )
                        Spacer()
                    }
                    
                    if userProfile.email != ""  {
                        VStack(alignment: .leading, spacing: 15) {
                            Image(ImageConstantsName.EmailImg)
                            Text(userProfile.email ?? "No Email")
                        }.onTapGesture {
                            self.showEmailActions = true
                        }.disabled(userProfile.email == nil)
                        .actionSheet(isPresented: $showEmailActions, content: { self.emailActionSheet } )
                    }
                    if userProfile.phone == "" {
                        Spacer()
                    }
                }
            }
        }.navigationBarHidden(false)
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle("", displayMode: .inline)
        .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
        .padding(.horizontal)
        .fullScreenCover(isPresented: $showMailScreen) {
            if MFMailComposeViewController.canSendMail() {
                MailView(result: self.$result,recipients: [self.userProfile.email ?? ""],subject: "[DTR]",body: "<p></p><p>", isHTML: false,isRideFeedback:false, rideFeedbackData: DtrRide.default,comesFromProfileScreen:true)
            } else {
                VStack {
                    Text("This device is not setup to send email")
                        .padding()
                    Button(action: {
                    }) {
                        Text("Dismiss")
                    }
                    .padding()
                }
            }
        }
    }
    
    var emailActionSheet: ActionSheet {
        ActionSheet(
            title: Text(self.userProfile.name),
            buttons: [
                .default(Text("Send Email"), action: {
                    showMailScreen = true
                }),
                .cancel {
                }
            ])
    }
    
    var phoneActionSheet: ActionSheet {
        ActionSheet(
            title: Text(self.userProfile.name),
            buttons: [
                .default(Text("Call"), action: {
                    if let number = self.userProfile.phone {
                        number.makeACall()
                    }
                }),
                .cancel {
                }
            ])
    }
}
