//
//  FollowerUnFollowCount.swift
//  dtr-ios-test
//
//  Created by Mobile on 21/01/21.
//

import SwiftUI

struct FollowerUnFollowCount: View {
    
    @Binding var getStatusMutaluser: String
    @Binding var showPopUpForSendRequestFollow : Bool
    @State var  notificationInfo : AppNotification = AppNotification(data: ["1":"2"])
    @Binding var profile : DtrProfile
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State private var relationBtnHidden = false
    @State private var showPopUp: Bool = false
    @Binding var showPopUpSendRequest : Bool
    @Binding var isAlreadyFriend : Bool
    @State private var userActionShow: Bool = false
    @State private var checkStatusForProfile: String = ""
    @State private var showListOfPeoleScreen = false
    @State private var selectedTab = 0
    
    @State var following = "0"
    @State var followed = "0"
    @State var friends = "0"
    @State var profileID = ""
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading) {
                HStack {
                    VStack(alignment:.leading,spacing:10) {
                        Text("\(following)")
                            .foregroundColor(.white)
                        Text("Following")
                            .foregroundColor(.gray)
                    }.onTapGesture {
                            checkStatusForProfile = "Following"
                            showListOfPeoleScreen = true
                            selectedTab = 0
                    }
                    VStack(alignment:.leading,spacing:10) {
                        Text("\(followed)")
                            .foregroundColor(.white)
                        Text("Followers")
                            .foregroundColor(.gray)
                    }.onTapGesture {
                            checkStatusForProfile = "Followers"
                            showListOfPeoleScreen = true
                            selectedTab = 1
                    }
                    VStack(alignment:.leading,spacing:10) {
                        Text("\(friends)")
                            .foregroundColor(.white)
                        Text("Friends")
                            .foregroundColor(.gray)
                    }.onTapGesture {
                            checkStatusForProfile = "Friends"
                            showListOfPeoleScreen = true
                            selectedTab = 2
                    }
                    Spacer()
                    if dtrData.profileID != profile.id {
                        Text(returnRelation().0).font(.system(size: 15, weight: .bold, design: .default))
                            .foregroundColor(Color.init(ColorConstantsName.CrownColour))
                            .multilineTextAlignment(.trailing)
                            .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    }
                }.padding()
                .background(Color.init(ColorConstantsName.SectionBgColour))
                if dtrData.profileID != profile.id {
                    VStack(alignment: .leading, spacing: 15) {
                        if returnRelation().4 != "" {
                            Text(returnRelation().4)
                                .fixedSize(horizontal: false, vertical:true )
                                .foregroundColor(.white)
                        }
                        
                        if getStatusMutaluser != "" {
                            Text(getStatusMutaluser)
                                .fixedSize(horizontal: false, vertical:true )
                                .foregroundColor(.gray)
                        }
                        
                        HStack(spacing:20) {
                            if returnRelation().1 == "Accept Friend Request" {
                                HStack {
                                    CustomButtonCircleWithCustomProperty(btnTitle: "Accept", btnWidth: 90, btnHeight:10, backgroundColord: true, txtColorAccent: false, strokeColor: true) {
                                        self.dtrCommands.createFriendShipRelation(profileID: profile.id) { (result) in
                                            result.debug(methodName:"createFriendShipRelation")
                                            if result.result == .ok {
                                                self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":notificationInfo.payloadID,"source":notificationInfo.source,"type":notificationInfo.type.rawValue,"target":notificationInfo.target]) { (result) in
                                                }
                                            }
                                        }
                                    }.buttonStyle(BorderlessButtonStyle())
                                    
                                    CustomButtonCircleWithCustomProperty(btnTitle: "Ignore", btnWidth: 90, btnHeight:10, backgroundColord: false, txtColorAccent: false, strokeColor: false) {
                                        self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":notificationInfo.payloadID,"source":notificationInfo.source,"type":notificationInfo.type.rawValue,"target":notificationInfo.target]) { (result) in
                                            result.debug(methodName:"clearNotifcationFromServer")
                                        }
                                    }.buttonStyle(BorderlessButtonStyle()).padding(.leading,30)
                                }.padding(.leading,30)
                            } else {
                                if !returnRelation().2 {
                                    if returnRelation().1 != "" {
                                        Button(action: {
                                            updateRelationStatus(returnRelation().1)
                                        }) {
                                            Text(returnRelation().1)
                                                .frame(width: 200, height: 25)
                                                .padding()
                                                .foregroundColor(.white)
                                                .background(Color.init(ColorConstantsName.AccentColour))
                                                .cornerRadius(200/2)
                                        }
                                    }else{
                                        Spacer()
                                    }
                                }
                                
                                if returnRelation().3.count > 0 {
                                    Button(action: {
                                        showPopUpSendRequest = false
                                        if returnRelation().3.count > 0 {
                                            showPopUp.toggle()
                                        }
                                    }){
                                        Image(ImageConstantsName.OutlinedBtnImg)
                                            .resizable()
                                            .frame(width: 40, height: 40)
                                    }
                                }
                            }
                        }
                        if userActionShow {
                            RelationPopUp(arrayCustom: returnRelation().3, show: $showPopUp) { (index) in
                                updateRelationStatus(index)
                            }.offset(x: 90, y: -30)
                        }
                    }.padding(.all)
                }
            }.background(Color.init(ColorConstantsName.MainThemeBgColour))
            .actionSheet(isPresented: $showPopUpForSendRequestFollow, content: { self.confirmationForSendFriendRequestOrFollow } )
            //NavigationLink(destination:AllUserInOneTab(selectedTab:selectedTab, profile: profile),isActive: $showListOfPeoleScreen) {}
            .fullScreenCover(isPresented: $showListOfPeoleScreen) {
                NavigationView{
                    AllUserInOneTab(selectedTab:selectedTab, profile: profile)
                }
            }
        }
        
        .onReceive(self.dtrData.$notifications) { getLatestNotification in
            if dtrData.relationship(profileID: profile.id).rawValue == "requestedFriend" {
                getAllTypesOfNotifcation()
            }
        }
        
        .onReceive( dtrData.$profile, perform: { response in
            if profile.id != CommonAllString.BlankStr && profile.id != CommonAllString.UnknownStr {
                self.dtrData.loadProfile(profileID: profile.id) { responseProfile in
                    following = "\(responseProfile?.following.count ?? 0)"
                    followed = "\(responseProfile?.followed.count ?? 0)"
                    friends =  "\(responseProfile?.friends.count ?? 0)"
                }
            }
        })
        
        
        .onAppear {
            if dtrData.relationship(profileID: profile.id).rawValue == "requestedFriend" {
                getAllTypesOfNotifcation()
            }
            
            self.dtrData.loadProfile(profileID: profile.id) { responseProfile in
                following = "\(responseProfile?.following.count ?? 0)"
                followed = "\(responseProfile?.followed.count ?? 0)"
                friends =  "\(responseProfile?.friends.count ?? 0)"
                profileID = responseProfile?.id ?? ""
            }
        }
        .onTapGesture {
            if showPopUp == true {
                showPopUp.toggle()
            }
        }
    }
    
    var confirmationForSendFriendRequestOrFollow: ActionSheet {
        ActionSheet(title: Text(""), buttons: [.default(Text("Do you want to follow and send friend request to this user"), action: {
            callFollowAndFriendRequestMethod()
        }),.destructive(Text("Cancel"))])
    }
    
    
    func callFollowAndFriendRequestMethod() {
        if self.dtrData.relationship(profileID: profile.id) == Relationship.none || self.dtrData.relationship(profileID: profile.id) == Relationship.followed {
            if self.dtrData.relationship(profileID: profile.id) != Relationship.acceptedFriend {
                self.dtrCommands.profileFollow(profileID: profile.id){ respose in
                    respose.debug(methodName:"profileFollow")
                    profile.isFollower = true
                    profile.followed.append(dtrData.profileID)
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                        if self.dtrData.relationship(profileID: profile.id) == Relationship.following {
                            self.dtrCommands.sendFriendRequest(profileID: profile.id){ respose in
                                respose.debug(methodName:"sendFriendRequest")
                                showPopUpForSendRequestFollow = false
                            }
                        }
                    }
                }
            }
        }else if self.dtrData.relationship(profileID: profile.id) == Relationship.following {
            if self.dtrData.relationship(profileID: profile.id) != Relationship.acceptedFriend {
                self.dtrCommands.sendFriendRequest(profileID: profile.id){ respose in
                    respose.debug(methodName:"sendFriendRequest")
                    showPopUpForSendRequestFollow = false
                }
            }
        }
    }

    
    func getAllTypesOfNotifcation() {
        let notifications = self.dtrData.notifications
        for index in 0..<notifications.count {
            if notifications[index].type == .friendInvite {
                if notifications[index].source == profile.id {
                    notificationInfo = notifications[index]
                    return
                }
            }
        }
    }

    
    func updateRelationStatus(_ index:String) {
        if profile.id == "Default Profile" {
            profile.id =  profileID
        }
        
        if index == "Cancel friend request" || index == "CANCEL REQUEST" {
            self.dtrCommands.userDecline(profileID: profile.id){ respose in
                respose.debug(methodName:"userDecline")
//                respose.debug()
//                let filtered = self.dtrData.sentNotifications.filter { $0.target == profile.id}
//                if filtered.count > 0 {
//                    let arraySen = filtered.filter { $0.type == .friendInvite}
//                    if filtered.count > 0 {
//                    }
//                }
            }
        } else if index == "Unfollow" {
            self.dtrCommands.profileUnfollow(profileID: profile.id){ respose in
                respose.debug(methodName:"profileUnfollow")
                profile.isFollower = false
                for _ in (0..<profile.followed.count) {
                    if profile.followed.contains(dtrData.profileID) {
                        profile.followed.removeAll(where: { $0 == dtrData.profileID})
                    }
                }
                
                // Workaround if sometime another user profile is id not present in the own
                // profile following array so we can check first and after update and remove the another user id
                if respose.feedback.contains("does not follow") {
                    if !profile.followed.contains(dtrData.profile.id) {
                        if dtrData.profile.following.contains(profile.id) {
                            self.dtrData.removeFriendItem(tbleName: "profiles", arrayUpdate: "following", userId: profile.id) { response in
                                print("Testing Purpose = ",response as Any)
                            }
                        }
                    }
                }
            }
        } else if index == "UnFriend" {
            self.dtrCommands.userUnfriend(profileID: profile.id){ respose in
                respose.debug(methodName:"userUnfriend")
                // Workaround if sometime another user profile is id not present in the own
                // profile friend array so we can check first and after update and remove the another user id
                if respose.feedback.contains("are not friends") {
                    if !profile.friends.contains(dtrData.profile.id) {
                        if dtrData.profile.friends.contains(profile.id) {
                            self.dtrData.removeFriendItem(tbleName: "profiles", arrayUpdate: "friends", userId: profile.id) { response in
                                print("Testing Purpose = ",response as Any)
                            }
                        }
                    }
                }
            }
        } else if index == "Follow" {
            if self.dtrData.relationship(profileID: profile.id) == Relationship.none || self.dtrData.relationship(profileID: profile.id) == Relationship.followed {
                self.dtrCommands.profileFollow(profileID: profile.id){ respose in
                    respose.debug(methodName:"profileFollow")
                    profile.isFollower = true
                    profile.followed.append(dtrData.profileID)
                }
            }
        } else if index == "SEND FRIEND REQUEST" {
            if self.dtrData.relationship(profileID: profile.id) == Relationship.following {
                self.dtrCommands.sendFriendRequest(profileID: profile.id){ respose in
                    respose.debug(methodName:"sendFriendRequest")
                }
            }
        } else if index == "Accept Friend Request" {
            self.dtrCommands.createFriendShipRelation(profileID: profile.id) { (result) in
                result.debug(methodName:"createFriendShipRelation")
                if result.result == .ok {
                    self.dtrCommands.clearNotifcationFromServer(data: ["payloadID":notificationInfo.payloadID,"source":notificationInfo.source,"type":notificationInfo.type.rawValue,"target":notificationInfo.target]) { (result) in
                    }
                }
            }
        }
    }
    
    func returnRelation() -> (String,String,Bool,[String],String) {
        var status = ""
        var status1 = ""
        var relationBtnHidden = false
        var arrayCustom = [String]()
        var txtCenterDescrp = ""
        
        if self.dtrData.relationship(profileID: profile.id) == Relationship.none {
            status = "Not connected"
            status1 = "Follow"
            txtCenterDescrp = "Follow \(profile.name) to connect and see their rides for followers."
        }
        
        // if user follow me but me not follow again
        if self.dtrData.relationship(profileID: profile.id) == Relationship.followed {
            status = "Not connected"
            status1 = "Follow"
            txtCenterDescrp = "Follow \(profile.name) to connect and see their rides for followers."
        }
        
        if self.dtrData.relationship(profileID: profile.id) == Relationship.following {
            status = "Following"
            status1 = "SEND FRIEND REQUEST"
            txtCenterDescrp = "Send friend request to \(profile.name) to see friends-only rides and information."
            arrayCustom.append("Unfollow")
            DispatchQueue.main.async {
                userActionShow = true
            }
        }
        
        if self.dtrData.relationship(profileID: profile.id) == Relationship.invitedFriend {
            status = "Friend request sent"
            arrayCustom.append("Cancel friend request")
            DispatchQueue.main.async {
                userActionShow = true
            }
            status1 = "CANCEL REQUEST"
            relationBtnHidden = true
            txtCenterDescrp = "You sent \(profile.name) a friend request"
        }
        
        if self.dtrData.relationship(profileID: profile.id) == Relationship.follwoingWithInvite {
            status = "Following, Friend request sent"
            arrayCustom.append("Unfollow")
            arrayCustom.append("Cancel friend request")
            DispatchQueue.main.async {
                userActionShow = true
            }
            status1 = "CANCEL REQUEST"
            txtCenterDescrp = "You sent \(profile.name) a friend request"
        }
        
        if self.dtrData.relationship(profileID: profile.id) == Relationship.acceptedFriend {
            status = "Friend"
            status1 = ""
            arrayCustom.append("UnFriend")
            txtCenterDescrp  = ""
            DispatchQueue.main.async {
                userActionShow = true
                isAlreadyFriend = true
            }
        }
        
        if self.dtrData.relationship(profileID: profile.id) == Relationship.requestedFriend {
            status = ""
            status1 = "Accept Friend Request"
            txtCenterDescrp = ""
            arrayCustom.append("Ignore")
            DispatchQueue.main.async {
                userActionShow = true
            }
        }
        return (status,status1,relationBtnHidden,arrayCustom,txtCenterDescrp)
    }
}
