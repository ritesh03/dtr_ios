//
//  ProfileImageFullScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 25/03/21.
//

import SwiftUI
import SDWebImage
import SDWebImageSwiftUI


struct ProfileImageFullScreen: View {
    @Binding var userProfile: DtrProfile
    @State var imagaName:Image
    @Environment(\.presentationMode) var presentationMode
    @State var dragOffset: CGSize = CGSize.zero
    @State var dragOffsetPredicted: CGSize = CGSize.zero
    @State private var image = Image(ImageConstantsName.PersonPlaceholderUserImg)
    @ObservedObject var dtrData = DtrData.sharedInstance
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            VStack {
                HStack(alignment: .top){
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        HStack {
                            Image(systemName: ImageConstantsName.ArrowLeftImg)
                                .foregroundColor(.white)
                        }}
                    Spacer()
                }.padding()
                Text("")
                    .frame(width: 0, height: UIScreen.screenHeight*0.16, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                VStack {
                    if userProfile.profileImage != "" {
                        WebImage(url:URL(string: userProfile.profileImage), options: [.progressiveLoad], isAnimating: .constant(true))
                            .renderingMode(.original)
                            .resizable()
                            .scaledToFit()
                            .frame(minWidth: UIScreen.main.bounds.size.width, idealWidth: UIScreen.main.bounds.size.width, maxWidth: UIScreen.main.bounds.size.width, minHeight: UIScreen.main.bounds.size.width, idealHeight: UIScreen.main.bounds.size.width, maxHeight: UIScreen.main.bounds.size.width, alignment: .center)
                            .pinchToZoom()
                    } else {
                        Image(uiImage: self.dtrData.profilePictureWithName(profileID: userProfile.id, inLarge: true))
                            .renderingMode(.original)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(height: 350)
                            .pinchToZoom()
                    }
                    Spacer()
                }
            }
        }
    }
}

struct ProfileImageScreen_Previews: PreviewProvider {
    static var previews: some View {
        ProfileImageFullScreen(userProfile: .constant(DtrProfile.default), imagaName: Image(CommonAllString.BlankStr))
    }
}
