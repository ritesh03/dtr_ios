//
//  ReturnProfileImageNameCitySince.swift
//  dtr-ios-test
//
//  Created by apple on 21/07/21.
//

import SwiftUI

struct ReturnProfileImageNameCitySince: View {
    @Binding var userProfile: DtrProfile
    @State var showProfileImageScreen = false
    @ObservedObject var dtrData = DtrData.sharedInstance
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            HStack(alignment: .top) {
                ProfileHead(profile: userProfile, size: .large,comeFromAnoucmentScreen:false)
                    .onTapGesture {
                        showProfileImageScreen = true
                    }
                VStack(alignment: .leading,spacing: 10) {
                    Text(userProfile.name)
                        .foregroundColor(.white)
                        .font(.system(size: 17, weight: .bold, design: .default))
                    Text(userProfile.defaultStatus)
                        .font(.system(size: 14, design: .default))
                        .foregroundColor(.white)
                        .opacity(0.65)
                }
            }
            
            HStack(alignment:.center) {
                if userProfile.cityState != CommonAllString.BlankStr && userProfile.cityState != CommonAllString.NullStr {
                    Text(userProfile.cityState).fixedSize(horizontal: false, vertical: true)
                        .foregroundColor(.white)
                        .opacity(0.65)
                    Circle()
                        .frame(width: 5, height: 5)
                        .foregroundColor(Color.gray)
                }
                Text("DTR since \(memberSince(self.userProfile.activated))").fixedSize(horizontal: false, vertical: true)
                    .foregroundColor(.white)
                    .opacity(0.65)
            }.frame(width: UIScreen.screenWidth - 25, alignment: .leading)
        }.padding()
        .fullScreenCover(isPresented: $showProfileImageScreen) {
            ProfileImageFullScreen(userProfile: $userProfile, imagaName: Image(uiImage: self.dtrData.profilePicture(profileID: self.userProfile.id)))
        }
    }
}

struct ReturnProfileImageNameCitySince_Previews: PreviewProvider {
    static var previews: some View {
        ReturnProfileImageNameCitySince(userProfile: .constant(DtrProfile.default))
    }
}
