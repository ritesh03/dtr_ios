//
//  ReturnProfileDescriptionVw.swift
//  dtr-ios-test
//
//  Created by apple on 21/07/21.
//

import SwiftUI

struct ReturnProfileDescriptionVw: View {
    @Binding var userProfile:DtrProfile
    @Binding var showEveryThingDone : Bool
    @State var screenWidth :CGFloat = 0.0
    
    var body: some View {
        Group{
            if showEveryThingDone {
                if userProfile.description != "" && userProfile.description != "\n" {
                    Divider()
                        .background(Color.white)
                    HStack {
                        VStack(alignment: .leading, spacing: 15) {
                            Text("BIO")
                            TextWithAttributedString(heightText:.constant(0.0),attributedString: NSAttributedString(string: userProfile.description), fixedWidth:screenWidth - 20 * 2)
                        }.padding(.horizontal)
                    }
                }
            }
        }.onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
                showEveryThingDone = true
            }
        }
    }
}
