//
//  ReturnUpcomingRides.swift
//  dtr-ios-test
//
//  Created by apple on 21/07/21.
//

import SwiftUI

struct ReturnUpcomingRides: View {
    @Binding var day : Int
    @Binding var userProfile : DtrProfile
    @State private var upcomingRides = [DtrRide]()
    @ObservedObject var dtrData = DtrData.sharedInstance
    var body: some View {
        Group{
            VStack(alignment: .leading, spacing: 20) {
                if self.upcomingRides.count > 0 {
                    Divider()
                        .background(Color.white)
                    Text("UPCOMING RIDES")
                        .padding(.leading)
                    Divider()
                        .background(Color.white)
                }
            }
            
            if self.upcomingRides.count > 0 {
                ForEach(self.upcomingRides.indices, id: \.self) { ride in
                    let isOn = Binding(
                        get: {
                            ride < self.upcomingRides.count ? self.upcomingRides[ride] : DtrRide.default
                        },
                        set: {
                            let isIndexValid = self.upcomingRides.indices.contains(ride)
                            if isIndexValid {
                                self.upcomingRides[ride] = $0
                            }
                        }
                    )
                    
                    NavigationLink(destination: RideDetail(day:$day,myRideForDay: isOn,presentModelOldHistoryScreen: .constant(false),myRideForDay1: RideEditViewModel.init(ride: isOn.wrappedValue))) {
                        OtherUserRideMiniViewScreen(day: .constant(0), RideForDayDetail: RideEditViewModel.init(ride: isOn.wrappedValue), currentRideUserDay: .constant(DtrRide.default),isComesFromProfileScreen:true)
                    }
                }
            }
        }
        .onAppear {
            self.dtrData.upcomingRides(for: userProfile) { profileRides in
                //let showRides = profileRides.filter { $0.visibleTo.contains(DtrData.sharedInstance.profile.id) }
                self.upcomingRides = profileRides
            }
        }
    }
}
