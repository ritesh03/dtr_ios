//
//  ProfileScreen.swift
//  dtr-ios-test
//
//  Created by Mobile on 21/01/21.
//


import SwiftUI
import LinkPresentation
import Firebase
import FirebaseDynamicLinks
import MobileCoreServices
import MessageUI

struct ProfileScreen: View {
    
    @Binding var updatedDismissView:Bool
    @Binding var day : Int
    @Binding var userProfile : DtrProfile
    
    @State var showPopUpForSendRequestFollow = false
    @State var notificationInfo :AppNotification = AppNotification(data: [CommonAllString.BlankStr:CommonAllString.BlankStr])
    @State var creatingLinks = false
    @State var profileImage =  Image(CommonAllString.BlankStr)
    @State var showBackButtonOrNot = false
    
    @ObservedObject var dtrData = DtrData.sharedInstance
    @ObservedObject var dtrCommands = DtrCommand.sharedInstance
    @State var activeSheet: ActiveSheetForProfileScreen?
    @Environment(\.presentationMode) var presentationMode
    
    @State private var links = [Any]()
    @State private var copyURlLink = CommonAllString.BlankStr
    @State private var isShareLinkOn = false
    @State private var isAlreadyFriend = false
    @State private var showSettingScreen = false
    @State private var showEveryThingDone = false
    @State private var showPopUpAfterFollowButton = false
    @State private var showEditProfileScreen = false
    @State private var copyLinkCodeBtnText = "SHARE MY FRIEND LINK"
    @State private var getStatusMutaluser = ""
    @State var showCreateGroupScreen = false
    
    var body: some View {
        ZStack {
            Color.init(ColorConstantsName.MainThemeBgColour)
                .ignoresSafeArea()
            GeometryReader { geo in
                ScrollView(.vertical) {
                    VStack(alignment: .center) {
                        //Text(updatedDismissView == true ? "Updated":"")
                        ReturnProfileImageNameCitySince(userProfile: $userProfile)
                        
                        ReturnGroupSection(userProfile:userProfile)
                        if dtrData.profileID == userProfile.id {
                            EditShareCrateBtn
                        }
                        
                        FollowerUnFollowCount(getStatusMutaluser:$getStatusMutaluser,showPopUpForSendRequestFollow:$showPopUpForSendRequestFollow,notificationInfo:notificationInfo,profile: $userProfile, showPopUpSendRequest: $showPopUpAfterFollowButton, isAlreadyFriend: $isAlreadyFriend)
                        
                        ShowPhoneNumberAndEmail
                        
                        ReturnProfileEmailPhone(isAlreadyFriend: $isAlreadyFriend, userProfile: $userProfile, showPopUpAfterFollowButton: $showPopUpAfterFollowButton)
                        
                        ReturnProfileDescriptionVw(userProfile: $userProfile,showEveryThingDone:$showEveryThingDone,screenWidth:geo.size.width == 0.0 ? UIScreen.screenWidth : geo.size.width)
                        
                        if userProfile.id != dtrData.profile.id || dtrData.profile.friends.contains(userProfile.id) || dtrData.profile.followed.contains(userProfile.id) {
                            ReturnUpcomingRides(day: $day, userProfile: $userProfile)
                        }
                    }
                    .frame(width: geo.size.width)
                }
            }.padding(.top,5)
            
            NavigationLink(destination:  ProfileEditScrn(dismissView:$showEditProfileScreen, rideDetailDay: RideEditViewModel.init(ride: DtrRide.default), displayName: dtrData.profile.name, displayLocation: dtrData.profile.cityState, displayCountry: dtrData.profile.country, bio: dtrData.profile.description, email: dtrData.profile.email ?? CommonAllString.BlankStr, phone: dtrData.profile.phone ?? CommonAllString.BlankStr), isActive: $showEditProfileScreen){
                EmptyView()
            }
            
            NavigationLink(destination:SettingScreen(dismissView:$showSettingScreen,comesFromDirectHomeScreen:showBackButtonOrNot), isActive: $showSettingScreen){
                EmptyView()
            }
            
            NavigationLink(destination:CreateGroupScreen(dismissView: $showCreateGroupScreen, updateCommandRunOrNot: .constant(false),  comeFomEditScreen:false,groupDetailEdit: GroupEditViewModel.init(group: DtrGroup.default)), isActive: $showCreateGroupScreen){
                EmptyView()
            }
        }
        .onDisappear{
            self.updatedDismissView = true
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarTitle(CommonAllString.BlankStr, displayMode: .inline)
        .navigationTitle(CommonAllString.BlankStr)
        .navigationViewStyle(.stack)//ritesh
        .navigationBarTitleDisplayMode(.inline)
        //.navigationBarItems(leading:AnyView(leadingButton),trailing:AnyView(self.trailingButton))
        
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                leadingButton.frame(width: 50, alignment: .leading)
            }
            
            ToolbarItem(placement: .navigationBarTrailing) {
                trailingButton
            }
        }.foregroundColor(Color.white)
            .onAppear {
                Analytics.logEvent(AnalyticsScreen.profile.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
                if dtrData.profileID == userProfile.id {
                    isAlreadyFriend = true
                }
                showEveryThingDone = true
                callGetMutualConectionStatus()
            }
        
            .background(EmptyView().sheet(isPresented: self.$isShareLinkOn) {
                Modal(isPresented: self.$isShareLinkOn, title: ProfileAndEditScreenConstant.Share_Profile_Str) {
                    ShareProfile(isPresented: self.$isShareLinkOn, links: links)
                }
                .onAppear {
                    Analytics.logEvent(AnalyticsScreen.friendLink.rawValue, parameters: [AnalyticsParameterScreenName:AnalyticsScreen.navigationLink.rawValue])
                }
            })
    }
    
    func callGetMutualConectionStatus() {
        dtrCommands.getMutualConnectionParticularUser(profileID: userProfile.id) { response in
            if response.result == .ok {
                let dictUsr = [asDataDict(response.output)]
                for i in 0..<dictUsr.count {
                    if let val = dictUsr[i] as? [String:String] {
                        for objectID in val.keys {
                            if let name = val[objectID] {
                                if name != "" {
                                    if !name.contains("undefined") {
                                        getStatusMutaluser = name
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    var leadingButton: some View {
        HStack{
            if showBackButtonOrNot {
                NavigtionItemBar(isImage: true, isSystemImage: true, isText:true,textTitle: CommonAllString.BlankStr, systemImageName: ImageConstantsName.ArrowLeftImg, action: {
                    UserStore.shared.isRootClassSearch = false
                    presentationMode.wrappedValue.dismiss()
                })
            }else if dtrData.profileID == userProfile.id {
                Text(ProfileAndEditScreenConstant.Profile_Str)
            }
        }
    }
    
    
    var EditShareCrateBtn: some View {
        VStack(alignment: .leading, spacing: 20) {
            VStack{
                CustomButtonCircleWithCustomProperty(btnTitle: ProfileAndEditScreenConstant.Edit_Profile_Str, btnWidth: UIScreen.screenWidth*0.8, btnHeight: 10, backgroundColord: false, txtColorAccent:true,strokeColor:true,textBold:true) {
                    showEditProfileScreen = true
                }
                
                CustomButtonCircleWithCustomProperty(btnTitle: copyLinkCodeBtnText, btnWidth: UIScreen.screenWidth*0.8, btnHeight: 10, backgroundColord: false, txtColorAccent:true,strokeColor:true,textBold:true) {
                    
                    Analytics.logEvent(AnalyticsObjectEvent.share_friend_link.rawValue, parameters: nil)
                    if self.dtrData.profile.linkCode == CommonAllString.BlankStr {
                        self.dtrData.createFriendLink() { link in
                            
                            self.constructLinks(isCodeCopyForNote: true)
                        }
                    }else{
                        self.constructLinks(isCodeCopyForNote: true)
                    }
                }
                
                CustomButtonCircleWithCustomProperty(btnTitle: "CREATE A GROUP", btnWidth: UIScreen.screenWidth*0.8, btnHeight: 10, backgroundColord: false, txtColorAccent:true,strokeColor:true,textBold:true) {
                    self.showCreateGroupScreen = true
                }
                
            }
            
        }.padding()
    }
    
    var ShowPhoneNumberAndEmail: some View {
        Group {
            if isAlreadyFriend || dtrData.profileID == userProfile.id {
                if userProfile.phone != CommonAllString.BlankStr ||  userProfile.email != CommonAllString.BlankStr &&  userProfile.phone != nil &&  userProfile.email != nil {
                    Divider()
                        .background(Color.white)
                        .offset(x: 0, y:-6)
                }
            }
        }
    }
    
    var trailingButton: some View {
        HStack {
            if dtrData.profileID == userProfile.id {
                NavigationLink(destination: FollowerScreen(currentDay:$day,comesFromChatScreen:true)) {
                    Image(systemName: ImageConstantsNameForChatScreen.PersonImg)
                        .padding(.trailing)
                }
                Button(action: {
                    Analytics.logEvent(AnalyticsObjectEvent.share_friend_link.rawValue, parameters: nil)
                    if self.dtrData.profile.linkCode == "" {
                        self.dtrData.createFriendLink() { link in
                            self.constructLinks(isCodeCopyForNote: false)
                        }
                    }else{
                        self.constructLinks(isCodeCopyForNote: false)
                    }
                }) {
                    Image(ImageConstantsName.DTRShareIconImg)
                        .resizable().frame(width: 20, height: 20)
                }.padding(.trailing)
                Button(action: {
                    showSettingScreen = true
                }) {
                    Image(ImageConstantsName.SettingIconImg)
                        .resizable().frame(width: 20, height: 20)
                }
            }else{
                EmptyView()
            }
        }
    }
    
    func constructLinks(isCodeCopyForNote:Bool) {
        self.creatingLinks = true
        if UserStore.shared.isLocalBuild == true {
            guard let link = URL(string: "https://socialactive.page.link/friend/\(self.dtrData.profile.linkCode)") else {
                print("ERROR constructing initial URL")
                return
            }
            let dynamicLinksDomainURIPrefix = AppInformation.AppDeafultStagingUrl
            createLink(link:link,dynamicLinksDomainURIPrefix:dynamicLinksDomainURIPrefix,isCodeCopyForNote:isCodeCopyForNote)
        }else{
            guard let link = URL(string: "https://socialactive.app/friend/\(self.dtrData.profile.linkCode)") else {
                print("ERROR constructing initial URL")
                return
            }
            let dynamicLinksDomainURIPrefix = AppInformation.AppDeafultUrl
            createLink(link:link,dynamicLinksDomainURIPrefix:dynamicLinksDomainURIPrefix,isCodeCopyForNote:isCodeCopyForNote)
        }
    }
    
    func createLink(link:URL,dynamicLinksDomainURIPrefix:String,isCodeCopyForNote:Bool){
        let appBundleID = AppInformation.AppBundleId
        if let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix) {
            linkBuilder.navigationInfoParameters = DynamicLinkNavigationInfoParameters()
            linkBuilder.navigationInfoParameters?.isForcedRedirectEnabled = true
            linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: appBundleID)
            linkBuilder.iOSParameters?.appStoreID = AppInformation.AppStoreId
            linkBuilder.iOSParameters?.minimumAppVersion = AppInformation.AppMinimumVersion
            linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: appBundleID)
            linkBuilder.androidParameters?.minimumVersion = 0
            linkBuilder.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
            linkBuilder.socialMetaTagParameters!.title = dtrData.profile.name
            linkBuilder.socialMetaTagParameters!.descriptionText = "Become Friends with \(dtrData.profile.name) On the Down to Ride App"
            linkBuilder.socialMetaTagParameters!.imageURL = URL(string:dtrData.profile.profileImage)
            
            guard let longDynamicLink = linkBuilder.url else {
                print("ERROR in linkBuilder")
                return
            }
            DynamicLinkComponents.shortenURL(longDynamicLink, options: nil) { url, warnings, error in
                if let error = error {
                    print("error shortening link: \(error.localizedDescription)")
                    return
                }
                if let url = url {
                    print("Profile Url Share = ",url)
                    let message = "\(self.dtrData.profile.name) \(ProfileAndEditScreenConstant.SHARE_LINK_URL_STR)"
                    let itemSource = ShareActivityItemSource(type: .friend, message: message, url: url, imageURl: dtrData.profile.profileImage)
                    self.links = [url, itemSource]
                    self.creatingLinks = false
                    copyURlLink = "\("\(message)")\n\n\(url.absoluteString)"
                    UIPasteboard.general.setValue(copyURlLink,forPasteboardType: kUTTypePlainText as String)
                    copyURlLink = url.absoluteString
                    isShareLinkOn = true
                    return
                }
            }
        }
    }
}

enum ActiveSheetForProfileScreen: Identifiable {
    case pfoileImgVwScreen,showMailScreen
    var id: Int {
        hashValue
    }
}

struct ProfileAndEditScreenConstant {
    
    static let Edit_Profile_Str = "EDIT PROFILE"
    static let Profile_Str = "Profile"
    static let SHARE_LINK_URL_STR = " would like to be friends within the Down to Ride (DTR) app"
    static let Share_Profile_Str = "Share Profile"
}
