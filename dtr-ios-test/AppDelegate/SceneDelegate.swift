//
//  SceneDelegate.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/26/20.
//

import UIKit
import SwiftUI
import Firebase

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    var dtrData = DtrData.sharedInstance
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        if let url = URLContexts.first?.url {
           
            if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url){
                if dynamicLink.url?.absoluteString != "" && dynamicLink.url?.absoluteString != nil {
                    getLinkDetail(resolved:dynamicLink.url!.absoluteString)
                }
            } else {
                print("False")
            }
        }
    }

    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        let contentView = MainContentView()
            
        let firstTime = UserDefaults.standard.bool(forKey: "isFirstTime")
        
        if let windowScene = scene as? UIWindowScene {
            if firstTime == false   {
                Analytics.logEvent(AnalyticsObjectEvent.first_launch.rawValue, parameters: nil)
                
            }
            UserDefaults.standard.set(true, forKey: "isFirstTime")
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = UIHostingController(rootView: contentView.environmentObject(self.dtrData))
            self.window = window
            window.makeKeyAndVisible()
        }
        
        
        if let url = connectionOptions.urlContexts.first?.url {
            if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url){
                if dynamicLink.url?.absoluteString != "" && dynamicLink.url?.absoluteString != nil {
                    getLinkDetail(resolved:dynamicLink.url!.absoluteString)
                }
            } else {
                print("False")
            }
        }
        
        if let url = connectionOptions.userActivities.first?.webpageURL {
            if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
                if dynamicLink.url?.absoluteString != "" && dynamicLink.url?.absoluteString != nil {
                    getLinkDetail(resolved:dynamicLink.url!.absoluteString)
                }
            } else {
                shortenUrlToGetInfo(url: url)
            }
        }
    }
    
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        if let url = userActivity.webpageURL {
            shortenUrlToGetInfo(url: url)
        }
    }
    
    func shortenUrlToGetInfo(url:URL){
        let resolver = ShortenedLinkResolver(url) { resolved, error in
            if let error = error {
                print("link resolver error: \(error.localizedDescription)")
                return
            }
            self.getLinkDetail(resolved:resolved)
        }
        resolver.start()
    }
    
    func getLinkDetail(resolved:String){
        let linkInfo = parseLinkPath(resolved)
        print("Link Info continue = ", linkInfo)
        UserStore.shared.dyanamicLinkNotWorking = "\("\(linkInfo.source)" + "\(linkInfo.type)" + "\(linkInfo.data)")"
        switch linkInfo.type {
        case "ride":
           
            Analytics.logEvent(AnalyticsObjectEvent.open_link_ride.rawValue, parameters: nil)
            self.dtrData.processRideLink(type: linkInfo.type, source: linkInfo.source, rideID: linkInfo.data)
        case "friend":
          
            Analytics.logEvent(AnalyticsObjectEvent.open_link_friend.rawValue, parameters: nil)
            self.dtrData.processFriendLink(type: linkInfo.type, source: linkInfo.source)
        case "code":
        
            Analytics.logEvent(AnalyticsObjectEvent.open_link_code.rawValue, parameters: nil)
            self.dtrData.processCodeLink(source: linkInfo.source)
        case "group":
          
            Analytics.logEvent(AnalyticsObjectEvent.open_link_group.rawValue, parameters: nil)
            self.getGroupDetails(type:linkInfo.type,groupID:linkInfo.source)
        default:
            print("unknown link type: \(resolved)")
        }
    }
    
    
    func getGroupDetails(type:String,groupID:String){
        var source = groupID
        if let dotRange = source.range(of: "+") {
            source.removeSubrange(dotRange.lowerBound..<source.endIndex)
        }
        self.dtrData.processGroupLink(type: type, source: source, groupID: groupID)
    }
    
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
}
