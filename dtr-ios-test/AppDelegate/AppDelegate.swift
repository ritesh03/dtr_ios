//
//  AppDelegate.swift
//  dtr-ios-harness
//
//  Created by Dan Kinney on 12/26/20.
//

import UIKit 
import UserNotifications
import Firebase

@UIApplicationMain//
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {
    var fromNotification: Bool = false
    let customURLScheme = AppInformation.AppBundleId
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = self.customURLScheme
        if UserStore.shared.isLocalBuild == true {
            print("Is Local Build Run = Yes")
            let filePath = Bundle.main.path(forResource: "GoogleService-Info-Local", ofType: "plist")!
            let options = FirebaseOptions(contentsOfFile: filePath)
            FirebaseApp.configure(options: options!)
        } else {
            print("Is Production Build Run = Yes")
            let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")!
            let options = FirebaseOptions(contentsOfFile: filePath)
            FirebaseApp.configure(options: options!)
        }
        Analytics.setAnalyticsCollectionEnabled(false)
        Messaging.messaging().delegate = self
        UserStore.shared.UpdateAndRestLocalStoreWhenStartingApp()
        	
        // Register with APNs
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (success, error) in
            if error == nil {
                if success == true {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }else  if success == false {
                } else {
                    print(error?.localizedDescription as Any)
                }
            }
        }
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool{
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
              let incomingURL = userActivity.webpageURL
        else {
            return false
        }
        print("*** \(#function) incomingURL: \(incomingURL)")
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            print("handle universal link!")
            if let link = dynamiclink {
                if let url = link.url {
                    print("url: \(url.absoluteString)")
                }
            }
        }
        return handled
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
        Analytics.logEvent("receive_notification", parameters: nil)
        Analytics.logEvent(AnalyticsObjectEvent.receive_notification.rawValue, parameters: nil)
        print(userInfo)
    }
    
    
    @available(iOS 9.0, *)
    func handlePasswordlessSignIn(withURL url: URL) -> Bool {
        let link = url.absoluteString
        if Auth.auth().isSignIn(withEmailLink: link) {
            UserDefaults.standard.set(link, forKey: "Link")
            return true
        }
        return false
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        print("*** open URL: \(url.absoluteString)")
        if handlePasswordlessSignIn(withURL: url) {
            return true
        }
        return application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: "")
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        print("••• open URL: \(url.absoluteString)")
        if let source = sourceApplication {
            print("source application = \(source)")
        }
        if let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true),
           let path = components.path,let params = components.queryItems{
            print("    path: \(path)")
            print("    params: \(params)")
        }
        
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            // Handle the deep link. For example, show the deep-linked content or
            // apply a promotional offer to the user's account.
            if let deepLink = dynamicLink.url {
                print("deep link: \(deepLink.absoluteString)")
            }
            return true
        }
        print("*** \(#function) deep link failed to match pending dynamic link")
        return false
    }
    
    
    // Handle remote notification registration.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let deviceTokenString = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("didRegisterForRemoteNotificationsWithDeviceToken: \(deviceTokenString)")
        if let token = Messaging.messaging().fcmToken {
            if let currentToken = DtrData.sharedInstance.profile.pushToken {
                if currentToken != token {
                    DtrData.sharedInstance.updatePushToken(token: token)
                }else{
                    DtrData.sharedInstance.updatePushToken(token: token)
                }
            } else {
                DtrData.sharedInstance.updatePushToken(token: token)
            }
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
        let dataDict:[String: String] = ["token": fcmToken!]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //The token is not currently available.
        print("Remote notification support is unavailable due to error: \(error.localizedDescription)")
    }
}


extension AppDelegate: UNUserNotificationCenterDelegate {
    // NOTE: This is here to satisfy the requirements of the delegate, but will not actually be called - another delegate will override it
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping(UNNotificationPresentationOptions) -> Swift.Void){
        print("••• willPresent Notification \(notification.request.content.userInfo) Identifier: \(notification.request.identifier)")
        completionHandler([.banner, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping() -> Swift.Void) {
        print("••• didReceive Notification (background) \(response.notification.request.content.userInfo) Identifier: \(response.notification.request.identifier)")
        Analytics.logEvent(AnalyticsObjectEvent.click_notification.rawValue, parameters: nil)
        fromNotification = true
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        print("••• openSettingsFor Notification")
    }
}
