//
//  testTapAndHalfUITests.swift
//  dtr-ios-testTapAndHalfUITests
//
//  Created by apple on 07/12/21.
//

import XCTest

class testTapAndHalfUITests: XCTestCase {
    
    private var app:XCUIApplication!
    
    let create_Ride_Button_Expction = XCTestExpectation(description: "create_Ride_Button_Expction")
    
    override func setUp() {
        super.setUp()
        self.app = XCUIApplication()
        self.app.launch()
    }
    
    func test_Button_Click_From_Home_Screen(){
        sleep(20)
        create_Ride_Button_Expction.expectedFulfillmentCount = 2
        let downToRideButton = self.app.buttons["I'M DOWN TO RIDE!"]
        if downToRideButton.exists {
            XCTAssertTrue(downToRideButton.exists)
            downToRideButton.tap()
            click_On_Update_Button()
            create_Ride_Button_Expction.fulfill()
        }else{
            XCTAssertTrue(downToRideButton.exists == false)
        }
    }
    
    
    func click_On_Update_Button(){
        let createRideButton = self.app.buttons["Update_Create_Ride"]
        XCTAssertTrue(createRideButton.exists)
        if createRideButton.exists {
            createRideButton.tap()
            create_Ride_Button_Expction.fulfill()
            sleep(10)
        }
    }
}
