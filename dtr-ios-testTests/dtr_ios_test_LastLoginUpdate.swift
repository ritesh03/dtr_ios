//
//  dtr_ios_test_LastLoginUpdate.swift
//  dtr-ios-testTests
//
//  Created by apple on 19/11/21.
//

import XCTest
@testable import dtr_ios_test

class dtr_ios_test_LastLoginUpdate: XCTestCase {
    
    let lastLoginUpdatedOrNt = XCTestExpectation(description: "lastLoginUpdatedOrNt")
    
    func testLastLoginSuccessFullyUpdatedOrNot() {
        DtrData.sharedInstance.loadProfile(profileID: UserStore.shared.profileIdForUnitTest) { profileDetails in
            if profileDetails != nil {
                if profileDetails?.lastSignIn != nil {
                    let differnce = UserStore.shared.lastLoginUpdatedTimeStamp - profileDetails!.lastSignIn
                    print("Differnce in seconds = ",differnce)
                    XCTAssert(differnce < 10)
                }
                self.lastLoginUpdatedOrNt.fulfill()
            }
        }
        wait(for: [lastLoginUpdatedOrNt], timeout: 60.0)
    }
}
