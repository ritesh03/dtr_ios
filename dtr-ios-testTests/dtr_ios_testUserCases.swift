//
//  dtr_ios_testUserCases.swift
//  dtr-ios-testTests
//
//  Created by apple on 22/11/21.
//

import XCTest
@testable import dtr_ios_test

class dtr_ios_testUserCases: XCTestCase {
    
    let signIn_Verify_Correct_Otp = XCTestExpectation(description: "signIn_Verify_Correct_Otp")
    let signIn_Verify_Wrong_Otp = XCTestExpectation(description: "signIn_Verify_Wrong_Otp")
    let signIn_Verify_New_User = XCTestExpectation(description: "signIn_Verify_New_User")
    let create_New_User = XCTestExpectation(description: "create_New_User")
    
    //MARK: Correct Number And Correct OTP ------------------------------------------------------------------------------------------------
    
        func testsendOTPAndVerify() {
            signIn_Verify_Correct_Otp.expectedFulfillmentCount = 2
            let signupClassMethod = SignupScreen(phoneNumber: .constant("2485551008"))
            signupClassMethod.verify { response, verificationId in
                XCTAssert(response == true)
                if response == true {
                    self.signIn_Verify_Correct_Otp.fulfill()
                    self.testVerifyOTPCorrect(OtpEndrdCode: "000000", verificationID: verificationId,type:0)
                }
            }
            wait(for: [signIn_Verify_Correct_Otp], timeout: 60.0)
        }
    
        func testVerifyOTPCorrect(OtpEndrdCode:String,verificationID:String,type:Int) {
            let verifyClassTest = VerficationCodeScreen(phoneNumber: .constant(CommonAllString.BlankStr), verificationID: .constant(verificationID))
            verifyClassTest.submitPin(OtpStrng: OtpEndrdCode) { response in
                XCTAssert(response == "Already_User")
                self.signIn_Verify_Correct_Otp.fulfill()
            }
        }
    
    //MARK: Correct Number And Wrong OTP -------------------------------------------------------------------------------------------------
    
        func testCorrectNumberAndWrongOTP() {
            signIn_Verify_Wrong_Otp.expectedFulfillmentCount = 2
            let signupClassMethod = SignupScreen(phoneNumber: .constant("2485551008"))
            signupClassMethod.verify { response, verificationId in
                print(" Correct Number And Wrong OTP = ",response,verificationId)
                XCTAssert(response == true && verificationId != "")
                if response == true {
                    self.signIn_Verify_Wrong_Otp.fulfill()
                    self.testVerifyOTPWrong(OtpEndrdCode: "123456", verificationID: verificationId, type: 1)
                }
            }
            wait(for: [signIn_Verify_Wrong_Otp], timeout: 60.0)
        }
    
        func testVerifyOTPWrong(OtpEndrdCode:String,verificationID:String,type:Int) {
            let verifyClassTest = VerficationCodeScreen(phoneNumber: .constant(CommonAllString.BlankStr), verificationID: .constant(verificationID))
            verifyClassTest.submitPin(OtpStrng: OtpEndrdCode) { response in
                XCTAssert(response == "Wrong_OTP")
                self.signIn_Verify_Wrong_Otp.fulfill()
            }
        }
    
    
    //MARK: New User-------------------------------------------------------------------------------------------------
    
    func testForNewUser() {
        signIn_Verify_New_User.expectedFulfillmentCount = 2
        // Need new number for this use-case
        let signupClassMethod = SignupScreen(phoneNumber: .constant("2485551009"))
        signupClassMethod.verify { response, verificationId in
            XCTAssert(response == true && verificationId != "")
            if response == true {
                self.signIn_Verify_New_User.fulfill()
                self.testVerifyForNewUser(OtpEndrdCode: "000000", verificationID: verificationId, type: 1)
            }
        }
        wait(for: [signIn_Verify_New_User], timeout: 60.0)
    }
    
    func testVerifyForNewUser(OtpEndrdCode:String,verificationID:String,type:Int) {
        let verifyClassTest = VerficationCodeScreen(phoneNumber: .constant(CommonAllString.BlankStr), verificationID: .constant(verificationID))
        verifyClassTest.submitPin(OtpStrng: OtpEndrdCode) { response in
            print("Response = ",response)
            if response == "Already_User" {
                XCTAssert(response == "Already_User")
            }else{
                XCTAssert(response == "First_Time_User")
                self.testCreate_New_User()
            }
            self.signIn_Verify_New_User.fulfill()
        }
    }
    
    
    func testCreate_New_User(){
        create_New_User.expectedFulfillmentCount = 1
        let create_Profil_View = CreateProfileScreen()
        create_Profil_View.updateAndCreateNewUser()
        wait(for: [create_New_User], timeout:180.0)
    }
}
